# Kundematch | Håndværker.dk PartnerLogin

Det perfekte match

## Notes before deploying

- Comment English to set Danish as default language in main.dart
- Increment version in pubspec.yaml

## Deployment options

1. Codemagic (Option 1)

   1. update build for platforms (set it to ios/android or both on which platform you want to deploy)
   2. Note: this would push the app to the appstore testflight and playstore internal testing

2. Uploading Appbundle to playstore(Option 2) (Android only)

   1. Create new release
   2. Upload appbundle
   3. Review release
   4. Rollout release

3. Upload to testflight through Xcode (Option 2) (iOS only)

   1. Set team profile for distribution
   2. Set generic device for build
   3. Archive build
   4. Set distribution profile (if manual, can be automatic)
   5. Validate archive
   6. Deploy archive
   7. In testflight: add build to then submit for review

## Store links

- [Playstore](https://play.google.com/store/apps/details?id=dk.haandvaerker.partnerapp)
- [AppStore](https://apps.apple.com/us/app/h%C3%A5ndv%C3%A6rker-dk-partnerlogin/id1529848721)
