library modal_manager;

import 'dart:async';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

part 'modal_templates.dart';

final modalManager = ModalManager();

class ModalManager {
  ModalManager._privateConstructor();

  static final ModalManager _modalManager = ModalManager._privateConstructor();

  factory ModalManager() {
    return _modalManager;
  }

  BuildContext? _context;
  bool _isShowingModal = false;
  OverlayEntry? _overlayEntry;

  void setContext(BuildContext context) {
    _context = context;
  }

  void showLoadingModal({String? message}) {
    if (!_isShowingModal) {
      _isShowingModal = true;
      _overlayEntry = OverlayEntry(
        builder: (context) => CustomLoader(message: message),
      );
      if (_context != null && _overlayEntry != null) {
        Overlay.of(_context!).insert(_overlayEntry!);
      }
    }
  }

  void hideLoadingModal() {
    if (_isShowingModal) {
      if (_overlayEntry != null) {
        _overlayEntry!.remove();
        _overlayEntry = null;
      }
      _isShowingModal = false;
    }
  }
}
