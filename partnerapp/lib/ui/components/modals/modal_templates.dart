part of 'modal_manager.dart';

class CustomLoader extends StatefulWidget {
  const CustomLoader({super.key, this.message});
  final String? message;
  @override
  CustomLoaderState createState() => CustomLoaderState();
}

class CustomLoaderState extends State<CustomLoader> {
  int percent = 0;
  late Timer timer;

  @override
  void initState() {
    timer = Timer.periodic(const Duration(milliseconds: 1000), (e) {
      final tempPercent = ((e.tick / 10) * 100).round();
      setState(() {
        if (tempPercent >= 100) {
          percent = 99;
          timer.cancel();
          // percent=0;
        } else {
          percent = tempPercent;
        }
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.black.withValues(alpha: 0.4),
      shadowColor: Colors.black.withValues(alpha: 0.4),
      elevation: 8,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Center(
          child: SingleChildScrollView(
            physics: const NeverScrollableScrollPhysics(),
            child: Container(
              padding: const EdgeInsets.all(40),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                children: [
                  CircularPercentIndicator(
                    radius: 55,
                    animation: true,
                    backgroundColor: Colors.grey,
                    circularStrokeCap: CircularStrokeCap.round,
                    percent: percent / 100,
                    animateFromLastPercent: true,
                    center: Text(
                      '$percent%',
                      style: const TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    ),
                    progressColor: PartnerAppColors.accentBlue,
                  ),
                  if (widget.message != null &&
                      (widget.message ?? '').isNotEmpty) ...[
                    SmartGaps.gapH20,
                    Text(
                      widget.message ?? '',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                            fontSize: 16,
                          ),
                    ),
                  ]
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
