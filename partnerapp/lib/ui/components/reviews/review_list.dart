import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/recommendation_read_answer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';

Future<List<int>?> chooseReviewListDialog(
    {required BuildContext context, required List<int> alreadySelectedId}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: ReviewList(alreadySelectedId: alreadySelectedId))),
      );
    },
  );
}

class ReviewList extends StatefulWidget {
  const ReviewList({super.key, required this.alreadySelectedId});
  final List<int> alreadySelectedId;

  @override
  State<ReviewList> createState() => _ReviewListState();
}

class _ReviewListState extends State<ReviewList> {
  List<int> selectedReviews = [];

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      setState(() {
        selectedReviews.addAll([...widget.alreadySelectedId]);
      });
      final vm = context.read<RecommendationReadAnswerViewModel>();
      vm.setBusy(true);
      vm.currentPage = 1;
      await tryCatchWrapper(
              context: myGlobals.homeScaffoldKey!.currentContext,
              function: Future.value(vm.getRecommendations(rating: 0)))
          .whenComplete(() => vm.setBusy(false));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<RecommendationReadAnswerViewModel>(builder: (_, vm, __) {
      if (vm.busy) return const Center(child: ConnectivityLoader());

      return Column(
        children: [
          Text(
            tr('max_5_reviews'),
            style: Theme.of(context).textTheme.displayLarge!.copyWith(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: PartnerAppColors.darkBlue,
                ),
          ),
          SmartGaps.gapH20,
          GridView.builder(
            itemBuilder: (context, index) {
              final recommendation = vm.recommendations[index];
              return InkWell(
                onTap: () {
                  if (selectedReviews.length != 5) {
                    setState(() {
                      if (selectedReviews.contains(recommendation.id)) {
                        selectedReviews.remove(recommendation.id);
                      } else {
                        selectedReviews.add(recommendation.id!);
                      }
                    });
                  }
                },
                child: Card(
                  shape: selectedReviews.contains(recommendation.id)
                      ? const RoundedRectangleBorder(
                          side: BorderSide(color: PartnerAppColors.malachite))
                      : null,
                  child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          RatingBar(
                            ignoreGestures: true,
                            itemSize: 14,
                            initialRating: num.parse('${recommendation.rating}')
                                .toDouble(),
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            ratingWidget: RatingWidget(
                              full: Image.asset(ImagePaths.singleHouse),
                              half: Image.asset(ImagePaths.halfHouse),
                              empty: Opacity(
                                opacity: 0.1,
                                child: Image.asset(ImagePaths.singleHouse),
                              ),
                            ),
                            itemPadding:
                                const EdgeInsets.symmetric(horizontal: 1.0),
                            onRatingUpdate: (value) {},
                          ),
                          SmartGaps.gapH20,
                          Text(
                              Formatter.formatDateStrings(
                                  type: DateFormatType.standardDate,
                                  dateString: recommendation.date!),
                              style: Theme.of(context)
                                  .textTheme
                                  .displayLarge!
                                  .copyWith(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: PartnerAppColors.darkBlue,
                                  )),
                          SmartGaps.gapH10,
                          Text(tr('header'),
                              style: Theme.of(context)
                                  .textTheme
                                  .displayLarge!
                                  .copyWith(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    color: PartnerAppColors.darkBlue,
                                  )),
                          Text(recommendation.title!,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context)
                                  .textTheme
                                  .displayLarge!
                                  .copyWith(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: PartnerAppColors.darkBlue,
                                  )),
                          SmartGaps.gapH10,
                          Text(tr('comments'),
                              style: Theme.of(context)
                                  .textTheme
                                  .displayLarge!
                                  .copyWith(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    color: PartnerAppColors.darkBlue,
                                  )),
                          Tooltip(
                              triggerMode: TooltipTriggerMode.tap,
                              message: recommendation.description,
                              child: Text(recommendation.description!,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: Theme.of(context)
                                      .textTheme
                                      .displayLarge!
                                      .copyWith(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color: PartnerAppColors.darkBlue,
                                      ))),
                        ],
                      )),
                ),
              );
            },
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 10,
              childAspectRatio: 0.9,
            ),
            itemCount: vm.recommendations.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
          ),
          SmartGaps.gapH20,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              if (vm.currentPage != 1) ...[
                GestureDetector(
                    onTap: () async {
                      vm.setBusy(true);
                      vm.currentPage = vm.currentPage - 1;
                      vm.setBusy(true);
                      await tryCatchWrapper(
                              context:
                                  myGlobals.homeScaffoldKey!.currentContext,
                              function: Future.value(
                                  vm.getRecommendations(rating: 0)))
                          .whenComplete(() => vm.setBusy(false));
                    },
                    child: const Icon(FeatherIcons.chevronLeft))
              ] else ...[
                Container()
              ],
              Text('${vm.currentPage}/${vm.lastPage}'),
              if (vm.currentPage == vm.lastPage) ...[
                Container()
              ] else ...[
                GestureDetector(
                    onTap: () async {
                      vm.currentPage = vm.currentPage + 1;
                      vm.setBusy(true);
                      await tryCatchWrapper(
                              context:
                                  myGlobals.homeScaffoldKey!.currentContext,
                              function: Future.value(
                                  vm.getRecommendations(rating: 0)))
                          .whenComplete(() => vm.setBusy(false));
                    },
                    child: const Icon(FeatherIcons.chevronRight))
              ],
            ],
          ),
          SmartGaps.gapH20,
          CustomDesignTheme.flatButtonStyle(
            height: 50,
            backgroundColor: PartnerAppColors.malachite,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            onPressed: () {
              Navigator.of(context).pop(selectedReviews);
            },
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Text(
                tr('select'),
                style: Theme.of(context).textTheme.displayLarge!.copyWith(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                    ),
              )
            ]),
          ),
        ],
      );
    });
  }
}
