import 'dart:io';

import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class CustomHeroGallery extends StatefulWidget {
  const CustomHeroGallery({
    super.key,
    this.images,
    this.backgroundColor,
    this.assetFiles,
  });

  final List<String>? images;
  final List<File>? assetFiles;
  final Color? backgroundColor;

  @override
  CustomHeroGalleryState createState() => CustomHeroGalleryState();
}

class CustomHeroGalleryState extends State<CustomHeroGallery> {
  List<String>? images;
  List<File>? assetFiles;
  bool verticalGallery = false;
  List<GalleryItem>? galleryItems;
  @override
  void initState() {
    galleryItems = [];
    images = widget.images;
    assetFiles = widget.assetFiles;

    if (images != null) {
      for (var image in images!) {
        galleryItems!.add(GalleryItem(id: image, resource: image));
      }
    } else {
      for (var asset in assetFiles!) {
        galleryItems!.add(GalleryItem(id: asset.path, assetImage: asset));
      }
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          child: Center(
            child: Container(
              height: 150.0,
              alignment: Alignment.topLeft,
              padding: const EdgeInsets.symmetric(vertical: 16),
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                physics: const ClampingScrollPhysics(),
                shrinkWrap: true,
                itemCount: images?.length ?? assetFiles?.length ?? 0,
                itemBuilder: (BuildContext context, int index) {
                  return GalleryItemThumbnail(
                    galleryItem: galleryItems![index],
                    onTap: () {
                      open(context, index);
                    },
                  );
                },
              ),
            ),
          ),
        ),
      ],
    );
  }

  void open(BuildContext context, final int index) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => GalleryPhotoViewWrapper(
          galleryItems: galleryItems,
          backgroundDecoration: BoxDecoration(
            color: widget.backgroundColor ?? Colors.transparent,
          ),
          initialIndex: index,
          scrollDirection: Axis.horizontal,
        ),
      ),
    );
  }
}

class GalleryPhotoViewWrapper extends StatefulWidget {
  GalleryPhotoViewWrapper({
    super.key,
    this.loadingBuilder,
    this.backgroundDecoration,
    this.minScale,
    this.maxScale,
    required this.initialIndex,
    required this.galleryItems,
    this.scrollDirection = Axis.horizontal,
  }) : pageController = PageController(initialPage: initialIndex);

  final LoadingBuilder? loadingBuilder;
  final Decoration? backgroundDecoration;
  final dynamic minScale;
  final dynamic maxScale;
  final int initialIndex;
  final PageController pageController;
  final List<GalleryItem>? galleryItems;
  final Axis scrollDirection;

  @override
  GalleryPhotoViewWrapperState createState() => GalleryPhotoViewWrapperState();
}

class GalleryPhotoViewWrapperState extends State<GalleryPhotoViewWrapper> {
  int? currentIndex;

  @override
  void initState() {
    currentIndex = widget.initialIndex;
    super.initState();
  }

  void onPageChanged(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black12,
      body: Container(
        decoration: widget.backgroundDecoration,
        constraints: BoxConstraints.expand(
          height: MediaQuery.of(context).size.height,
        ),
        child: Stack(
          alignment: Alignment.bottomRight,
          children: <Widget>[
            PhotoViewGallery.builder(
              scrollPhysics: const BouncingScrollPhysics(),
              builder: _buildItem,
              itemCount: widget.galleryItems!.length,
              loadingBuilder: widget.loadingBuilder,
              backgroundDecoration:
                  widget.backgroundDecoration as BoxDecoration?,
              pageController: widget.pageController,
              onPageChanged: onPageChanged,
              scrollDirection: widget.scrollDirection,
            ),
            Container(
              alignment: Alignment.bottomCenter,
              padding: const EdgeInsets.all(20),
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: widget.galleryItems!.length,
                itemBuilder: (BuildContext context, int index) {
                  if (index == currentIndex) {
                    return Align(
                      alignment: Alignment.bottomCenter,
                      child: Icon(
                        Icons.blur_circular,
                        color: Theme.of(context).colorScheme.primary,
                        size: 12,
                      ),
                    );
                  } else {
                    return const Align(
                      alignment: Alignment.bottomCenter,
                      child: Icon(
                        Icons.blur_circular,
                        color: Colors.black45,
                        size: 10,
                      ),
                    );
                  }
                },
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: GestureDetector(
                onTap: () => Navigator.of(context).pop(),
                child: Container(
                    margin: const EdgeInsets.all(15),
                    padding: const EdgeInsets.all(5),
                    decoration: const BoxDecoration(
                      color: Colors.black45,
                      shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black45,
                          blurRadius: 2.0,
                          spreadRadius: 0.0,
                          offset: Offset(2.0, 2.0),
                        ),
                      ],
                    ),
                    child: const Icon(
                      Icons.close,
                      color: Colors.white,
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }

  PhotoViewGalleryPageOptions _buildItem(BuildContext context, int index) {
    final GalleryItem item = widget.galleryItems![index];
    final image = item.resource != null
        ? NetworkImage(item.resource!)
        : FileImage(item.assetImage!);
    return PhotoViewGalleryPageOptions(
      imageProvider: image as ImageProvider,
      initialScale: PhotoViewComputedScale.contained * 0.7,
      minScale: PhotoViewComputedScale.contained * (0.5 + index / 10),
      maxScale: PhotoViewComputedScale.covered * 1.1,
      heroAttributes: PhotoViewHeroAttributes(tag: item.id!),
    );
  }
}

class GalleryItem {
  GalleryItem({
    this.id,
    this.resource,
    this.assetImage,
  });

  final String? id;
  final File? assetImage;
  final String? resource;
}

class GalleryItemThumbnail extends StatelessWidget {
  const GalleryItemThumbnail({super.key, this.galleryItem, this.onTap});

  final GalleryItem? galleryItem;

  final GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5.0),
      child: GestureDetector(
        onTap: onTap,
        child: Hero(
          tag: galleryItem!.id!,
          child: galleryItem!.resource != null
              ? FadeInImage.assetNetwork(
                  height: 80,
                  image: galleryItem!.resource!,
                  placeholder: ImagePaths.imgPlaceholder,
                  placeholderScale: 0.5,
                )
              : Image.file(galleryItem!.assetImage!),
        ),
      ),
    );
  }
}
