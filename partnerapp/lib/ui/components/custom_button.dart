import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

/*
 big button 
 default - full width, green background
*/
class SubmitButton extends StatelessWidget {
  const SubmitButton({
    super.key,
    required this.onPressed,
    required this.text,
    this.enabled = true,
    this.height,
    this.backgroundColor,
    this.textColor,
    this.textSize,
    this.fontWeight,
    this.hasIcon = false,
  });

  final GestureTapCallback? onPressed;
  final Color? backgroundColor;
  final Color? textColor;
  final String text;
  final double? textSize;
  final double? height;
  final FontWeight? fontWeight;
  final bool hasIcon;
  final bool? enabled;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: enabled!
          ? backgroundColor ?? Theme.of(context).colorScheme.secondary
          : Colors.grey,
      child: InkWell(
        splashColor: Colors.blue,
        onTap: enabled! ? onPressed : null,
        child: Container(
          height: height ?? 80,
          width: double.infinity,
          alignment: Alignment.center,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.fade,
                  style: TextStyle(
                    fontSize: textSize ?? 24,
                    fontWeight: fontWeight ?? FontWeight.bold,
                    color: textColor ?? Colors.white,
                    height: 1,
                  ),
                ),
              ),
              if (hasIcon) const Icon(Icons.arrow_forward, color: Colors.white),
            ],
          ),
        ),
      ),
    );
  }
}

/*
Toggle Button for showing more or less
*/
class ShowHideButton extends StatelessWidget {
  const ShowHideButton({super.key, this.show, this.onPressed});
  final bool? show;
  final GestureTapCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Center(
          child: InkWell(
              onTap: onPressed,
              child: Column(
                children: <Widget>[
                  Container(
                      padding: const EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.grey, width: 0.5)),
                      child: Icon(
                          !show! ? Icons.arrow_downward : Icons.arrow_upward,
                          color: Theme.of(context).colorScheme.secondary)),
                  Text(tr(!show! ? 'read_more' : 'read_less'),
                      style: Theme.of(context).textTheme.titleMedium!.apply(
                          color: Theme.of(context).colorScheme.secondary)),
                ],
              ))),
    );
  }
}
