import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/packages/packages_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:provider/provider.dart';
import 'package:tap_debouncer/tap_debouncer.dart';
import 'package:url_launcher/url_launcher.dart';

Future<void> showFreemiumDialog(BuildContext context) async => showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext ctx) => const AlertDialog(
        contentPadding: EdgeInsets.all(0),
        content: FreemiumDialog(),
      ),
    );

class FreemiumDialog extends StatefulWidget {
  const FreemiumDialog({super.key});

  @override
  FreemiumDialogState createState() => FreemiumDialogState();
}

class FreemiumDialogState extends State<FreemiumDialog> {
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20))),
            padding: const EdgeInsets.all(10),
            height: 250,
            width: MediaQuery.of(context).size.width * 0.80,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SmartGaps.gapH25,
                Builder(
                  builder: (_) {
                    final userVm = context.read<UserViewModel>();

                    final companyName =
                        unescapedValue(userVm.userModel?.user?.name ?? '');
                    return Text("${tr('hey')}  $companyName",
                        style: Theme.of(context).textTheme.headlineMedium);
                  },
                ),
                SmartGaps.gapH20,
                Text(
                  tr('you_must_be_a_paying_member'),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headlineSmall,
                ),
                SmartGaps.gapH40,
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TapDebouncer(
                      cooldown: const Duration(milliseconds: 200),
                      onTap: () async {
                        final navigator = Navigator.of(context);

                        await launchUrl(Uri.parse(
                                context.read<PackagesViewModel>().bookingUrl))
                            .whenComplete(() {
                          navigator.pop();
                          if (navigator.canPop()) {
                            navigator.pop();
                          }
                        });
                      },
                      builder: (context, onTap) {
                        return CustomDesignTheme.flatButtonStyle(
                          onPressed: onTap,
                          backgroundColor:
                              Theme.of(context).colorScheme.onSurfaceVariant,
                          child: Text(
                            tr('upgrade_your_membership'),
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium!
                                .copyWith(color: Colors.white),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ],
            )),
        Positioned(
          right: 0,
          top: 0,
          child: Opacity(
            opacity: 0.7,
            child: Container(
              padding: const EdgeInsets.all(5),
              decoration: const BoxDecoration(
                color: Colors.black,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black45,
                    blurRadius: 2.0,
                    spreadRadius: 0.0,
                    offset: Offset(2.0, 2.0),
                  ),
                ],
              ),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                  if (Navigator.of(context).canPop()) {
                    Navigator.of(context).pop();
                  }
                  // if (Navigator.of(myGlobals.drawerRouteKey.currentContext).canPop()) Navigator.of(myGlobals.drawerRouteKey.currentContext).pop();
                },
                child: Icon(Icons.close, color: Colors.grey[200]),
              ),
            ),
          ),
        )
      ],
    );
  }

  String unescapedValue(String value) {
    var unescape = HtmlUnescape();
    return unescape.convert(value);
  }
}
