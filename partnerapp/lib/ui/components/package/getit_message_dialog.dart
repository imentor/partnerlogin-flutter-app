import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

Future<void> showGetItMessageDialog(
  BuildContext context, {
  bool popTwice = true,
}) async {
  await showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) => AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      content: _GetItMessageDialog(popTwice: popTwice),
    ),
  );
}

class _GetItMessageDialog extends StatelessWidget {
  const _GetItMessageDialog({required this.popTwice});

  final bool popTwice;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20))),
      padding: const EdgeInsets.symmetric(vertical: 20),
      height: 250,
      width: MediaQuery.of(context).size.width * 0.80,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Icon(ElementIcons.document_checked,
              color: PartnerAppColors.malachite, size: 40),
          const Spacer(),
          Text(
            tr('thank_you_for_inquiry'),
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headlineSmall,
          ),
          const Spacer(),
          Text(
            tr('contact_you_soon'),
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.titleSmall,
          ),
          const Spacer(),
          CustomDesignTheme.flatButtonStyle(
            onPressed: () {
              Navigator.of(context).pop();
              if (Navigator.of(context).canPop()) Navigator.of(context).pop();
              if (Navigator.of(
                          myGlobals.drawerRouteKey?.currentContext ?? context)
                      .canPop() &&
                  popTwice) {
                Navigator.of(
                        myGlobals.drawerRouteKey?.currentContext ?? context)
                    .pop();
              }
            },
            backgroundColor: Theme.of(context).colorScheme.onSurfaceVariant,
            child: Text(
              tr('close'),
              style: Theme.of(context)
                  .textTheme
                  .titleMedium!
                  .copyWith(color: Colors.white),
            ),
          ),
        ],
      ),
      // Positioned(
      //   right: 0,
      //   top: 0,
      //   child: Opacity(
      //     opacity: 0.7,
      //     child: Container(
      //         padding: EdgeInsets.all(5),
      //         decoration: BoxDecoration(
      //           color: Colors.black,
      //           shape: BoxShape.circle,
      //           boxShadow: [
      //             BoxShadow(
      //               color: Colors.black45,
      //               blurRadius: 2.0,
      //               spreadRadius: 0.0,
      //               offset: Offset(2.0, 2.0),
      //             ),
      //           ],
      //         ),
      //         child: InkWell(
      //             onTap: () {
      //               Navigator.of(context).pop();
      //               if (Navigator.of(context).canPop()) {
      //                 Navigator.of(context).pop();
      //               }
      //               if (Navigator.of(myGlobals.drawerRouteKey.currentContext)
      //                   .canPop()) {
      //                 Navigator.of(myGlobals.drawerRouteKey.currentContext)
      //                     .pop();
      //               }
      //             },
      //             child: Icon(
      //               Icons.close,
      //               color: Colors.grey[200],
      //             ))),
      //   ),
      // )
      // ],
      // ),
    );
  }
}
