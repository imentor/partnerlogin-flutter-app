import 'package:Haandvaerker.dk/services/packages/packages_service.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/components/package/getit_message_dialog.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
// import 'package:Haandvaerker.dk/ui/components/package/getit_message_dialog.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/viewmodel/packages/packages_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:provider/provider.dart';
import 'package:tap_debouncer/tap_debouncer.dart';

Future<void> showUnAvailableDialog(
  BuildContext context,
  FeatureStatus feature, {
  bool? popTwice,
  bool? backDrawerRouteInGetIt,
}) async {
  bool popRouteTwice = popTwice ?? true;
  bool backDrawerRouteInGetIt0 = backDrawerRouteInGetIt ?? false;
  await showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) => AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      content: PackageNotAvailableDialog(
          feature: feature,
          popTwice: popRouteTwice,
          backDrawerRouteInGetIt: backDrawerRouteInGetIt0),
    ),
  );
}

class PackageNotAvailableDialog extends StatefulWidget {
  const PackageNotAvailableDialog(
      {super.key,
      required this.feature,
      this.popTwice = true,
      this.backDrawerRouteInGetIt = false});

  final FeatureStatus feature;
  final bool popTwice;
  final bool backDrawerRouteInGetIt;

  @override
  PackageNotAvailableDialogState createState() =>
      PackageNotAvailableDialogState();
}

class PackageNotAvailableDialogState extends State<PackageNotAvailableDialog> {
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 18.0),
          height: 350,
          width: MediaQuery.of(context).size.width * 0.80,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SmartGaps.gapH25,
              Builder(
                builder: (_) {
                  final userVm = context.read<UserViewModel>();
                  return Text(
                    "${tr('hey')}  ${userVm.userModel?.user?.name ?? ''}",
                    style: Theme.of(context).textTheme.headlineMedium,
                  );
                },
              ),
              SmartGaps.gapH20,
              Text(
                tr('feature_unavailable'),
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              SmartGaps.gapH45,
              Text(
                tr('feature_purchase_message'),
                textAlign: TextAlign.center,
              ),
              const Spacer(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TapDebouncer(
                    cooldown: const Duration(milliseconds: 200),
                    onTap: () async {
                      Navigator.of(context).pop();
                      if (widget.popTwice) {
                        if (Navigator.of(context).canPop()) {
                          Navigator.of(context).pop();
                        }
                        if (Navigator.of(
                                myGlobals.drawerRouteKey!.currentContext!)
                            .canPop()) {
                          Navigator.of(
                                  myGlobals.drawerRouteKey!.currentContext!)
                              .pop();
                        }
                      }
                    },
                    builder: (context, onTap) {
                      return CustomDesignTheme.flatButtonStyle(
                        onPressed: onTap,
                        backgroundColor:
                            Theme.of(context).colorScheme.onSurfaceVariant,
                        child: Text(
                          tr('no_thanks'),
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium!
                              .copyWith(color: Colors.white),
                        ),
                      );
                    },
                  ),
                  const Spacer(),
                  TapDebouncer(
                    cooldown: const Duration(milliseconds: 200),
                    onTap: () => onUpgrade(),
                    builder: (context, onTap) {
                      return CustomDesignTheme.flatButtonStyle(
                        onPressed: onTap,
                        backgroundColor:
                            Theme.of(context).colorScheme.onSurfaceVariant,
                        child: Text(
                          tr('upgrade'),
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium!
                              .copyWith(color: Colors.white),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
        Positioned(
          right: 0,
          top: 0,
          child: Opacity(
            opacity: 0.7,
            child: Container(
              padding: const EdgeInsets.all(5),
              decoration: const BoxDecoration(
                color: Colors.black,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black45,
                    blurRadius: 2.0,
                    spreadRadius: 0.0,
                    offset: Offset(2.0, 2.0),
                  ),
                ],
              ),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                  if (Navigator.of(context).canPop()) {
                    Navigator.of(context).pop();
                  }
                  if (Navigator.of(myGlobals.drawerRouteKey!.currentContext!)
                          .canPop() &&
                      widget.popTwice) {
                    Navigator.of(myGlobals.drawerRouteKey!.currentContext!)
                        .pop();
                  }
                },
                child: Icon(
                  Icons.close,
                  color: Colors.grey[200],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Future<void> onUpgrade() async {
    final packageService = context.read<PackagesService>();
    final packageVm = context.read<PackagesViewModel>();

    String packageHandle = packageVm.packageHandle;

    // Navigator.of(context).pop();

    if (packageHandle.isEmpty) {
      final packages = await packageService.getPackages();

      if (packages != null) {
        packageHandle = packages.data?.packageHandle ?? '';
      }
    }

    modalManager.showLoadingModal();

    await packageService
        .requestFeature(
      shortCode: widget.feature.feature.shortCode!,
      packageHandle: packageHandle,
    )
        .then((value) async {
      modalManager.hideLoadingModal();

      if (!mounted) return;

      if (value.success ?? true) {
        await showGetItMessageDialog(context,
            popTwice: widget.backDrawerRouteInGetIt);
      }
    });
  }

  String unescapedValue(String value) {
    var unescape = HtmlUnescape();
    return unescape.convert(value);
  }
}
