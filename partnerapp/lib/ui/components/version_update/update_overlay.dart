import 'dart:async';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/viewmodel/version_checker/version_checker_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_exit_app/flutter_exit_app.dart';
import 'package:provider/provider.dart';

void showUpdateOverlay(BuildContext context) {
  OverlayState overlayState = Overlay.of(context);
  OverlayEntry overlayEntry = OverlayEntry(
    builder: (context) => const UpdateOverlay(),
  );
  overlayState.insert(overlayEntry);
}

class UpdateOverlay extends StatefulWidget {
  const UpdateOverlay({super.key});

  @override
  UpdateOverlayState createState() => UpdateOverlayState();
}

class UpdateOverlayState extends State<UpdateOverlay> {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.black.withValues(alpha: 0.5),
      child: Center(
        child: Container(
          margin: const EdgeInsets.all(20),
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                tr('update_available'),
                style: Theme.of(context).textTheme.headlineLarge,
              ),
              const SizedBox(height: 20),
              Text(
                tr('update_app_to_continue'),
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .headlineSmall!
                    .copyWith(fontWeight: FontWeight.normal),
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    fixedSize: const Size(130, 50),
                    elevation: 0,
                    backgroundColor: PartnerAppColors.malachite),
                onPressed: () async {
                  context
                      .read<VersionCheckerViewmodel>()
                      .appUpgrader
                      .sendUserToAppStore();
                  Timer(const Duration(seconds: 1), () {
                    FlutterExitApp.exitApp();
                  });
                },
                child: Text(tr('update_now'),
                    style: Theme.of(context).textTheme.titleSmall!.copyWith(
                        fontWeight: FontWeight.bold, color: Colors.white)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
