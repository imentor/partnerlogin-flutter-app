import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:url_launcher/url_launcher.dart';

Future<void> termsDialog(
    {required BuildContext context,
    required String? link,
    required String? termsHtml}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.of(dialogContext).pop();
              },
              child: const Icon(Icons.close),
            )
          ],
        ),
        content: SingleChildScrollView(
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                if (link != null)
                  SizedBox(
                    height: 500,
                    width: MediaQuery.of(context).size.width,
                    child: InAppWebView(
                      initialUrlRequest: URLRequest(url: WebUri(link)),
                    ),
                  )
                else
                  SizedBox(
                    child: Html(
                      data: termsHtml,
                      onLinkTap: (url, attributes, element) {
                        launchUrl(Uri.parse(url!));
                      },
                    ),
                  ),
              ],
            ),
          ),
        ),
      );
    },
  );
}
