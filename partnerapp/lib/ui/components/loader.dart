import 'dart:io';

import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/app/connectivity_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

Widget loader({String? message}) {
  if (message != null && message.isNotEmpty) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _load(),
        Text(message,
            style: const TextStyle(
                fontSize: 16,
                color: Color.fromRGBO(0, 54, 69, 1),
                fontWeight: FontWeight.w700,
                height: 1.618))
      ],
    );
  }
  return _load();
}

Widget _load() {
  if (Platform.isIOS) {
    return const CupertinoActivityIndicator();
  }
  return const CircularProgressIndicator(strokeWidth: 2);
}

class ConnectivityLoader extends StatelessWidget {
  const ConnectivityLoader({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<ConnectivityViewModel>(builder: (_, vm, __) {
      switch (vm.connectionStatus) {
        case 'wifi':
          return loader();
        case 'mobile':
          return loader();
        case 'Unknown':
          return loader();
        case 'none':
          return const ConnectionIssuesSign();
        default:
          return const ConnectionIssuesSign();
      }
    });
  }
}

class ConnectivityLoaderWidgetWrapper extends StatelessWidget {
  const ConnectivityLoaderWidgetWrapper({super.key, required this.child});

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Consumer<ConnectivityViewModel>(builder: (_, vm, __) {
      switch (vm.connectionStatus) {
        case 'wifi':
          return child;
        case 'mobile':
          return child;
        case 'Unknown':
          return loader();
        case 'none':
          return const ConnectionIssuesSign();
        default:
          return const ConnectionIssuesSign();
      }
    });
  }
}

class ConnectionIssuesSign extends StatelessWidget {
  const ConnectionIssuesSign({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          Icons.wifi_off,
          color: Colors.grey[500],
          size: 64,
        ),
        SmartGaps.gapH10,
        Text(
          tr('connection_issues'),
          style: Theme.of(context)
              .textTheme
              .titleSmall!
              .copyWith(color: Colors.grey[500]),
        )
      ],
    );
  }
}
