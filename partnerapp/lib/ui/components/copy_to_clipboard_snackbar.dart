import 'package:Haandvaerker.dk/utils/extension/snackbar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

Future<void> copyToClipboard({
  required String copyString,
  required BuildContext context,
  String? displayStatus,
  Icon? displayIcon,
}) async {
  final BuildContext currentContext = context;

  await Clipboard.setData(ClipboardData(text: copyString));

  if (currentContext.mounted) {
    currentContext.showCustomSnackBar(
      SnackBar(
        content: Row(
          children: [
            displayIcon ??
                const Icon(
                  Icons.copy,
                  color: Colors.white,
                ),
            SmartGaps.gapW10,
            Expanded(
              child: Text(
                displayStatus ?? '',
                maxLines: 2,
                style: const TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
