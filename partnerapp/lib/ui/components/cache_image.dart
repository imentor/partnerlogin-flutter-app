import 'package:Haandvaerker.dk/utils/extensions.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CacheImage extends StatefulWidget {
  const CacheImage({
    super.key,
    required this.imageUrl,
    this.height,
    this.width,
    this.fit,
    this.errorIcon,
  });

  final String? imageUrl;
  final double? height;
  final double? width;
  final Widget? errorIcon;
  final BoxFit? fit;

  @override
  CacheImageState createState() => CacheImageState();
}

class CacheImageState extends State<CacheImage> {
  @override
  Widget build(BuildContext context) {
    if (widget.imageUrl == null || widget.imageUrl!.isEmpty) {
      return Container(
        height: widget.height,
        width: widget.width,
        decoration: const BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fill,
            image: AssetImage("assets/images/default_image.jpeg"),
          ),
        ),
      );
    }

    final modifiedImageUrl =
        widget.imageUrl!.replaceAll('håndværker', 'haandvaerker');

    return CachedNetworkImage(
      imageUrl: modifiedImageUrl,
      imageBuilder: (_, imageProvider) {
        return Container(
          height: widget.height,
          width: widget.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: imageProvider,
              fit: widget.fit ?? BoxFit.cover,
            ),
          ),
        );
      },
      errorWidget: (_, __, error) {
        return Container(
          height: widget.height,
          width: widget.width,
          decoration: const BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage("assets/images/default_image.jpeg"),
            ),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    if (widget.imageUrl != null) {
      if (CachedNetworkImageProvider(widget.imageUrl!).isNotNull) {
        CachedNetworkImageProvider(widget.imageUrl!).evict();
      }
    }
  }
}
