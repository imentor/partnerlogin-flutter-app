import 'dart:async';
import 'dart:io';

import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/pdf_viewer.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:path_provider/path_provider.dart';
import 'package:photo_view/photo_view.dart';

Future<void> showUrlViewer(
  BuildContext context,
  String? path, {
  bool isImg = false,
  File? file,
  bool rightClose = false,
  bool isPdf = false,
  String? pdfUrl,
}) async {
  final loadingKey = GlobalKey();
  showLoadingDialog(context, loadingKey);
  final pdfPath = await _getPdfFileFromUrl(pdfUrl ?? '').then((f) => f.path);
  Navigator.of(loadingKey.currentContext!, rootNavigator: true).pop();

  //
  if (context.mounted) {
    return await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => isPdf
          ? PdfViewer(pdfPath: pdfPath)
          : FullUrlViewer(
              url: path,
              isImg: isImg,
              file: file,
              rightClose: rightClose,
              isPdf: isPdf,
              pdfUrl: pdfUrl,
            ),
    );
  }
}

Future<File> _getPdfFileFromUrl(String url, [String? pdfFileName]) async {
  Completer<File> completer = Completer();

  try {
    final fileName = pdfFileName ?? url.substring(url.lastIndexOf("/") + 1);
    var request = await HttpClient().getUrl(Uri.parse(url));
    var response = await request.close();
    var bytes = await consolidateHttpClientResponseBytes(response);
    var dir = await getApplicationDocumentsDirectory();
    File file = File("${dir.path}/$fileName");

    await file.writeAsBytes(bytes, flush: true);
    completer.complete(file);
  } catch (e) {
    throw Exception('Error parsing asset file! $e');
  }

  return completer.future;
}

class FullUrlViewer extends StatefulWidget {
  const FullUrlViewer({
    super.key,
    required this.url,
    required this.isImg,
    required this.file,
    this.isPdf = false,
    this.pdfUrl,
    this.rightClose = false,
  });

  final String? url;
  final bool isImg;
  final File? file;
  final bool rightClose;
  final bool isPdf;
  final String? pdfUrl;

  @override
  FullUrlViewerState createState() => FullUrlViewerState();
}

class FullUrlViewerState extends State<FullUrlViewer> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: widget.url == null || widget.url!.isEmpty
                ? Text(tr('cannot_load_page'))
                : widget.isImg
                    ? ImgViewer(
                        img: widget.url,
                        imgFile: widget.file,
                      )
                    : ConnectivityLoaderWidgetWrapper(
                        child: InAppWebView(
                          initialUrlRequest: URLRequest(
                              url: widget.url != null
                                  ? WebUri(widget.url!)
                                  : null),
                        ),
                      ),
          ),
          widget.rightClose
              ? Positioned(
                  right: 10,
                  top: 10,
                  child: Opacity(
                    opacity: 0.7,
                    child: Container(
                      padding: const EdgeInsets.all(5),
                      decoration: const BoxDecoration(
                        color: Colors.black,
                        shape: BoxShape.circle,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black45,
                            blurRadius: 2.0,
                            spreadRadius: 0.0,
                            offset: Offset(2.0, 2.0),
                          ),
                        ],
                      ),
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context, rootNavigator: true).pop();
                        },
                        child: Icon(
                          Icons.close,
                          color: Colors.grey[200],
                        ),
                      ),
                    ),
                  ),
                )
              : Positioned(
                  left: 10,
                  top: 10,
                  child: Opacity(
                    opacity: 0.7,
                    child: Container(
                      padding: const EdgeInsets.all(5),
                      decoration: const BoxDecoration(
                        color: Colors.black,
                        shape: BoxShape.circle,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black45,
                            blurRadius: 2.0,
                            spreadRadius: 0.0,
                            offset: Offset(2.0, 2.0),
                          ),
                        ],
                      ),
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context, rootNavigator: true).pop();
                        },
                        child: Icon(
                          Icons.close,
                          color: Colors.grey[200],
                        ),
                      ),
                    ),
                  ),
                )
        ],
      ),
    );
  }
}

class ImgViewer extends StatefulWidget {
  const ImgViewer({super.key, this.img, this.imgFile});

  final String? img;
  final File? imgFile;

  @override
  FullUrlViewerZoomState createState() => FullUrlViewerZoomState();
}

class FullUrlViewerZoomState extends State<ImgViewer> {
  @override
  Widget build(BuildContext context) {
    return PhotoView(
      imageProvider: FileImage(widget.imgFile!),
    );
  }
}
