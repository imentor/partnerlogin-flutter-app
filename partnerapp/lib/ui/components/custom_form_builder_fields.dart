import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';

class CustomTextFieldFormBuilder extends StatelessWidget {
  const CustomTextFieldFormBuilder(
      {super.key,
      required this.name,
      this.validator,
      this.labelText,
      this.hintText,
      this.keyboardType,
      this.maxLines,
      this.minLines,
      this.isRequired,
      this.suffixIcon,
      this.initialValue,
      this.enabled = true,
      this.prefixIcon,
      this.onChanged,
      this.inputFormatters,
      this.readOnly = false,
      this.textAlign = TextAlign.start,
      this.tooltip,
      this.obscureText = false,
      this.prefixTextDecoration,
      this.textColor = PartnerAppColors.darkBlue});

  final String name;
  final bool? isRequired;
  final String? Function(String?)? validator;
  final String? labelText;
  final String? hintText;
  final TextInputType? keyboardType;
  final int? minLines;
  final int? maxLines;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final Widget? prefixTextDecoration;
  final String? initialValue;
  final bool enabled;
  final void Function(String?)? onChanged;
  final List<TextInputFormatter>? inputFormatters;
  final bool readOnly;
  final TextAlign textAlign;
  final Widget? tooltip;
  final bool obscureText;
  final Color textColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (labelText != null) ...[
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: labelText,
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        letterSpacing: 1.0,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                        fontSize: 18),
                  ),
                  if (isRequired != null && isRequired!)
                    TextSpan(
                      text: '*',
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(letterSpacing: 1.0, color: Colors.red),
                    ),
                  WidgetSpan(
                      alignment: PlaceholderAlignment.middle,
                      child: tooltip ?? const SizedBox.shrink()),
                ],
              ),
            ),
            SmartGaps.gapH10,
          ],
          Theme(
            data: Theme.of(context).copyWith(
              textSelectionTheme: const TextSelectionThemeData(
                  selectionColor: Colors.transparent),
            ),
            child: FormBuilderTextField(
              key: key,
              name: name,
              validator: validator,
              keyboardType: keyboardType,
              maxLines: maxLines,
              minLines: minLines,
              initialValue: initialValue,
              enabled: enabled,
              onChanged: onChanged,
              inputFormatters: inputFormatters,
              readOnly: readOnly,
              textAlign: textAlign,
              obscureText: obscureText,
              textInputAction:
                  (minLines ?? 1) > 1 ? null : TextInputAction.done,
              style: Theme.of(context)
                  .textTheme
                  .titleMedium!
                  .copyWith(color: textColor),
              decoration: InputDecoration(
                  hintText: hintText,
                  filled: true,
                  isDense: true,
                  errorMaxLines: 3,
                  prefix: prefixTextDecoration,
                  prefixIconConstraints: const BoxConstraints(),
                  fillColor: Colors.white,
                  hintStyle: Theme.of(context)
                      .textTheme
                      .headlineSmall!
                      .copyWith(
                          fontWeight: FontWeight.normal,
                          color: Colors.grey.shade600),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: PartnerAppColors.darkBlue.withValues(alpha: .4)),
                    borderRadius: const BorderRadius.all(Radius.circular(3)),
                  ),
                  disabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: PartnerAppColors.darkBlue.withValues(alpha: .4),
                        width: 0.8),
                    borderRadius: const BorderRadius.all(Radius.circular(3)),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: PartnerAppColors.darkBlue.withValues(alpha: .4)),
                    borderRadius: const BorderRadius.all(Radius.circular(3)),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context).colorScheme.error, width: 0.8),
                    borderRadius: const BorderRadius.all(Radius.circular(3)),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: PartnerAppColors.darkBlue.withValues(alpha: .4),
                        width: 0.8),
                    borderRadius: const BorderRadius.all(Radius.circular(3)),
                  ),
                  suffixIcon: suffixIcon,
                  prefixIcon: prefixIcon),
            ),
          ),
        ],
      ),
    );
  }
}

class CustomDropdownFormBuilder extends StatelessWidget {
  const CustomDropdownFormBuilder({
    super.key,
    this.hintText,
    required this.items,
    this.labelText,
    required this.name,
    this.validator,
    this.initialValue,
    this.onChanged,
    this.isRequired,
    this.tooltip,
  });

  final String name;
  final String? Function(String?)? validator;
  final String? labelText;
  final String? hintText;
  final String? initialValue;
  final List<DropdownMenuItem<String>> items;
  final void Function(String?)? onChanged;
  final bool? isRequired;
  final Widget? tooltip;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (labelText != null) ...[
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: labelText,
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        letterSpacing: 1.0,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                        fontSize: 18),
                  ),
                  if (isRequired != null && isRequired!)
                    TextSpan(
                      text: '*',
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(letterSpacing: 1.0, color: Colors.red),
                    ),
                  WidgetSpan(
                      alignment: PlaceholderAlignment.middle,
                      child: tooltip ?? const SizedBox.shrink()),
                ],
              ),
            ),
            SmartGaps.gapH10,
          ],
          FormBuilderDropdown<String>(
            name: name,
            validator: validator,
            items: items,
            initialValue: initialValue,
            decoration: InputDecoration(
              hintText: hintText,
              border: OutlineInputBorder(
                borderSide: BorderSide(
                  color: PartnerAppColors.darkBlue.withValues(alpha: .4),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: PartnerAppColors.darkBlue.withValues(alpha: .4),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: PartnerAppColors.darkBlue.withValues(alpha: .4),
                ),
              ),
            ),
            alignment: Alignment.centerLeft,
            icon: const Icon(Icons.expand_more),
            focusColor: Colors.white,
            onChanged: onChanged,
          ),
        ],
      ),
    );
  }
}

class CustomRadioGroupFormBuilder extends StatelessWidget {
  const CustomRadioGroupFormBuilder({
    super.key,
    this.hintText,
    required this.options,
    this.labelText,
    required this.name,
    this.validator,
    this.initialValue,
    this.onChanged,
    this.isRequired,
    this.optionsOrientation,
    this.separator,
    this.tooltip,
    this.itemDecoration,
    this.controlAffinity = ControlAffinity.leading,
    this.wrapSpacing = 0.0,
  });

  final String name;
  final bool? isRequired;
  final String? initialValue;
  final String? Function(String?)? validator;
  final String? labelText;
  final String? hintText;
  final List<FormBuilderFieldOption<String>> options;
  final void Function(String?)? onChanged;
  final OptionsOrientation? optionsOrientation;
  final Widget? separator;
  final Widget? tooltip;
  final BoxDecoration? itemDecoration;
  final ControlAffinity controlAffinity;
  final double wrapSpacing;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (labelText != null) ...[
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: labelText,
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 18),
                ),
                if (isRequired != null && isRequired!)
                  TextSpan(
                    text: '*',
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium!
                        .copyWith(letterSpacing: 1.0, color: Colors.red),
                  ),
                WidgetSpan(
                    alignment: PlaceholderAlignment.middle,
                    child: tooltip ?? const SizedBox.shrink()),
              ],
            ),
          ),
          SmartGaps.gapH10,
        ],
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: FormBuilderRadioGroup<String>(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            name: name,
            initialValue: initialValue,
            validator: validator,
            options: options,
            activeColor: PartnerAppColors.blue,
            onChanged: onChanged,
            decoration: const InputDecoration(
                border: InputBorder.none, contentPadding: EdgeInsets.zero),
            orientation: optionsOrientation ?? OptionsOrientation.wrap,
            separator: separator,
            itemDecoration: itemDecoration,
            controlAffinity: controlAffinity,
            wrapSpacing: wrapSpacing,
          ),
        ),
      ],
    );
  }
}

class CustomCheckboxGroupFormBuilder extends StatelessWidget {
  const CustomCheckboxGroupFormBuilder({
    super.key,
    this.hintText,
    required this.options,
    this.labelText,
    required this.name,
    this.validator,
    this.initialValue,
    this.onSaved,
    this.isRequired,
    this.optionsOrientation,
    this.separator,
    this.onChanged,
    this.tooltip,
    this.enable = true,
  });

  final String name;
  final bool? isRequired;
  final List<String>? initialValue;
  final String? Function(List<String>?)? validator;
  final String? labelText;
  final String? hintText;
  final List<FormBuilderFieldOption<String>> options;
  final void Function(List<String>?)? onSaved;
  final OptionsOrientation? optionsOrientation;
  final Widget? separator;
  final void Function(List<String>?)? onChanged;
  final Widget? tooltip;
  final bool enable;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (labelText != null) ...[
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: labelText,
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 18),
                ),
                if (isRequired != null && isRequired!)
                  TextSpan(
                    text: '*',
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium!
                        .copyWith(letterSpacing: 1.0, color: Colors.red),
                  ),
                WidgetSpan(
                    alignment: PlaceholderAlignment.middle,
                    child: tooltip ?? const SizedBox.shrink()),
              ],
            ),
          ),
          SmartGaps.gapH10,
        ],
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: FormBuilderCheckboxGroup<String>(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            name: name,
            validator: validator,
            options: options,
            activeColor: PartnerAppColors.blue,
            onSaved: onSaved,
            onChanged: onChanged,
            decoration: const InputDecoration(
                border: InputBorder.none, contentPadding: EdgeInsets.zero),
            orientation: optionsOrientation ?? OptionsOrientation.wrap,
            separator: separator,
            initialValue: initialValue,
            enabled: enable,
          ),
        ),
      ],
    );
  }
}

class CustomSliderFormBuilder extends StatelessWidget {
  const CustomSliderFormBuilder({
    super.key,
    this.hintText,
    this.labelText,
    required this.name,
    this.validator,
    required this.initialValue,
    this.onChanged,
    this.isRequired,
    required this.max,
    required this.min,
    this.suffexIcon,
    this.divisions,
    this.displayValues = DisplayValues.none,
    this.onChangeEnd,
  });

  final String name;
  final bool? isRequired;
  final String? Function(double?)? validator;
  final String? labelText;
  final String? hintText;
  final void Function(double?)? onChanged;
  final void Function(double)? onChangeEnd;
  final double initialValue;
  final double min;
  final double max;
  final Widget? suffexIcon;
  final int? divisions;
  final DisplayValues displayValues;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (labelText != null) ...[
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: labelText,
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 18),
                ),
                if (isRequired != null && isRequired!)
                  TextSpan(
                    text: '*',
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium!
                        .copyWith(letterSpacing: 1.0, color: Colors.red),
                  ),
              ],
            ),
          ),
          SmartGaps.gapH10,
        ],
        SizedBox(
          // margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            children: [
              Expanded(
                child: FormBuilderSlider(
                  name: name,
                  initialValue: initialValue,
                  max: max,
                  min: min,
                  validator: validator,
                  divisions: divisions,
                  activeColor: PartnerAppColors.blue,
                  inactiveColor:
                      PartnerAppColors.spanishGrey.withValues(alpha: .2),
                  onChanged: onChanged, onChangeEnd: onChangeEnd,
                  decoration: const InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.zero,
                  ),
                  // maxTextStyle: context.textTheme.displaySmall!
                  //     .copyWith(fontWeight: FontWeight.normal, fontSize: 14),
                  // minTextStyle: context.textTheme.displaySmall!
                  //     .copyWith(fontWeight: FontWeight.normal, fontSize: 14),
                  displayValues: displayValues,
                ),
              ),
              suffexIcon ?? const SizedBox.shrink()
            ],
          ),
        ),
      ],
    );
  }
}

class CustomRatingFormBuilder extends StatelessWidget {
  const CustomRatingFormBuilder({
    super.key,
    this.hintText,
    this.labelText,
    required this.name,
    this.validator,
    required this.initialRating,
    this.onChanged,
    this.isRequired,
  });

  final String name;
  final bool? isRequired;
  final String? Function(double?)? validator;
  final String? labelText;
  final String? hintText;
  final void Function(double?)? onChanged;
  final double initialRating;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (labelText != null) ...[
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: labelText,
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 18),
                ),
                if (isRequired != null && isRequired!)
                  TextSpan(
                    text: '*',
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium!
                        .copyWith(letterSpacing: 1.0, color: Colors.red),
                  ),
              ],
            ),
          ),
          SmartGaps.gapH30,
        ],
        SizedBox(
            // margin: const EdgeInsets.symmetric(vertical: 10),
            child: FormBuilderField<double?>(
          name: name,
          validator: validator,
          builder: (FormFieldState field) {
            return InputDecorator(
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.zero,
                errorText: field.errorText,
              ),
              child: RatingBar.builder(
                minRating: 1,
                maxRating: 10,
                glow: false,
                itemCount: 10,
                initialRating: initialRating,
                onRatingUpdate: (double value) => field.didChange(value),
                itemBuilder: (context, index) => Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Icon(
                      Icons.star,
                      color: PartnerAppColors.malachite,
                      size: 30,
                    ),
                    // Container(
                    //   width: 20,
                    //   height: 20,
                    //   decoration: BoxDecoration(
                    //     border: Border.all(color: PartnerAppColors.malachite),
                    //     borderRadius: BorderRadius.circular(10),
                    //   ),
                    //   child: const Center(
                    //       child: Icon(
                    //     Icons.remove,
                    //     size: 14,
                    //   )),
                    // ),
                    SmartGaps.gapH2,
                    Builder(builder: (context) {
                      return Center(
                        child: Text(
                          (index + 1).toString(),
                          style: Theme.of(context)
                              .textTheme
                              .bodyMedium!
                              .copyWith(
                                  fontSize: 12,
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.bold),
                        ),
                      );
                    }),
                  ],
                ),
                itemSize: 38,
              ),
            );
          },
        )),
      ],
    );
  }
}

class CustomDatePickerFormBuilder extends StatelessWidget {
  const CustomDatePickerFormBuilder({
    super.key,
    required this.name,
    this.validator,
    this.labelText,
    this.hintText,
    this.keyboardType,
    this.maxLines,
    this.minLines,
    this.isRequired,
    this.suffixIcon,
    this.initialDate,
    this.enabled = true,
    this.prefixIcon,
    this.inputType = InputType.date,
    this.dateFormat,
    this.initialValue,
    this.onChanged,
  });

  final String name;
  final bool? isRequired;
  final String? Function(DateTime?)? validator;
  final String? labelText;
  final String? hintText;
  final TextInputType? keyboardType;
  final int? minLines;
  final int? maxLines;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final DateTime? initialDate;
  final DateFormat? dateFormat;
  final InputType inputType;
  final bool enabled;
  final DateTime? initialValue;
  final void Function(DateTime?)? onChanged;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (labelText != null) ...[
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: labelText,
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        letterSpacing: 1.0,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                        fontSize: 18),
                  ),
                  if (isRequired != null && isRequired!)
                    TextSpan(
                      text: '*',
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(letterSpacing: 1.0, color: Colors.red),
                    ),
                ],
              ),
            ),
            SmartGaps.gapH10,
          ],
          Theme(
            data: Theme.of(context).copyWith(
              colorScheme: const ColorScheme.light(
                primary: PartnerAppColors.blue,
                onSurface: PartnerAppColors.darkBlue,
              ),
            ),
            child: FormBuilderDateTimePicker(
              name: name,
              maxLines: maxLines,
              minLines: minLines,
              initialDate: initialDate,
              initialValue: initialValue,
              enabled: enabled,
              initialEntryMode: DatePickerEntryMode.calendarOnly,
              timePickerInitialEntryMode: TimePickerEntryMode.input,
              inputType: inputType,
              format: dateFormat,
              validator: validator,
              firstDate: DateTime.now(),
              onChanged: onChanged,
              decoration: InputDecoration(
                  hintText: hintText,
                  filled: true,
                  isDense: true,
                  prefixIconConstraints: const BoxConstraints(),
                  fillColor: Colors.white,
                  hintStyle: Theme.of(context)
                      .textTheme
                      .headlineSmall!
                      .copyWith(
                          fontWeight: FontWeight.normal,
                          color: Colors.grey.shade600),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: PartnerAppColors.darkBlue.withValues(alpha: .4)),
                    borderRadius: const BorderRadius.all(Radius.circular(3)),
                  ),
                  disabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: PartnerAppColors.darkBlue.withValues(alpha: .4),
                        width: 0.8),
                    borderRadius: const BorderRadius.all(Radius.circular(3)),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: PartnerAppColors.darkBlue.withValues(alpha: .4)),
                    borderRadius: const BorderRadius.all(Radius.circular(3)),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context).colorScheme.error, width: 0.8),
                    borderRadius: const BorderRadius.all(Radius.circular(3)),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: PartnerAppColors.darkBlue.withValues(alpha: .4),
                        width: 0.8),
                    borderRadius: const BorderRadius.all(Radius.circular(3)),
                  ),
                  suffixIcon: suffixIcon,
                  prefixIcon: prefixIcon),
            ),
          ),
        ],
      ),
    );
  }
}

class CustomFilePickerFormBuilder extends StatelessWidget {
  const CustomFilePickerFormBuilder({
    super.key,
    required this.name,
    this.description,
    this.labelText,
    this.isRequired,
    this.validator,
  });

  final String name;
  final String? description;
  final String? labelText;
  final bool? isRequired;
  final String? Function(List<PlatformFile>?)? validator;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (labelText != null) ...[
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: labelText,
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 18),
                ),
                if (isRequired != null && isRequired!)
                  TextSpan(
                    text: '*',
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium!
                        .copyWith(letterSpacing: 1.0, color: Colors.red),
                  ),
              ],
            ),
          ),
          SmartGaps.gapH10,
        ],
        FormBuilderField<List<PlatformFile>>(
          name: name,
          validator: validator,
          builder: (field) {
            return InputDecorator(
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.zero,
                errorText: field.errorText,
              ),
              child: Column(
                children: [
                  DottedBorder(
                    dashPattern: const [10, 10, 10, 10],
                    child: Container(
                      padding: const EdgeInsets.all(20),
                      width: double.infinity,
                      child: Column(
                        children: [
                          Text(
                            description ?? '',
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium!
                                .copyWith(color: PartnerAppColors.spanishGrey),
                            textAlign: TextAlign.center,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          ElevatedButton(
                            onPressed: () async {
                              var fileResultList =
                                  await FilePicker.platform.pickFiles(
                                type: FileType.any,
                                allowMultiple: true,
                              );

                              if (fileResultList != null) {
                                List<PlatformFile> files = [
                                  ...(field.value ?? [])
                                ];
                                files.addAll(fileResultList.files);
                                field.didChange(files);
                              }
                            },
                            style: ElevatedButton.styleFrom(
                                elevation: 0,
                                backgroundColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        color: PartnerAppColors.darkBlue
                                            .withValues(alpha: .4)))),
                            child: Text(
                              'Gennemse',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                    color: PartnerAppColors.spanishGrey,
                                  ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  ...(field.value ?? []).map((value) {
                    return Container(
                      margin: const EdgeInsets.only(bottom: 10),
                      padding: const EdgeInsets.all(10),
                      color: PartnerAppColors.spanishGrey.withValues(alpha: .3),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                              child: Text(
                            value.name.split('_').last,
                            style: Theme.of(context).textTheme.titleSmall,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          )),
                          InkWell(
                            onTap: () {
                              List<PlatformFile> files = [
                                ...(field.value ?? [])
                              ];
                              files.remove(value);

                              field.didChange(files);
                            },
                            child: const Icon(
                              FeatherIcons.x,
                              color: PartnerAppColors.spanishGrey,
                            ),
                          )
                        ],
                      ),
                    );
                  })
                ],
              ),
            );
          },
        )
      ],
    );
  }
}
