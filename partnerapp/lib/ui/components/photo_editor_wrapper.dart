import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/photo_edit_feature.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';

class Editor {
  static Future<File?> standard({
    required BuildContext context,
    File? file,
    required bool isImageUrl,
    String? imageUrl,
    required GlobalKey<NavigatorState> loadingkey,
  }) async {
    File? mfile = file;

    if (isImageUrl == false && mfile == null) return null;

    try {
      if (mfile == null) {
        showLoadingDialog(context, loadingkey);

        var base64String = await Formatter.networkImageToBase64(imageUrl);
        mfile = await Formatter.returnFileFromString(
            prename: 'profile',
            uniqueId: DateTime.now().toString(),
            encodedStr: base64String!);

        Navigator.of(loadingkey.currentContext!).pop();
      }
    } catch (e) {
      log("$e");
    }
    if (context.mounted) {
      final cropImage = await _cropImage(context: context, file: mfile!);
      return cropImage;
    }
    return null;
  }

  static Future<File?> photo({
    required BuildContext context,
    File? file,
    required bool isImageUrl,
    String? imageUrl,
    required GlobalKey<NavigatorState> loadingkey,
  }) async {
    File? mfile = file;

    if (isImageUrl == false && mfile == null) return null;

    if (loadingkey.currentContext != null) {
      showLoadingDialog(context, loadingkey);
    }
    double aspectRatio = 1.6;

    if (mfile == null) {
      var base64String = await Formatter.networkImageToBase64(imageUrl);

      if (base64String == null) {
        if (loadingkey.currentContext != null) {
          Navigator.of(loadingkey.currentContext!).pop();
        }
        return null;
      }

      log('BASE64: $base64String');
      mfile = await Formatter.returnFileFromString(
          prename: 'profile',
          uniqueId: DateTime.now().toString(),
          encodedStr: base64String);
    }

    if (context.mounted) {
      // cloudinaryUrl = await uploadImage(context, mfile);
      final base64String = base64Encode(await mfile.readAsBytes());

      aspectRatio = await Formatter.getAspectRatio(mfile);

      if (loadingkey.currentContext != null) {
        Navigator.of(loadingkey.currentContext!).pop();
      }
      if (context.mounted) {
        mfile = (await photoEdit(
            context: context,
            aspectRatio: aspectRatio,
            selectedImage: mfile,
            originalImageUrl: base64String));

        return mfile;
      }
    }
    return null;
  }

  static Future<File?> photoEdit({
    required BuildContext context,
    required double aspectRatio,
    required File selectedImage,
    required String originalImageUrl,
  }) async {
    File? editedFile = await Navigator.of(context, rootNavigator: true).push(
      MaterialPageRoute(
        builder: (context) => PhotoEditor(
          originalPhoto: selectedImage,
          photoUrl: originalImageUrl,
          aspectRatio: aspectRatio,
          onEditPhoto: (file) {},
          onEditUrl: (url) {},
        ),
      ),
    );

    return editedFile;
  }

  static Future<File?> _cropImage({
    required File file,
    required BuildContext context,
  }) async {
    final fileToBeCropped = await Formatter.compressImageToRightSize(file);
    if (context.mounted) {
      final CroppedFile? croppedFile0 = await ImageCropper().cropImage(
        sourcePath: fileToBeCropped.path,
        aspectRatio: const CropAspectRatio(ratioX: 4, ratioY: 3),
        uiSettings: [
          AndroidUiSettings(
              toolbarTitle: tr('crop_image'),
              toolbarColor: Colors.black,
              toolbarWidgetColor: Colors.white,
              initAspectRatio: CropAspectRatioPreset.ratio4x3,
              backgroundColor: Colors.white,
              lockAspectRatio: false),
          IOSUiSettings(
            title: tr('crop_image'),
          )
        ],
      );

      File? croppedFile = croppedFile0 == null
          ? null
          : File.fromUri(Uri(path: croppedFile0.path));

      return croppedFile;
    }
    return null;
  }
}
