import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/viewmodel/messages/message_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:badges/badges.dart';
import 'package:flutter/material.dart' hide Badge;
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class MessageAppBarIcon extends StatefulWidget {
  const MessageAppBarIcon({super.key});

  @override
  MessageAppBarIconState createState() => MessageAppBarIconState();
}

class MessageAppBarIconState extends State<MessageAppBarIcon> {
  @override
  Widget build(BuildContext context) {
    return Consumer<MessageV2ViewModel>(builder: (_, vm, __) {
      return SizedBox(
        width: 50,
        child: IconButton(
            icon: Badge(
              showBadge: vm.unreadInbox > 0,
              badgeAnimation: const BadgeAnimation.fade(),
              position: BadgePosition.topEnd(top: -2, end: -8),
              badgeStyle: BadgeStyle(
                badgeColor: Theme.of(context).colorScheme.error,
                shape: BadgeShape.circle,
              ),
              badgeContent: Center(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 2),
                  child: Text(
                    '${vm.unreadInbox}',
                    style: const TextStyle(color: Colors.white, fontSize: 10),
                  ),
                ),
              ),
              child: SvgPicture.asset(
                SvgIcons.appBarMessage,
                // fit: BoxFit.fitWidth,
                width: 32.0,
                height: 40.0,
                colorFilter: ColorFilter.mode(
                  Theme.of(context).colorScheme.onSurfaceVariant,
                  BlendMode.srcIn,
                ),
              ),
            ),
            onPressed: context.watch<NewsFeedViewModel>().busy
                ? null
                : () {
                    //

                    // COMMENTED FOR NOW.
                    // WILL BE REPLACED WHEN READY.

                    // context.read<MessageV2ViewModel>().defaultValues();
                    changeDrawerRoute(Routes.messageScreenV2);

                    // changeDrawerRoute(Routes.messageScreen);

                    //
                  }),
      );
    });
  }
}
