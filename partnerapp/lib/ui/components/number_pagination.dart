import 'package:Haandvaerker.dk/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';

class NumberPagination extends StatelessWidget {
  final int currentPage;
  final int totalPages;
  final Function(int) onPageChanged;

  const NumberPagination({
    super.key,
    required this.currentPage,
    required this.totalPages,
    required this.onPageChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        // Button to go to the first page
        _buildNavigationButton(currentPage > 1, FeatherIcons.chevronsLeft, () {
          onPageChanged(1);
        }),
        // Button to go to the previous page
        _buildNavigationButton(currentPage > 1, Icons.chevron_left, () {
          if (currentPage > 1) {
            onPageChanged(currentPage - 1);
          }
        }),
        Flexible(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: _buildPageButtons(context),
          ),
        ),
        // Button to go to the next page
        _buildNavigationButton(currentPage < totalPages, Icons.chevron_right,
            () {
          if (currentPage < totalPages) {
            onPageChanged(currentPage + 1);
          }
        }),
        // Button to go to the last page
        _buildNavigationButton(
            currentPage < totalPages, FeatherIcons.chevronsRight, () {
          onPageChanged(totalPages);
        }),
      ],
    );
  }

  // Widget for creating navigation buttons
  Widget _buildNavigationButton(
      bool isEnabled, IconData icon, VoidCallback onPressed) {
    return IconButton(
      icon: Icon(
        icon,
        size: 30,
        color: isEnabled
            ? PartnerAppColors.darkBlue
            : PartnerAppColors.spanishGrey,
      ),
      onPressed: isEnabled ? onPressed : null,
    );
  }

  // Build the list of page number buttons
  List<Widget> _buildPageButtons(BuildContext context) {
    List<Widget> buttons = [];
    for (int i = _calculateStartPage(); i <= _calculateEndPage(); i++) {
      buttons.add(
        _buildButton(
          pageNumber: i,
          text: i.toString(),
          context: context,
          isSelected: i == currentPage,
        ),
      );
    }

    if (buttons.length == 2) {
      if (currentPage == 1) {
        buttons.add(_buildButton(
            pageNumber: 0,
            text: '. . .',
            context: context,
            isSelected: false,
            isEnabled: false));
      } else if (currentPage == totalPages) {
        buttons.insert(
            0,
            _buildButton(
                pageNumber: 0,
                text: '. . .',
                context: context,
                isSelected: false,
                isEnabled: false));
      }
    }
    return buttons;
  }

  // Calculate the start page based on the current page
  int _calculateStartPage() {
    int startPage = currentPage - 1;
    if (startPage < 1) {
      startPage = 1;
    }
    return startPage;
  }

  // Calculate the end page based on the current page
  int _calculateEndPage() {
    int endPage = currentPage + 1;
    if (endPage > totalPages) {
      endPage = totalPages;
    }
    return endPage;
  }

  // Build individual page number button
  Widget _buildButton(
      {required int pageNumber,
      required String text,
      required BuildContext context,
      required bool isSelected,
      bool isEnabled = true}) {
    return GestureDetector(
      onTap: isEnabled
          ? () {
              onPageChanged(pageNumber);
            }
          : null,
      child: Container(
        padding: const EdgeInsets.all(15),
        // Apply background color if the button is selected
        decoration: isSelected
            ? const BoxDecoration(
                shape: BoxShape.circle,
                color: PartnerAppColors.blue,
              )
            : null,
        child: Center(
          child: Text(
            text,
            style: TextStyle(
              fontSize: 16,
              color: isSelected ? Colors.white : PartnerAppColors.darkBlue,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
    );
  }
}
