import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/labor_task_toggle_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class M2mOptionsDialog extends StatelessWidget {
  const M2mOptionsDialog({super.key});

  @override
  Widget build(BuildContext context) {
    List<Map<String, dynamic>> m2mOptions = [
      {
        'title': tr('find_task'),
        'image': 'assets/images/blueprint.svg',
        'screen': Routes.marketPlaceScreen,
        'args': null,
      },
      {
        'title': tr('find_labor'),
        'image': 'assets/images/recruitment.svg',
        'screen': Routes.marketPlaceScreen,
        'args': null,
      },
      {
        'title': tr('post_task'),
        'image': 'assets/images/mortgage.svg',
        'screen': Routes.m2mTaskScreen,
        'args': true,
      },
      {
        'title': tr('post_labor'),
        'image': 'assets/images/select-worker.svg',
        'screen': Routes.m2mLaborScreen,
        'args': true,
      },
    ];

    return SingleChildScrollView(
      physics: const AlwaysScrollableScrollPhysics(),
      child: Container(
        margin: const EdgeInsets.only(top: 50, bottom: 50),
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: GridView.count(
          crossAxisCount: 2,
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          children: List.generate(
            m2mOptions.length,
            (index) {
              final f = m2mOptions.elementAt(index);
              return InkWell(
                splashColor: Theme.of(context).colorScheme.primary,
                onTap: () {
                  final toggleVM = context.read<LaborTaskToggleViewModel>();
                  if (index == 0) {
                    toggleVM.setTask(true);
                  } else if (index == 1) {
                    toggleVM.setTask(false);
                  }
                  changeDrawerRoute(f['screen'] as String?,
                      arguments: f['args']);
                  Navigator.pop(context);
                },
                child: Container(
                    height: 180,
                    decoration: BoxDecoration(
                        border: Border(
                      right: (index % 2 == 0)
                          ? BorderSide(
                              color: Colors.grey[300]!,
                              width: 1.5,
                            )
                          : BorderSide.none,
                      bottom: (index < 2)
                          ? BorderSide(
                              color: Colors.grey[300]!,
                              width: 1.5,
                            )
                          : BorderSide.none,
                    )),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: Center(
                                    child: SvgPicture.asset(
                                        f['image'] as String,
                                        colorFilter: ColorFilter.mode(
                                          Theme.of(context).colorScheme.primary,
                                          BlendMode.srcIn,
                                        ),
                                        width: 50,
                                        height: 50)),
                              )
                            ],
                          ),
                          // Image.asset(f['image']),
                          SmartGaps.gapH10,
                          Flexible(
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, right: 8.0),
                              child: SizedBox(
                                height: 50,
                                child: Text(
                                  f['title'] as String,
                                  textAlign: TextAlign.center,
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleSmall!
                                      .copyWith(
                                          height: 1.2,
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                ),
                              ),
                            ),
                          )
                        ])),
              );
            },
          ),
        ),
      ),
    );
  }
}
