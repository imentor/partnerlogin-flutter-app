import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/location/location_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/market_place_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_background_geolocation/flutter_background_geolocation.dart'
    as bg;
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';

class AppBottomNavigationBar extends StatefulWidget {
  const AppBottomNavigationBar({super.key});

  @override
  AppBottomNavigationBarState createState() => AppBottomNavigationBarState();
}

class AppBottomNavigationBarState extends State<AppBottomNavigationBar> {
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
        selectedItemColor: Theme.of(context).colorScheme.onSurfaceVariant,
        unselectedItemColor: Theme.of(context).colorScheme.onSurfaceVariant,
        selectedLabelStyle: Theme.of(context)
            .textTheme
            .bodyMedium!
            .copyWith(fontWeight: FontWeight.normal),
        unselectedLabelStyle: Theme.of(context)
            .textTheme
            .bodyMedium!
            .copyWith(fontWeight: FontWeight.normal),
        selectedFontSize: 12,
        unselectedFontSize: 12,
        backgroundColor: Colors.white,
        elevation: 10,
        onTap: (value) {
          switch (value) {
            case 0:
              myCustomerRedirect();
              break;
            case 1:
              marketplaceRedirect();
              break;
            case 2:
              menuRedirect();
            default:
          }
        },
        items: [
          BottomNavigationBarItem(
            label: tr('my_customers'),
            icon: SvgPicture.asset(
              SvgIcons.customer,
              height: 30.0,
              width: 30.0,
              colorFilter: ColorFilter.mode(
                Theme.of(context).colorScheme.onSurfaceVariant,
                BlendMode.srcIn,
              ),
            ),
          ),
          BottomNavigationBarItem(
            label: tr('marketplace'),
            icon: SvgPicture.asset(
              SvgIcons.marketplace,
              height: 30.0,
              width: 30.0,
              colorFilter: ColorFilter.mode(
                Theme.of(context).colorScheme.onSurfaceVariant,
                BlendMode.srcIn,
              ),
            ),
          ),
          const BottomNavigationBarItem(
            label: 'Menu',
            icon: Icon(Icons.menu, color: PartnerAppColors.darkBlue, size: 30),
          )
        ]);
  }

  void myCustomerRedirect() async {
    final locationVm = context.read<LocationViewModel>();
    final appDrawerVm = context.read<AppDrawerViewModel>();
    final userVm = context.read<UserViewModel>();

    final isGrowthPlusPackage =
        appDrawerVm.bitrixCompanyModel?.isVaekstPlus ?? false;
    final hasAccountIntegration =
        (appDrawerVm.bitrixCompanyModel?.accountIntegration ?? false);

    if ((appDrawerVm.bitrixCompanyModel?.companyTracking ?? 0) == 1) {
      await locationVm.checkPermission().whenComplete(() {
        final isLocationDenied =
            locationVm.locationPermission == LocationPermission.denied;
        final isLocationDeniedForever =
            locationVm.locationPermission == LocationPermission.deniedForever;
        final isLocationUnableToDetermine = locationVm.locationPermission ==
            LocationPermission.unableToDetermine;
        final isLocationWhileInUse =
            locationVm.locationPermission == LocationPermission.whileInUse;
        if ((!hasAccountIntegration &&
                (isLocationDenied ||
                    isLocationDeniedForever ||
                    isLocationUnableToDetermine ||
                    isLocationWhileInUse)) &&
            isGrowthPlusPackage) {
          locationVm.updateLocationInfo(gpsLocation: 0);

          if (!mounted) return;

          disabledDialog(
                  context: context,
                  message: tr('no_gps_access_my_customer'),
                  companyName: userVm.userModel?.user?.name ?? '')
              .then((value) {
            if ((value ?? false)) {
              tryCatchWrapper(
                      context: myGlobals.innerScreenScaffoldKey!.currentContext,
                      function: locationVm.checkPermission())
                  .then((value) {
                if (locationVm.locationPermission ==
                    LocationPermission.always) {
                  bg.BackgroundGeolocation.stop();
                  locationVm.startLocation();

                  // if (locationVm.locationPermission ==
                  //     LocationPermission.whileInUse) {
                  //   showLocationPermissionSettingsDialog(context);
                  // }

                  locationVm.updateLocationInfo(gpsLocation: 1);
                } else if (locationVm.locationPermission ==
                        LocationPermission.deniedForever ||
                    locationVm.locationPermission ==
                        LocationPermission.denied ||
                    locationVm.locationPermission ==
                        LocationPermission.whileInUse) {
                  // showLocationPermissionSettingsDialog(context);

                  locationVm.updateLocationInfo(gpsLocation: 0);
                }
              });
            }
          });
        } else {
          changeDrawerRoute(Routes.yourClient);
        }
      });
    } else {
      changeDrawerRoute(Routes.yourClient);
    }
  }

  void marketplaceRedirect() {
    final marketplaceVm = context.read<MarketPlaceV2ViewModel>();
    if (!marketplaceVm.fromNewsFeed) {
      marketplaceVm.setBusy(true);
    }
    changeDrawerRoute(Routes.marketPlaceScreen);
  }

  void menuRedirect() {
    if (myGlobals.innerScreenScaffoldKey!.currentState!.isDrawerOpen) {
      // Navigator.of(context).pop();
      myGlobals.innerScreenScaffoldKey!.currentState!.closeDrawer();
    } else {
      myGlobals.innerScreenScaffoldKey!.currentState!.openDrawer();
    }

    context.read<AppDrawerViewModel>().isDrawerOpen =
        myGlobals.innerScreenScaffoldKey!.currentState!.isDrawerOpen;
  }
}
