import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:timeline_tile/timeline_tile.dart';

class StepsWidget extends StatelessWidget {
  const StepsWidget(
      {super.key,
      this.currentPage,
      this.steps = 2,
      this.title1 = 'Step',
      this.title2 = 'Step',
      this.title3 = 'Step'});

  final int? currentPage;
  final int steps;
  final String title1;
  final String title2;
  final String title3;

  @override
  Widget build(BuildContext context) {
    return _timeline(context);
  }

  Widget _timeline(BuildContext context) {
    const width = 20.00;

    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20),
      height: 120,
      width: double.infinity,
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Builder(builder: (context) {
              final isCurrentPage = 0 <= currentPage!;
              return TimelineTile(
                axis: TimelineAxis.horizontal,
                alignment: TimelineAlign.center,
                isFirst: true,
                indicatorStyle: IndicatorStyle(
                  width: 30.0,
                  height: 30.0,
                  indicator: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.fromBorderSide(BorderSide(
                          color: Theme.of(context).colorScheme.primary)),
                      color: isCurrentPage
                          ? Theme.of(context).colorScheme.primary
                          : Colors.white,
                    ),
                    child: Center(
                        child: Text('',
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(
                                  color: isCurrentPage
                                      ? Colors.white
                                      : Theme.of(context).colorScheme.primary,
                                ))),
                  ),
                ),
                beforeLineStyle: LineStyle(
                    thickness: 1.0,
                    color: Theme.of(context).colorScheme.primary),
                startChild: Container(
                  constraints: const BoxConstraints(
                    maxHeight: 20,
                    minWidth: width,
                  ),
                  child: Text(title1,
                      style: Theme.of(context).textTheme.bodyMedium,
                      textAlign: TextAlign.center),
                ),
              );
            }),
            Builder(builder: (context) {
              final isCurrentPage = 0 <= currentPage!;
              return TimelineTile(
                axis: TimelineAxis.horizontal,
                alignment: TimelineAlign.center,
                indicatorStyle: IndicatorStyle(
                  width: 30.0,
                  height: 30.0,
                  indicator: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.fromBorderSide(BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withValues(alpha: 0.4))),
                      color: isCurrentPage
                          ? Theme.of(context).colorScheme.primary
                          : Colors.white,
                    ),
                    child: Center(
                        child: Text('1',
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(
                                  color: isCurrentPage
                                      ? Colors.white
                                      : Theme.of(context)
                                          .colorScheme
                                          .primary
                                          .withValues(alpha: 0.4),
                                ))),
                  ),
                ),
                afterLineStyle: LineStyle(
                    thickness: 1.0,
                    color: Theme.of(context)
                        .colorScheme
                        .primary
                        .withValues(alpha: 1 <= currentPage! ? 1.0 : 0.4)),
                beforeLineStyle: LineStyle(
                    thickness: 1.0,
                    color: Theme.of(context).colorScheme.primary),
                startChild: Container(
                  constraints: const BoxConstraints(
                    maxHeight: 20,
                    minWidth: width,
                  ),
                  child: Opacity(
                    opacity: isCurrentPage ? 1.0 : 0.4,
                    child: Text(title2,
                        style: Theme.of(context).textTheme.bodyMedium,
                        textAlign: TextAlign.center),
                  ),
                ),
              );
            }),
            Builder(builder: (context) {
              final isCurrentPage = 1 <= currentPage!;
              return TimelineTile(
                axis: TimelineAxis.horizontal,
                alignment: TimelineAlign.center,
                isLast: true,
                indicatorStyle: IndicatorStyle(
                  width: 30.0,
                  height: 30.0,
                  indicator: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.fromBorderSide(BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withValues(alpha: 0.4))),
                      color: isCurrentPage
                          ? Theme.of(context).colorScheme.primary
                          : Colors.white,
                    ),
                    child: Center(
                        child: Text('2',
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(
                                    color: isCurrentPage
                                        ? Colors.white
                                        : Theme.of(context)
                                            .colorScheme
                                            .primary
                                            .withValues(alpha: 0.4)))),
                  ),
                ),
                beforeLineStyle: LineStyle(
                    thickness: 1.0,
                    color: Theme.of(context)
                        .colorScheme
                        .primary
                        .withValues(alpha: isCurrentPage ? 1.0 : 0.4)),
                startChild: Container(
                  constraints: const BoxConstraints(
                    minWidth: width,
                  ),
                  child: Opacity(
                    opacity: isCurrentPage ? 1.0 : 0.4,
                    child: Text(title3,
                        style: Theme.of(context).textTheme.bodyMedium,
                        textAlign: TextAlign.center),
                  ),
                ),
              );
            }),
          ],
        ),
      ),
    );
  }
}

class DynamicStepsWidget extends StatelessWidget {
  const DynamicStepsWidget({
    super.key,
    required this.currentPage,
    required this.steps,
    required this.titles,
    this.isLabelAtTop = true,
    this.isHorizontal = true,
  });
  final int currentPage;
  final int steps;
  final List<String> titles;
  final bool? isLabelAtTop;
  final bool? isHorizontal;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: List.generate(
        steps,
        (index) {
          final isCurrentPage = index <= currentPage;
          return Expanded(
            child: TimelineTile(
              axis: TimelineAxis.horizontal,
              alignment: TimelineAlign.center,
              isFirst: index == 0,
              isLast: index == steps - 1,
              indicatorStyle: IndicatorStyle(
                width: 30.0,
                height: 30.0,
                indicator: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: isCurrentPage
                        ? Theme.of(context).colorScheme.primary
                        : const Color(0xffCECECE),
                  ),
                  child: Center(
                    child: Text(
                      '${index + 1}',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.notoSans(
                        textStyle: const TextStyle(
                          fontSize: 20,
                          height: 1.5,
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              afterLineStyle: LineStyle(
                  thickness: 1.0,
                  color: currentPage > index
                      ? Theme.of(context).colorScheme.primary
                      : const Color(0xffCECECE)),
              beforeLineStyle: LineStyle(
                thickness: 1.0,
                color: isCurrentPage
                    ? Theme.of(context).colorScheme.primary
                    : const Color(0xffCECECE),
              ),
              startChild: isLabelAtTop!
                  ? Text(
                      titles[index],
                      style: GoogleFonts.notoSans(
                        textStyle: const TextStyle(
                          fontSize: 13,
                          height: 2,
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    )
                  : null,
              endChild: isLabelAtTop!
                  ? null
                  : Text(
                      titles[index],
                      textAlign: TextAlign.center,
                      style: GoogleFonts.notoSans(
                        textStyle: const TextStyle(
                          fontSize: 13,
                          height: 2,
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
            ),
          );
        },
      ),
    );
  }
}
