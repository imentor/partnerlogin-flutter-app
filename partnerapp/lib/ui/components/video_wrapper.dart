import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoSection extends StatefulWidget {
  const VideoSection({
    super.key,
    required this.videoUrl,
    this.fromMarketPlace = false,
    this.completeAction,
  });

  final String? videoUrl;
  final bool fromMarketPlace;
  final VoidCallback? completeAction;

  @override
  VideoSectionState createState() => VideoSectionState();
}

class VideoSectionState extends State<VideoSection> {
  VideoPlayerController? _controller;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.networkUrl(Uri.parse(widget.videoUrl!))
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {});
      });

    if (widget.fromMarketPlace) {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        _controller!.addListener(() async {
          final position = await _controller!.position;
          if (position!.inSeconds >= 32) {
            widget.completeAction!.call();
            _controller!.removeListener(() {});
          }
        });
      });
    } else {
      _controller!.setLooping(true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: _controller!.value.aspectRatio,
      child: _controller!.value.isInitialized
          ? Center(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: <Widget>[
                  VideoPlayer(_controller!..play()),
                  _controller!.value.isBuffering
                      ? Center(
                          child: loader(),
                        )
                      : PlayPauseOverlay(controller: _controller),
                  VideoProgressIndicator(_controller!, allowScrubbing: false),
                ],
              ),
            )
          : Container(
              decoration: BoxDecoration(
                  color: PartnerAppColors.accentBlue.withValues(alpha: 0.2)),
              child: _controller!.value.isBuffering
                  ? Center(
                      child: loader(),
                    )
                  : Center(
                      child: Icon(
                        Icons.play_circle_filled,
                        size: 50,
                        color: Colors.black.withValues(alpha: 0.4),
                      ),
                    ),
            ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller!.dispose();
  }
}

class PlayPauseOverlay extends StatefulWidget {
  const PlayPauseOverlay({super.key, this.controller});

  final VideoPlayerController? controller;

  @override
  PlayPauseOverlayState createState() => PlayPauseOverlayState();
}

class PlayPauseOverlayState extends State<PlayPauseOverlay> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: const Duration(milliseconds: 50),
          reverseDuration: const Duration(milliseconds: 200),
          child: widget.controller!.value.isPlaying
              ? const SizedBox.shrink()
              : Container(
                  color: Colors.black26,
                  child: const Center(
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: 100.0,
                    ),
                  ),
                ),
        ),
        GestureDetector(
          onTap: () {
            setState(() {
              widget.controller!.value.isPlaying
                  ? widget.controller!.pause()
                  : widget.controller!.play();
            });
          },
        ),
      ],
    );
  }
}
