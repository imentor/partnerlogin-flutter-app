import 'package:flutter/material.dart';

class CustomNumberPaginator extends StatefulWidget {
  /// Total number of pages that should be shown.
  final int numberPages;

  /// For going back to index 1 if inputting in searchbox.
  final bool isSearching;

  /// Index of initially selected page.
  final int initialPage;

  /// This function is called when the user switches between pages. The received
  /// parameter indicates the selected index, starting from 0.
  final Function(int)? onPageChange;

  /// The height of the number paginator.
  final double height;

  /// The shape of the [PaginatorButton]s.
  final OutlinedBorder? buttonShape;

  /// The [PaginatorButton]'s foreground color (text/icon color) when selected.
  ///
  /// Defaults to [Colors.white].
  final Color? buttonSelectedForegroundColor;

  /// The [PaginatorButton]'s foreground color (text/icon color) when unselected.
  ///
  /// Defaults to `null`.
  final Color? buttonUnselectedForegroundColor;

  /// The [PaginatorButton]'s background color when selected.
  ///
  /// Defaults to the [Theme]'s accent color.
  final Color? buttonSelectedBackgroundColor;

  /// The [PaginatorButton]'s background color when unselected.
  ///
  /// Defaults to `null`.
  final Color? buttonUnselectedBackgroundColor;

  /// Creates an instance of [CustomNumberPaginator].
  const CustomNumberPaginator({
    super.key,
    required this.numberPages,
    this.initialPage = 0,
    this.isSearching = false,
    this.onPageChange,
    this.height = 48.0,
    this.buttonShape,
    this.buttonSelectedForegroundColor,
    this.buttonUnselectedForegroundColor,
    this.buttonSelectedBackgroundColor,
    this.buttonUnselectedBackgroundColor,
  });

  @override
  CustomNumberPaginatorState createState() => CustomNumberPaginatorState();
}

class CustomNumberPaginatorState extends State<CustomNumberPaginator> {
  late int _currentPage;
  int _availableSpots = 0;

  @override
  void initState() {
    _currentPage = widget.initialPage;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: widget.height,
      child: Row(
        children: [
          PaginatorButton(
            onPressed: _currentPage > 0 ? _toFirst : null,
            shape: widget.buttonShape,
            selectedForegroundColor: widget.buttonSelectedForegroundColor,
            unSelectedforegroundColor: widget.buttonUnselectedForegroundColor,
            selectedBackgroundColor: widget.buttonSelectedBackgroundColor,
            unSelectedBackgroundColor: widget.buttonUnselectedBackgroundColor,
            child: const Icon(Icons.keyboard_double_arrow_left_rounded),
          ),
          PaginatorButton(
            onPressed: _currentPage > 0 ? _prev : null,
            shape: widget.buttonShape,
            selectedForegroundColor: widget.buttonSelectedForegroundColor,
            unSelectedforegroundColor: widget.buttonUnselectedForegroundColor,
            selectedBackgroundColor: widget.buttonSelectedBackgroundColor,
            unSelectedBackgroundColor: widget.buttonUnselectedBackgroundColor,
            child: const Icon(Icons.keyboard_arrow_left_rounded),
          ),
          Expanded(
            child: LayoutBuilder(
              builder: (context, constraints) {
                _availableSpots = (constraints.maxWidth / _buttonWidth).floor();

                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    ..._generateButtonList(),
                  ],
                );
              },
            ),
          ),
          PaginatorButton(
            onPressed: _currentPage < widget.numberPages - 1 ? _next : null,
            shape: widget.buttonShape,
            selectedForegroundColor: widget.buttonSelectedForegroundColor,
            unSelectedforegroundColor: widget.buttonUnselectedForegroundColor,
            selectedBackgroundColor: widget.buttonSelectedBackgroundColor,
            unSelectedBackgroundColor: widget.buttonUnselectedBackgroundColor,
            child: const Icon(Icons.keyboard_arrow_right_rounded),
          ),
          PaginatorButton(
            onPressed: _currentPage < widget.numberPages - 1 ? _toLast : null,
            shape: widget.buttonShape,
            selectedForegroundColor: widget.buttonSelectedForegroundColor,
            unSelectedforegroundColor: widget.buttonUnselectedForegroundColor,
            selectedBackgroundColor: widget.buttonSelectedBackgroundColor,
            unSelectedBackgroundColor: widget.buttonUnselectedBackgroundColor,
            child: const Icon(Icons.keyboard_double_arrow_right_rounded),
          ),
        ],
      ),
    );
  }

  /// Buttons have an aspect ratio of 1:1. Therefore use paginator height as
  /// button width.
  double get _buttonWidth => widget.height;

  void _prev() {
    FocusManager.instance.primaryFocus?.unfocus();

    setState(() {
      _currentPage--;
    });
    widget.onPageChange?.call(_currentPage);
  }

  void _next() {
    FocusManager.instance.primaryFocus?.unfocus();

    setState(() {
      _currentPage++;
    });
    widget.onPageChange?.call(_currentPage);
  }

  void _toFirst() {
    FocusManager.instance.primaryFocus?.unfocus();

    setState(() {
      _currentPage = 0;
    });
    widget.onPageChange?.call(_currentPage);
  }

  void _toLast() {
    FocusManager.instance.primaryFocus?.unfocus();

    setState(() {
      _currentPage = widget.numberPages - 1;
    });
    widget.onPageChange?.call(_currentPage);
  }

  /// Generates the variable button list which is on the left of the (optional)
  /// dots. The very last page is shown independently of this list.
  List<Widget> _generateButtonList() {
    final indicator = _availableSpots - 1;
    List<int> minPages = [];
    List<int> maxPages = [];
    minPages.addAll(List.generate(indicator, (index) => index).toList());
    maxPages.addAll(
        List.generate(indicator, (index) => (widget.numberPages - 1) - index)
            .toList());

    if (minPages.contains(_currentPage)) {
      return List.generate(_availableSpots, (index) => _buildPageButton(index));
    } else if (maxPages.contains(_currentPage)) {
      return List.generate(
          _availableSpots,
          (index) => _buildPageButton(
              (widget.numberPages - 1) - ((_availableSpots - 1) - index)));
    } else {
      return List.generate(
          _availableSpots, (index) => _buildPageButton(_currentPage + index));
    }
  }

  /// Builds a button for the given index.
  Widget _buildPageButton(int index) {
    return PaginatorButton(
      onPressed: () {
        FocusManager.instance.primaryFocus?.unfocus();
        setState(() {
          _currentPage = index;
        });
        widget.onPageChange?.call(index);
      },
      selected: _selected(index),
      shape: widget.buttonShape,
      selectedForegroundColor: widget.buttonSelectedForegroundColor,
      unSelectedforegroundColor: widget.buttonUnselectedForegroundColor,
      selectedBackgroundColor: widget.buttonSelectedBackgroundColor,
      unSelectedBackgroundColor: widget.buttonUnselectedBackgroundColor,
      child: Text((index + 1).toString()),
    );
  }

  /// Checks if the given index is currently selected.
  bool _selected(index) {
    if (widget.isSearching) {
      /// To avoid `setState() or markNeedsBuild called during build` error,
      /// the setstate is wrapped with `Future.delayed()`
      Future.delayed(Duration.zero, () async {
        setState(() {
          index = 0;
          _currentPage = 0;
        });
      });
    }

    return index == _currentPage;
  }
}

class PaginatorButton extends StatelessWidget {
  /// Callback for button press.
  final VoidCallback? onPressed;

  /// The child of the button.
  final Widget child;

  /// Whether the button is currently selected.
  final bool selected;

  /// The shape of the button as an [OutlinedBorder].
  ///
  /// Defaults to [CircleBorder].
  final OutlinedBorder? shape;

  /// The button's background color when selected.
  final Color? selectedBackgroundColor;

  /// The button's background color when not selected.
  final Color? unSelectedBackgroundColor;

  /// The button's foreground color when selected.
  final Color? selectedForegroundColor;

  /// The button's foreground color when not selected.
  final Color? unSelectedforegroundColor;

  /// Creates an instance of [PaginatorButton].
  const PaginatorButton({
    super.key,
    required this.onPressed,
    required this.child,
    this.selected = false,
    this.shape,
    this.selectedBackgroundColor,
    this.unSelectedBackgroundColor,
    this.selectedForegroundColor,
    this.unSelectedforegroundColor,
  });

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: TextButton(
          onPressed: onPressed,
          style: TextButton.styleFrom(
            shape: shape ?? const CircleBorder(),
            backgroundColor: _backgroundColor(context, selected),
            foregroundColor: _foregroundColor(context, selected),
          ),
          child: child,
        ),
      ),
    );
  }

  Color? _backgroundColor(BuildContext context, bool selected) => selected
      ? (selectedBackgroundColor ?? Theme.of(context).colorScheme.secondary)
      : unSelectedBackgroundColor;

  Color? _foregroundColor(BuildContext context, bool selected) => selected
      ? (selectedForegroundColor ?? Colors.white)
      : unSelectedforegroundColor;
}
