import 'dart:io';

import 'package:Haandvaerker.dk/ui/components/get_image_with_custom_picker.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SingleImageSelector extends StatefulWidget {
  const SingleImageSelector({
    super.key,
    required this.height,
    required this.width,
    this.initialFile,
    this.onImageChange,
    this.disable = false,
  });

  final double height;
  final double width;
  final File? initialFile;
  final Function(File?)? onImageChange;
  final bool disable;

  @override
  SingleImageSelectorState createState() => SingleImageSelectorState();
}

class SingleImageSelectorState extends State<SingleImageSelector> {
  File? _selectedImage;

  @override
  void initState() {
    // _selectedImage = widget.initialFile;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _selectedImage = widget.initialFile;
    return (_selectedImage == null)
        ? GestureDetector(
            onTap: () async {
              if (widget.disable) {
                showOkAlertDialog(
                    context: myGlobals.homeScaffoldKey!.currentContext!,
                    message: tr('uploading_is_currently_disabled'),
                    title: tr('profile_pictures'));
              } else {
                File? file = await getImage(context);
                setState(() {
                  _selectedImage = file;
                });
                widget.onImageChange!(_selectedImage);
              }
            },
            child: Container(
                margin: const EdgeInsets.only(left: 10),
                height: 150,
                width: 150,
                decoration: const BoxDecoration(color: Color(0xffb2d4db)),
                child: const Center(
                    child: Icon(FontAwesomeIcons.camera,
                        color: Color(0xff11a0bf)))),
          )
        : Stack(
            children: [
              Container(
                  margin: const EdgeInsets.only(left: 10),
                  height: 150,
                  width: 150,
                  decoration: const BoxDecoration(color: Color(0xffb2d4db)),
                  child: Image.file(_selectedImage!, fit: BoxFit.cover)),
              Positioned(
                  top: 5,
                  right: 5,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        _selectedImage = null;
                      });
                      widget.onImageChange!(null);
                    },
                    child: Container(
                        padding: const EdgeInsets.all(2),
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle, color: Colors.grey),
                        child: const Icon(
                          Icons.close,
                          color: Colors.white,
                          size: 25,
                        )),
                  )),
            ],
          );
  }
}
