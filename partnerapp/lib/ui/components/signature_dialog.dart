import 'dart:convert';
import 'dart:ui' as ui;

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/create_offer_wizard/sign_submit_offer/create_offer.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:syncfusion_flutter_signaturepad/signaturepad.dart';

Future<String?> signatureDialog({
  required BuildContext context,
  required String label,
  required String htmlTerms,
  String buttonLabel = 'submit',
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: CommonSignatureDialog(
              htmlTerms: htmlTerms,
              label: label,
              buttonLabel: buttonLabel,
            ))),
      );
    },
  );
}

class CommonSignatureDialog extends StatefulWidget {
  const CommonSignatureDialog({
    super.key,
    required this.htmlTerms,
    required this.label,
    required this.buttonLabel,
  });

  final String label;
  final String htmlTerms;
  final String buttonLabel;

  @override
  CommonSignatureDialogState createState() => CommonSignatureDialogState();
}

class CommonSignatureDialogState extends State<CommonSignatureDialog> {
  final GlobalKey<SfSignaturePadState> _signaturePadKey = GlobalKey();

  bool isChecked = false;
  bool didSign = false;

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            tr('sign_here'),
            style: Theme.of(context).textTheme.displayLarge!.copyWith(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
          ),
          InkWell(
            onTap: () {
              _signaturePadKey.currentState!.clear();
            },
            child: Row(children: [
              const Icon(
                FeatherIcons.trash2,
                color: PartnerAppColors.red,
                size: 16,
              ),
              SmartGaps.gapW10,
              Text(
                tr('clear_signature'),
                style: Theme.of(context).textTheme.displayLarge!.copyWith(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: PartnerAppColors.red,
                    ),
              )
            ]),
          )
        ],
      ),
      SmartGaps.gapH10,
      Container(
        height: 250,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
            color: PartnerAppColors.black.withValues(alpha: 0.5),
          ),
        ),
        child: Center(
          child: SfSignaturePad(
            key: _signaturePadKey,
            minimumStrokeWidth: 1,
            maximumStrokeWidth: 3,
            onDraw: (offset, time) {
              setState(() {
                didSign = true;
              });
            },
          ),
        ),
      ),
      SmartGaps.gapH10,
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Transform.scale(
            scale: 1.2,
            child: SizedBox(
              width: 24,
              height: 24,
              child: Checkbox(
                onChanged: (val) {
                  setState(() {
                    isChecked = val!;
                  });
                },
                value: isChecked,
                activeColor: PartnerAppColors.darkBlue,
              ),
            ),
          ),
          SmartGaps.gapW10,
          GestureDetector(
            onTap: () {
              termsDialog(context: context, termsHTML: widget.htmlTerms);
            },
            child: Text(
              widget.label,
              style: Theme.of(context).textTheme.displayLarge!.copyWith(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: PartnerAppColors.blue,
                  decoration: TextDecoration.underline),
            ),
          ),
        ],
      ),
      SmartGaps.gapH30,
      CustomDesignTheme.flatButtonStyle(
        height: 50,
        backgroundColor: isChecked && didSign
            ? PartnerAppColors.malachite
            : PartnerAppColors.spanishGrey,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        onPressed: () async {
          if (isChecked && didSign) {
            final BuildContext currentContext = context;

            if (_signaturePadKey.currentState != null) {
              final data =
                  await _signaturePadKey.currentState!.toImage(pixelRatio: 1.0);
              final bytes =
                  await data.toByteData(format: ui.ImageByteFormat.png);

              if (currentContext.mounted) {
                final signature =
                    'data:image/png;base64,${base64.encode(bytes!.buffer.asUint8List())}';
                Navigator.of(currentContext).pop(signature);
              }
            }
          }
        },
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text(
            tr(widget.buttonLabel),
            style: Theme.of(context).textTheme.displayLarge!.copyWith(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                  color: Colors.white,
                ),
          )
        ]),
      )
    ]);
  }
}
