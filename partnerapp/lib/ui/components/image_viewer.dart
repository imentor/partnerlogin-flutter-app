import 'package:Haandvaerker.dk/theme.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';

Future<void> imageViewerDialog(
    {required BuildContext context, required String fileUrl}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: ImageViewer(fileUrl: fileUrl),
        ),
      );
    },
  );
}

class ImageViewer extends StatefulWidget {
  const ImageViewer({super.key, required this.fileUrl});
  final String fileUrl;

  @override
  State<ImageViewer> createState() => _ImageViewerState();
}

class _ImageViewerState extends State<ImageViewer> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Flexible(
            child: CarouselSlider(items: [
          CachedNetworkImage(
            imageUrl: widget.fileUrl,
          )
        ], options: CarouselOptions()))
      ],
    );
  }
}
