import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField({
    super.key,
    required this.controller,
    required this.labelText,
    this.hintText,
    this.infoText,
    this.isMultiline = false,
    this.showInfo = false,
    this.validation,
    this.keyboardType,
    this.inputAction = TextInputAction.done,
    this.onTap,
    this.readOnly = false,
    this.isObscured = false,
    this.suffixIcon,
    this.titleWidget,
    this.decoration,
    this.inputStyle,
    this.onChanged,
    this.maxLength,
    this.prefixIcon,
    // this.hasClearIcon = false,
  });

  final bool showInfo;
  final Widget? titleWidget;
  final String labelText;
  final String? hintText;
  final String? infoText;
  final bool isMultiline;
  final TextEditingController controller;
  final String? Function(String?)? validation;
  final TextInputType? keyboardType;
  final TextInputAction inputAction;
  final Function()? onTap;
  final bool readOnly;
  final bool isObscured;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final InputDecoration? decoration;
  final TextStyle? inputStyle;
  final Function(String?)? onChanged;
  final int? maxLength;
  // final bool hasClearIcon;

  @override
  Widget build(BuildContext context) {
    String? defaultValidation(String? value) {
      if (value == null || value.trim().isEmpty) {
        return tr('please_enter_some_text');
      }
      return null;
    }

    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          if (titleWidget != null)
            titleWidget!
          else
            Text(labelText,
                style: Theme.of(context)
                    .textTheme
                    .titleSmall!
                    .copyWith(height: 1.2)),
          SmartGaps.gapH7,
          isMultiline
              ? TextFormField(
                  onChanged: onChanged,
                  keyboardType: keyboardType ?? TextInputType.multiline,
                  textInputAction: inputAction,
                  obscureText: isObscured,
                  maxLines: null,
                  controller: controller,
                  onTap: onTap,
                  readOnly: readOnly,
                  style: inputStyle,
                  maxLength: maxLength,
                  decoration: decoration ??
                      InputDecoration(
                          border: InputBorder.none,
                          filled: true,
                          fillColor: Colors.grey[200],
                          hintStyle: Theme.of(context).textTheme.titleSmall,
                          hintText: hintText,
                          suffixIcon: suffixIcon,
                          prefixIcon: prefixIcon),
                  validator: validation ?? defaultValidation)
              : TextFormField(
                  onChanged: onChanged,
                  controller: controller,
                  textInputAction: inputAction,
                  keyboardType: keyboardType ?? TextInputType.text,
                  onTap: onTap,
                  obscureText: isObscured,
                  readOnly: readOnly,
                  style: inputStyle,
                  maxLength: maxLength,
                  decoration: decoration ??
                      InputDecoration(
                          border: InputBorder.none,
                          filled: true,
                          hintStyle: const TextStyle(fontSize: 18),
                          hintText: hintText ?? '',
                          suffixIcon: suffixIcon,
                          prefixIcon: prefixIcon),
                  validator: validation ?? defaultValidation),
          if (showInfo)
            Container(
              padding: const EdgeInsets.only(top: 5),
              child: Text(infoText ?? '',
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium!
                      .copyWith(color: Colors.grey[400], height: 1.2)),
            )
          else
            Container(),
        ],
      ),
    );
  }
}

class SearchTextField extends StatefulWidget {
  const SearchTextField({
    super.key,
    this.hintText,
    required this.controller,
    required this.onChange,
    required this.onSubmit,
    required this.onClear,
    this.contentPadding =
        const EdgeInsets.only(left: 15, bottom: 20, top: 20, right: 15),
    this.elevation = 10.0,
    this.color = Colors.white,
  });

  final TextEditingController controller;
  final String? hintText;
  final Function(String text) onChange;
  final Function(String text) onSubmit;
  final Function() onClear;
  final EdgeInsets contentPadding;
  final double elevation;
  final Color color;

  @override
  SearchTextFieldState createState() => SearchTextFieldState();
}

class SearchTextFieldState extends State<SearchTextField> {
  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: widget.elevation,
      color: widget.color,
      shadowColor: Colors.white,
      child: Theme(
        data: Theme.of(context).copyWith(
            primaryColor: Theme.of(context).colorScheme.onSurfaceVariant),
        child: TextField(
          controller: widget.controller,
          textInputAction: TextInputAction.search,
          onChanged: (val) {
            widget.onChange(val);
          },
          onSubmitted: (val) {
            widget.onSubmit(val);
          },
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: widget.hintText ?? '',
            prefixIcon: const Icon(Icons.search),
            contentPadding: widget.contentPadding,
            suffixIcon: IconButton(
                icon: const Icon(Icons.clear),
                onPressed: () {
                  widget.controller.clear();
                  widget.onClear();
                }),
          ),
        ),
      ),
    );
  }
}

class CustomTextFieldNew extends StatelessWidget {
  const CustomTextFieldNew({
    super.key,
    required this.controller,
    this.labelText,
    this.hintText,
    this.infoText,
    this.suffixWidget,
    this.rightWidget,
    this.leftWidget,
    this.isMultiline = false,
    this.enabled,
    this.minLines = 3,
    this.maxLines = 10,
    this.flexTextField = 1,
    this.showInfo = false,
    this.validation,
    this.keyboardType,
    this.onTap,
    this.readOnly = false,
    this.focusNode,
    this.contentPadding,
    this.padding = const EdgeInsets.symmetric(vertical: 10),
    this.obscureText = false,
    this.onChanged,
    this.autofocus = false,
    this.isAutovalidate = false,
    this.isValidateAfterSave = false,
    this.labelColor = PartnerAppColors.darkBlue,
    this.inputTextColor = PartnerAppColors.darkBlue,
    this.isRequired = false,
    this.customStyle,
    this.prefixWidget,
    this.isEnabled = false,
    this.inputFormatters,
    this.textAlign,
    this.labelFontWeight,
    this.labelFontSize,
    this.tooltip,
  });

  final bool isRequired;
  final bool showInfo;
  final String? labelText;
  final String? hintText;
  final String? infoText;
  final Widget? suffixWidget;
  final Widget? prefixWidget;
  final Widget? rightWidget;
  final Widget? leftWidget;
  final bool? enabled;
  final EdgeInsetsGeometry? contentPadding;
  final bool isMultiline;
  final int minLines;
  final int maxLines;
  final int flexTextField;
  final TextEditingController? controller;
  final String? Function(String?)? validation;
  final TextInputType? keyboardType;
  final Function()? onTap;
  final bool readOnly;
  final FocusNode? focusNode;
  final EdgeInsetsGeometry padding;
  final Function(String)? onChanged;
  final bool obscureText;
  final bool autofocus;
  final bool isAutovalidate;
  final bool isValidateAfterSave;
  final Color labelColor;
  final Color inputTextColor;
  final TextStyle? customStyle;
  final bool isEnabled;
  final List<TextInputFormatter>? inputFormatters;
  final TextAlign? textAlign;
  final FontWeight? labelFontWeight;
  final double? labelFontSize;
  final Widget? tooltip;
  // final bool hasClearIcon;

  InputDecoration _customDecorations(BuildContext context) {
    return InputDecoration(
      filled: true,
      hintText: hintText,
      contentPadding: contentPadding ??
          const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      suffixIcon: suffixWidget,
      isDense: true,
      prefixIcon: prefixWidget,
      prefixIconConstraints: const BoxConstraints(),
      fillColor: Colors.white,
      hintStyle: Theme.of(context).textTheme.bodyLarge!.copyWith(
          fontWeight: FontWeight.normal,
          color: isValidateAfterSave == true
              ? Theme.of(context).colorScheme.error
              : Colors.grey.shade600),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
            color: Theme.of(context)
                .colorScheme
                .onSurfaceVariant
                .withValues(alpha: 0.4)),
        borderRadius: const BorderRadius.all(Radius.circular(3)),
      ),
      disabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
            color: Theme.of(context)
                .colorScheme
                .onSurfaceVariant
                .withValues(alpha: 0.4),
            width: 0.8),
        borderRadius: const BorderRadius.all(Radius.circular(3)),
      ),
      enabledBorder: isEnabled
          ? OutlineInputBorder(
              borderSide: BorderSide(
                  color: isValidateAfterSave == true
                      ? Theme.of(context).colorScheme.error
                      : Theme.of(context)
                          .colorScheme
                          .onSurfaceVariant
                          .withValues(alpha: 0.4)),
              borderRadius: const BorderRadius.all(Radius.circular(3)),
            )
          : OutlineInputBorder(
              borderSide: BorderSide(
                  color: isValidateAfterSave == true
                      ? Theme.of(context).colorScheme.error
                      : Theme.of(context)
                          .colorScheme
                          .onSurfaceVariant
                          .withValues(alpha: 0.4)),
              borderRadius: const BorderRadius.all(Radius.circular(3)),
            ),
      errorBorder: OutlineInputBorder(
        borderSide:
            BorderSide(color: Theme.of(context).colorScheme.error, width: 0.8),
        borderRadius: const BorderRadius.all(Radius.circular(3)),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderSide: BorderSide(
            color: Theme.of(context)
                .colorScheme
                .onSurfaceVariant
                .withValues(alpha: 0.4),
            width: 0.8),
        borderRadius: const BorderRadius.all(Radius.circular(3)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    String? defaultValidation(String? value) =>
        value!.isEmpty ? tr('required') : null;

    return Container(
      padding: padding,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          if (labelText != null)
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: labelText,
                    style: customStyle ??
                        Theme.of(context).textTheme.bodyLarge!.copyWith(
                            letterSpacing: 1.0,
                            color: labelColor,
                            fontWeight: labelFontWeight ?? FontWeight.bold,
                            fontSize: labelFontSize ?? 18),
                  ),
                  if (isRequired)
                    TextSpan(
                      text: '*',
                      style: Theme.of(context)
                          .textTheme
                          .bodyLarge!
                          .copyWith(letterSpacing: 1.0, color: Colors.red),
                    ),
                  WidgetSpan(
                      alignment: PlaceholderAlignment.middle,
                      child: tooltip ?? const SizedBox.shrink()),
                ],
              ),
            ),
          if (labelText != null) SmartGaps.gapH7,
          if (isMultiline)
            Row(
              children: [
                leftWidget ?? const SizedBox.shrink(),
                Expanded(
                  flex: flexTextField,
                  child: TextFormField(
                    onTap: onTap,
                    enabled: enabled,
                    maxLines: maxLines,
                    minLines: minLines,
                    readOnly: readOnly,
                    onChanged: onChanged,
                    focusNode: focusNode,
                    autofocus: autofocus,
                    controller: controller,
                    obscureText: obscureText,
                    inputFormatters: inputFormatters,
                    textInputAction: TextInputAction.newline,
                    validator: validation ?? defaultValidation,
                    autovalidateMode: isAutovalidate == true
                        ? AutovalidateMode.onUserInteraction
                        : AutovalidateMode.disabled,
                    keyboardType: keyboardType ?? TextInputType.multiline,
                    style: TextStyle(color: inputTextColor),
                    decoration: _customDecorations(context),
                    textAlign: textAlign ?? TextAlign.justify,
                  ),
                ),
                rightWidget ?? const SizedBox.shrink(),
              ],
            )
          else
            Row(
              children: [
                leftWidget ?? const SizedBox.shrink(),
                Expanded(
                  flex: flexTextField,
                  child: TextFormField(
                    onTap: onTap,
                    enabled: enabled,
                    readOnly: readOnly,
                    onChanged: onChanged,
                    focusNode: focusNode,
                    autofocus: autofocus,
                    controller: controller,
                    inputFormatters: inputFormatters,
                    obscureText: obscureText,
                    validator: validation ?? defaultValidation,
                    autovalidateMode: isAutovalidate == true
                        ? AutovalidateMode.onUserInteraction
                        : AutovalidateMode.disabled,
                    keyboardType: keyboardType ?? TextInputType.text,
                    style: TextStyle(color: inputTextColor),
                    decoration: _customDecorations(context),
                    textAlign: textAlign ?? TextAlign.justify,
                  ),
                ),
                rightWidget ?? const SizedBox.shrink(),
              ],
            ),
          if (showInfo)
            Container(
              padding: const EdgeInsets.only(top: 5),
              child: Text(infoText ?? '',
                  style: Theme.of(context)
                      .textTheme
                      .bodyLarge!
                      .copyWith(color: Colors.blueGrey[200], height: 1.2)),
            )
          else
            Container(),
        ],
      ),
    );
  }
}
