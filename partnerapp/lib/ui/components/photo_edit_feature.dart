// ignore_for_file: invalid_return_type_for_catch_error

import 'dart:convert';
import 'dart:io';

import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_xlider/flutter_xlider.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_cropper/image_cropper.dart';

enum PhotoSettings {
  contrast,
  saturation,
  brightness,
  vibrance,
  autoEnhance,
}

enum PhotoState {
  cropped,
  edited,
  enhanced,
  original,
  cropping,
}

class PhotoEditor extends StatefulWidget {
  const PhotoEditor(
      {super.key,
      this.aspectRatio,
      required this.onEditUrl,
      required this.onEditPhoto,
      required this.photoUrl,
      required this.originalPhoto});

  final double? aspectRatio;
  final Function(File) onEditPhoto;
  final String photoUrl;
  final File originalPhoto;
  final Function(String?) onEditUrl;

  @override
  PhotoEditorViewState createState() => PhotoEditorViewState();
}

class PhotoEditorViewState extends State<PhotoEditor> {
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();

  double? photoAspectRatio;
  PhotoState? state;
  int? _selectedIndex;
  String? editedPhoto;
  late File croppedPhoto;
  late bool viewOriginal;
  late bool isAutoEnhanced;

  Map<PhotoSettings, int> editable = {
    PhotoSettings.contrast: 0,
    PhotoSettings.saturation: 0,
    PhotoSettings.brightness: 0,
    PhotoSettings.vibrance: 0,
  };

  Map<PhotoSettings, String> commands = {
    PhotoSettings.contrast: 'e_contrast',
    PhotoSettings.saturation: 'e_saturation',
    PhotoSettings.brightness: 'e_brightness',
    PhotoSettings.vibrance: 'e_vibrance',
    PhotoSettings.autoEnhance: 'e_viesus_correct',
  };

  bool? isApplying;

  @override
  void initState() {
    isAutoEnhanced = false;
    editedPhoto = widget.photoUrl;
    croppedPhoto = widget.originalPhoto;
    _selectedIndex = 0;
    isApplying = false;
    viewOriginal = false;
    super.initState();
  }

  void resetImage() {
    setState(() {
      editedPhoto = widget.photoUrl;
      state = PhotoState.original;
    });
  }

  void resetEditableValues() {
    setState(() {
      editable[PhotoSettings.brightness] = 0;
      editable = {
        PhotoSettings.contrast: 0,
        PhotoSettings.saturation: 0,
        PhotoSettings.brightness: 0,
        PhotoSettings.vibrance: 0,
      };
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
            onTap: () => Navigator.of(context).pop(),
            child: Center(
                child: Text(tr('back'),
                    style: TextStyle(
                        color:
                            Theme.of(context).colorScheme.onSurfaceVariant)))),
        backgroundColor: Colors.white,
        title: Text(tr('edit_photo'),
            style: TextStyle(
                color: Theme.of(context).colorScheme.onSurfaceVariant)),
        centerTitle: true,
        actions: <Widget>[
          Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: GestureDetector(
                onTap: resetImage,
                child: Icon(
                  Icons.restore,
                  color: Theme.of(context).colorScheme.onSurfaceVariant,
                ),
              )),
          Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: GestureDetector(
                onTap: _save,
                child: Icon(
                  Icons.check_circle,
                  color: Theme.of(context).colorScheme.onSurfaceVariant,
                ),
              )),
        ],
      ),
      body: SizedBox(
        height: double.infinity,
        child: Column(
          children: <Widget>[
            getPhoto(),
            Expanded(
              child: Container(
                child: viewButtonsRow(),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        currentIndex: _selectedIndex ?? 0,
        unselectedItemColor: Colors.grey,
        onTap: (value) async {
          switch (value) {
            case 0:
              if (state != PhotoState.cropping) _cropImage();
              break;
            case 1:
              showModalBottomSheet(
                enableDrag: false,
                isDismissible: true,
                context: context,
                backgroundColor: Colors.black54,
                builder: (context) => BottomModalSheetWidget(
                  onLoad: (newValues) {
                    setState(() {
                      editable = newValues;
                      isApplying = true;
                    });
                    applyChanges();
                  },
                  settings: editable,
                ),
              );
              break;
          }
        },
        items: [
          BottomNavigationBarItem(
            label: 'Crop',
            icon: Icon(
              FontAwesomeIcons.crop,
              size: 13,
              color: Theme.of(context).colorScheme.onSurfaceVariant,
            ),
            // title: Text(tr('crop'),
            //     style: TextStyle(color: Theme.of(context).colorScheme.onSurfaceVariant)),
          ),
          BottomNavigationBarItem(
            label: 'Adjust',
            icon: Icon(
              Icons.adjust,
              size: 13,
              color: Theme.of(context).colorScheme.onSurfaceVariant,
            ),
            // title: Text(
            //   tr('edit'),
            //   style: TextStyle(color: Theme.of(context).colorScheme.onSurfaceVariant),
            // ),
          )
        ],
      ),
    );
  }

  Widget viewButtonsRow() {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          viewButton(
              onTap: _showOriginal,
              label: tr('view_original'),
              icon: Icon(
                Icons.visibility,
                size: 30,
                color: viewOriginal
                    ? Theme.of(context).colorScheme.primary
                    : Theme.of(context).colorScheme.onSurfaceVariant,
              )),
          viewButton(
              onTap: _enhanceImage,
              label: tr('auto_enhance'),
              icon: Icon(
                Icons.ac_unit,
                color: isAutoEnhanced
                    ? Theme.of(context).colorScheme.primary
                    : Theme.of(context).colorScheme.onSurfaceVariant,
                size: 30,
              )),
        ],
      ),
    );
  }

  void _enhanceImage() {
    setState(() {
      isAutoEnhanced = !isAutoEnhanced;
    });
    applyChanges();
  }

  void _showOriginal() {
    setState(() {
      viewOriginal = !viewOriginal;
    });
  }

  Widget viewButton({Function()? onTap, required String label, Icon? icon}) {
    return Container(
      padding: const EdgeInsets.all(5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(60),
                  border: Border.all(color: Colors.black12)),
              child: ClipOval(
                child: Material(
                  color: Colors.white,
                  child: InkWell(
                    splashColor: Theme.of(context).colorScheme.primary,
                    onTap: onTap,
                    child: SizedBox(width: 66, height: 66, child: icon),
                  ),
                ),
              )),
          SmartGaps.gapH10,
          Text(
            label,
            style: TextStyle(
                color: Theme.of(context).colorScheme.onSurfaceVariant,
                fontSize: 12),
          ),
        ],
      ),
    );
  }

  Widget getSaveButton() {
    return Container(
      width: double.infinity,
      height: 60,
      padding: const EdgeInsets.all(10),
      child: CustomDesignTheme.flatButtonStyle(
        onPressed: _save,
        child: Text(tr('save'),
            style: const TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );
  }

  Future<void> _save() async {
    if (editedPhoto != null) {
      showLoadingDialog(context, loadingKey);

      // String? base64 = await Formatter.networkImageToBase64(editedPhoto)
      //     .catchError((_) => _closeLoadingPopup());
      File file = await Formatter.returnFileFromString(
              prename: 'profile',
              uniqueId: DateTime.now().toString(),
              encodedStr: editedPhoto!)
          .catchError((_) => _closeLoadingPopup());

      Navigator.of(loadingKey.currentContext ?? context).pop();

      widget.onEditUrl(editedPhoto);
      widget.onEditPhoto(file);
      if (mounted) {
        Navigator.of(context).pop(file);
      }
    }
  }

  void _closeLoadingPopup() {
    Navigator.of(loadingKey.currentContext!).pop();
  }

  Widget getPhoto() {
    return widget.photoUrl.isEmpty
        ? Container()
        : Container(
            child:
                _blurHashImage(viewOriginal ? widget.photoUrl : editedPhoto!),
          );
  }

  Widget _blurHashImage(String viewUrl) {
    return SizedBox(
      width: double.infinity,
      height: MediaQuery.of(context).size.height * 0.60,
      child: Image.memory(
        base64Decode(viewUrl),
        // placeholder: (context, url) =>
        //     const BlurHash(hash: 'LEHV6nWB2yk8pyo0adR*.7kCMdnj'),
        // errorWidget: (context, url, error) => const Icon(Icons.error),
        // imageUrl: viewUrl,
        // fit: BoxFit.cover,
      ),
    );
  }

  Future<void> _cropImage() async {
    setState(() {
      state = PhotoState.cropping;
    });

    var fileToBeCropped =
        await Formatter.compressImageToRightSize(widget.originalPhoto);
    if (mounted) {
      CroppedFile? croppedFile0 = await ImageCropper().cropImage(
        sourcePath: fileToBeCropped.path,
        aspectRatio: const CropAspectRatio(ratioX: 4, ratioY: 3),
        uiSettings: [
          AndroidUiSettings(
              toolbarTitle: tr('crop_image'),
              toolbarColor: Colors.black,
              toolbarWidgetColor: Colors.white,
              initAspectRatio: CropAspectRatioPreset.ratio4x3,
              backgroundColor: Colors.white,
              lockAspectRatio: false),
          IOSUiSettings(
            title: tr('crop_image'),
          )
        ],
      );

      if (croppedFile0 != null) {
        File? croppedFile = File.fromUri(Uri(path: croppedFile0.path));

        setState(() {
          state = PhotoState.cropped;
        });

        if (mounted) {
          croppedPhoto = croppedFile;
          final base64 = base64Encode(await croppedPhoto.readAsBytes());
          setState(() {
            editedPhoto = base64;
          });
        }
      }
    }
  }

  void applyChanges() async {
    setState(() {
      // editedPhoto = getTransformationString(editedPhoto!);
      isApplying = false;
    });
  }

  String generateTransformationString(String originalurl) {
    String fullString = '';

    if (editable[PhotoSettings.saturation] != 0) {
      originalurl.indexOf(commands[PhotoSettings.saturation]!);

      fullString = fullString + getSpecificValues(PhotoSettings.saturation);
    }

    if (editable[PhotoSettings.brightness] != 0) {
      fullString = fullString + getSpecificValues(PhotoSettings.brightness);
    }

    if (editable[PhotoSettings.contrast] != 0) {
      fullString = fullString + getSpecificValues(PhotoSettings.contrast);
    }

    if (editable[PhotoSettings.vibrance] != 0) {
      fullString = fullString + getSpecificValues(PhotoSettings.vibrance);
    }

    if (isAutoEnhanced) {
      fullString = fullString + automaticEnhanceImage();
    }

    return fullString;
  }

  String getSpecificValues(PhotoSettings key) {
    return '/${commands[key]}:${editable[key]}';
  }

  String automaticEnhanceImage() {
    return '/e_viesus_correct';
  }
}

class BottomModalSheetWidget extends StatefulWidget {
  const BottomModalSheetWidget({super.key, this.settings, this.onLoad});

  final Map<PhotoSettings, int>? settings;
  final Function(Map<PhotoSettings, int> newValues)? onLoad;

  @override
  BottomModalSheetWidgetState createState() => BottomModalSheetWidgetState();
}

class BottomModalSheetWidgetState extends State<BottomModalSheetWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 335,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(15)),
      ),
      child: SingleChildScrollView(
        child: TrackbarList(
          settings: widget.settings,
          onLoad: (newValues) {
            widget.onLoad!(widget.settings!);
          },
        ),
      ),
    );
  }
}

class TrackbarList extends StatefulWidget {
  const TrackbarList({super.key, this.settings, this.onLoad});

  final Map<PhotoSettings, int>? settings;
  final Function(Map<PhotoSettings, int?>? newValues)? onLoad;

  @override
  TrackbarListState createState() => TrackbarListState();
}

class TrackbarListState extends State<TrackbarList> {
  Map<PhotoSettings, int?>? editableSettings;

  @override
  void initState() {
    editableSettings = widget.settings ??
        {
          PhotoSettings.contrast: 0,
          PhotoSettings.saturation: 0,
          PhotoSettings.brightness: 0,
          PhotoSettings.vibrance: 0,
        };
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return sliderWrapper(context);
  }

  Widget sliderWrapper(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            CustomDesignTheme.flatButtonStyle(
              onPressed: resetEditableValues,
              child: Text(
                tr('reset'),
                style: TextStyle(
                    color: Theme.of(context).colorScheme.onSurfaceVariant),
              ),
            ),
            CustomDesignTheme.flatButtonStyle(
              onPressed: () async {
                widget.onLoad!(editableSettings);
                Navigator.of(context).pop();
              },
              child: Text(
                tr('done'),
                style: TextStyle(
                    color: Theme.of(context).colorScheme.onSurfaceVariant),
              ),
            ),
          ],
        ),
        getSliders(),
      ],
    );
  }

  Widget getSliders() {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          getTrackbars(PhotoSettings.brightness),
          getTrackbars(PhotoSettings.contrast),
          getTrackbars(PhotoSettings.saturation),
          getTrackbars(PhotoSettings.vibrance),
        ],
      ),
    );
  }

  Widget getTrackbars(PhotoSettings key) {
    return Container(
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            getTrackLabel(key),
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Theme.of(context).colorScheme.onSurfaceVariant),
          ),
          SmartGaps.gapH5,
          FlutterSlider(
            handlerWidth: 10,
            handlerHeight: 10,
            values: [editableSettings![key]!.toDouble()],
            max: 100,
            min: -100,
            onDragCompleted: (handlerIndex, lowerValue, upperValue) {
              setState(() {
                editableSettings![key] =
                    lowerValue is int ? lowerValue : lowerValue.round() as int?;
              });
            },
            handler: FlutterSliderHandler(
              decoration: const BoxDecoration(),
              child: Material(
                type: MaterialType.canvas,
                color: Theme.of(context).colorScheme.primary,
                elevation: 3,
                child: Container(
                  padding: const EdgeInsets.all(5),
                ),
              ),
            ),
            trackBar: FlutterSliderTrackBar(
              inactiveTrackBar: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: Colors.black12,
              ),
              activeTrackBar: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Theme.of(context)
                      .colorScheme
                      .primary
                      .withValues(alpha: 0.5)),
            ),
          ),
        ],
      ),
    );
  }

  String getTrackLabel(PhotoSettings key) {
    switch (key) {
      case PhotoSettings.brightness:
        return tr('brightness');
      case PhotoSettings.contrast:
        return tr('contrast');
      case PhotoSettings.saturation:
        return tr('saturation');
      case PhotoSettings.vibrance:
        return tr('vibrance');
      default:
        return '';
    }
  }

  void resetEditableValues() {
    setState(() {
      editableSettings![PhotoSettings.brightness] = 0;
      editableSettings![PhotoSettings.saturation] = 0;
      editableSettings![PhotoSettings.contrast] = 0;
      editableSettings![PhotoSettings.vibrance] = 0;
    });
  }
}
