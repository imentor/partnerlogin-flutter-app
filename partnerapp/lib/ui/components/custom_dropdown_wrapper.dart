import 'package:flutter/material.dart';

class CustomDropDownWrapper extends StatelessWidget {
  const CustomDropDownWrapper(
      {super.key, required this.widget, required this.decoration, required this.padding});

  final Widget widget;
  final Decoration decoration;
  final EdgeInsetsGeometry padding;

  @override
  Widget build(BuildContext context) {
    return Container(decoration: decoration, padding: padding, child: widget);
  }
}
