import 'dart:async';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

Future<bool> showSuccessAnimationDialog(
  BuildContext context,
  bool isSuccessful,
  String? message,
) async {
  Dialog simpleDialog = Dialog(
    child: SingleChildScrollView(
      physics: const AlwaysScrollableScrollPhysics(),
      child: Container(
        margin:
            const EdgeInsets.only(left: 20.0, right: 20.0, top: 40, bottom: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            isSuccessful
                ? Lottie.asset(
                    'assets/animation/success.json',
                  )
                : Icon(
                    ElementIcons.circle_close,
                    color: Theme.of(context).colorScheme.error,
                    size: 48,
                  ),
            SmartGaps.gapH20,
            Text(
              isSuccessful && (message ?? '').isEmpty
                  ? tr('changes_saved')
                  : (message ?? ''),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                  fontWeight: FontWeight.w600,
                  color: Theme.of(context).colorScheme.onSurfaceVariant),
            ),
            SmartGaps.gapH20,
            SizedBox(
              width: 150,
              height: 60,
              child: CustomDesignTheme.flatButtonStyle(
                key: const Key(Keys.successBackButton),
                backgroundColor: Theme.of(context).colorScheme.onSurfaceVariant,
                onPressed: () {
                  if (Navigator.of(context, rootNavigator: true).canPop()) {
                    Navigator.of(context, rootNavigator: true).pop(true);
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(tr('close'),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(color: Colors.white)),
                    SmartGaps.gapW10,
                    const Icon(
                      ElementIcons.right,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
  await showDialog(
      context: context, builder: (BuildContext context) => simpleDialog);
  return true;
}

Future<void> showLoadingDialog(BuildContext context, GlobalKey? key,
    {String? message}) async {
  return await showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return PopScope(
          canPop: false,
          child: SimpleDialog(
              key: key,
              backgroundColor: Colors.white,
              children: <Widget>[
                SizedBox(
                  height: 200,
                  child: Center(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  Theme.of(context)
                                      .colorScheme
                                      .onSurfaceVariant)),
                          SmartGaps.gapW10,
                          Text(
                            message ?? "${tr('please_wait')}....",
                            style: TextStyle(
                                color: Theme.of(context)
                                    .colorScheme
                                    .onSurfaceVariant),
                          )
                        ]),
                  ),
                )
              ]),
        );
      });
}

Future<void> showErrorDialog(BuildContext context, String headline,
    String message, String buttonText) async {
  Dialog simpleDialog = Dialog(
    child: SingleChildScrollView(
      physics: const AlwaysScrollableScrollPhysics(),
      child: Container(
        margin:
            const EdgeInsets.only(left: 20.0, right: 20.0, top: 20, bottom: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Icon(
              ElementIcons.warning_outline,
              color: Colors.red,
              size: 64,
            ),
            SmartGaps.gapH10,
            Text(
              headline,
              style: Theme.of(context).textTheme.headlineLarge,
            ),
            SmartGaps.gapH10,
            Text(
              message,
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            SmartGaps.gapH30,
            SizedBox(
              child: CustomDesignTheme.flatButtonStyle(
                backgroundColor: Theme.of(context).colorScheme.onSurfaceVariant,
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(buttonText,
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(color: Colors.white, height: 1)),
                ),
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pop();
                },
              ),
            ),
          ],
        ),
      ),
    ),
  );
  await showDialog(
      context: context, builder: (BuildContext context) => simpleDialog);
}

Future<bool?> showSuccessDialog(
    BuildContext context, bool isSuccessful, String? message) async {
  Dialog simpleDialog = Dialog(
    child: SingleChildScrollView(
      physics: const AlwaysScrollableScrollPhysics(),
      child: Container(
        margin:
            const EdgeInsets.only(left: 20.0, right: 20.0, top: 20, bottom: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            isSuccessful
                ? Lottie.asset(
                    'assets/animation/success.json',
                  )
                : const Icon(
                    Icons.close,
                    color: Colors.red,
                    size: 50,
                  ),
            SmartGaps.gapH5,
            Text(
              isSuccessful && (message ?? '').isEmpty
                  ? 'Success'
                  : (message ?? ''),
              style: TextStyle(
                  color: Theme.of(context).colorScheme.onSurfaceVariant,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              // width: 60,
              child: CustomDesignTheme.flatButtonStyle(
                backgroundColor: Theme.of(context).colorScheme.primary,
                child: Text(tr('close'),
                    style: const TextStyle(color: Colors.white)),
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pop(true);
                },
              ),
            ),
          ],
        ),
      ),
    ),
  );
  return await showDialog(
      context: context, builder: (BuildContext context) => simpleDialog);
}

Future<void> showCustomDialog({
  required BuildContext context,
  Function()? cancelFunction,
  required Function() continueFunction,
  Icon? contentIcon,
  required String contentText,
  String? cancelButtonLabel,
  String? continueButtonLabel,
}) async {
  await showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        backgroundColor: Colors.white,
        titlePadding: EdgeInsets.zero,
        title: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  const Spacer(),
                  IconButton(
                    onPressed: cancelFunction ?? () => Navigator.pop(context),
                    icon: Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          width: 23,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black.withValues(alpha: 0.4),
                              width: 2,
                            ),
                            shape: BoxShape.circle,
                          ),
                        ),
                        Icon(
                          Icons.close,
                          size: 18,
                          color: Colors.black.withValues(alpha: 0.4),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        content: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            contentIcon ??
                Icon(
                  Icons.error,
                  color: Colors.yellow[700],
                  size: 30,
                ),
            SmartGaps.gapW5,
            Expanded(
              child: Text(
                contentText,
                textAlign: TextAlign.start,
                style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      color: Theme.of(context).colorScheme.onTertiaryContainer,
                    ),
              ),
            ),
          ],
        ),
        actionsOverflowDirection: VerticalDirection.down,
        actionsPadding:
            const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        actions: [
          TextButton(
            onPressed: () {
              if (cancelFunction != null) {
                cancelFunction.call();
              }
              Navigator.pop(context);
            },
            style: TextButton.styleFrom(
              backgroundColor: PartnerAppColors.red.withValues(alpha: 0.5),
            ),
            child: Text(
              cancelButtonLabel ?? tr('no'),
              style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                  ),
            ),
          ),
          TextButton(
            onPressed: () {
              continueFunction.call();
              Navigator.pop(context);
            },
            style: TextButton.styleFrom(
              backgroundColor: PartnerAppColors.green.withValues(alpha: 0.5),
            ),
            child: Text(
              continueButtonLabel ?? tr('yes'),
              style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                  ),
            ),
          ),
        ],
      );
    },
  );
}
