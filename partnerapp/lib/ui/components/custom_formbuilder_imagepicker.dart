import 'dart:io';

import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/get_image_with_custom_picker.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class CustomFormBuilderImagePicker extends StatefulWidget {
  final String attribute;
  final List<FormFieldValidator> validators;
  final List? initialValue;
  final bool readOnly;
  @Deprecated('Set the `labelText` within decoration attribute')
  final String? labelText;
  final InputDecoration decoration;
  final ValueTransformer? valueTransformer;
  final ValueChanged? onChanged;
  final FormFieldSetter? onSaved;

  final double imageWidth;
  final double imageHeight;
  final EdgeInsets? imageMargin;
  final Color? iconColor;

  //customs
  final bool useOriginalImagePicker; //set to true to use a custom button
  final Icon?
      originalIcon; //custom icon for the original form build image picker
  final Icon? bottomModalIconCamera; //Icon for camera
  final Icon? bottomModalIconGallery; //Icon for gallery
  final Widget? modalCameraLabel; //Label after Icon for camera
  final Widget? modalGalleryLabel; // label after icon for gallery
  final Icon? cameraPickerIcon; //Icon for the main camera picker icon
  final Color? disabledIconColor;
  final Color? disabledContainerColor;
  final bool enableDottedBorder;
  final Color? dottedBorderColor;

  //for use if useOriginalImagePicker is set to false
  //customize flatbutton custom properties
  final Widget? customButtonChild; //child widget inside the flatbutton
  final String? customButtonImagePath; //image for button
  final Widget? customButtonLabel; // label after the image
  final ShapeBorder? customButtonShape;
  final Color? customButtonSplashColor;
  final EdgeInsetsGeometry? customButtonPadding;
  final double? customButtonHeight;
  final double? customButtonWidth;
  final Color? customButtonColor;
  final double? customButtonImageHeight;
  final double? customButtonImageWidth;
  final EdgeInsetsGeometry? customButtonMargin;

  const CustomFormBuilderImagePicker({
    super.key,
    required this.attribute,
    this.initialValue,
    this.validators = const [],
    this.valueTransformer,
    this.labelText,
    this.onChanged,
    this.imageWidth = 130,
    this.imageHeight = 130,
    this.imageMargin,
    this.readOnly = false,
    this.onSaved,
    this.decoration = const InputDecoration(),
    this.iconColor,
    this.useOriginalImagePicker = true,
    this.originalIcon,
    this.modalCameraLabel,
    this.modalGalleryLabel,
    this.bottomModalIconCamera,
    this.bottomModalIconGallery,
    this.cameraPickerIcon,
    this.customButtonChild,
    this.customButtonImagePath,
    this.customButtonLabel,
    this.customButtonPadding,
    this.customButtonShape,
    this.customButtonSplashColor,
    this.disabledContainerColor,
    this.disabledIconColor,
    this.dottedBorderColor,
    this.enableDottedBorder = true,
    this.customButtonColor,
    this.customButtonHeight,
    this.customButtonImageHeight,
    this.customButtonImageWidth,
    this.customButtonWidth,
    this.customButtonMargin,
  });

  @override
  CustomFormBuilderImagePickerState createState() =>
      CustomFormBuilderImagePickerState();
}

class CustomFormBuilderImagePickerState
    extends State<CustomFormBuilderImagePicker> {
  bool _readOnly = false;
  List? _initialValue;
  final GlobalKey<FormBuilderState> _fieldKey = GlobalKey<FormBuilderState>();
  FormBuilderState? _formState;
  //late dynamic _formState;

  @override
  void initState() {
    _formState = FormBuilder.of(context);
    _initialValue = List.of(widget.initialValue ?? []);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!_formState!.enabled) {
      _readOnly = true;
    } else {
      _readOnly = widget.readOnly;
    }

    return FormBuilderField<List>(
      name: widget.attribute,
      key: _fieldKey,
      enabled: !_readOnly,
      initialValue: _initialValue,
      validator: (val) {
        for (int i = 0; i < widget.validators.length; i++) {
          if (widget.validators[i](val) != null) {
            return widget.validators[i](val);
          }
        }
        return null;
      },
      onSaved: (val) {
        dynamic transformed;
        if (widget.valueTransformer != null) {
          transformed = widget.valueTransformer!(val);
          _formState?.setInternalFieldValue(
            widget.attribute,
            transformed,
          );
        } else {
          _formState?.setInternalFieldValue(
            widget.attribute,
            val,
          );
        }
        if (widget.onSaved != null) {
          widget.onSaved!(transformed ?? val);
        }
      },
      builder: (field) {
        return InputDecorator(
          decoration: widget.decoration.copyWith(
            enabled: !_readOnly,
            errorText: field.errorText,
            // ignore: deprecated_member_use_from_same_package
            labelText: widget.decoration.labelText ?? widget.labelText,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              getDefaultCustomButton(field),
              SmartGaps.gapH8,
              SizedBox(
                height: widget.useOriginalImagePicker
                    ? widget.imageHeight
                    : field.value!.isEmpty
                        ? 0
                        : widget.imageHeight,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: field.value!.map<Widget>((item) {
                    return Stack(
                      alignment: Alignment.topRight,
                      children: <Widget>[
                        Container(
                          width: widget.imageWidth,
                          height: widget.imageHeight,
                          margin: widget.imageMargin,
                          child: item is String
                              ? Image.network(item, fit: BoxFit.cover)
                              : Image.file(item as File, fit: BoxFit.cover),
                        ),
                        if (!_readOnly)
                          InkWell(
                            onTap: () {
                              field.didChange([...field.value!]..remove(item));
                            },
                            child: Container(
                              margin: const EdgeInsets.all(3),
                              decoration: BoxDecoration(
                                color: Colors.grey.withValues(alpha: .7),
                                shape: BoxShape.circle,
                              ),
                              alignment: Alignment.center,
                              height: 22,
                              width: 22,
                              child: const Icon(
                                Icons.close,
                                size: 18,
                                color: Colors.white,
                              ),
                            ),
                          ),
                      ],
                    );
                  }).toList()
                    ..add(originalPicker(field)),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget getDefaultCustomButton(FormFieldState<List<dynamic>> field) {
    return widget.useOriginalImagePicker
        ? Container()
        : widget.enableDottedBorder
            ? Container(
                margin: widget.customButtonMargin ?? const EdgeInsets.all(10),
                child: DottedBorder(
                  color: widget.dottedBorderColor ??
                      Theme.of(context).colorScheme.primary,
                  strokeWidth: 1,
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    height: 80,
                    width: double.infinity,
                    child: defaultButton(field),
                  ),
                ),
              )
            : Container(
                margin: widget.customButtonMargin,
                width: widget.customButtonWidth ?? double.infinity,
                height: widget.customButtonHeight ?? 70,
                child: defaultButton(field),
              );
  }

  Widget defaultButton(FormFieldState<List<dynamic>> field) {
    return CustomDesignTheme.flatButtonStyle(
      backgroundColor: widget.customButtonColor,
      padding: widget.customButtonPadding,
      foregroundColor: widget.customButtonSplashColor,
      shape: widget.customButtonShape as OutlinedBorder?,
      onPressed: _readOnly
          ? null
          : () async {
              File? file = await getImage(context);
              if (file != null) {
                field.didChange([...field.value!, file]);
              }
            },
      child: widget.customButtonChild ??
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                widget.customButtonImagePath ?? ImagePaths.picturePrimaryColor,
                height: widget.customButtonImageHeight,
                width: widget.customButtonImageWidth,
              ),
              SmartGaps.gapW5,
              widget.customButtonLabel ??
                  RichText(
                    maxLines: 2,
                    text: TextSpan(
                      text: '',
                      style: TextStyle(
                          color: Theme.of(context).colorScheme.primary),
                      children: <TextSpan>[
                        TextSpan(
                            text: tr('click_to_select_files'),
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Theme.of(context).colorScheme.primary)),
                      ],
                    ),
                  ),
            ],
          ),
    );
  }

  Widget originalPicker(FormFieldState<List<dynamic>> field) {
    return widget.useOriginalImagePicker
        ? GestureDetector(
            onTap: _readOnly
                ? null
                : () async {
                    File? file = await getImage(context);
                    if (file != null) {
                      field.didChange([...field.value!, file]);
                    }
                  },
            child: Container(
              width: widget.imageWidth,
              height: widget.imageHeight,
              color: (_readOnly
                      ? widget.disabledContainerColor ??
                          Theme.of(context).disabledColor
                      : widget.iconColor ?? Theme.of(context).primaryColor)
                  .withAlpha(50),
              child: Icon(
                  widget.cameraPickerIcon as IconData? ?? Icons.camera_enhance,
                  color: _readOnly
                      ? widget.disabledIconColor ??
                          Theme.of(context).disabledColor
                      : widget.iconColor ?? Theme.of(context).primaryColor),
            ),
          )
        : Container();
  }

  Future getModalBottomSheet(FormFieldState<List<dynamic>> field) {
    return showModalBottomSheet(
      context: context,
      builder: (_) {
        return ImageSourceSheet(
          onImageSelected: (image) {
            field.didChange([...field.value!, image]);
            Navigator.of(context).pop();
          },
        );
      },
    );
  }
}

class ImageSourceSheet extends StatelessWidget {
  final Function(File) onImageSelected;
  final Icon? cameraIcon;
  final Icon? galleryIcon;
  final Widget? cameraLabel;
  final Widget? galleryLabel;

  const ImageSourceSheet(
      {super.key,
      required this.onImageSelected,
      this.cameraLabel,
      this.galleryLabel,
      this.galleryIcon,
      this.cameraIcon});

  void imageSelected(File image) async {
    onImageSelected(image);
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: <Widget>[
        ListTile(
          leading: cameraIcon ?? const Icon(Icons.camera_enhance),
          title: cameraLabel ?? const Text('Camera'),
          onTap: () async {
            File? file = await getImage(context);
            if (file != null) {
              imageSelected(file);
            }
          },
        ),
        ListTile(
          leading: galleryIcon ?? const Icon(Icons.image),
          title: galleryLabel ?? const Text('Gallery'),
          onTap: () async {
            File? file = await getImage(context);
            if (file != null) {
              imageSelected(file);
            }
          },
        )
      ],
    );
  }
}
