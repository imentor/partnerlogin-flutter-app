import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

Future<bool> deleteConfirmDialog(BuildContext context) async {
  final result = await showOkCancelAlertDialog(
      context: context,
      title: tr('confirm_delete'),
      message: tr('are_you_sure'),
      okLabel: tr('delete'));
  return result == OkCancelResult.ok;
}

Future<void> payproffVerifyDialog(
    {required BuildContext context, required VoidCallback verify}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.of(dialogContext).pop();
              },
              child: const Icon(Icons.close),
            )
          ],
        ),
        content: SingleChildScrollView(
          child: Column(
            children: [
              Text(tr('validate_payproff_account'),
                  style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                      fontSize: 22,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.bold)),
              SmartGaps.gapH20,
              TextButton(
                  style: TextButton.styleFrom(
                      backgroundColor: PartnerAppColors.darkBlue),
                  onPressed: () {
                    Navigator.of(dialogContext).pop();
                    verify();
                  },
                  child: Text(
                    tr('validate_account'),
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.normal),
                  ))
            ],
          ),
        ),
      );
    },
  );
}

Future<void> successDialog(
    {required BuildContext context,
    VoidCallback? exit,
    String successMessage = 'success'}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.of(dialogContext).pop();
                if (exit != null) {
                  exit();
                }
              },
              child: const Icon(Icons.close),
            )
          ],
        ),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: 100,
                  width: 100,
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: PartnerAppColors.malachite),
                  child: const Center(
                      child: Icon(
                    FeatherIcons.check,
                    color: Colors.white,
                    size: 90,
                  )),
                ),
                SmartGaps.gapH20,
                Text(
                  tr(successMessage),
                  style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                      fontSize: 18,
                      color: Theme.of(context).colorScheme.onSurfaceVariant),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}

Future<void> failedDialog(
    {required BuildContext context,
    VoidCallback? exit,
    String failedMessage = 'failed_v2'}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: const EdgeInsets.all(20),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(
                onTap: () {
                  Navigator.of(dialogContext).pop();
                  if (exit != null) {
                    exit();
                  }
                },
                child: const Icon(Icons.close),
              ),
            ],
          ),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    height: 100,
                    width: 100,
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle, color: PartnerAppColors.red),
                    child: const Center(
                      child: Icon(
                        FeatherIcons.x,
                        color: Colors.white,
                        size: 90,
                      ),
                    ),
                  ),
                  SmartGaps.gapH20,
                  Text(tr(failedMessage),
                      style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                            fontSize: 18,
                            color:
                                Theme.of(context).colorScheme.onSurfaceVariant,
                          )),
                  SmartGaps.gapH20,
                  Text(tr('failed_v2_message'),
                      style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                            fontSize: 18,
                            color:
                                Theme.of(context).colorScheme.onSurfaceVariant,
                          )),
                ],
              ),
            ),
          ),
        );
      });
}

Future<void> showLocationPermissionSettingsDialog(BuildContext context) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(tr('enable_location_access')),
        content: Text(
          '${tr('request_location_description')} '
          '${tr('request_location_settings')}',
        ),
        actions: [
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: Text(tr('cancel')),
          ),
          TextButton(
            onPressed: () async {
              Navigator.pop(context);
              openAppSettings();
            },
            child: Text(tr('open_settings')),
          ),
        ],
      );
    },
  );
}

Future<bool?> disabledDialog(
    {required BuildContext context,
    required String companyName,
    required String message}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '${tr('hey')} $companyName,',
                  style: Theme.of(context).textTheme.displayLarge!.copyWith(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: PartnerAppColors.darkBlue,
                      ),
                ),
                SmartGaps.gapH20,
                Text(message),
                SmartGaps.gapH20,
                CustomDesignTheme.flatButtonStyle(
                  height: 50,
                  backgroundColor: PartnerAppColors.darkBlue,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  onPressed: () {
                    openAppSettings();
                    Navigator.of(dialogContext).pop(true);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        tr('activate_now'),
                        style:
                            Theme.of(context).textTheme.displayLarge!.copyWith(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white,
                                ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}

Future<void> pdfViewerV2Dialog({
  required BuildContext context,
  required String pdfUrl,
}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
            insetPadding: const EdgeInsets.all(20),
            contentPadding: const EdgeInsets.all(20),
            title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
              InkWell(
                onTap: () {
                  Navigator.of(dialogContext).pop();
                },
                child: const Icon(
                  FeatherIcons.x,
                  color: PartnerAppColors.spanishGrey,
                ),
              )
            ]),
            content: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: SfPdfViewer.network(pdfUrl),
            ));
      });
}

Future<void> commonSuccessOrFailedDialog({
  required BuildContext context,
  String? successMessage,
  String? message,
  bool isSuccess = true,
  bool showTitle = true,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 65,
                  width: 65,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: isSuccess
                        ? PartnerAppColors.malachite
                        : PartnerAppColors.red,
                  ),
                  child: Center(
                    child: isSuccess
                        ? const Icon(
                            FeatherIcons.check,
                            color: Colors.white,
                            size: 50,
                          )
                        : const Icon(
                            FontAwesomeIcons.exclamation,
                            color: Colors.white,
                            size: 50,
                          ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                if (showTitle)
                  Text(
                    successMessage ?? tr('success'),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: Colors.black,
                        ),
                  ),
                if (message != null) ...[
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    message,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                          color: PartnerAppColors.spanishGrey,
                        ),
                  ),
                ]
              ],
            ),
          ),
        ),
      );
    },
  );
}

Future<void> commonWebViewDialog({
  required BuildContext context,
  required String webUrl,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: InAppWebView(
            initialSettings: InAppWebViewSettings(transparentBackground: true),
            initialUrlRequest: URLRequest(
              url: WebUri(webUrl),
            ),
          ),
        ),
      );
    },
  );
}
