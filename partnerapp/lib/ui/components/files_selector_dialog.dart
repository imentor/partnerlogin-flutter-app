import 'dart:io';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/get_image_with_custom_picker.dart';
import 'package:cunning_document_scanner/cunning_document_scanner.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:image_picker/image_picker.dart';

Future<List<File>?> filesSelectorDialog({
  required BuildContext context,
}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: const EdgeInsets.all(20),
          title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            InkWell(
              onTap: () {
                Navigator.of(dialogContext).pop();
              },
              child: const Icon(
                FeatherIcons.x,
                color: PartnerAppColors.spanishGrey,
              ),
            )
          ]),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: const SingleChildScrollView(
              child: FilesSelectorDialog(),
            ),
          ),
        );
      });
}

class FilesSelectorDialog extends StatefulWidget {
  const FilesSelectorDialog({super.key});

  @override
  State<FilesSelectorDialog> createState() => _FilesSelectorDialogState();
}

class _FilesSelectorDialogState extends State<FilesSelectorDialog> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        InkWell(
          onTap: () => onScan(),
          child: SizedBox(
            width: 80,
            child: Column(
              children: [
                Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: PartnerAppColors.blue,
                      ),
                      borderRadius: BorderRadius.circular(5)),
                  child: const Center(
                    child: Icon(
                      FeatherIcons.maximize,
                      color: PartnerAppColors.blue,
                      size: 35,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  'Scan',
                  style: Theme.of(context).textTheme.titleSmall,
                )
              ],
            ),
          ),
        ),
        InkWell(
          onTap: () => onCamera(),
          child: SizedBox(
            width: 80,
            child: Column(
              children: [
                Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: PartnerAppColors.blue,
                      ),
                      borderRadius: BorderRadius.circular(5)),
                  child: const Center(
                    child: Icon(
                      FeatherIcons.camera,
                      color: PartnerAppColors.blue,
                      size: 35,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  tr('camera'),
                  style: Theme.of(context).textTheme.titleSmall,
                )
              ],
            ),
          ),
        ),
        InkWell(
          onTap: () => onFileUpload(),
          child: SizedBox(
            width: 80,
            child: Column(
              children: [
                Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: PartnerAppColors.blue,
                      ),
                      borderRadius: BorderRadius.circular(5)),
                  child: const Center(
                    child: Icon(
                      FeatherIcons.upload,
                      color: PartnerAppColors.blue,
                      size: 35,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  tr('file_upload'),
                  style: Theme.of(context).textTheme.titleSmall,
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  void onScan() async {
    List<File> documents = [];
    final scans = await CunningDocumentScanner.getPictures();

    if (!mounted) return;

    for (var element in (scans ?? <String>[])) {
      documents.add(File(element));
    }

    Navigator.of(context).pop(documents);
  }

  void onCamera() async {
    List<File> documents = [];

    final cameras = await imagePicker.pickImage(
      source: ImageSource.camera,
    );

    if (!mounted) return;

    if (cameras != null) {
      documents.add(File(cameras.path));
      Navigator.of(context).pop(documents);
    }
  }

  void onFileUpload() async {
    List<File> documents = [];

    final files = await FilePicker.platform.pickFiles(allowMultiple: true);

    if (!mounted) return;

    for (var element in (files?.files ?? <PlatformFile>[])) {
      documents.add(File(element.path!));
    }

    Navigator.of(context).pop(documents);
  }
}
