import 'package:Haandvaerker.dk/theme.dart';
import 'package:flutter/material.dart';

mixin CustomDesignTheme {
  static BoxDecoration get coldBlueBorderShadows {
    return BoxDecoration(
      color: Colors.white,
      border: Border.all(color: PartnerAppColors.blue.withValues(alpha: 0.25)),
      borderRadius: BorderRadius.circular(4),
      boxShadow: [
        BoxShadow(
          color: Colors.black.withValues(alpha: .03),
          blurRadius: 8.0,
          spreadRadius: -1,
          offset: const Offset(0, 5),
        )
      ],
    );
  }

  static BoxDecoration get borderShadowsOnly {
    return BoxDecoration(
      color: Colors.white,
      border: Border.all(color: PartnerAppColors.blue.withValues(alpha: 0)),
      borderRadius: BorderRadius.circular(4),
      boxShadow: [
        BoxShadow(
          color: Colors.black.withValues(alpha: .08),
          blurRadius: 8.0,
          spreadRadius: -3,
          offset: const Offset(0, 5),
        )
      ],
    );
  }

  static BoxDecoration get greyBorderShadows {
    return BoxDecoration(
      color: Colors.white,
      border: Border.all(color: Colors.grey.withAlpha(50)),
      borderRadius: BorderRadius.circular(4),
      boxShadow: [
        BoxShadow(
          color: Colors.black.withValues(alpha: .08),
          blurRadius: 8.0,
          spreadRadius: -3,
          offset: const Offset(0, 5),
        )
      ],
    );
  }

  static BoxDecoration get greyBorderOnly {
    return BoxDecoration(
      color: Colors.white,
      border: Border.all(color: Colors.grey),
      borderRadius: BorderRadius.circular(4),
    );
  }

  static BoxDecoration get coldBlueBorder {
    return BoxDecoration(
      color: Colors.white,
      border: Border.all(color: PartnerAppColors.blue.withValues(alpha: 0.25)),
    );
  }

  static double get buttonHeight => 60;
  static double get buttonWidth => double.infinity;

  static TextButton flatButtonStyle({
    Key? key,
    EdgeInsetsGeometry? padding,
    double width = 88,
    double height = 36,
    Color? foregroundColor,
    Color? backgroundColor,
    Color? disabledForegroundColor,
    Color? disabledBackgroundColor,
    Color? shadowColor,
    Color? surfaceTintColor,
    Color? iconColor,
    Color? disabledIconColor,
    double? elevation,
    TextStyle? textStyle,
    Size? minimumSize,
    Size? fixedSize,
    Size? maximumSize,
    BorderSide? side,
    OutlinedBorder? shape,
    MouseCursor? enabledMouseCursor,
    MouseCursor? disabledMouseCursor,
    VisualDensity? visualDensity,
    MaterialTapTargetSize? tapTargetSize,
    Duration? animationDuration,
    bool? enableFeedback,
    AlignmentGeometry? alignment,
    InteractiveInkFeatureFactory? splashFactory,
    Color? primary,
    Color? onSurface,
    Function? onPressed,
    required Widget child,
  }) {
    padding ??= const EdgeInsets.symmetric(horizontal: 16.0);

    minimumSize ??= Size(width, height);

    final style = TextButton.styleFrom(
      minimumSize: minimumSize,
      backgroundColor: backgroundColor,
      padding: padding,
      foregroundColor: foregroundColor,
      disabledForegroundColor: disabledForegroundColor,
      disabledBackgroundColor: disabledBackgroundColor,
      shadowColor: shadowColor,
      surfaceTintColor: surfaceTintColor,
      iconColor: iconColor,
      disabledIconColor: disabledIconColor,
      elevation: elevation,
      textStyle: textStyle,
      fixedSize: fixedSize,
      maximumSize: maximumSize,
      side: side,
      shape: shape,
      enabledMouseCursor: enabledMouseCursor,
      disabledMouseCursor: disabledMouseCursor,
      visualDensity: visualDensity,
      tapTargetSize: tapTargetSize,
      animationDuration: animationDuration,
      enableFeedback: enableFeedback,
      alignment: alignment,
      splashFactory: splashFactory,
    );

    return TextButton(
        key: key,
        style: style,
        onPressed: onPressed as void Function()?,
        child: child);
  }

  static OutlinedButton outlineButtonStyle({
    Key? key,
    double width = 88,
    double height = 36,
    Color? foregroundColor,
    Color? backgroundColor,
    Color? disabledForegroundColor,
    Color? disabledBackgroundColor,
    Color? shadowColor,
    Color? surfaceTintColor,
    double? elevation,
    TextStyle? textStyle,
    EdgeInsetsGeometry? padding,
    Size? minimumSize,
    Size? fixedSize,
    Size? maximumSize,
    BorderSide? side,
    OutlinedBorder? shape,
    MouseCursor? enabledMouseCursor,
    MouseCursor? disabledMouseCursor,
    VisualDensity? visualDensity,
    MaterialTapTargetSize? tapTargetSize,
    Duration? animationDuration,
    bool? enableFeedback,
    AlignmentGeometry? alignment,
    InteractiveInkFeatureFactory? splashFactory,
    Color? primary,
    Color? onSurface,
    Function? onPressed,
    required Widget child,
  }) {
    padding ??= const EdgeInsets.symmetric(horizontal: 16.0);

    minimumSize ??= Size(width, height);

    final style = OutlinedButton.styleFrom(
      foregroundColor: foregroundColor,
      backgroundColor: backgroundColor,
      disabledForegroundColor: disabledForegroundColor,
      disabledBackgroundColor: disabledBackgroundColor,
      shadowColor: shadowColor,
      surfaceTintColor: surfaceTintColor,
      elevation: elevation,
      textStyle: textStyle,
      padding: padding,
      minimumSize: minimumSize,
      fixedSize: fixedSize,
      maximumSize: maximumSize,
      side: side,
      shape: shape,
      enabledMouseCursor: enabledMouseCursor,
      disabledMouseCursor: disabledMouseCursor,
      visualDensity: visualDensity,
      tapTargetSize: tapTargetSize,
      animationDuration: animationDuration,
      enableFeedback: enableFeedback,
      alignment: alignment,
      splashFactory: splashFactory,
    );

    return OutlinedButton(
        key: key,
        style: style,
        onPressed: onPressed as void Function()?,
        child: child);
  }
}
