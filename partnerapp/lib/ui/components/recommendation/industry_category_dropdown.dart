import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/ui/components/custom_dropdown_wrapper.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/recommendation_read_answer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

enum CustomDropDownType { simple, bordered }

class IndustryCategoryDropdown extends StatefulWidget {
  const IndustryCategoryDropdown(
      {super.key,
      this.onIndustryChanged,
      this.initialValue,
      this.type = CustomDropDownType.bordered,
      this.showLabel = true});

  final Function(int?)? onIndustryChanged;
  final int? initialValue;
  final CustomDropDownType type;
  final bool showLabel;

  @override
  IndustryCategoryDropdownState createState() =>
      IndustryCategoryDropdownState();
}

class IndustryCategoryDropdownState extends State<IndustryCategoryDropdown> {
  List<FullIndustry> _industries = [];
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final vm = context.read<RecommendationReadAnswerViewModel>();

      if (vm.industries.isEmpty) {
        if (!mounted) return;
        setState(() {
          _isLoading = true;
        });

        await vm.getIndustries();

        setState(() {
          _isLoading = false;
        });
      }

      if (!mounted) return;
      setState(() {
        _industries = vm.industries;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SmartGaps.gapH20,
          if (widget.showLabel)
            Text(tr('industry'),
                style: Theme.of(context).textTheme.headlineSmall),
          if (widget.showLabel) SmartGaps.gapH5,
          _isLoading
              ? Center(child: loader())
              : widget.type == CustomDropDownType.bordered
                  ? CustomDropDownWrapper(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Theme.of(context)
                                  .colorScheme
                                  .onSurfaceVariant,
                              width: 2)),
                      widget: ButtonTheme(
                        alignedDropdown: true,
                        child: DropdownButtonFormField<int>(
                          isExpanded: true,
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.only(left: 20.0),
                            focusedBorder: const UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.transparent)),
                            enabledBorder: const UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.transparent)),
                            hintText: tr('please_choose_an_industry'),
                            hintStyle: Theme.of(context).textTheme.titleSmall,
                            fillColor: Colors.black.withValues(alpha: 0.05),
                          ),
                          isDense: false,
                          items: _industries.map(
                            (e) {
                              return DropdownMenuItem<int>(
                                value: e.brancheId,
                                child: Text(
                                  context.locale.languageCode == 'da'
                                      ? e.brancheDa!
                                      : e.brancheEn!,
                                  style: Theme.of(context).textTheme.titleSmall,
                                ),
                              );
                            },
                          ).toList(),
                          onChanged: (value) {
                            widget.onIndustryChanged!(value);
                          },
                          value: widget.initialValue,
                        ),
                      ),
                    )
                  : CustomDropDownWrapper(
                      decoration: BoxDecoration(color: Colors.grey[200]),
                      padding: EdgeInsets.symmetric(
                          vertical: 10,
                          horizontal: widget.initialValue != null ? 0 : 10),
                      widget: ButtonTheme(
                        alignedDropdown: true,
                        child: DropdownButtonFormField<int>(
                          isExpanded: true,
                          decoration: InputDecoration.collapsed(
                            hintText: tr('please_choose_an_industry'),
                            hintStyle: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(fontWeight: FontWeight.w500),
                            fillColor: Colors.grey[200],
                            filled: true,
                          ),
                          isDense: false,
                          items: _industries.map(
                            (description) {
                              return DropdownMenuItem<int>(
                                value: description.brancheId,
                                child: Text(
                                  context.locale.languageCode == 'da'
                                      ? description.brancheDa!
                                      : description.brancheEn!,
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyMedium!
                                      .copyWith(
                                        fontWeight: FontWeight.w500,
                                      ),
                                ),
                              );
                            },
                          ).toList(),
                          onChanged: (value) {
                            widget.onIndustryChanged!(value);
                          },
                          value: widget.initialValue,
                        ),
                      ),
                    ),
        ],
      ),
    );
  }
}
