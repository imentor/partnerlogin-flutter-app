import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';

fileTypeIconSvg(
    {double height = 30,
    double width = 30,
    required String fileType,
    ColorFilter? colorFilter}) {
  switch (fileType) {
    case '.pdf':
      return SvgPicture.asset(
        SvgIcons.pdfFileFormat,
        width: width,
        height: height,
        colorFilter: colorFilter,
      );
    case '.jpg':
    case '.png':
    case '.jpeg':
    case '.heif':
    case '.hevc':
      return SvgPicture.asset(
        SvgIcons.imageFileFormat,
        width: width,
        height: height,
        colorFilter: colorFilter,
      );
    case '.docx':
    case '.doc':
      return SvgPicture.asset(
        SvgIcons.docFileFormat,
        width: width,
        height: height,
        colorFilter: colorFilter,
      );
    case '.xls':
    case '.xlsx':
      return SvgPicture.asset(
        SvgIcons.excelFileFormat,
        width: width,
        height: height,
        colorFilter: colorFilter,
      );
    default:
      return SizedBox(
        width: width,
        height: height,
        child: const Icon(FeatherIcons.alertTriangle),
      );
  }
}
