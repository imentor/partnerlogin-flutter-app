// ignore_for_file: use_build_context_synchronously

import 'dart:io';

import 'package:Haandvaerker.dk/utils/extension/snackbar.dart';
import 'package:Haandvaerker.dk/utils/file_utils.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

final imagePicker = ImagePicker();

Future<File?> getImage(
  BuildContext context, {
  ImageSource? source,
  bool hasNoSizeLimit = false,
}) async {
  try {
    File? file;

    switch (source) {
      case ImageSource.gallery:
      case ImageSource.camera:
        file = await imagePicker
            .pickImage(source: source!)
            .then((r) => r == null ? null : File(r.path));
        break;
      default:
        final keySelected = await showModalActionSheet(
          context: context,
          actions: [
            SheetAction(
              label: tr('camera'),
              icon: Icons.camera_alt,
              key: 'camera',
            ),
            SheetAction(
              label: tr('gallery'),
              icon: Icons.add_photo_alternate,
              key: 'gallery',
            ),
          ],
        );

        file = await imagePicker
            .pickImage(
                source: keySelected == 'camera'
                    ? ImageSource.camera
                    : ImageSource.gallery)
            .then((r) => r == null ? null : File(r.path));
        break;
    }

    if (file == null) return null;

    if (isUnderMaxImageSize(file)) {
      final newImage = await resizeImage(file);

      if (newImage != null) {
        return newImage;
      } else {
        context.showErrorSnackBar(message: tr('error_resizing_file'));
        return null;
      }
    } else {
      context.showErrorSnackBar(message: tr('image_exceed_500mb'));

      return null;
    }
  } catch (e) {
    if (e is PlatformException) {
      if (e.code == "photo_access_denied") {
        await Permission.photos.request().then((value) async {
          await catchPermission(context, value, "Photos");
        });
      } else if (e.code == "camera_access_denied") {
        await Permission.camera.request().then((value) async {
          await catchPermission(context, value, "Camera");
        });
      }
    }

    return null;
  }
}

Future<void> catchPermission(BuildContext context, PermissionStatus value,
    String permissionAccess) async {
  if (value.isPermanentlyDenied) {
    showOkCancelAlertDialog(
            context: context,
            okLabel: tr('open_settings'),
            message: '$permissionAccess ${tr('access_permanently_denied')}')
        .then((a) {
      if (a == OkCancelResult.ok) {
        openAppSettings();
      }
    });
  }
}

Future<bool> hasFilesCamerasPermissions() async {
  final deviceInfo = DeviceInfoPlugin();
  await Permission.camera.request();
  Map<Permission, PermissionStatus> statuses;

  if (Platform.isAndroid) {
    final androidInfo = await deviceInfo.androidInfo;
    if (androidInfo.version.sdkInt <= 32) {
      statuses = await [Permission.camera, Permission.storage].request();
    } else {
      statuses = await [Permission.camera, Permission.photos].request();
    }
  } else {
    statuses = await [Permission.camera, Permission.photos].request();
  }

  final cameraPermStatus = statuses[Permission.camera];
  final filesPermStatus =
      Platform.isAndroid && (await deviceInfo.androidInfo).version.sdkInt <= 32
          ? statuses[Permission.storage]
          : statuses[Permission.photos];

  return cameraPermStatus == PermissionStatus.granted &&
      filesPermStatus == PermissionStatus.granted;
}
