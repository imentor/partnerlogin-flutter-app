import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

Widget empty({String? emptyText, Color? color}) {
  var bgColor = color ?? Colors.grey[300];
  return Builder(
    builder: (context) => Center(
      child: Container(
        width: double.infinity,
        color: bgColor,
        child: Padding(
          padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
          child: Center(
            child: Text(
              emptyText ?? tr('empty'),
              style: Theme.of(context).textTheme.titleSmall!.copyWith(
                  color: Theme.of(context).colorScheme.onSurfaceVariant,
                  fontWeight: FontWeight.w600),
            ),
          ),
        ),
      ),
    ),
  );
}

class EmptyListIndicator extends StatelessWidget {
  const EmptyListIndicator({super.key, this.route, this.message});

  final String? route;
  final String? message;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 100,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(child: _getModuleIcon()),
          Flexible(
            child: Text(message ?? tr('empty'),
                style: Theme.of(context).textTheme.titleSmall!.copyWith(
                    color: Colors.grey[400], fontWeight: FontWeight.w600)),
          )
        ],
      ),
    );
  }

  Widget _getModuleIcon() {
    switch (route) {
      case Routes.readAndAnswerRecommendations:
        return Icon(
          ElementIcons.star_off,
          size: 54,
          color: Colors.grey[400],
        );
      case Routes.partners:
        return Icon(
          ElementIcons.files,
          size: 54,
          color: Colors.grey[400],
        );
      case Routes.yourClient:
        return Icon(
          ElementIcons.files,
          size: 54,
          color: Colors.grey[400],
        );
      case Routes.myProjects:
        return Icon(
          ElementIcons.picture_outline,
          size: 54,
          color: Colors.grey[400],
        );
      case Routes.allOffers:
        return Icon(
          ElementIcons.files,
          size: 54,
          color: Colors.grey[400],
        );
      case Routes.phoneNumber:
        return Icon(
          ElementIcons.phone_outline,
          size: 54,
          color: Colors.grey[400],
        );
      case Routes.badges:
        return Icon(
          ElementIcons.present,
          size: 54,
          color: Colors.grey[400],
        );
      case Routes.messageScreen:
        return Icon(
          ElementIcons.message,
          size: 54,
          color: Colors.grey[400],
        );

      default:
        return Icon(
          ElementIcons.notebook_1,
          size: 54,
          color: Colors.grey[400],
        );
    }
  }
}
