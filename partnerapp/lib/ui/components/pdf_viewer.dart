import 'dart:async';
import 'dart:io';

import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:path_provider/path_provider.dart';

Future<void> showPdf({
  required BuildContext context,
  String? path,
  Uint8List? pdfData,
  VoidCallback? downloadPdf,
  void Function(bool, dynamic)? onWillPop,
  VoidCallback? popRoute,
}) async {
  final BuildContext currentContext = context;

  if (path != null) {
    final file =
        await _fromAsset(path, path.substring(path.lastIndexOf('/') + 1));

    if (currentContext.mounted) {
      await showDialog(
        barrierDismissible: false,
        context: currentContext,
        builder: (context) {
          return PdfViewer(
            pdfPath: file.path,
            pdfData: pdfData,
            downloadPdf: downloadPdf,
            onWillPop: onWillPop,
            popRoute: popRoute,
          );
        },
      );
    }
  } else {
    if (currentContext.mounted) {
      await showDialog(
        barrierDismissible: false,
        context: currentContext,
        builder: (context) {
          return PdfViewer(
            pdfData: pdfData,
            downloadPdf: downloadPdf,
            onWillPop: onWillPop,
            popRoute: popRoute,
          );
        },
      );
    }
  }
}

Future<File> _fromAsset(String asset, String filename) async {
  Completer<File> completer = Completer();

  try {
    var dir = await getApplicationDocumentsDirectory();
    File file = File("${dir.path}/$filename");
    var data = await rootBundle.load(asset);
    var bytes = data.buffer.asUint8List();
    await file.writeAsBytes(bytes, flush: true);
    completer.complete(file);
  } catch (e) {
    throw Exception('Error parsing asset file!');
  }

  return completer.future;
}

class PdfViewer extends StatefulWidget {
  const PdfViewer({
    super.key,
    this.pdfPath,
    this.pdfData,
    this.downloadPdf,
    this.onWillPop,
    this.popRoute,
  });

  final String? pdfPath;
  final Uint8List? pdfData;
  final VoidCallback? downloadPdf;
  final void Function(bool, dynamic)? onWillPop;
  final VoidCallback? popRoute;

  @override
  PdfViewerState createState() => PdfViewerState();
}

class PdfViewerState extends State<PdfViewer> {
  final _controller = Completer<PDFViewController>();

  int actualPageNumber = 0, allPagesCount = 0;
  bool isReady = true;

  @override
  Widget build(BuildContext context) {
    return PopScope(
      onPopInvokedWithResult: widget.onWillPop,
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: isReady
                  ? PDFView(
                      swipeHorizontal: true,
                      filePath: widget.pdfPath,
                      pdfData: widget.pdfData,
                      onRender: (pages) {
                        setState(() {
                          allPagesCount = pages!;
                          isReady = true;
                        });
                      },
                      onViewCreated: (PDFViewController pdfViewController) {
                        _controller.complete(pdfViewController);
                      },
                      onPageChanged: (currentPage, totalpage) {
                        setState(() {
                          actualPageNumber = currentPage!;
                        });
                      },
                    )
                  : Center(child: loader()),
            ),
            Container(
              alignment: Alignment.bottomCenter,
              padding: const EdgeInsets.all(20),
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: allPagesCount,
                itemBuilder: (BuildContext context, int index) {
                  if (index == actualPageNumber) {
                    return Align(
                      alignment: Alignment.bottomCenter,
                      child: Icon(
                        Icons.blur_circular,
                        color: Theme.of(context).colorScheme.primary,
                        size: 12,
                      ),
                    );
                  } else {
                    return const Align(
                      alignment: Alignment.bottomCenter,
                      child: Icon(
                        Icons.blur_circular,
                        color: Colors.black45,
                        size: 10,
                      ),
                    );
                  }
                },
              ),
            ),
            Positioned(
              left: 20,
              top: 20,
              child: Opacity(
                opacity: 0.7,
                child: Container(
                  padding: const EdgeInsets.all(5),
                  decoration: const BoxDecoration(
                    color: Colors.black,
                    shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black45,
                        blurRadius: 2.0,
                        spreadRadius: 0.0,
                        offset: Offset(2.0, 2.0),
                      ),
                    ],
                  ),
                  child: InkWell(
                    onTap: widget.popRoute ??
                        () {
                          if (widget.downloadPdf != null) {
                            ScaffoldMessenger.of(context).clearSnackBars();
                          }

                          Navigator.of(context, rootNavigator: true).pop();
                        },
                    child: Icon(
                      Icons.close,
                      color: Colors.grey[200],
                    ),
                  ),
                ),
              ),
            ),
            if (widget.downloadPdf != null)
              Positioned(
                right: 20,
                top: 20,
                child: Opacity(
                  opacity: 0.7,
                  child: Container(
                      padding: const EdgeInsets.all(5),
                      decoration: const BoxDecoration(
                        color: Colors.black,
                        shape: BoxShape.circle,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black45,
                            blurRadius: 2.0,
                            spreadRadius: 0.0,
                            offset: Offset(2.0, 2.0),
                          ),
                        ],
                      ),
                      child: InkWell(
                          onTap: widget.downloadPdf!,
                          child: Icon(
                            Icons.download_rounded,
                            color: Colors.grey[200],
                          ))),
                ),
              )
          ],
        ),
      ),
    );
  }
}
