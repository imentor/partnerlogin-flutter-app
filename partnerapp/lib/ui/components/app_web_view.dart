import 'dart:io';

import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class AppWebView extends StatefulWidget {
  const AppWebView({
    super.key,
    this.url,
    this.isChild = false,
    this.html,
    this.headers,
  });

  final Uri? url;
  final bool? isChild;
  final String? html;
  final Map<String, String>? headers;

  @override
  State<AppWebView> createState() => _AppWebViewState();
}

class _AppWebViewState extends State<AppWebView> {
  final GlobalKey webViewKey = GlobalKey();

  InAppWebViewController? webViewController;

  late PullToRefreshController pullToRefreshController;

  double progress = 0;

  @override
  void initState() {
    super.initState();

    pullToRefreshController = PullToRefreshController(
      settings: PullToRefreshSettings(color: Colors.blue),
      onRefresh: () async {
        if (Platform.isAndroid) {
          webViewController?.reload();
        } else if (Platform.isIOS) {
          webViewController?.loadUrl(
              urlRequest: URLRequest(
                  url: await webViewController?.getUrl(),
                  headers: widget.headers));
        }
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.isChild!
        ? Center(
            child: Stack(
              alignment: Alignment.center,
              children: [
                InAppWebView(
                  key: webViewKey,
                  initialUrlRequest: widget.url != null
                      ? URLRequest(
                          url: WebUri(widget.url.toString()),
                          headers: widget.headers)
                      : null,
                  initialData: widget.html != null
                      ? InAppWebViewInitialData(data: widget.html!)
                      : null,
                  initialSettings: InAppWebViewSettings(
                    mediaPlaybackRequiresUserGesture: false,
                    transparentBackground: true,
                    useHybridComposition: true,
                    allowsInlineMediaPlayback: true,
                  ),
                  pullToRefreshController: pullToRefreshController,
                  onWebViewCreated: (controller) {
                    webViewController = controller;
                  },
                  onProgressChanged: (controller, progress) {
                    if (progress == 100) {
                      pullToRefreshController.endRefreshing();
                    }
                    setState(() {
                      this.progress = progress / 100;
                    });
                  },
                ),
                progress < 1.0
                    ? Container(
                        height: 100,
                        width: 100,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Stack(
                              alignment: Alignment.center,
                              children: [
                                const CircularProgressIndicator(
                                  strokeWidth: 5,
                                ),
                                Text('${(progress * 100).toInt().toString()}%'),
                              ],
                            ),
                            SmartGaps.gapH5,
                            const Text('Loading...'),
                          ],
                        ),
                      )
                    : Container(),
              ],
            ),
          )
        : Scaffold(
            appBar: const DrawerAppBar(),
            body: Center(
              child: Stack(
                alignment: Alignment.center,
                children: [
                  InAppWebView(
                    key: webViewKey,
                    initialUrlRequest:
                        URLRequest(url: WebUri(widget.url.toString())),
                    initialSettings: InAppWebViewSettings(
                      mediaPlaybackRequiresUserGesture: false,
                      transparentBackground: true,
                      useHybridComposition: true,
                      allowsInlineMediaPlayback: true,
                    ),
                    pullToRefreshController: pullToRefreshController,
                    onWebViewCreated: (controller) {
                      webViewController = controller;
                    },
                    onProgressChanged: (controller, progress) {
                      if (progress == 100) {
                        pullToRefreshController.endRefreshing();
                      }
                      setState(() {
                        this.progress = progress / 100;
                      });
                    },
                  ),
                  progress < 1.0
                      ? Container(
                          height: 100,
                          width: 100,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Stack(
                                alignment: Alignment.center,
                                children: [
                                  const CircularProgressIndicator(
                                    strokeWidth: 5,
                                  ),
                                  Text(
                                      '${(progress * 100).toInt().toString()}%'),
                                ],
                              ),
                              SmartGaps.gapH5,
                              const Text('Loading...'),
                            ],
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          );
  }
}
