import 'package:Haandvaerker.dk/ui/components/app_web_view.dart';
import 'package:Haandvaerker.dk/ui/components/copy_to_clipboard_snackbar.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/widgets_recommendations_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RecommendationWidgetsScreen extends StatefulWidget {
  const RecommendationWidgetsScreen({super.key});

  @override
  RecommendationWidgetsScreenState createState() =>
      RecommendationWidgetsScreenState();
}

class RecommendationWidgetsScreenState
    extends State<RecommendationWidgetsScreen> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      context.read<WidgetsViewModel>().setBannerValues();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var viewModel = Provider.of<WidgetsViewModel>(context, listen: true);

    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SingleChildScrollView(
        key: const Key(Keys.bannersScrollView),
        child: Container(
          padding: const EdgeInsets.only(top: 50, left: 20.0, right: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(tr('banner_for_your_website'),
                  style: Theme.of(context).textTheme.headlineLarge),
              SmartGaps.gapH20,
              Text(tr('recommendation_widget_title'),
                  style: Theme.of(context).textTheme.headlineMedium),
              SmartGaps.gapH10,
              Text(tr('widget_description_1'),
                  style: Theme.of(context).textTheme.bodyMedium),
              SmartGaps.gapH20,
              Text(tr('here_how_you_do_it'),
                  style: Theme.of(context).textTheme.headlineMedium),
              SmartGaps.gapH20,
              ListTile(
                  leading: Text('1',
                      textAlign: TextAlign.right,
                      style: Theme.of(context)
                          .textTheme
                          .headlineLarge!
                          .copyWith(
                              height: 1,
                              color: Theme.of(context).colorScheme.primary)),
                  title: Text(tr('widget_instruction_1'),
                      style: Theme.of(context).textTheme.titleSmall)),
              SmartGaps.gapH5,
              ListTile(
                  leading: Text('2',
                      textAlign: TextAlign.right,
                      style: Theme.of(context)
                          .textTheme
                          .headlineLarge!
                          .copyWith(
                              height: 1,
                              color: Theme.of(context).colorScheme.primary)),
                  title: Text(tr('widget_instruction_2'),
                      style: Theme.of(context).textTheme.titleSmall)),
              SmartGaps.gapH40,
              Container(
                padding: const EdgeInsets.only(bottom: 20),
                decoration: BoxDecoration(
                    color: Colors.white,
                    border:
                        Border.all(color: Colors.grey.withValues(alpha: 0.25)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withValues(alpha: .1),
                        blurRadius: 8.0,
                        spreadRadius: -1,
                        offset: const Offset(
                          0,
                          5,
                        ),
                      )
                    ]),
                child: Container(
                  margin: const EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        tr('banner_with_recommendations'),
                        style: Theme.of(context).textTheme.headlineMedium,
                      ),
                      SmartGaps.gapH10,
                      Text(tr('banner_with_recommendations_description'),
                          style: Theme.of(context).textTheme.bodyMedium),
                      SmartGaps.gapH20,
                      Container(
                        // margin: const EdgeInsets.only(top: 10),
                        height: 200,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.white.withValues(alpha: 0.1)),
                        ),
                        child: viewModel.busy
                            ? Center(
                                child: loader(),
                              )
                            : AppWebView(
                                url: Uri.parse(
                                  'https://widget5.haandvaerker.dk/hvdk/newpartner/widgetr.php?id=${context.read<UserViewModel>().userModel?.user?.id}&company=${context.read<UserViewModel>().userModel?.user?.name}',
                                ),
                                isChild: true,
                              ),
                      ),
                      getFirstCode(viewModel: viewModel),
                      SmartGaps.gapH10,
                      Text(tr('copy_and_submit_to_webmaster_label'),
                          style: Theme.of(context).textTheme.bodyMedium),
                      SmartGaps.gapH10,
                      Container(
                        width: double.infinity,
                        height: 70,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Theme.of(context).colorScheme.secondary,
                                width: 2)),
                        child: CustomDesignTheme.flatButtonStyle(
                          disabledForegroundColor: Colors.grey,
                          onPressed: viewModel.busy
                              ? null
                              : () async {
                                  await copyToClipboard(
                                      context: context,
                                      copyString:
                                          viewModel.recommendationBanner,
                                      displayIcon: const Icon(
                                        Icons.check_circle_outline,
                                        color: Colors.green,
                                      ),
                                      displayStatus: tr('code_is_now_copied'));
                                },
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Icon(
                                Icons.content_copy,
                                color: viewModel.busy
                                    ? Colors.grey
                                    : Theme.of(context).colorScheme.secondary,
                              ),
                              SmartGaps.gapW10,
                              Flexible(
                                child: Text(
                                  tr('copy_and_submit_to_webmaster'),
                                  maxLines: 2,
                                  overflow: TextOverflow.fade,
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SmartGaps.gapH30,
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    border:
                        Border.all(color: Colors.grey.withValues(alpha: 0.25)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withValues(alpha: .1),
                        blurRadius: 8.0,
                        spreadRadius: -1,
                        offset: const Offset(
                          0,
                          5,
                        ),
                      )
                    ]),
                child: Container(
                  margin: const EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        tr('banner_with_testimonials'),
                        style: Theme.of(context).textTheme.headlineMedium,
                      ),
                      SmartGaps.gapH10,
                      Text(tr('banner_with_testimonials_description'),
                          style: Theme.of(context).textTheme.bodyMedium),
                      Container(
                        margin: const EdgeInsets.only(top: 20),
                        padding: const EdgeInsets.all(0),
                        height: 200,
                        width: double.infinity,
                        color: Colors.transparent,
                        child: viewModel.busy
                            ? Center(child: loader())
                            : AppWebView(
                                url: Uri.parse(
                                    'https://widget5.haandvaerker.dk/hvdk/newpartner/widgetc.php?id=${context.read<UserViewModel>().userModel?.user?.id}&company=${context.read<UserViewModel>().userModel?.user?.name}'),
                                isChild: true,
                              ),
                      ),
                      getSecondCode(viewModel: viewModel),
                      SmartGaps.gapH20,
                      Text(tr('copy_and_submit_to_webmaster_label'),
                          style: Theme.of(context).textTheme.bodyMedium),
                      SmartGaps.gapH20,
                      Container(
                        width: double.infinity,
                        height: 70,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Theme.of(context).colorScheme.secondary,
                                width: 2)),
                        child: CustomDesignTheme.flatButtonStyle(
                          disabledForegroundColor: Colors.grey,
                          onPressed: viewModel.busy
                              ? null
                              : () async {
                                  await copyToClipboard(
                                      copyString: viewModel.testimonialBanner,
                                      displayStatus: tr('code_is_now_copied'),
                                      displayIcon: const Icon(
                                        Icons.check_circle_outline,
                                        color: Colors.green,
                                      ),
                                      context: context);
                                },
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Icon(
                                Icons.content_copy,
                                color: viewModel.busy
                                    ? Colors.grey
                                    : Theme.of(context).colorScheme.secondary,
                              ),
                              SmartGaps.gapW10,
                              Flexible(
                                child: Text(
                                  tr('copy_and_submit_to_webmaster'),
                                  maxLines: 2,
                                  overflow: TextOverflow.fade,
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget getFirstCode({required WidgetsViewModel viewModel}) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      padding: const EdgeInsets.all(5),
      height: 120,
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey.withValues(alpha: 0.1)),
      ),
      child: viewModel.busy
          ? Center(child: loader())
          : Center(
              child: Text(viewModel.recommendationBanner,
                  style: const TextStyle(
                      fontSize: 11,
                      color: Color.fromRGBO(0, 54, 69, 0.6),
                      fontWeight: FontWeight.w600,
                      height: 1.618))),
    );
  }

  Widget getSecondCode({required WidgetsViewModel viewModel}) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      padding: const EdgeInsets.all(5),
      height: 120,
      width: double.infinity,
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey.withValues(alpha: 0.3))),
      child: viewModel.busy
          ? Center(child: loader())
          : Center(
              child: Text(
              viewModel.testimonialBanner,
              style: const TextStyle(
                  fontSize: 11,
                  color: Color.fromRGBO(0, 54, 69, 0.6),
                  fontWeight: FontWeight.w600,
                  height: 1.618),
            )),
    );
  }
}
