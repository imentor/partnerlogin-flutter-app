import 'package:Haandvaerker.dk/model/recommendation/recommendation_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/recommendation_read_answer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

Future<void> recommendationReply({
  required BuildContext context,
  Recommendation? recommendationItem,
}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: const EdgeInsets.all(20),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(
                  onTap: () {
                    Navigator.of(dialogContext).pop();
                  },
                  child: const Icon(
                    FeatherIcons.x,
                    color: PartnerAppColors.spanishGrey,
                  )),
            ],
          ),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: RecommendationReplyDialog(
                    recommendationItem: recommendationItem)),
          ),
        );
      });
}

class RecommendationReplyDialog extends StatefulWidget {
  const RecommendationReplyDialog({super.key, this.recommendationItem});
  final Recommendation? recommendationItem;

  @override
  State<RecommendationReplyDialog> createState() =>
      _RecommendationReplyDialogState();
}

class _RecommendationReplyDialogState extends State<RecommendationReplyDialog> {
  final formKey = GlobalKey<FormBuilderState>();

  void onPressedComment() async {
    final recommendVm = context.read<RecommendationReadAnswerViewModel>();

    final currentState = formKey.currentState;
    if (currentState != null && currentState.validate()) {
      modalManager.showLoadingModal();
      final recommendationItemId = widget.recommendationItem?.id;
      final recommendationCommentValue =
          formKey.currentState?.fields['recommendationComment']?.value;
      if (recommendationItemId != null && recommendationCommentValue != null) {
        await tryCatchWrapper(
            context: context,
            function: recommendVm.commentOnReview(
                reviewId: recommendationItemId,
                supplierComment: recommendationCommentValue));

        if (!mounted) return;

        modalManager.hideLoadingModal();
        Navigator.of(context).pop();
        replySuccessDialog(context: context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                tr('customer_assessment_header').split(';').first,
                style: Theme.of(context)
                    .textTheme
                    .headlineLarge!
                    .copyWith(fontWeight: FontWeight.normal),
              ),
              Text(
                widget.recommendationItem?.contact?.name ?? '',
                style: Theme.of(context)
                    .textTheme
                    .headlineLarge!
                    .copyWith(fontWeight: FontWeight.bold),
              ),
            ],
          ),
          Text(
            tr('customer_assessment_header').split(';').last,
            style: Theme.of(context)
                .textTheme
                .headlineLarge!
                .copyWith(fontWeight: FontWeight.normal),
          ),
          SmartGaps.gapH20,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                tr('overall_assessment'),
                style: Theme.of(context)
                    .textTheme
                    .headlineLarge!
                    .copyWith(fontWeight: FontWeight.bold),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  RecommendationRatings(
                      rating: widget.recommendationItem?.rating),
                ],
              )
            ],
          ),
          SmartGaps.gapH5,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                tr('work_rating_desc'),
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(fontWeight: FontWeight.normal),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  RecommendationRatings(
                      rating: widget.recommendationItem?.workRating),
                ],
              )
            ],
          ),
          SmartGaps.gapH5,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                tr('price_rating_desc'),
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(fontWeight: FontWeight.normal),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  RecommendationRatings(
                      rating: widget.recommendationItem?.priceRating),
                ],
              )
            ],
          ),
          SmartGaps.gapH5,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                tr('cleans_up_after'),
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(fontWeight: FontWeight.normal),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  RecommendationRatings(
                      rating: widget.recommendationItem?.cleaningRating),
                ],
              )
            ],
          ),
          SmartGaps.gapH5,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                tr('keeps_agreements'),
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(fontWeight: FontWeight.normal),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  RecommendationRatings(
                      rating: widget.recommendationItem?.agreementRating),
                ],
              )
            ],
          ),
          SmartGaps.gapH5,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                tr('good_at_listening'),
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(fontWeight: FontWeight.normal),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  RecommendationRatings(
                      rating: widget.recommendationItem?.contactRating),
                ],
              )
            ],
          ),
          SmartGaps.gapH5,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                tr('recommend_company_rating_desc').replaceAll('?', ''),
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(fontWeight: FontWeight.normal),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  RecommendationRatings(
                      rating: widget.recommendationItem?.recommendRating),
                ],
              )
            ],
          ),
          SmartGaps.gapH20,
          Text(
            widget.recommendationItem?.headline ?? '',
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(fontWeight: FontWeight.bold),
          ),
          Text(
            widget.recommendationItem?.description ?? '',
            style: Theme.of(context).textTheme.bodyLarge,
          ),
          SmartGaps.gapH30,
          Text(
            tr('comment'),
            style: Theme.of(context).textTheme.labelLarge,
          ),
          SmartGaps.gapH5,
          CustomTextFieldFormBuilder(
            minLines: 5,
            name: 'recommendationComment',
            hintText: tr('write_comment_on_recommendation'),
            initialValue: widget.recommendationItem?.comment,
            validator: FormBuilderValidators.required(
              errorText: tr('required'),
            ),
          ),
          SmartGaps.gapH30,
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CustomDesignTheme.flatButtonStyle(
                child: Row(
                  children: [
                    const Icon(
                      Icons.arrow_back,
                      color: PartnerAppColors.grey1,
                      size: 15.0,
                    ),
                    SmartGaps.gapW5,
                    Text(tr('close'),
                        style: context.pttTitleLarge.copyWith(
                          fontSize: 15,
                          color: PartnerAppColors.grey1,
                        ))
                  ],
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              // SmartGaps.gapW10,
              CustomDesignTheme.flatButtonStyle(
                height: 40,
                backgroundColor: PartnerAppColors.malachite,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                  side: const BorderSide(
                    color: PartnerAppColors.malachite,
                  ),
                ),
                child: Row(
                  children: [
                    Text(tr('save_comment'),
                        style: context.pttTitleLarge.copyWith(
                          fontSize: 15,
                          color: Colors.white,
                        )),
                    SmartGaps.gapW5,
                    const Icon(
                      Icons.arrow_forward,
                      color: Colors.white,
                      size: 19.0,
                    )
                  ],
                ),
                onPressed: () => onPressedComment(),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class RecommendationRatings extends StatelessWidget {
  const RecommendationRatings({super.key, this.rating});
  final dynamic rating;

  @override
  Widget build(BuildContext context) {
    dynamic tempRating;
    if (rating != null) {
      if (rating.runtimeType == int) {
        tempRating = rating.toDouble();
      } else if (rating.runtimeType == String) {
        if (rating.contains('.')) {
          tempRating = double.parse(rating);
        } else {
          tempRating = int.parse(rating).toDouble();
        }
      }
      return RatingBar(
        ignoreGestures: true,
        itemSize: 15,
        initialRating: tempRating,
        direction: Axis.horizontal,
        allowHalfRating: true,
        itemCount: 5,
        ratingWidget: RatingWidget(
          full: Image.asset(ImagePaths.singleHouse),
          half: Image.asset(ImagePaths.halfHouse),
          empty: Opacity(
            opacity: 0.1,
            child: Image.asset(ImagePaths.singleHouse),
          ),
        ),
        itemPadding: const EdgeInsets.symmetric(horizontal: 1.0),
        onRatingUpdate: (value) {},
      );
    } else {
      return const SizedBox();
    }
  }
}

Future<void> replySuccessDialog({required BuildContext context}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: const EdgeInsets.all(20),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(
                  onTap: () {
                    Navigator.of(dialogContext).pop();
                  },
                  child: const Icon(
                    FeatherIcons.x,
                    color: PartnerAppColors.spanishGrey,
                  )),
            ],
          ),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Icon(
                    Icons.check_circle_outline,
                    color: PartnerAppColors.malachite,
                    size: 70,
                  ),
                  SmartGaps.gapH20,
                  Text(
                    tr('success!'),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: Colors.black,
                        ),
                  ),
                  SmartGaps.gapH20,
                  CustomDesignTheme.flatButtonStyle(
                    backgroundColor: Theme.of(context).colorScheme.secondary,
                    onPressed: () async {
                      Navigator.pop(dialogContext);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 10),
                      child: Text(
                        tr('close'),
                        style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                              letterSpacing: 1.1,
                            ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      });
}
