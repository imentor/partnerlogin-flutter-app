import 'dart:io';

import 'package:Haandvaerker.dk/model/recommendation/recommendation_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/screens/recommendations/components/recommendation_reply_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/recommendations/components/report_recommendation_dialog.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/recommendation_read_answer_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:blur/blur.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:skeletonizer/skeletonizer.dart';

class RecommendationsCardTemplate extends StatefulWidget {
  const RecommendationsCardTemplate({super.key});

  @override
  State<RecommendationsCardTemplate> createState() =>
      _RecommendationsCardTemplateState();
}

class _RecommendationsCardTemplateState
    extends State<RecommendationsCardTemplate> {
  @override
  Widget build(BuildContext context) {
    final recommendVm = context.read<RecommendationReadAnswerViewModel>();
    return Skeletonizer(
      enabled: recommendVm.isGettingRecommendationList,
      child: ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: recommendVm.isGettingRecommendationList
              ? 10
              : recommendVm.recommendations.length,
          itemBuilder: (context, index) {
            return recommendVm.isGettingRecommendationList
                ? FakeRecommendationCard(index: index)
                : RecommendationCard(recommendVm: recommendVm, index: index);
          }),
    );
  }
}

class RecommendationButtons extends StatelessWidget {
  const RecommendationButtons({super.key, required this.recommendationItem});

  final Recommendation recommendationItem;

  @override
  Widget build(BuildContext context) {
    final recommendVm = context.read<RecommendationReadAnswerViewModel>();
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        CustomDesignTheme.flatButtonStyle(
          height: 35,
          width: 35,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
            side: const BorderSide(
              color: PartnerAppColors.darkBlue,
              width: 0.5,
            ),
          ),
          child: SvgPicture.asset(
            'assets/images/answer_recommendation.svg',
          ),
          onPressed: () {
            recommendationReply(
              context: context,
              recommendationItem: recommendationItem,
            );
          },
        ),
        SmartGaps.gapW5,
        CustomDesignTheme.flatButtonStyle(
          height: 35,
          width: 35,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
            side: const BorderSide(
              color: PartnerAppColors.darkBlue,
              width: 0.5,
            ),
          ),
          child: SvgPicture.asset(
            'assets/images/save_icon.svg',
          ),
          onPressed: () async {
            if (recommendationItem.id != null) {
              modalManager.showLoadingModal(message: 'Downloading af fil...');

              await tryCatchWrapper(
                context: myGlobals.homeScaffoldKey!.currentContext,
                function: recommendVm.getReviewImageDownload(
                    reviewId: recommendationItem.id!),
              ).then((value) async {
                modalManager.hideLoadingModal();
                if (value != null) {
                  await showOkAlertDialog(
                    context: myGlobals.homeScaffoldKey!.currentContext!,
                    message: value
                        ? 'Filen er blevet downloadet'
                        : 'Der opstod en fejl',
                    okLabel: 'OK',
                  );
                }
              });
            }
          },
        ),
        SmartGaps.gapW5,
        CustomDesignTheme.flatButtonStyle(
          height: 35,
          width: 35,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
            side: const BorderSide(
              color: PartnerAppColors.darkBlue,
              width: 0.5,
            ),
          ),
          child: SvgPicture.asset(
            'assets/images/share_icon.svg',
          ),
          onPressed: () async {
            if (recommendationItem.id != null) {
              modalManager.showLoadingModal();

              await recommendVm
                  .getReviewImageShare(reviewId: recommendationItem.id!)
                  .then((value) async {
                if (value != null) {
                  final response = await http.get(value);
                  final bytes = response.bodyBytes;

                  final temp = await getTemporaryDirectory();
                  final path = '${temp.path}/image.jpg';
                  File(path).writeAsBytesSync(bytes);

                  modalManager.hideLoadingModal();

                  await Share.shareXFiles([XFile(path)]);
                }
              });
            }
          },
        ),
        SmartGaps.gapW5,
        CustomDesignTheme.flatButtonStyle(
          height: 35,
          width: 35,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
            side: const BorderSide(
              color: PartnerAppColors.darkBlue,
              width: 0.5,
            ),
          ),
          child: SvgPicture.asset(
            'assets/images/flag_icon.svg',
          ),
          onPressed: () {
            reportRecommenationDialog(
              context: context,
              recommendationItem: recommendationItem,
            ).whenComplete(() => recommendVm.initReportRecommend());
          },
        ),
      ],
    );
  }
}

class RecommendationRatingBar extends StatelessWidget {
  const RecommendationRatingBar({super.key, this.rating});
  final dynamic rating;

  @override
  Widget build(BuildContext context) {
    return RatingBar(
      itemSize: 15,
      initialRating: rating.runtimeType == String
          ? double.parse(rating)
          : rating.toDouble(),
      direction: Axis.horizontal,
      allowHalfRating: true,
      updateOnDrag: true,
      ignoreGestures: true,
      itemCount: 5,
      ratingWidget: RatingWidget(
        full: Image.asset(ImagePaths.singleHouse),
        half: Image.asset(ImagePaths.halfHouse),
        empty: Opacity(
          opacity: 0.1,
          child: Image.asset(ImagePaths.singleHouse),
        ),
      ),
      itemPadding: const EdgeInsets.symmetric(horizontal: 1.0),
      onRatingUpdate: (value) {},
    );
  }
}

class RecommendationCard extends StatefulWidget {
  const RecommendationCard(
      {super.key, required this.index, required this.recommendVm});
  final RecommendationReadAnswerViewModel recommendVm;
  final int index;

  @override
  State<RecommendationCard> createState() => _RecommendationCardState();
}

class _RecommendationCardState extends State<RecommendationCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      key: ValueKey('recommendationCard.${widget.index}'),
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
        side: const BorderSide(color: PartnerAppColors.grey1, width: 0.5),
      ),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(
                  radius: 30,
                  backgroundColor: PartnerAppColors.grey1,
                  foregroundImage: NetworkImage(widget.recommendVm
                          .recommendations[widget.index].contact?.avatar ??
                      ''),
                ),
                SizedBox(
                  width: 125,
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RecommendationRatingBar(
                          rating: widget.recommendVm
                              .recommendations[widget.index].rating),
                      SmartGaps.gapH5,
                      FittedBox(
                        child: Text(
                          widget.recommendVm.recommendations[widget.index]
                                  .contact?.name ??
                              '',
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium!
                              .copyWith(color: Colors.black),
                        ),
                      ),
                      SmartGaps.gapH5,
                      Text(
                        widget.recommendVm.recommendations[widget.index]
                                .branch ??
                            '',
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(color: PartnerAppColors.accentBlue),
                      ),
                    ],
                  ),
                ),
                SmartGaps.gapW30,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      widget.recommendVm.createdOnShowFormatter(widget
                              .recommendVm
                              .recommendations[widget.index]
                              .createdOnShow ??
                          ''),
                      style: Theme.of(context)
                          .textTheme
                          .bodyLarge!
                          .copyWith(color: PartnerAppColors.grey1),
                    ),
                  ],
                ),
              ],
            ),
            SmartGaps.gapH15,
            widget.recommendVm.recommendations[widget.index].status !=
                    'trademan_complaints'
                ? Text(
                    widget.recommendVm.recommendations[widget.index]
                            .description ??
                        '',
                    style: Theme.of(context)
                        .textTheme
                        .bodyLarge!
                        .copyWith(color: PartnerAppColors.spanishGrey))
                : Blur(
                    blur: 2.5,
                    child: Text(
                        widget.recommendVm.recommendations[widget.index]
                                .description ??
                            '',
                        style: Theme.of(context)
                            .textTheme
                            .bodyLarge!
                            .copyWith(color: PartnerAppColors.spanishGrey)),
                  ),
            SmartGaps.gapH20,
            widget.recommendVm.recommendations[widget.index].status !=
                    'trademan_complaints'
                ? RecommendationButtons(
                    recommendationItem:
                        widget.recommendVm.recommendations[widget.index])
                : Blur(
                    blur: 2.5,
                    child: RecommendationButtons(
                        recommendationItem:
                            widget.recommendVm.recommendations[widget.index])),
            if (widget.recommendVm.recommendations[widget.index].status ==
                'trademan_complaints') ...[
              SmartGaps.gapH5,
              Container(
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: Colors.blue[50],
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Text(tr('recommendation_reported'),
                    style: Theme.of(context)
                        .textTheme
                        .bodyLarge!
                        .copyWith(color: PartnerAppColors.blue)),
              ),
            ],
          ],
        ),
      ),
    );
  }
}

class FakeRecommendationCard extends StatelessWidget {
  const FakeRecommendationCard({super.key, required this.index});
  final int index;

  @override
  Widget build(BuildContext context) {
    return Card(
      key: ValueKey('fakeRecommendationCard.$index'),
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
        side: const BorderSide(color: PartnerAppColors.grey1, width: 0.5),
      ),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const CircleAvatar(
                  radius: 30,
                  backgroundColor: PartnerAppColors.grey,
                ),
                SizedBox(
                  width: 125,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const RecommendationRatingBar(rating: 5.0),
                      SmartGaps.gapH5,
                      FittedBox(
                        child: Text(
                          'Fake Name',
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium!
                              .copyWith(color: Colors.black),
                        ),
                      ),
                      SmartGaps.gapH5,
                      Text(
                        'Fake Branch',
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(color: PartnerAppColors.accentBlue),
                      ),
                    ],
                  ),
                ),
                SmartGaps.gapW30,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      '00 Fake 0000',
                      style: Theme.of(context)
                          .textTheme
                          .bodyLarge!
                          .copyWith(color: PartnerAppColors.grey1),
                    ),
                  ],
                ),
              ],
            ),
            SmartGaps.gapH15,
            Text('Fake Description',
                style: Theme.of(context)
                    .textTheme
                    .bodyLarge!
                    .copyWith(color: PartnerAppColors.spanishGrey)),
            SmartGaps.gapH20,
          ],
        ),
      ),
    );
  }
}
