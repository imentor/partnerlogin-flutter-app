import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class CreateRecommendationStep1 extends StatefulWidget {
  const CreateRecommendationStep1({super.key, this.id, this.onNext});

  final String? id;
  final Function(int, int, int, int, int, int, int)? onNext;

  @override
  CreateRecommendationStep1State createState() =>
      CreateRecommendationStep1State();
}

class CreateRecommendationStep1State extends State<CreateRecommendationStep1> {
  GlobalKey<State>? loadingKey;
  int rating = 0;
  int ratingAccess = 0;
  int ratingCollaboration = 0;
  int ratingCommunication = 0;
  int ratingFinancialSecurity = 0;
  int ratingRealisticExpectations = 0;
  int ratingTime = 0;

  bool desc1 = false;
  bool desc2 = false;
  bool desc3 = false;
  bool desc4 = false;
  bool desc5 = false;
  bool desc6 = false;

  @override
  void initState() {
    loadingKey = GlobalKey<State>();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SmartGaps.gapH20,
        _getStarRatings(),
        SmartGaps.gapH20,
        _getMainRating(),
        SmartGaps.gapH40,
        _getNextButton(),
      ],
    );
  }

  Widget _getStarRatings() {
    return Container(
      decoration: CustomDesignTheme.coldBlueBorderShadows,
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          StarWithLabel(
            key: UniqueKey(),
            showDesc: desc1,
            onShowDesc: () {},
            title: 'Kommunikation',
            desc:
                'Svarprocent for kommunikation. - Er boligejer lydhør over for dialoger med håndværkeren, for at de kan afslutte deres projekt korrekt?',
            initialRating: ratingCommunication,
            onRatingChanged: (value) {
              setState(() {
                ratingCommunication = value;
              });
            },
          ),
          SmartGaps.gapH10,
          StarWithLabel(
            key: UniqueKey(),
            showDesc: desc2,
            onShowDesc: () {},
            title: 'Overholdte Tidplanerne',
            desc:
                'Aktualitet for møder og tidsplan. - Overholdt husejeren planlagte møder vedrørende projektet?',
            initialRating: ratingTime,
            onRatingChanged: (value) {
              setState(() {
                ratingTime = value;
              });
            },
          ),
          SmartGaps.gapH10,
          StarWithLabel(
            key: UniqueKey(),
            showDesc: desc3,
            onShowDesc: () {},
            title: 'Realistiske forventninger',
            desc:
                'Forventninger: Opgav husejeren ærlige og realistiske projektdetaljer og deadlines?',
            initialRating: ratingRealisticExpectations,
            onRatingChanged: (value) {
              setState(() {
                ratingRealisticExpectations = value;
              });
            },
          ),
          SmartGaps.gapH10,
          StarWithLabel(
              key: UniqueKey(),
              showDesc: desc4,
              onShowDesc: () {},
              title: 'Samarbejdsvillighed',
              desc: 'Var husejeren nem at arbejde med?',
              initialRating: ratingCollaboration,
              onRatingChanged: (value) {
                setState(() {
                  ratingCollaboration = value;
                });
              }),
          SmartGaps.gapH10,
          StarWithLabel(
            key: UniqueKey(),
            showDesc: desc5,
            onShowDesc: () {},
            title: 'Økonomisk sikkerhed',
            desc:
                'Var husejeren i stand til at betale til tiden for deres projekter, eller satte husejeren et realistisk budget?',
            initialRating: ratingFinancialSecurity,
            onRatingChanged: (value) {
              setState(() {
                ratingFinancialSecurity = value;
              });
            },
          ),
          SmartGaps.gapH10,
          StarWithLabel(
            key: UniqueKey(),
            showDesc: desc6,
            onShowDesc: () {},
            title: 'Adgang til arbejdet',
            desc: 'var der plads til at kunne udføre arbejdet',
            initialRating: ratingAccess,
            onRatingChanged: (value) {
              setState(() {
                ratingAccess = value;
              });
            },
          ),
        ],
      ),
    );
  }

  Widget _getMainRating() {
    return Container(
      padding: const EdgeInsets.all(20),
      decoration: CustomDesignTheme.coldBlueBorder,
      child: Column(
        children: [
          Text('Din samlede vurdering',
              style: Theme.of(context).textTheme.headlineMedium),
          SmartGaps.gapH10,
          StarWithLabel(
            starItemOnly: true,
            initialRating: rating,
            onRatingChanged: (value) {
              setState(() {
                rating = value;
              });
            },
          ),
        ],
      ),
    );
  }

  Widget _getNextButton() {
    return Container(
      height: 60,
      width: double.infinity,
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.secondary,
      ),
      child: CustomDesignTheme.flatButtonStyle(
        child: Text(
          tr('next').toUpperCase(),
          style: Theme.of(context)
              .textTheme
              .headlineMedium!
              .copyWith(color: Colors.white),
        ),
        onPressed: () async {
          widget.onNext!(
              rating,
              ratingAccess,
              ratingCollaboration,
              ratingCommunication,
              ratingFinancialSecurity,
              ratingRealisticExpectations,
              ratingTime);
        },
      ),
    );
  }
}

class StarWithLabel extends StatefulWidget {
  const StarWithLabel(
      {this.title,
      this.desc,
      this.initialRating,
      this.onRatingChanged,
      this.starItemOnly = false,
      this.showDesc,
      this.onShowDesc,
      super.key});
  final String? title;
  final int? initialRating;
  final Function(int)? onRatingChanged;
  final bool starItemOnly;
  final String? desc;
  final VoidCallback? onShowDesc;
  final bool? showDesc;

  @override
  StarWithLabelState createState() => StarWithLabelState();
}

class StarWithLabelState extends State<StarWithLabel> {
  @override
  Widget build(BuildContext context) {
    return widget.starItemOnly ? _starItem() : _starWithLabel();
  }

  Widget _starWithLabel() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(children: [
                TextSpan(
                    text: widget.title,
                    style: Theme.of(context)
                        .textTheme
                        .bodyMedium!
                        .copyWith(fontWeight: FontWeight.normal)),
                WidgetSpan(
                    child: Container(
                  width: 15,
                  height: 25,
                  alignment: Alignment.topCenter,
                  child: IconButton(
                      icon: const Icon(ElementIcons.question, size: 15),
                      onPressed: () {
                        widget.onShowDesc!();
                      }),
                )),
              ]),
            ),
            const Spacer(),
            _starItem(itemSize: 25),
          ],
        ),
        SizedBox(
          width: 160,
          child: widget.showDesc!
              ? Container(
                  padding: const EdgeInsets.all(5),
                  color: Theme.of(context).colorScheme.primary.withAlpha(50),
                  child: Text(widget.desc!,
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          fontWeight: FontWeight.normal, fontSize: 10)),
                )
              : const SizedBox.shrink(),
        ),
      ],
    );
  }

  Widget _starItem({double itemSize = 30}) {
    return Container(
      alignment: Alignment.center,
      child: RatingBar(
        itemSize: itemSize,
        initialRating: widget.initialRating!.toDouble(),
        direction: Axis.horizontal,
        allowHalfRating: true,
        itemCount: 5,
        ratingWidget: RatingWidget(
          full: Image.asset(ImagePaths.singleHouse),
          half: Image.asset(ImagePaths.halfHouse),
          empty: Opacity(
            opacity: 0.1,
            child: Image.asset(ImagePaths.singleHouse),
          ),
        ),
        itemPadding: const EdgeInsets.symmetric(horizontal: 1.5),
        onRatingUpdate: (value) {
          setState(() {
            widget.onRatingChanged!(value.toInt());
          });
        },
      ),
    );
  }
}
