import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class CreateRecommendationStep2 extends StatefulWidget {
  const CreateRecommendationStep2(
      {super.key, this.id, this.onBack, this.onNext});

  final String? id;
  final VoidCallback? onBack;
  final Function(String, String)? onNext;

  @override
  CreateRecommendationStep2State createState() =>
      CreateRecommendationStep2State();
}

class CreateRecommendationStep2State extends State<CreateRecommendationStep2> {
  TextEditingController comment = TextEditingController();
  TextEditingController headline = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    comment.dispose();
    headline.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _getForm(),
        SmartGaps.gapH20,
        Row(
          children: [
            Flexible(child: _getBackButton()),
            SmartGaps.gapW10,
            Flexible(child: _getNextButton()),
          ],
        ),
      ],
    );
  }

  Widget _getForm() {
    return Container(
      decoration: CustomDesignTheme.coldBlueBorderShadows,
      padding: const EdgeInsets.all(20),
      child: Form(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RichText(
              maxLines: 2,
              text: TextSpan(
                text: '* ',
                style: const TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.red),
                children: <TextSpan>[
                  TextSpan(
                    text: 'Overskrift',
                    style: Theme.of(context).textTheme.titleSmall,
                  ),
                ],
              ),
            ),
            SmartGaps.gapH5,
            Container(
              decoration: CustomDesignTheme.coldBlueBorderShadows,
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: TextFormField(
                  controller: headline,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      fillColor: Colors.grey[200],
                      hintStyle: Theme.of(context)
                          .textTheme
                          .bodyMedium!
                          .copyWith(
                              fontWeight: FontWeight.w500,
                              color: Colors.grey.withValues(alpha: 0.4)),
                      hintText: 'Giv din anbefaling en titel. fx “Nyt tag”'),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return tr('please_enter_some_text');
                    }
                    return null;
                  }),
            ),
            SmartGaps.gapH20,
            RichText(
              maxLines: 2,
              text: TextSpan(
                text: '* ',
                style: const TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.red),
                children: <TextSpan>[
                  TextSpan(
                    text: 'Din anbefaling',
                    style: Theme.of(context).textTheme.titleSmall,
                  ),
                ],
              ),
            ),
            SmartGaps.gapH5,
            Container(
              decoration: CustomDesignTheme.coldBlueBorderShadows,
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: TextFormField(
                  controller: comment,
                  maxLines: 5,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      fillColor: Colors.grey[200],
                      hintStyle: Theme.of(context)
                          .textTheme
                          .bodyMedium!
                          .copyWith(
                              fontWeight: FontWeight.w500,
                              color: Colors.grey.withValues(alpha: 0.4)),
                      hintText:
                          'Beskriv hvad du har fået lavet og hvordan du oplevede vores arbejde og service.'),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return tr('please_enter_some_text');
                    }
                    return null;
                  }),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getNextButton() {
    return Container(
      height: 60,
      width: double.maxFinite,
      decoration: BoxDecoration(color: Theme.of(context).colorScheme.secondary),
      child: CustomDesignTheme.flatButtonStyle(
        child: Text(
          tr('next').toUpperCase(),
          style: Theme.of(context)
              .textTheme
              .headlineMedium!
              .copyWith(color: Colors.white),
        ),
        onPressed: () async {
          widget.onNext!(headline.text, comment.text);
        },
      ),
    );
  }

  Widget _getBackButton() {
    return Container(
      height: 60,
      width: double.maxFinite,
      decoration: BoxDecoration(
          border: Border.all(color: Theme.of(context).colorScheme.primary)),
      child: CustomDesignTheme.flatButtonStyle(
        child: Text(
          tr('back').toUpperCase(),
          style: Theme.of(context)
              .textTheme
              .headlineMedium!
              .copyWith(color: Theme.of(context).colorScheme.primary),
        ),
        onPressed: () async {
          widget.onBack!();
        },
      ),
    );
  }
}
