import 'package:Haandvaerker.dk/model/recommendation/partners_reviews_on_client.dart';
import 'package:Haandvaerker.dk/ui/components/custom_button.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class EditReviewDialog extends StatefulWidget {
  const EditReviewDialog(
      {super.key, required this.review, required this.onSubmit});

  final PartnersReviewsOnClient review;
  final Function(
          String, String, double?, double?, double?, double?, double?, double?)
      onSubmit;

  @override
  State<EditReviewDialog> createState() => _EditReviewDialoggState();
}

class _EditReviewDialoggState extends State<EditReviewDialog> {
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();

  final TextEditingController _headline = TextEditingController();
  final TextEditingController _comment = TextEditingController();
  double? ratingCommunication = 0.0;
  double? ratingTime = 0.0;
  double? ratingRealisticExpectations = 0.0;
  double? ratingFinancialSecurity = 0.0;
  double? ratingCollaboration = 0.0;
  double? ratingAccess = 0.0;

  @override
  void initState() {
    _headline.text = widget.review.ufHeadline!;
    _comment.text = widget.review.ufComment!;
    ratingCommunication = widget.review.ufRatingCommunication;
    ratingTime = widget.review.ufRatingTime;
    ratingRealisticExpectations = widget.review.ufRatingRealisticExpectations;
    ratingFinancialSecurity = widget.review.ufRatingFinancialSecurity;
    ratingCollaboration = widget.review.ufRatingCollaboration;
    ratingAccess = widget.review.ufRatingAccess is double
        ? widget.review.ufRatingAccess
        : double.parse(widget.review.ufRatingAccess.toString());

    super.initState();
  }

  @override
  void dispose() {
    _headline.dispose();
    _comment.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width / 1.1,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(3), color: Colors.white),
          padding: const EdgeInsets.all(20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Edit review',
                    style: Theme.of(context).textTheme.headlineLarge),
                SmartGaps.gapH10,
                _getForm(),
                GridView.count(
                    padding: EdgeInsets.zero,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    childAspectRatio: 1.25,
                    crossAxisCount: 2,
                    children: [
                      RecommendationRating(
                        initialRating: ratingCommunication,
                        label: 'Kommunikation',
                        onRatingUpdate: (value) {
                          setState(() {
                            ratingCommunication = value;
                          });
                        },
                      ),
                      RecommendationRating(
                        initialRating: ratingTime,
                        label: 'Overholdte Tidplanerne',
                        onRatingUpdate: (value) {
                          setState(() {
                            ratingTime = value;
                          });
                        },
                      ),
                      RecommendationRating(
                        initialRating: ratingRealisticExpectations,
                        label: 'Realistike forventninger',
                        onRatingUpdate: (value) {
                          setState(() {
                            ratingRealisticExpectations = value;
                          });
                        },
                      ),
                      RecommendationRating(
                        initialRating: ratingCollaboration,
                        label: 'Samarbejdsvillighed',
                        onRatingUpdate: (value) {
                          setState(() {
                            ratingCollaboration = value;
                          });
                        },
                      ),
                      RecommendationRating(
                        initialRating: ratingFinancialSecurity,
                        label: 'Økomonisk sikkerhed',
                        onRatingUpdate: (value) {
                          setState(() {
                            ratingFinancialSecurity = value;
                          });
                        },
                      ),
                      RecommendationRating(
                        initialRating: ratingAccess,
                        label: 'Adgang til arbejdet',
                        onRatingUpdate: (value) {
                          setState(() {
                            ratingAccess = value;
                          });
                        },
                      ),
                    ]),
                SmartGaps.gapH10,
                Row(
                  children: [
                    Flexible(
                      child: SizedBox(
                        height: 60,
                        child: CustomDesignTheme.flatButtonStyle(
                          padding: const EdgeInsets.all(0),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(2),
                            side: BorderSide(
                                color: Theme.of(context).colorScheme.primary,
                                width: 2),
                          ),
                          onPressed: () => Navigator.of(context).pop(),
                          child: Align(
                            alignment: Alignment.center,
                            child: Text(
                              tr('back'),
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .copyWith(
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold,
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SmartGaps.gapW10,
                    Flexible(
                      child: SubmitButton(
                        height: 60,
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            var response = await showOkCancelAlertDialog(
                                context: context,
                                title: tr('confirmation'),
                                message: tr('update_your_review'),
                                okLabel: tr('confirm'),
                                cancelLabel: tr('cancel'));
                            if (response == OkCancelResult.ok) {
                              widget.onSubmit(
                                  _headline.text,
                                  _comment.text,
                                  ratingAccess,
                                  ratingCollaboration,
                                  ratingCommunication,
                                  ratingFinancialSecurity,
                                  ratingRealisticExpectations,
                                  ratingTime);
                            }
                          }
                        },
                        text: tr('submit'),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _getForm() {
    return Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(tr('header'),
                style: Theme.of(context)
                    .textTheme
                    .titleSmall!
                    .copyWith(fontWeight: FontWeight.w500)),
            SmartGaps.gapH5,
            TextFormField(
                controller: _headline,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    filled: true,
                    fillColor: Colors.grey[200],
                    hintStyle: Theme.of(context)
                        .textTheme
                        .bodyMedium!
                        .copyWith(fontWeight: FontWeight.w500),
                    hintText: tr('header')),
                validator: (value) {
                  if (value!.isEmpty) {
                    return tr('please_enter_some_text');
                  }
                  return null;
                }),
            SmartGaps.gapH20,
            Text(tr('comment'),
                style: Theme.of(context)
                    .textTheme
                    .titleSmall!
                    .copyWith(fontWeight: FontWeight.w500)),
            SmartGaps.gapH5,
            TextFormField(
                controller: _comment,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    filled: true,
                    fillColor: Colors.grey[200],
                    hintStyle: Theme.of(context)
                        .textTheme
                        .bodyMedium!
                        .copyWith(fontWeight: FontWeight.w500),
                    hintText: tr('comment')),
                validator: (value) {
                  if (value!.isEmpty) {
                    return tr('please_enter_some_text');
                  }
                  return null;
                }),
            SmartGaps.gapH20,
          ],
        ));
  }
}

class RecommendationRating extends StatelessWidget {
  const RecommendationRating(
      {super.key,
      required this.label,
      required this.initialRating,
      required this.onRatingUpdate});

  final String label;
  final double? initialRating;
  final Function(double) onRatingUpdate;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 50,
            child: Text(label,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.titleSmall),
          ),
          SmartGaps.gapH10,
          Row(
            children: [
              RatingBar(
                initialRating: initialRating!,
                allowHalfRating: true,
                itemSize: 25,
                ratingWidget: RatingWidget(
                  full: Image.asset(ImagePaths.singleHouse),
                  half: Image.asset(ImagePaths.halfHouse),
                  empty: Opacity(
                    opacity: 0.1,
                    child: Image.asset(ImagePaths.singleHouse),
                  ),
                ),
                itemPadding: const EdgeInsets.symmetric(horizontal: 1.0),
                onRatingUpdate: onRatingUpdate,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
