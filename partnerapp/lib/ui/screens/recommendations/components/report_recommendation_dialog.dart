import 'dart:developer';
import 'dart:io';

import 'package:Haandvaerker.dk/model/recommendation/recommendation_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/recommendation_read_answer_viewmodel.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:file_selector/file_selector.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

Future<void> reportRecommenationDialog({
  required BuildContext context,
  Recommendation? recommendationItem,
}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: const EdgeInsets.all(20),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(
                onTap: () {
                  Navigator.of(dialogContext).pop();
                },
                child: const Icon(
                  FeatherIcons.x,
                  color: PartnerAppColors.spanishGrey,
                ),
              ),
            ],
          ),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: ReportRecommendationDialog(
                  recommendationItem: recommendationItem),
            ),
          ),
        );
      });
}

class ReportRecommendationDialog extends StatefulWidget {
  const ReportRecommendationDialog({super.key, this.recommendationItem});
  final Recommendation? recommendationItem;

  @override
  State<ReportRecommendationDialog> createState() =>
      _ReportRecommendationDialogState();
}

class _ReportRecommendationDialogState
    extends State<ReportRecommendationDialog> {
  final formKey = GlobalKey<FormBuilderState>();

  void onPressedRequestChange() async {
    final recommendVm = context.read<RecommendationReadAnswerViewModel>();

    if (formKey.currentState!.validate()) {
      modalManager.showLoadingModal();
      if (widget.recommendationItem != null &&
          widget.recommendationItem?.id != null) {
        int? category;
        if ((formKey.currentState?.fields['jobIndustry']?.value ?? '')
            .isNotEmpty) {
          category = int.parse(
            formKey.currentState?.fields['jobIndustry']?.value ?? '',
          );
        } else {
          category = null;
        }
        await tryCatchWrapper(
            context: context,
            function: recommendVm.createTradesmanComplaint(
              reviewId: widget.recommendationItem!.id!,
              complaint:
                  formKey.currentState?.fields['reportReason']?.value ?? '',
              headline:
                  formKey.currentState?.fields['writeComment']?.value ?? '',
              category: category,
              file: recommendVm.file,
              fileName: recommendVm.fileName,
            ));
        if (!mounted) return;

        modalManager.hideLoadingModal();
        Navigator.of(context).pop();
        reportRecommendationSuccessDialog(context: context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final recommendVm = context.read<RecommendationReadAnswerViewModel>();
    return FormBuilder(
      key: formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomDropdownFormBuilder(
            name: 'reportReason',
            hintText: tr('choose_setting'),
            validator: FormBuilderValidators.required(
              errorText: tr('required'),
            ),
            onChanged: (p0) {
              setState(() {
                recommendVm.reportReason = p0 ?? '';
              });
            },
            items: [
              DropdownMenuItem(
                value: '1',
                child: Text(
                  tr('i_did_not_do_the_work'),
                  style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        fontWeight: FontWeight.w500,
                      ),
                ),
              ),
              DropdownMenuItem(
                value: '2',
                child: Text(
                  tr('bad_language'),
                  style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        fontWeight: FontWeight.w500,
                      ),
                ),
              ),
              DropdownMenuItem(
                value: '3',
                child: Text(
                  tr('wrong_number_of_stars'),
                  style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        fontWeight: FontWeight.w500,
                      ),
                ),
              ),
              DropdownMenuItem(
                value: '4',
                child: Text(
                  tr('wrong_category'),
                  style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        fontWeight: FontWeight.w500,
                      ),
                ),
              ),
              DropdownMenuItem(
                value: '5',
                child: Text(
                  tr('other_reasons'),
                  style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        fontWeight: FontWeight.w500,
                      ),
                ),
              ),
            ],
          ),
          if (recommendVm.reportReason == '4') ...[
            SmartGaps.gapH20,
            CustomDropdownFormBuilder(
              name: 'jobIndustry',
              hintText: tr('choose'),
              validator: FormBuilderValidators.required(
                errorText: tr('required'),
              ),
              items: [
                ...recommendVm.industries.map((element) {
                  return DropdownMenuItem(
                    value: '${element.brancheId}',
                    child: Text(
                      context.locale.languageCode == 'en'
                          ? element.brancheEn ?? ''
                          : element.brancheDa ?? '',
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                            fontWeight: FontWeight.w500,
                          ),
                    ),
                  );
                })
              ],
            ),
          ],
          SmartGaps.gapH20,
          CustomTextFieldFormBuilder(
            minLines: 2,
            name: 'writeComment',
            hintText: tr('write_a_comment'),
            validator: FormBuilderValidators.required(
              errorText: tr('required'),
            ),
          ),
          if (recommendVm.reportReason == '4') ...[
            SmartGaps.gapH20,
            DottedBorder(
              color: Theme.of(context).colorScheme.primary,
              dashPattern: const [6, 4, 6, 4],
              strokeWidth: 2,
              borderType: BorderType.RRect,
              child: CustomDesignTheme.flatButtonStyle(
                padding: const EdgeInsets.all(10.0),
                width: double.infinity,
                backgroundColor: Colors.white,
                child: Text(
                  tr('submit_documentation_task_category'),
                  textAlign: TextAlign.center,
                  style: context.pttTitleLarge.copyWith(
                    fontSize: 15,
                    fontWeight: FontWeight.normal,
                    color: PartnerAppColors.darkBlue,
                  ),
                ),
                onPressed: () async {
                  if (Platform.isIOS) {
                    var fileResultList = await FilePicker.platform.pickFiles(
                      type: FileType.any,
                      allowMultiple: false,
                    );

                    if (fileResultList != null) {
                      recommendVm
                          .addSelectedFileIOS(file: fileResultList.files.first)
                          .whenComplete(() => recommendVm.setFileForReport(
                              platformFile: fileResultList.files.first));
                    }
                  } else if (Platform.isAndroid) {
                    const XTypeGroup fileTypeGroupAndroid = XTypeGroup(
                        label: 'files',
                        extensions: [
                          'pdf',
                          'jpg',
                          'jpeg',
                          'png',
                          'docx',
                          'doc',
                          'xls',
                          'xlsx',
                          'heif',
                          'hevc'
                        ]);

                    final selectedFile = await openFile(
                      acceptedTypeGroups: <XTypeGroup>[fileTypeGroupAndroid],
                    );

                    if (selectedFile != null) {
                      recommendVm
                          .addSelectedFileAndroid(file: selectedFile)
                          .whenComplete(() => recommendVm.setFileForReport(
                              xFile: recommendVm.selectedFileAndroid.first));
                      log('Selected file: ${selectedFile.name.replaceAll(RegExp('[^A-Za-z0-9]'), '')}.${selectedFile.mimeType!.split('/').last}');
                    }
                  }
                  setState(() {});
                },
              ),
            ),
            SmartGaps.gapH15,
            ListView.separated(
              separatorBuilder: ((context, index) {
                return SmartGaps.gapH10;
              }),
              itemBuilder: (context, index) {
                return Container(
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: PartnerAppColors.spanishGrey.withValues(alpha: .3),
                      borderRadius: BorderRadius.circular(5)),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                            child: Text(Platform.isIOS
                                ? recommendVm.selectedFileIOS[index].name
                                : '${recommendVm.selectedFileAndroid[index].name.replaceAll(RegExp('[^A-Za-z0-9]'), '')}.${recommendVm.selectedFileAndroid[index].mimeType!.split('/').last}')),
                        SmartGaps.gapW10,
                        GestureDetector(
                          onTap: () {
                            if (Platform.isIOS) {
                              recommendVm.deleteSelectedFileIOS(index: index);
                            } else if (Platform.isAndroid) {
                              recommendVm.deleteSelectedFileAndroid(
                                  index: index);
                            }
                            setState(() {});
                          },
                          child: const Icon(
                            FeatherIcons.trash2,
                            color: PartnerAppColors.spanishGrey,
                          ),
                        )
                      ]),
                );
              },
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: Platform.isIOS
                  ? recommendVm.selectedFileIOS.length
                  : recommendVm.selectedFileAndroid.length,
            ),
          ],
          SmartGaps.gapH20,
          CustomDesignTheme.flatButtonStyle(
            height: 40,
            width: double.infinity,
            backgroundColor: PartnerAppColors.malachite,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
              side: const BorderSide(
                color: PartnerAppColors.malachite,
              ),
            ),
            child: Text(
              tr('request_a_change'),
              style: context.pttTitleLarge.copyWith(
                fontSize: 15,
                color: Colors.white,
              ),
            ),
            onPressed: () => onPressedRequestChange(),
          ),
        ],
      ),
    );
  }
}

Future<void> reportRecommendationSuccessDialog(
    {required BuildContext context}) {
  final recommendVm = context.read<RecommendationReadAnswerViewModel>();
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: const EdgeInsets.all(20),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(
                  onTap: () {
                    recommendVm.getRecommendations(rating: 0);
                    Navigator.of(dialogContext).pop();
                  },
                  child: const Icon(
                    FeatherIcons.x,
                    color: PartnerAppColors.spanishGrey,
                  )),
            ],
          ),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Icon(
                    Icons.check_circle_outline,
                    color: PartnerAppColors.malachite,
                    size: 70,
                  ),
                  SmartGaps.gapH20,
                  Text(
                    tr('success!'),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: Colors.black,
                        ),
                  ),
                  SmartGaps.gapH20,
                  CustomDesignTheme.flatButtonStyle(
                    backgroundColor: Theme.of(context).colorScheme.secondary,
                    onPressed: () async {
                      recommendVm.getRecommendations(rating: 0);
                      Navigator.pop(dialogContext);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 10),
                      child: Text(
                        tr('close'),
                        style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                              letterSpacing: 1.1,
                            ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      });
}
