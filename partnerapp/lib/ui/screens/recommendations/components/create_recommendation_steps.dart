import 'package:Haandvaerker.dk/ui/components/steps_widget.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:flutter/material.dart';

class CreateRecommendationSteps extends StatelessWidget {
  const CreateRecommendationSteps({super.key, this.children, this.currentStep});

  final List<Widget>? children;
  final int? currentStep;

  @override
  Widget build(BuildContext context) {
    return _body(context);
  }

  Widget _body(BuildContext context) {
    return Column(
      children: [
        //_stepHeader(context),
        StepsWidget(
          currentPage: currentStep,
          steps: children!.length,
          title1: '',
          title2: '',
          title3: '',
        ),
        children!.elementAt(currentStep!),
        SmartGaps.gapH2,
      ],
    );
  }

  // Widget _stepHeader(BuildContext context) {
  //   if (currentStep == 1)
  //     return Padding(
  //       padding: const EdgeInsets.only(bottom: 10),
  //       child: Center(
  //         child: Text('Indhent pris på en elektriker-opgave',
  //             style: Theme.of(context)
  //                 .textTheme
  //                 .headlineSmall
  //                 .copyWith(fontWeight: FontWeight.normal)),
  //       ),
  //     );
  // }
}
