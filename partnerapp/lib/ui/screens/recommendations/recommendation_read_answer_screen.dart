import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/number_pagination.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/recommendations/components/recommendation_card_template.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/recommendation_read_answer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';

class RecommendationReadAnswerScreen extends StatefulWidget {
  const RecommendationReadAnswerScreen({super.key});

  @override
  RecommendationReadAnswerScreenState createState() =>
      RecommendationReadAnswerScreenState();
}

class RecommendationReadAnswerScreenState
    extends State<RecommendationReadAnswerScreen> {
  late ScrollController scroll;

  @override
  void initState() {
    scroll = ScrollController();

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final recommendVm = context.read<RecommendationReadAnswerViewModel>();
      recommendVm.ratingCriteria = 'all';

      await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey?.currentContext ?? context,
        function: Future.wait([
          recommendVm.getRecommendations(rating: 0),
          recommendVm.getIndustries()
        ]).whenComplete(() {
          recommendVm.industries.sort((a, b) {
            return (a.brancheId ?? 0).compareTo((b.brancheId ?? 0));
          });
        }),
      );

      recommendVm.currentPage = 1;
    });

    super.initState();
  }

  @override
  void dispose() {
    scroll.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        controller: scroll,
        floatHeaderSlivers: true,
        headerSliverBuilder: (context, _) {
          return [
            const DrawerAppBar(
              isSliver: true,
            )
          ];
        },
        body: Consumer<RecommendationReadAnswerViewModel>(
            builder: (_, recommendVm, __) {
          return SizedBox(
            height: double.infinity,
            child: RefreshIndicator(
              onRefresh: () async {
                setState(() {});
                await tryCatchWrapper(
                  context: myGlobals.homeScaffoldKey?.currentContext ?? context,
                  function: Future.wait([
                    recommendVm.getRecommendations(rating: 0),
                    recommendVm.getIndustries()
                  ]).whenComplete(() {
                    recommendVm.industries.sort((a, b) {
                      return (a.brancheId ?? 0).compareTo((b.brancheId ?? 0));
                    });
                  }),
                );
              },
              child: SingleChildScrollView(
                key: const Key(Keys.bannersScrollView),
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SmartGaps.gapH10,
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 3.0),
                        child: Text(
                          tr('read_and_answer_your_recommendations'),
                          style: Theme.of(context)
                              .textTheme
                              .headlineLarge!
                              .copyWith(
                                color: PartnerAppColors.black,
                              ),
                        ),
                      ),
                      SmartGaps.gapH15,
                      Card(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                          side: const BorderSide(
                              color: PartnerAppColors.darkBlue, width: 0.5),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(tr('new_opportunity_for_you'),
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineMedium!
                                      .copyWith(
                                          color: PartnerAppColors.darkBlue)),
                              SmartGaps.gapH10,
                              Text(tr('new_opportunity_desc_1'),
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyLarge!
                                      .copyWith(
                                        fontWeight: FontWeight.w400,
                                        color: PartnerAppColors.grey1,
                                        height: 2,
                                      )),
                              SmartGaps.gapH10,
                              Text(tr('new_opportunity_desc_2'),
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyLarge!
                                      .copyWith(
                                        fontWeight: FontWeight.w400,
                                        color: PartnerAppColors.grey1,
                                        height: 2,
                                      )),
                            ],
                          ),
                        ),
                      ),
                      SmartGaps.gapH20,
                      SizedBox(
                        width: double.infinity,
                        child: CustomDesignTheme.flatButtonStyle(
                            height: 48,
                            width: double.infinity,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                              side: const BorderSide(
                                color: PartnerAppColors.accentBlue,
                              ),
                            ),
                            child: Text(tr('make_recommendations_auto_pilot'),
                                style: context.pttTitleLarge.copyWith(
                                  fontSize: 17,
                                  color: PartnerAppColors.accentBlue,
                                )),
                            onPressed: () {
                              changeDrawerRoute(
                                  Routes.autoPilotRecommendations);
                            }),
                      ),
                      SmartGaps.gapH10,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          InkWell(
                            onTap: () =>
                                recommendationsFilterDialog(context: context),
                            child: const Icon(FeatherIcons.filter,
                                size: 40, color: PartnerAppColors.spanishGrey),
                          ),
                        ],
                      ),
                      SmartGaps.gapH10,
                      Column(
                        children: [
                          const RecommendationsCardTemplate(),
                          if (!recommendVm.isGettingRecommendationList &&
                              recommendVm.recommendations.isEmpty) ...[
                            const EmptyListIndicator()
                          ],
                        ],
                      ),
                      SmartGaps.gapH20,
                      NumberPagination(
                        currentPage: recommendVm.currentPage,
                        totalPages: recommendVm.lastPage,
                        onPageChanged: (p0) async {
                          scroll.animateTo(0,
                              duration: const Duration(milliseconds: 700),
                              curve: Curves.easeOut);
                          recommendVm.currentPage = p0;
                          recommendVm.getRecommendationData = true;
                          int rating = 0;
                          if (recommendVm.ratingCriteria.isNotEmpty &&
                              recommendVm.ratingCriteria != 'all') {
                            rating = int.parse(recommendVm.ratingCriteria);
                          }
                          await tryCatchWrapper(
                                  context:
                                      myGlobals.homeScaffoldKey!.currentContext,
                                  function: Future.value(recommendVm
                                      .getRecommendations(rating: rating)))
                              .whenComplete(() =>
                                  recommendVm.getRecommendationData = false);
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}

Future<void> recommendationsFilterDialog({required BuildContext context}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(70),
        contentPadding: const EdgeInsets.all(10),
        // title: Row(
        //   mainAxisAlignment: MainAxisAlignment.end,
        //   children: [
        //     InkWell(
        //         onTap: () {
        //           Navigator.of(dialogContext).pop();
        //         },
        //         child: const Icon(
        //           FeatherIcons.x,
        //           color: PartnerAppColors.spanishGrey,
        //         )),
        //   ],
        // ),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: const SingleChildScrollView(child: RecommendationsFilter()),
        ),
      );
    },
  );
}

class RecommendationsFilter extends StatefulWidget {
  const RecommendationsFilter({super.key});

  @override
  State<RecommendationsFilter> createState() => _RecommendationsFilterState();
}

class _RecommendationsFilterState extends State<RecommendationsFilter> {
  @override
  Widget build(BuildContext context) {
    final recommendVm = context.read<RecommendationReadAnswerViewModel>();
    return SingleChildScrollView(
      padding: const EdgeInsets.all(10),
      child: Column(
        children: [
          CustomCheckboxGroupFormBuilder(
              name: 'recommendationFilter',
              optionsOrientation: OptionsOrientation.vertical,
              options: [
                FormBuilderFieldOption(
                  value: 'all',
                  child: Padding(
                    padding: const EdgeInsets.only(top: 2.0),
                    child: Text(tr('all'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(
                                color: PartnerAppColors.darkBlue,
                                fontSize: 25)),
                  ),
                ),
                FormBuilderFieldOption(
                  value: '5',
                  child: ratingBar(rating: 5),
                ),
                FormBuilderFieldOption(
                  value: '4',
                  child: ratingBar(rating: 4),
                ),
                FormBuilderFieldOption(
                  value: '3',
                  child: ratingBar(rating: 3),
                ),
                FormBuilderFieldOption(
                  value: '2',
                  child: ratingBar(rating: 2),
                ),
                FormBuilderFieldOption(
                  value: '1',
                  child: ratingBar(rating: 1),
                ),
              ],
              initialValue: [(recommendVm.ratingCriteria)],
              onChanged: (p0) async {
                if (p0 != null && p0.isNotEmpty) {
                  Navigator.of(context).pop();
                  recommendVm.ratingCriteria = p0.last;
                  await tryCatchWrapper(
                          context: myGlobals.homeScaffoldKey!.currentContext,
                          function: Future.value(recommendVm.getRecommendations(
                              rating:
                                  p0.last == 'all' ? 0 : int.parse(p0.last))))
                      .whenComplete(() {});
                }
              }),
        ],
      ),
    );
  }

  Widget ratingBar({required int rating}) {
    return Row(
      children: [
        RatingBar(
          ignoreGestures: true,
          itemSize: 18,
          initialRating: rating.toDouble(),
          direction: Axis.horizontal,
          itemCount: rating,
          ratingWidget: RatingWidget(
            full: Image.asset(ImagePaths.singleHouse),
            half: Image.asset(ImagePaths.halfHouse),
            empty: Opacity(
              opacity: 0.1,
              child: Image.asset(ImagePaths.singleHouse),
            ),
          ),
          itemPadding: const EdgeInsets.symmetric(horizontal: 1.0),
          onRatingUpdate: (value) {},
        ),
        SmartGaps.gapW10,
        Text(
          '$rating ${tr('stars')}',
          style: Theme.of(context)
              .textTheme
              .labelLarge!
              .copyWith(color: PartnerAppColors.spanishGrey),
        ),
      ],
    );
  }
}
