import 'package:Haandvaerker.dk/model/integration/integration_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/components/package/package_popup_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/pdf_viewer.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/facebook/facebook_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/packages/packages_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/recommendation_autopilot_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher_string.dart';

import '../customers/tender_folder/create_offer_wizard/sign_submit_offer/create_offer.dart';

String feedbackIconUrl = 'assets/images/feedback.svg';
String ecommerceIconUrl = 'assets/images/ecommerce.svg';
String billIconUrl = 'assets/images/bill.svg';

class RecommendationAutoPilotScreen extends StatefulWidget {
  const RecommendationAutoPilotScreen({super.key, this.fromScreen});

  final String? fromScreen;

  @override
  RecommendationAutoPilotScreenState createState() =>
      RecommendationAutoPilotScreenState();
}

class RecommendationAutoPilotScreenState
    extends State<RecommendationAutoPilotScreen> {
  late PackagesViewModel packagesVm;
  final dineroFirma = TextEditingController();
  final dinero = TextEditingController();
  final facebook = TextEditingController();
  final economic = TextEditingController();
  final billy = TextEditingController();
  final dineroFormKey = GlobalKey<FormState>();
  final economicFormKey = GlobalKey<FormState>();
  final billyFormKey = GlobalKey<FormState>();
  final loadingKey = GlobalKey<State>();
  bool isDineroChecked = false;
  bool isEconomicChecked = false;
  bool isBillyChecked = false;

  @override
  void initState() {
    super.initState();
    packagesVm = context.read<PackagesViewModel>();
    WidgetsBinding.instance
        .addPostFrameCallback((_) async => await getSupplierIntegrations());
  }

  @override
  void dispose() {
    dinero.dispose();
    facebook.dispose();
    dineroFirma.dispose();
    economic.dispose();
    billy.dispose();
    super.dispose();
  }

  Future<void> getSupplierIntegrations() async {
    final viewModel = context.read<RecommendationAutoPilotViewModel>();
    final facebookViewModel = context.read<FacebookViewModel>();

    await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function: Future.wait([
          viewModel.getSupplierIntegrations().then((onValue) {
            viewModel.integrations =
                IntegrationModel.fromCollection(onValue!.data as List<dynamic>);
          }),
          facebookViewModel.getFacebookPage()
        ]));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SingleChildScrollView(
        physics: const ClampingScrollPhysics(),
        key: const Key(Keys.autoPilotScrollView),
        child: Container(
          padding: const EdgeInsets.only(top: 20, left: 20.0, right: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(tr('recommendations_on_auto_pilot'),
                  style: Theme.of(context).textTheme.headlineLarge),
              SmartGaps.gapH20,
              Text(
                tr('save_time_with_autopilots'),
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    fontWeight: FontWeight.w700,
                    color: Theme.of(context).colorScheme.primary),
              ),
              SmartGaps.gapH5,
              Text(
                tr('autopilot_integration_description_1'),
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              SmartGaps.gapH30,
              Center(
                child: Row(
                  children: <Widget>[
                    SvgPicture.asset(
                      billIconUrl,
                      width: 36.0,
                      height: 36.0,
                      colorFilter: ColorFilter.mode(
                        Theme.of(context).colorScheme.primary,
                        BlendMode.srcIn,
                      ),
                    ),
                    SmartGaps.gapW10,
                    Text(
                      tr('your_invoice_your_customers'),
                      style: Theme.of(context)
                          .textTheme
                          .titleSmall!
                          .copyWith(height: 1.2, fontWeight: FontWeight.w700),
                    ),
                  ],
                ),
              ),
              SmartGaps.gapH30,
              Center(
                child: Row(
                  children: <Widget>[
                    SvgPicture.asset(
                      ecommerceIconUrl,
                      width: 36.0,
                      height: 36.0,
                      colorFilter: ColorFilter.mode(
                        Theme.of(context).colorScheme.primary,
                        BlendMode.srcIn,
                      ),
                    ),
                    SmartGaps.gapW10,
                    Flexible(
                      child: Text(
                        tr('select_who_we_encourage_to_recommend_you'),
                        style: Theme.of(context)
                            .textTheme
                            .titleSmall!
                            .copyWith(height: 1.2, fontWeight: FontWeight.w700),
                        overflow: TextOverflow.fade,
                      ),
                    ),
                  ],
                ),
              ),
              SmartGaps.gapH30,
              Center(
                child: Row(
                  children: <Widget>[
                    SvgPicture.asset(
                      feedbackIconUrl,
                      width: 28.0,
                      height: 36.0,
                      colorFilter: ColorFilter.mode(
                        Theme.of(context).colorScheme.primary,
                        BlendMode.srcIn,
                      ),
                    ),
                    SmartGaps.gapW20,
                    Flexible(
                      child: Text(
                        context
                            .tr('receive_recommendations_on_your_profile_page'),
                        style: Theme.of(context)
                            .textTheme
                            .titleSmall!
                            .copyWith(height: 1.2, fontWeight: FontWeight.w700),
                        overflow: TextOverflow.fade,
                      ),
                    ),
                  ],
                ),
              ),
              // Center(
              //     child: Image.asset(
              //   ImagePaths.photo1,
              // )),
              SmartGaps.gapH30,
              Text(
                tr('accounting_autopilot'),
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: Theme.of(context).colorScheme.primary,
                    fontWeight: FontWeight.w700),
              ),
              SmartGaps.gapH5,
              Text(
                tr('autopilot_description_2'),
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              SmartGaps.gapH10,
              Text(
                tr('accounting_autopilot_3'),
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: Theme.of(context).colorScheme.primary,
                    fontWeight: FontWeight.w700),
              ),
              SmartGaps.gapH5,
              Text(
                tr('autopilot_description_4'),
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              SmartGaps.gapH40,
              Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey.withValues(alpha: 0.1)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withValues(alpha: .3),
                      blurRadius: 10.0,
                      spreadRadius: -1,
                      offset: const Offset(0, 5),
                    )
                  ],
                ),
                child: dineroExpansion(),
              ),
              SmartGaps.gapH20,
              Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey.withValues(alpha: 0.1)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withValues(alpha: .3),
                      blurRadius: 10.0,
                      spreadRadius: -1,
                      offset: const Offset(0, 5),
                    )
                  ],
                ),
                child: economicExpansion(),
              ),
              SmartGaps.gapH20,
              Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey.withValues(alpha: 0.1)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withValues(alpha: .3),
                      blurRadius: 10.0,
                      spreadRadius: -1,
                      offset: const Offset(0, 5),
                    )
                  ],
                ),
                child: billyExpansion(),
              ),
              SmartGaps.gapH40,
              Text(
                tr('facebook_integration'),
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    fontWeight: FontWeight.w700,
                    color: Theme.of(context).colorScheme.primary),
              ),
              SmartGaps.gapH10,
              Text(
                tr('facebook_integration_description'),
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              SmartGaps.gapH20,
              Consumer<RecommendationAutoPilotViewModel>(
                builder: (_, vm, consumerWidget) {
                  if (vm.fbRecommendationBanner.isNotEmpty) {
                    return Image.asset(vm.fbRecommendationBanner);
                  }

                  return Center(child: Image.asset(ImagePaths.shareFbPhoto));
                },
              ),
              SmartGaps.gapH20,
              Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey.withValues(alpha: 0.1)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withValues(alpha: .3),
                      blurRadius: 10.0,
                      spreadRadius: -1,
                      offset: const Offset(0, 5),
                    )
                  ],
                ),
                child: facebookExpansion(),
              ),
              SmartGaps.gapH20,
            ],
          ),
        ),
      ),
    );
  }

  Widget dineroExpansion() {
    var viewModel =
        Provider.of<RecommendationAutoPilotViewModel>(context, listen: true);
    final theme = Theme.of(context).copyWith(dividerColor: Colors.transparent);

    dinero.text = viewModel.integrations
            .any((element) => element.thirdPartName == 'dinero')
        ? viewModel.integrations
            .firstWhere((element) => element.thirdPartName == 'dinero',
                orElse: () => IntegrationModel.defaults())
            .accessKey!
        : '';
    dineroFirma.text = viewModel.integrations
            .any((element) => element.thirdPartName == 'dinero')
        ? viewModel.integrations
            .firstWhere((element) => element.thirdPartName == 'dinero',
                orElse: () => IntegrationModel.defaults())
            .levId!
            .toString()
        : '';

    return Theme(
      data: theme,
      child: ExpansionTile(
        key: const Key(Keys.dineroRecommendationKey),
        title: Container(
            height: 50,
            alignment: Alignment.centerLeft,
            child: SvgPicture.asset(ImagePaths.dinero)),
        trailing: const Icon(
          Icons.add_circle,
          color: Colors.transparent,
        ),
        children: <Widget>[
          Form(
            key: dineroFormKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SmartGaps.gapH10,
                Text(
                  tr('dinero_expansion_header'),
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
                SmartGaps.gapH10,
                if (viewModel.integrations.isNotEmpty)
                  if (viewModel.integrations
                      .any((element) => element.thirdPartName == 'dinero'))
                    Container(
                      padding: const EdgeInsets.all(20),
                      color: Theme.of(context).colorScheme.onSurfaceVariant,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            tr('dinero_api_update_text_1'),
                            style: const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                            ),
                          ),
                          SmartGaps.gapH10,
                          Text(tr('dinero_api_update_text_2'),
                              style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                              ))
                        ],
                      ),
                    ),
                SmartGaps.gapH10,
                Container(
                  decoration: BoxDecoration(
                    // border: Border.all(color: Theme.of(context).colorScheme.primary)
                    color: Theme.of(context).colorScheme.primary,
                  ),
                  height: 70,
                  width: double.infinity,
                  child: CustomDesignTheme.flatButtonStyle(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          tr('view_instructions_here'),
                          style: const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 18),
                        ),
                        const Icon(
                          Icons.keyboard_arrow_right,
                          color: Colors.white,
                        )
                      ],
                    ),
                    onPressed: () => showPdf(
                        context: context,
                        path: DocumentPaths.dineroIntegrationPdf),
                  ),
                ),
                SmartGaps.gapH10,
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Theme.of(context).colorScheme.primary)),
                  height: 70,
                  width: double.infinity,
                  child: CustomDesignTheme.flatButtonStyle(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          tr('see_video_tutorial'),
                          style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                              fontWeight: FontWeight.bold,
                              fontSize: 18),
                        ),
                        Icon(
                          Icons.keyboard_arrow_right,
                          color: Theme.of(context).colorScheme.primary,
                        )
                      ],
                    ),
                    onPressed: () {
                      launchUri(NetworkPaths.dinerVideo);
                    },
                  ),
                ),
                SmartGaps.gapH10,
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Theme.of(context).colorScheme.primary)),
                  height: 70,
                  width: double.infinity,
                  child: CustomDesignTheme.flatButtonStyle(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          tr('open_link_to_dinero'),
                          style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                              fontWeight: FontWeight.bold,
                              fontSize: 18),
                        ),
                        Icon(
                          Icons.keyboard_arrow_right,
                          color: Theme.of(context).colorScheme.primary,
                        )
                      ],
                    ),
                    onPressed: () => launchUri(NetworkPaths.dineroPage),
                  ),
                ),
                SmartGaps.gapH30,
                Text(
                  tr('api_key'),
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).colorScheme.onSurfaceVariant),
                ),
                SmartGaps.gapH5,
                TextFormField(
                    keyboardType: TextInputType.text,
                    controller: dinero,
                    decoration: const InputDecoration(
                        border: InputBorder.none,
                        filled: true,
                        hintStyle: TextStyle(fontSize: 13),
                        hintText: ''),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return tr('please_enter_some_text');
                      }
                      return null;
                    }),
                SmartGaps.gapH5,
                Text(tr('please_insert_a_valid_api_key'),
                    style: const TextStyle(fontSize: 12)),
                SmartGaps.gapH30,
                Text(tr('company_id'),
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).colorScheme.onSurfaceVariant)),
                SmartGaps.gapH5,
                TextFormField(
                    keyboardType: TextInputType.number,
                    controller: dineroFirma,
                    decoration: const InputDecoration(
                        border: InputBorder.none,
                        filled: true,
                        hintStyle: TextStyle(fontSize: 13),
                        hintText: ''),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return tr('please_enter_some_text');
                      }
                      return null;
                    }),
                SmartGaps.gapH5,
                Text(tr('please_enter_a_valid_company_id'),
                    style: const TextStyle(fontSize: 12)),
                SmartGaps.gapH15,
                // Checkbox Implementation - Dinero
                Row(children: [
                  Checkbox(
                    value: isDineroChecked,
                    activeColor: PartnerAppColors.blue,
                    onChanged: (bool? val) {
                      setState(() {
                        isDineroChecked = val!;
                      });
                    },
                  ),
                  GestureDetector(
                    onTap: () {
                      termsDialog(
                          context: context,
                          termsHTML: viewModel.termsAndConditionsHtml);
                    },
                    child: Text(
                      viewModel.termsAndConditionsLabel,
                      style:
                          Theme.of(context).textTheme.headlineMedium!.copyWith(
                                decoration: TextDecoration.underline,
                                letterSpacing: 1.0,
                                color: PartnerAppColors.blue,
                                fontWeight: FontWeight.normal,
                                fontSize: 18,
                              ),
                    ),
                  ),
                ]),
                SmartGaps.gapH15,
                SizedBox(
                  height: 70,
                  child: CustomDesignTheme.flatButtonStyle(
                    backgroundColor: !isDineroChecked
                        ? PartnerAppColors.grey
                        : Theme.of(context).colorScheme.secondary,
                    onPressed: () async {
                      if (!packagesVm.partnerAccountIntegration.status) {
                        if (isDineroChecked) {
                          await showUnAvailableDialog(
                              context, packagesVm.partnerAccountIntegration,
                              popTwice: false);
                        }
                      } else {
                        if (isDineroChecked) {
                          serviceDinero();
                        }
                      }
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          tr('save'),
                          style: const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 18),
                        ),
                        const Icon(Icons.arrow_right, color: Colors.white),
                      ],
                    ),
                  ),
                ),
                SmartGaps.gapH20,
                Container(
                  padding: const EdgeInsets.all(5),
                  color: Colors.black12.withValues(alpha: 0.05),
                  height: 45,
                  width: double.infinity,
                  child: Center(
                    child: Text(
                        '${tr('last_update')} ${viewModel.integrations.any((element) => element.thirdPartName == 'dinero') ? viewModel.integrations.firstWhere((element) => element.thirdPartName == 'dinero', orElse: () => IntegrationModel.defaults()).lastUpdated ?? viewModel.integrations.firstWhere((element) => element.thirdPartName == 'dinero', orElse: () => IntegrationModel.defaults()).createDate : ''}',
                        style: Theme.of(context).textTheme.bodyMedium),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget economicExpansion() {
    var viewModel =
        Provider.of<RecommendationAutoPilotViewModel>(context, listen: true);
    final theme = Theme.of(context).copyWith(dividerColor: Colors.transparent);
    economic.text = viewModel.integrations
            .any((element) => element.thirdPartName == 'e-conomic')
        ? viewModel.integrations
            .firstWhere((element) => element.thirdPartName == 'e-conomic',
                orElse: () => IntegrationModel.defaults())
            .accessKey!
        : '';

    return Theme(
      data: theme,
      child: ExpansionTile(
        key: const Key(Keys.economicRecommendationKey),
        title: Container(
            alignment: Alignment.centerLeft,
            height: 50,
            child: SvgPicture.asset(ImagePaths.vismaeconomic)),
        trailing: const Icon(
          Icons.add_circle,
          color: Colors.transparent,
        ),
        children: <Widget>[
          Form(
            key: economicFormKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SmartGaps.gapH10,
                Text(tr('insert_id_below'),
                    style: Theme.of(context).textTheme.bodyMedium),
                SmartGaps.gapH10,
                if (viewModel.integrations.isNotEmpty)
                  if (viewModel.integrations
                      .any((element) => element.thirdPartName == 'e-conomic'))
                    Container(
                      padding: const EdgeInsets.all(20),
                      color: Theme.of(context).colorScheme.onSurfaceVariant,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            tr('economic_api_update_text_1'),
                            style: const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                          SmartGaps.gapH10,
                          Text(tr('economic_api_update_text_2'),
                              style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ))
                        ],
                      ),
                    ),
                SmartGaps.gapH10,
                Container(
                  decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.primary
                      // border: Border.all(color: Theme.of(context).colorScheme.primary, width: 2)
                      ),
                  height: 70,
                  width: double.infinity,
                  child: CustomDesignTheme.flatButtonStyle(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          tr('view_instructions_here'),
                          style: Theme.of(context)
                              .textTheme
                              .labelMedium!
                              .copyWith(color: Colors.white),
                        ),
                        const Icon(Icons.keyboard_arrow_right,
                            color: Colors.white
                            // Theme.of(context).colorScheme.primary,
                            )
                      ],
                    ),
                    onPressed: () => showPdf(
                        context: context,
                        path: DocumentPaths.economicIntegrationPdf),
                  ),
                ),
                SmartGaps.gapH10,
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Theme.of(context).colorScheme.primary)),
                  height: 60,
                  width: double.infinity,
                  child: CustomDesignTheme.flatButtonStyle(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          tr('open_link_to_economic'),
                          style: Theme.of(context).textTheme.labelMedium,
                        ),
                        Icon(
                          Icons.keyboard_arrow_right,
                          color: Theme.of(context).colorScheme.primary,
                        )
                      ],
                    ),
                    onPressed: () => launchUri(NetworkPaths.economicPage),
                  ),
                ),
                SmartGaps.gapH20,
                Text(tr('id'),
                    style: TextStyle(
                        color: Theme.of(context).colorScheme.onSurfaceVariant,
                        fontWeight: FontWeight.bold)),
                SmartGaps.gapH5,
                TextFormField(
                    keyboardType: TextInputType.text,
                    controller: economic,
                    decoration: const InputDecoration(
                        border: InputBorder.none,
                        filled: true,
                        hintStyle: TextStyle(fontSize: 13),
                        hintText: ''),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return tr('please_enter_some_text');
                      }
                      return null;
                    }),
                SmartGaps.gapH5,
                Text(tr('please_enter_a_valid_id'),
                    style: const TextStyle(fontSize: 12)),
                SmartGaps.gapH15,
                // Checkbox Implementation - Economic
                Row(children: [
                  Checkbox(
                    value: isEconomicChecked,
                    activeColor: PartnerAppColors.blue,
                    onChanged: (bool? val) {
                      setState(() {
                        isEconomicChecked = val!;
                      });
                    },
                  ),
                  GestureDetector(
                    onTap: () {
                      termsDialog(
                          context: context,
                          termsHTML: viewModel.termsAndConditionsHtml);
                    },
                    child: Text(
                      viewModel.termsAndConditionsLabel,
                      style:
                          Theme.of(context).textTheme.headlineMedium!.copyWith(
                                decoration: TextDecoration.underline,
                                letterSpacing: 1.0,
                                color: PartnerAppColors.blue,
                                fontWeight: FontWeight.normal,
                                fontSize: 18,
                              ),
                    ),
                  ),
                ]),
                SmartGaps.gapH15,
                SizedBox(
                  height: 70,
                  child: CustomDesignTheme.flatButtonStyle(
                    backgroundColor: !isEconomicChecked
                        ? PartnerAppColors.grey
                        : Theme.of(context).colorScheme.secondary,
                    onPressed: () async {
                      if (!packagesVm.partnerAccountIntegration.status) {
                        if (isEconomicChecked) {
                          await showUnAvailableDialog(
                              context, packagesVm.partnerAccountIntegration,
                              popTwice: false);
                        }
                      } else {
                        if (isEconomicChecked) {
                          serviceEconomic();
                        }
                      }
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          tr('save'),
                          style: const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 24),
                        ),
                        const Icon(Icons.arrow_right, color: Colors.white),
                      ],
                    ),
                  ),
                ),
                SmartGaps.gapH20,
                Container(
                  padding: const EdgeInsets.all(10),
                  color: Colors.black12.withValues(alpha: 0.05),
                  height: 45,
                  child: Center(
                    child: Text(
                        '${tr('last_update')} ${viewModel.integrations.any((element) => element.thirdPartName == 'e-conomic') ? viewModel.integrations.firstWhere((element) => element.thirdPartName == 'e-conomic', orElse: () => IntegrationModel.defaults()).lastUpdated ?? viewModel.integrations.firstWhere((element) => element.thirdPartName == 'e-conomic', orElse: () => IntegrationModel.defaults()).createDate : ''}',
                        style: Theme.of(context).textTheme.bodyMedium),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget billyExpansion() {
    var viewModel =
        Provider.of<RecommendationAutoPilotViewModel>(context, listen: true);
    final theme = Theme.of(context).copyWith(dividerColor: Colors.transparent);
    billy.text = viewModel.integrations
            .any((element) => element.thirdPartName == 'billy')
        ? viewModel.integrations
            .firstWhere((element) => element.thirdPartName == 'billy',
                orElse: () => IntegrationModel.defaults())
            .accessKey!
        : '';

    return Theme(
      data: theme,
      child: ExpansionTile(
        key: const Key(Keys.billyRecommendationKey),
        title: Container(
            alignment: Alignment.centerLeft,
            height: 50,
            child: SvgPicture.asset(ImagePaths.billy)),
        trailing: const Icon(
          Icons.add_circle,
          color: Colors.transparent,
        ),
        children: <Widget>[
          Form(
            key: billyFormKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SmartGaps.gapH10,
                Text(tr('insert_id_below'),
                    style: Theme.of(context).textTheme.bodyMedium),
                SmartGaps.gapH10,
                viewModel.busy ? loader() : Container(),
                if (viewModel.integrations.isNotEmpty)
                  if (viewModel.integrations
                      .any((element) => element.thirdPartName == 'billy'))
                    Container(
                      padding: const EdgeInsets.all(20),
                      color: Theme.of(context).colorScheme.onSurfaceVariant,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            tr('billy_api_update_text_1'),
                            style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 16),
                          ),
                          SmartGaps.gapH10,
                          Text(tr('billy_api_update_text_2'),
                              style: const TextStyle(
                                  color: Colors.white, fontSize: 16))
                        ],
                      ),
                    ),
                SmartGaps.gapH20,
                Container(
                  decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.primary,
                      border: Border.all(
                          color: Theme.of(context).colorScheme.primary,
                          width: 2)),
                  height: 70,
                  width: double.infinity,
                  child: CustomDesignTheme.flatButtonStyle(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            tr('view_instructions_here'),
                            style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                          const Icon(
                            Icons.keyboard_arrow_right,
                            color: Colors.white,
                          )
                        ],
                      ),
                      onPressed: () {
                        showPdf(
                            context: context,
                            path: DocumentPaths.billyIntegrationPdf);
                      }),
                ),
                SmartGaps.gapH10,
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Theme.of(context).colorScheme.primary,
                          width: 2)),
                  height: 70,
                  width: double.infinity,
                  child: CustomDesignTheme.flatButtonStyle(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          tr('open_link_to_billy'),
                          style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                        Icon(
                          Icons.keyboard_arrow_right,
                          color: Theme.of(context).colorScheme.primary,
                        )
                      ],
                    ),
                    onPressed: () => launchUri(NetworkPaths.billyPage),
                  ),
                ),
                SmartGaps.gapH30,
                Text(tr('api_key'),
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).colorScheme.onSurfaceVariant)),
                SmartGaps.gapH5,
                TextFormField(
                    keyboardType: TextInputType.text,
                    controller: billy,
                    decoration: const InputDecoration(
                        border: InputBorder.none,
                        filled: true,
                        hintStyle: TextStyle(fontSize: 13),
                        hintText: ''),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return tr('please_enter_some_text');
                      }
                      return null;
                    }),
                SmartGaps.gapH5,
                Text(tr('please_insert_a_valid_api_key'),
                    style: const TextStyle(fontSize: 12)),
                SmartGaps.gapH15,
                // Checkbox Implementation - Billy
                Row(children: [
                  Checkbox(
                    value: isBillyChecked,
                    activeColor: PartnerAppColors.blue,
                    onChanged: (bool? val) {
                      setState(() {
                        isBillyChecked = val!;
                      });
                    },
                  ),
                  GestureDetector(
                    onTap: () {
                      termsDialog(
                          context: context,
                          termsHTML: viewModel.termsAndConditionsHtml);
                    },
                    child: Text(
                      viewModel.termsAndConditionsLabel,
                      style:
                          Theme.of(context).textTheme.headlineMedium!.copyWith(
                                decoration: TextDecoration.underline,
                                letterSpacing: 1.0,
                                color: PartnerAppColors.blue,
                                fontWeight: FontWeight.normal,
                                fontSize: 18,
                              ),
                    ),
                  ),
                ]),
                SmartGaps.gapH15,
                SizedBox(
                  height: 70,
                  child: CustomDesignTheme.flatButtonStyle(
                    backgroundColor: !isBillyChecked
                        ? PartnerAppColors.grey
                        : Theme.of(context).colorScheme.secondary,
                    onPressed: () async {
                      if (!packagesVm.partnerAccountIntegration.status) {
                        if (isBillyChecked) {
                          await showUnAvailableDialog(
                              context, packagesVm.partnerAccountIntegration,
                              popTwice: false);
                        }
                      } else {
                        if (isBillyChecked) {
                          serviceBilly();
                        }
                      }
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          tr('save'),
                          style: const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                        const Icon(Icons.arrow_right, color: Colors.white),
                      ],
                    ),
                  ),
                ),
                SmartGaps.gapH10,
                Container(
                  padding: const EdgeInsets.all(5),
                  color: Colors.black12,
                  height: 45,
                  child: Center(
                    child: Text(
                        '${tr('last_update')} ${viewModel.integrations.any((element) => element.thirdPartName == 'billy') ? viewModel.integrations.firstWhere((element) => element.thirdPartName == 'billy', orElse: () => IntegrationModel.defaults()).lastUpdated ?? viewModel.integrations.firstWhere((element) => element.thirdPartName == 'billy', orElse: () => IntegrationModel.defaults()).createDate : ''}',
                        style: Theme.of(context).textTheme.bodyMedium),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget facebookExpansion() {
    final theme = Theme.of(context).copyWith(dividerColor: Colors.transparent);
    var viewModel =
        Provider.of<RecommendationAutoPilotViewModel>(context, listen: true);
    facebook.text = viewModel.integrations
            .any((element) => element.thirdPartName == 'facebook')
        ? viewModel.integrations
            .firstWhere((element) => element.thirdPartName == 'facebook',
                orElse: () => IntegrationModel.defaults())
            .organizationId!
        : '';

    return Consumer<FacebookViewModel>(builder: (_, vm, __) {
      return Theme(
        data: theme,
        child: ExpansionTile(
          leading: const SizedBox(
              height: 50,
              child: FaIcon(
                FontAwesomeIcons.facebook,
                color: Color.fromRGBO(24, 119, 242, 1.0),
                size: 50,
              )),
          title: Container(
            alignment: Alignment.centerLeft,
            height: 50,
            child: Text(
              'facebook',
              style: GoogleFonts.openSans(
                  textStyle: const TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Color.fromRGBO(24, 119, 242, 1.0))),
            ),
          ),
          trailing: const Icon(
            Icons.add_circle,
            color: Colors.transparent,
          ),
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SmartGaps.gapH20,
                Text(
                  tr('facebook_integration'),
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
                SmartGaps.gapH10,
                ...vm.facebookPages.map((e) {
                  return ListTile(
                    contentPadding: EdgeInsets.zero,
                    title: Text(e.pageName ?? ''),
                    trailing: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          CupertinoSwitch(
                              value: e.postReview == 1,
                              onChanged: (onChange) async {
                                modalManager.showLoadingModal();

                                await vm
                                    .activateDeactivatePost(
                                        isActivate: onChange, id: e.id!)
                                    .whenComplete(
                                        () => modalManager.hideLoadingModal());
                              }),
                          SmartGaps.gapW5,
                          GestureDetector(
                            onTap: () async {
                              modalManager.showLoadingModal();

                              await vm.deletePage(id: e.id!).whenComplete(
                                  () => modalManager.hideLoadingModal());
                            },
                            child: Text(tr('delete'),
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.red)),
                          )
                        ]),
                  );
                }),
                SmartGaps.gapH10,
                SizedBox(
                  width: double.infinity,
                  height: 70,
                  child: TextButton.icon(
                    style: TextButton.styleFrom(
                        backgroundColor: PartnerAppColors.blue),
                    onPressed: () async {
                      await launchUrlString(
                          '${applic.bitrixApiUrl}/facebook/relogin',
                          mode: LaunchMode.externalApplication);
                    },
                    icon: const FaIcon(
                      FontAwesomeIcons.squareFacebook,
                      color: Colors.white,
                    ),
                    label: Text(
                      tr('connect_facebook'),
                      style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                  ),
                ),
                // if (viewModel.integrations.isNotEmpty)
                //   if (viewModel.integrations
                //       .any((element) => element.thirdPartName == 'facebook'))
                //     Container(
                //       padding: const EdgeInsets.all(20),
                //       color: Theme.of(context).colorScheme.onSurfaceVariant,
                //       child: Column(
                //         crossAxisAlignment: CrossAxisAlignment.start,
                //         children: <Widget>[
                //           Text(
                //             tr('facebook_api_update_text_1'),
                //             style: const TextStyle(
                //                 color: Colors.white,
                //                 fontWeight: FontWeight.bold,
                //                 fontSize: 16),
                //           ),
                //           SmartGaps.gapH10,
                //           Text(tr('facebook_api_update_text_2'),
                //               style: const TextStyle(
                //                   color: Colors.white,
                //                   fontWeight: FontWeight.bold,
                //                   fontSize: 16))
                //         ],
                //       ),
                //     ),
                // SmartGaps.gapH20,
                // Container(
                //   decoration: BoxDecoration(
                //       border: Border.all(
                //           color: Theme.of(context).colorScheme.primary,
                //           width: 2)),
                //   height: 70,
                //   width: double.infinity,
                //   child: CustomDesignTheme.flatButtonStyle(
                //     child: Row(
                //       mainAxisAlignment: MainAxisAlignment.center,
                //       children: <Widget>[
                //         Text(
                //           tr('view_instructions_here'),
                //           style: TextStyle(
                //               color: Theme.of(context).colorScheme.primary,
                //               fontWeight: FontWeight.bold,
                //               fontSize: 20),
                //         ),
                //         Icon(
                //           Icons.keyboard_arrow_right,
                //           color: Theme.of(context).colorScheme.primary,
                //         )
                //       ],
                //     ),
                //     onPressed: () => showPdf(
                //         context: context,
                //         path: DocumentPaths.facebookIntegrationPdf),
                //   ),
                // ),
                // SmartGaps.gapH30,
                // Text(tr('facebook_page_id'),
                //     style: Theme.of(context).textTheme.bodyMedium),
                // SmartGaps.gapH5,
                // TextFormField(
                //     keyboardType: TextInputType.number,
                //     controller: facebook,
                //     decoration: const InputDecoration(
                //         border: InputBorder.none,
                //         filled: true,
                //         hintStyle: TextStyle(fontSize: 13),
                //         hintText: ''),
                //     validator: (value) {
                //       if (value!.isEmpty) {
                //         return tr('please_enter_some_text');
                //       }
                //       return null;
                //     }),
                // SmartGaps.gapH10,
                // SizedBox(
                //   width: double.infinity,
                //   height: 70,
                //   child: TextButton.icon(
                //     style: ButtonStyle(
                //         backgroundColor: MaterialStateColor.resolveWith(
                //             (states) => Colors.blue)),
                //     onPressed: () async {
                //       if (!packagesVm.fbPostRecommendation.status) {
                //         await showUnAvailableDialog(
                //             context, packagesVm.fbPostRecommendation,
                //             popTwice: false);
                //       } else {
                //         final url =
                //             'https://www.kundematch.dk/newpartner/facebook.php?id=${data[UserData.key_portal]}&levid=${data[UserData.haaid]}&pageid=${facebook.text.trim()}';

                //         await launchUrlString(url,
                //             mode: LaunchMode.externalApplication);
                //       }
                //     },
                //     icon: const FaIcon(
                //       FontAwesomeIcons.squareFacebook,
                //       color: Colors.white,
                //     ),
                //     label: Text(
                //       tr('connect_facebook'),
                //       style: const TextStyle(
                //           color: Colors.white,
                //           fontWeight: FontWeight.bold,
                //           fontSize: 20),
                //     ),
                //   ),
                // ),
                // SmartGaps.gapH20,
                // Container(
                //   padding: const EdgeInsets.all(5),
                //   color: Colors.black12.withValues(alpha:0.05),
                //   height: 45,
                //   child: Center(
                //     child: Text(
                //       '${tr('last_update')} ${viewModel.integrations.any((element) => element.thirdPartName == 'facebook') ? viewModel.integrations.firstWhere((element) => element.thirdPartName == 'facebook').lastUpdated ?? viewModel.integrations.firstWhere((element) => element.thirdPartName == 'facebook').createDate : ''}',
                //       style: Theme.of(context).textTheme.bodyMedium,
                //     ),
                //   ),
                // )
              ],
            ),
          ],
        ),
      );
    });
  }

  Future<void> serviceBilly() async {
    var viewModel = context.read<RecommendationAutoPilotViewModel>();
    final userVm = context.read<UserViewModel>();

    if (billyFormKey.currentState!.validate()) {
      final payload = {
        'supplierId': userVm.userModel?.user?.id ?? 0,
        'accessKey': billy.text.trim(),
        'integration': 'billy',
      };
      showLoadingDialog(context, loadingKey);

      await tryCatchWrapper(
              context: myGlobals.homeScaffoldKey!.currentContext,
              function: viewModel.saveSupplierIntegration(payload: payload))
          .then((value) async {
        await getSupplierIntegrations();
        Navigator.of(loadingKey.currentContext!, rootNavigator: true).pop();
        await showSuccessAnimationDialog(
            myGlobals.homeScaffoldKey!.currentContext!,
            value!.success!,
            value.message);
      });
    }
  }

  Future<void> serviceDinero() async {
    var viewModel = context.read<RecommendationAutoPilotViewModel>();
    final userVm = context.read<UserViewModel>();

    if (dineroFormKey.currentState!.validate()) {
      final payload = {
        'supplierId': userVm.userModel?.user?.id ?? 0,
        'accessKey': dinero.text.trim(),
        'integration': 'dinero',
        'organizationId': dineroFirma.text.trim(),
      };

      showLoadingDialog(context, loadingKey);

      await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function: viewModel.saveSupplierIntegration(payload: payload).then(
          (value) async {
            await getSupplierIntegrations();
            Navigator.of(loadingKey.currentContext!, rootNavigator: true).pop();
            await showSuccessAnimationDialog(
                myGlobals.homeScaffoldKey!.currentContext!,
                value.success!,
                value.message);
          },
        ),
      );
    }
  }

  Future<void> serviceEconomic() async {
    var viewModel = context.read<RecommendationAutoPilotViewModel>();
    final userVm = context.read<UserViewModel>();

    if (economicFormKey.currentState!.validate()) {
      final payload = {
        'supplierId': userVm.userModel?.user?.id ?? 0,
        'accessKey': economic.text.trim(),
        'integration': 'e-conomic',
      };

      showLoadingDialog(context, loadingKey);

      await tryCatchWrapper(
              context: myGlobals.homeScaffoldKey!.currentContext,
              function: viewModel.saveSupplierIntegration(payload: payload))
          .then((value) async {
        await getSupplierIntegrations();
        Navigator.of(loadingKey.currentContext!, rootNavigator: true).pop();
        await showSuccessAnimationDialog(
            myGlobals.homeScaffoldKey!.currentContext!,
            value!.success!,
            value.message);
      });
    }
  }

  Future<void> launchUri(String url) async {
    // if (await canLaunch(Uri.encodeFull(url))) {
    await launchUrlString(Uri.encodeFull(url));
    // }
  }

  Future<void> viewPdf(String path) async {
    await showPdf(context: context, path: path);
  }
}
