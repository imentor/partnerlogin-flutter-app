import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/recommendations/components/create_recommendation_step1.dart';
import 'package:Haandvaerker.dk/ui/screens/recommendations/components/create_recommendation_step2.dart';
import 'package:Haandvaerker.dk/ui/screens/recommendations/components/create_recommendation_steps.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/recommendation_read_answer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CreateRecommendationScreen extends StatefulWidget {
  const CreateRecommendationScreen({super.key, this.id = '0'});

  final String id;

  @override
  CreateRecommendationScreenState createState() =>
      CreateRecommendationScreenState();
}

class CreateRecommendationScreenState
    extends State<CreateRecommendationScreen> {
  GlobalKey<State>? loadingKey;
  int currentStep = 0;
  int rating = 0;
  int ratingAccess = 0;
  int ratingCollaboration = 0;
  int ratingCommunication = 0;
  int ratingFinancialSecurity = 0;
  int ratingRealisticExpectations = 0;
  int ratingTime = 0;

  @override
  void initState() {
    loadingKey = GlobalKey<State>();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(top: 20, left: 20.0, right: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(tr('thank_you_for_giving_recommendation'),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headlineLarge),
              SmartGaps.gapH20,
              Text(tr('create_recommendation_description'),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyMedium),
              SmartGaps.gapH20,
              CreateRecommendationSteps(
                currentStep: currentStep,
                children: [
                  CreateRecommendationStep1(
                      id: widget.id,
                      onNext: (rating,
                          ratingAccess,
                          ratingCollaboration,
                          ratingCommunication,
                          ratingFinancialSecurity,
                          ratingRealisticExpectations,
                          ratingTime) {
                        if (currentStep < 2) {
                          setState(() {
                            this.rating = rating;
                            this.ratingAccess = ratingAccess;
                            this.ratingCollaboration = ratingCollaboration;
                            this.ratingCommunication = ratingCommunication;
                            this.ratingFinancialSecurity =
                                ratingFinancialSecurity;
                            this.ratingRealisticExpectations =
                                ratingRealisticExpectations;
                            this.ratingTime = ratingTime;
                            currentStep++;
                          });
                        }
                      }),
                  CreateRecommendationStep2(
                    onNext: (headline, comment) {
                      savePartnerReview(headline, comment);
                    },
                    onBack: () {
                      if (currentStep > 0) {
                        setState(() {
                          currentStep--;
                        });
                      }
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> savePartnerReview(String headline, String comment) async {
    var vm = context.read<RecommendationReadAnswerViewModel>();
    showLoadingDialog(context, loadingKey);
    await vm
        .savePartnerReviewOnHomeowner(
            comment: comment,
            customerId: int.parse(widget.id),
            enterpriseSum: 0,
            headline: headline,
            rating: rating,
            ratingAccess: ratingAccess,
            ratingCollaboration: ratingCollaboration,
            ratingCommunication: ratingCommunication,
            ratingFinancialSecurity: ratingFinancialSecurity,
            ratingRealisticExpectations: ratingRealisticExpectations,
            ratingTime: ratingTime)
        .then((value) async {
      if (!mounted) return;

      Navigator.of(loadingKey!.currentContext!).pop();
      if (value.success!) {
        await showSuccessAnimationDialog(context, true, tr('success'));
        if (mounted) {
          backDrawerRoute();
        }
      }
    }).catchError((onError) {
      Navigator.of(loadingKey!.currentContext!).pop();
    });
  }
}
