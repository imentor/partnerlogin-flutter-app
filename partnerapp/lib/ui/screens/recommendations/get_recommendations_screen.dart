import 'dart:developer';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/membership_benefits/contact_selector.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:Haandvaerker.dk/utils/extensions.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/widgets_recommendations_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fast_contacts/fast_contacts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:validators/validators.dart';

String hiredIconUrl = 'assets/images/hired.svg';
String filesIconUrl = 'assets/images/files.svg';

class GetRecommendationsScreen extends StatefulWidget {
  const GetRecommendationsScreen({super.key});

  @override
  GetRecommendationsScreenState createState() =>
      GetRecommendationsScreenState();
}

class GetRecommendationsScreenState extends State<GetRecommendationsScreen> {
  late TextEditingController nameController;
  late TextEditingController emailController;
  late TextEditingController phoneController;
  late FocusNode nameFocusNode;
  late FocusNode emailFocusNode;
  late FocusNode phoneFocusNode;

  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();
  bool busy = false;
  bool isGrantedContactPermission = false;

  @override
  void initState() {
    determinePermission();
    nameController = TextEditingController();
    emailController = TextEditingController();
    phoneController = TextEditingController();
    nameFocusNode = FocusNode();
    emailFocusNode = FocusNode();
    phoneFocusNode = FocusNode();

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final widgetsVm = context.read<WidgetsViewModel>();
      final newsFeedVm = context.read<NewsFeedViewModel>();
      final appDrawerVm = context.read<AppDrawerViewModel>();
      const dynamicStoryPage = 'anbefalinger';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;
      widgetsVm.setBusy(true);

      if (dynamicStoryEnable) {
        await Future.wait([
          widgetsVm.getInvitedCustomers(),
          newsFeedVm.getDynamicStory(page: dynamicStoryPage),
        ]).whenComplete(() {
          if (!mounted) return;

          widgetsVm.setBusy(false);
          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      } else {
        await Future.value(widgetsVm.getInvitedCustomers()).whenComplete(() {
          widgetsVm.setBusy(false);
        });
      }
    });
    super.initState();
  }

  void determinePermission() async {
    try {
      isGrantedContactPermission = await Permission.contacts.status.isGranted;
    } catch (e) {
      log("$e");
    }

    if (!mounted) return;
    setState(() {});
  }

  @override
  void dispose() {
    nameController.dispose();
    emailController.dispose();
    phoneController.dispose();
    nameFocusNode.dispose();
    emailFocusNode.dispose();
    phoneFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: KeyboardActions(
        tapOutsideBehavior: TapOutsideBehavior.opaqueDismiss,
        config: KeyboardActionsConfig(
          actions: [
            KeyboardActionsItem(
              focusNode: phoneFocusNode,
              displayDoneButton: false,
              displayArrows: false,
            ),
          ],
        ),
        child: SingleChildScrollView(
          key: const Key(Keys.getRecomScrollview),
          child: Container(
            margin: const EdgeInsets.all(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  tr('get_recommendations'),
                  style: Theme.of(context).textTheme.headlineLarge,
                ),
                SmartGaps.gapH10,
                Text(tr('get_recommendations_description_1'),
                    style: Theme.of(context).textTheme.bodyMedium),
                SmartGaps.gapH30,
                SmartGaps.gapH20,
                Center(
                  child: Row(
                    children: <Widget>[
                      SmartGaps.gapW20,
                      SvgPicture.asset(
                        filesIconUrl,
                        width: 40.0,
                        height: 40.0,
                        colorFilter: ColorFilter.mode(
                          Theme.of(context).colorScheme.primary,
                          BlendMode.srcIn,
                        ),
                      ),
                      SmartGaps.gapW20,
                      Text(
                        tr('stand_out_and_be_seen'),
                        style: Theme.of(context).textTheme.headlineSmall,
                      ),
                    ],
                  ),
                ),
                SmartGaps.gapH20,
                Center(
                  child: Row(
                    children: <Widget>[
                      SmartGaps.gapW20,
                      SvgPicture.asset(
                        hiredIconUrl,
                        width: 40.0,
                        height: 40.0,
                        colorFilter: ColorFilter.mode(
                          Theme.of(context).colorScheme.primary,
                          BlendMode.srcIn,
                        ),
                      ),
                      SmartGaps.gapW20,
                      Text(
                        tr('get_more_customers'),
                        style: Theme.of(context).textTheme.headlineSmall,
                      ),
                    ],
                  ),
                ),
                SmartGaps.gapH30,
                Text(tr('get_recommendations_description_2'),
                    style: Theme.of(context).textTheme.bodyMedium),
                SmartGaps.gapH30,
                Text(tr('get_recommendations_description_3'),
                    style: Theme.of(context).textTheme.bodyMedium),
                SmartGaps.gapH30,
                if (context.watch<WidgetsViewModel>().busy)
                  Center(child: loader())
                else
                  getForm(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getForm() {
    return Container(
      padding: const EdgeInsets.only(top: 20),
      child: Form(
        key: formkey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            isGrantedContactPermission
                ? ContactSelector(
                    onContactChanged: (Contact? contact) {
                      initFields(contact: contact);
                    },
                  )
                : Text(tr('needs_permission_to_select_from_contacts'),
                    style: Theme.of(context).textTheme.titleSmall!.copyWith(
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).colorScheme.primary)),
            SmartGaps.gapH20,
            Text(tr('customer_name'),
                style: Theme.of(context)
                    .textTheme
                    .titleSmall!
                    .copyWith(fontWeight: FontWeight.w500)),
            SmartGaps.gapH5,
            TextFormField(
                key: const Key(Keys.getRecommName),
                controller: nameController,
                focusNode: nameFocusNode,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    filled: true,
                    fillColor: Colors.grey[200],
                    hintStyle: Theme.of(context)
                        .textTheme
                        .bodyMedium!
                        .copyWith(fontWeight: FontWeight.w500),
                    hintText: tr('name')),
                validator: (value) {
                  if (value.isNullOrEmpty) {
                    return tr('please_enter_some_text');
                  }
                  return null;
                }),
            SmartGaps.gapH20,
            Text(tr('customer_email'),
                style: Theme.of(context)
                    .textTheme
                    .titleSmall!
                    .copyWith(fontWeight: FontWeight.w500)),
            SmartGaps.gapH5,
            TextFormField(
                key: const Key(Keys.getRecomEmail),
                keyboardType: TextInputType.emailAddress,
                controller: emailController,
                focusNode: emailFocusNode,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    filled: true,
                    fillColor: Colors.grey[200],
                    hintStyle: Theme.of(context)
                        .textTheme
                        .bodyMedium!
                        .copyWith(fontWeight: FontWeight.w500),
                    hintText: tr('email')),
                validator: (value) {
                  if (value.isNullOrEmpty) {
                    return tr('required');
                  }
                  if (value!.trim().isNotEmpty &&
                      isEmail(value.trim()) == false) {
                    return tr('enter_valid_email');
                  }

                  return null;
                }),
            SmartGaps.gapH20,
            Text(tr('phone'),
                style: Theme.of(context)
                    .textTheme
                    .titleSmall!
                    .copyWith(fontWeight: FontWeight.w500)),
            SmartGaps.gapH5,
            TextFormField(
                key: const Key(Keys.getRecomPhone),
                focusNode: phoneFocusNode,
                keyboardType: TextInputType.phone,
                controller: phoneController,
                maxLength: 8,
                decoration: InputDecoration(
                    counterText: '',
                    border: InputBorder.none,
                    filled: true,
                    fillColor: Colors.grey[200],
                    hintStyle: Theme.of(context)
                        .textTheme
                        .bodyMedium!
                        .copyWith(fontWeight: FontWeight.w500),
                    hintText: tr('phone_number')),
                validator: (value) {
                  if (value.isNullOrEmpty) {
                    return tr('required');
                  }

                  return null;
                }),
            SmartGaps.gapH30,
            SmartGaps.gapH20,
            SizedBox(
              width: 150,
              height: 50,
              child: CustomDesignTheme.flatButtonStyle(
                key: const Key(Keys.getRecomButton),
                disabledBackgroundColor: Colors.grey,
                backgroundColor: Theme.of(context).colorScheme.secondary,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(0),
                ),
                onPressed: context.watch<WidgetsViewModel>().busy
                    ? null
                    : () async {
                        if (formkey.currentState!.validate()) {
                          await sendRecommendation(
                            email: emailController.text.trim(),
                            telephone: phoneController.text.trim(),
                            requesterName: nameController.text.trim(),
                          );
                        }
                      },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(tr('send'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(color: Colors.white, height: 1)),
                    SmartGaps.gapW10,
                    const Icon(
                      ElementIcons.right,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
            ),
            SmartGaps.gapH20,
            ...context.watch<WidgetsViewModel>().invitedCustomers.map(
                  (e) => InvitedCustomerTemplate(
                    name: e.customerName!,
                    email: e.customerEmail!,
                    number: e.cutomerPhone!,
                    onDelete: () async =>
                        deleteInvitation(inviteId: e.id.toString()),
                  ),
                ),
          ],
        ),
      ),
    );
  }

  Future<void> deleteInvitation({required String inviteId}) async {
    //

    final widgetsVm = context.read<WidgetsViewModel>();

    final response = await showOkCancelAlertDialog(
      context: context,
      title: tr('delete'),
      message: tr('delete_customer_invite_record'),
      okLabel: tr('continue'),
      cancelLabel: tr('cancel'),
    );

    if (response == OkCancelResult.ok && mounted) {
      showLoadingDialog(context, loadingKey);

      try {
        final delete =
            await widgetsVm.deleteInvitedCustomer(inviteId: inviteId);

        if (delete && mounted) {
          Navigator.pop(loadingKey.currentContext!);
          showSuccessAnimationDialog(context, true, '');
          widgetsVm.setBusy(true);
          await widgetsVm.getInvitedCustomers();
          widgetsVm.setBusy(false);
        }
      } catch (_) {
        Navigator.pop(loadingKey.currentContext!);
        showSuccessAnimationDialog(
            myGlobals.homeScaffoldKey!.currentContext!, false, '');
      }
    }

    //
  }

  Future<void> sendRecommendation({
    required String email,
    required String telephone,
    required String requesterName,
  }) async {
    final widgetsVm = context.read<WidgetsViewModel>();

    showLoadingDialog(context, loadingKey);

    try {
      final response = await widgetsVm.inviteCustomer(
        name: requesterName,
        email: email,
        phone: telephone,
      );
      if (response && mounted) {
        Navigator.pop(loadingKey.currentContext!);
        nameController.clear();
        nameFocusNode.unfocus();
        emailController.clear();
        emailFocusNode.unfocus();
        phoneController.clear();
        phoneFocusNode.unfocus();
        showSuccessAnimationDialog(context, true, '');
        widgetsVm.setBusy(true);
        await widgetsVm.getInvitedCustomers();
        widgetsVm.setBusy(false);
      }
    } catch (_) {
      Navigator.pop(loadingKey.currentContext!);
      showSuccessAnimationDialog(
          myGlobals.homeScaffoldKey!.currentContext!, false, '');
    }
  }

  void initFields({Contact? contact}) {
    if (contact == null) {
      emailController.clear();
      phoneController.clear();
      nameController.clear();
    } else {
      try {
        emailController.text =
            contact.emails.isEmpty ? '' : contact.emails.first.address;
        phoneController.text =
            contact.phones.isEmpty ? '' : contact.phones.first.number;
        nameController.text =
            '${contact.structuredName!.displayName.displayEmptyIfNull()} ${contact.structuredName!.familyName.displayEmptyIfNull()}';
      } catch (e) {
        log("$e");
      }
    }
  }
}

class InvitedCustomerTemplate extends StatelessWidget {
  const InvitedCustomerTemplate({
    super.key,
    required this.name,
    required this.email,
    required this.number,
    required this.onDelete,
  });

  final String name;
  final String email;
  final String number;
  final VoidCallback onDelete;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black),
        borderRadius: BorderRadius.circular(5),
      ),
      padding: const EdgeInsets.all(5),
      margin: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('${tr('name')}: $name'),
              SmartGaps.gapH2,
              Text('${tr('email')}: $email'),
              SmartGaps.gapH2,
              Text('${tr('phone_number')}: $number'),
            ],
          ),
          const Spacer(),
          CustomDesignTheme.flatButtonStyle(
            backgroundColor: PartnerAppColors.red,
            padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
            onPressed: onDelete,
            child: Center(
              child: Text(
                tr('delete'),
                style: const TextStyle(color: Colors.white),
              ),
            ),
          ),
          SmartGaps.gapW10,
        ],
      ),
    );
  }
}
