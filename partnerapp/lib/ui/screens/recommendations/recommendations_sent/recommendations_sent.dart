import 'dart:async';

import 'package:Haandvaerker.dk/model/recommendation/partners_reviews_on_client.dart';
import 'package:Haandvaerker.dk/ui/components/cache_image.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/components/number_pagination.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/recommendations/components/edit_recommendation_sent_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/recommendations/recommendations_sent/recommendations_sent_search.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/recommendation_read_answer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class RecommendationsSentScreen extends StatefulWidget {
  const RecommendationsSentScreen({super.key});

  @override
  RecommendationsSentScreenState createState() =>
      RecommendationsSentScreenState();
}

class RecommendationsSentScreenState extends State<RecommendationsSentScreen> {
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();

  late ScrollController scroll;
  Timer? _debounce;
  final Duration _debounceDuration = const Duration(milliseconds: 600);

  @override
  void initState() {
    scroll = ScrollController();
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final recommendVm = context.read<RecommendationReadAnswerViewModel>();
      recommendVm.setSearchControllers();
      if ((recommendVm.partnerReviewsModel?.data ?? <PartnersReviewsOnClient>[])
          .isEmpty) {
        modalManager.showLoadingModal();
      }
      getReviews().whenComplete(() => modalManager.hideLoadingModal());
    });
  }

  @override
  void dispose() {
    scroll.dispose();
    _debounce?.cancel();
    super.dispose();
  }

  Future<void> getReviews({String? homeowner}) async {
    var viewModel = context.read<RecommendationReadAnswerViewModel>();

    setState(() {
      viewModel.getRecommendationData = true;
    });
    await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function:
            viewModel.getPartnersReviewsDoneOnClientV2(homeowner: homeowner));
    setState(() {
      viewModel.getRecommendationData = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        controller: scroll,
        floatHeaderSlivers: true,
        headerSliverBuilder: (context, _) {
          return [const DrawerAppBar(isSliver: true)];
        },
        body: Consumer<RecommendationReadAnswerViewModel>(
            builder: (_, viewModel, __) {
          return GestureDetector(
            onTap: () => (viewModel.searchFocusNode ?? FocusNode()).unfocus(),
            child: Container(
              height: double.infinity,
              padding: const EdgeInsets.only(top: 10, left: 20.0, right: 20.0),
              child: RefreshIndicator(
                onRefresh: getReviews,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(tr('all_recommendations'),
                          style: Theme.of(context).textTheme.headlineLarge),
                      SmartGaps.gapH20,
                      Text(tr('written_recommendation_desc'),
                          style: Theme.of(context).textTheme.bodyMedium),
                      SmartGaps.gapH20,
                      RecommendationsSentSearch(
                        onChanged: (value) {
                          if (_debounce?.isActive ?? false) _debounce?.cancel();
                          _debounce = Timer(_debounceDuration, () async {
                            viewModel.currentPage = 1;
                            getReviews(
                                homeowner: viewModel.searchController?.text);

                            scroll.animateTo(0,
                                duration: const Duration(milliseconds: 700),
                                curve: Curves.easeOut);
                          });
                        },
                        onClear: () async {
                          (viewModel.searchController ??
                                  TextEditingController())
                              .clear();
                          (viewModel.searchFocusNode ?? FocusNode()).unfocus();
                          viewModel.currentPage = 1;

                          getReviews();

                          scroll.animateTo(0,
                              duration: const Duration(milliseconds: 700),
                              curve: Curves.easeOut);
                        },
                      ),
                      SmartGaps.gapH20,
                      (viewModel.partnerReviewsModel?.data ??
                                  <PartnersReviewsOnClient>[])
                              .isEmpty
                          ? const EmptyListIndicator(
                              route: Routes.sentRecommendations)
                          : Skeletonizer(
                              enabled: viewModel.getRecommendationData,
                              child: ListView.builder(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount: (viewModel
                                                .partnerReviewsModel?.data ??
                                            <PartnersReviewsOnClient>[])
                                        .isEmpty
                                    ? 0
                                    : (viewModel.partnerReviewsModel?.data ??
                                            <PartnersReviewsOnClient>[])
                                        .length,
                                itemBuilder: (BuildContext context, int index) {
                                  var model =
                                      (viewModel.partnerReviewsModel?.data ??
                                          <PartnersReviewsOnClient>[])[index];
                                  return RecommendationSentTile(
                                    review: model,
                                    onEdit: editReview,
                                    onDelete: deleteReviewById,
                                  );
                                },
                              ),
                            ),
                      if ((viewModel.partnerReviewsModel?.data ??
                              <PartnersReviewsOnClient>[])
                          .isNotEmpty) ...[
                        SmartGaps.gapH20,
                        NumberPagination(
                          currentPage: viewModel.currentPage,
                          totalPages: viewModel.lastPage,
                          onPageChanged: (page) async {
                            scroll.animateTo(0,
                                duration: const Duration(milliseconds: 700),
                                curve: Curves.easeOut);
                            viewModel.currentPage = page;
                            getReviews();
                          },
                        ),
                      ],
                    ],
                  ),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }

  Future<void> deleteReviewById(int? id) async {
    var viewModel = context.read<RecommendationReadAnswerViewModel>();

    var response = await deleteConfirmDialog(context);

    if (response && mounted) {
      showLoadingDialog(context, loadingKey);
      await tryCatchWrapper(
              context: myGlobals.homeScaffoldKey!.currentContext,
              function: viewModel.deleteReviewById(reviewId: id))
          .then((value) async {
        await viewModel.getPartnersReviewsDoneOnClientV2();
        Navigator.of(loadingKey.currentContext!, rootNavigator: true).pop();
        await showSuccessAnimationDialog(
            myGlobals.homeScaffoldKey!.currentContext!, value!, 'Success');
      });
    }
  }

  Future<void> editReview(PartnersReviewsOnClient review) async {
    showDialog(
        context: context,
        builder: (context) {
          return EditReviewDialog(
              review: review,
              onSubmit: (
                headline,
                comment,
                ratingAccess,
                ratingCollaboration,
                ratingCommunication,
                ratingFinancialSecurity,
                ratingRealisticExpectations,
                ratingTime,
              ) async {
                var viewModel =
                    context.read<RecommendationReadAnswerViewModel>();

                showLoadingDialog(context, loadingKey);
                await tryCatchWrapper(
                        context: myGlobals.homeScaffoldKey!.currentContext,
                        function: viewModel.updateSavePartnerReviewonHomeowner(
                            reviewId: review.id,
                            accessRating: ratingAccess!,
                            collaborationRating: ratingCollaboration!,
                            comment: comment,
                            communicationRating: ratingCommunication!,
                            customerId: review.ufCustomerId,
                            enterpriseSum: 0,
                            financialRating: ratingFinancialSecurity!,
                            headline: headline,
                            realisticExpectationRating:
                                ratingRealisticExpectations!,
                            timeRating: ratingTime!))
                    .then((value) async {
                  await viewModel.getPartnersReviewsDoneOnClientV2();
                  Navigator.of(loadingKey.currentContext!, rootNavigator: true)
                      .pop();
                  await showSuccessAnimationDialog(
                      myGlobals.homeScaffoldKey!.currentContext!,
                      value!,
                      value ? 'Success' : '');
                  if (value && mounted) {
                    Navigator.of(myGlobals.homeScaffoldKey!.currentContext!,
                            rootNavigator: true)
                        .pop();
                  }
                });
              });
        });
  }
}

class RecommendationSentTile extends StatelessWidget {
  const RecommendationSentTile(
      {super.key,
      required this.review,
      required this.onDelete,
      required this.onEdit});

  final PartnersReviewsOnClient review;
  final Function(int?) onDelete;
  final Function(PartnersReviewsOnClient) onEdit;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 170,
      margin: const EdgeInsets.only(bottom: 20),
      padding: const EdgeInsets.all(10),
      decoration: CustomDesignTheme.coldBlueBorderShadows,
      child: Material(
        child: Row(
          children: [
            if ((review.homeOwnerName?.avatar ?? '').isNotEmpty)
              Expanded(
                child: AspectRatio(
                  aspectRatio: 2 / 3,
                  child: CacheImage(
                      imageUrl: review.homeOwnerName?.avatar ?? '',
                      fit: BoxFit.fitHeight),
                ),
              ),
            if ((review.homeOwnerName?.avatar ?? '').isEmpty)
              const Expanded(
                child: AspectRatio(
                  aspectRatio: 2 / 3,
                  child: Image(
                      image: AssetImage(ImagePaths.sampleImageCover),
                      fit: BoxFit.fitHeight),
                ),
              ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RatingBar(
                      initialRating: review.ufRatingOverall!,
                      allowHalfRating: true,
                      itemSize: 20,
                      ratingWidget: RatingWidget(
                        full: Image.asset(ImagePaths.singleHouse),
                        half: Image.asset(ImagePaths.halfHouse),
                        empty: Opacity(
                          opacity: 0.1,
                          child: Image.asset(ImagePaths.singleHouse),
                        ),
                      ),
                      itemPadding: const EdgeInsets.symmetric(horizontal: 1.0),
                      onRatingUpdate: (rating) {},
                      ignoreGestures: true,
                    ),
                    Text(
                      review.ufHeadline ?? '',
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(
                          color: Theme.of(context).colorScheme.primary,
                          fontWeight: FontWeight.w600),
                    ),
                    Text(
                      review.homeOwnerName?.name ?? '',
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context)
                          .textTheme
                          .bodyMedium!
                          .copyWith(fontWeight: FontWeight.w300),
                    ),
                    Text(
                      Formatter.formatDateStrings(
                          type: DateFormatType.standardDate,
                          dateString: review.ufReviewDate),
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context)
                          .textTheme
                          .bodyMedium!
                          .copyWith(fontWeight: FontWeight.normal),
                    ),
                    Text(
                      review.ufComment ?? '',
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(color: Colors.grey, fontSize: 15),
                    ),
                    if ((review.ufAnswer ?? '').isNotEmpty)
                      InkWell(
                        onTap: () {
                          viewReply(context, review);
                        },
                        child: const Text(
                          'View reply',
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(color: Colors.black, fontSize: 15),
                        ),
                      ),
                    SmartGaps.gapH10,
                  ],
                ),
              ),
            ),
            SizedBox(
                width: 50,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          onEdit(review);
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color:
                                      Theme.of(context).colorScheme.primary)),
                          child: Icon(ElementIcons.edit_outline,
                              size: 15,
                              color: Theme.of(context).colorScheme.primary),
                        ),
                      ),
                    ),
                    SmartGaps.gapH20,
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          onDelete(review.id);
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.red)),
                          child: const Icon(ElementIcons.delete,
                              size: 15, color: Colors.red),
                        ),
                      ),
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }

  Future<void> viewReply(
      BuildContext context, PartnersReviewsOnClient review) async {
    showDialog(
        context: context,
        builder: (context) {
          return ReviewReplyDialog(review: review);
        });
  }
}

class ReviewReplyDialog extends StatelessWidget {
  const ReviewReplyDialog({super.key, required this.review});

  final PartnersReviewsOnClient review;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width / 1.1,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(3), color: Colors.white),
          padding: const EdgeInsets.all(20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('View reply',
                        style: Theme.of(context).textTheme.headlineLarge),
                    IconButton(
                        icon: const Icon(ElementIcons.circle_close),
                        onPressed: () {
                          Navigator.of(context).pop();
                        }),
                  ],
                ),
                SmartGaps.gapH10,
                SizedBox(
                  height: 170,
                  child: Row(
                    children: [
                      const Expanded(
                        child: AspectRatio(
                          aspectRatio: 2 / 3,
                          child: Image(
                            image: AssetImage(ImagePaths.sampleImageCover),
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              RatingBar(
                                initialRating: review.ufRatingOverall!,
                                allowHalfRating: true,
                                itemSize: 20,
                                ratingWidget: RatingWidget(
                                  full: Image.asset(ImagePaths.singleHouse),
                                  half: Image.asset(ImagePaths.halfHouse),
                                  empty: Opacity(
                                    opacity: 0.1,
                                    child: Image.asset(ImagePaths.singleHouse),
                                  ),
                                ),
                                itemPadding:
                                    const EdgeInsets.symmetric(horizontal: 1.0),
                                onRatingUpdate: (rating) {},
                                ignoreGestures: true,
                              ),
                              Text(
                                review.ufHeadline!,
                                overflow: TextOverflow.ellipsis,
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium!
                                    .copyWith(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                        fontWeight: FontWeight.w600),
                              ),
                              Text(
                                Formatter.formatDateStrings(
                                    type: DateFormatType.standardDate,
                                    dateString: review.ufReviewDate),
                                overflow: TextOverflow.ellipsis,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyMedium!
                                    .copyWith(fontWeight: FontWeight.normal),
                              ),
                              Text(
                                review.ufComment!,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                    color: Colors.grey, fontSize: 15),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const Divider(
                  thickness: 1,
                  color: Colors.grey,
                ),
                SmartGaps.gapH5,
                Container(
                  padding: const EdgeInsets.all(10),
                  color: Colors.grey[200],
                  width: double.maxFinite,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        children: [
                          const CircleAvatar(
                            radius: 25.0,
                            backgroundImage:
                                AssetImage("assets/launcher/h.png"),
                          ),
                          SmartGaps.gapH10,
                          Text(
                            review.homeOwnerName?.name ?? '',
                            overflow: TextOverflow.ellipsis,
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(fontWeight: FontWeight.w300),
                          ),
                        ],
                      ),
                      SmartGaps.gapW10,
                      Flexible(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              Formatter.formatDateStrings(
                                  type: DateFormatType.standardDate,
                                  dateString: review.ufReviewDate),
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(fontWeight: FontWeight.normal),
                            ),
                            SmartGaps.gapH5,
                            Text(
                              '" ${review.ufAnswer} "',
                              overflow: TextOverflow.ellipsis,
                              maxLines: 3,
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
