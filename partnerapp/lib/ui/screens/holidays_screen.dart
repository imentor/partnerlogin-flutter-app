import 'package:Haandvaerker.dk/model/settings/holiday_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:Haandvaerker.dk/utils/extension/snackbar.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/holidays_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class HolidayScreen extends StatefulWidget {
  const HolidayScreen({super.key});

  @override
  State<HolidayScreen> createState() => _HolidayScreenState();
}

class _HolidayScreenState extends State<HolidayScreen> {
  Future<void> getHolidays() async {
    final viewModel = context.read<HolidaysViewModel>();

    viewModel.partnerHolidays = <HolidayModel>[];
    viewModel.isGettingHolidayData = true;
    await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function: Future.value(viewModel.getHolidays()));
    viewModel.isGettingHolidayData = false;
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final appDrawerVm = context.read<AppDrawerViewModel>();
      final newsFeedVm = context.read<NewsFeedViewModel>();
      const dynamicStoryPage = 'Ferieperioder';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      getHolidays();
      if (dynamicStoryEnable) {
        await Future.value(newsFeedVm.getDynamicStory(page: dynamicStoryPage))
            .whenComplete(() {
          if (!mounted) return;

          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final holidaysVm = context.read<HolidaysViewModel>();
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                tr('your_holidays'),
                style: Theme.of(context).textTheme.headlineLarge,
              ),
              SmartGaps.gapH10,
              Text(
                tr('your_holidays_desc'),
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              SmartGaps.gapH30,
              if (holidaysVm.isGettingHolidayData)
                SizedBox(
                  height: 200,
                  child: Center(
                    child: loader(),
                  ),
                )
              else ...[
                if (context
                    .watch<HolidaysViewModel>()
                    .availableDates
                    .isNotEmpty) ...[
                  const ChooseToFromDate(),
                  SmartGaps.gapH30,
                  const HolidayPeriods(),
                ] else
                  const ChooseToFromDate(),
              ],
            ],
          ),
        ),
      ),
    );
  }
}

class ChooseToFromDate extends StatefulWidget {
  const ChooseToFromDate({super.key});

  @override
  State<ChooseToFromDate> createState() => _ChooseToFromDateState();
}

class _ChooseToFromDateState extends State<ChooseToFromDate> {
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();
  GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();

  late TextEditingController _fromController;
  late TextEditingController _toController;

  @override
  void initState() {
    _fromController = TextEditingController();
    _toController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _fromController.dispose();
    _toController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            tr('choose_new_holiday_date'),
            style: Theme.of(context)
                .textTheme
                .titleLarge!
                .copyWith(fontWeight: FontWeight.normal),
          ),
          SmartGaps.gapH10,
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(right: 8),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(tr('from'),
                          style: Theme.of(context)
                              .textTheme
                              .titleSmall!
                              .copyWith(fontWeight: FontWeight.w700)),
                      SmartGaps.gapH10,
                      InkWell(
                        onTap: () async {
                          final holidaysVm = context.read<HolidaysViewModel>();
                          FocusScope.of(context).unfocus();
                          final date = await showDatePicker(
                            context: context,
                            locale: context.locale,
                            initialDate: holidaysVm.fromDate ?? DateTime.now(),
                            firstDate: DateTime.now(),
                            lastDate: DateTime(3000),
                          );

                          if (date != null && mounted) {
                            holidaysVm.fromDate = date;
                            String month = date.month.toString();
                            if (month.isNotEmpty && month != '12') {
                              month = '0$month';
                            }
                            _fromController.text =
                                '${date.day}.$month.${date.year}';
                          }
                        },
                        child: IgnorePointer(
                            child: _templateTextField(
                                controller: _fromController)),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(tr('to'),
                        style: Theme.of(context)
                            .textTheme
                            .titleSmall!
                            .copyWith(fontWeight: FontWeight.w700)),
                    SmartGaps.gapH10,
                    InkWell(
                      onTap: () async {
                        final holidaysVm = context.read<HolidaysViewModel>();
                        FocusScope.of(context).unfocus();
                        final date = await showDatePicker(
                          context: context,
                          locale: context.locale,
                          initialDate: holidaysVm.toDate ?? DateTime.now(),
                          firstDate: DateTime.now(),
                          lastDate: DateTime(3000),
                        );

                        if (date != null && mounted) {
                          holidaysVm.toDate = date;
                          String month = date.month.toString();
                          if (month.isNotEmpty && month != '12') {
                            month = '0$month';
                          }
                          _toController.text =
                              '${date.day}.$month.${date.year}';
                        }
                      },
                      child: IgnorePointer(
                          child: _templateTextField(controller: _toController)),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SmartGaps.gapH20,
          SizedBox(
            height: 60,
            width: double.infinity,
            child: CustomDesignTheme.flatButtonStyle(
                backgroundColor: Theme.of(context).colorScheme.secondary,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      tr('save'),
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(color: Colors.white, height: 1),
                    ),
                    SmartGaps.gapW10,
                    const Icon(
                      ElementIcons.right,
                      color: Colors.white,
                    ),
                  ],
                ),
                onPressed: () async {
                  if (formKey.currentState!.validate()) {
                    await saveHoliday();
                    _fromController.clear();
                    _toController.clear();
                  }
                }),
          ),
        ],
      ),
    );
  }

  TextFormField _templateTextField(
      {required TextEditingController controller}) {
    return TextFormField(
      validator: (value) {
        if (value!.isEmpty) {
          tr('date_validator');
        }
        return null;
      },
      textAlign: TextAlign.left,
      controller: controller,
      decoration: InputDecoration(
        errorStyle: const TextStyle(height: 0),
        suffixIcon: Padding(
          padding: const EdgeInsets.all(10.0),
          child: SvgPicture.asset(
            'assets/images/calendar.svg',
            width: 27,
            colorFilter: const ColorFilter.mode(
              PartnerAppColors.accentBlue,
              BlendMode.srcIn,
            ),
          ),
        ),
        counterText: '',
        hintText: tr('date_hint_text'),
        contentPadding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        border: OutlineInputBorder(
          borderSide: const BorderSide(color: PartnerAppColors.accentBlue),
          borderRadius: BorderRadius.circular(3),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: PartnerAppColors.accentBlue),
          borderRadius: BorderRadius.circular(3),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: PartnerAppColors.accentBlue),
          borderRadius: BorderRadius.circular(3),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: PartnerAppColors.accentBlue),
          borderRadius: BorderRadius.circular(3),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: PartnerAppColors.accentBlue),
          borderRadius: BorderRadius.circular(3),
        ),
      ),
    );
  }

  Future<void> saveHoliday() async {
    var viewModel = context.read<HolidaysViewModel>();

    showLoadingDialog(context, loadingKey);

    // await viewModel
    //     .serviceFree(datechooseqfreestart: fromDate, datechooseqfreeend: toDate)
    await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function: viewModel.addHoliday(
          from: Formatter.formatDateTime(
              type: DateFormatType.isoDate, date: viewModel.fromDate),
          to: Formatter.formatDateTime(
              type: DateFormatType.isoDate, date: viewModel.toDate),
        )).then((value) async {
      Navigator.of(loadingKey.currentContext!).pop();
      showSuccessAnimationDialog(myGlobals.homeScaffoldKey!.currentContext!,
          (value?.success ?? false), (value?.message ?? ''));
    }).catchError((onError) {
      if (!mounted) return;

      Navigator.of(loadingKey.currentContext!).pop();
      context.showErrorSnackBar();
    });
  }
}

class HolidayPeriods extends StatefulWidget {
  const HolidayPeriods({super.key});

  @override
  HolidayPeriodsState createState() => HolidayPeriodsState();
}

class HolidayPeriodsState extends State<HolidayPeriods> {
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    var viewModel = Provider.of<HolidaysViewModel>(context, listen: true);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(tr('your_holiday_periods'),
            style: Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(fontWeight: FontWeight.w700)),
        viewModel.busy
            ? const Center(child: ConnectivityLoader())
            : Scrollbar(
                controller: _scrollController,
                child: SingleChildScrollView(
                  controller: _scrollController,
                  physics: const ClampingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  child: DataTable(
                    columnSpacing: 20,
                    columns: <DataColumn>[
                      DataColumn(
                        label: Text(tr('from'),
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(fontWeight: FontWeight.w700)),
                      ),
                      DataColumn(
                        label: Text(tr('to'),
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(fontWeight: FontWeight.w700)),
                      ),
                      const DataColumn(label: Text('')),
                    ],
                    rows: viewModel.partnerHolidays
                        .map((holiday) => DataRow(cells: <DataCell>[
                              DataCell(Text(holiday.holidayFrom ?? '',
                                  style:
                                      Theme.of(context).textTheme.titleSmall)),
                              DataCell(Text(holiday.holidayTo ?? '',
                                  style:
                                      Theme.of(context).textTheme.titleSmall)),
                              DataCell(CustomDesignTheme.flatButtonStyle(
                                child: Icon(ElementIcons.delete,
                                    color: Theme.of(context).colorScheme.error,
                                    size: 18),
                                onPressed: () =>
                                    deleteHoliday(holidayId: holiday.id),
                              ))
                            ]))
                        .toList(),
                  ),
                ),
              )
      ],
    );
  }

  Future<void> deleteHoliday({required holidayId}) async {
    bool result = await deleteConfirmDialog(context);
    if (result && mounted) {
      var viewModel = context.read<HolidaysViewModel>();
      showLoadingDialog(context, loadingKey);

      viewModel.deleteHoliday(holidayId: holidayId).then((value) async {
        Navigator.of(loadingKey.currentContext!).pop();
        showSuccessAnimationDialog(myGlobals.homeScaffoldKey!.currentContext!,
            (value.success ?? false), value.message);
        if ((value.success ?? false)) {
          viewModel.setBusy(true);
          await viewModel.getHolidays();
          viewModel.setBusy(false);
        }
      }).catchError((onError) {
        if (!mounted) return;

        Navigator.of(loadingKey.currentContext!).pop();
        context.showErrorSnackBar();
      });
    } else {
      setState(() {});
    }
  }
}
