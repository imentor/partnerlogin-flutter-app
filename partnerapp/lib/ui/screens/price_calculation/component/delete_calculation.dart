import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/price_calculation/price_calculation_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

Future<String?> deleteCalculationDialog({
  required BuildContext context,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: const SingleChildScrollView(child: DeleteCalculation())),
      );
    },
  );
}

Future<List<Map<String, dynamic>>?> priceSurveyDialog({
  required BuildContext context,
  required String reason,
  required List<Map<String, dynamic>> industryDescriptions,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: PriceSurvey(
              reason: reason,
              industryDescriptions: industryDescriptions,
            ))),
      );
    },
  );
}

class DeleteCalculation extends StatefulWidget {
  const DeleteCalculation({super.key});

  @override
  State<DeleteCalculation> createState() => _DeleteCalculationState();
}

class _DeleteCalculationState extends State<DeleteCalculation> {
  String? reason;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          tr('delete_calculation_confirmation'),
          style: Theme.of(context).textTheme.displayLarge!.copyWith(
                fontSize: 16,
                fontWeight: FontWeight.w400,
                color: PartnerAppColors.darkBlue,
              ),
        ),
        SmartGaps.gapH20,
        ...List.generate(3, (index) {
          String value = tr('price_too_high');
          String valueTitle = tr('price_too_high');

          switch (index) {
            case 0:
              value = tr('price_too_high');
              valueTitle = tr('price_too_high');
              break;
            case 1:
              value = tr('price_too_low');
              valueTitle = tr('price_too_low');
              break;
            case 2:
              value = tr('other_reason');
              valueTitle = tr('other_reason');
              break;
            default:
              value = tr('price_too_high');
              valueTitle = tr('price_too_high');
          }

          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: RadioListTile(
                title: Text(valueTitle),
                value: value,
                groupValue: reason,
                activeColor: PartnerAppColors.blue,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                    side: const BorderSide(color: PartnerAppColors.darkBlue)),
                onChanged: (val) {
                  setState(() {
                    reason = val;
                  });
                }),
          );
        }),
        SmartGaps.gapH30,
        CustomDesignTheme.flatButtonStyle(
          height: 50,
          backgroundColor: reason == null
              ? PartnerAppColors.spanishGrey
              : PartnerAppColors.darkBlue,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
          onPressed: () {
            if (reason != null) {
              Navigator.of(context).pop(reason);
            }
          },
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text(
              tr('next'),
              style: Theme.of(context).textTheme.displayLarge!.copyWith(
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                  ),
            )
          ]),
        ),
      ],
    );
  }
}

class PriceSurvey extends StatefulWidget {
  const PriceSurvey({
    super.key,
    required this.reason,
    required this.industryDescriptions,
  });

  final String reason;
  final List<Map<String, dynamic>> industryDescriptions;

  @override
  PriceSurveyState createState() => PriceSurveyState();
}

class PriceSurveyState extends State<PriceSurvey> {
  List<Map<String, dynamic>> industryDescriptions = [];

  final formKey = GlobalKey<FormBuilderState>();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      List<Map<String, dynamic>> tempIndustryDescriptions = [];

      for (var industry in widget.industryDescriptions) {
        for (var subIndustry in (industry['subIndustry'] as List)) {
          final tempSubIndustry = subIndustry as Map<String, dynamic>;

          tempSubIndustry.putIfAbsent(
              'originalPrice', () => tempSubIndustry['price_max']);

          tempIndustryDescriptions.add(tempSubIndustry);
        }
      }

      setState(() {
        industryDescriptions.addAll(tempIndustryDescriptions);
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: formKey,
      child: Column(
        children: [
          if (widget.reason == tr('other_reason')) ...[
            CustomTextFieldFormBuilder(
              name: 'notes',
              minLines: 5,
              labelText: tr('write_reason_here'),
              validator:
                  FormBuilderValidators.required(errorText: tr('required')),
            )
          ] else ...[
            Text(
              tr('price_expectation'),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.displayLarge!.copyWith(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: PartnerAppColors.darkBlue,
                  ),
            ),
            SmartGaps.gapH20,
            ListView.separated(
              itemBuilder: (context, index) {
                final subIndustryMap = industryDescriptions[index];

                return Card(
                  child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: Text(subIndustryMap['subIndustryName'],
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleSmall!
                                        .copyWith(
                                            fontSize: 14,
                                            color: PartnerAppColors.darkBlue,
                                            fontWeight: FontWeight.normal)),
                              ),
                              Container(
                                padding: const EdgeInsets.all(10),
                                width: 150,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(3),
                                    border: Border.all(
                                        color: PartnerAppColors.darkBlue
                                            .withValues(alpha: .4))),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Text(NumberFormat.currency(
                                              locale: 'da',
                                              symbol: 'kr.',
                                              decimalDigits:
                                                  subIndustryMap['price_max'] ==
                                                          0
                                                      ? 0
                                                      : 2)
                                          .format(subIndustryMap['price_max'])),
                                    ]),
                              )
                            ],
                          ),
                          SmartGaps.gapH20,
                          CustomSliderFormBuilder(
                            name: 'updatePrice',
                            initialValue:
                                num.parse('${subIndustryMap['price_max']}')
                                    .toDouble(),
                            max: num.parse('${subIndustryMap['price_max']}') +
                                (num.parse('${subIndustryMap['price_max']}') *
                                    2),
                            min: 1000,
                            divisions: 50,
                            displayValues: DisplayValues.none,
                            onChanged: (p0) {
                              setState(() {
                                industryDescriptions[index]['price_max'] = p0;
                              });
                            },
                          ),
                        ],
                      )),
                );
              },
              separatorBuilder: (context, index) {
                return SmartGaps.gapH20;
              },
              itemCount: industryDescriptions.length,
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
            ),
          ],
          SmartGaps.gapH30,
          CustomDesignTheme.flatButtonStyle(
            height: 50,
            backgroundColor: PartnerAppColors.malachite,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            onPressed: () {
              if (widget.reason == tr('other_reason')) {
                Navigator.of(context).pop([
                  {"reason": formKey.currentState!.fields['notes']!.value}
                ]);
              } else {
                List<Map<String, dynamic>> payloadList = [];
                for (var element in industryDescriptions) {
                  payloadList.add({
                    "industryId": element['subIndustryId'],
                    "jobIndustryId": element['productTypeId'],
                    "suggestedPrice": element['price_max'],
                    "Price": element['originalPrice'],
                    "reason": widget.reason
                  });
                }

                Navigator.of(context).pop(payloadList);
              }
            },
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Text(
                tr('submit'),
                style: Theme.of(context).textTheme.displayLarge!.copyWith(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                    ),
              )
            ]),
          ),
        ],
      ),
    );
  }
}

Future<void> writeReviewDialog({required BuildContext context}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(40),
          contentPadding: const EdgeInsets.all(20),
          title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            InkWell(
                onTap: () {
                  Navigator.of(dialogContext).pop();
                },
                child: const Icon(
                  FeatherIcons.x,
                  color: PartnerAppColors.spanishGrey,
                )),
          ]),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: const SingleChildScrollView(child: WriteReviewDialog()),
          ),
        );
      });
}

class WriteReviewDialog extends StatefulWidget {
  const WriteReviewDialog({super.key});

  @override
  State<WriteReviewDialog> createState() => _WriteReviewDialogState();
}

class _WriteReviewDialogState extends State<WriteReviewDialog> {
  final formKey = GlobalKey<FormBuilderState>();

  void onPressedSubmit() async {
    final priceCalculationVm = context.read<PriceCalculationViewmodel>();

    final navigator = Navigator.of(context);

    if ((formKey.currentState!).validate()) {
      modalManager.showLoadingModal();
      final reviewCommentValue = formKey.currentState?.fields['review']?.value;
      if (reviewCommentValue != null) {
        List<Future<void>> futures = [];
        futures.add(priceCalculationVm
            .saveAiPriceSurvey(payload: {"review": reviewCommentValue}));

        await tryCatchWrapper(context: context, function: Future.wait(futures))
            .whenComplete(() {
          if (!mounted) return;

          formKey.currentState?.reset();
          modalManager.hideLoadingModal();
          navigator.pop();
          replySuccessDialog(context: context);
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            tr('give_us_feedback'),
            style: Theme.of(context)
                .textTheme
                .labelLarge!
                .copyWith(fontWeight: FontWeight.normal),
          ),
          SmartGaps.gapH5,
          CustomTextFieldFormBuilder(
              minLines: 5,
              name: 'review',
              hintText: tr('write_here'),
              validator:
                  FormBuilderValidators.required(errorText: tr('required'))),
          SmartGaps.gapH30,
          CustomDesignTheme.flatButtonStyle(
            height: 50,
            width: double.infinity,
            backgroundColor: PartnerAppColors.malachite,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
              side: const BorderSide(
                color: PartnerAppColors.malachite,
              ),
            ),
            child: Text(tr('submit'),
                style: context.pttTitleLarge.copyWith(
                  fontSize: 15,
                  color: Colors.white,
                )),
            onPressed: () => onPressedSubmit(),
          ),
        ],
      ),
    );
  }
}

Future<void> replySuccessDialog({required BuildContext context}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: const EdgeInsets.all(20),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(
                  onTap: () {
                    Navigator.of(dialogContext).pop();
                  },
                  child: const Icon(
                    FeatherIcons.x,
                    color: PartnerAppColors.spanishGrey,
                  )),
            ],
          ),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Icon(
                    Icons.check_circle_outline,
                    color: PartnerAppColors.malachite,
                    size: 70,
                  ),
                  SmartGaps.gapH20,
                  Text(
                    tr('success!'),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: Colors.black,
                        ),
                  ),
                  SmartGaps.gapH20,
                  CustomDesignTheme.flatButtonStyle(
                    backgroundColor: Theme.of(context).colorScheme.secondary,
                    onPressed: () async {
                      Navigator.pop(dialogContext);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 10),
                      child: Text(
                        tr('close'),
                        style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                              letterSpacing: 1.1,
                            ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      });
}
