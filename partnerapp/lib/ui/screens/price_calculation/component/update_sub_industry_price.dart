import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

Future<double?> updateSubIndustryPriceDialog({
  required BuildContext context,
  required String subIndustryName,
  required double price,
  required double priceMin,
  required double priceMax,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: UpdateSubIndustryPrice(
                    price: price,
                    priceMax: priceMax,
                    priceMin: priceMin,
                    subIndustryName: subIndustryName))),
      );
    },
  );
}

class UpdateSubIndustryPrice extends StatefulWidget {
  const UpdateSubIndustryPrice(
      {super.key,
      required this.price,
      required this.priceMax,
      required this.priceMin,
      required this.subIndustryName});

  final String subIndustryName;
  final double price;
  final double priceMin;
  final double priceMax;

  @override
  State<UpdateSubIndustryPrice> createState() => _UpdateSubIndustryPriceState();
}

class _UpdateSubIndustryPriceState extends State<UpdateSubIndustryPrice> {
  double? updatedPrice;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: Text(widget.subIndustryName,
                  style: Theme.of(context).textTheme.titleSmall!.copyWith(
                      fontSize: 14,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal)),
            ),
            InkWell(
              onTap: () {
                showTextInputDialog(
                  context: context,
                  textFields: [
                    DialogTextField(
                        initialText: updatedPrice == null
                            ? '${widget.price}'
                            : '$updatedPrice',
                        validator: (value) {
                          if ((value ?? '').isEmpty) {
                            return tr('required');
                          }
                          return null;
                        },
                        keyboardType: TextInputType.number,
                        suffixText: 'kr.  '),
                  ],
                  style: AdaptiveStyle.iOS,
                  title: widget.subIndustryName,
                  cancelLabel: tr('cancel'),
                  okLabel: tr('update'),
                ).then((value) {
                  if (value != null) {
                    setState(() {
                      updatedPrice = double.tryParse(value.first);
                    });
                  }
                });
              },
              child: Container(
                padding: const EdgeInsets.all(10),
                width: 150,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(3),
                    border: Border.all(
                        color:
                            PartnerAppColors.darkBlue.withValues(alpha: .4))),
                child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                  SizedBox(
                    height: 19,
                    width: 150 * 0.85,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                          Formatter.curencyFormat(
                              amount: updatedPrice ?? widget.price),
                          overflow: TextOverflow.ellipsis),
                    ),
                  ),
                ]),
              ),
            )
          ],
        ),
        if (widget.priceMin < widget.priceMax) ...[
          SmartGaps.gapH20,
          CustomSliderFormBuilder(
            name: 'updatePrice',
            initialValue: widget.price,
            max: widget.priceMax,
            min: widget.priceMin,
            divisions: 50,
            displayValues: DisplayValues.none,
            onChanged: (p0) {
              setState(() {
                updatedPrice = p0;
              });
            },
          ),
        ],
        SmartGaps.gapH30,
        CustomDesignTheme.flatButtonStyle(
          height: 50,
          backgroundColor: updatedPrice == null
              ? PartnerAppColors.spanishGrey
              : PartnerAppColors.darkBlue,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
          onPressed: () {
            if (updatedPrice != null) {
              Navigator.of(context).pop(updatedPrice);
            }
          },
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text(
              tr('done'),
              style: Theme.of(context).textTheme.displayLarge!.copyWith(
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                  ),
            )
          ]),
        ),
      ],
    );
  }
}

Future<Map<String, dynamic>?> updateSubIndustryPriceAndDescriptionDialog({
  required BuildContext context,
  required String subIndustryName,
  required double price,
  required double priceMin,
  required double priceMax,
  required List<String> descriptions,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: UpdateSubIndustryPriceAndDescription(
              price: price,
              priceMax: priceMax,
              priceMin: priceMin,
              subIndustryName: subIndustryName,
              descriptions: descriptions,
            ))),
      );
    },
  );
}

class UpdateSubIndustryPriceAndDescription extends StatefulWidget {
  const UpdateSubIndustryPriceAndDescription({
    super.key,
    required this.price,
    required this.priceMax,
    required this.priceMin,
    required this.subIndustryName,
    required this.descriptions,
  });

  final String subIndustryName;
  final double price;
  final double priceMin;
  final double priceMax;
  final List<String> descriptions;

  @override
  State<UpdateSubIndustryPriceAndDescription> createState() =>
      _UpdateSubIndustryPriceAndDescriptionState();
}

class _UpdateSubIndustryPriceAndDescriptionState
    extends State<UpdateSubIndustryPriceAndDescription> {
  double? updatedPrice;

  List<String> descriptions = [];

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      setState(() {
        descriptions = [...widget.descriptions];
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: Text(widget.subIndustryName,
                  style: Theme.of(context).textTheme.titleSmall!.copyWith(
                      fontSize: 14,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal)),
            ),
            InkWell(
              onTap: () {
                showTextInputDialog(
                  context: context,
                  textFields: [
                    DialogTextField(
                        initialText: updatedPrice == null
                            ? '${widget.price}'
                            : '$updatedPrice',
                        validator: (value) {
                          if ((value ?? '').isEmpty) {
                            return tr('required');
                          }
                          return null;
                        },
                        keyboardType: TextInputType.number,
                        suffixText: 'kr.  ')
                  ],
                  style: AdaptiveStyle.iOS,
                  title: widget.subIndustryName,
                  cancelLabel: tr('cancel'),
                  okLabel: tr('update'),
                ).then((value) {
                  if (value != null) {
                    setState(() {
                      updatedPrice = double.tryParse(value.first);
                    });
                  }
                });
              },
              child: Container(
                padding: const EdgeInsets.all(10),
                width: 150,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(3),
                    border: Border.all(
                        color:
                            PartnerAppColors.darkBlue.withValues(alpha: .4))),
                child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                  SizedBox(
                    height: 19,
                    width: 150 * 0.85,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(NumberFormat.currency(
                              locale: 'da',
                              symbol: 'kr.',
                              decimalDigits: updatedPrice == null
                                  ? widget.price == 0
                                      ? 0
                                      : 2
                                  : updatedPrice == 0
                                      ? 0
                                      : 2)
                          .format(updatedPrice ?? widget.price)),
                    ),
                  ),
                ]),
              ),
            )
          ],
        ),
        SmartGaps.gapH20,
        CustomSliderFormBuilder(
          name: 'updatePrice',
          initialValue: widget.price,
          max: widget.priceMax,
          min: widget.priceMin,
          divisions: 50,
          displayValues: DisplayValues.none,
          onChanged: (p0) {
            setState(() {
              updatedPrice = p0;
            });
          },
        ),
        SmartGaps.gapH30,
        const Divider(),
        SizedBox(
          height: 300,
          child: ListView.separated(
              separatorBuilder: (context, index) {
                return const Divider();
              },
              itemBuilder: (context, index) {
                return Row(
                  children: [
                    Flexible(
                        child: Container(
                      width: MediaQuery.of(context).size.width,
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: PartnerAppColors.darkBlue
                                  .withValues(alpha: .4)),
                          borderRadius: BorderRadius.circular(5)),
                      child: Text(descriptions[index],
                          style: Theme.of(context)
                              .textTheme
                              .titleSmall!
                              .copyWith(
                                  fontSize: 14,
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.normal)),
                    )),
                    SmartGaps.gapW5,
                    GestureDetector(
                      onTap: () {
                        showTextInputDialog(
                          context: context,
                          textFields: [
                            DialogTextField(
                              initialText: descriptions[index],
                              maxLines: 5,
                              validator: (value) {
                                if ((value ?? '').isEmpty) {
                                  return tr('required');
                                }
                                return null;
                              },
                            )
                          ],
                          style: AdaptiveStyle.iOS,
                          title: widget.subIndustryName,
                          cancelLabel: tr('cancel'),
                          okLabel: tr('update'),
                        ).then((value) {
                          if (value != null) {
                            setState(() {
                              descriptions[index] = value.first;
                            });
                          }
                        });
                      },
                      child: const Icon(
                        FeatherIcons.edit3,
                        color: PartnerAppColors.blue,
                        size: 30,
                      ),
                    ),
                    SmartGaps.gapW5,
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          descriptions.removeAt(index);
                        });

                        showOkAlertDialog(
                            context: context,
                            message: tr('description_deleted'),
                            okLabel: tr('ok'));
                      },
                      child: const Icon(
                        FeatherIcons.trash2,
                        color: PartnerAppColors.red,
                        size: 30,
                      ),
                    )
                  ],
                );
              },
              itemCount: descriptions.length,
              shrinkWrap: true),
        ),
        SmartGaps.gapH20,
        GestureDetector(
            onTap: () {
              showTextInputDialog(
                context: context,
                textFields: [
                  DialogTextField(
                    initialText: '',
                    maxLines: 5,
                    validator: (value) {
                      if ((value ?? '').isEmpty) {
                        return tr('required');
                      }
                      return null;
                    },
                  )
                ],
                style: AdaptiveStyle.iOS,
                title: widget.subIndustryName,
                cancelLabel: tr('cancel'),
                okLabel: tr('add'),
              ).then((value) {
                if (value != null) {
                  setState(() {
                    descriptions.add(value.first);
                  });
                }
              });
            },
            child: Text(tr('add_description_industry'),
                style: Theme.of(context).textTheme.titleSmall!.copyWith(
                    fontSize: 14,
                    color: PartnerAppColors.blue,
                    fontWeight: FontWeight.normal))),
        SmartGaps.gapH30,
        CustomDesignTheme.flatButtonStyle(
          height: 50,
          backgroundColor: PartnerAppColors.darkBlue,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
          onPressed: () {
            Navigator.of(context).pop({
              'price': updatedPrice ?? widget.price,
              'descriptions': descriptions
            });
          },
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text(
              tr('done'),
              style: Theme.of(context).textTheme.displayLarge!.copyWith(
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                  ),
            )
          ]),
        ),
      ],
    );
  }
}
