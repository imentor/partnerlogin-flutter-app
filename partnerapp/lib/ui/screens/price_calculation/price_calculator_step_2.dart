import 'dart:async';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer/create_offer_wizard_dynamic_forms.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/price_calculation/price_calculation_viewmodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:expandable_page_view/expandable_page_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PriceCalculatorStepTwo extends StatefulWidget {
  const PriceCalculatorStepTwo({
    super.key,
    required this.vm,
  });
  final PriceCalculationViewmodel vm;

  @override
  State<PriceCalculatorStepTwo> createState() => _PriceCalculatorStepTwoState();
}

class _PriceCalculatorStepTwoState extends State<PriceCalculatorStepTwo> {
  static const int debounceDurationMilliseconds = 1500;
  static const String placeholderText = 'N/A';
  static const String successMessageKey = 'you_have_successfully_added';
  static const String moreJobsMessageKey = 'want_to_add_more_jobs';

  Timer? _debounce;
  late PageController pageController;
  late int activeSectionIndex;

  @override
  void initState() {
    activeSectionIndex = 0;
    pageController = PageController(initialPage: activeSectionIndex);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (pageController.hasClients) {
      pageController.animateToPage(
        widget.vm.priceCalculatorProductsStep,
        duration: const Duration(milliseconds: 800),
        curve: Curves.easeOutQuart,
      );
    }
    return Column(
      children: body(step: widget.vm.priceCalculatorSubStepTwo),
    );
  }

  List<Widget> body({required int step}) {
    switch (step + 1) {
      case 1:
        return industryList();
      case 2:
        return questionnaire();
      case 3:
        return industryBreakdown();
      default:
        return [];
    }
  }

  List<Widget> industryList({bool isFromBreakdown = false}) {
    final filteredIndustries = widget.vm.filteredIndustries
        .where((element) => element.industry != '0')
        .toList();
    return [
      CustomTextFieldFormBuilder(
        name: 'industry_search',
        suffixIcon: const Icon(FeatherIcons.search),
        onChanged: (p0) {
          if (_debounce?.isActive ?? false) {
            _debounce?.cancel();
          }
          _debounce = Timer(
            const Duration(milliseconds: debounceDurationMilliseconds),
            () {
              if (p0 != null) {
                widget.vm.searchIndustry(query: p0);
              }
            },
          );
        },
      ),
      SmartGaps.gapH10,
      GridView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: filteredIndustries.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, mainAxisExtent: 180, crossAxisSpacing: 2),
        itemBuilder: (context, index) {
          final industry = filteredIndustries.elementAt(index);
          return InkWell(
            onTap: () {
              if (isFromBreakdown) {
                if (widget.vm.industryDescriptions
                    .where((element) =>
                        element['productTypeId'] == industry.producttypeid)
                    .isEmpty) {
                  widget.vm.updateSelectedIndustryList(industry: industry);
                }
              } else {
                widget.vm.updateSelectedIndustryList(industry: industry);
              }
            },
            child: Card(
              elevation: 1.5,
              shape: widget.vm.selectedIndustries.contains(industry)
                  ? Border.all(color: PartnerAppColors.blue)
                  : null,
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 50,
                      width: 50,
                      child: CachedNetworkImage(
                        imageUrl:
                            'https://newcrm.kundematch.dk/upload/products/${industry.mpic}',
                        fit: BoxFit.fill,
                      ),
                    ),
                    SmartGaps.gapH20,
                    FittedBox(
                      child: Text(
                        industry.producttypeDa ?? placeholderText,
                        textAlign: TextAlign.center,
                        style: Theme.of(context)
                            .textTheme
                            .displaySmall!
                            .copyWith(
                                fontSize: 14, color: PartnerAppColors.darkBlue),
                      ),
                    ),
                    SmartGaps.gapH5,
                    Text(
                      industry.generalDescription ?? placeholderText,
                      textAlign: TextAlign.center,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.displaySmall!.copyWith(
                          fontSize: 12, color: PartnerAppColors.spanishGrey),
                    )
                  ],
                ),
              ),
            ),
          );
        },
      )
    ];
  }

  List<Widget> questionnaire() {
    return [
      Builder(builder: (context) {
        final List<Widget> body = [];

        for (var i = 0; i < widget.vm.wizardQuestions.length; i++) {
          body.add(CreateOfferWizardDynamicForms(
              label:
                  widget.vm.wizardQuestions[i].producttype ?? placeholderText,
              productWizard: widget.vm.wizardQuestions[i],
              currentStep: activeSectionIndex + 1,
              totalStep: widget.vm.wizardQuestions.length));
        }

        return ExpandablePageView(
          physics: const NeverScrollableScrollPhysics(),
          controller: pageController,
          onPageChanged: (index) {
            setState(() {
              activeSectionIndex = index;
            });
          },
          children: body,
        );
      })
    ];
  }

  List<Widget> industryBreakdown() {
    return [
      const Icon(
        FontAwesomeIcons.solidCircleCheck,
        color: PartnerAppColors.malachite,
        size: 60,
      ),
      SizedBox(
        height: 20,
        width: MediaQuery.of(context).size.width,
      ),
      RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          children: [
            TextSpan(
              text: '${tr(successMessageKey)} ',
              style: Theme.of(context).textTheme.displaySmall!.copyWith(
                  fontWeight: FontWeight.normal,
                  fontSize: 16,
                  color: PartnerAppColors.spanishGrey),
            ),
            TextSpan(
              text: widget.vm.industryDescriptions
                  .map((e) => e['productName'])
                  .toList()
                  .join(','),
              style: Theme.of(context).textTheme.displaySmall!.copyWith(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  color: PartnerAppColors.darkBlue),
            ),
            TextSpan(
              text: ' ${tr('the_job')}',
              style: Theme.of(context).textTheme.displaySmall!.copyWith(
                  fontWeight: FontWeight.normal,
                  fontSize: 16,
                  color: PartnerAppColors.spanishGrey),
            ),
          ],
        ),
      ),
      SmartGaps.gapH20,
      Text(
        tr(moreJobsMessageKey),
        style: Theme.of(context).textTheme.displaySmall!.copyWith(
            fontWeight: FontWeight.normal,
            fontSize: 16,
            color: PartnerAppColors.spanishGrey),
      ),
      SmartGaps.gapH20,
      CustomDesignTheme.flatButtonStyle(
        height: 50,
        backgroundColor: PartnerAppColors.darkBlue,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        onPressed: () async {
          modalManager.showLoadingModal();
          final currentContext =
              myGlobals.homeScaffoldKey?.currentContext ?? context;
          await tryCatchWrapper(
                  context: currentContext,
                  function: Future.value(widget.vm.submitWizardAnswersV3()))
              .whenComplete(() {
            modalManager.hideLoadingModal();
            widget.vm.updateStep(step: widget.vm.priceCalculatorWizardStep + 1);
          });
        },
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text(
            tr('jump'),
            style: Theme.of(context).textTheme.displayLarge!.copyWith(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
          ),
          const Icon(
            FeatherIcons.arrowRight,
            color: Colors.white,
          )
        ]),
      ),
      SmartGaps.gapH30,
      ...industryList(isFromBreakdown: true)
    ];
  }
}
