import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/price_calculation/price_calculation_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PriceCalculatorStepFive extends StatefulWidget {
  const PriceCalculatorStepFive({
    super.key,
    required this.vm,
  });
  final PriceCalculationViewmodel vm;

  @override
  State<PriceCalculatorStepFive> createState() =>
      _PriceCalculatorStepFiveState();
}

class _PriceCalculatorStepFiveState extends State<PriceCalculatorStepFive> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomTextFieldFormBuilder(
          labelText: tr('project_day_estimate'),
          hintText: tr('days_hint_text'),
          name: 'priceCalculatorProjectEstimate',
          keyboardType: TextInputType.number,
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
        CustomDatePickerFormBuilder(
          name: 'priceCalculatorProjectStartDate',
          labelText: tr('date_to_start_project'),
          initialDate: DateTime.now(),
          dateFormat: DateFormatType.isoDate.formatter,
          validator: FormBuilderValidators.required(errorText: tr('required')),
          suffixIcon: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 5),
            child: Icon(Icons.event, color: PartnerAppColors.blue),
          ),
        ),
        SmartGaps.gapH10,
        RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: '${tr('your_offer_is_valid')} ',
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.normal,
                    fontSize: 18),
              ),
              TextSpan(
                text: widget
                    .vm.priceCalculatorForms['priceCalculatorSignatureValid'],
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              ),
              TextSpan(
                text: ' ${tr('days_valid')}',
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.normal,
                    fontSize: 18),
              )
            ],
          ),
        ),
        SmartGaps.gapH10,
        CustomDropdownFormBuilder(
          items: [
            ...List.generate(
              30,
              (index) => DropdownMenuItem(
                  value: '${index + 1}', child: Text('${index + 1}')),
            ),
          ],
          name: 'priceCalculatorSignatureValid',
          initialValue:
              widget.vm.priceCalculatorForms['priceCalculatorSignatureValid'],
          onChanged: (p0) {
            if (p0 != null) {
              widget.vm.updatePriceCalculatorForms(
                  key: 'priceCalculatorSignatureValid', value: p0);
            }
          },
        )
      ],
    );
  }
}
