import 'dart:async';

import 'package:Haandvaerker.dk/model/autocomplete/autocomplete_model.dart';
import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/location/location_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/price_calculation/price_calculation_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

class PriceCalculatorStepOne extends StatefulWidget {
  const PriceCalculatorStepOne({
    super.key,
    required this.vm,
    required this.formKey,
  });

  final PriceCalculationViewmodel vm;
  final GlobalKey<FormBuilderState> formKey;

  @override
  State<PriceCalculatorStepOne> createState() => _PriceCalculatorStepOneState();
}

class _PriceCalculatorStepOneState extends State<PriceCalculatorStepOne> {
  late GoogleMapController mapController;
  Set<Marker> markers = {};

  bool disableSearch = false;

  AutoCompleteAddress? selectedAddress;

  Timer? _debounce;

  // @override
  // void initState() {
  //   WidgetsBinding.instance.addPostFrameCallback((_) async {
  //     final locationViewModel = context.read<LocationViewModel>();

  //     setState(() {
  //       isSearchingAddress = true;
  //     });

  //     await widget.vm
  //         .searchAddress(query: locationViewModel.initialAddressName)
  //         .then((value) {
  //       setState(() {
  //         isSearchingAddress = false;
  //       });
  //     });
  //   });
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    final vm = widget.vm;
    final formKey = widget.formKey;
    final locationViewModel = context.read<LocationViewModel>();

    return Column(
      children: [
        CustomTextFieldFormBuilder(
          name: 'address',
          // initialValue: locationViewModel.initialAddressName,
          validator: FormBuilderValidators.required(errorText: tr('required')),
          enabled: !locationViewModel.isSearchingAddress,
          onChanged: !disableSearch
              ? (p0) {
                  if (p0 != null && p0.isNotEmpty) {
                    if (_debounce?.isActive ?? false) {
                      _debounce?.cancel();
                    }

                    if (p0 != (selectedAddress?.tekst ?? '')) {
                      _debounce = Timer(
                        const Duration(milliseconds: 1000),
                        () async {
                          // setState(() {
                          locationViewModel.isSearchingAddress = true;
                          // });

                          await vm.searchAddress(query: p0).whenComplete(() {
                            setState(() {
                              locationViewModel.isSearchingAddress = false;
                            });
                          });
                        },
                      );
                    }
                  } else {
                    setState(() {
                      locationViewModel.isSearchingAddress = false;
                      vm.searchAddressList = [];
                    });
                  }
                }
              : null,
        ),
        SizedBox(
          height: 200,
          child: context.watch<LocationViewModel>().isSearchingAddress
              ? Center(
                  child: CircularProgressIndicator(
                    color: PartnerAppColors.blue.withValues(alpha: .3),
                  ),
                )
              : ListView.separated(
                  itemCount: vm.searchAddressList.length,
                  // padding: const EdgeInsets.all(10),
                  shrinkWrap: true,
                  separatorBuilder: (context, index) {
                    return SmartGaps.gapH10;
                  },
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        final tempAddress = Address(
                            addressId: vm.searchAddressList[index].data!.id,
                            adgangsAddressId: vm.searchAddressList[index].data!
                                .adgangsadresseid!,
                            address: vm.searchAddressList[index].tekst!,
                            zip: vm.searchAddressList[index].data!.postnr!,
                            latitude: '${vm.searchAddressList[index].data!.y!}',
                            longitude:
                                '${vm.searchAddressList[index].data!.x!}');

                        vm.selectedAddress = tempAddress;

                        setState(() {
                          selectedAddress = vm.searchAddressList[index];
                          markers = {
                            Marker(
                              markerId:
                                  MarkerId(selectedAddress?.data?.id ?? ''),
                              position: LatLng(
                                double.parse(tempAddress.latitude ?? '0'),
                                double.parse(tempAddress.longitude ?? '0'),
                              ),
                            )
                          };
                        });

                        mapController
                            .animateCamera(CameraUpdate.newCameraPosition(
                          CameraPosition(
                            target: LatLng(
                              double.parse(tempAddress.latitude ?? '0'),
                              double.parse(tempAddress.longitude ?? '0'),
                            ),
                            zoom: 16,
                          ),
                        ));

                        vm.updatePriceCalculatorForms(
                          key: Constants.priceCalculatorAddress,
                          value: tempAddress,
                        );

                        formKey.currentState!
                            .patchValue({'address': tempAddress.address});
                        setState(() {
                          disableSearch =
                              false; // Enable search after processing list item selection
                        });
                      },
                      child: Container(
                        color:
                            PartnerAppColors.spanishGrey.withValues(alpha: .2),
                        padding: const EdgeInsets.all(10),
                        child: Row(
                          children: [
                            Radio(
                              value: vm.searchAddressList[index].data?.id ?? '',
                              groupValue: selectedAddress?.data?.id,
                              onChanged: (val) {},
                              activeColor: PartnerAppColors.blue,
                            ),
                            Text(vm.searchAddressList[index].tekst ?? 'N/A',
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineSmall!
                                    .copyWith(
                                        color: PartnerAppColors.darkBlue,
                                        fontSize: 14)),
                          ],
                        ),
                      ),
                    );
                  }),
        ),
        SmartGaps.gapH20,
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 200,
          child: AbsorbPointer(
            absorbing: true,
            child: GoogleMap(
              onMapCreated: (controller) async {
                mapController = controller;
              },
              myLocationButtonEnabled: true,
              initialCameraPosition: CameraPosition(
                zoom: 16,
                target: locationViewModel.initialPosition,
              ),
              markers: markers,
              myLocationEnabled: false,
              mapType: MapType.normal,
              zoomControlsEnabled: false,
              zoomGesturesEnabled: false,
            ),
          ),
        ),
      ],
    );
  }
}

class Constants {
  static const String priceCalculatorAddress = 'priceCalculatorAddress';
  static const String completeAddress = 'complete_address';
}
