import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/screens/price_calculation/component/update_sub_industry_price.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/string_utils.dart';
import 'package:Haandvaerker.dk/viewmodel/price_calculation/price_calculation_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';

class PriceCalculatorStepFour extends StatefulWidget {
  const PriceCalculatorStepFour({
    super.key,
    required this.vm,
  });
  final PriceCalculationViewmodel vm;

  @override
  State<PriceCalculatorStepFour> createState() =>
      _PriceCalculatorStepFourState();
}

class _PriceCalculatorStepFourState extends State<PriceCalculatorStepFour> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('review_and_edit_description'),
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              letterSpacing: 1.0,
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18),
        ),
        SmartGaps.gapH20,
        ListView.separated(
          itemBuilder: (context, industryIndex) {
            final industryMap = widget.vm.industryDescriptions[industryIndex];
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                    '${industryIndex + 1}. ${(industryMap['productName'] as String).toCapitalizedFirst()}',
                    style: Theme.of(context).textTheme.titleSmall!.copyWith(
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.bold)),
                ...(industryMap['subIndustry'] as List).map((sub) {
                  final subIndustryIndex =
                      (industryMap['subIndustry'] as List).indexOf(sub);

                  final subIndustryMap = sub as Map<String, dynamic>;

                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: Card(
                        child: Padding(
                            padding: const EdgeInsets.all(20),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Flexible(
                                      child: Text(
                                          '${industryIndex + 1}.${subIndustryIndex + 1}. ${(subIndustryMap['subIndustryName'] as String).toCapitalizedFirst()}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleSmall!
                                              .copyWith(
                                                  fontSize: 14,
                                                  color:
                                                      PartnerAppColors.darkBlue,
                                                  fontWeight:
                                                      FontWeight.normal)),
                                    ),
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                          0.38,
                                      child: FittedBox(
                                        alignment: Alignment.centerRight,
                                        fit: BoxFit.scaleDown,
                                        child: Text(NumberFormat.currency(
                                                locale: 'da',
                                                symbol: 'kr.',
                                                decimalDigits: subIndustryMap[
                                                            'price_max'] ==
                                                        0
                                                    ? 0
                                                    : 2)
                                            .format(
                                                subIndustryMap['price_max'])),
                                      ),
                                    )
                                  ],
                                ),
                                SmartGaps.gapH20,
                                GestureDetector(
                                  onTap: () async {
                                    updateSubIndustryPriceAndDescriptionDialog(
                                        context: context,
                                        price: num.parse(
                                                '${subIndustryMap['price_max']}')
                                            .toDouble(),
                                        priceMax: num.parse('${subIndustryMap['price_max']}') + (num.parse('${subIndustryMap['price_max']}') * 2),
                                        priceMin: 10000,
                                        subIndustryName: (subIndustryMap['subIndustryName'] as String).toCapitalizedFirst(),
                                        descriptions: [
                                          ...(subIndustryMap['descriptions']
                                                  as List)
                                              .map((e) => e)
                                        ]).then((value) {
                                      if (value != null) {
                                        widget.vm.updateIndustryPrice(
                                            industryIndex: industryIndex,
                                            subIndustryIndex: subIndustryIndex,
                                            price: value['price']);

                                        widget.vm.updateIndustryAllDescriptions(
                                            industryIndex: industryIndex,
                                            subIndustryIndex: subIndustryIndex,
                                            descriptions:
                                                value['descriptions']);
                                      }
                                    });
                                  },
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: RichText(
                                        text: TextSpan(children: [
                                      const WidgetSpan(
                                          child: Icon(FeatherIcons.edit,
                                              color: PartnerAppColors.blue,
                                              size: 16),
                                          alignment:
                                              PlaceholderAlignment.middle),
                                      const WidgetSpan(
                                          child: SizedBox(
                                        width: 5,
                                      )),
                                      TextSpan(
                                          text: tr('edit'),
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleSmall!
                                              .copyWith(
                                                  fontSize: 14,
                                                  color: PartnerAppColors.blue,
                                                  fontWeight:
                                                      FontWeight.normal))
                                    ])),
                                  ),
                                ),
                                SmartGaps.gapH30,
                                if ((subIndustryMap['hide_description']
                                    as bool)) ...[
                                  ListView.separated(
                                      itemCount: (subIndustryMap['descriptions']
                                              as List)
                                          .length,
                                      shrinkWrap: true,
                                      padding: EdgeInsets.zero,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      separatorBuilder:
                                          (context, descriptionIndex) {
                                        return const Divider();
                                      },
                                      itemBuilder: (context, descriptionIndex) {
                                        return industryDescriptionList(
                                            industryIndex: industryIndex,
                                            subIndustryIndex: subIndustryIndex,
                                            descriptionIndex: descriptionIndex,
                                            subIndustryMap: subIndustryMap);
                                      }),
                                  SmartGaps.gapH20,
                                ],
                                GestureDetector(
                                  onTap: () {
                                    widget.vm.hideOrUnhideDescriptions(
                                        industryIndex: industryIndex,
                                        subIndustryIndex: subIndustryIndex,
                                        hideDescription:
                                            !(subIndustryMap['hide_description']
                                                as bool));
                                  },
                                  child: Text(
                                    (subIndustryMap['hide_description'] as bool)
                                        ? tr('hide_description')
                                        : tr('see_and_correct_description'),
                                    style: Theme.of(context)
                                        .textTheme
                                        .headlineSmall!
                                        .copyWith(
                                            color: PartnerAppColors.blue,
                                            fontWeight: FontWeight.normal),
                                  ),
                                )
                              ],
                            )),
                      ),
                    ),
                  );
                })
              ],
            );
          },
          separatorBuilder: (context, index) {
            return SmartGaps.gapH10;
          },
          itemCount: widget.vm.industryDescriptions.length,
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
        )
      ],
    );
  }

  Widget industryDescriptionList(
      {required int industryIndex,
      required int subIndustryIndex,
      required int descriptionIndex,
      required Map<String, dynamic> subIndustryMap}) {
    final description =
        (subIndustryMap['descriptions'] as List)[descriptionIndex];
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
            '${industryIndex + 1}.${subIndustryIndex + 1}.${descriptionIndex + 1}',
            style: Theme.of(context).textTheme.titleSmall!.copyWith(
                fontSize: 14,
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.normal)),
        SmartGaps.gapW10,
        Flexible(
          child: Text(description,
              style: Theme.of(context).textTheme.titleSmall!.copyWith(
                  fontSize: 14,
                  color: PartnerAppColors.darkBlue,
                  fontWeight: FontWeight.normal)),
        ),
      ],
    );
  }
}
