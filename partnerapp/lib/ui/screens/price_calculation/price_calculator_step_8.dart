import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/price_calculation/price_calculation_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_contacts/flutter_contacts.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PriceCalculatorStepEight extends StatefulWidget {
  const PriceCalculatorStepEight(
      {super.key, required this.vm, required this.formKey});
  final PriceCalculationViewmodel vm;
  final GlobalKey<FormBuilderState> formKey;

  @override
  State<PriceCalculatorStepEight> createState() =>
      _PriceCalculatorStepEightState();
}

class _PriceCalculatorStepEightState extends State<PriceCalculatorStepEight> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('customer_info'),
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              letterSpacing: 1.0,
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18),
        ),
        SmartGaps.gapH20,
        TextButton(
            style: TextButton.styleFrom(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                    side: const BorderSide(color: PartnerAppColors.darkBlue))),
            onPressed: () async {
              if (await FlutterContacts.requestPermission()) {
                final contact = await FlutterContacts.openExternalPick();

                if (contact != null) {
                  widget.formKey.currentState!
                      .patchValue({'homeOwnerName': contact.displayName});
                  widget.formKey.currentState!.patchValue({
                    'homeOwnerPhone': contact.phones.first.number
                        .replaceAll('-', '')
                        .replaceAll('(', '')
                        .replaceAll(')', '')
                        .replaceAll(' ', '')
                        .substring(0, 8)
                  });
                }
              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Icon(
                  FeatherIcons.phone,
                  color: PartnerAppColors.darkBlue,
                ),
                SmartGaps.gapW10,
                Text(
                  tr('select_phone_contacts'),
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium!
                      .copyWith(color: PartnerAppColors.darkBlue),
                ),
              ],
            )),
        SmartGaps.gapH20,
        CustomTextFieldFormBuilder(
          name: 'homeOwnerName',
          labelText: tr('full_name'),
          validator: FormBuilderValidators.compose([
            (val) {
              if (val != null) {
                if (val.isEmpty) {
                  return tr('required');
                }

                if (val.split(' ').length != 2) {
                  return '${tr('full_name')} ${tr('required')}';
                }
              }

              return null;
            }
          ]),
        ),
        CustomTextFieldFormBuilder(
          name: 'homeOwnerPhone',
          labelText: tr('phone_number'),
          inputFormatters: [
            LengthLimitingTextInputFormatter(8),
          ],
          keyboardType: TextInputType.number,
          validator: FormBuilderValidators.required(errorText: tr('required')),
          prefixIcon: Container(
            padding: const EdgeInsets.all(10),
            child: Text('+45',
                style: Theme.of(context)
                    .textTheme
                    .headlineSmall!
                    .copyWith(fontWeight: FontWeight.normal)),
          ),
        ),
        CustomTextFieldFormBuilder(
          name: 'homeOwnerEmail',
          labelText: tr('email'),
          keyboardType: TextInputType.emailAddress,
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
      ],
    );
  }
}
