import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/reviews/review_list.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/price_calculation/price_calculation_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/svg.dart';

class PriceCalculatorStepSix extends StatefulWidget {
  const PriceCalculatorStepSix({
    super.key,
    required this.vm,
  });
  final PriceCalculationViewmodel vm;

  @override
  State<PriceCalculatorStepSix> createState() => _PriceCalculatorStepSixState();
}

class _PriceCalculatorStepSixState extends State<PriceCalculatorStepSix> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('add_reservations_to_offer'),
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              letterSpacing: 1.0,
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18),
        ),
        SmartGaps.gapH10,
        CustomRadioGroupFormBuilder(
          options: [
            FormBuilderFieldOption(
              value: 'yes',
              child: Text(
                tr('save_as_default_text'),
                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                    ),
              ),
            )
          ],
          name: 'saveConditionDefault',
        ),
        CustomTextFieldFormBuilder(
          name: 'offerCondition',
          hintText: tr('add_conditions_to_offer'),
          maxLines: 10,
        ),
        SmartGaps.gapH11,
        Row(
          children: [
            InkWell(
              onTap: () {
                chooseReviewListDialog(
                        context: context,
                        alreadySelectedId:
                            (widget.vm.priceCalculatorForms['reviewIds'] ?? []))
                    .then((value) {
                  if (value != null) {
                    widget.vm.updatePriceCalculatorForms(
                        key: 'reviewIds', value: value);
                  }
                });
              },
              child: Row(
                children: [
                  const Padding(
                    padding: EdgeInsets.only(bottom: 4),
                    child: Icon(
                      FeatherIcons.plus,
                      color: PartnerAppColors.accentBlue,
                      size: 20,
                    ),
                  ),
                  SmartGaps.gapW5,
                  Text(
                    tr('add_your_recommendation'),
                    style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                        letterSpacing: 1.0,
                        color: PartnerAppColors.accentBlue,
                        fontWeight: FontWeight.normal,
                        fontSize: 15),
                  ),
                ],
              ),
            ),
            SmartGaps.gapW5,
            InkWell(
              onTap: () {
                addRecommendationTooltipDialog(context: context);
              },
              child: SvgPicture.asset(
                SvgIcons.questionCirlce,
                width: 19,
                height: 19,
                colorFilter: const ColorFilter.mode(
                  PartnerAppColors.spanishGrey,
                  BlendMode.srcIn,
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}

Future<void> addRecommendationTooltipDialog({required BuildContext context}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding:
              const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          contentPadding: const EdgeInsets.all(20),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(
                  onTap: () {
                    Navigator.of(dialogContext).pop();
                  },
                  child: const Icon(
                    FeatherIcons.x,
                    color: PartnerAppColors.spanishGrey,
                  )),
            ],
          ),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: Column(
              children: [
                Text(
                  tr('add_recommendation_tooltip_message'),
                  textAlign: TextAlign.center,
                  style: Theme.of(dialogContext)
                      .textTheme
                      .headlineSmall!
                      .copyWith(
                          color: PartnerAppColors.spanishGrey,
                          fontWeight: FontWeight.normal),
                ),
                SmartGaps.gapH45,
              ],
            )),
          ),
        );
      });
}
