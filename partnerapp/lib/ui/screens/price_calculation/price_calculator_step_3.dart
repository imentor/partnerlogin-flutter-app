import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/screens/price_calculation/component/update_sub_industry_price.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/string_utils.dart';
import 'package:Haandvaerker.dk/viewmodel/price_calculation/price_calculation_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class PriceCalculatorStepThree extends StatefulWidget {
  const PriceCalculatorStepThree({
    super.key,
    required this.vm,
  });
  final PriceCalculationViewmodel vm;

  @override
  State<PriceCalculatorStepThree> createState() =>
      _PriceCalculatorStepThreeState();
}

class _PriceCalculatorStepThreeState extends State<PriceCalculatorStepThree> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListView.separated(
          itemBuilder: (context, industryIndex) {
            final industryMap = widget.vm.industryDescriptions[industryIndex];
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                    '${industryIndex + 1}. ${(industryMap['productName'] as String).toCapitalizedFirst()}',
                    style: Theme.of(context).textTheme.titleSmall!.copyWith(
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.bold)),
                ...(industryMap['subIndustry'] as List).map((sub) {
                  final subIndustryIndex =
                      (industryMap['subIndustry'] as List).indexOf(sub);

                  final subIndustryMap = sub as Map<String, dynamic>;

                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: Card(
                        child: Padding(
                            padding: const EdgeInsets.all(20),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Flexible(
                                      child: Text(
                                          '${industryIndex + 1}.${subIndustryIndex + 1}. ${(subIndustryMap['subIndustryName'] as String).toCapitalizedFirst()}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleSmall!
                                              .copyWith(
                                                  fontSize: 14,
                                                  color:
                                                      PartnerAppColors.darkBlue,
                                                  fontWeight:
                                                      FontWeight.normal)),
                                    ),
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                          0.38,
                                      child: FittedBox(
                                          alignment: Alignment.centerRight,
                                          fit: BoxFit.scaleDown,
                                          child: Text(NumberFormat.currency(
                                                  locale: 'da',
                                                  symbol: 'kr.',
                                                  decimalDigits: subIndustryMap[
                                                              'price_max'] ==
                                                          0
                                                      ? 0
                                                      : 2)
                                              .format(subIndustryMap[
                                                  'price_max']))),
                                    )
                                  ],
                                ),
                                SmartGaps.gapH20,
                                GestureDetector(
                                  onTap: () async {
                                    await updateSubIndustryPriceDialog(
                                            context: context,
                                            price: num.parse(
                                                    '${subIndustryMap['price_max']}')
                                                .toDouble(),
                                            priceMax: (num.parse(
                                                        '${subIndustryMap['price_max']}') +
                                                    (num.parse(
                                                            '${subIndustryMap['price_max']}') *
                                                        2))
                                                .ceilToDouble(),
                                            priceMin: 1000,
                                            subIndustryName: (subIndustryMap[
                                                        'subIndustryName']
                                                    as String)
                                                .toCapitalizedFirst())
                                        .then((value) {
                                      if (value != null) {
                                        widget.vm.updateIndustryPrice(
                                            industryIndex: industryIndex,
                                            subIndustryIndex: subIndustryIndex,
                                            price: value);
                                      }
                                    });
                                  },
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: Text(tr('adjust_price'),
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleSmall!
                                            .copyWith(
                                                fontSize: 14,
                                                color: PartnerAppColors.blue,
                                                fontWeight: FontWeight.normal)),
                                  ),
                                )
                              ],
                            )),
                      ),
                    ),
                  );
                })
              ],
            );
          },
          separatorBuilder: (context, index) {
            return SmartGaps.gapH10;
          },
          itemCount: widget.vm.industryDescriptions.length,
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
        )
      ],
    );
  }
}
