import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/components/signature_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/price_calculation/component/delete_calculation.dart';
import 'package:Haandvaerker.dk/ui/screens/price_calculation/price_calculator_step_1.dart';
import 'package:Haandvaerker.dk/ui/screens/price_calculation/price_calculator_step_2.dart';
import 'package:Haandvaerker.dk/ui/screens/price_calculation/price_calculator_step_3.dart';
import 'package:Haandvaerker.dk/ui/screens/price_calculation/price_calculator_step_4.dart';
import 'package:Haandvaerker.dk/ui/screens/price_calculation/price_calculator_step_5.dart';
import 'package:Haandvaerker.dk/ui/screens/price_calculation/price_calculator_step_6.dart';
import 'package:Haandvaerker.dk/ui/screens/price_calculation/price_calculator_step_7.dart';
import 'package:Haandvaerker.dk/ui/screens/price_calculation/price_calculator_step_8.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/location/location_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/price_calculation/price_calculation_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:timeline_tile/timeline_tile.dart';

Future<void> successCreatingPriceCalculationDialog({
  required BuildContext context,
  required String homeOwnerName,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: Column(
              children: [
                SvgPicture.asset(
                  SvgIcons.sendIconUrl,
                  colorFilter: const ColorFilter.mode(
                    PartnerAppColors.blue,
                    BlendMode.srcIn,
                  ),
                  height: 60,
                  width: 60,
                ),
                Text(
                  tr('such'),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.displayLarge!.copyWith(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: PartnerAppColors.darkBlue,
                      ),
                ),
                SmartGaps.gapH10,
                Text(
                  '${tr('offer_sent_to')} $homeOwnerName. ${tr('you_can_find')} $homeOwnerName ${tr('under_my_customer')}',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.displayLarge!.copyWith(
                        fontSize: 16,
                        fontWeight: FontWeight.normal,
                        color: PartnerAppColors.darkBlue,
                      ),
                ),
              ],
            ))),
      );
    },
  );
}

class PriceCalculatorPage extends StatefulWidget {
  const PriceCalculatorPage({super.key});

  @override
  State<PriceCalculatorPage> createState() => _PriceCalculatorPageState();
}

class _PriceCalculatorPageState extends State<PriceCalculatorPage> {
  final formKey = GlobalKey<FormBuilderState>();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final vm = context.read<PriceCalculationViewmodel>();
      final locationVm = context.read<LocationViewModel>();
      final newsFeedVm = context.read<NewsFeedViewModel>();
      final appDrawerVm = context.read<AppDrawerViewModel>();
      const dynamicStoryPage = 'pricecalculator';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;
      modalManager
          .setContext(myGlobals.homeScaffoldKey?.currentContext ?? context);
      locationVm.isSearchingAddress = true;

      if (dynamicStoryEnable) {
        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: Future.wait([
              vm.initPriceCalculator(
                  initialPosition: locationVm.initialPosition),
              newsFeedVm.getDynamicStory(page: dynamicStoryPage),
            ])).whenComplete(() {
          if (!mounted) return;

          locationVm.isSearchingAddress = false;
          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      } else {
        await tryCatchWrapper(
                context: myGlobals.homeScaffoldKey!.currentContext,
                function: Future.value(vm.initPriceCalculator(
                    initialPosition: locationVm.initialPosition)))
            .whenComplete(() {
          locationVm.isSearchingAddress = false;
        });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    formKey.currentState?.reset();
    formKey.currentState?.fields.forEach((key, value) {
      formKey.currentState?.fields[key]?.didChange(null);
    });
    super.dispose();
  }

  void onTapDelete(
      {required PriceCalculationViewmodel priceCalculationVm}) async {
    final reason = await deleteCalculationDialog(context: context);

    if (mounted && reason != null) {
      final result = await priceSurveyDialog(
          context: context,
          reason: reason,
          industryDescriptions: priceCalculationVm.industryDescriptions);

      if (mounted && result != null) {
        modalManager.showLoadingModal();
        List<Future<void>> futures = [];

        for (var element in result) {
          if (element['reason'] != tr('other_reason')) {
            final industryIndex = result.indexOf(element);
            for (int i = 0;
                i < priceCalculationVm.industryDescriptions.length;
                i++) {
              final industryMap = priceCalculationVm.industryDescriptions[i]
                  ['subIndustry'] as List;
              industryMap.map((sub) {
                final subIndustryIndex = industryMap.indexOf(sub);
                priceCalculationVm.updateIndustryPrice(
                    industryIndex: industryIndex,
                    subIndustryIndex: subIndustryIndex,
                    price: element['suggestedPrice']);
              });
            }
          }
          futures.add(priceCalculationVm.saveAiPriceSurvey(payload: element));
        }

        await tryCatchWrapper(context: context, function: Future.wait(futures))
            .whenComplete(() {
          setState(() {});
          modalManager.hideLoadingModal();
          formKey.currentState?.reset();
          formKey.currentState?.fields.forEach((key, value) {
            formKey.currentState?.fields[key]?.didChange(null);
          });
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DrawerAppBar(actions: [
        Tooltip(
          triggerMode: TooltipTriggerMode.tap,
          showDuration: const Duration(seconds: 5),
          message: tr('price_calculator_tootltip_message'),
          margin: const EdgeInsets.all(20),
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: PartnerAppColors.darkBlue,
              ),
              borderRadius: BorderRadius.circular(5)),
          textStyle: Theme.of(context).textTheme.headlineSmall!.copyWith(
              color: PartnerAppColors.darkBlue,
              fontSize: 14,
              fontWeight: FontWeight.normal),
          child: Container(
            width: 85,
            margin: const EdgeInsets.only(left: 20, top: 15),
            padding: const EdgeInsets.only(right: 20),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('Beta',
                    style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                        color: PartnerAppColors.spanishGrey, fontSize: 14)),
                SmartGaps.gapW5,
                const Icon(
                  FeatherIcons.alertCircle,
                  color: PartnerAppColors.spanishGrey,
                  size: 14,
                )
              ],
            ),
          ),
        ),
      ]),
      floatingActionButton: SizedBox(
        height: 60,
        width: 60,
        child: FloatingActionButton(
          backgroundColor: Colors.white,
          elevation: 3,
          child: SvgPicture.asset(
            SvgIcons.writeReview,
            colorFilter: ColorFilter.mode(
              Theme.of(context).colorScheme.onSurfaceVariant,
              BlendMode.srcIn,
            ),
            height: 25,
          ),
          onPressed: () {
            writeReviewDialog(context: context);
          },
        ),
      ),
      bottomNavigationBar: floatingButtons(),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: FormBuilder(
          key: formKey,
          child: Consumer<PriceCalculationViewmodel>(builder: (_, vm, __) {
            return Column(
              children: [
                if ((vm.priceCalculatorWizardStep + 1) == 1) ...[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(tr('ai_price_calculator'),
                          style: context.pttTitleLarge),
                      SmartGaps.gapH10,
                      Text(tr('ai_price_calculator_message'),
                          style: context.pttTitleSmall.copyWith(
                              color: PartnerAppColors.grey1,
                              fontWeight: FontWeight.normal)),
                      SmartGaps.gapH10,
                    ],
                  ),
                ],
                if ((vm.priceCalculatorWizardStep + 1) < 5) ...[
                  SizedBox(
                    height: 65,
                    width: MediaQuery.of(context).size.width,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        itemCount: 4,
                        itemBuilder: (context, index) {
                          final isCurrentPage =
                              index <= vm.priceCalculatorWizardStep;
                          return TimelineTile(
                            axis: TimelineAxis.horizontal,
                            alignment: TimelineAlign.center,
                            isFirst: index == 0,
                            isLast: index == 4 - 1,
                            indicatorStyle: IndicatorStyle(
                              width: 30.0,
                              height: 30.0,
                              indicator: DecoratedBox(
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border:
                                      Border.all(color: PartnerAppColors.blue),
                                  color: isCurrentPage
                                      ? PartnerAppColors.blue
                                      : Colors.white,
                                ),
                                child: Center(
                                    child: Text('${index + 1}',
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodySmall!
                                            .copyWith(
                                                color: isCurrentPage
                                                    ? Colors.white
                                                    : PartnerAppColors.blue,
                                                fontWeight: FontWeight.bold))),
                              ),
                            ),
                            beforeLineStyle: const LineStyle(
                                thickness: 1.0,
                                color: PartnerAppColors.spanishGrey),
                            endChild: Container(
                              margin: const EdgeInsets.only(top: 5),
                              constraints: const BoxConstraints(
                                maxHeight: 30,
                                minWidth: 50,
                              ),
                              child: Text('${tr('step')} ${index + 1}',
                                  style: Theme.of(context).textTheme.bodySmall,
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.center),
                            ),
                          );
                        }),
                  ),
                ],
                if ((vm.priceCalculatorWizardStep + 1) == 3) ...[
                  SmartGaps.gapH18,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        tr('ai_generated_estimate'),
                        style: Theme.of(context)
                            .textTheme
                            .titleLarge!
                            .copyWith(fontSize: 16),
                      ),
                      GestureDetector(
                        onTap: () => onTapDelete(priceCalculationVm: vm),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const Align(
                              alignment: Alignment.centerRight,
                              child: Icon(FeatherIcons.trash2,
                                  color: PartnerAppColors.blue, size: 18),
                            ),
                            SmartGaps.gapW5,
                            Text(tr('delete_calculation'),
                                style: Theme.of(context)
                                    .textTheme
                                    .titleSmall!
                                    .copyWith(
                                        fontSize: 14,
                                        color: PartnerAppColors.blue,
                                        fontWeight: FontWeight.normal))
                          ],
                        ),
                      )
                    ],
                  ),
                ],
                SmartGaps.gapH20,
                step(
                    step: vm.priceCalculatorWizardStep,
                    formKey: formKey,
                    vm: vm)
              ],
            );
          }),
        ),
      ),
    );
  }

  Widget floatingButtons() {
    return Consumer<PriceCalculationViewmodel>(builder: (_, vm, __) {
      String nextButtonTitle = tr('next');

      Color nextButtonColor = PartnerAppColors.darkBlue;

      switch (vm.priceCalculatorWizardStep + 1) {
        // case 2:
        //   switch (vm.priceCalculatorSubStepTwo + 1) {
        //     case 3:
        //       if (vm.industryDescriptions.length ==
        //           vm.selectedIndustries.length) {
        //         nextButtonColor = PartnerAppColors.spanishGrey;
        //       } else {
        //         nextButtonColor = PartnerAppColors.darkBlue;
        //       }
        //       break;
        //   }
        //   break;
        case 3:
          nextButtonTitle = tr('create_description');
          break;
        case 8:
          nextButtonTitle = tr('submit');
          break;
        default:
      }

      return Container(
        padding:
            const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
        color: Colors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            if ((vm.priceCalculatorWizardStep + 1) == 3) ...[
              RichText(
                  text: TextSpan(children: [
                const WidgetSpan(
                    child: Icon(
                      FeatherIcons.alertCircle,
                      color: PartnerAppColors.accentBlue,
                    ),
                    alignment: PlaceholderAlignment.middle),
                const WidgetSpan(
                    child: SizedBox(
                  width: 10,
                )),
                TextSpan(
                    text: tr('change_price_next_step'),
                    style: Theme.of(context).textTheme.titleSmall!.copyWith(
                        fontSize: 15,
                        color: PartnerAppColors.accentBlue,
                        fontWeight: FontWeight.normal))
              ])),
              SmartGaps.gapH10,
            ],
            Row(children: [
              if (vm.priceCalculatorWizardStep != 0) ...[
                Expanded(
                    child: TextButton(
                        style: TextButton.styleFrom(
                            fixedSize:
                                Size(MediaQuery.of(context).size.width, 45),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                                side: const BorderSide(
                                    color: PartnerAppColors.darkBlue))),
                        onPressed: () {
                          switch (vm.priceCalculatorWizardStep + 1) {
                            case 2:
                              switch (vm.priceCalculatorSubStepTwo + 1) {
                                case 1:
                                  vm.updateStep(
                                      step: vm.priceCalculatorWizardStep - 1);
                                  break;
                                case 2:
                                  if (vm.priceCalculatorProductsStep == 0) {
                                    formKey.currentState!.reset();
                                    formKey.currentState!.fields
                                        .forEach((key, value) {
                                      formKey.currentState!.fields[key]
                                          ?.didChange(null);
                                    });
                                    vm.updateSubStep(
                                        subStep:
                                            vm.priceCalculatorSubStepTwo - 1);
                                  } else {
                                    vm.updateProductStep(
                                        productStep:
                                            vm.priceCalculatorProductsStep - 1);
                                  }
                                  break;
                                case 3:
                                  formKey.currentState!.reset();
                                  formKey.currentState!.fields
                                      .forEach((key, value) {
                                    formKey.currentState!.fields[key]
                                        ?.didChange(null);
                                  });
                                  vm.updateSubStep(subStep: 0);
                                  break;
                              }
                              break;
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                              vm.updateStep(
                                  step: vm.priceCalculatorWizardStep - 1);
                              break;
                            default:
                          }
                        },
                        child: Text(
                          tr('back'),
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(color: PartnerAppColors.darkBlue),
                        ))),
                SmartGaps.gapW20,
              ],
              Expanded(
                  child: TextButton(
                      style: TextButton.styleFrom(
                          fixedSize:
                              Size(MediaQuery.of(context).size.width, 45),
                          backgroundColor: nextButtonColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          )),
                      onPressed: () async {
                        switch (vm.priceCalculatorWizardStep + 1) {
                          case 1:
                            if (vm.priceCalculatorForms[
                                    'priceCalculatorAddress'] !=
                                null) {
                              modalManager.showLoadingModal();
                              if (vm.selectedAddress?.addressId != null &&
                                  (vm.selectedAddress?.addressId ?? '')
                                      .isNotEmpty) {
                                vm
                                    .createAssistantWizard(
                                        addressId:
                                            vm.selectedAddress?.addressId ?? '')
                                    .whenComplete(() {
                                  modalManager.hideLoadingModal();
                                  vm.updateStep(
                                      step: vm.priceCalculatorWizardStep + 1);
                                });
                              } else {
                                modalManager.hideLoadingModal();
                              }
                              // vm.updateStep(
                              //     step: vm.priceCalculatorWizardStep + 1);
                            }
                            break;
                          case 2:
                            switch (vm.priceCalculatorSubStepTwo + 1) {
                              case 1:
                                if (vm.selectedIndustries.isNotEmpty) {
                                  modalManager.showLoadingModal();

                                  await tryCatchWrapper(
                                    context: myGlobals
                                        .homeScaffoldKey!.currentContext,
                                    function: vm.getWizardProductQuestions(),
                                  ).whenComplete(() {
                                    vm.updateSelectedIndustryLength(
                                        length: vm.selectedIndustries.length);
                                    modalManager.hideLoadingModal();
                                  });
                                }
                                break;
                              case 2:
                                if (vm.priceCalculatorProductsStep ==
                                    vm.wizardQuestions.length - 1) {
                                  modalManager.showLoadingModal();
                                  await tryCatchWrapper(
                                    context: myGlobals
                                        .homeScaffoldKey!.currentContext,
                                    // function: vm.submitWizardAnswers(),
                                    function: vm.assistantWizardMessage(),
                                  ).whenComplete(
                                      () => modalManager.hideLoadingModal());
                                } else {
                                  formKey.currentState!.fields
                                      .forEach((key, value) {
                                    if (key.contains('checkbox')) {
                                      if (value.value is List) {
                                        for (final val in value.value as List) {
                                          vm.updateWizardProductAnswers(
                                              key: '$key$val', value: '$val');
                                        }
                                      }
                                    } else {
                                      vm.updateWizardProductAnswers(
                                          key: key, value: '${value.value}');
                                    }
                                  });

                                  vm.updateProductStep(
                                      productStep:
                                          vm.priceCalculatorProductsStep + 1);
                                }
                                break;
                              case 3:
                                if (vm.selectedIndustryLength !=
                                    vm.selectedIndustries.length) {
                                  modalManager.showLoadingModal();
                                  await tryCatchWrapper(
                                    context: myGlobals
                                        .homeScaffoldKey!.currentContext,
                                    function: vm.getWizardProductQuestions(),
                                  ).whenComplete(() {
                                    vm.updateSelectedIndustryLength(
                                        length: vm.selectedIndustries.length);
                                    modalManager.hideLoadingModal();
                                  });
                                } else {
                                  modalManager.showLoadingModal();
                                  final currentContext = myGlobals
                                          .homeScaffoldKey?.currentContext ??
                                      context;
                                  await tryCatchWrapper(
                                          context: currentContext,
                                          function: Future.value(
                                              vm.submitWizardAnswersV3()))
                                      .whenComplete(() {
                                    modalManager.hideLoadingModal();
                                    vm.updateStep(
                                        step: vm.priceCalculatorWizardStep + 1);
                                  });
                                }
                                break;
                            }
                            break;
                          case 3:
                            vm.updateStep(
                                step: vm.priceCalculatorWizardStep + 1);
                            break;
                          case 4:
                            final pricesHasZero = vm.industryDescriptions
                                .expand(
                                    (element) => element['subIndustry'] as List)
                                .where((e) => e['price_max'] == 0)
                                .isNotEmpty;

                            double totalPriceWithoutVat = 0;

                            for (var industry in vm.industryDescriptions) {
                              for (var subIndutry
                                  in industry['subIndustry'] as List) {
                                totalPriceWithoutVat += subIndutry['price_max'];
                              }
                            }

                            if (pricesHasZero) {
                              showOkAlertDialog(
                                  context: context,
                                  message: tr('price_cannot_be_lower_than_0'),
                                  okLabel: tr('ok'));
                            } else if ((totalPriceWithoutVat +
                                    (totalPriceWithoutVat * .25)) <
                                1000) {
                              showOkAlertDialog(
                                  context: context,
                                  message:
                                      tr('total_should_not_be_less_than_1000'),
                                  okLabel: tr('ok'));
                            } else {
                              modalManager.showLoadingModal();

                              await tryCatchWrapper(
                                      context: myGlobals
                                          .homeScaffoldKey!.currentContext,
                                      function: vm.getTitleAndSummary())
                                  .then((value) =>
                                      modalManager.hideLoadingModal());
                            }

                            break;
                          case 5:
                          case 6:
                            if (formKey.currentState!.validate()) {
                              formKey.currentState!.fields
                                  .forEach((key, value) {
                                vm.updatePriceCalculatorForms(
                                    key: key, value: '${value.value}');
                              });

                              vm.updateStep(
                                  step: vm.priceCalculatorWizardStep + 1);
                            }
                            break;

                          case 7:
                            if (formKey.currentState!.validate()) {
                              formKey.currentState!.fields
                                  .forEach((key, value) {
                                vm.updatePriceCalculatorForms(
                                    key: key, value: '${value.value}');
                              });

                              vm.updateStep(
                                  step: vm.priceCalculatorWizardStep + 1);
                            }
                            break;
                          case 8:
                            final clientVm = context.read<ClientsViewModel>();
                            final tenderFolderVm =
                                context.read<TenderFolderViewModel>();
                            final createOfferVm =
                                context.read<CreateOfferViewModel>();

                            if (formKey.currentState!.validate()) {
                              formKey.currentState!.fields
                                  .forEach((key, value) {
                                vm.updatePriceCalculatorForms(
                                    key: key, value: '${value.value}');
                              });

                              final signature = await signatureDialog(
                                  context: context,
                                  label: context
                                      .read<SharedDataViewmodel>()
                                      .termsLabel,
                                  htmlTerms: context
                                      .read<SharedDataViewmodel>()
                                      .termsHtml,
                                  buttonLabel: tr('send_offer'));

                              if (signature != null) {
                                modalManager.showLoadingModal();

                                await tryCatchWrapper(
                                        context: myGlobals
                                            .homeScaffoldKey!.currentContext,
                                        function: vm.submitPriceCalculator(
                                            signature: signature))
                                    .then((value) async {
                                  if (!mounted) return;

                                  if ((value ?? false)) {
                                    if (clientVm.partnerJobs.isNotEmpty) {
                                      await tryCatchWrapper(
                                              context: myGlobals
                                                  .homeScaffoldKey!
                                                  .currentContext,
                                              function: clientVm.getClients(
                                                  tenderVm: tenderFolderVm,
                                                  createOfferVm: createOfferVm))
                                          .then((value) {
                                        if (!mounted) return;

                                        modalManager.hideLoadingModal();
                                        successCreatingPriceCalculationDialog(
                                                context: context,
                                                homeOwnerName:
                                                    vm.priceCalculatorForms[
                                                        'homeOwnerName'])
                                            .then((value) {
                                          backDrawerRoute();
                                          changeDrawerRoute(Routes.yourClient);
                                          vm.resetPriceCalculationStates();
                                        });
                                      });
                                    } else {
                                      modalManager.hideLoadingModal();
                                      successCreatingPriceCalculationDialog(
                                              context: context,
                                              homeOwnerName:
                                                  vm.priceCalculatorForms[
                                                      'homeOwnerName'])
                                          .then((value) {
                                        backDrawerRoute();
                                        changeDrawerRoute(Routes.yourClient);
                                        vm.resetPriceCalculationStates();
                                      });
                                    }
                                  } else {
                                    modalManager.hideLoadingModal();
                                  }
                                });
                              }
                            }

                            break;
                          default:
                        }
                      },
                      child: Text(
                        nextButtonTitle,
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(color: Colors.white),
                      ))),
            ]),
          ],
        ),
      );
    });
  }

  Widget step(
      {required int step,
      required GlobalKey<FormBuilderState> formKey,
      required PriceCalculationViewmodel vm}) {
    switch (step + 1) {
      case 1:
        return PriceCalculatorStepOne(
          vm: vm,
          formKey: formKey,
        );
      case 2:
        return PriceCalculatorStepTwo(
          vm: vm,
        );
      case 3:
        return PriceCalculatorStepThree(
          vm: vm,
        );
      case 4:
        return PriceCalculatorStepFour(
          vm: vm,
        );
      case 5:
        return PriceCalculatorStepFive(
          vm: vm,
        );
      case 6:
        return PriceCalculatorStepSix(
          vm: vm,
        );
      case 7:
        return PriceCalculatorStepSeven(
          vm: vm,
        );
      case 8:
        return PriceCalculatorStepEight(
          vm: vm,
          formKey: formKey,
        );
      default:
        return PriceCalculatorStepOne(
          vm: vm,
          formKey: formKey,
        );
    }
  }
}
