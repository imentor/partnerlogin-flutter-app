import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/phone_numbers_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class PhoneNumbersScreen extends StatefulWidget {
  const PhoneNumbersScreen({super.key});

  @override
  PhoneNumbersScreenState createState() => PhoneNumbersScreenState();
}

class PhoneNumbersScreenState extends State<PhoneNumbersScreen> {
  late TextEditingController _phoneNumberController;
  late FocusNode _phoneNumberFocus;
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  void initState() {
    _phoneNumberController = TextEditingController();
    _phoneNumberFocus = FocusNode();
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final newsFeedVm = context.read<NewsFeedViewModel>();
      final appDrawerVm = context.read<AppDrawerViewModel>();
      const dynamicStoryPage = 'ekstraphone';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      if (dynamicStoryEnable) {
        await tryCatchWrapper(
          context: myGlobals.homeScaffoldKey!.currentContext,
          function: Future.wait([
            context.read<PhoneNumbersViewModel>().getPhoneNumbers(),
            newsFeedVm.getDynamicStory(page: dynamicStoryPage),
          ]),
        ).whenComplete(() {
          if (!mounted) return;

          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      } else {
        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: Future.value(
                context.read<PhoneNumbersViewModel>().getPhoneNumbers()));
      }
    });
  }

  @override
  void dispose() {
    _phoneNumberController.dispose();
    _phoneNumberFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(20),
          child: _getBody(),
        ),
      ),
    );
  }

  Widget _getBody() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(tr('your_phone_numbers'),
            style: Theme.of(context).textTheme.headlineMedium),
        SmartGaps.gapH5,
        Text(tr('phone_number_desc'),
            style: Theme.of(context).textTheme.titleSmall),
        SmartGaps.gapH10,
        if (context.watch<PhoneNumbersViewModel>().busy)
          const SizedBox(
            height: 200,
            child: Center(
              child: ConnectivityLoader(),
            ),
          )
        else ...[
          _getAddField(),
          SmartGaps.gapH10,
          Text(tr('saved_phone_numbers'),
              style: Theme.of(context).textTheme.headlineSmall),
          SmartGaps.gapH10,
          const PhoneList(),
        ],
      ],
    );
  }

  Widget _getAddField() {
    return Form(
      key: formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SmartGaps.gapH5,
          Text(tr('enter_phone_number'),
              style: Theme.of(context)
                  .textTheme
                  .titleSmall!
                  .copyWith(height: 1.2, fontWeight: FontWeight.bold)),
          SmartGaps.gapH7,
          TextFormField(
            keyboardType: TextInputType.number,
            controller: _phoneNumberController,
            focusNode: _phoneNumberFocus,
            decoration: InputDecoration(
              border: InputBorder.none,
              filled: true,
              fillColor: Colors.grey[200],
              hintStyle: Theme.of(context)
                  .textTheme
                  .bodyMedium!
                  .copyWith(fontWeight: FontWeight.w500),
              hintText: tr('must_contain_8_digits'),
            ),
            inputFormatters: [
              FilteringTextInputFormatter.deny(
                RegExp('[\\.|\\,|\\-]'),
              ),
            ],
            validator: (value) {
              final text = value;
              if (text == null || text.isEmpty) {
                return tr('please_input_a_valid_number');
              }

              if (text.length < 8 || text.length > 8) {
                return tr('phone_number_8_digit');
              }

              return null;
            },
          ),
          SmartGaps.gapH20,
          SizedBox(
            width: 150,
            height: 50,
            child: CustomDesignTheme.flatButtonStyle(
              disabledBackgroundColor: Colors.grey,
              backgroundColor: Theme.of(context).colorScheme.secondary,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0),
              ),
              onPressed: () async {
                if (formKey.currentState!.validate()) {
                  _phoneNumberFocus.unfocus();
                  await addTelefon();
                }
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(tr('save'),
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium!
                          .copyWith(color: Colors.white, height: 1)),
                  SmartGaps.gapW10,
                  const Icon(
                    ElementIcons.right,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> addTelefon() async {
    var vm = context.read<PhoneNumbersViewModel>();

    showLoadingDialog(context, loadingKey);
    try {
      await vm
          .addPhoneNumber(phoneNumber: _phoneNumberController.text.trim())
          .whenComplete(() {
        if (!mounted) return;

        Navigator.of(loadingKey.currentContext!).pop();
        _phoneNumberController.clear();
        showSuccessAnimationDialog(
            context, true, tr('add_phone_number_success'));
      });
    } on ApiException catch (e) {
      Navigator.of(loadingKey.currentContext!).pop();
      showErrorDialog(myGlobals.homeScaffoldKey!.currentContext!, '', e.message,
          tr('continue'));
    }
  }
}

class PhoneList extends StatefulWidget {
  const PhoneList({super.key});

  @override
  PhoneListState createState() => PhoneListState();
}

class PhoneListState extends State<PhoneList> {
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    var vm = Provider.of<PhoneNumbersViewModel>(context, listen: true);

    return vm.currentPhoneNumbers.isEmpty
        ? const EmptyListIndicator(route: Routes.phoneNumber)
        : LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ListTile(
                      title: Row(
                    children: <Widget>[
                      SizedBox(
                        width: constraints.maxWidth * 0.985 / 3,
                        child: Text(tr('telephone'),
                            style: Theme.of(context)
                                .textTheme
                                .titleSmall!
                                .copyWith(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        width: constraints.maxWidth * 0.985 / 3,
                        child: Text(tr('date'),
                            style: Theme.of(context)
                                .textTheme
                                .titleSmall!
                                .copyWith(fontWeight: FontWeight.bold)),
                      )
                    ],
                  )),
                  ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: vm.currentPhoneNumbers.length,
                    itemBuilder: (context, index) {
                      var item = vm.currentPhoneNumbers.elementAt(index);

                      return ListTile(
                        title: Row(
                          children: <Widget>[
                            SizedBox(
                              width: constraints.maxWidth * 0.985 / 3,
                              child: Text(item.phone!,
                                  style:
                                      Theme.of(context).textTheme.titleSmall),
                            ),
                            SizedBox(
                              width: constraints.maxWidth * 0.985 / 3,
                              child: Text(item.dateCreated!.split(" ").first,
                                  style:
                                      Theme.of(context).textTheme.titleSmall),
                            ),
                            IconButton(
                              icon: const Icon(ElementIcons.delete,
                                  color: Colors.red),
                              onPressed: () async {
                                var response = await showOkCancelAlertDialog(
                                    context: context,
                                    message: tr('are_you_sure'),
                                    title: tr('delete'),
                                    okLabel: tr('delete'),
                                    cancelLabel: tr('cancel'));

                                if (response == OkCancelResult.ok) {
                                  await telefonRemove(phoneid: item.id);
                                }
                              },
                            )
                          ],
                        ),
                      );
                    },
                  ),
                ],
              );
            },
          );
  }

  Future<void> telefonRemove({required phoneid}) async {
    var vm = context.read<PhoneNumbersViewModel>();

    showLoadingDialog(context, loadingKey);
    try {
      await vm.deletePhoneNumber(phoneId: phoneid).whenComplete(() {
        if (!mounted) return;

        Navigator.of(loadingKey.currentContext!).pop();

        showSuccessAnimationDialog(
            context, true, tr('delete_phone_number_success'));
      });
    } on ApiException catch (e) {
      Navigator.of(loadingKey.currentContext!).pop();
      showErrorDialog(myGlobals.homeScaffoldKey!.currentContext!, '', e.message,
          tr('continue'));
    }
  }
}
