import 'package:Haandvaerker.dk/ui/components/custom_button.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class ChangePasswordDomainEmailScreen extends StatefulWidget {
  const ChangePasswordDomainEmailScreen({super.key});

  @override
  ChangePasswordDomainEmailScreenState createState() =>
      ChangePasswordDomainEmailScreenState();
}

class ChangePasswordDomainEmailScreenState
    extends State<ChangePasswordDomainEmailScreen> {
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _oldPasswordController = TextEditingController();
  final _newPasswordController = TextEditingController();

  @override
  void dispose() {
    _oldPasswordController.dispose();
    _newPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Center(
          child: Form(
        key: _formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Center(
            child: ListView(
              physics: const ClampingScrollPhysics(),
              shrinkWrap: true,
              children: <Widget>[
                Text(tr('change_email_password'),
                    textAlign: TextAlign.center,
                    // tr('first_name'),
                    style: Theme.of(context).textTheme.headlineLarge),
                SmartGaps.gapH20,
                Text(tr('old_password'),
                    style: Theme.of(context).textTheme.titleSmall),
                SmartGaps.gapH5,
                TextFormField(
                    keyboardType: TextInputType.text,
                    obscureText: false,
                    controller: _oldPasswordController,
                    decoration: const InputDecoration(
                        border: InputBorder.none,
                        filled: true,
                        hintStyle: TextStyle(fontSize: 13),
                        hintText: '********'),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return tr('please_enter_some_text');
                      }
                      if (value.length < 12) {
                        return context
                            .tr('password_must_be_at_least_12_characters');
                      }
                      return null;
                    }),
                SmartGaps.gapH20,
                Text(tr('new_password'),
                    style: Theme.of(context).textTheme.titleSmall),
                SmartGaps.gapH5,
                TextFormField(
                    keyboardType: TextInputType.text,
                    obscureText: false,
                    controller: _newPasswordController,
                    decoration: const InputDecoration(
                        border: InputBorder.none,
                        filled: true,
                        hintStyle: TextStyle(fontSize: 13),
                        hintText: '********'),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return tr('please_enter_some_text');
                      }
                      if (value.length < 12) {
                        return context
                            .tr('password_must_be_at_least_12_characters');
                      }
                      return null;
                    }),
                SmartGaps.gapH50,
                SubmitButton(
                  text: tr('change_password'),
                  onPressed: () async {
                    // if (_formKey.currentState.validate()) {
                    //   showLoadingDialog(context, loadingKey);
                    //   final emailVM =
                    //       Provider.of<EmailViewModel>(context, listen: false);
                    //   print('old: ${_oldPasswordController.text}');
                    //   print('new: ${_newPasswordController.text}');
                    //   await emailVM.changeEmailPassword(
                    //       oldPassword: _oldPasswordController.text,
                    //       newPassword: _newPasswordController.text);
                    //   if (emailVM.isChangePasswordSuccess) {
                    //     emailVM.storeEmailCredentialsToCache(
                    //         password: _newPasswordController.text);
                    //   }
                    //   Navigator.of(loadingKey.currentContext).pop();
                    //   showSuccessAnimationDialog(
                    //       myGlobals.homeScaffoldKey.currentContext,
                    //       emailVM.isChangePasswordSuccess,
                    //       getLocal(
                    //           myGlobals.homeScaffoldKey.currentContext,
                    //           emailVM.isChangePasswordSuccess
                    //               ? 'your_password_was_changed_successfully'
                    //               : 'error_in_changing_password'));
                    // }
                  },
                ),
                SmartGaps.gapH20,
              ],
            ),
          ),
        ),
      )),
    );
  }
}
