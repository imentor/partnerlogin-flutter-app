import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/model/file/file_data_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/file_format_svg.dart';
import 'package:Haandvaerker.dk/ui/components/image_viewer.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app_links/app_links_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:path/path.dart' as path;
import 'package:provider/provider.dart';

class TenderFolder extends StatefulWidget {
  const TenderFolder({
    super.key,
    required this.job,
  });

  final PartnerJobModel? job;

  @override
  TenderFolderState createState() => TenderFolderState();
}

class TenderFolderState extends State<TenderFolder> {
  @override
  void initState() {
    final tenderVm = context.read<TenderFolderViewModel>();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final appLinksVm = context.read<AppLinksViewModel>();

      int? tenderFolderId;

      if (widget.job?.parentProject != null &&
          widget.job?.parentProject?.id != null) {
        tenderFolderId =
            widget.job?.parentProject?.projectTenderFolderId as int?;
      } else {
        tenderFolderId = widget.job?.projectTenderFolderId;
      }

      await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey?.currentContext ?? context,
        function: tenderVm.getFiles(folderId: tenderFolderId!),
      ).whenComplete(() {
        if (appLinksVm.pageRedirect == 'uploadinvoice' &&
            tenderVm.files.where((l) => l.nameDa == 'Faktura').isNotEmpty) {
          changeDrawerRoute(
            Routes.viewTenderFolder,
            arguments: {
              'files': [],
              'projectParentId': widget.job!.projectId,
              'parentFile':
                  tenderVm.files.firstWhere((l) => l.nameDa == 'Faktura'),
              'jobProjectName': widget.job!.projectName,
            },
          );
          appLinksVm.resetLoginAppLink();
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SafeArea(
        top: true,
        bottom: true,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  tr('description'),
                  style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                      ),
                ),
                SmartGaps.gapH10,
                GestureDetector(
                  onTap: () {
                    changeDrawerRoute(Routes.jobDescriptions,
                        arguments: widget.job);
                  },
                  child: const DescriptionCard(),
                ),
                SmartGaps.gapH20,
                Consumer<TenderFolderViewModel>(
                  builder: (context, tenderVm, child) {
                    List<FileData> files = [
                      ...tenderVm.files.where((l) => l.type == 'file')
                    ];
                    List<FileData> folders = [
                      ...tenderVm.files.where((l) => l.type == 'folder')
                    ];

                    if (tenderVm.busy) {
                      return const Center(child: ConnectivityLoader());
                    }

                    if (tenderVm.files.isEmpty) {
                      return const EmptyListIndicator(
                          route: Routes.tenderFolder);
                    }
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          tr('files'),
                          style: Theme.of(context)
                              .textTheme
                              .headlineLarge!
                              .copyWith(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                              ),
                        ),
                        SmartGaps.gapH10,
                        if (files.isEmpty)
                          Text(
                            tr('empty'),
                            style: Theme.of(context)
                                .textTheme
                                .headlineLarge!
                                .copyWith(
                                  fontSize: 20,
                                  fontWeight: FontWeight.normal,
                                ),
                          )
                        else
                          ...files.map(
                            (file) => GestureDetector(
                              onTap: () async {
                                if (file.type != 'file') {
                                  dynamic files = [];
                                  if (file.name == 'Public' ||
                                      file.name == 'Archive') {
                                    files = null;
                                  } else {
                                    files = widget.job!.tenderFiles!
                                        .where((element) =>
                                            element.type!.toLowerCase() ==
                                            file.name!.toLowerCase())
                                        .toList();
                                  }
                                  changeDrawerRoute(
                                    Routes.viewTenderFolder,
                                    arguments: {
                                      'files': files,
                                      'projectParentId': widget.job!.projectId,
                                      'parentFile': file,
                                      'jobProjectName': widget.job!.projectName,
                                    },
                                  );
                                } else {
                                  switch (path.extension(file.name!)) {
                                    case '.pdf':
                                      if (file.downloadUrl != null) {
                                        pdfViewerV2Dialog(
                                            context: context,
                                            pdfUrl: file.downloadUrl!);
                                      }
                                      break;
                                    case '.jpg':
                                    case '.png':
                                    case '.jpeg':
                                    case '.heif':
                                    case '.hevc':
                                      imageViewerDialog(
                                          context: context,
                                          fileUrl: file.downloadUrl!);
                                      break;
                                    default:
                                      if (file.downloadUrl != null) {
                                        changeDrawerRoute(Routes.inAppWebView,
                                            arguments:
                                                Uri.parse(file.downloadUrl!));
                                      }
                                  }
                                }
                              },
                              child: FilerCard(file: file),
                            ),
                          ),
                        SmartGaps.gapH20,
                        Text(
                          tr('folders'),
                          style: Theme.of(context)
                              .textTheme
                              .headlineLarge!
                              .copyWith(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                              ),
                        ),
                        SmartGaps.gapH10,
                        if (folders.isEmpty)
                          Text(
                            tr('empty'),
                            style: Theme.of(context)
                                .textTheme
                                .headlineLarge!
                                .copyWith(
                                  fontSize: 20,
                                  fontWeight: FontWeight.normal,
                                ),
                          )
                        else
                          ...folders.map(
                            (file) => GestureDetector(
                              onTap: () async {
                                if (file.type != 'file') {
                                  dynamic files = [];
                                  if (file.name == 'Public' ||
                                      file.name == 'Archive') {
                                    files = null;
                                  } else {
                                    files = widget.job!.tenderFiles!
                                        .where((element) =>
                                            element.type!.toLowerCase() ==
                                            file.name!.toLowerCase())
                                        .toList();
                                  }
                                  changeDrawerRoute(
                                    Routes.viewTenderFolder,
                                    arguments: {
                                      'files': files,
                                      'projectParentId': widget.job!.projectId,
                                      'parentFile': file,
                                      'jobProjectName': widget.job!.projectName,
                                    },
                                  );
                                } else {
                                  switch (path.extension(file.name!)) {
                                    case '.pdf':
                                      if (file.downloadUrl != null) {
                                        pdfViewerV2Dialog(
                                            context: context,
                                            pdfUrl: file.downloadUrl!);
                                      }
                                      break;
                                    case '.jpg':
                                    case '.png':
                                    case '.jpeg':
                                    case '.heif':
                                    case '.hevc':
                                      imageViewerDialog(
                                          context: context,
                                          fileUrl: file.downloadUrl!);
                                      break;
                                    default:
                                      if (file.downloadUrl != null) {
                                        changeDrawerRoute(Routes.inAppWebView,
                                            arguments:
                                                Uri.parse(file.downloadUrl!));
                                      }
                                  }
                                }
                              },
                              child: FilerCard(file: file),
                            ),
                          ),
                      ],
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class DescriptionCard extends StatelessWidget {
  const DescriptionCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      margin: const EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
            color:
                Theme.of(context).colorScheme.primary.withValues(alpha: 0.25)),
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withValues(alpha: .1),
            blurRadius: 1.0,
            spreadRadius: -4,
            offset: const Offset(0, 5),
          )
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SvgPicture.asset(
            'assets/images/tender-desc.svg',
            width: 40,
            colorFilter: ColorFilter.mode(
              Theme.of(context).colorScheme.primary,
              BlendMode.srcIn,
            ),
          ),
          SmartGaps.gapW20,
          Expanded(
            child: Text(
              tr('question_answer'),
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                    fontSize: 20,
                    fontWeight: FontWeight.w400,
                  ),
            ),
          ),
        ],
      ),
    );
  }
}

class FilerCard extends StatelessWidget {
  const FilerCard({
    super.key,
    required this.file,
  });

  final FileData file;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      margin: const EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
            color:
                Theme.of(context).colorScheme.primary.withValues(alpha: 0.25)),
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withValues(alpha: .1),
            blurRadius: 1.0,
            spreadRadius: -4,
            offset: const Offset(0, 5),
          )
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (file.type == 'file')
            fileTypeIconSvg(
                fileType: path.extension(file.name!),
                colorFilter: const ColorFilter.mode(
                  PartnerAppColors.blue,
                  BlendMode.srcIn,
                ))
          else
            SvgPicture.asset(
              'assets/images/tender-folder.svg',
              width: 40,
              colorFilter: ColorFilter.mode(
                Theme.of(context).colorScheme.primary,
                BlendMode.srcIn,
              ),
            ),
          SmartGaps.gapW20,
          Expanded(
            child: Text(
              file.type == 'file'
                  ? file.name ?? ''
                  : context.locale.languageCode == 'da'
                      ? (file.nameDa ?? '')
                      : (file.name ?? ''),
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                    fontSize: 20,
                    fontWeight: FontWeight.w400,
                  ),
            ),
          ),
        ],
      ),
    );
  }
}
