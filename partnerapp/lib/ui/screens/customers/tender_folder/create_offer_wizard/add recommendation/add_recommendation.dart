import 'package:Haandvaerker.dk/ui/components/reviews/review_list.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class AddRecommendation extends StatelessWidget {
  const AddRecommendation({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        //

        GestureDetector(
          onTap: () async {
            final offerVm = context.read<CreateOfferViewModel>();

            await chooseReviewListDialog(
                    context: context,
                    alreadySelectedId:
                        context.read<CreateOfferViewModel>().offerReviewId)
                .then((reviewIds) {
              offerVm.offerReviewId = reviewIds ?? [];
            });
          },
          child: Row(
            children: [
              //

              const Icon(
                Icons.add_rounded,
                color: Color(0xff499DBB),
              ),

              SmartGaps.gapW5,

              Text(
                tr('add_your_recommendation'),
                style: GoogleFonts.notoSans(
                  textStyle: const TextStyle(
                    fontSize: 12,
                    color: Color(0xff499DBB),
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),

              //
            ],
          ),
        ),

        SmartGaps.gapW10,

        GestureDetector(
          onTap: () async {
            await showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    backgroundColor: Colors.white,
                    titlePadding: EdgeInsets.zero,
                    insetPadding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 120),
                    alignment: Alignment.topCenter,
                    title: SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: 200,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          //

                          Align(
                            alignment: Alignment.topRight,
                            child: IconButton(
                              onPressed: () {
                                //

                                Navigator.pop(context);

                                //
                              },
                              icon: Stack(
                                alignment: Alignment.center,
                                children: [
                                  //

                                  Container(
                                    width: 23,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color:
                                            Colors.black.withValues(alpha: 0.4),
                                        width: 2,
                                      ),
                                      shape: BoxShape.circle,
                                    ),
                                  ),

                                  Icon(
                                    Icons.close,
                                    size: 18,
                                    color: Colors.black.withValues(alpha: 0.4),
                                  ),

                                  //
                                ],
                              ),
                            ),
                          ),

                          Padding(
                            padding: const EdgeInsets.all(30),
                            child: Text(
                              tr('add_recommendation_suggestion'),
                              textAlign: TextAlign.center,
                              style: GoogleFonts.notoSans(
                                textStyle: const TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ),

                          //
                        ],
                      ),
                    ),
                  );
                });
          },
          child: SvgPicture.asset(
            'assets/images/question-mark.svg',
          ),
        )
      ],
    );
  }
}
