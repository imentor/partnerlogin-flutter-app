part of '../create_offer_page_wrapper.dart';

class CreateOfferWizardStep1 extends StatelessWidget {
  const CreateOfferWizardStep1({super.key});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final secondaryOfferListV3 = context
        .select<CreateOfferViewModel, List<List<ListPricePartnerAiV3Model>>>(
            (model) => model.secondaryOfferListV3);

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // PRIMARY OFFER LIST
          CustomExpansionTile(
            textColor: Colors.white,
            backgroundColor: Theme.of(context).colorScheme.onSurfaceVariant,
            iconColor: Colors.white,
            tileSize: Size(size.width, 40),
            titleSize: 18,
            title: tr('offer_list'),
            child: const OfferListTemplate(),
          ),

          SmartGaps.gapH40,

          // OTHER OFFER LIST
          if (secondaryOfferListV3.isNotEmpty &&
              secondaryOfferListV3.first.isNotEmpty)
            CustomExpansionTile(
              textColor: Colors.white,
              backgroundColor: Theme.of(context).colorScheme.onSurfaceVariant,
              iconColor: Colors.white,
              tileSize: Size(size.width, 40),
              titleSize: 18,
              title: tr('see_other_companies'),
              child:
                  SeeOtherCompanies(secondaryOfferList: secondaryOfferListV3),
            ),

          // SmartGaps.gapH80,
        ],
      ),
    );
  }
}
