part of '../create_offer_page_wrapper.dart';

class CreateOfferWizardStep2 extends StatefulWidget {
  const CreateOfferWizardStep2({super.key});

  @override
  State<CreateOfferWizardStep2> createState() => _CreateOfferWizardStep2State();
}

class _CreateOfferWizardStep2State extends State<CreateOfferWizardStep2> {
  final key = GlobalKey();
  final key1 = GlobalKey();

  String formatDate(DateTime date) {
    String year = date.year.toString();
    String month =
        date.month < 10 ? '0${date.month.toString()}' : date.month.toString();
    String day =
        date.day < 10 ? '0${date.day.toString()}' : date.day.toString();

    return '$year-$month-$day';
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Form(
        key: context.watch<CreateOfferViewModel>().formKeys[1],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //EXPECTED NUMBER OF DAYS
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //

                Text(
                  tr('estimate_days_project'),
                  textAlign: TextAlign.left,
                  style: GoogleFonts.notoSans(
                    textStyle: const TextStyle(
                      fontSize: 16,
                      height: 1.38,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),

                SmartGaps.gapH20,

                _templateTextField(
                    controller: context
                        .watch<CreateOfferViewModel>()
                        .utilityControllers['days']!,
                    isDate: false,
                    fieldKey: key)

                //
              ],
            ),

            SmartGaps.gapH40,

            //EXPECTED DATE
            //EXPECTED NUMBER OF DAYS
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //

                Text(
                  tr('project_start_date'),
                  textAlign: TextAlign.left,
                  style: GoogleFonts.notoSans(
                    textStyle: const TextStyle(
                      fontSize: 16,
                      height: 1.38,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),

                SmartGaps.gapH20,

                InkWell(
                  onTap: () async {
                    final createOfferVm = context.read<CreateOfferViewModel>();
                    FocusScope.of(context).unfocus();
                    final date = await showDatePicker(
                      context: context,
                      locale: context.locale,
                      initialDate: createOfferVm.startDate.isEmpty
                          ? DateTime.now()
                          : DateTime.parse(createOfferVm.startDate),
                      firstDate: DateTime.now(),
                      lastDate: DateTime(3000),
                    );
                    if (date != null && mounted) {
                      createOfferVm.startDate = formatDate(date);
                      createOfferVm.convertDate();

                      final utilityControllers =
                          <String, TextEditingController>{
                        ...createOfferVm.utilityControllers
                      };

                      utilityControllers['date']!.text =
                          createOfferVm.startDate;

                      createOfferVm.utilityControllers = utilityControllers;
                    }
                  },
                  child: IgnorePointer(
                    child: _templateTextField(
                        controller: context
                            .watch<CreateOfferViewModel>()
                            .utilityControllers['date']!,
                        isDate: true,
                        fieldKey: key1),
                  ),
                ),

                //
              ],
            ),
            SmartGaps.gapH40,

            // VALIDITY OF OFFER
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: '${tr('your_offer_is_valid')} ',
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        letterSpacing: 1.0,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                        fontSize: 18),
                  ),
                  TextSpan(
                    text: context
                        .watch<CreateOfferViewModel>()
                        .offerSignatureValid,
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        letterSpacing: 1.0,
                        color: PartnerAppColors.blue,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                  TextSpan(
                    text: ' ${tr('days_valid')}',
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        letterSpacing: 1.0,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                        fontSize: 18),
                  )
                ],
              ),
            ),
            SmartGaps.gapH10,
            DropdownButtonHideUnderline(
                child: ButtonTheme(
              alignedDropdown: true,
              child: CustomDropdownFormBuilder(
                items: [
                  ...List.generate(
                    30,
                    (index) => DropdownMenuItem(
                        value: '${index + 1}', child: Text('${index + 1}')),
                  ),
                ],
                name: 'offerSignatureValid',
                initialValue:
                    context.read<CreateOfferViewModel>().offerSignatureValid,
                onChanged: (p0) {
                  if (p0 != null) {
                    context.read<CreateOfferViewModel>().offerSignatureValid =
                        p0;
                  }
                },
              ),
            ))
          ],
        ),
      ),
    );
  }

  TextFormField _templateTextField({
    required TextEditingController controller,
    required bool isDate,
    required GlobalKey fieldKey,
  }) {
    return TextFormField(
      key: fieldKey,
      validator: (value) {
        if (value!.isEmpty) {
          Scrollable.ensureVisible(
            fieldKey.currentContext!,
            alignment: 0.4,
          );
          return isDate ? tr('date_validator') : tr('days_validator');
        }

        if (!isDate) {
          if (int.parse(value) > 1500) {
            return tr('days_validator_less_1500');
          }
        }
        return null;
      },
      inputFormatters: isDate
          ? null
          : [
              FilteringTextInputFormatter.digitsOnly,
            ],
      onChanged: isDate
          ? null
          : (value) {
              if (value.isNotEmpty) {
                context.read<CreateOfferViewModel>().numberOfWeeksToFinish =
                    int.parse(value);
              } else {
                context.read<CreateOfferViewModel>().numberOfWeeksToFinish = 0;
              }
            },
      keyboardType: isDate ? null : TextInputType.number,
      textAlign: TextAlign.left,
      controller: controller,
      maxLength: isDate ? null : 4,
      decoration: InputDecoration(
        errorStyle: const TextStyle(height: 0),
        suffixIcon: !isDate
            ? null
            : Padding(
                padding: const EdgeInsets.all(10.0),
                child: SvgPicture.asset(
                  'assets/images/calendar.svg',
                  width: 27,
                  colorFilter: const ColorFilter.mode(
                    PartnerAppColors.accentBlue,
                    BlendMode.srcIn,
                  ),
                ),
              ),
        counterText: '',
        hintText: isDate ? tr('date_hint_text') : tr('days_hint_text'),
        contentPadding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        border: OutlineInputBorder(
          borderSide: const BorderSide(color: PartnerAppColors.accentBlue),
          borderRadius: BorderRadius.circular(3),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: PartnerAppColors.accentBlue),
          borderRadius: BorderRadius.circular(3),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: PartnerAppColors.accentBlue),
          borderRadius: BorderRadius.circular(3),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: PartnerAppColors.accentBlue),
          borderRadius: BorderRadius.circular(3),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: PartnerAppColors.accentBlue),
          borderRadius: BorderRadius.circular(3),
        ),
      ),
    );
  }
}
