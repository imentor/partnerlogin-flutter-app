part of '../create_offer_page_wrapper.dart';

class CreateOfferWizardStepTimeline extends StatelessWidget {
  const CreateOfferWizardStepTimeline({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 80,
      child: DynamicStepsWidget(
        currentPage: context.watch<CreateOfferViewModel>().currentWizardStep,
        steps: 4,
        titles: [
          '${tr('step')} 1',
          '${tr('step')} 2',
          '${tr('step')} 3',
          '${tr('step')} 4',
        ],
        isLabelAtTop: false,
      ),
    );
  }
}
