part of '../create_offer_page_wrapper.dart';

class CreateOfferWizardStep4 extends StatelessWidget {
  const CreateOfferWizardStep4({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //

          Text(
            tr('summary'),
            style: GoogleFonts.notoSans(
              textStyle: const TextStyle(
                fontSize: 24,
                height: 1.6,
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),

          SmartGaps.gapH15,

          for (final offer in context.select<CreateOfferViewModel,
                  List<List<ListPricePartnerAiV3Model>>>(
              (model) => model.primaryOfferListV3))
            ...offer.map(
              (price) {
                //
                List<String> descriptions = [];

                if (context
                    .watch<CreateOfferViewModel>()
                    .editableDescriptions
                    .containsKey("${price.industry!}_${price.jobIndustry!}")) {
                  descriptions = [
                    ...context
                            .watch<CreateOfferViewModel>()
                            .editableDescriptions[
                        "${price.industry!}_${price.jobIndustry!}"]
                  ];
                }

                final enterpriseIndex = context
                    .select<CreateOfferViewModel,
                            List<List<ListPricePartnerAiV3Model>>>(
                        (model) => model.primaryOfferListV3)
                    .indexOf(offer);
                final jobIndustryIndex = offer.indexOf(price);

                final jobData = JobIndustryModelCard(
                  enterpriseId: price.industry!.toString(),
                  enterpriseIndex: enterpriseIndex,
                  jobIndustryId: price.jobIndustry!.toString(),
                  jobIndustryIndex: jobIndustryIndex,
                  descriptions: descriptions,
                );

                return JobIndustryOfferCard(jobData: jobData, isSummary: true);
              },
            ),

          // CALCULATION AND OTHER FIELDS
          SmartGaps.gapH30,

          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: OfferCalculationFields(),
          ),

          Divider(
            height: 60,
            color: Theme.of(context)
                .colorScheme
                .onTertiaryContainer
                .withValues(alpha: 0.6),
          ),

          //DATE FIELDS
          DateFieldSummaryTemplate(
            label: tr('estimate_days_project'),
            value:
                '${context.watch<CreateOfferViewModel>().utilityControllers['days']!.text.trim()} ${tr('days')}',
          ),
          SmartGaps.gapH25,
          DateFieldSummaryTemplate(
            label: tr('project_start_date'),
            value: Formatter.formatDateStrings(
                type: DateFormatType.standardDate,
                dateString: context
                    .watch<CreateOfferViewModel>()
                    .utilityControllers['date']!
                    .text
                    .trim()),
          ),
          SmartGaps.gapH25,
          RichText(
            text: TextSpan(
                text: 'Dit tilbud er gældende ',
                style: GoogleFonts.notoSans(
                  textStyle: const TextStyle(
                    fontSize: 16,
                    height: 1,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.normal,
                  ),
                ),
                children: [
                  TextSpan(
                    text: context
                        .watch<CreateOfferViewModel>()
                        .offerSignatureValid,
                    style: GoogleFonts.notoSans(
                      textStyle: const TextStyle(
                        fontSize: 16,
                        height: 1,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  TextSpan(
                    text: ' dage fra din underskrift',
                    style: GoogleFonts.notoSans(
                      textStyle: const TextStyle(
                        fontSize: 16,
                        height: 1,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  )
                ]),
          ),

          Divider(
            height: 60,
            color: Theme.of(context)
                .colorScheme
                .onTertiaryContainer
                .withValues(alpha: 0.6),
          ),

          if (context
              .watch<CreateOfferViewModel>()
              .utilityControllers['standardText']!
              .text
              .trim()
              .isNotEmpty) ...[
            Text(
              tr('standard_text'),
              style: GoogleFonts.notoSans(
                textStyle: const TextStyle(
                  fontSize: 16,
                  height: 1,
                  color: PartnerAppColors.darkBlue,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            SmartGaps.gapH15,
            Text(
              context
                  .watch<CreateOfferViewModel>()
                  .utilityControllers['standardText']!
                  .text
                  .trim(),
              style: GoogleFonts.notoSans(
                textStyle: const TextStyle(
                  fontSize: 16,
                  height: 1,
                  color: Colors.black,
                  fontWeight: FontWeight.w300,
                ),
              ),
            ),
          ],

          // SmartGaps.gapH100,
          //
        ],
      ),
    );
  }
}
