part of '../create_offer_page_wrapper.dart';

class CreateOfferWizardStep3 extends StatefulWidget {
  const CreateOfferWizardStep3({super.key});

  @override
  State<CreateOfferWizardStep3> createState() => _CreateOfferWizardStep3State();
}

class _CreateOfferWizardStep3State extends State<CreateOfferWizardStep3> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Form(
        key: context.watch<CreateOfferViewModel>().formKeys[2],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //

            Text(
              tr('add_reservations_to_offer'),
              textAlign: TextAlign.left,
              style: GoogleFonts.notoSans(
                textStyle: const TextStyle(
                  fontSize: 18,
                  height: 1.38,
                  color: PartnerAppColors.darkBlue,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),

            SmartGaps.gapH20,

            //SAVE AS STANDARD TEXT
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Transform.scale(
                  scale: 1.2,
                  child: SizedBox(
                    width: 24,
                    height: 24,
                    child: Radio(
                      toggleable: true,
                      onChanged: (val) {
                        context.read<CreateOfferViewModel>().saveAsDefaultText =
                            val ?? false;
                      },
                      value: true,
                      groupValue: context.select(
                          (CreateOfferViewModel val) => val.saveAsDefaultText),
                      visualDensity: VisualDensity.compact,
                      activeColor: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                ),
                SmartGaps.gapW10,
                Text(
                  tr('save_as_default_text'),
                  style: GoogleFonts.notoSans(
                    textStyle: TextStyle(
                      fontSize: 12,
                      letterSpacing: 0.5,
                      height: 1,
                      fontWeight: FontWeight.w600,
                      color: context.select((CreateOfferViewModel val) =>
                              val.saveAsDefaultText)
                          ? Theme.of(context).colorScheme.onTertiaryContainer
                          : Theme.of(context)
                              .colorScheme
                              .onTertiaryContainer
                              .withValues(alpha: 0.3),
                    ),
                  ),
                ),
              ],
            ),

            SmartGaps.gapH15,

            //TEXTFIELD FOR STANDARD TEXT
            TextFormField(
              controller: context
                  .watch<CreateOfferViewModel>()
                  .utilityControllers['standardText'],
              maxLines: 12,
              minLines: 12,
              decoration: InputDecoration(
                hintText: tr('add_conditions_to_offer'),
                hintStyle: Theme.of(context).textTheme.headlineLarge!.copyWith(
                      fontSize: 14,
                      fontWeight: FontWeight.w100,
                      color: Colors.black.withValues(alpha: 0.3),
                    ),
                contentPadding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                border: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: PartnerAppColors.accentBlue,
                  ),
                  borderRadius: BorderRadius.circular(5),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: PartnerAppColors.accentBlue,
                  ),
                  borderRadius: BorderRadius.circular(5),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: Theme.of(context).colorScheme.error),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: PartnerAppColors.accentBlue,
                  ),
                  borderRadius: BorderRadius.circular(5),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: Theme.of(context).colorScheme.error),
                ),
              ),
            ),

            SmartGaps.gapH15,

            // WILL ADD THE RECOMMENDATIONS HERE BELOW
            // FOR NOW WILL LEAVE IT BLANK
            // NOT YET IMPLEMENTED IN APP
            //

            const AddRecommendation()
          ],
        ),
      ),
    );
  }
}
