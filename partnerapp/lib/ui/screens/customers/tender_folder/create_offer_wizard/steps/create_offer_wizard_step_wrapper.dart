part of '../create_offer_page_wrapper.dart';

class CreateOfferWizardStepWrapper extends StatelessWidget {
  const CreateOfferWizardStepWrapper({super.key});

  @override
  Widget build(BuildContext context) {
    switch (context.watch<CreateOfferViewModel>().currentWizardStep) {
      case 1:
        return const CreateOfferWizardStep2();
      case 2:
        return const CreateOfferWizardStep3();
      case 3:
        return const CreateOfferWizardStep4();
      default:
        return const CreateOfferWizardStep1();
    }
  }
}
