part of '../create_offer_page_wrapper.dart';

class OfferCalculationFields extends StatelessWidget {
  const OfferCalculationFields({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // SUBTOTAL WITHOUT VAT
        CalculationTemplate(
            label: tr('unit_price_ex_vat'),
            value: Formatter.curencyFormat(
                amount:
                    context.watch<CreateOfferViewModel>().subValueWithoutVat)),

        // VAT VALUE
        CalculationTemplate(
          label: tr('vat_value'),
          value: Formatter.curencyFormat(
              amount: context.watch<CreateOfferViewModel>().vatValue),
        ),

        // TOTAL
        CalculationTemplate(
          label: tr('total_with_vat'),
          value: Formatter.curencyFormat(
              amount: context.watch<CreateOfferViewModel>().totalValue),
        ),
      ],
    );
  }
}

class CalculationTemplate extends StatelessWidget {
  const CalculationTemplate({
    super.key,
    required this.label,
    required this.value,
  });

  final String label;
  final String value;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            flex: 3,
            child: Text(
              label,
              style: GoogleFonts.notoSans(
                textStyle: const TextStyle(
                  fontSize: 15,
                  height: 1.40,
                  color: Color(0xff505050),
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
          const Spacer(),
          Expanded(
            flex: 2,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              alignment: Alignment.centerRight,
              child: Text(
                value,
                textAlign: TextAlign.right,
                style: GoogleFonts.notoSans(
                  textStyle: const TextStyle(
                    fontSize: 21,
                    height: 1.33,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
