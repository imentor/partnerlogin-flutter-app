part of '../create_offer_page_wrapper.dart';

class OfferListTemplate extends StatefulWidget {
  const OfferListTemplate({
    super.key,
  });

  @override
  State<OfferListTemplate> createState() => _OfferListTemplateState();
}

class _OfferListTemplateState extends State<OfferListTemplate> {
  @override
  Widget build(BuildContext context) {
    return Form(
      key: context.watch<CreateOfferViewModel>().formKeys[0],
      child: Column(
        children: [
          SmartGaps.gapH20,
          if (context
              .select<CreateOfferViewModel,
                      List<List<ListPricePartnerAiV3Model>>>(
                  (model) => model.primaryOfferListV3)
              .isNotEmpty)
            Column(
              children: [
                // JOB OFFER CARDS

                for (final offer in context.select<CreateOfferViewModel,
                        List<List<ListPricePartnerAiV3Model>>>(
                    (model) => model.primaryOfferListV3))
                  ...offer.map(
                    (price) {
                      //

                      final editableDescriptions = context
                          .watch<CreateOfferViewModel>()
                          .editableDescriptions;

                      final enterpriseIndex = context
                          .select<CreateOfferViewModel,
                                  List<List<ListPricePartnerAiV3Model>>>(
                              (model) => model.primaryOfferListV3)
                          .indexOf(offer);

                      final jobIndustryIndex = offer.indexOf(price);

                      List<String> descriptions = [];

                      if (editableDescriptions.containsKey(
                          "${price.industry!}_${price.jobIndustry!}")) {
                        if (editableDescriptions[
                                "${price.industry!}_${price.jobIndustry!}"]
                            is List) {
                          descriptions = [
                            ...editableDescriptions[
                                "${price.industry!}_${price.jobIndustry!}"]
                          ];
                        }
                      }

                      final jobData = JobIndustryModelCard(
                        enterpriseId: price.industry!.toString(),
                        enterpriseIndex: enterpriseIndex,
                        jobIndustryId: price.jobIndustry!.toString(),
                        jobIndustryIndex: jobIndustryIndex,
                        descriptions: descriptions,
                      );

                      return JobIndustryOfferCard(jobData: jobData);
                    },
                  ),

                Divider(
                  height: 60,
                  color: Theme.of(context)
                      .colorScheme
                      .onTertiaryContainer
                      .withValues(alpha: 0.6),
                ),

                // CALCULATION AND OTHER FIELDS

                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: OfferCalculationFields(),
                ),

                Divider(
                  height: 60,
                  color: Theme.of(context)
                      .colorScheme
                      .onTertiaryContainer
                      .withValues(alpha: 0.6),
                ),

                SmartGaps.gapH20,
              ],
            )
          else
            const Center(
              child: EmptyListIndicator(),
            ),
        ],
      ),
    );
  }
}
