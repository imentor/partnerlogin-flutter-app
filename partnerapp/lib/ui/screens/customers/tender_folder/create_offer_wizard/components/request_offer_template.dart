part of '../create_offer_page_wrapper.dart';

class SendRequestOfferTemplate extends StatelessWidget {
  const SendRequestOfferTemplate({
    super.key,
    required this.price,
  });

  final ListPricePartnerAiV3Model price;

  @override
  Widget build(BuildContext context) {
    final tenderVm = context.read<TenderFolderViewModel>();
    final createOfferVm = context.read<CreateOfferViewModel>();
    final locale = context.locale.languageCode;
    final jobType = tenderVm.fullIndustries.firstWhere(
      (element) => element.brancheId == price.jobIndustry,
      orElse: () => FullIndustry.defaults(),
    );

    return InkWell(
      onTap: () async {
        final payload = {
          'contactId': price.contactId,
          'industry': context
              .read<JobTypesViewModel>()
              .partnerJobtypes!
              .cPrimaryIndustry!
              .business,
          'jobIndustryId': price.jobIndustry,
          'projectId': price.projectId,
          'wizardId': price.id,
        };
        await showDialog(
          context: context,
          builder: (context) => SendRequestOfferDialog(
            payload: payload,
          ),
        );
      },
      child: Card(
        elevation: 5,
        margin: const EdgeInsets.symmetric(vertical: 10),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                createOfferVm.activeJob!.contractType == "Fagentreprise"
                    ? locale == 'da'
                        ? (jobType.brancheDa ?? '')
                        : (jobType.brancheEn ?? '')
                    : createOfferVm.activeJob?.projectName ?? '',
                style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                    ),
              ),
              SvgPicture.asset(
                'assets/images/padlock.svg',
                colorFilter: ColorFilter.mode(
                  Theme.of(context).colorScheme.primary,
                  BlendMode.srcIn,
                ),
                width: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SendRequestOfferDialog extends StatefulWidget {
  const SendRequestOfferDialog({
    super.key,
    required this.payload,
  });

  final Map<String, dynamic> payload;

  @override
  SendRequestOfferDialogState createState() => SendRequestOfferDialogState();
}

class SendRequestOfferDialogState extends State<SendRequestOfferDialog> {
  RequestResponse requestResponse = RequestResponse.idle;
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      backgroundColor: Colors.white,
      titlePadding: EdgeInsets.zero,
      title: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Row(
          children: [
            const Spacer(),
            IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    width: 23,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black.withValues(alpha: 0.4),
                        width: 2,
                      ),
                      shape: BoxShape.circle,
                    ),
                  ),
                  Icon(
                    Icons.close,
                    size: 18,
                    color: Colors.black.withValues(alpha: 0.4),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      contentPadding: const EdgeInsets.only(top: 20),
      content: context.watch<CreateOfferViewModel>().sendingRequestOffer
          ? const SizedBox(
              height: 200,
              child: Center(
                child: ConnectivityLoader(),
              ),
            )
          : requestResponse == RequestResponse.idle
              ? SvgPicture.asset(
                  'assets/images/req.svg',
                  width: 150,
                  colorFilter: ColorFilter.mode(
                    Theme.of(context)
                        .colorScheme
                        .onTertiaryContainer
                        .withValues(alpha: 0.7),
                    BlendMode.srcIn,
                  ),
                )
              : SizedBox(
                  height: 200,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        requestResponse == RequestResponse.success
                            ? tr('completed')
                            : tr('error_occurred'),
                        style:
                            Theme.of(context).textTheme.headlineLarge!.copyWith(
                                  fontSize: 30,
                                  fontWeight: FontWeight.w700,
                                ),
                      ),
                      SmartGaps.gapH10,
                      Text(
                        requestResponse == RequestResponse.success
                            ? tr('request_offer_sent').toUpperCase()
                            : tr('request_offer_failed').toUpperCase(),
                        textAlign: TextAlign.center,
                        style:
                            Theme.of(context).textTheme.headlineLarge!.copyWith(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w300,
                                ),
                      ),
                      SmartGaps.gapH20,
                      requestResponse == RequestResponse.success
                          ? SvgPicture.asset('assets/images/sadan.svg',
                              width: 100)
                          : Icon(
                              Icons.close,
                              size: 100,
                              color: Theme.of(context).colorScheme.error,
                            ),
                    ],
                  ),
                ),
      actionsPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
      actions: [
        Visibility(
          visible: context.watch<CreateOfferViewModel>().sendingRequestOffer
              ? false
              : requestResponse == RequestResponse.success ||
                      requestResponse == RequestResponse.failed
                  ? false
                  : true,
          child: CustomDesignTheme.flatButtonStyle(
            onPressed: () async {
              final createVm = context.read<CreateOfferViewModel>();

              createVm.sendingRequestOffer = true;
              await createVm
                  .sendRequestOffer(payload: widget.payload)
                  .then((response) {
                setState(() {
                  requestResponse = response;
                });

                createVm.sendingRequestOffer = false;
              });
            },
            backgroundColor: Theme.of(context).colorScheme.secondary,
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 50,
              child: Center(
                child: Text(
                  tr('request_to_submit_offer'),
                  style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                      ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
