part of '../create_offer_page_wrapper.dart';

class SeeOtherCompanies extends StatelessWidget {
  const SeeOtherCompanies({
    super.key,
    required this.secondaryOfferList,
  });

  final List<List<ListPricePartnerAiV3Model>> secondaryOfferList;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 20),
      child: Column(
        children: [
          if (context.watch<CreateOfferViewModel>().activeJob!.contractType ==
              'Fagentreprise')
            for (final offer in secondaryOfferList)
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SmartGaps.gapH10,
                  Text(
                    context.locale.languageCode == 'da'
                        ? context
                                .watch<TenderFolderViewModel>()
                                .wizardTexts
                                .firstWhere(
                                    (element) =>
                                        element.producttypeid ==
                                        offer.first.industry.toString(),
                                    orElse: () =>
                                        AllWizardText(producttypeDa: 'N/A'))
                                .producttypeDa ??
                            'N/A'
                        : context
                                .watch<TenderFolderViewModel>()
                                .wizardTexts
                                .firstWhere(
                                    (element) =>
                                        element.producttypeid ==
                                        offer.first.industry.toString(),
                                    orElse: () =>
                                        AllWizardText(producttypeEn: 'N/A'))
                                .producttypeEn ??
                            'N/A',
                  ),
                  ...offer.map(
                    (e) {
                      return SendRequestOfferTemplate(
                        price: e,
                      );
                    },
                  )
                ],
              )
          else
            SendRequestOfferTemplate(price: secondaryOfferList.first.first)
        ],
      ),
    );
  }
}
