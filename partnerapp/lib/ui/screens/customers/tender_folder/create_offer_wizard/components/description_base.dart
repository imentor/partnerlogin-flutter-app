part of '../create_offer_page_wrapper.dart';

class DescriptionBase extends StatefulWidget {
  const DescriptionBase({
    super.key,
    required this.description,
  });

  final DescriptionData description;

  @override
  State<DescriptionBase> createState() => _DescriptionBaseState();
}

class _DescriptionBaseState extends State<DescriptionBase> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.end,
        //   children: [
        //     GestureDetector(
        //       onTap: () {
        //         //

        //         FocusScope.of(context).unfocus();

        //         context.read<CreateOfferViewModel>().indexesToEdit = [
        //           ...context.read<CreateOfferViewModel>().indexesToEdit,
        //           widget.description.index
        //         ];

        //         //
        //       },
        //       child: SvgPicture.asset(
        //         'assets/images/extra_work_edit.svg',
        //         width: 19,
        //         colorFilter: const ColorFilter.mode(
        //           PartnerAppColors.accentBlue,
        //           BlendMode.srcIn,
        //         ),
        //       ),
        //     ),
        //     SmartGaps.gapW30,
        //     GestureDetector(
        //       onTap: () async {
        //         FocusScope.of(context).unfocus();
        //         final createOfferVm = context.read<CreateOfferViewModel>();

        //         final String key =
        //             '${widget.description.industry}_${widget.description.jobIndustry}';

        //         if (<String>[...createOfferVm.editableDescriptions[key]]
        //                 .where((element) => element != '')
        //                 .toList()
        //                 .length ==
        //             1) {
        //           await showErrorDialog(
        //             context,
        //             '',
        //             tr('delete_description_trap'),
        //             tr('confirm'),
        //           );
        //         } else {
        //           await showCustomDialog(
        //             context: context,
        //             continueFunction: () {
        //               final editable = <String>[
        //                 ...createOfferVm.editableDescriptions[key]
        //               ];

        //               if (widget.description.index <=
        //                   <String>[...createOfferVm.templateDescriptions[key]]
        //                           .length -
        //                       1) {
        //                 createOfferVm.deletedDescriptions[key] = {
        //                   ...createOfferVm.deletedDescriptions[key] ?? {},
        //                   "${widget.description.index}":
        //                       widget.description.description,
        //                 };

        //                 editable[widget.description.index] = '';

        //                 if (createOfferVm.updatedDescriptions.isNotEmpty &&
        //                     Map.from(createOfferVm.updatedDescriptions)
        //                         .containsKey(key)) {
        //                   if (Map.from(createOfferVm.updatedDescriptions[key])
        //                       .containsKey("${widget.description.index}")) {
        //                     final updated = {
        //                       ...Map.from(
        //                           createOfferVm.updatedDescriptions[key])
        //                     };

        //                     updated.remove("${widget.description.index}");

        //                     createOfferVm.updatedDescriptions[key] = updated;

        //                     createOfferVm.updatedDescriptions =
        //                         createOfferVm.updatedDescriptions;
        //                   }
        //                 }
        //               } else {
        //                 editable.removeAt(widget.description.index);
        //               }

        //               createOfferVm.editableDescriptions[key] = editable;

        //               createOfferVm.editableDescriptions =
        //                   createOfferVm.editableDescriptions;
        //             },
        //             contentText: tr('delete_description'),
        //           );
        //         }
        //       },
        //       child: SvgPicture.asset(
        //         'assets/images/Icon feather-trash-2.svg',
        //         width: 19,
        //         colorFilter: const ColorFilter.mode(
        //           PartnerAppColors.accentBlue,
        //           BlendMode.srcIn,
        //         ),
        //       ),
        //     ),
        //   ],
        // ),

        Padding(
          padding: const EdgeInsets.only(top: 13),
          child: Text(
            '${widget.description.numbering}.${widget.description.description}',
            style: GoogleFonts.notoSans(
              textStyle: const TextStyle(
                fontSize: 15,
                height: 1.81,
                color: Colors.black,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        )
      ],
    );
  }
}
