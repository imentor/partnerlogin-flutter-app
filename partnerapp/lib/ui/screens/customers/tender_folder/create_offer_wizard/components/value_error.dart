part of '../create_offer_page_wrapper.dart';

class ValueError extends StatelessWidget {
  const ValueError({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      backgroundColor: Colors.white,
      titlePadding: EdgeInsets.zero,
      title: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 300,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                const Spacer(),
                IconButton(
                  onPressed: () async {
                    Navigator.pop(context);
                  },
                  icon: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        width: 23,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.black.withValues(alpha: 0.4),
                            width: 2,
                          ),
                          shape: BoxShape.circle,
                        ),
                      ),
                      Icon(
                        Icons.close,
                        size: 18,
                        color: Colors.black.withValues(alpha: 0.4),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Expanded(
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.error_outline,
                        color: Theme.of(context).colorScheme.error,
                        size: 60,
                      ),
                      SmartGaps.gapH10,
                      Text(
                        tr('total_value_error'),
                        textAlign: TextAlign.center,
                        style: GoogleFonts.notoSans(
                          textStyle: TextStyle(
                            fontSize: 20,
                            height: 1.81,
                            color: Colors.black.withValues(alpha: 0.6),
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                      SmartGaps.gapH30,
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
