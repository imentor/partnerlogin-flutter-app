part of '../create_offer_page_wrapper.dart';

class JobIndustryOfferCard extends StatefulWidget {
  const JobIndustryOfferCard({
    super.key,
    required this.jobData,
    this.isSummary = false,
  });

  final JobIndustryModelCard jobData;
  final bool isSummary;
  @override
  State<JobIndustryOfferCard> createState() => _JobIndustryOfferCardState();
}

class _JobIndustryOfferCardState extends State<JobIndustryOfferCard> {
  bool _showDescription = false;
  final key = GlobalKey();
  final CurrencyTextInputFormatter _subValueFormatter =
      CurrencyTextInputFormatter(NumberFormat.currency(
    locale: 'da',
    decimalDigits: 2,
    symbol: '',
  ));

  @override
  Widget build(BuildContext context) {
    //

    final tenderVm = context.read<TenderFolderViewModel>();
    final createOfferVm = context.read<CreateOfferViewModel>();

    final locale = context.locale.languageCode;
    final productType = tenderVm.wizardTexts.firstWhere(
      (element) => element.producttypeid == widget.jobData.enterpriseId,
      orElse: () => AllWizardText.defaults(),
    );
    final jobType = tenderVm.fullIndustries.firstWhere(
      (element) => element.brancheId.toString() == widget.jobData.jobIndustryId,
      orElse: () => FullIndustry.defaults(),
    );

    final enterpriseNumber = (widget.jobData.enterpriseIndex + 1).toString();
    final jobNumber = (widget.jobData.jobIndustryIndex + 1).toString();

    final mapKey =
        '${widget.jobData.enterpriseId}_${widget.jobData.jobIndustryId}';

    return Card(
      elevation: 5,
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: Text(
                        '$enterpriseNumber. ${locale == 'da' ? productType.producttypeDa : productType.producttypeEn}',
                        maxLines: 5,
                        style: GoogleFonts.notoSans(
                          textStyle: const TextStyle(
                            fontSize: 17,
                            height: 1.375,
                            color: PartnerAppColors.darkBlue,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                    SmartGaps.gapH4,
                    Flexible(
                      child: Text(
                        '$enterpriseNumber.$jobNumber. ${locale == 'da' ? jobType.brancheDa : jobType.brancheEn}',
                        style: GoogleFonts.notoSans(
                          textStyle: const TextStyle(
                            fontSize: 14,
                            height: 1.42,
                            color: PartnerAppColors.accentBlue,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SmartGaps.gapW30,
                Flexible(
                  child: SizedBox(
                    height: 45,
                    child: widget.isSummary
                        ? Text(
                            '${createOfferVm.controllers['${widget.jobData.jobIndustryId}-${widget.jobData.enterpriseId}']!.text.trim()} kr.',
                            textAlign: TextAlign.right,
                            style: GoogleFonts.notoSans(
                              textStyle: const TextStyle(
                                fontSize: 19,
                                height: 1.37,
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          )
                        : SizedBox(
                            height: 50,
                            child: TextFormField(
                              // textDirection: TextDirection.rtl,
                              key: key,
                              controller: createOfferVm.controllers[
                                  '${widget.jobData.jobIndustryId}-${widget.jobData.enterpriseId}'],
                              onChanged: (val) {
                                createOfferVm.updateSubValueWithoutVat();
                              },
                              textAlign: TextAlign.end,
                              keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[
                                _subValueFormatter
                              ],
                              validator: (value) {
                                if (value!.isEmpty) {
                                  Scrollable.ensureVisible(
                                    key.currentContext!,
                                    alignment: 0.4,
                                  );
                                  return ' '; // Placeholder to preserve space
                                }
                                return null;
                              },
                              style: GoogleFonts.notoSans(
                                textStyle: const TextStyle(
                                  fontSize: 21,
                                  height: 1.37,
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              decoration: InputDecoration(
                                errorStyle: const TextStyle(
                                    height: 1, fontSize: 0), // Non-zero height
                                contentPadding: const EdgeInsets.symmetric(
                                    horizontal: 15, vertical: 10),
                                suffix: Text(
                                  'kr.',
                                  style: GoogleFonts.notoSans(
                                    textStyle: const TextStyle(
                                      fontSize: 21,
                                      height: 1.37,
                                      color: PartnerAppColors.darkBlue,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                                focusedErrorBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Theme.of(context).colorScheme.error),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                                errorBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color:
                                          Theme.of(context).colorScheme.error),
                                ),
                              ),
                            ),
                          ),
                  ),
                ),
              ],
            ),
            if (_showDescription)
              Padding(
                padding: const EdgeInsets.only(top: 30, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (!widget.isSummary) ...[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            onTap: () {
                              changeDrawerRoute(
                                  Routes.createOfferEditDescription,
                                  arguments: {
                                    'descriptions': widget.jobData.descriptions,
                                    'title':
                                        '$enterpriseNumber.$jobNumber. ${locale == 'da' ? jobType.brancheDa : jobType.brancheEn}',
                                    'enterpriseId': widget.jobData.enterpriseId,
                                    'jobIndustryId':
                                        widget.jobData.jobIndustryId,
                                  });
                            },
                            child: RichText(
                              text: TextSpan(
                                children: [
                                  const WidgetSpan(
                                    alignment: PlaceholderAlignment.middle,
                                    child: Icon(
                                      FeatherIcons.edit,
                                      size: 16,
                                      color: PartnerAppColors.blue,
                                    ),
                                  ),
                                  TextSpan(
                                    text: ' Rediger beskrivelse',
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleSmall!
                                        .copyWith(
                                          color: PartnerAppColors.blue,
                                        ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              if (createOfferVm.hasAcceptedAiPopup) {
                                changeDrawerRoute(
                                    Routes.createOfferAiCalculation,
                                    arguments: {
                                      'title':
                                          '$enterpriseNumber.$jobNumber. ${locale == 'da' ? jobType.brancheDa : jobType.brancheEn}',
                                      'enterpriseId':
                                          widget.jobData.enterpriseId,
                                      'jobIndustryId':
                                          widget.jobData.jobIndustryId,
                                      'descriptions':
                                          widget.jobData.descriptions,
                                    });
                              } else {
                                aiCalculationInformDialog(context: context)
                                    .then((value) {
                                  if ((value ?? false)) {
                                    changeDrawerRoute(
                                        Routes.createOfferAiCalculation,
                                        arguments: {
                                          'title':
                                              '$enterpriseNumber.$jobNumber. ${locale == 'da' ? jobType.brancheDa : jobType.brancheEn}',
                                          'enterpriseId':
                                              widget.jobData.enterpriseId,
                                          'jobIndustryId':
                                              widget.jobData.jobIndustryId,
                                          'descriptions':
                                              widget.jobData.descriptions,
                                        });
                                  }
                                });
                              }
                            },
                            child: RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: 'Brug AI-prisberegner ',
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleSmall!
                                        .copyWith(
                                          color: PartnerAppColors.blue,
                                        ),
                                  ),
                                  const WidgetSpan(
                                    alignment: PlaceholderAlignment.middle,
                                    child: Icon(
                                      FeatherIcons.info,
                                      size: 16,
                                      color: PartnerAppColors.blue,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                    // Padding(
                    //   padding: const EdgeInsets.only(top: 10, bottom: 20),
                    //   child: Row(
                    //     children: [
                    //       const Spacer(),
                    //       GestureDetector(
                    //         onTap: () async {
                    //           await showCustomDialog(
                    //             context: context,
                    //             continueFunction: () {
                    //               final createOfferVm =
                    //                   context.read<CreateOfferViewModel>();

                    //               final String key =
                    //                   '${widget.jobData.enterpriseId}_${widget.jobData.jobIndustryId}';

                    //               List<String> editable = <String>[
                    //                 ...createOfferVm.editableDescriptions[key]
                    //               ];

                    //               editable = <String>[
                    //                 ...createOfferVm.templateDescriptions[key]
                    //               ];

                    //               createOfferVm.editableDescriptions[key] =
                    //                   editable;

                    //               createOfferVm.editableDescriptions =
                    //                   createOfferVm.editableDescriptions;
                    //             },
                    //             contentText: tr('restore_description'),
                    //           );
                    //         },
                    //         child: Row(
                    //           children: [
                    //             const Icon(
                    //               Icons.rotate_right,
                    //               color: PartnerAppColors.accentBlue,
                    //               size: 30,
                    //             ),
                    //             SmartGaps.gapW5,
                    //             Text(
                    //               tr('restore_text'),
                    //               style: const TextStyle(
                    //                 color: PartnerAppColors.accentBlue,
                    //                 fontSize: 14,
                    //               ),
                    //             ),
                    //           ],
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),

                    ...widget.jobData.descriptions
                        .where((e) => e.isNotEmpty)
                        .map(
                      (desc) {
                        final descriptionNumber =
                            (widget.jobData.descriptions.indexOf(desc) + 1)
                                .toString();
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: Column(
                            children: [
                              if (!widget.isSummary)
                                if (!context
                                    .watch<CreateOfferViewModel>()
                                    .indexesToEdit
                                    .contains(int.parse(descriptionNumber) - 1))
                                  DescriptionBase(
                                    description: DescriptionData(
                                      industry: widget.jobData.enterpriseId,
                                      jobIndustry: widget.jobData.jobIndustryId,
                                      numbering:
                                          '$enterpriseNumber.$jobNumber.$descriptionNumber',
                                      description: desc,
                                      index: int.parse(descriptionNumber) - 1,
                                    ),
                                  )
                                else
                                  DescriptionEdit(
                                    description: DescriptionData(
                                      industry: widget.jobData.enterpriseId,
                                      jobIndustry: widget.jobData.jobIndustryId,
                                      numbering:
                                          '$enterpriseNumber.$jobNumber.$descriptionNumber',
                                      description: desc,
                                      index: int.parse(descriptionNumber) - 1,
                                    ),
                                  )
                              else
                                Padding(
                                  padding: const EdgeInsets.only(top: 13),
                                  child: Text(
                                    '$enterpriseNumber.$jobNumber.$descriptionNumber.$desc',
                                    style: GoogleFonts.notoSans(
                                      textStyle: const TextStyle(
                                        fontSize: 15,
                                        height: 1.81,
                                        color: Colors.black,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  ),
                                )
                            ],
                          ),
                        );
                      },
                    ),
                    if (createOfferVm.addDescription[mapKey] == true)
                      DescriptionEdit(
                        isAdd: true,
                        addNumbering:
                            '$enterpriseNumber.$jobNumber.${createOfferVm.editableDescriptions[mapKey].length + 1}',
                        mapKey: mapKey,
                        addIndex:
                            createOfferVm.editableDescriptions[mapKey].length,
                      )
                    else if (!widget.isSummary)
                      const SizedBox.shrink()
                    // Row(
                    //   children: [
                    //     const Spacer(),
                    //     GestureDetector(
                    //       onTap: () {
                    //         createOfferVm.addDescription[mapKey] = true;
                    //         createOfferVm.addDescription =
                    //             createOfferVm.addDescription;
                    //       },
                    //       child: Row(
                    //         children: [
                    //           const Icon(
                    //             Icons.add_circle_outline_outlined,
                    //             color: PartnerAppColors.accentBlue,
                    //             size: 20,
                    //           ),
                    //           SmartGaps.gapW5,
                    //           Text(
                    //             tr('add_description'),
                    //             style: GoogleFonts.notoSans(
                    //               textStyle: const TextStyle(
                    //                 fontSize: 13,
                    //                 height: 1.46,
                    //                 color: PartnerAppColors.accentBlue,
                    //                 fontWeight: FontWeight.w400,
                    //               ),
                    //             ),
                    //           )
                    //         ],
                    //       ),
                    //     ),
                    //   ],
                    // ),
                  ],
                ),
              ),
            SmartGaps.gapH67,
            Align(
              alignment: Alignment.center,
              child: GestureDetector(
                onTap: () {
                  createOfferVm.addDescription[mapKey] = false;
                  createOfferVm.addDescription = createOfferVm.addDescription;
                  setState(() {
                    _showDescription = !_showDescription;
                  });
                },
                child: Text(
                  _showDescription
                      ? tr('hide_description')
                      : tr('see_and_correct_description'),
                  style: GoogleFonts.notoSans(
                    textStyle: const TextStyle(
                      fontSize: 14,
                      height: 1.42,
                      color: PartnerAppColors.accentBlue,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
