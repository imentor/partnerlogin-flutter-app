part of '../create_offer_page_wrapper.dart';

class DateFieldSummaryTemplate extends StatelessWidget {
  const DateFieldSummaryTemplate({
    super.key,
    required this.label,
    required this.value,
  });

  final String label;
  final String value;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: GoogleFonts.notoSans(
            textStyle: const TextStyle(
              fontSize: 16,
              height: 1,
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        SmartGaps.gapH15,
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(
              'assets/images/calendar.svg',
              width: 27,
              colorFilter: const ColorFilter.mode(
                PartnerAppColors.accentBlue,
                BlendMode.srcIn,
              ),
            ),
            SmartGaps.gapW10,
            Text(
              value,
              style: GoogleFonts.notoSans(
                textStyle: const TextStyle(
                  fontSize: 16,
                  height: 1,
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}
