part of '../create_offer_page_wrapper.dart';

class DescriptionEdit extends StatefulWidget {
  const DescriptionEdit({
    super.key,
    this.description,
    this.isAdd = false,
    this.addNumbering,
    this.mapKey,
    this.addIndex,
  });

  final DescriptionData? description;
  final bool? isAdd;
  final String? addNumbering;
  final String? mapKey;
  final int? addIndex;

  @override
  State<DescriptionEdit> createState() => _DescriptionEditState();
}

class _DescriptionEditState extends State<DescriptionEdit> {
  late TextEditingController controller;
  final formKey = GlobalKey<FormState>();

  @override
  void initState() {
    controller = TextEditingController(
        text: widget.isAdd! ? '' : widget.description!.description);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: widget.isAdd!
                  ? () {
                      FocusScope.of(context).unfocus();

                      context
                          .read<CreateOfferViewModel>()
                          .addDescription[widget.mapKey!] = false;
                      context.read<CreateOfferViewModel>().addDescription =
                          context.read<CreateOfferViewModel>().addDescription;
                    }
                  : () {
                      //
                      FocusScope.of(context).unfocus();
                      final indexes =
                          context.read<CreateOfferViewModel>().indexesToEdit;

                      indexes.removeWhere(
                          (element) => element == widget.description!.index);

                      context.read<CreateOfferViewModel>().indexesToEdit =
                          indexes;

                      //
                    },
              child: const Icon(
                Icons.close,
                size: 19,
                color: PartnerAppColors.accentBlue,
              ),
            ),
            SmartGaps.gapW30,
            GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();

                if (formKey.currentState!.validate()) {
                  final createOfferVm = context.read<CreateOfferViewModel>();

                  if (!widget.isAdd!) {
                    final String key =
                        '${widget.description!.industry}_${widget.description!.jobIndustry}';
                    final editable = <String>[
                      ...createOfferVm.editableDescriptions[key]
                    ];
                    if (widget.description!.index <=
                        <String>[...createOfferVm.templateDescriptions[key]]
                                .length -
                            1) {
                      createOfferVm.updatedDescriptions[key] = {
                        ...createOfferVm.updatedDescriptions[key] ?? {},
                        "${widget.description!.index}": controller.text.trim()
                      };
                    }

                    editable[widget.description!.index] =
                        controller.text.trim();

                    createOfferVm.editableDescriptions[key] = editable;
                    createOfferVm.editableDescriptions =
                        createOfferVm.editableDescriptions;

                    final indexes =
                        context.read<CreateOfferViewModel>().indexesToEdit;

                    indexes.removeWhere(
                        (element) => element == widget.description!.index);

                    context.read<CreateOfferViewModel>().indexesToEdit =
                        indexes;
                  } else {
                    createOfferVm.addedDescriptions[widget.mapKey!] = {
                      ...createOfferVm.addedDescriptions[widget.mapKey!] ?? {},
                      "${widget.addIndex}": controller.text.trim()
                    };

                    final editable = <String>[
                      ...createOfferVm.editableDescriptions[widget.mapKey!]
                    ];

                    editable.add(controller.text.trim());

                    createOfferVm.addDescription[widget.mapKey!] = false;
                    createOfferVm.addDescription =
                        context.read<CreateOfferViewModel>().addDescription;

                    createOfferVm.editableDescriptions[widget.mapKey!] =
                        editable;
                    createOfferVm.editableDescriptions =
                        createOfferVm.editableDescriptions;
                  }
                }
              },
              child: const Icon(
                Icons.check,
                size: 19,
                color: PartnerAppColors.accentBlue,
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(top: 13),
          child: Form(
            key: formKey,
            child: TextFormField(
              controller: controller,
              minLines: 1,
              maxLines: 10,
              validator: (val) {
                if (val!.isEmpty) {
                  return tr('add_description_validator');
                }
                return null;
              },
              style: GoogleFonts.notoSans(
                textStyle: TextStyle(
                  fontSize: 15,
                  height: 1.81,
                  color: Colors.black.withValues(alpha: 0.6),
                  fontWeight: FontWeight.w400,
                ),
              ),
              decoration: InputDecoration(
                prefix: Text(
                  widget.isAdd!
                      ? '${widget.addNumbering}. '
                      : '${widget.description!.numbering}. ',
                  style: GoogleFonts.notoSans(
                    textStyle: TextStyle(
                      fontSize: 15,
                      height: 1.81,
                      color: Colors.black.withValues(alpha: 0.6),
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
                contentPadding: const EdgeInsets.all(20),
                border: const OutlineInputBorder(),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
