part of '../create_offer_page_wrapper.dart';

class CreateOfferWizardButton extends StatelessWidget {
  const CreateOfferWizardButton({super.key, this.formKey});

  final GlobalKey<FormState>? formKey;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        buttons(context),
      ],
    );
  }

  buttons(BuildContext context) {
    switch (context.watch<CreateOfferViewModel>().currentWizardStep) {
      case 0:
        return CustomDesignTheme.flatButtonStyle(
          onPressed: () async {
            final createOfferVm = context.read<CreateOfferViewModel>();
            if (!createOfferVm.busy) {
              if (createOfferVm
                      .formKeys[createOfferVm.currentWizardStep].currentState
                      ?.validate() ??
                  true) {
                FocusScope.of(context).unfocus();
                if (createOfferVm.totalValue < 1000) {
                  await showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (context) => const ValueError(),
                  );
                } else {
                  int currentStep = createOfferVm.currentWizardStep;
                  currentStep++;
                  createOfferVm.currentWizardStep = currentStep;
                }
              }
            }
          },
          backgroundColor: Theme.of(context).colorScheme.secondary,
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 50,
            child: Center(
              child: Text(
                tr('next'),
                style: GoogleFonts.notoSans(
                  textStyle: const TextStyle(
                    fontSize: 16,
                    height: 1,
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
          ),
        );
      default:
        return Row(
          children: [
            Expanded(
              child: TextButton(
                onPressed: () {
                  int currentStep =
                      context.read<CreateOfferViewModel>().currentWizardStep;
                  currentStep--;
                  context.read<CreateOfferViewModel>().currentWizardStep =
                      currentStep;
                },
                style: TextButton.styleFrom(
                  side: const BorderSide(color: PartnerAppColors.darkBlue),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  fixedSize: const Size(double.infinity, 50),
                  backgroundColor: Colors.white,
                ),
                child: Center(
                  child: Text(
                    tr('back'),
                    style: GoogleFonts.notoSans(
                      textStyle: const TextStyle(
                        fontSize: 16,
                        height: 1,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SmartGaps.gapW20,
            Expanded(
              child: ElevatedButton(
                onPressed: () async {
                  final createOfferVm = context.read<CreateOfferViewModel>();

                  if (createOfferVm.currentWizardStep == 3) {
                    if (createOfferVm.csUserId.isNotEmpty) {
                      showOkCancelAlertDialog(
                              context: context,
                              message: tr('send_offer_by_cs'),
                              okLabel: tr('yes'),
                              cancelLabel: tr('no'))
                          .then((value) async {
                        if (!context.mounted) return;

                        if (value == OkCancelResult.ok) {
                          await submitOffer(context: context);
                        }
                      });
                    } else {
                      await submitOffer(context: context);
                    }
                  } else {
                    if (createOfferVm.formKeys[createOfferVm.currentWizardStep]
                            .currentState
                            ?.validate() ??
                        true) {
                      FocusScope.of(context).unfocus();
                      if (createOfferVm.totalValue < 1000) {
                        await showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (context) => const ValueError(),
                        );
                      } else {
                        int currentStep = createOfferVm.currentWizardStep;
                        currentStep++;
                        createOfferVm.currentWizardStep = currentStep;
                      }
                    }
                  }
                },
                style: ElevatedButton.styleFrom(
                  elevation: 0,
                  backgroundColor: Theme.of(context).colorScheme.secondary,
                  fixedSize: const Size(double.infinity, 50),
                ),
                child: Center(
                  child: Text(
                    context.watch<CreateOfferViewModel>().currentWizardStep == 3
                        ? tr('done')
                        : tr('next'),
                    style: GoogleFonts.notoSans(
                      textStyle: const TextStyle(
                        fontSize: 16,
                        height: 1,
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
    }
  }
}
