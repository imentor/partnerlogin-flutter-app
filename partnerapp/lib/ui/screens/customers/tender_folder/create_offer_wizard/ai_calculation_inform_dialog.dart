import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';

Future<bool?> aiCalculationInformDialog({
  required BuildContext context,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child:
              const SingleChildScrollView(child: AiCalculationInformDialog()),
        ),
      );
    },
  );
}

class AiCalculationInformDialog extends StatefulWidget {
  const AiCalculationInformDialog({super.key});

  @override
  State<AiCalculationInformDialog> createState() =>
      _AiCalculationInformDialogState();
}

class _AiCalculationInformDialogState extends State<AiCalculationInformDialog> {
  bool isAccepted = false;

  @override
  Widget build(BuildContext context) {
    return Consumer<CreateOfferViewModel>(builder: (_, createOfferVm, __) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            tr('ai_price_info_description'),
            style: Theme.of(context).textTheme.titleMedium!.copyWith(
                color: PartnerAppColors.spanishGrey,
                fontWeight: FontWeight.normal),
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            children: [
              SizedBox(
                height: 24,
                width: 24,
                child: Checkbox(
                    value: isAccepted,
                    onChanged: (value) {
                      setState(() {
                        isAccepted = value ?? false;
                      });
                    }),
              ),
              const SizedBox(
                width: 5,
              ),
              Text(
                tr('accept'),
                style: Theme.of(context).textTheme.titleMedium,
              )
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          TextButton(
            style: TextButton.styleFrom(
                fixedSize: Size(MediaQuery.of(context).size.width, 45),
                backgroundColor: isAccepted
                    ? PartnerAppColors.malachite
                    : PartnerAppColors.spanishGrey),
            child: Text(
              tr('continue'),
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium!
                  .copyWith(color: Colors.white),
            ),
            onPressed: () {
              if (isAccepted) {
                createOfferVm.hasAcceptedAiPopup = true;
                Navigator.of(context).pop(true);
              }
            },
          ),
        ],
      );
    });
  }
}
