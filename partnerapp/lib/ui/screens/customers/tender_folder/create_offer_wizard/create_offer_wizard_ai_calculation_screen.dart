import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class CreateOfferWizardAiCalculationScreen extends StatefulWidget {
  const CreateOfferWizardAiCalculationScreen({
    super.key,
    required this.title,
    required this.enterpriseId,
    required this.jobIndustryId,
    required this.descriptions,
  });

  final String title;
  final String enterpriseId;
  final String jobIndustryId;
  final List<String> descriptions;

  @override
  State<CreateOfferWizardAiCalculationScreen> createState() =>
      _CreateOfferWizardAiCalculationScreenState();
}

class _CreateOfferWizardAiCalculationScreenState
    extends State<CreateOfferWizardAiCalculationScreen> {
  Map<String, dynamic>? pricesMap;

  final CurrencyTextInputFormatter _subValueFormatter =
      CurrencyTextInputFormatter(NumberFormat.currency(
    locale: 'da',
    decimalDigits: 2,
    symbol: '',
  ));

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final createOfferVm = context.read<CreateOfferViewModel>();

      if ((createOfferVm.activeJob?.projectAddressId ?? '').isEmpty ||
          (createOfferVm.activeJob?.projectAdgangsAddressId ?? '').isEmpty ||
          widget.jobIndustryId.isEmpty) {
        showOkAlertDialog(
                context: context, message: tr('error_getting_ai_calculation'))
            .whenComplete(() {
          backDrawerRoute();
        });
      } else {
        await tryCatchWrapper(
                context: context,
                function: createOfferVm.getAiCalculationPriceV3(
                    industry: widget.jobIndustryId,
                    productType: widget.enterpriseId))
            .then((value) {
          if (value != null) {
            pricesMap = value;
          }
        });
      }
    });
    super.initState();
  }

  Widget buttonNav(CreateOfferViewModel createOfferVm) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextButton(
            style: TextButton.styleFrom(
                fixedSize: Size(MediaQuery.of(context).size.width, 45),
                backgroundColor: PartnerAppColors.malachite),
            child: Text(
              tr('save_ai_pricing'),
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium!
                  .copyWith(color: Colors.white),
            ),
            onPressed: () {
              createOfferVm
                      .controllers['${widget.jobIndustryId}-${widget.enterpriseId}']
                      ?.text =
                  _subValueFormatter.formatDouble(
                      double.parse('${pricesMap?['price'] ?? 0}'));

              createOfferVm.updateSubValueWithoutVat();

              backDrawerRoute();
            },
          ),
          const SizedBox(
            height: 10,
          ),
          InkWell(
            onTap: () {
              backDrawerRoute();
            },
            child: Text(
              tr('insert_own_prices'),
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                  color: PartnerAppColors.blue, fontWeight: FontWeight.normal),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<CreateOfferViewModel>(builder: (_, createOfferVm, __) {
      return Scaffold(
        appBar: DrawerAppBar(
          overrideBackFunction: () {
            if (!createOfferVm.busy) {
              backDrawerRoute();
            }
          },
        ),
        bottomNavigationBar: buttonNav(createOfferVm),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: Skeletonizer(
            enabled: createOfferVm.busy,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  tr('ai_price_calculator'),
                  style: Theme.of(context)
                      .textTheme
                      .titleMedium!
                      .copyWith(fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  tr('ai_price_description'),
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                const SizedBox(
                  height: 10,
                ),
                const Divider(
                  color: PartnerAppColors.darkBlue,
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  tr('your_ai_estimate'),
                  style: Theme.of(context)
                      .textTheme
                      .titleMedium!
                      .copyWith(fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Text(
                        widget.title,
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(fontWeight: FontWeight.normal),
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Text(
                      Formatter.curencyFormat(amount: pricesMap?['price'] ?? 0),
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium!
                          .copyWith(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                ...widget.descriptions.map((value) {
                  final descriptionIndex = widget.descriptions.indexOf(value);

                  return Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          '${widget.title.split(' ').first}${descriptionIndex + 1}',
                          style: Theme.of(context).textTheme.titleSmall,
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          flex: 2,
                          child: Text(
                            value,
                            style: Theme.of(context).textTheme.titleSmall,
                          ),
                        ),
                      ],
                    ),
                  );
                })
              ],
            ),
          ),
        ),
      );
    });
  }
}
