library create_offer;

import 'package:Haandvaerker.dk/model/create_offer/list_price_partner_ai_v3_model.dart';
import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/steps_widget.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/components/custom_expansion_tile.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/create_offer_wizard/add%20recommendation/add_recommendation.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/create_offer_wizard/ai_calculation_inform_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/create_offer_wizard/sign_submit_offer/create_offer.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/packages/packages_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/job_types_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart' hide TextDirection;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

part 'components/calculation_template.dart';
part 'components/create_offer_wizard_button.dart';
part 'components/date_field_summary_template.dart';
part 'components/description_base.dart';
part 'components/description_edit.dart';
part 'components/job_industry_offer_card.dart';
//COMPONENTS
part 'components/offer_list_template.dart';
part 'components/request_offer_template.dart';
part 'components/see_other_companies.dart';
part 'components/value_error.dart';
part 'steps/create_offer_wizard_step_1.dart';
part 'steps/create_offer_wizard_step_2.dart';
part 'steps/create_offer_wizard_step_3.dart';
part 'steps/create_offer_wizard_step_4.dart';
//STEPS
part 'steps/create_offer_wizard_step_timeline.dart';
part 'steps/create_offer_wizard_step_wrapper.dart';

class CreateOfferPageWrapper extends StatefulWidget {
  const CreateOfferPageWrapper({
    super.key,
    required this.job,
  });
  final PartnerJobModel job;
  @override
  State<CreateOfferPageWrapper> createState() => _CreateOfferPageWrapperState();
}

class _CreateOfferPageWrapperState extends State<CreateOfferPageWrapper> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      //

      final createOfferVm = context.read<CreateOfferViewModel>();
      final packageVm = context.read<PackagesViewModel>();
      final jobTypesVm = context.read<JobTypesViewModel>();

      createOfferVm.setValuesToDefault();

      int projectParentId = widget.job.projectParentId != null
          ? int.parse(widget.job.projectParentId)
          : widget.job.projectId;

      createOfferVm.activeJob = widget.job;

      if (createOfferVm.toUpdateOffer) {
        //

        await Future.wait([
          createOfferVm.getListPriceMinbolig(
            projectId: projectParentId,
            contactId: widget.job.customerId!,
          ),
          packageVm.getPackages(),
        ]);

        //
      }
      //
      else {
        //

        await Future.wait([
          createOfferVm.getListPricePartnerAiV3(
            parentProjectId: projectParentId,
            contactId: widget.job.customerId!,
          ),
          jobTypesVm.getSupplierProfileById(),
          packageVm.getPackages(),
        ]);

        //
      }

      // if (!packageVm.availableUserFeatures
      //         .any((element) => element.shortCode == "OfferCreator") &&
      //     mounted) {
      //   await showUnAvailableDialog(context, packageVm.offerCreator);
      // }

      //
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardVisibilityBuilder(builder: (context, isKeyboardVisible) {
      return Scaffold(
        appBar: const DrawerAppBar(),
        bottomNavigationBar: Container(
          margin: EdgeInsets.only(
              bottom: MediaQuery.of(context).viewInsets.bottom + 20),
          padding:
              const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
          color: Colors.white,
          child: const CreateOfferWizardButton(),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //

              //STEP TIMELINE
              const CreateOfferWizardStepTimeline(),

              SmartGaps.gapH30,

              if (context.watch<CreateOfferViewModel>().busy)
                const SizedBox(
                  height: 300,
                  child: Center(
                    child: ConnectivityLoader(),
                  ),
                )
              else
                const CreateOfferWizardStepWrapper(),

              //
            ],
          ),
        ),
      );
    });
  }
}

// Model for offer Job Industry Card
class JobIndustryModelCard {
  final String enterpriseId;
  final int enterpriseIndex;
  final String jobIndustryId;
  final int jobIndustryIndex;
  final List<String> descriptions;

  JobIndustryModelCard({
    required this.enterpriseId,
    required this.enterpriseIndex,
    required this.jobIndustryId,
    required this.jobIndustryIndex,
    required this.descriptions,
  });
}

//DESCRIPTION DATA MODEL
class DescriptionData {
  final String numbering;
  final String description;
  final String industry;
  final String jobIndustry;
  final int index;

  DescriptionData({
    required this.numbering,
    required this.description,
    required this.industry,
    required this.jobIndustry,
    required this.index,
  });
}
