import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

class CreateOfferWizardEditDescriptionsScreen extends StatefulWidget {
  const CreateOfferWizardEditDescriptionsScreen({
    super.key,
    required this.title,
    required this.descriptions,
    required this.enterpriseId,
    required this.jobIndustryId,
  });

  final List<String> descriptions;
  final String title;
  final String enterpriseId;
  final String jobIndustryId;

  @override
  State<CreateOfferWizardEditDescriptionsScreen> createState() =>
      _CreateOfferWizardEditDescriptionsScreenState();
}

class _CreateOfferWizardEditDescriptionsScreenState
    extends State<CreateOfferWizardEditDescriptionsScreen> {
  static final formKey = GlobalKey<FormBuilderState>();
  List<Widget> descriptions = [];

  @override
  void initState() {
    for (var i = 0; i < widget.descriptions.length; i++) {
      descriptions.add(EditDescription(
        key: ValueKey(i),
        name: 'description_$i',
        initialValue: widget.descriptions[i],
        onDelete: () {
          setState(() {
            descriptions.removeWhere((e) => e.key == ValueKey(i));
          });
        },
      ));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<CreateOfferViewModel>(builder: (_, createOfferVm, __) {
      return Scaffold(
        appBar: const DrawerAppBar(),
        bottomNavigationBar: buttonNav(createOfferVm),
        body: FormBuilder(
          clearValueOnUnregister: true,
          key: formKey,
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  tr('adjust_description'),
                  style: Theme.of(context)
                      .textTheme
                      .titleMedium!
                      .copyWith(fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  tr('adjust_description_text'),
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                const SizedBox(
                  height: 10,
                ),
                const Divider(
                  color: PartnerAppColors.darkBlue,
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Text(
                        widget.title,
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(fontWeight: FontWeight.bold),
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    InkWell(
                      onTap: () {
                        List<Widget> tempWidget = [];

                        for (var i = 0;
                            i <
                                createOfferVm.templateDescriptions[
                                    '${widget.enterpriseId}_${widget.jobIndustryId}'];
                            i++) {
                          tempWidget.add(EditDescription(
                            key: ValueKey(i),
                            name: 'description_$i',
                            initialValue: createOfferVm.templateDescriptions[
                                '${widget.enterpriseId}_${widget.jobIndustryId}'][i],
                            onDelete: () {
                              setState(() {
                                descriptions
                                    .removeWhere((e) => e.key == ValueKey(i));
                              });
                            },
                          ));
                        }

                        setState(() {
                          descriptions = [...tempWidget];
                        });
                      },
                      child: RichText(
                          text: TextSpan(children: [
                        const WidgetSpan(
                            child: Icon(
                              Icons.rotate_right,
                              color: PartnerAppColors.blue,
                            ),
                            alignment: PlaceholderAlignment.middle),
                        TextSpan(
                            text: tr('restore_text'),
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium!
                                .copyWith(color: PartnerAppColors.blue))
                      ])),
                    )
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                ...descriptions,
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          descriptions.add(EditDescription(
                            key: ValueKey(descriptions.length),
                            name: 'description_${descriptions.length}',
                            initialValue: '',
                            onDelete: () {
                              setState(() {
                                descriptions.removeWhere((e) =>
                                    e.key == ValueKey(descriptions.length));
                              });
                            },
                          ));
                        });
                      },
                      child: RichText(
                        text: TextSpan(
                          children: [
                            const WidgetSpan(
                                child: Icon(
                                  Icons.add,
                                  size: 14,
                                  color: PartnerAppColors.blue,
                                ),
                                alignment: PlaceholderAlignment.middle),
                            TextSpan(
                                text: tr('add_description'),
                                style: Theme.of(context)
                                    .textTheme
                                    .titleSmall!
                                    .copyWith(
                                      color: PartnerAppColors.blue,
                                    ))
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    const Opacity(
                      opacity: 0,
                      child: Icon(
                        FeatherIcons.trash2,
                        color: PartnerAppColors.blue,
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      );
    });
  }

  Widget buttonNav(CreateOfferViewModel createOfferVm) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextButton(
            style: TextButton.styleFrom(
                fixedSize: Size(MediaQuery.of(context).size.width, 45),
                backgroundColor: PartnerAppColors.malachite),
            child: Text(
              tr('change_text'),
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium!
                  .copyWith(color: Colors.white),
            ),
            onPressed: () {
              List<String> newDescriptions = [];

              if (formKey.currentState!.validate()) {
                for (var element in formKey.currentState!.fields.entries) {
                  newDescriptions.add(element.value.value);
                }
              }

              createOfferVm.editableDescriptions[
                      '${widget.enterpriseId}_${widget.jobIndustryId}'] =
                  newDescriptions;

              createOfferVm.editableDescriptions =
                  createOfferVm.editableDescriptions;

              backDrawerRoute();
            },
          ),
          const SizedBox(
            height: 10,
          ),
          InkWell(
            onTap: () {
              backDrawerRoute();
            },
            child: Text(
              tr('close_without_saving'),
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                  color: PartnerAppColors.blue, fontWeight: FontWeight.normal),
            ),
          )
        ],
      ),
    );
  }
}

class EditDescription extends StatelessWidget {
  const EditDescription(
      {super.key, required this.name, this.onDelete, this.initialValue});

  final VoidCallback? onDelete;
  final String name;
  final String? initialValue;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: CustomTextFieldFormBuilder(
            name: name,
            initialValue: initialValue,
            minLines: 3,
            validator:
                FormBuilderValidators.required(errorText: tr('required')),
            onChanged: (value) {
              // descriptions[index] = value ?? '';
            },
          ),
        ),
        const SizedBox(
          width: 5,
        ),
        InkWell(
          onTap: onDelete,
          child: const Icon(
            FeatherIcons.trash2,
            color: PartnerAppColors.blue,
          ),
        )
      ],
    );
  }
}
