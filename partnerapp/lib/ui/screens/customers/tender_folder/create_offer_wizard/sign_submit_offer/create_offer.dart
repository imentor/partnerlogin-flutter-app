library submit_offer;

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/app_web_view.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/debouncer.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_signaturepad/signaturepad.dart';
import 'package:url_launcher/url_launcher.dart';

part 'components/create_offer_dialog_close_button.dart';
part 'components/create_offer_dialog_template.dart';
part 'create_offer_loading_dialog.dart';
part 'create_offer_signature_dialog.dart';
part 'create_offer_success_failed_dialog.dart';
part 'create_offer_terms_dialog.dart';

Future<void> submitOffer({
  required BuildContext context,
}) async {
  return await showDialog(
    context: context,
    barrierDismissible: false,
    builder: (dialogContext) => CreateOffer(dialogContext: dialogContext),
  );
}

class CreateOffer extends StatelessWidget {
  const CreateOffer({super.key, required this.dialogContext});

  final BuildContext dialogContext;

  @override
  Widget build(BuildContext context) {
    return Consumer<CreateOfferViewModel>(
      builder: (context, createOfferVm, _) {
        if (createOfferVm.uploadUpdateOfferAndList) {
          return const CreateOfferLoadingDialog();
        } else {
          if (createOfferVm.createOfferRequest == RequestResponse.idle) {
            return CreateOfferSignatureDialog(dialogContext: dialogContext);
          } else {
            return const CreateOfferSuccessFailedDialog();
          }
        }
      },
    );
  }
}
