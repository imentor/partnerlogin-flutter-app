part of 'create_offer.dart';

Future<void> termsDialog({
  required BuildContext context,
  String? link,
  String? termsHTML,
}) async {
  return await showDialog(
    barrierDismissible: true,
    context: context,
    builder: (context) => CreateOfferTerms(
      link: link,
      termsHTML: termsHTML,
    ),
  );
}

class CreateOfferTerms extends StatelessWidget {
  const CreateOfferTerms({
    super.key,
    this.link,
    this.termsHTML,
  });

  final String? link;
  final String? termsHTML;

  @override
  Widget build(BuildContext context) {
    return CreateOfferDialogTemplate(
      title:
          CreateOfferDialogCloseButton(onPressed: () => Navigator.pop(context)),
      contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      content: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            children: [
              ///

              if (link != null)
                AppWebView(
                  url: Uri.parse(link!),
                  isChild: true,
                )
              else
                Html(
                  data: termsHTML!,
                  onLinkTap: (url, attributes, element) {
                    launchUrl(Uri.parse(url!));
                  },
                )

              ///
            ],
          ),
        ),
      ),
    );
  }
}
