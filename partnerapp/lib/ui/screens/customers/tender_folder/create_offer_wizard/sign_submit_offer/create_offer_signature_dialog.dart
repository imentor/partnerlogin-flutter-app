part of 'create_offer.dart';

class CreateOfferSignatureDialog extends StatefulWidget {
  const CreateOfferSignatureDialog({super.key, required this.dialogContext});

  final BuildContext dialogContext;

  @override
  State<CreateOfferSignatureDialog> createState() =>
      _CreateOfferSignatureDialogState();
}

class _CreateOfferSignatureDialogState
    extends State<CreateOfferSignatureDialog> {
  final Debouncer debouncer = Debouncer(milliseconds: 500);

  @override
  Widget build(BuildContext context) {
    final signaturePadKey =
        context.select((CreateOfferViewModel value) => value.signaturePadKey)!;
    final signatureValidate =
        context.select((CreateOfferViewModel value) => value.signatureValidate);
    final confirmConditions =
        context.select((CreateOfferViewModel val) => val.confirmConditions);

    return Consumer<CreateOfferViewModel>(builder: (_, createOfferVm, __) {
      return CreateOfferDialogTemplate(
        title: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CreateOfferDialogCloseButton(
                  onPressed: () => Navigator.pop(context)),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      tr('sign_here'),
                      style:
                          Theme.of(context).textTheme.headlineLarge!.copyWith(
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                              ),
                    ),
                    InkWell(
                      onTap: () {
                        signaturePadKey.currentState!.clear();
                        createOfferVm.isSigned = false;
                      },
                      child: Row(children: [
                        const Icon(
                          FeatherIcons.trash2,
                          color: PartnerAppColors.red,
                          size: 16,
                        ),
                        SmartGaps.gapW10,
                        Text(
                          tr('clear_signature'),
                          style: Theme.of(context)
                              .textTheme
                              .displayLarge!
                              .copyWith(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: PartnerAppColors.red,
                              ),
                        )
                      ]),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        content: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.only(bottom: 10, top: 10),
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 250,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(
                      color: PartnerAppColors.black.withValues(alpha: 0.5),
                    ),
                  ),
                  child: Center(
                    child: SfSignaturePad(
                      key: signaturePadKey,
                      minimumStrokeWidth: 1,
                      maximumStrokeWidth: 3,
                      onDrawEnd: () {
                        createOfferVm.isSigned = true;
                        createOfferVm.signatureValidate = false;
                      },
                    ),
                  ),
                ),
                if (signatureValidate)
                  Padding(
                    padding: const EdgeInsets.only(top: 5),
                    child: Text(
                      tr('signature_validator'),
                      style:
                          Theme.of(context).textTheme.headlineLarge!.copyWith(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Theme.of(context).colorScheme.error,
                              ),
                    ),
                  ),
                SmartGaps.gapH20,
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Transform.scale(
                          scale: 1.2,
                          child: SizedBox(
                            width: 24,
                            height: 24,
                            child: Checkbox(
                              onChanged: (val) => createOfferVm
                                  .confirmConditions = (val ?? false),
                              value: confirmConditions,
                              activeColor: PartnerAppColors.darkBlue,
                            ),
                          ),
                        ),
                        SmartGaps.gapW10,
                        GestureDetector(
                          onTap: () => termsDialog(
                            context: context,
                            link: null,
                            termsHTML:
                                context.read<SharedDataViewmodel>().termsHtml,
                          ),
                          child: Text(
                            context.read<SharedDataViewmodel>().termsLabel,
                            style: Theme.of(context)
                                .textTheme
                                .displayLarge!
                                .copyWith(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: PartnerAppColors.blue,
                                    decoration: TextDecoration.underline),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SmartGaps.gapH30,
                CustomDesignTheme.flatButtonStyle(
                  height: 50,
                  onPressed: (confirmConditions && createOfferVm.isSigned)
                      ? () => onSendOffer(createOfferVm)
                      : null,
                  disabledBackgroundColor: Theme.of(context)
                      .colorScheme
                      .onTertiaryContainer
                      .withValues(alpha: 0.2),
                  backgroundColor: Theme.of(context).colorScheme.secondary,
                  child: Center(
                    child: Text(
                      tr('done_send_offer'),
                      style:
                          Theme.of(context).textTheme.headlineLarge!.copyWith(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: Colors.white,
                              ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }

  Future<void> onSendOffer(CreateOfferViewModel createOfferVm) async {
    final newsfeedVm = context.read<NewsFeedViewModel>();
    final clientVm = context.read<ClientsViewModel>();
    final tenderFolderVm = context.read<TenderFolderViewModel>();

    if (createOfferVm.isSigned) {
      debouncer.run(() async {
        createOfferVm.uploadUpdateOfferAndList = true;
        await tryCatchWrapper(
                context: context, function: createOfferVm.createOffer())
            .whenComplete(() async {
          if (createOfferVm.isOfferUploaded) {
            await Future.wait([
              newsfeedVm.getLatestJobFeed(),
              clientVm.getClients(
                  tenderVm: tenderFolderVm, createOfferVm: createOfferVm),
            ]).whenComplete(() async {
              if (!widget.dialogContext.mounted) return;

              Navigator.pop(widget.dialogContext);
              await successDialog(
                      context:
                          myGlobals.innerScreenScaffoldKey!.currentContext!,
                      successMessage: 'successfully_created_offer')
                  .whenComplete(() async {
                backDrawerRoute();
              });
            });
          } else if (!createOfferVm.isOfferUploaded &&
              createOfferVm.toUpdateOffer &&
              (createOfferVm.offerVersionList?.offerSeen ?? true) == true) {
            if (!widget.dialogContext.mounted) return;

            Navigator.pop(widget.dialogContext);
            await failedDialog(
                    context: myGlobals.innerScreenScaffoldKey!.currentContext!,
                    failedMessage: 'failed_v2')
                .whenComplete(() async {
              backDrawerRoute();
              modalManager.showLoadingModal();
              await Future.wait([
                newsfeedVm.getLatestJobFeed(),
                clientVm.getClients(
                    tenderVm: tenderFolderVm, createOfferVm: createOfferVm),
              ]);
              modalManager.hideLoadingModal();
            });
          }
        });
      });
    }
  }
}
