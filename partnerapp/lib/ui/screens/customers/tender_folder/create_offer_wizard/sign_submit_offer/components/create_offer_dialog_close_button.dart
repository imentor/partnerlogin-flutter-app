part of '../create_offer.dart';

class CreateOfferDialogCloseButton extends StatelessWidget {
  const CreateOfferDialogCloseButton({super.key, required this.onPressed});

  final Function()? onPressed;
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: IconButton(
        onPressed: onPressed,
        splashRadius: 20,
        icon: Icon(
          Icons.close,
          size: 20,
          color: Colors.black.withValues(alpha: 0.4),
        ),
      ),
    );
  }
}
