part of 'create_offer.dart';

class CreateOfferLoadingDialog extends StatelessWidget {
  const CreateOfferLoadingDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return CreateOfferDialogTemplate(
      title: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 400,
        child: const Center(
          child: ConnectivityLoader(),
        ),
      ),
    );
  }
}
