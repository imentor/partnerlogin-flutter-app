part of '../create_offer.dart';

class CreateOfferDialogTemplate extends StatelessWidget {
  const CreateOfferDialogTemplate({
    super.key,
    this.title,
    this.content,
    this.contentPadding,
  });

  final Widget? title;
  final Widget? content;
  final EdgeInsets? contentPadding;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      backgroundColor: Colors.white,
      titlePadding: EdgeInsets.zero,
      contentPadding: contentPadding ??
          const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      insetPadding: const EdgeInsets.all(20),
      title: title,
      content: content,
    );
  }
}
