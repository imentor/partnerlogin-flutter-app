part of 'create_offer.dart';

class CreateOfferSuccessFailedDialog extends StatelessWidget {
  const CreateOfferSuccessFailedDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<CreateOfferViewModel>(
      builder: (context, createOfferVm, _) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
          backgroundColor: Colors.white,
          titlePadding: EdgeInsets.zero,
          title: SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 400,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    const Spacer(),
                    IconButton(
                      onPressed: () async {
                        Navigator.pop(context);
                        context.read<CreateOfferViewModel>().popMyCustomer =
                            true;

                        changeDrawerReplaceRoute(Routes.yourClient);
                      },
                      icon: Stack(
                        alignment: Alignment.center,
                        children: [
                          Container(
                            width: 23,
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black.withValues(alpha: 0.4),
                                width: 2,
                              ),
                              shape: BoxShape.circle,
                            ),
                          ),
                          Icon(
                            Icons.close,
                            size: 18,
                            color: Colors.black.withValues(alpha: 0.4),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SmartGaps.gapH80,
                        Text(
                          createOfferVm.createOfferRequest ==
                                  RequestResponse.success
                              ? tr('offer_done')
                              : tr('error_occurred'),
                          style: Theme.of(context)
                              .textTheme
                              .headlineLarge!
                              .copyWith(
                                fontSize: 30,
                                fontWeight: FontWeight.w700,
                              ),
                        ),
                        SmartGaps.gapH30,
                        if (createOfferVm.createOfferRequest ==
                            RequestResponse.failed)
                          Icon(
                            Icons.error,
                            color: Theme.of(context).colorScheme.error,
                            size: 150,
                          )
                        else
                          SvgPicture.asset(
                            'assets/images/sadan.svg',
                            width: 150,
                          ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
