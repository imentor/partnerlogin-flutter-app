import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/model/file/file_data_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/image_viewer.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/tender_folder.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/upload_files.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:path/path.dart' as path;
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class ViewFolder extends StatefulWidget {
  const ViewFolder({
    super.key,
    required this.parentFile,
    this.projectParentId,
    this.files,
    this.jobProjectName,
  });
  final FileData parentFile;
  final int? projectParentId;
  final List<dynamic>? files;
  final String? jobProjectName;

  @override
  State<ViewFolder> createState() => _ViewFolderState();
}

class _ViewFolderState extends State<ViewFolder> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final tenderVm = context.read<TenderFolderViewModel>();
      if (widget.parentFile.name == 'Public') {
        await tenderVm.getPublicFiles(projectParentId: widget.projectParentId!);
      } else if (widget.parentFile.name == 'Archive') {
        await tenderVm.getArchivedFiles(folderId: widget.parentFile.id!);
      } else {
        tenderVm.updatedTenderFilesBreadcrumbs(
            selectedFile: widget.parentFile, toAdd: true);
        await tenderVm.getTenderFiles(folderId: widget.parentFile.id!);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      bottomNavigationBar: widget.parentFile.name == 'Invoice'
          ? Container(
              padding: const EdgeInsets.only(
                  left: 20, right: 20, bottom: 20, top: 10),
              color: Colors.white,
              child: TextButton(
                  style: TextButton.styleFrom(
                      fixedSize: Size(MediaQuery.of(context).size.width, 45),
                      backgroundColor: PartnerAppColors.darkBlue,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      )),
                  onPressed: () {
                    uploadFileDialog(
                        context: context,
                        parentFile: widget.parentFile,
                        projectId: widget.projectParentId!);
                  },
                  child: Text(
                    tr('upload'),
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium!
                        .copyWith(color: Colors.white),
                  )),
            )
          : null,
      body: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                context.locale.languageCode == 'da'
                    ? (widget.parentFile.nameDa ?? '')
                    : (widget.parentFile.name ?? ''),
                style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                    ),
              ),
              SmartGaps.gapH30,
              fileList(),
            ],
          )),
    );
  }

  Widget fileList() {
    final userVm = context.read<UserViewModel>();

    return Consumer<TenderFolderViewModel>(builder: (context, tenderVm, child) {
      List<FileData> tempFileList = [...tenderVm.tenderFiles];

      if ((widget.parentFile.name ?? '').toLowerCase() == 'offers') {
        tempFileList = [
          ...tempFileList
              .where((value) => value.partnerId == userVm.userModel?.user?.id)
        ];
      }

      return Skeletonizer(
        enabled: tenderVm.busy,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (widget.parentFile.name == 'Public') ...[
              if (tenderVm.publicFiles.isNotEmpty)
                GridView.count(
                  childAspectRatio: 2.0,
                  crossAxisCount: 2,
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 10,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  children: tenderVm.publicFiles
                      .map(
                        (publicFile) => GestureDetector(
                          onTap: () async {
                            if ((publicFile.link ?? '').isNotEmpty) {
                              switch (path.extension(publicFile.link!)) {
                                case '.pdf':
                                  if (publicFile.link != null) {
                                    pdfViewerV2Dialog(
                                        context: context,
                                        pdfUrl: publicFile.link!);
                                  }
                                  break;
                                case '.jpg':
                                case '.png':
                                case '.jpeg':
                                case '.heif':
                                case '.hevc':
                                  imageViewerDialog(
                                      context: context,
                                      fileUrl: publicFile.link!);
                                  break;
                                default:
                                  if ((publicFile.link ?? '').isNotEmpty) {
                                    changeDrawerRoute(Routes.inAppWebView,
                                        arguments: Uri.parse(publicFile.link!));
                                  }
                              }
                            }
                          },
                          child: PublicFileCard(
                            publicFile: publicFile,
                            index: tenderVm.publicFiles.indexOf(publicFile),
                          ),
                        ),
                      )
                      .toList(),
                )
              else
                Text(
                  tr('empty'),
                  style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                        fontSize: 20,
                        fontWeight: FontWeight.normal,
                      ),
                )
            ] else if (widget.parentFile.name == 'Archive') ...[
              if (tenderVm.archiveFiles.isNotEmpty)
                GridView.count(
                  childAspectRatio: 2.0,
                  crossAxisCount: 2,
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 10,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  children: tenderVm.archiveFiles
                      .map(
                        (archivedFile) => GestureDetector(
                          onTap: () async {
                            switch (path.extension(archivedFile.name!)) {
                              case '.pdf':
                                if (archivedFile.downloadUrl != null) {
                                  pdfViewerV2Dialog(
                                      context: context,
                                      pdfUrl: archivedFile.downloadUrl!);
                                }
                                break;
                              case '.jpg':
                              case '.png':
                              case '.jpeg':
                              case '.heif':
                              case '.hevc':
                                imageViewerDialog(
                                    context: context,
                                    fileUrl: archivedFile.downloadUrl!);
                                break;
                              default:
                                if (archivedFile.downloadUrl != null) {
                                  changeDrawerRoute(Routes.inAppWebView,
                                      arguments:
                                          Uri.parse(archivedFile.downloadUrl!));
                                }
                            }
                          },
                          child: FilerCard(
                            file: archivedFile,
                          ),
                        ),
                      )
                      .toList(),
                )
              else
                Text(
                  tr('empty'),
                  style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                        fontSize: 20,
                        fontWeight: FontWeight.normal,
                      ),
                )
            ] else ...[
              if (tempFileList.isNotEmpty) ...[
                ...tempFileList.map(
                  (file) => GestureDetector(
                    onTap: () async {
                      switch (path.extension(file.name!)) {
                        case '.pdf':
                          if (file.downloadUrl != null) {
                            pdfViewerV2Dialog(
                                context: context, pdfUrl: file.downloadUrl!);
                          }
                          break;
                        case '.jpg':
                        case '.png':
                        case '.jpeg':
                        case '.heif':
                        case '.hevc':
                          imageViewerDialog(
                              context: context, fileUrl: file.downloadUrl!);
                          break;
                        default:
                          if (file.downloadUrl != null) {
                            changeDrawerRoute(Routes.inAppWebView,
                                arguments: Uri.parse(file.downloadUrl!));
                          }
                      }
                    },
                    child: FilerCard(file: file),
                  ),
                ),
              ] else
                Text(
                  tr('empty'),
                  style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                        fontSize: 20,
                        fontWeight: FontWeight.normal,
                      ),
                ),
            ]
          ],
        ),
      );
    });
  }
}

class TenderFileCard extends StatelessWidget {
  const TenderFileCard({
    super.key,
    required this.tenderFile,
    required this.jobProjectName,
  });

  final TenderFiles tenderFile;
  final String? jobProjectName;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      margin: const EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
            color:
                Theme.of(context).colorScheme.primary.withValues(alpha: 0.25)),
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withValues(alpha: .1),
            blurRadius: 1.0,
            spreadRadius: -4,
            offset: const Offset(0, 5),
          )
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (tenderFile.url != null && tenderFile.url!.contains('.pdf'))
            SvgPicture.asset(
              'assets/images/pdf-file.svg',
              width: 25,
              colorFilter: ColorFilter.mode(
                Theme.of(context).colorScheme.primary,
                BlendMode.srcIn,
              ),
            )
          else
            Icon(
              Icons.link,
              size: 25,
              color: Theme.of(context).colorScheme.primary,
            ),
          SmartGaps.gapW20,
          Expanded(
            child: Text(
              '$jobProjectName-${tenderFile.projectId}-${tenderFile.type}',
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                    fontSize: 20,
                    fontWeight: FontWeight.w400,
                  ),
            ),
          ),
        ],
      ),
    );
  }
}

class PublicFileCard extends StatelessWidget {
  const PublicFileCard({
    super.key,
    required this.publicFile,
    required this.index,
  });

  final PublicFileData publicFile;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
            color:
                Theme.of(context).colorScheme.primary.withValues(alpha: 0.25)),
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withValues(alpha: .1),
            blurRadius: 1.0,
            spreadRadius: -4,
            offset: const Offset(0, 5),
          )
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (publicFile.link != null && publicFile.link!.contains('.pdf'))
            SvgPicture.asset(
              'assets/images/pdf-file.svg',
              width: 25,
              colorFilter: ColorFilter.mode(
                Theme.of(context).colorScheme.primary,
                BlendMode.srcIn,
              ),
            )
          else
            Icon(
              Icons.link,
              size: 25,
              color: Theme.of(context).colorScheme.primary,
            ),
          SmartGaps.gapW20,
          Expanded(
            child: Tooltip(
              message: publicFile.year == null
                  ? tr('job_description_conditions')
                  : "${publicFile.year}_${publicFile.addressType}",
              child: Text(
                publicFile.year == null
                    ? tr('job_description_conditions')
                    : "${publicFile.year}_${publicFile.addressType}",
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                    ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
