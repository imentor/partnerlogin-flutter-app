import 'package:Haandvaerker.dk/model/file/file_data_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';

Future<void> uploadFileDialog(
    {required BuildContext context,
    required FileData parentFile,
    required int projectId}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: UploadFile(
              parentFile: parentFile,
              projectId: projectId,
            ))),
      );
    },
  );
}

class UploadFile extends StatefulWidget {
  const UploadFile(
      {super.key, required this.parentFile, required this.projectId});

  final FileData parentFile;
  final int projectId;

  @override
  State<UploadFile> createState() => _UploadFileState();
}

class _UploadFileState extends State<UploadFile> {
  List<PlatformFile> files = [];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: () async {
            var fileResultList = await FilePicker.platform.pickFiles(
              type: FileType.any,
              allowMultiple: true,
            );

            if (fileResultList != null) {
              setState(() {
                files.addAll(fileResultList.files);
              });
            }
          },
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
                border: Border.all(
                  color: PartnerAppColors.darkBlue.withValues(alpha: .3),
                ),
                borderRadius: BorderRadius.circular(8)),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Icon(
                    FeatherIcons.uploadCloud,
                    color: PartnerAppColors.darkBlue,
                    size: 30,
                  ),
                  SmartGaps.gapW10,
                  Text(
                    tr('upload_invoices'),
                    style: Theme.of(context).textTheme.displayLarge!.copyWith(
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          color: PartnerAppColors.darkBlue,
                        ),
                  ),
                ]),
          ),
        ),
        SmartGaps.gapH20,
        if (files.isNotEmpty) ...[
          ListView.separated(
            itemBuilder: (context, index) {
              return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(child: Text(files[index].name)),
                    SmartGaps.gapW10,
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          files.removeAt(index);
                        });
                      },
                      child: const Icon(
                        FeatherIcons.trash2,
                        color: PartnerAppColors.spanishGrey,
                      ),
                    )
                  ]);
            },
            separatorBuilder: (context, index) {
              return SmartGaps.gapH10;
            },
            itemCount: files.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
          ),
          SmartGaps.gapH20,
        ],
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextButton(
                style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                        side: const BorderSide(
                            color: PartnerAppColors.darkBlue))),
                onPressed: () => Navigator.of(context).pop(),
                child: Text(
                  tr('cancel'),
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium!
                      .copyWith(color: PartnerAppColors.darkBlue),
                )),
            TextButton(
                style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    backgroundColor: PartnerAppColors.malachite),
                onPressed: () async {
                  if (files.isNotEmpty) {
                    Navigator.of(context).pop();

                    final vm = context.read<TenderFolderViewModel>();

                    await tryCatchWrapper(
                            context: myGlobals.homeScaffoldKey!.currentContext,
                            function: vm.uploadFiles(
                                files: files,
                                type: 'Invoice',
                                parentFile: widget.parentFile,
                                projectId: widget.projectId))
                        .then((value) {
                      if ((value ?? false)) {
                        showOkAlertDialog(
                            context: myGlobals.homeScaffoldKey!.currentContext!,
                            message: tr('file_uploaded_successfully'));
                      }
                    });
                  }
                },
                child: Text(
                  tr('submit'),
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium!
                      .copyWith(color: Colors.white),
                ))
          ],
        )
      ],
    );
  }
}
