import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomExpansionTile extends StatefulWidget {
  const CustomExpansionTile({
    super.key,
    required this.title,
    required this.titleSize,
    required this.backgroundColor,
    required this.textColor,
    required this.iconColor,
    required this.tileSize,
    required this.child,
  });

  final String title;
  final double titleSize;
  final Color backgroundColor;
  final Color textColor;
  final Color iconColor;
  final Size tileSize;
  final Widget child;

  @override
  CustomExpansionTileState createState() => CustomExpansionTileState();
}

class CustomExpansionTileState extends State<CustomExpansionTile> {
  bool _isTapped = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: () {
            setState(() {
              _isTapped = !_isTapped;
            });
          },
          child: Container(
            width: widget.tileSize.width,
            height: widget.tileSize.height,
            decoration: BoxDecoration(
              color: widget.backgroundColor.withValues(alpha: 0.9),
              borderRadius: BorderRadius.circular(5),
            ),
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  widget.title,
                  style: GoogleFonts.notoSans(
                    textStyle: TextStyle(
                      fontSize: 19,
                      height: 1.32,
                      color: widget.textColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Icon(
                  _isTapped
                      ? Icons.keyboard_arrow_up
                      : Icons.keyboard_arrow_down,
                  color: widget.iconColor,
                  size: 30,
                )
              ],
            ),
          ),
        ),
        if (_isTapped) widget.child
      ],
    );
  }
}
