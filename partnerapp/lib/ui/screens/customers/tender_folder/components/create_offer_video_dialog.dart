import 'package:Haandvaerker.dk/ui/components/video_wrapper.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/market_place_v2_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CreateOfferVideoDialog extends StatelessWidget {
  const CreateOfferVideoDialog({
    super.key,
    this.extraAction,
  });

  final Widget? extraAction;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.white,
      titlePadding: EdgeInsets.zero,
      insetPadding: const EdgeInsets.symmetric(horizontal: 20),
      title: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Align(
          alignment: Alignment.bottomRight,
          child: IconButton(
            onPressed: () {
              if (extraAction != null) {
                context.read<MarketPlaceV2ViewModel>().cancelDialog = true;
              } else {
                Navigator.pop(context);
              }
            },
            splashRadius: 20,
            icon: Stack(
              alignment: Alignment.center,
              children: [
                Container(
                  width: 23,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black.withValues(alpha: 0.4),
                      width: 2,
                    ),
                    shape: BoxShape.circle,
                  ),
                ),
                Icon(
                  Icons.close,
                  size: 18,
                  color: Colors.black.withValues(alpha: 0.4),
                ),
              ],
            ),
          ),
        ),
      ),
      contentPadding: const EdgeInsets.fromLTRB(20, 0, 20, 35),
      content: SingleChildScrollView(
        child: Column(
          children: [
            VideoSection(
              videoUrl: Links.createOfferVideo,
              fromMarketPlace: true,
              completeAction: () =>
                  context.read<MarketPlaceV2ViewModel>().isVideoDone = true,
            ),
            if (extraAction != null)
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: extraAction!,
              )
          ],
        ),
      ),
    );
  }
}
