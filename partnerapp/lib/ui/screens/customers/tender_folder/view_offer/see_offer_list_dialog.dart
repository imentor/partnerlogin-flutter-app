import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/model/offer_versions_list_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/view_offer/see_offer_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/upload_offer/view_uploaded_offer/view_uploaded_offer.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/messages/components.dart';
import 'package:Haandvaerker.dk/utils/extensions.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/edit_offer/edit_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';

Future<void> seeOfferListDialog(
    {required BuildContext context,
    required PartnerJobModel? partnerJob}) async {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20.0),
          contentPadding: const EdgeInsets.all(20.0),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(
                  onTap: () {
                    Navigator.of(dialogContext).pop();
                  },
                  child: const Icon(
                    FeatherIcons.x,
                    color: PartnerAppColors.spanishGrey,
                  )),
            ],
          ),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: SeeOfferListDialog(partnerJob: partnerJob)),
          ),
        );
      });
}

class SeeOfferListDialog extends StatefulWidget {
  const SeeOfferListDialog({super.key, this.partnerJob});
  final PartnerJobModel? partnerJob;

  @override
  State<SeeOfferListDialog> createState() => _SeeOfferListDialogState();
}

class _SeeOfferListDialogState extends State<SeeOfferListDialog> {
  int mainOfferVersion = 0;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      final createOfferVm = context.read<CreateOfferViewModel>();
      final editOfferVm = context.read<EditOfferViewmodel>();

      createOfferVm.isOfferListLoading = true;

      await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function: Future.wait([
          createOfferVm.getOfferById(offerId: widget.partnerJob?.offerId),
          editOfferVm.getOfferId(offerId: widget.partnerJob?.offerId)
        ]).whenComplete(() {
          final offerVersionList = createOfferVm.offerVersionList;

          if (offerVersionList != null &&
              offerVersionList.offerVersions != null) {
            mainOfferVersion = offerVersionList.offerVersions!.length;
          }

          createOfferVm.isOfferListLoading = false;
        }),
      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(tr('which_offer_preview'),
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.titleMedium!.copyWith(
                color: Colors.black,
                fontSize: 18,
                fontWeight: FontWeight.bold)),
        SmartGaps.gapH20,
        SingleChildScrollView(
          child: Consumer<CreateOfferViewModel>(
              builder: (context, createOfferVm, _) {
            final offerVersionList =
                createOfferVm.offerVersionList?.offerVersions;
            if (createOfferVm.isOfferListLoading) {
              return Card(
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                    side: const BorderSide(color: PartnerAppColors.grey)),
                child: SizedBox(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    child: const Center(child: ConnectivityLoader())),
              );
            } else {
              return Column(
                children: [
                  OfferCardTile(
                    mainOfferDate:
                        createOfferVm.offerVersionList?.createdAt ?? '',
                    mainOfferVersion: mainOfferVersion,
                    totalVat: createOfferVm.offerVersionList?.totalVat ?? 0,
                    showDialogWidget: SeeOffer(
                      contactId: widget.partnerJob?.customerId,
                      projectId: widget.partnerJob?.projectParentId != null
                          ? int.parse(widget.partnerJob?.projectParentId)
                          : widget.partnerJob?.projectId,
                    ),
                    createOfferVm: createOfferVm,
                    partnerJob: widget.partnerJob,
                  ),
                  for (var i = 0; i < mainOfferVersion; i++)
                    OfferCardTile(
                      index: i,
                      createOfferVm: createOfferVm,
                      offerVersionList:
                          offerVersionList ?? <OfferVersionsModel>[],
                      showDialogWidget: SeeOffer(
                        offerVersionId: offerVersionList?[i].id,
                        contactId: widget.partnerJob?.customerId,
                        projectId: widget.partnerJob?.projectParentId != null
                            ? int.parse(widget.partnerJob?.projectParentId)
                            : widget.partnerJob?.projectId,
                      ),
                      partnerJob: widget.partnerJob,
                    ),
                ],
              );
            }
          }),
        ),
      ],
    );
  }
}

class OfferCardTile extends StatefulWidget {
  const OfferCardTile(
      {super.key,
      required this.createOfferVm,
      required this.showDialogWidget,
      this.offerVersionList,
      this.index,
      this.mainOfferDate,
      this.mainOfferVersion,
      this.totalVat,
      this.partnerJob});
  final CreateOfferViewModel createOfferVm;
  final Widget showDialogWidget;
  final List<OfferVersionsModel>? offerVersionList;
  final int? index;
  final String? mainOfferDate;
  final int? mainOfferVersion;
  final dynamic totalVat;
  final PartnerJobModel? partnerJob;

  @override
  State<OfferCardTile> createState() => _OfferCardTileState();
}

class _OfferCardTileState extends State<OfferCardTile> {
  @override
  Widget build(BuildContext context) {
    final offerVersionList = widget.offerVersionList;
    final createOfferVm = widget.createOfferVm;
    final mainOfferVersion = createOfferVm.offerVersionList;
    final index = widget.index;
    return InkWell(
      onTap: () async {
        Navigator.of(context).pop();
        if (index != null && !offerVersionList.isNullOrEmpty) {
          if (offerVersionList?[index].ufType != null &&
              offerVersionList![index].ufType!.isNotEmpty &&
              offerVersionList[index].ufType == 'fast-offer' &&
              offerVersionList[index].offerFile != null &&
              offerVersionList[index].offerFile!.isNotEmpty &&
              offerVersionList[index].ufOfferId != null &&
              offerVersionList[index].ufTotalVat != null &&
              offerVersionList[index].ufSubTotalPrice != null) {
            await viewUploadedOffer(
                context: context,
                offerId: offerVersionList[index].ufOfferId!,
                totalPrice: offerVersionList[index].ufTotalVat!,
                subtotalPrice: offerVersionList[index].ufSubTotalPrice!,
                pdfUrl: offerVersionList[index].offerFile!);
          } else {
            await showDialog(
              barrierDismissible: false,
              context: context,
              builder: (context) => widget.showDialogWidget,
            );
          }
        } else {
          if ((((mainOfferVersion?.createdBy ?? '') == 'homeowner') ||
                  ((mainOfferVersion?.createdBy ?? '') == 'Homeowner')) &&
              ((widget.partnerJob?.statusNameEN ?? '') != 'Job Lost')) {
            await landingScreenDialog(
                context: context, partnerJob: widget.partnerJob);
          } else {
            if (mainOfferVersion?.offerType != null &&
                mainOfferVersion!.offerType!.isNotEmpty &&
                mainOfferVersion.offerType == 'fast-offer' &&
                mainOfferVersion.offerFile != null &&
                mainOfferVersion.offerFile!.isNotEmpty &&
                mainOfferVersion.id != null) {
              await viewUploadedOffer(
                  context: context, offerId: mainOfferVersion.id!);
            } else {
              await showDialog(
                barrierDismissible: false,
                context: context,
                builder: (context) => widget.showDialogWidget,
              );
            }
          }
        }
      },
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
            side: const BorderSide(color: PartnerAppColors.grey)),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Text(
                    tr('offer'),
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                          color: PartnerAppColors.darkBlue,
                          fontSize: 13,
                        ),
                  ),
                  SmartGaps.gapW10,
                  Text(
                    (index != null && !offerVersionList.isNullOrEmpty)
                        ? '${createOfferVm.formatDate(offerVersionList?[index].ufCreatedAt)} - v${offerVersionList?[index].ufVersion ?? 0}'
                        : '${createOfferVm.formatDate(widget.mainOfferDate)} - v${widget.mainOfferVersion! + 1}',
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                          color: PartnerAppColors.darkBlue,
                          fontSize: 13,
                        ),
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    (index != null && !offerVersionList.isNullOrEmpty)
                        ? Formatter.curencyFormat(
                            amount: offerVersionList![index]
                                    .ufTotalVat!
                                    .contains('.')
                                ? double.parse(
                                    offerVersionList[index].ufTotalVat!)
                                : int.parse(
                                    offerVersionList[index].ufTotalVat!))
                        : Formatter.curencyFormat(amount: widget.totalVat),
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                          color: PartnerAppColors.darkBlue,
                          fontSize: 15,
                        ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(bottom: 3),
                    child: Icon(
                      FeatherIcons.chevronRight,
                      color: PartnerAppColors.grey1,
                      size: 30,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Future<void> landingScreenDialog(
    {required BuildContext context,
    required PartnerJobModel? partnerJob}) async {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
            insetPadding: const EdgeInsets.all(20.0),
            contentPadding: const EdgeInsets.all(20.0),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                    onTap: () {
                      Navigator.of(dialogContext).pop();
                    },
                    child: const Icon(
                      FeatherIcons.x,
                      color: PartnerAppColors.spanishGrey,
                    )),
              ],
            ),
            content: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                    padding: const EdgeInsets.all(20),
                    child: LandingScreenDialog(partnerJob: partnerJob))));
      });
}

class LandingScreenDialog extends StatelessWidget {
  const LandingScreenDialog({super.key, required this.partnerJob});
  final PartnerJobModel? partnerJob;

  @override
  Widget build(BuildContext context) {
    final editOfferVm = context.read<EditOfferViewmodel>();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RichText(
            text: TextSpan(children: [
          TextSpan(
            text: '${tr('welcome')}, ',
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(fontWeight: FontWeight.normal),
          ),
          TextSpan(
            text: context.read<UserViewModel>().userModel?.user?.name ?? '',
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(fontWeight: FontWeight.bold),
          )
        ])),
        SmartGaps.gapH10,
        RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: "${(editOfferVm.viewedOffer?.homeowner?.name ?? '')}  ",
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(fontWeight: FontWeight.bold),
              ),
              TextSpan(
                text: "${tr('offer_changes_description_1')}  ",
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(fontWeight: FontWeight.normal),
              ),
              TextSpan(
                text: Formatter.formatDateStrings(
                    type: DateFormatType.standardDate,
                    dateString: editOfferVm.viewedOffer?.updatedAt),
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(fontWeight: FontWeight.bold),
              ),
              TextSpan(
                text: "${tr('for_project')} ",
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(fontWeight: FontWeight.normal),
              ),
              TextSpan(
                text: (partnerJob?.projectName ?? ''),
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        SmartGaps.gapH30,
        Text("${tr('offer_changes_description_2')} ",
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(fontWeight: FontWeight.normal)),
        SmartGaps.gapH10,
        Text(tr('offer_changes_description_3'),
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(fontWeight: FontWeight.normal)),
        SmartGaps.gapH30,
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Container(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, bottom: 20, top: 10),
                color: Colors.white,
                child: SizedBox(
                  height: 60,
                  child: CustomButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                      changeDrawerRoute(Routes.editOffer);
                    },
                    text: tr('see_updated_offer'),
                  ),
                ))
          ],
        ),
      ],
    );
  }
}
