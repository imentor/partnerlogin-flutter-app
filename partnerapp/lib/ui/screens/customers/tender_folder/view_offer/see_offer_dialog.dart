import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/see_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SeeOffer extends StatefulWidget {
  const SeeOffer({
    super.key,
    required this.contactId,
    required this.projectId,
    this.offerVersionId,
  });

  final int? contactId;
  final int projectId;
  final int? offerVersionId;

  @override
  State<SeeOffer> createState() => _SeeOfferState();
}

class _SeeOfferState extends State<SeeOffer> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      final seeOfferVm = context.read<SeeOfferViewModel>();

      if (widget.offerVersionId == null) {
        await tryCatchWrapper(
          context: myGlobals.homeScaffoldKey!.currentContext,
          function: Future.value(
            seeOfferVm.getListPriceMinbolig(
                contactId: widget.contactId!, projectId: widget.projectId),
          ),
        );
      } else {
        await tryCatchWrapper(
          context: myGlobals.homeScaffoldKey!.currentContext,
          function: Future.value(
            seeOfferVm.getListPriceMinbolig(
                contactId: widget.contactId!,
                projectId: widget.projectId,
                offerVersionId: widget.offerVersionId),
          ),
        );
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<SeeOfferViewModel>(
      builder: (context, seeOfferVm, _) {
        if (seeOfferVm.busy) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            backgroundColor: Colors.white,
            titlePadding: EdgeInsets.zero,
            title: SizedBox(
              height: 300,
              width: MediaQuery.of(context).size.width,
              child: const Center(
                child: ConnectivityLoader(),
              ),
            ),
          );
        }
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
          backgroundColor: Colors.white,
          titlePadding: EdgeInsets.zero,
          title: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    const Spacer(),
                    IconButton(
                      onPressed: () async {
                        Navigator.pop(context);
                      },
                      icon: Stack(
                        alignment: Alignment.center,
                        children: [
                          Container(
                            width: 23,
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black.withValues(alpha: 0.4),
                                width: 2,
                              ),
                              shape: BoxShape.circle,
                            ),
                          ),
                          Icon(
                            Icons.close,
                            size: 18,
                            color: Colors.black.withValues(alpha: 0.4),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Text(
                    tr('offer_list'),
                    style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                          fontSize: 26,
                          fontWeight: FontWeight.w700,
                        ),
                  ),
                ),
              ],
            ),
          ),
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //LIST PRICES
                ...seeOfferVm.seeOfferJobList.map(
                  (jobIndustry) {
                    final locale = context.locale.languageCode;
                    final jobIndustryLabel = locale == 'da'
                        ? context
                            .watch<TenderFolderViewModel>()
                            .fullIndustries
                            .firstWhere(
                              (element) =>
                                  element.brancheId.toString() ==
                                  jobIndustry.jobIndustryId,
                              orElse: () => FullIndustry.defaults(),
                            )
                            .brancheDa
                        : context
                            .watch<TenderFolderViewModel>()
                            .fullIndustries
                            .firstWhere(
                              (element) =>
                                  element.brancheId.toString() ==
                                  jobIndustry.jobIndustryId,
                              orElse: () => FullIndustry.defaults(),
                            )
                            .brancheEn;
                    final jobIndustryIndex =
                        seeOfferVm.seeOfferJobList.indexOf(jobIndustry) + 1;
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${jobIndustryIndex.toString()}. $jobIndustryLabel',
                        ),
                        ...jobIndustry.enterprises.map((enterprise) {
                          final enterpriseLabel = locale == 'da'
                              ? context
                                  .watch<TenderFolderViewModel>()
                                  .wizardTexts
                                  .firstWhere(
                                    (element) =>
                                        element.producttypeid ==
                                        enterprise.enterpriseId,
                                    orElse: () => AllWizardText.defaults(),
                                  )
                                  .producttypeDa
                              : context
                                  .watch<TenderFolderViewModel>()
                                  .wizardTexts
                                  .firstWhere(
                                    (element) =>
                                        element.producttypeid ==
                                        enterprise.enterpriseId,
                                    orElse: () => AllWizardText.defaults(),
                                  )
                                  .producttypeEn;
                          final enterpriseIndex =
                              jobIndustry.enterprises.indexOf(enterprise) + 1;

                          return Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      '${jobIndustryIndex.toString()}.${enterpriseIndex.toString()}. $enterpriseLabel',
                                    ),
                                    const Spacer(),
                                    Text(
                                      Formatter.curencyFormat(
                                        amount: double.parse(enterprise.total),
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10, right: 50),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: enterprise.descriptions
                                        .map((description) {
                                      final descriptionIndex = enterprise
                                              .descriptions
                                              .indexOf(description) +
                                          1;
                                      return Text(
                                        '${jobIndustryIndex.toString()}.${enterpriseIndex.toString()}.${descriptionIndex.toString()}. $description',
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ],
                            ),
                          );
                        }),
                      ],
                    );
                  },
                ),

                // STANDARD TEXT
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SmartGaps.gapH5,
                      Text(
                        tr('craftsman_standard_text'),
                      ),
                      SmartGaps.gapH5,
                      Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 20),
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            color: Theme.of(context)
                                .colorScheme
                                .onTertiaryContainer
                                .withValues(alpha: 0.5),
                          ),
                        ),
                        child: Text(
                          seeOfferVm.minboligListPrice?.result?.first.note ??
                              "",
                        ),
                      ),
                    ],
                  ),
                ),

                //PRICE VALUES
                Divider(
                  color: Theme.of(context)
                      .colorScheme
                      .onTertiaryContainer
                      .withValues(alpha: 0.5),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SmartGaps.gapH10,
                    Row(
                      children: [
                        Text(
                          tr('unit_price_excl_vat'),
                        ),
                        const Spacer(),
                        Text(
                          Formatter.curencyFormat(
                            amount: seeOfferVm.total,
                          ),
                        ),
                      ],
                    ),
                    SmartGaps.gapH20,
                    Row(
                      children: [
                        Text(
                          tr('total_price_incl_vat'),
                        ),
                        const Spacer(),
                        Text(
                          Formatter.curencyFormat(
                              amount: seeOfferVm.totalWithVat),
                        ),
                      ],
                    ),
                    SmartGaps.gapH20,
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
