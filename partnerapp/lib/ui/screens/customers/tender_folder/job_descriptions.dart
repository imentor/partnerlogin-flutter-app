import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/model/questions/questions_response_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/question_answer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:badges/badges.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart' hide Badge;
import 'package:provider/provider.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class JobDescriptions extends StatefulWidget {
  const JobDescriptions({
    super.key,
    required this.job,
  });

  final PartnerJobModel? job;

  @override
  State<JobDescriptions> createState() => _JobDescriptionsState();
}

class _JobDescriptionsState extends State<JobDescriptions> {
  TextEditingController? _newQuestionController;
  PanelController? _panelController;
  late Widget questionBox;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    _newQuestionController = TextEditingController();
    _panelController = PanelController();

    super.initState();
    final tenderVm = context.read<TenderFolderViewModel>();
    final questionAnswerVm = context.read<QuestionAnswerViewModel>();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      int? projectParentId = widget.job!.projectParentId != null
          ? int.parse(widget.job!.projectParentId)
          : widget.job!.projectId;
      await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function: Future.wait([
          questionAnswerVm.getAllQuestions(projectParentId: projectParentId!),
          tenderVm.getListPriceAi(
              projectParentId, widget.job!.customerId!, widget.job!),
        ]),
      );
      questionAnswerVm.getQuestion = 'parent';
    });
  }

  bool isPanelOpen = false;
  int onPanelSlideControl = 0;
  bool descriptionTapped = false;

  @override
  void dispose() {
    _newQuestionController!.dispose();
    // _tabController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final questionAnswerVm = context.read<QuestionAnswerViewModel>();
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SlidingUpPanel(
        parallaxEnabled: true,
        controller: _panelController,
        maxHeight: 0.75 * size.height,
        minHeight: 80,
        margin: const EdgeInsets.symmetric(horizontal: 10),
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(5),
          topRight: Radius.circular(5),
        ),
        onPanelClosed: () {
          questionAnswerVm.getQuestion = 'parent';

          setState(() {
            isPanelOpen = false;
            if (descriptionTapped) descriptionTapped = false;
          });
        },
        onPanelOpened: () {
          setState(() {
            isPanelOpen = true;
          });
        },
        onPanelSlide: (val) async {
          if (val == 1.0 && onPanelSlideControl < 1 && !descriptionTapped) {
            int? projectParentId = widget.job!.projectParentId != null
                ? int.parse(widget.job!.projectParentId)
                : widget.job!.projectId;
            await questionAnswerVm.getAllQuestions(
                projectParentId: projectParentId!);

            if (mounted) questionAnswerVm.getQuestion = 'parent';
            setState(() {
              onPanelSlideControl++;
            });
          }

          if (val == 0.0) {
            setState(() {
              onPanelSlideControl = 0;
            });
          }
        },
        panelBuilder: (scrollController) {
          return GestureDetector(
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            child: Column(
              children: [
                SmartGaps.gapH15,
                GestureDetector(
                  onTap: () async {
                    int? projectParentId = widget.job!.projectParentId != null
                        ? int.parse(widget.job!.projectParentId)
                        : widget.job!.projectId;
                    if (isPanelOpen) {
                      _panelController!.close();
                    } else {
                      _panelController!.open();
                      await questionAnswerVm.getAllQuestions(
                          projectParentId: projectParentId!);
                      questionAnswerVm.getQuestion = 'parent';
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: Text(
                      tr('questions'),
                      style:
                          Theme.of(context).textTheme.headlineLarge!.copyWith(
                                fontSize: 24,
                                fontWeight: FontWeight.w800,
                              ),
                    ),
                  ),
                ),
                Expanded(
                  child: Consumer<QuestionAnswerViewModel>(
                    builder: (context, questionAnswerVm, child) {
                      if (questionAnswerVm.busy &&
                          questionAnswerVm.getQuestion == 'parent') {
                        return const Center(child: ConnectivityLoader());
                      } else if (questionAnswerVm.questionsList.isEmpty) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            if (descriptionTapped) questionBox,
                            const Expanded(
                                child: EmptyListIndicator(
                                    route: Routes.jobDescriptions)),
                          ],
                        );
                      } else {
                        return SingleChildScrollView(
                          controller: scrollController,
                          child: Column(
                            children: [
                              if (descriptionTapped) questionBox,
                              if (!descriptionTapped)
                                ...questionAnswerVm.questionsList.map(
                                  (question) => Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: QuestionTemplate(
                                      question: question,
                                      job: widget.job,
                                    ),
                                  ),
                                )
                              else
                                ...questionAnswerVm.activeQuestionList.map(
                                  (question) => Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: QuestionTemplate(
                                      question: question,
                                      job: widget.job,
                                    ),
                                  ),
                                ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).viewInsets.bottom +
                                          20),
                            ],
                          ),
                        );
                      }
                    },
                  ),
                ),
              ],
            ),
          );
        },
        body: Container(
          width: size.width,
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          child: ListView(
            shrinkWrap: true,
            physics: const BouncingScrollPhysics(),
            children: [
              SmartGaps.gapH20,
              Text(
                tr('question_answer'),
                style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                      fontSize: 24,
                      fontWeight: FontWeight.w800,
                    ),
              ),
              SmartGaps.gapH40,
              Text(
                widget.job!.projectName!,
                style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                      fontSize: 22,
                      fontWeight: FontWeight.w700,
                    ),
              ),
              SmartGaps.gapH30,
              Consumer<TenderFolderViewModel>(
                builder: (context, tenderVm, child) {
                  if (tenderVm.busy) {
                    return const Center(child: ConnectivityLoader());
                  } else if (tenderVm.listPriceAi.isEmpty) {
                    return const EmptyListIndicator(
                        route: Routes.jobDescriptions);
                  } else {
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 200),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ...tenderVm.listPriceAi.map(
                            (e) {
                              return _jobDescriptionTemplate(
                                enterpriseIndustry: e,
                                tenderVm: tenderVm,
                                questionAnswerVm: questionAnswerVm,
                              );
                            },
                          ),
                        ],
                      ),
                    );
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _jobDescriptionTemplate({
    required EnterpriseIndustry enterpriseIndustry,
    required TenderFolderViewModel tenderVm,
    QuestionAnswerViewModel? questionAnswerVm,
  }) {
    final enterprise = context.locale.languageCode == 'da'
        ? tenderVm.wizardTexts
                .firstWhere(
                  (element) =>
                      element.producttypeid == enterpriseIndustry.enterpriseId,
                  orElse: () => AllWizardText(producttypeDa: 'N/A'),
                )
                .producttypeDa ??
            'N/A'
        : tenderVm.wizardTexts
                .firstWhere(
                    (element) =>
                        element.producttypeid ==
                        enterpriseIndustry.enterpriseId,
                    orElse: () => AllWizardText(producttypeEn: 'N/A'))
                .producttypeEn ??
            'N/A';

    final enterpriseIndex = tenderVm.listPriceAi.indexWhere((element) =>
            element.enterpriseId == enterpriseIndustry.enterpriseId) +
        1;

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // ENTERPRISE
          Text(
            '$enterpriseIndex. ${enterprise.trim()}',
            style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                ),
          ),
          SmartGaps.gapH10,

          // JOB INDUSTRY
          ...enterpriseIndustry.jobIndustries!.map((jobIndustry) {
            final industry = context.locale.languageCode == 'da'
                ? tenderVm.fullIndustries
                    .firstWhere(
                      (element) =>
                          element.brancheId.toString() ==
                          jobIndustry.jobIndustryId,
                      orElse: () => FullIndustry.defaults(),
                    )
                    .brancheDa
                : tenderVm.fullIndustries
                    .firstWhere(
                      (element) =>
                          element.brancheId.toString() ==
                          jobIndustry.jobIndustryId,
                      orElse: () => FullIndustry.defaults(),
                    )
                    .brancheEn;
            final industryIndex = enterpriseIndustry.jobIndustries != null
                ? enterpriseIndustry.jobIndustries!.indexWhere((element) =>
                        element.jobIndustryId == jobIndustry.jobIndustryId) +
                    1
                : 0;

            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '$enterpriseIndex.$industryIndex. ${industry != null ? industry.trim() : ''}',
                    style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                        ),
                  ),
                  if (jobIndustry.descriptions != null &&
                      jobIndustry.descriptions!.isNotEmpty)
                    ...jobIndustry.descriptions!.map((description) {
                      final decriptionIndex = jobIndustry.descriptions!
                              .indexWhere((element) => element == description) +
                          1;

                      int numberOfQuestions = 0;
                      List<QuestionResponseModel> questions = [];

                      for (final question in questionAnswerVm!.questionsList) {
                        if (question.descriptionId!.split('-').last ==
                                jobIndustry.descriptions!
                                    .indexOf(description)
                                    .toString() &&
                            tenderVm.listPriceAi[enterpriseIndex - 1]
                                    .enterpriseId ==
                                question.industryId &&
                            jobIndustry.jobIndustryId ==
                                question.jobIndustryId) {
                          questions.add(question);
                          numberOfQuestions++;
                        }
                      }

                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              child: Text(
                                '$enterpriseIndex.$industryIndex.$decriptionIndex. ${description.trim()}',
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineLarge!
                                    .copyWith(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                    ),
                              ),
                            ),
                            SmartGaps.gapW5,
                            GestureDetector(
                              onTap: () {
                                questionAnswerVm.getQuestion = 'children';
                                setState(() {
                                  questionBox = _addAQuestion(
                                    tenderVm: tenderVm,
                                    enterpriseId:
                                        enterpriseIndustry.enterpriseId,
                                    jobIndustryId: jobIndustry.jobIndustryId,
                                    descriptionIndex:
                                        (decriptionIndex - 1).toString(),
                                    questionAnswerVm: questionAnswerVm,
                                  );
                                  descriptionTapped = true;
                                });
                                questionAnswerVm.activeQuestionList = questions;
                                _panelController!.open();
                              },
                              child: Badge(
                                position: BadgePosition.topEnd(top: -5, end: 1),
                                badgeContent: Text(
                                  numberOfQuestions.toString(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineLarge!
                                      .copyWith(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white,
                                      ),
                                ),
                                child: Icon(
                                  Icons.message_outlined,
                                  size: 30,
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onTertiaryContainer
                                      .withValues(alpha: 0.7),
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    }),
                ],
              ),
            );
          }),
        ],
      ),
    );
  }

  Widget _addAQuestion({
    String? enterpriseId,
    String? jobIndustryId,
    String? descriptionIndex,
    TenderFolderViewModel? tenderVm,
    QuestionAnswerViewModel? questionAnswerVm,
  }) {
    int? projectId = widget.job!.projectParentId != null
        ? int.parse(widget.job!.projectParentId)
        : widget.job!.projectId;
    final enterprise = context.locale.languageCode == 'da'
        ? tenderVm!.wizardTexts
            .firstWhere(
              (element) => element.producttypeid == enterpriseId,
              orElse: () => AllWizardText.defaults(),
            )
            .producttypeDa
        : tenderVm!.wizardTexts
            .firstWhere(
              (element) => element.producttypeid == enterpriseId,
              orElse: () => AllWizardText.defaults(),
            )
            .producttypeEn;
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: Text(
              'Tags',
              style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context)
                        .colorScheme
                        .onTertiaryContainer
                        .withValues(alpha: 0.6),
                  ),
            ),
          ),
          SmartGaps.gapH5,
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 30),
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
              border: Border.all(
                color: Theme.of(context)
                    .colorScheme
                    .onTertiaryContainer
                    .withValues(alpha: 0.2),
              ),
              borderRadius: BorderRadius.circular(5),
            ),
            child: Row(
              children: [
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Theme.of(context)
                        .colorScheme
                        .primary
                        .withValues(alpha: 0.5),
                  ),
                  child: Center(
                    child: Text(
                      enterprise ?? '',
                      style:
                          Theme.of(context).textTheme.headlineLarge!.copyWith(
                                fontSize: 18,
                                fontWeight: FontWeight.w500,
                                color: Colors.black.withValues(alpha: 0.7),
                              ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SmartGaps.gapH15,
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: TextFormField(
              controller: _newQuestionController,
              onChanged: (val) {},
              validator: (val) {
                if (val!.isEmpty) {
                  return tr('input_question_validator');
                }
                return null;
              },
              minLines: 7,
              maxLines: 12,
              decoration: InputDecoration(
                hintText: tr('input_question_here'),
                hintStyle: Theme.of(context).textTheme.headlineLarge!.copyWith(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: Theme.of(context)
                          .colorScheme
                          .onTertiaryContainer
                          .withValues(alpha: 0.2),
                    ),
                isDense: true,
                contentPadding: const EdgeInsets.fromLTRB(10, 15, 5, 0),
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Theme.of(context)
                        .colorScheme
                        .onTertiaryContainer
                        .withValues(alpha: 0.2),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Theme.of(context).colorScheme.primary,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
            child: CustomDesignTheme.flatButtonStyle(
              backgroundColor: Theme.of(context).colorScheme.onSurfaceVariant,
              textStyle: const TextStyle(color: Colors.white),
              onPressed: () async {
                if (_formKey.currentState!.validate()) {
                  final payload = {
                    'projectId': projectId,
                    'contactId': widget.job!.customerId.toString(),
                    'descriptionId': descriptionIndex,
                    'industryId': enterpriseId,
                    'jobIndustryId': jobIndustryId,
                    'question': _newQuestionController!.text.trim(),
                  };
                  questionAnswerVm!.getQuestion = 'parent';
                  await questionAnswerVm
                      .addQuestion(
                    projectId: payload['projectId'] as int,
                    contactId: payload['contactId'] as String,
                    descriptionId: payload['descriptionId'] as String,
                    industryId: payload['industryId'] as String,
                    jobIndustryId: payload['jobIndustryId'] as String,
                    question: payload['question'] as String,
                  )
                      .whenComplete(() async {
                    _newQuestionController!.clear();
                    await questionAnswerVm
                        .getAllQuestions(projectParentId: projectId!)
                        .whenComplete(() {
                      List<QuestionResponseModel> questions = [];

                      for (final question in questionAnswerVm.questionsList) {
                        if (question.descriptionId!.split('-').last ==
                                payload['descriptionId'] &&
                            payload['industryId'] == question.industryId &&
                            payload['jobIndustryId'] ==
                                question.jobIndustryId) {
                          questions.add(question);
                        }
                      }
                      questionAnswerVm.activeQuestionList = questions;
                    });
                  });
                }
              },
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12),
                  child: Text(
                    tr('add_question'),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class QuestionTemplate extends StatefulWidget {
  const QuestionTemplate(
      {super.key, required this.question, required this.job});

  final QuestionResponseModel question;
  final PartnerJobModel? job;
  @override
  State<QuestionTemplate> createState() => _QuestionTemplateState();
}

class _QuestionTemplateState extends State<QuestionTemplate> {
  int? projectId;
  final _formKey = GlobalKey<FormState>();
  TextEditingController? _replyController;

  void onTap({required QuestionAnswerViewModel questionAnswerVm}) async {
    questionAnswerVm.getQuestion = 'children';

    if (widget.question.totalReply! > 0) {
      modalManager.showLoadingModal();

      await tryCatchWrapper(
          context: context,
          function: questionAnswerVm.getChildQuestions(
              projectId: projectId!, parentId: widget.question.id!));

      modalManager.hideLoadingModal();

      if (!mounted) return;

      await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) => _repliesPopUpDialog(context: context),
      );
    }
  }

  @override
  void initState() {
    _replyController = TextEditingController();
    projectId = widget.job!.projectParentId != null
        ? int.parse(widget.job!.projectParentId)
        : widget.job!.projectId;
    super.initState();
  }

  @override
  void dispose() {
    _replyController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<QuestionAnswerViewModel>(
      builder: (context, questionAnswerVm, child) {
        return Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.question.partnerId == widget.job!.partnerId
                    ? tr('you')
                    : tr('craftsman'),
                style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      color: Theme.of(context)
                          .colorScheme
                          .onTertiaryContainer
                          .withValues(alpha: 0.7),
                    ),
              ),
              SmartGaps.gapH8,
              Row(
                children: [
                  Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Theme.of(context)
                          .colorScheme
                          .primary
                          .withValues(alpha: 0.6),
                    ),
                    child: Center(
                      child: Text(
                        widget.question.industry!.first.name!,
                        style:
                            Theme.of(context).textTheme.headlineLarge!.copyWith(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onTertiaryContainer
                                      .withValues(alpha: 0.7),
                                ),
                      ),
                    ),
                  ),
                  const Spacer(),
                ],
              ),
              SmartGaps.gapH10,
              Text(
                widget.question.message!,
                style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                    ),
              ),
              SmartGaps.gapH15,
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () => onTap(questionAnswerVm: questionAnswerVm),
                    child: Text(
                      questionAnswerVm.questionsList
                                  .firstWhere(
                                    (element) =>
                                        element.id == widget.question.id,
                                    orElse: () =>
                                        QuestionResponseModel.defaults(),
                                  )
                                  .totalReply! >
                              1
                          ? '${questionAnswerVm.questionsList.firstWhere(
                                (element) => element.id == widget.question.id,
                                orElse: () => QuestionResponseModel.defaults(),
                              ).totalReply.toString()} Answers'
                          : '${questionAnswerVm.questionsList.firstWhere(
                                (element) => element.id == widget.question.id,
                                orElse: () => QuestionResponseModel.defaults(),
                              ).totalReply.toString()} Answer',
                      style:
                          Theme.of(context).textTheme.headlineLarge!.copyWith(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: Theme.of(context).colorScheme.primary,
                              ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _repliesPopUpDialog({
    BuildContext? context,
  }) {
    return Consumer<QuestionAnswerViewModel>(
      builder: (context, questionAnswerVm, child) {
        if (widget.question.totalReply! > 0 &&
            questionAnswerVm.busy &&
            questionAnswerVm.getQuestion == 'children') {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            backgroundColor: Colors.white,
            titlePadding: const EdgeInsets.symmetric(vertical: 100),
            title: const Center(
              child: ConnectivityLoader(),
            ),
          );
        }
        int rebuildControl = 0;
        return StatefulBuilder(
          builder: (context, setState) => AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            backgroundColor: Colors.white,
            titlePadding: EdgeInsets.zero,
            title: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Align(
                    alignment: Alignment.topRight,
                    child: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                        _replyController!.clear();
                      },
                      icon: Stack(
                        alignment: Alignment.center,
                        children: [
                          Container(
                            width: 23,
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black.withValues(alpha: 0.4),
                                width: 2,
                              ),
                              shape: BoxShape.circle,
                            ),
                          ),
                          Icon(
                            Icons.close,
                            size: 18,
                            color: Colors.black.withValues(alpha: 0.4),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Text(
                      widget.question.message!,
                      style:
                          Theme.of(context).textTheme.headlineLarge!.copyWith(
                                fontSize: 20,
                                fontWeight: FontWeight.w400,
                              ),
                    ),
                  ),
                ],
              ),
            ),
            contentPadding:
                const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            content: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SmartGaps.gapH10,
                    ...questionAnswerVm.childQuestionsList.map(
                      (e) => ReplyTemplate(
                        childQuestion: e,
                        index: questionAnswerVm.childQuestionsList.indexOf(e),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 20),
                      child: TextFormField(
                        controller: _replyController,
                        onChanged: (val) {
                          if (rebuildControl < 1) {
                            rebuildControl++;
                            setState(() {});
                          }
                          if (val.isEmpty) {
                            rebuildControl = 0;
                            setState(() {});
                          }
                        },
                        validator: (val) {
                          if (val!.isEmpty) {
                            return tr('reply_validator');
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          suffixIcon: IconButton(
                            onPressed: () async {
                              if (_formKey.currentState!.validate()) {
                                final payload = {
                                  'projectId': projectId,
                                  'parentId': widget.question.id,
                                  'message': _replyController!.text.trim(),
                                };
                                questionAnswerVm.getQuestion = 'children';
                                final response =
                                    await questionAnswerVm.replyQuestion(
                                  projectId: payload['projectId'] as int,
                                  parentId: payload['parentId'] as int,
                                  message: payload['message'] as String,
                                );

                                _replyController!.clear();
                                rebuildControl = 0;
                                questionAnswerVm.childQuestionsList = [
                                  ...questionAnswerVm.childQuestionsList,
                                  response
                                ];
                                await questionAnswerVm.getAllQuestions(
                                    projectParentId: projectId!);
                              }
                            },
                            color: _replyController!.text.trim().isEmpty
                                ? Theme.of(context)
                                    .colorScheme
                                    .onTertiaryContainer
                                    .withValues(alpha: 0.4)
                                : Theme.of(context)
                                    .colorScheme
                                    .primary
                                    .withValues(alpha: 0.8),
                            padding: EdgeInsets.zero,
                            icon: const Icon(
                              Icons.send,
                              size: 16,
                            ),
                          ),
                          hintText: tr('input_reply_here'),
                          hintStyle: Theme.of(context)
                              .textTheme
                              .headlineLarge!
                              .copyWith(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: Theme.of(context)
                                    .colorScheme
                                    .onTertiaryContainer
                                    .withValues(alpha: 0.2),
                              ),
                          isDense: true,
                          contentPadding:
                              const EdgeInsets.fromLTRB(10, 15, 5, 0),
                          border: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Theme.of(context)
                                  .colorScheme
                                  .onTertiaryContainer
                                  .withValues(alpha: 0.2),
                            ),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class ReplyTemplate extends StatelessWidget {
  const ReplyTemplate({
    super.key,
    required this.childQuestion,
    required this.index,
  });

  final ChildQuestionResponseModel childQuestion;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.only(
        left: 20,
        top: 10,
        bottom: 10,
      ),
      decoration: BoxDecoration(
        color: index % 2 == 0
            ? Theme.of(context).colorScheme.primary.withValues(alpha: 0.2)
            : Colors.transparent,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            childQuestion.messageFromId == childQuestion.partnerId
                ? tr('you')
                : tr('homeowner'),
            style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  color: Theme.of(context).colorScheme.primary,
                ),
          ),
          Text(
            childQuestion.message!,
            style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: Theme.of(context).colorScheme.onTertiaryContainer,
                ),
          )
        ],
      ),
    );
  }
}
