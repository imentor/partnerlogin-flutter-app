import 'dart:io';
import 'dart:math' as math;

import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart'
    as job_model;
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/get_image_with_custom_picker.dart';
import 'package:Haandvaerker.dk/ui/components/image_viewer.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

Future<void> jobNotesDialog({
  required BuildContext context,
  required job_model.PartnerJobModel job,
  bool isAffiliate = false,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: JobNotes(
              job: job,
              isAffiliate: isAffiliate,
            ),
          ),
        ),
      );
    },
  );
}

class JobNotes extends StatefulWidget {
  const JobNotes({super.key, required this.job, this.isAffiliate = false});
  final job_model.PartnerJobModel job;
  final bool isAffiliate;

  @override
  State<JobNotes> createState() => _JobNotesState();
}

class _JobNotesState extends State<JobNotes> {
  bool showForm = false;
  List<File> files = [];
  List<String> randNum = [];

  final formKey = GlobalKey<FormBuilderState>();

  void onTap({required job_model.JobNotes note}) async {
    final clientsVm = context.read<ClientsViewModel>();
    final tenderVm = context.read<TenderFolderViewModel>();

    showOkCancelAlertDialog(
            context: context,
            title: tr('delete_note_confirmation'),
            okLabel: tr('yes_thank_you'),
            cancelLabel: tr('no_thanks'))
        .then((value) async {
      if (value == OkCancelResult.ok && mounted) {
        Navigator.of(context).pop();
        Navigator.of(context).pop();

        clientsVm.getYourCustomerData = true;
        modalManager.showLoadingModal();

        await tryCatchWrapper(
                context: myGlobals.homeScaffoldKey!.currentContext,
                function: tenderVm.deleteNote(
                    jobId: widget.job.id!, noteId: note.id!))
            .then((deleteNote) async {
          await clientsVm.getPartnerJobsV2();

          if ((tenderVm.deleteNoteResponse?.success ?? false)) {
            clientsVm.getYourCustomerData = false;
            modalManager.hideLoadingModal();
            commonSuccessOrFailedDialog(
              context: myGlobals.homeScaffoldKey!.currentContext!,
            );
          } else {
            clientsVm.getYourCustomerData = false;
            modalManager.hideLoadingModal();
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    context.read<UserViewModel>().userModel?.user?.name ?? '',
                    style: context.pttTitleSmall.copyWith(
                      color: PartnerAppColors.darkBlue,
                    ),
                  ),
                  Text(
                    '${tr('list_your_notes')} :',
                    style: context.pttTitleSmall.copyWith(
                        color: PartnerAppColors.darkBlue,
                        fontSize: 12,
                        fontWeight: FontWeight.normal),
                  )
                ],
              ),
              SmartGaps.gapW20,
              CustomDesignTheme.flatButtonStyle(
                backgroundColor: PartnerAppColors.blue,
                onPressed: () {
                  setState(() {
                    if (!showForm) {
                      files = [];
                      randNum = [];
                    }
                    showForm = !showForm;
                  });
                },
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(
                      FeatherIcons.plusCircle,
                      color: Colors.white,
                    ),
                    SmartGaps.gapW10,
                    Flexible(
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(
                          tr('add_note'),
                          textAlign: TextAlign.start,
                          style: context.pttBodySmall.copyWith(
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          if (showForm) ...[
            SmartGaps.gapH20,
            CustomTextFieldFormBuilder(
              name: 'note',
              hintText: tr('enter_notes_here'),
              minLines: 5,
              validator:
                  FormBuilderValidators.required(errorText: tr('required')),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  border: Border.all(
                    color: PartnerAppColors.darkBlue.withValues(alpha: .3),
                  ),
                  borderRadius: BorderRadius.circular(8)),
              child: GestureDetector(
                onTap: () async {
                  final fileResultList = await getImage(context);
                  final rand = math.Random().nextInt(10000);

                  if (fileResultList != null) {
                    setState(() {
                      files.add(fileResultList);
                      randNum.add('$rand');
                    });
                  }
                },
                child:
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  const Icon(
                    FeatherIcons.uploadCloud,
                    color: PartnerAppColors.darkBlue,
                    size: 30,
                  ),
                  SmartGaps.gapW10,
                  Text(
                    tr('upload_file'),
                    style: Theme.of(context).textTheme.displayLarge!.copyWith(
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          color: PartnerAppColors.darkBlue,
                        ),
                  ),
                ]),
              ),
            ),
            if (files.isNotEmpty) ...[
              SmartGaps.gapH10,
              ListView.separated(
                separatorBuilder: ((context, index) {
                  return SmartGaps.gapH10;
                }),
                itemBuilder: (context, index) {
                  return Container(
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color:
                            PartnerAppColors.spanishGrey.withValues(alpha: .3),
                        borderRadius: BorderRadius.circular(5)),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                              child: Text(
                                  'noteImage_$index${randNum[index]}.${files[index].path.split('.').last}')),
                          SmartGaps.gapW10,
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                files.removeAt(index);
                              });
                            },
                            child: const Icon(
                              FeatherIcons.trash2,
                              color: PartnerAppColors.spanishGrey,
                            ),
                          )
                        ]),
                  );
                },
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: files.length,
              )
            ],
            SmartGaps.gapH20,
            CustomDesignTheme.flatButtonStyle(
              height: 50,
              backgroundColor: PartnerAppColors.malachite,
              onPressed: () async {
                final clientsVm = context.read<ClientsViewModel>();
                final tenderVm = context.read<TenderFolderViewModel>();

                if (formKey.currentState!.validate()) {
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();

                  clientsVm.getYourCustomerData = true;
                  modalManager.showLoadingModal();

                  final data = FormData();

                  data.fields.add(MapEntry('note',
                      '${formKey.currentState!.fields['note']!.value}'));

                  for (final file in files) {
                    data.files.add(
                      MapEntry(
                        'files[${files.indexOf(file).toString()}]',
                        await MultipartFile.fromFile(
                          file.path,
                          filename: file.path.split('/').last,
                        ),
                      ),
                    );
                  }

                  if (widget.isAffiliate) {
                    data.fields
                        .add(MapEntry('affiliateId', '${widget.job.id}'));

                    await tryCatchWrapper(
                            context: myGlobals.homeScaffoldKey!.currentContext,
                            function:
                                clientsVm.addAffiliateJobNotes(data: data))
                        .then((value) async {
                      await isSuccessfull(
                          clientsVm: clientsVm,
                          tenderVm: tenderVm,
                          isSuccessful: (value ?? false),
                          isAffiliate: true);
                    });
                  } else {
                    data.fields.add(MapEntry('jobId', '${widget.job.id}'));

                    await tryCatchWrapper(
                            context: myGlobals.homeScaffoldKey!.currentContext,
                            function: tenderVm.addNote(payload: data))
                        .then((value) async {
                      await isSuccessfull(
                          clientsVm: clientsVm,
                          tenderVm: tenderVm,
                          isSuccessful:
                              (tenderVm.addNoteResponse?.success ?? false),
                          isAffiliate: false);
                    });
                  }
                }
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SmartGaps.gapW10,
                  Flexible(
                    child: Text(
                      tr('add_note'),
                      style: context.pttBodySmall.copyWith(
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                        letterSpacing: 0.5,
                      ),
                    ),
                  ),
                  const Icon(
                    Icons.arrow_forward,
                    color: Colors.white,
                  ),
                ],
              ),
            )
          ],
          if ((widget.job.notes ?? []).isNotEmpty) ...[
            SmartGaps.gapH30,
            ...(widget.job.notes ?? []).map(
              (note) {
                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: PartnerAppColors.darkBlue.withValues(alpha: .3),
                      ),
                    ),
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: Text(
                                note.note ?? '',
                                style: context.pttTitleSmall.copyWith(
                                    color: PartnerAppColors.darkBlue,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                            if (!widget.isAffiliate)
                              GestureDetector(
                                onTap: () => onTap(note: note),
                                child: const Icon(
                                  FeatherIcons.trash2,
                                  color: PartnerAppColors.blue,
                                ),
                              )
                          ],
                        ),
                        SmartGaps.gapH10,
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              ...(note.files ?? []).map(
                                (noteFiles) {
                                  if ((noteFiles.fileURL ?? '').isEmpty) {
                                    return const SizedBox.shrink();
                                  }
                                  return InkWell(
                                    onTap: () => imageViewerDialog(
                                        context: context,
                                        fileUrl: noteFiles.fileURL!),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 5),
                                      child: Container(
                                        height: 75,
                                        width: 75,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          border: Border.all(
                                              color: PartnerAppColors.darkBlue
                                                  .withValues(alpha: .3)),
                                          image: DecorationImage(
                                            image: CachedNetworkImageProvider(
                                              noteFiles.fileURL!,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ]
        ],
      ),
    );
  }

  Future<void> isSuccessfull(
      {required ClientsViewModel clientsVm,
      required TenderFolderViewModel tenderVm,
      required bool isSuccessful,
      bool isAffiliate = false}) async {
    if (isAffiliate) {
      await clientsVm.getAffiliateJobs();
    } else {
      await clientsVm.getPartnerJobsV2();
    }

    if (isSuccessful) {
      clientsVm.getYourCustomerData = false;
      modalManager.hideLoadingModal();
      commonSuccessOrFailedDialog(
        context: myGlobals.homeScaffoldKey!.currentContext!,
      );
    } else {
      clientsVm.getYourCustomerData = false;
      modalManager.hideLoadingModal();
    }
  }
}
