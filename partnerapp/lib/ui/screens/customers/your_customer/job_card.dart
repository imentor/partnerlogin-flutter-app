import 'package:Haandvaerker.dk/model/customer_minbolig/old_jobs_model.dart';
import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/job_detail.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class JobCard extends StatefulWidget {
  const JobCard({
    super.key,
    this.job,
    required this.jobIndex,
    this.oldJob,
  });

  final PartnerJobModel? job;
  final int jobIndex;
  final OldPartnerJobsModel? oldJob;

  @override
  State<JobCard> createState() => _JobCardState();
}

class _JobCardState extends State<JobCard> {
  @override
  Widget build(BuildContext context) {
    return Consumer<TenderFolderViewModel>(
      builder: (_, tenderVm, __) {
        bool isDK = context.locale.languageCode == 'da';

        String offerStatus = '';
        String customerName = '';
        String title = '';
        int differenceInDays = 0;
        String projectDeadlineOffer = '';
        String projectDeadlineOfferColorCode = '';
        Color projectDeadlineOfferColor = PartnerAppColors.darkBlue;

        if (widget.job != null) {
          customerName = widget.job?.customerName ?? '';
          title = widget.job?.projectName ?? '';

          if ((widget.job?.projectStatus ?? '') == 'Paused') {
            offerStatus = tr('deactivated_user');
          } else if ((widget.job?.alertStatus ?? '') == 'pending') {
            offerStatus = isDK ? 'Afventer gennemsyn' : 'Pending review';
          } else if ((widget.job?.statusNameEN ?? '') == 'Customer Deleted') {
            offerStatus = tr('customer_deleted');
          } else if ((widget.job?.statusNameEN ?? '') == 'Offer sent') {
            offerStatus = isDK ? 'I dialog' : 'In negotiation';
          } else if ((widget.job?.statusNameEN ?? '') == 'Job is won') {
            offerStatus = tr('job_is_won');
          } else if ((widget.job?.statusNameEN ?? '') == 'Job is done') {
            offerStatus = tr('job_is_done');
          } else if ((widget.job?.statusNameEN ?? '') == 'Job Lost') {
            offerStatus = tr('job_lost');
          } else if ((widget.job?.statusNameEN ?? '') == 'Invitation sent') {
            offerStatus = tr('invitation_sent');
          } else {
            offerStatus = isDK
                ? (widget.job?.status ?? [])
                        .firstWhere(
                          (element) =>
                              element.id == (widget.job?.statusId ?? 0),
                          orElse: () => JobStatus.defaults(),
                        )
                        .titleDk ??
                    ''
                : (widget.job?.status ?? [])
                        .firstWhere(
                          (element) =>
                              element.id == (widget.job?.statusId ?? 0),
                          orElse: () => JobStatus.defaults(),
                        )
                        .titleEn ??
                    '';
          }

          if (widget.job?.statusId == 1 || widget.job?.statusId == 2) {
            projectDeadlineOffer = widget.job?.offerExpireDate ?? '';
            projectDeadlineOfferColorCode =
                widget.job?.offerExpireDateColorCode ?? '';
            if (projectDeadlineOffer.isNotEmpty) {
              tenderVm.formatDate(projectDeadlineOffer);
              final offerDueDate =
                  DateTime.parse(tenderVm.projectDeadlineOffer);
              differenceInDays = tenderVm.calculateDifference(offerDueDate);
            }
            if (projectDeadlineOfferColorCode.isNotEmpty) {
              projectDeadlineOfferColor = tenderVm
                  .projectDeadlineOfferColor(projectDeadlineOfferColorCode);
            }
          }
        } else {
          customerName =
              '${widget.oldJob?.firstName ?? ''} ${widget.oldJob?.lastName ?? ''}';
          title = widget.oldJob?.title ?? '';
          offerStatus = tr(context
              .read<TenderFolderViewModel>()
              .oldtilbudStatus
              .firstWhere(
                  (element) =>
                      element.value == (widget.oldJob?.tilbudStatus ?? 0),
                  orElse: () => OldTilbudStatus.defaults())
              .text);
        }

        return InkWell(
          onTap: () => jobDetailDialog(
              context: context,
              job: widget.job,
              jobIndex: widget.jobIndex,
              oldJob: widget.oldJob),
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
              side: BorderSide(
                color: PartnerAppColors.darkBlue.withValues(alpha: .2),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          SvgPicture.asset(
                            SvgIcons.calendarCheck,
                            width: 26,
                          ),
                          if (context
                                  .read<AppDrawerViewModel>()
                                  .supplierProfileModel
                                  ?.bCrmCompany
                                  ?.deadlineOffer ==
                              1)
                            if (projectDeadlineOffer.isNotEmpty)
                              if (widget.job?.statusId == 1 ||
                                  widget.job?.statusId == 2) ...[
                                SmartGaps.gapH5,
                                Text(
                                  differenceInDays < 0
                                      ? tr('awaiting_offer')
                                      : differenceInDays == 0
                                          ? tr('due_today')
                                          : '$differenceInDays ${tr('days_left')}',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineLarge!
                                      .copyWith(
                                          fontSize: 12,
                                          fontWeight: FontWeight.normal,
                                          color: projectDeadlineOfferColor),
                                ),
                              ]
                        ],
                      ),
                      SmartGaps.gapW10,
                      Flexible(
                        child: Text(
                          offerStatus,
                          style: context.pttBodySmall.copyWith(
                            fontSize: 11,
                            color: PartnerAppColors.darkBlue,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      )
                    ],
                  ),
                  SmartGaps.gapH15,
                  Text(
                    customerName,
                    style: context.pttBodySmall.copyWith(
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SmartGaps.gapH5,
                  Skeleton.leaf(
                    child: Divider(
                      color: Colors.black.withValues(alpha: 0.5),
                      thickness: 0.8,
                    ),
                  ),
                  SmartGaps.gapH5,
                  Expanded(
                    child: Text(
                      title,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                      style: context.pttBodySmall.copyWith(
                        color: const Color(0xff134553),
                        height: 1.5,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
