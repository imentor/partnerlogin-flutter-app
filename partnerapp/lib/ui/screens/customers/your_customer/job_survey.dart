import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

Future<String?> jobSurveyDialog({
  required BuildContext context,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          GestureDetector(
            onTap: () => Navigator.of(dialogContext).pop(),
            child: Icon(
              Icons.close,
              size: 18,
              color: Colors.black.withValues(alpha: 0.4),
            ),
          )
        ]),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: const SingleChildScrollView(
            child: JobSurvey(),
          ),
        ),
      );
    },
  );
}

class JobSurvey extends StatefulWidget {
  const JobSurvey({super.key});

  @override
  State<JobSurvey> createState() => _JobSurveyState();
}

class _JobSurveyState extends State<JobSurvey> {
  final formKey = GlobalKey<FormBuilderState>();
  @override
  Widget build(BuildContext context) {
    return FormBuilder(
        key: formKey,
        child: Column(
          children: [
            Text(
              tr('reminder_survey_description'),
              style: context.pttTitleSmall.copyWith(
                color: PartnerAppColors.darkBlue,
              ),
            ),
            CustomRadioGroupFormBuilder(
              options: [
                FormBuilderFieldOption(
                  value: tr('reminder_survey_one'),
                  child: Text(
                    tr('reminder_survey_one'),
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                          color: PartnerAppColors.darkBlue,
                          fontWeight: FontWeight.normal,
                        ),
                  ),
                ),
                FormBuilderFieldOption(
                  value: tr('reminder_survey_two'),
                  child: Text(
                    tr('reminder_survey_two'),
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                          color: PartnerAppColors.darkBlue,
                          fontWeight: FontWeight.normal,
                        ),
                  ),
                ),
                FormBuilderFieldOption(
                  value: tr('reminder_survey_three'),
                  child: Text(
                    tr('reminder_survey_three'),
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                          color: PartnerAppColors.darkBlue,
                          fontWeight: FontWeight.normal,
                        ),
                  ),
                )
              ],
              name: 'reminder',
              validator:
                  FormBuilderValidators.required(errorText: tr('required')),
            ),
            SmartGaps.gapH20,
            Row(
              children: [
                Expanded(
                  child: CustomDesignTheme.flatButtonStyle(
                    height: 50,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                        side:
                            const BorderSide(color: PartnerAppColors.darkBlue)),
                    onPressed: () => Navigator.of(context).pop(),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            tr('back'),
                            style: Theme.of(context)
                                .textTheme
                                .displayLarge!
                                .copyWith(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400,
                                  color: PartnerAppColors.darkBlue,
                                ),
                          )
                        ]),
                  ),
                ),
                SmartGaps.gapW10,
                Expanded(
                    child: CustomDesignTheme.flatButtonStyle(
                  height: 50,
                  backgroundColor: PartnerAppColors.malachite,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  onPressed: () async {
                    if (formKey.currentState!.validate()) {
                      Navigator.of(context)
                          .pop(formKey.currentState!.fields['reminder']!.value);
                    }
                  },
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          tr('confirm'),
                          style: Theme.of(context)
                              .textTheme
                              .displayLarge!
                              .copyWith(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                                color: Colors.white,
                              ),
                        )
                      ]),
                ))
              ],
            )
          ],
        ));
  }
}
