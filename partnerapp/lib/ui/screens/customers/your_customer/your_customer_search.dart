import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class YourCustomerSearch extends StatelessWidget {
  const YourCustomerSearch({
    super.key,
    this.onChanged,
    this.onClear,
  });

  final Function(String)? onChanged;
  final Function()? onClear;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: context.select<ClientsViewModel, TextEditingController>(
          (value) => value.searchController ?? TextEditingController()),
      focusNode: context.select<ClientsViewModel, FocusNode>(
          (value) => value.searchFocusNode ?? FocusNode()),
      style: context.pttTitleMedium,
      onChanged: onChanged,
      decoration: InputDecoration(
        hintStyle:
            context.pttTitleMedium.copyWith(color: PartnerAppColors.grey),
        hintText: tr('search'),
        suffixIcon:
            context.watch<ClientsViewModel>().searchController?.text.isEmpty ??
                    false
                ? const Icon(Icons.search, size: 30)
                : GestureDetector(
                    onTap: onClear, child: const Icon(Icons.clear, size: 30)),
        contentPadding:
            const EdgeInsets.symmetric(vertical: 13, horizontal: 15),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: const BorderSide(
            color: PartnerAppColors.grey,
          ),
        ),
      ),
    );
  }
}
