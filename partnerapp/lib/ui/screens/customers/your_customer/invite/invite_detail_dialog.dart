import 'package:Haandvaerker.dk/model/invitations/subcontractor_invitation_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/launcher.dart';
import 'package:Haandvaerker.dk/utils/string_utils.dart';
import 'package:Haandvaerker.dk/viewmodel/contract/contract_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/invitation_subcontractor_manufacturer/invitation_subcontractor_manufacturer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

Future<void> inviteDetailDialog({
  required BuildContext context,
  required SubcontractorInvitation invitation,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: InviteDetailDialog(
              invitation: invitation,
            ),
          ),
        ),
      );
    },
  );
}

class InviteDetailDialog extends StatelessWidget {
  const InviteDetailDialog({super.key, required this.invitation});

  final SubcontractorInvitation invitation;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SvgPicture.asset(
          SvgIcons.calendarCheck,
          width: 40,
          height: 40,
        ),
        SmartGaps.gapH10,
        Text(
          invitation.companyInfo?.company ?? '',
          style: context.pttTitleMedium.copyWith(
            fontWeight: FontWeight.w600,
          ),
        ),
        SmartGaps.gapH20,
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset('assets/images/location.svg', width: 13),
            SmartGaps.gapW10,
            GestureDetector(
              onTap: () async {},
              child: Text(
                invitation.projectAddress ?? '',
                style: context.pttBodySmall.copyWith(
                  color: PartnerAppColors.darkBlue,
                ),
              ),
            )
          ],
        ),
        SmartGaps.gapH15,
        GestureDetector(
          onTap: () {
            Launcher().launchEmail(invitation.companyEmail);
          },
          child: Row(
            children: [
              SvgPicture.asset(
                'assets/images/mail-blue.svg',
                width: 15,
              ),
              SmartGaps.gapW10,
              Text(
                tr('send_email'),
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      height: 1,
                      color: PartnerAppColors.darkBlue,
                    ),
              ),
            ],
          ),
        ),
        SmartGaps.gapH15,
        GestureDetector(
          onTap: () async {
            Launcher().launchPhoneCall(invitation.companyPhone);
            Navigator.of(context).pop();
          },
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset(
                'assets/images/phone-contact.svg',
                width: 13,
              ),
              SmartGaps.gapW10,
              Text(
                '+45 ${invitation.companyPhone}',
                style: context.pttBodySmall.copyWith(
                  color: PartnerAppColors.darkBlue,
                ),
              ),
            ],
          ),
        ),
        SmartGaps.gapH30,
        Text(
          invitation.projectName ?? '',
          style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                fontSize: 18,
                fontWeight: FontWeight.w500,
                height: 1,
              ),
        ),
        SmartGaps.gapH10,
        Divider(
          color: Theme.of(context)
              .colorScheme
              .onTertiaryContainer
              .withValues(alpha: 0.4),
          thickness: 1,
        ),
        SmartGaps.gapH10,
        CustomDesignTheme.flatButtonStyle(
          height: 50,
          onPressed: () async {
            context.read<ContractViewModel>().contractDetailsToDefault();
            context
                .read<InvitationSubcontractorManufacturerViewmodel>()
                .getHeaders();

            if ((invitation.project ?? '').isNotEmpty) {
              context.read<MyProjectsViewModel>().currentProjectId =
                  int.parse(invitation.project!);

              Navigator.pop(context);

              changeDrawerRoute(Routes.projectDashboard, arguments: {
                'projectId': int.parse(invitation.project!),
                'pageRedirect': 'wallet_subcontractor'
              });
            }
          },
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(2),
            side: BorderSide(
              color: Theme.of(context).colorScheme.primary,
            ),
          ),
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text(
              tr('byggewallet').toTitleCase(),
              style: context.pttBodySmall.copyWith(
                fontWeight: FontWeight.w600,
                color: PartnerAppColors.accentBlue,
                letterSpacing: 0.5,
              ),
            )
          ]),
        ),
        SmartGaps.gapH10,
        CustomDesignTheme.flatButtonStyle(
          height: 50,
          onPressed: () async {
            Navigator.pop(context);

            changeDrawerRoute(Routes.createProjectMessage, arguments: {
              'customerId': invitation.company,
            });
          },
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(2),
            side: BorderSide(
              color: Theme.of(context).colorScheme.primary,
            ),
          ),
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text(
              tr('message')..toTitleCase(),
              style: context.pttBodySmall.copyWith(
                fontWeight: FontWeight.w600,
                color: PartnerAppColors.accentBlue,
                letterSpacing: 0.5,
              ),
            )
          ]),
        ),
      ],
    );
  }
}
