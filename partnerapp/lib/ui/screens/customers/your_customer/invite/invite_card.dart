import 'package:Haandvaerker.dk/model/invitations/subcontractor_invitation_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/invite/invite_detail_dialog.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:skeletonizer/skeletonizer.dart';

class InviteCard extends StatelessWidget {
  const InviteCard({super.key, required this.invitation});

  final SubcontractorInvitation invitation;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => inviteDetailDialog(context: context, invitation: invitation),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
          side: BorderSide(
            color: PartnerAppColors.darkBlue.withValues(alpha: .2),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SvgPicture.asset(
                    SvgIcons.calendarCheck,
                    width: 26,
                  ),
                  Text(
                    'Vandt Opgaven',
                    style: context.pttBodySmall.copyWith(
                      fontSize: 11,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.w600,
                    ),
                  )
                ],
              ),
              SmartGaps.gapH15,
              Text(
                invitation.companyInfo?.company ?? '',
                style: context.pttBodySmall.copyWith(
                  color: PartnerAppColors.darkBlue,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SmartGaps.gapH5,
              Skeleton.leaf(
                child: Divider(
                  color: Colors.black.withValues(alpha: 0.5),
                  thickness: 0.8,
                ),
              ),
              SmartGaps.gapH5,
              Expanded(
                child: Text(
                  invitation.projectName ?? '',
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                  style: context.pttBodySmall.copyWith(
                    color: const Color(0xff134553),
                    height: 1.5,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
