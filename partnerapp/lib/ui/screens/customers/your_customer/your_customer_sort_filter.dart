library mine_kunder_sort_filter;

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

part 'components/sort_jobs.dart';
part 'components/yearly_filter.dart';
part 'components/type_filter.dart';

class YourCustomerSortFilter extends StatelessWidget {
  const YourCustomerSortFilter({super.key});

  @override
  Widget build(BuildContext context) {
    return const Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        YearlyFilter(),
        Spacer(),
        SortJobs(),
        SmartGaps.gapW33,
        TypeFilter(),
      ],
    );
  }
}
