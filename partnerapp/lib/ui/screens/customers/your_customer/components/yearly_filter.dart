part of '../your_customer_sort_filter.dart';

class YearlyFilter extends StatefulWidget {
  const YearlyFilter({super.key});

  @override
  YearlyFilterState createState() => YearlyFilterState();
}

class YearlyFilterState extends State<YearlyFilter> {
  List<int> yearList = [];

  @override
  void initState() {
    super.initState();

    int minYear = 2018;
    int maxYear = DateTime.now().year;

    for (int year = maxYear; year >= minYear; year--) {
      yearList.add(year);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ClientsViewModel>(
      builder: (context, clientsVm, _) {
        return SizedBox(
          width: 145,
          child: DropdownButtonFormField2(
            value: clientsVm.customerYearFilter,
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.fromLTRB(20, 12, 15, 12),
              suffixIcon: clientsVm.customerYearFilter != null
                  ? InkWell(
                      onTap: () async {
                        clientsVm.changeCustomerYearFilter(null);
                        clientsVm.searchController!.clear();
                        clientsVm.getYourCustomerData = true;
                        clientsVm.currentPage = 1;
                        await tryCatchWrapper(
                          context: myGlobals.homeScaffoldKey!.currentContext,
                          function: clientsVm.getPartnerJobsV2(),
                        );
                        clientsVm.getYourCustomerData = false;
                      },
                      child: const Icon(
                        FeatherIcons.xCircle,
                        size: 18,
                        color: PartnerAppColors.darkBlue,
                      ),
                    )
                  : null,
              border: OutlineInputBorder(
                borderSide: const BorderSide(
                  color: Color(0xff707070),
                  width: 0.6,
                ),
                borderRadius: BorderRadius.circular(3),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  color: Color(0xff707070),
                  width: 0.6,
                ),
                borderRadius: BorderRadius.circular(3),
              ),
            ),
            items: yearList.map((year) {
              return DropdownMenuItem(
                value: year,
                child: Text(
                  year.toString(),
                  style: context.pttTitleLarge,
                ),
              );
            }).toList(),
            onChanged: (dynamic year) async {
              clientsVm.changeCustomerYearFilter(year!);
              clientsVm.searchController!.clear();
              clientsVm.getYourCustomerData = true;
              clientsVm.currentPage = 1;
              await tryCatchWrapper(
                context: myGlobals.homeScaffoldKey!.currentContext,
                function: clientsVm.getPartnerJobsV2(),
              );
              clientsVm.getYourCustomerData = false;
            },
            buttonStyleData: const ButtonStyleData(
              padding: EdgeInsets.symmetric(horizontal: 0),
              width: 0,
            ),
            iconStyleData: const IconStyleData(
              icon: Icon(Icons.arrow_drop_down, color: Colors.black),
              iconSize: 30,
            ),
            dropdownStyleData: DropdownStyleData(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
              ),
              offset: const Offset(0, -10),
            ),
          ),
        );
      },
    );
  }
}
