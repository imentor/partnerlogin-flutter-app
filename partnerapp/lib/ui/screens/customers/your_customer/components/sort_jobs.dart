part of '../your_customer_sort_filter.dart';

class SortJobs extends StatelessWidget {
  const SortJobs({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        PopupMenuButton(
          position: PopupMenuPosition.under,
          itemBuilder: (_) {
            return [const PopupMenuItem(enabled: false, child: SortJobsMenu())];
          },
          offset: const Offset(0, 20),
          child: SvgPicture.asset('assets/images/sort.svg'),
        ),
        SmartGaps.gapH5,
        Text(tr('sort'),
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.normal,
                fontSize: 14))
      ],
    );
  }
}

class SortJobsMenu extends StatefulWidget {
  const SortJobsMenu({super.key});

  @override
  SortJobsMenuState createState() => SortJobsMenuState();
}

class SortJobsMenuState extends State<SortJobsMenu> {
  @override
  Widget build(BuildContext context) {
    return Consumer<ClientsViewModel>(builder: (_, vm, __) {
      return Column(
        children: [
          CustomCheckboxGroupFormBuilder(
            options: [
              FormBuilderFieldOption(
                value: 'createdDesc',
                child: Text(
                  tr('sort_by_meeting_date'),
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 14),
                ),
              ),
              FormBuilderFieldOption(
                value: 'statusDesc',
                child: Text(
                  tr('sort_by_status'),
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 14),
                ),
              ),
              FormBuilderFieldOption(
                value: 'offerCreatedDesc',
                child: Text(
                  tr('sort_by_sent_quote_date'),
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 14),
                ),
              ),
              // FormBuilderFieldOption(
              //   value: 'sort_by_follow_up_date',
              //   child: Text(
              //     tr('sort_by_follow_up_date'),
              //     style: Theme.of(context).textTheme.titleMedium!.copyWith(
              //         color: PartnerAppColors.darkBlue,
              //         fontWeight: FontWeight.normal,
              //         fontSize: 14),
              //   ),
              // ),
            ],
            name: 'sortMenu',
            optionsOrientation: OptionsOrientation.vertical,
            initialValue: [(vm.sort)],
            onChanged: (p0) async {
              if (p0 != null && p0.isNotEmpty) {
                Navigator.of(context).pop();
                vm.sort = p0.last;

                vm.currentPage = 1;
                vm.getYourCustomerData = true;

                await vm.getPartnerJobsV2().then((value) {
                  vm.getYourCustomerData = false;
                });
              } else {
                Navigator.of(context).pop();
                vm.sort = '';

                vm.currentPage = 1;
                vm.getYourCustomerData = true;

                await vm.getPartnerJobsV2().then((value) {
                  vm.getYourCustomerData = false;
                });
              }
            },
          ),
        ],
      );
    });
  }
}
