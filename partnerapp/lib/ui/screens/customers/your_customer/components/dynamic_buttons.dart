import 'dart:developer';

import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/view_offer/see_offer_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/view_offer/see_offer_list_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/components/cannot_submit_popup.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/job_upload_files.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/upload_offer/create_upload_offer/upload_offer_page.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/upload_offer/view_uploaded_offer/view_uploaded_offer.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/utils/debouncer.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/contract/contract_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/kanikke_settings_response_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/see_offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class JobWonButtons extends StatelessWidget {
  const JobWonButtons({super.key, required this.partnerJob});
  final PartnerJobModel? partnerJob;

  @override
  Widget build(BuildContext context) {
    return Consumer3<CreateOfferViewModel, OfferViewModel, AppDrawerViewModel>(
        builder: (_, createOfferVm, offerVm, appDrawerVm, __) {
      final companyUploadOffer =
          appDrawerVm.bitrixCompanyModel?.companyUploadOffer;
      final enableEditOffer = appDrawerVm.bitrixCompanyModel?.enableEditOffer;
      final offerFileAndTypeBool = (partnerJob?.offerFile != null ||
          (partnerJob?.offerFile ?? '').isNotEmpty ||
          partnerJob?.offerType == 'fast-offer');
      final canUploadOffer = (companyUploadOffer ?? 0) == 1;

      return Column(
        children: [
          CustomDesignTheme.flatButtonStyle(
            height: 50,
            onPressed: () =>
                jobUploadFilesDialog(context: context, job: partnerJob!),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(2),
              side: BorderSide(
                color: Theme.of(context).colorScheme.primary,
              ),
            ),
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Text(
                'Upload dokumentation til Håndværker.dk',
                style: context.pttBodySmall.copyWith(
                  fontWeight: FontWeight.w600,
                  color: PartnerAppColors.accentBlue,
                  letterSpacing: 0.5,
                ),
              )
            ]),
          ),
          const SizedBox(
            height: 10,
          ),
          CustomDesignTheme.flatButtonStyle(
            height: 50,
            onPressed: () => makeOfferFunction(
                context: context,
                enableEditOffer: enableEditOffer ?? 0,
                canUploadOffer: canUploadOffer,
                offerFileAndTypeBool: offerFileAndTypeBool,
                createOfferVm: createOfferVm,
                offerVm: offerVm),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(2),
              side: BorderSide(
                color: Theme.of(context).colorScheme.primary,
              ),
            ),
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Text(
                'Afgiv tilbud',
                style: context.pttBodySmall.copyWith(
                  fontWeight: FontWeight.w600,
                  color: PartnerAppColors.accentBlue,
                  letterSpacing: 0.5,
                ),
              )
            ]),
          ),
        ],
      );
    });
  }

  Future<void> makeOfferFunction({
    required BuildContext context,
    required int enableEditOffer,
    required bool canUploadOffer,
    required bool offerFileAndTypeBool,
    required CreateOfferViewModel createOfferVm,
    required OfferViewModel offerVm,
  }) async {
    if (enableEditOffer == 1) {
      if (partnerJob?.offerSeen == true) {
        if (canUploadOffer) {
          chooseOfferDialog(
              context: context,
              createOffer: () {
                createOfferVm.toUpdateOffer = false;
                createOfferVm.isNewOfferVersion = true;

                if (context.mounted) {
                  Navigator.pop(context);
                  changeDrawerRoute(Routes.createOffer,
                      arguments: {'job': partnerJob});
                }
              },
              uploadOffer: () {
                offerVm.isUploadOfferUpdate = false;
                offerVm.isNewOfferVersion = true;
                Navigator.pop(context);
                changeDrawerRoute(Routes.uploadOffer, arguments: partnerJob);
              });
        }
      } else {
        editOfferDialog(context: context, partnerJob: partnerJob);
      }
    } else {
      if (!offerFileAndTypeBool) {
        createOfferVm.toUpdateOffer = true;

        if (context.mounted) {
          Navigator.pop(context);
          changeDrawerRoute(Routes.createOffer, arguments: {'job': partnerJob});
        }
      }
    }
  }
}

class OfferAcceptedButtons extends StatelessWidget {
  const OfferAcceptedButtons({super.key, required this.partnerJob});
  final PartnerJobModel? partnerJob;

  @override
  Widget build(BuildContext context) {
    return CustomDesignTheme.flatButtonStyle(
      height: 50,
      onPressed: () async {
        context.read<ContractViewModel>().contractDetailsToDefault();

        if (partnerJob != null && partnerJob?.projectId != null) {
          context.read<MyProjectsViewModel>().currentProjectId =
              partnerJob?.projectId;

          Navigator.pop(context);

          changeDrawerRoute(Routes.projectDashboard,
              arguments: {'projectId': partnerJob?.projectId});
        }
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(2),
        side: BorderSide(
          color: Theme.of(context).colorScheme.primary,
        ),
      ),
      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Text(
          tr('manage_project').toUpperCase(),
          style: context.pttBodySmall.copyWith(
            fontWeight: FontWeight.w600,
            color: PartnerAppColors.accentBlue,
            letterSpacing: 0.5,
          ),
        )
      ]),
    );
  }
}

class JobLostButtons extends StatelessWidget {
  const JobLostButtons({super.key, required this.partnerJob});
  final PartnerJobModel? partnerJob;

  @override
  Widget build(BuildContext context) {
    final appDrawerVm = context.read<AppDrawerViewModel>();
    final seeOfferVm = context.read<SeeOfferViewModel>();
    final enableEditOffer = appDrawerVm.bitrixCompanyModel?.enableEditOffer;
    final offerFileAndTypeBool = (partnerJob?.offerFile != null ||
        (partnerJob?.offerFile ?? '').isNotEmpty ||
        partnerJob?.offerType == 'fast-offer');

    if (partnerJob?.offerStatus == null) {
      return CustomDesignTheme.flatButtonStyle(
        height: 50,
        backgroundColor: PartnerAppColors.grey1,
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          SvgPicture.asset('assets/images/offers.svg',
              colorFilter:
                  const ColorFilter.mode(Colors.white, BlendMode.srcIn),
              width: 15),
          SmartGaps.gapW10,
          Text(
            tr('make_offer'),
            style: context.pttBodySmall.copyWith(
              fontWeight: FontWeight.w600,
              color: Colors.white,
              letterSpacing: 0.5,
            ),
          ),
        ]),
      );
    }

    return CustomDesignTheme.flatButtonStyle(
      height: 50,
      onPressed: () async {
        seeOfferVm.activeJob = partnerJob;
        if ((enableEditOffer ?? 0) == 1) {
          Navigator.pop(context);
          seeOfferListDialog(context: context, partnerJob: partnerJob);
        } else {
          if (!offerFileAndTypeBool) {
            await showDialog(
                barrierDismissible: false,
                context: context,
                builder: (context) => SeeOffer(
                    contactId: partnerJob?.customerId,
                    projectId: partnerJob?.projectParentId != null
                        ? int.parse(partnerJob?.projectParentId)
                        : partnerJob?.projectId));
          } else {
            viewUploadedOffer(context: context, offerId: partnerJob?.offerId);
          }
        }
        // if (partnerJob?.offerType == 'fast-offer') {
        //   viewUploadedOffer(context: context, offerId: partnerJob?.offerId);
        // } else {
        //   seeOfferVm.activeJob = partnerJob;
        //   await showDialog(
        //     barrierDismissible: false,
        //     context: context,
        //     builder: (context) => SeeOffer(
        //       contactId: partnerJob?.customerId,
        //       projectId: partnerJob?.projectParentId != null
        //           ? int.parse(partnerJob?.projectParentId)
        //           : partnerJob?.projectId,
        //     ),
        //   );
        // }
      },
      backgroundColor: Theme.of(context).colorScheme.onSurfaceVariant,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            tr('see_offer'),
            style: context.pttBodySmall.copyWith(
              fontWeight: FontWeight.w600,
              color: Colors.white,
              letterSpacing: 0.5,
            ),
          ),
        ],
      ),
    );
  }
}

class NewJobButtons extends StatelessWidget {
  NewJobButtons({super.key, required this.partnerJob});
  final PartnerJobModel? partnerJob;

  final Debouncer debouncer = Debouncer(milliseconds: 500);

  @override
  Widget build(BuildContext context) {
    final appDrawerVm = context.read<AppDrawerViewModel>();
    final createOfferVm = context.read<CreateOfferViewModel>();
    final kanikkeVm = context.read<KanIkkeSettingsResponseViewModel>();
    final offerVm = context.read<OfferViewModel>();
    final kanIkkeTilbud = appDrawerVm.bitrixCompanyModel?.kanIkkeTilbud;
    final companyUploadOffer =
        appDrawerVm.bitrixCompanyModel?.companyUploadOffer;

    return CustomDesignTheme.flatButtonStyle(
      height: 50,
      backgroundColor: Theme.of(context).colorScheme.onSurfaceVariant,
      onPressed: () async {
        debouncer.run(() async {
          final callFirstTxt = kanikkeVm.callFirstText;
          final canUploadOffer = (companyUploadOffer ?? 0) == 1;

          await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey?.currentContext ?? context,
            function: Future.value(appDrawerVm.getPrisenResultV2()),
          ).whenComplete(() async {
            if (!context.mounted) return;

            // Checks if the "no call" feature is enabled or not
            if ((appDrawerVm.bitrixCompanyModel?.companyVundetOpgaven ?? 0) ==
                1) {
              onMakeOffer(
                  context: context,
                  canUploadOffer: canUploadOffer,
                  createOfferVm: createOfferVm,
                  offerVm: offerVm);
            } else {
              if ((appDrawerVm.bitrixCompanyModel?.companyTrackingPhoneEmail ??
                      0) ==
                  1) {
                if ((kanIkkeTilbud ?? 0) == 1) {
                  if (partnerJob?.callDate != null ||
                      (partnerJob?.offerType ?? '') == 'fastcontract' ||
                      (partnerJob?.statusId ?? 0) > 3) {
                    onMakeOffer(
                        context: context,
                        canUploadOffer: canUploadOffer,
                        createOfferVm: createOfferVm,
                        offerVm: offerVm);
                  } else {
                    if (applic.env == 'master') {
                      onMakeOffer(
                          context: context,
                          canUploadOffer: canUploadOffer,
                          createOfferVm: createOfferVm,
                          offerVm: offerVm);
                    } else {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return PopScope(
                                canPop: false,
                                child: CannotSubmitPopup(callFirstTxt));
                          });
                    }
                  }
                } else {
                  if (partnerJob?.callDate != null) {
                    onMakeOffer(
                        context: context,
                        canUploadOffer: canUploadOffer,
                        createOfferVm: createOfferVm,
                        offerVm: offerVm);
                  } else {
                    if (applic.env == 'master') {
                      onMakeOffer(
                          context: context,
                          canUploadOffer: canUploadOffer,
                          createOfferVm: createOfferVm,
                          offerVm: offerVm);
                    } else {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return PopScope(
                                canPop: false,
                                child: CannotSubmitPopup(callFirstTxt));
                          });
                    }
                  }
                }
              } else {
                onMakeOffer(
                    context: context,
                    canUploadOffer: canUploadOffer,
                    createOfferVm: createOfferVm,
                    offerVm: offerVm);
              }
            }
          });
        });
      },
      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        SvgPicture.asset('assets/images/offers.svg',
            colorFilter: const ColorFilter.mode(Colors.white, BlendMode.srcIn),
            width: 15),
        SmartGaps.gapW10,
        Text(
          tr('make_offer'),
          style: context.pttBodySmall.copyWith(
            fontWeight: FontWeight.w600,
            color: Colors.white,
            letterSpacing: 0.5,
          ),
        ),
      ]),
    );
  }

  void onMakeOffer(
      {required BuildContext context,
      required bool canUploadOffer,
      required CreateOfferViewModel createOfferVm,
      required OfferViewModel offerVm}) {
    if (canUploadOffer) {
      chooseOfferDialog(
          context: context,
          createOffer: () {
            createOfferVm.toUpdateOffer = false;
            createOfferVm.isNewOfferVersion = false;

            if (context.mounted) {
              Navigator.pop(context);
              changeDrawerRoute(
                Routes.createOffer,
                arguments: {
                  'job': partnerJob,
                },
              );
            }
          },
          uploadOffer: () {
            offerVm.isUploadOfferUpdate = false;
            offerVm.isNewOfferVersion = false;
            Navigator.pop(context);
            changeDrawerRoute(Routes.uploadOffer, arguments: partnerJob);
          });
    } else {
      createOfferVm.toUpdateOffer = false;
      createOfferVm.isNewOfferVersion = false;

      if (context.mounted) {
        Navigator.pop(context);
        changeDrawerRoute(
          Routes.createOffer,
          arguments: {
            'job': partnerJob,
          },
        );
      }
    }
  }
}

class PendingStatusButtons extends StatelessWidget {
  const PendingStatusButtons({super.key, required this.partnerJob});
  final PartnerJobModel? partnerJob;

  @override
  Widget build(BuildContext context) {
    final appDrawerVm = context.read<AppDrawerViewModel>();
    final createOfferVm = context.read<CreateOfferViewModel>();
    final offerVm = context.read<OfferViewModel>();
    final companyUploadOffer =
        appDrawerVm.bitrixCompanyModel?.companyUploadOffer;
    final enableEditOffer = appDrawerVm.bitrixCompanyModel?.enableEditOffer;
    final offerFileAndTypeBool = (partnerJob?.offerFile != null ||
        (partnerJob?.offerFile ?? '').isNotEmpty ||
        partnerJob?.offerType == 'fast-offer');
    final alertStatus = partnerJob?.alertStatus ?? '';
    final alertStage = partnerJob?.alertStage ?? '';

    return Row(
      children: [
        Expanded(
          child: CustomDesignTheme.flatButtonStyle(
            height: 50,
            onPressed: () async {
              final seeOfferVm = context.read<SeeOfferViewModel>();

              if ((enableEditOffer ?? 0) == 1) {
                Navigator.pop(context);

                seeOfferVm.activeJob = partnerJob;
                seeOfferListDialog(context: context, partnerJob: partnerJob);
              } else {
                seeOfferVm.activeJob = partnerJob;
                if (!offerFileAndTypeBool) {
                  await showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (context) => SeeOffer(
                            contactId: partnerJob?.customerId,
                            projectId: partnerJob?.projectParentId != null
                                ? int.parse(partnerJob?.projectParentId)
                                : partnerJob?.projectId,
                          ));
                } else {
                  viewUploadedOffer(
                      context: context, offerId: partnerJob?.offerId);
                }
              }
            },
            backgroundColor: Theme.of(context).colorScheme.onSurfaceVariant,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  tr('see_offer'),
                  style: context.pttBodySmall.copyWith(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    letterSpacing: 0.5,
                  ),
                ),
              ],
            ),
          ),
        ),
        SmartGaps.gapW10,
        Expanded(
          child: CustomDesignTheme.flatButtonStyle(
            height: 50,
            onPressed: () {
              final canUploadOffer = (companyUploadOffer ?? 0) == 1;
              if ((alertStage == 'acceptable' ||
                      alertStage == 'warning' ||
                      alertStage == 'alert') &&
                  alertStatus == 'pending') {
                // can NOT make offer and can NOT kanikke
                return;
              } else if ((alertStage == 'restricted' &&
                  alertStatus == 'pending')) {
                // can make offer but can NOT kanikke
                makeOfferFunction(
                    context: context,
                    enableEditOffer: enableEditOffer ?? 0,
                    canUploadOffer: canUploadOffer,
                    offerFileAndTypeBool: offerFileAndTypeBool,
                    createOfferVm: createOfferVm,
                    offerVm: offerVm);
              } else if (alertStatus != 'pending') {
                makeOfferFunction(
                    context: context,
                    enableEditOffer: enableEditOffer ?? 0,
                    canUploadOffer: canUploadOffer,
                    offerFileAndTypeBool: offerFileAndTypeBool,
                    createOfferVm: createOfferVm,
                    offerVm: offerVm);
              }
            },
            backgroundColor:
                (((enableEditOffer ?? 0) == 0 && offerFileAndTypeBool) ||
                        alertStatus == 'pending')
                    ? PartnerAppColors.spanishGrey
                    : Theme.of(context).colorScheme.onSurfaceVariant,
            splashFactory:
                (((enableEditOffer ?? 0) == 0 && offerFileAndTypeBool) ||
                        alertStatus == 'pending')
                    ? NoSplash.splashFactory
                    : null,
            child: Text(
              ((partnerJob?.offerSeen == false &&
                          (enableEditOffer ?? 0) == 1) ||
                      (enableEditOffer ?? 0) == 0)
                  ? tr('manage_offers')
                  : tr('make_new_offer'),
              style: context.pttBodySmall.copyWith(
                fontWeight: FontWeight.w600,
                color: Colors.white,
                letterSpacing: 0.5,
              ),
            ),
          ),
        )
      ],
    );
  }

  Future<void> makeOfferFunction({
    required BuildContext context,
    required int enableEditOffer,
    required bool canUploadOffer,
    required bool offerFileAndTypeBool,
    required CreateOfferViewModel createOfferVm,
    required OfferViewModel offerVm,
  }) async {
    if (enableEditOffer == 1) {
      if (partnerJob?.offerSeen == true) {
        if (canUploadOffer) {
          chooseOfferDialog(
              context: context,
              createOffer: () {
                createOfferVm.toUpdateOffer = false;
                createOfferVm.isNewOfferVersion = true;

                if (context.mounted) {
                  Navigator.pop(context);
                  changeDrawerRoute(Routes.createOffer,
                      arguments: {'job': partnerJob});
                }
              },
              uploadOffer: () {
                offerVm.isUploadOfferUpdate = false;
                offerVm.isNewOfferVersion = true;
                Navigator.pop(context);
                changeDrawerRoute(Routes.uploadOffer, arguments: partnerJob);
              });
        }
      } else {
        editOfferDialog(context: context, partnerJob: partnerJob);
      }
    } else {
      if (!offerFileAndTypeBool) {
        createOfferVm.toUpdateOffer = true;

        if (context.mounted) {
          Navigator.pop(context);
          changeDrawerRoute(Routes.createOffer, arguments: {'job': partnerJob});
        }
      }
    }
  }
}

Future<void> editOfferDialog(
    {required BuildContext context,
    required PartnerJobModel? partnerJob}) async {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            InkWell(
                onTap: () {
                  Navigator.of(dialogContext).pop();
                },
                child: const Icon(
                  FeatherIcons.x,
                  color: PartnerAppColors.spanishGrey,
                )),
          ],
        ),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
              child: EditOfferDialog(partnerJob: partnerJob)),
        ),
      );
    },
  );
}

class EditOfferDialog extends StatelessWidget {
  const EditOfferDialog({super.key, this.partnerJob});
  final PartnerJobModel? partnerJob;

  @override
  Widget build(BuildContext context) {
    final appDrawerVm = context.read<AppDrawerViewModel>();
    final createOfferVm = context.read<CreateOfferViewModel>();
    final offerVm = context.read<OfferViewModel>();
    final companyUploadOffer =
        appDrawerVm.bitrixCompanyModel?.companyUploadOffer;
    final offerFileAndTypeBool = (((partnerJob?.offerFile != null) &&
            ((partnerJob?.offerFile ?? '').isNotEmpty)) ||
        partnerJob?.offerType == 'fast-offer');
    return Column(
      children: [
        Text(tr('customer_not_seen_text'),
            textAlign: TextAlign.center,
            style: Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(color: Colors.black, fontSize: 18)),
        SmartGaps.gapH20,
        CustomDesignTheme.flatButtonStyle(
          height: 50,
          width: double.infinity,
          backgroundColor: Theme.of(context).colorScheme.onSurfaceVariant,
          child: Text(
            tr('manage_offers'),
            style: context.pttBodySmall.copyWith(
              fontWeight: FontWeight.w600,
              fontSize: 18,
              color: Colors.white,
              letterSpacing: 0.5,
            ),
          ),
          onPressed: () {
            if (!offerFileAndTypeBool) {
              createOfferVm.toUpdateOffer = true;

              if (context.mounted) {
                Navigator.pop(context);
                Navigator.pop(context);
                changeDrawerRoute(
                  Routes.createOffer,
                  arguments: {
                    'job': partnerJob,
                  },
                );
              }
            } else {
              offerVm.isUploadOfferUpdate = true;
              offerVm.isNewOfferVersion = false;
              Navigator.pop(context);
              Navigator.pop(context);
              changeDrawerRoute(Routes.uploadOffer, arguments: partnerJob);
            }
          },
        ),
        SmartGaps.gapH10,
        CustomDesignTheme.flatButtonStyle(
          height: 50,
          width: double.infinity,
          shape: const RoundedRectangleBorder(
            side: BorderSide(
              color: PartnerAppColors.accentBlue,
            ),
          ),
          child: Text(
            tr('make_new_offer'),
            style: context.pttBodySmall.copyWith(
              fontWeight: FontWeight.w600,
              fontSize: 18,
              color: PartnerAppColors.accentBlue,
              letterSpacing: 0.5,
            ),
          ),
          onPressed: () {
            final canUploadOffer = (companyUploadOffer ?? 0) == 1;
            log(canUploadOffer.toString());
            if (canUploadOffer) {
              chooseOfferDialog(
                  context: context,
                  createOffer: () {
                    createOfferVm.toUpdateOffer = false;
                    createOfferVm.isNewOfferVersion = true;

                    if (context.mounted) {
                      Navigator.pop(context);
                      Navigator.pop(context);
                      changeDrawerRoute(
                        Routes.createOffer,
                        arguments: {
                          'job': partnerJob,
                        },
                      );
                    }
                  },
                  uploadOffer: () {
                    offerVm.isUploadOfferUpdate = false;
                    offerVm.isNewOfferVersion = true;
                    Navigator.pop(context);
                    Navigator.pop(context);
                    changeDrawerRoute(Routes.uploadOffer,
                        arguments: partnerJob);
                  });
            }
          },
        ),
        SmartGaps.gapH30,
      ],
    );
  }
}

class InviteSentButton extends StatelessWidget {
  const InviteSentButton({super.key, required this.partnerJob});
  final PartnerJobModel? partnerJob;

  @override
  Widget build(BuildContext context) {
    return CustomDesignTheme.flatButtonStyle(
      height: 50,
      onPressed: () async {
        final clientsVm = context.read<ClientsViewModel>();
        final appDrawerVm = context.read<AppDrawerViewModel>();

        modalManager.showLoadingModal();

        final response = await tryCatchWrapper(
            context: context,
            function: clientsVm.resendInvite(
                job: partnerJob!,
                isAffiliate:
                    (appDrawerVm.bitrixCompanyModel?.companyPackge ?? '') ==
                        'affiliatepartner'));

        modalManager.hideLoadingModal();

        if ((response ?? false) && context.mounted) {
          commonSuccessOrFailedDialog(
              context: context, successMessage: tr('success!'));
        }
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(2),
        side: BorderSide(
          color: Theme.of(context).colorScheme.primary,
        ),
      ),
      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Text(
          tr('send_contract_again').toUpperCase(),
          style: context.pttBodySmall.copyWith(
            fontWeight: FontWeight.w600,
            color: PartnerAppColors.accentBlue,
            letterSpacing: 0.5,
          ),
        )
      ]),
    );
  }
}
