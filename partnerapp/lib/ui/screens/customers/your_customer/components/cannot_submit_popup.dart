import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';

class CannotSubmitPopup extends StatelessWidget {
  const CannotSubmitPopup(this.callFirstTxt, {super.key});
  final String callFirstTxt;

  @override
  Widget build(BuildContext context) {
    final centeredCallFirstTxt =
        '<div style="text-align= center;"> $callFirstTxt </div>';
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      backgroundColor: Colors.white,
      titlePadding: EdgeInsets.zero,
      contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 35),
      insetPadding: const EdgeInsets.fromLTRB(20, 190, 20, 330),
      title: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Align(
          alignment: Alignment.bottomRight,
          child: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(Icons.close,
                size: 25, color: Colors.black.withValues(alpha: 0.4)),
          ),
        ),
      ),
      content: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4),
              child: HtmlWidget(
                centeredCallFirstTxt,
                textStyle: context.pttBodyLarge.copyWith(color: Colors.black),
              ),
            ),
            SmartGaps.gapH20,
            CustomDesignTheme.flatButtonStyle(
              width: MediaQuery.of(context).size.width,
              height: 50,
              backgroundColor: PartnerAppColors.malachite,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('OK',
                  style: Theme.of(context).textTheme.displayLarge!.copyWith(
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                      )),
            ),
          ],
        ),
      ),
    );
  }
}
