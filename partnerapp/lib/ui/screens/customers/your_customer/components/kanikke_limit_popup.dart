import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';

Future<void> kanikkeLimitPopupDialog({
  required BuildContext context,
  required String message,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          GestureDetector(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(Icons.close),
          )
        ]),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(child: KanikkeLimitPopup(message))),
      );
    },
  );
}

class KanikkeLimitPopup extends StatelessWidget {
  const KanikkeLimitPopup(this.messageContent, {super.key});
  final String messageContent;

  @override
  Widget build(BuildContext context) {
    final centeredMessageContent =
        '<div style="text-align= center;"> $messageContent </div>';
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 4),
          child: HtmlWidget(
            centeredMessageContent,
            textStyle: context.pttBodyLarge.copyWith(color: Colors.black),
          ),
        ),
        SmartGaps.gapH20,
        CustomDesignTheme.flatButtonStyle(
          width: MediaQuery.of(context).size.width,
          height: 50,
          backgroundColor: PartnerAppColors.malachite,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('OK',
              style: Theme.of(context).textTheme.displayLarge!.copyWith(
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                  )),
        ),
      ],
    );
  }
}
