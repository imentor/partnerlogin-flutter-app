part of '../your_customer_sort_filter.dart';

class TypeFilter extends StatelessWidget {
  const TypeFilter({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        PopupMenuButton(
          position: PopupMenuPosition.under,
          itemBuilder: (_) {
            return [
              const PopupMenuItem(enabled: false, child: TypeFilterMenu())
            ];
          },
          offset: const Offset(0, 20),
          child: SvgPicture.asset('assets/images/filter.svg'),
        ),
        SmartGaps.gapH5,
        Text('Filter',
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.normal,
                fontSize: 14))
      ],
    );
  }
}

class TypeFilterMenu extends StatefulWidget {
  const TypeFilterMenu({super.key});

  @override
  TypeFilterMenuState createState() => TypeFilterMenuState();
}

class TypeFilterMenuState extends State<TypeFilterMenu> {
  bool checkAllFilter = false;
  final formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Consumer<ClientsViewModel>(builder: (_, vm, __) {
      final groupOne = [
        'master_to_master',
        'private',
        'with_offers',
        'without_offers',
        'archive_customer'
      ];
      final groupTwo = [
        'new_task',
        'in_dialogue',
        'offers_have_been_sent',
        'won_the_task',
        'job_is_done'
      ];

      final initialFilterGroupOne = vm.currentJobFilters
          .where((sub) => groupOne.any((element) => element == sub))
          .map((e) => e)
          .toList();

      final initialFilterGroupTwo = vm.currentJobFilters
          .where((sub) => groupTwo.any((element) => element == sub))
          .map((e) => e)
          .toList();

      return FormBuilder(
        key: formKey,
        child: Column(
          children: [
            Row(
              children: [
                Checkbox(
                  value: checkAllFilter,
                  activeColor: PartnerAppColors.blue,
                  onChanged: (val) {
                    if ((val ?? false)) {
                      setState(() {
                        checkAllFilter = true;
                      });
                      formKey.currentState!.patchValue({
                        "typeGroupOne": [
                          'master_to_master',
                          'private',
                          'with_offers',
                          'archive_customer'
                        ],
                        "typeGroupTwo": groupTwo
                      });
                    } else {
                      List<String> empty = [];
                      setState(() {
                        checkAllFilter = false;
                      });
                      formKey.currentState!.patchValue(
                          {"typeGroupOne": empty, "typeGroupTwo": empty});
                    }
                  },
                ),
                Text(
                  'All',
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 16),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                    child: CustomCheckboxGroupFormBuilder(
                  options: [
                    FormBuilderFieldOption(
                      value: 'master_to_master',
                      child: Text(
                        tr('master_to_master'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 14),
                      ),
                    ),
                    FormBuilderFieldOption(
                      value: 'private',
                      child: Text(
                        tr('private'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 14),
                      ),
                    ),
                    FormBuilderFieldOption(
                      value: 'with_offers',
                      child: Text(
                        tr('with_offers'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 14),
                      ),
                    ),
                    FormBuilderFieldOption(
                      value: 'without_offers',
                      child: Text(
                        tr('without_offers'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 14),
                      ),
                    ),
                    FormBuilderFieldOption(
                      value: 'archive_customer',
                      child: Text(
                        tr('archive_customer'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 14),
                      ),
                    ),
                  ],
                  name: 'typeGroupOne',
                  optionsOrientation: OptionsOrientation.vertical,
                  initialValue: initialFilterGroupOne,
                  onChanged: (p0) {
                    if (p0 != null && p0.isNotEmpty) {
                      if (p0.last == 'with_offers') {
                        List<String> currentValues = [
                          ...(formKey.currentState!.fields['typeGroupOne']
                                  ?.value ??
                              [])
                        ];
                        if (currentValues.contains('without_offers')) {
                          currentValues.remove('without_offers');
                          formKey.currentState!
                              .patchValue({"typeGroupOne": currentValues});
                        }
                      } else if (p0.last == 'without_offers') {
                        List<String> currentValues = [
                          ...(formKey.currentState!.fields['typeGroupOne']
                                  ?.value ??
                              [])
                        ];
                        if (currentValues.contains('with_offers')) {
                          currentValues.remove('with_offers');
                          formKey.currentState!
                              .patchValue({"typeGroupOne": currentValues});
                        }
                      }
                    }
                  },
                )),
                Flexible(
                    child: CustomCheckboxGroupFormBuilder(
                  options: [
                    FormBuilderFieldOption(
                      value: 'new_task',
                      child: Text(
                        tr('new_task'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 14),
                      ),
                    ),
                    FormBuilderFieldOption(
                      value: 'in_dialogue',
                      child: Text(
                        tr('in_dialogue'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 14),
                      ),
                    ),
                    FormBuilderFieldOption(
                      value: 'offers_have_been_sent',
                      child: Text(
                        tr('offers_have_been_sent'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 14),
                      ),
                    ),
                    FormBuilderFieldOption(
                      value: 'won_the_task',
                      child: Text(
                        tr('won_the_task'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 14),
                      ),
                    ),
                    FormBuilderFieldOption(
                      value: 'job_is_done',
                      child: Text(
                        tr('job_is_done'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 14),
                      ),
                    ),
                  ],
                  name: 'typeGroupTwo',
                  initialValue: initialFilterGroupTwo,
                  optionsOrientation: OptionsOrientation.vertical,
                ))
              ],
            ),
            SmartGaps.gapH10,
            CustomDesignTheme.flatButtonStyle(
              height: 50,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                  side: const BorderSide(color: PartnerAppColors.darkBlue)),
              onPressed: () async {
                List<String> selectedFilter = [
                  ...(formKey.currentState!.fields['typeGroupOne']?.value ??
                      []),
                  ...(formKey.currentState!.fields['typeGroupTwo']?.value ?? [])
                ];

                vm.filterJobs(filters: selectedFilter);
                Navigator.of(context).pop();
              },
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text(
                  tr('filter_now'),
                  style: Theme.of(context).textTheme.displayLarge!.copyWith(
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                        color: PartnerAppColors.darkBlue,
                      ),
                )
              ]),
            ),
            SmartGaps.gapH10,
          ],
        ),
      );
    });
  }
}
