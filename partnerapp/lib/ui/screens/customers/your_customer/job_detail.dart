import 'package:Haandvaerker.dk/model/customer_minbolig/old_jobs_model.dart';
import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/components/dynamic_buttons.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/components/kanikke_limit_popup.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/job_notes.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/job_survey.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/job_unavailable.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/launcher.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/kanikke_settings_response_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

Future<void> jobDetailDialog({
  required BuildContext context,
  PartnerJobModel? job,
  required int jobIndex,
  OldPartnerJobsModel? oldJob,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: JobDetail(
              job: job,
              jobIndex: jobIndex,
              oldJob: oldJob,
            ))),
      );
    },
  );
}

Future<void> viewJobTitleDescriptionDialog({
  required BuildContext context,
  required PartnerJobModel job,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  tr('job_title'),
                  style: context.pttTitleMedium.copyWith(
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  job.projectName ?? '',
                  style: context.pttTitleMedium.copyWith(
                    fontWeight: FontWeight.normal,
                  ),
                ),
                SmartGaps.gapH30,
                Text(
                  tr('job_description'),
                  style: context.pttTitleMedium.copyWith(
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  job.projectDescription ?? '',
                  style: context.pttTitleMedium.copyWith(
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}

class JobDetail extends StatefulWidget {
  const JobDetail({
    super.key,
    this.job,
    required this.jobIndex,
    this.oldJob,
  });

  final PartnerJobModel? job;
  final int jobIndex;
  final OldPartnerJobsModel? oldJob;

  @override
  State<JobDetail> createState() => _JobDetailState();
}

class _JobDetailState extends State<JobDetail> {
  late bool hideDetails;
  late bool isArchived;

  @override
  void initState() {
    hideDetails = false;
    isArchived = false;
    super.initState();
  }

  void onChanged({String? value}) async {
    final clientsVm = context.read<ClientsViewModel>();
    final tenderVm = context.read<TenderFolderViewModel>();
    final kanikkeVm = context.read<KanIkkeSettingsResponseViewModel>();
    final appDrawerVm = context.read<AppDrawerViewModel>();

    if (widget.job != null) {
      switch (value) {
        case 'In negotiation':
          onNegotiation(clientsVm: clientsVm, tenderVm: tenderVm);
          break;
        case 'New task':
          onNewTask(clientsVm: clientsVm, tenderVm: tenderVm);
          break;
        case 'Unable to bid':
          onUnableToBid(
              clientsVm: clientsVm,
              tenderVm: tenderVm,
              appDrawerVm: appDrawerVm,
              kanikkeVm: kanikkeVm);
          break;
        case 'Archive the customer':
          onArchiveCustomer(
              clientsVm: clientsVm, tenderVm: tenderVm, archive: 1);
          break;
        case 'Unarchive the customer':
          onArchiveCustomer(
              clientsVm: clientsVm, tenderVm: tenderVm, archive: 0);
          break;
        case 'Won the task':
          onJobWon(clientsVm: clientsVm, tenderVm: tenderVm);
          break;
        default:
          break;
      }
    } else {
      if (value == '6' &&
          widget.oldJob?.idAppointment != null &&
          widget.oldJob?.contactId != null) {
        Navigator.of(context).pop();
        clientsVm.getYourCustomerData = true;

        final updateStatus = await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext!,
            function: tenderVm.archiveOldJob(
                jobId: int.parse(widget.oldJob!.idAppointment!),
                contactId: int.parse(widget.oldJob!.contactId!)));

        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext!,
            function: clientsVm.getPartnerJobsV2());

        clientsVm.getYourCustomerData = false;

        commonSuccessOrFailedDialog(
            context: myGlobals.homeScaffoldKey!.currentContext!,
            isSuccess: (updateStatus ?? false));
      }
    }
  }

  Future<void> onArchiveCustomer(
      {required ClientsViewModel clientsVm,
      required TenderFolderViewModel tenderVm,
      required int archive}) async {
    Navigator.of(context).pop();

    clientsVm.getYourCustomerData = true;

    final updateStatus = await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext!,
        function: tenderVm.updateArchiveJob(
            jobId: widget.job?.id ?? 0, isDeleted: archive));

    await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function: clientsVm.getPartnerJobsV2());

    clientsVm.getYourCustomerData = false;

    commonSuccessOrFailedDialog(
        context: myGlobals.homeScaffoldKey!.currentContext!,
        isSuccess: (updateStatus ?? false));
  }

  Future<void> onUnableToBid(
      {required ClientsViewModel clientsVm,
      required TenderFolderViewModel tenderVm,
      required AppDrawerViewModel appDrawerVm,
      required KanIkkeSettingsResponseViewModel kanikkeVm}) async {
    modalManager.showLoadingModal();

    await tryCatchWrapper(
        context: context,
        function: Future.wait([
          appDrawerVm.getPrisenResultV2(),
          kanikkeVm.getKanIkkeSettings(),
        ]));

    if (!mounted) return;

    if ((appDrawerVm.bitrixCompanyModel?.kanIkkeTilbud ?? 0) == 1) {
      if (kanikkeVm.isBlackListed) {
        if (kanikkeVm.featuresLimitation.isNotEmpty &&
            kanikkeVm.featuresLimitation.contains('mine_kunder')) {
          modalManager.hideLoadingModal();
          kanikkeLimitPopupDialog(
              context: context, message: kanikkeVm.popupText);
        } else {
          modalManager.hideLoadingModal();

          await jobUnavailable(
              clientsVm: clientsVm, tenderVm: tenderVm, kanikkeVm: kanikkeVm);
        }
      } else if (kanikkeVm.isWarningForBlackList) {
        if (kanikkeVm.featuresLimitation.isNotEmpty &&
            kanikkeVm.featuresLimitation.contains('mine_kunder')) {
          modalManager.hideLoadingModal();

          kanikkeLimitPopupDialog(
                  context: context, message: kanikkeVm.warningPopupText)
              .whenComplete(() async {
            await jobUnavailable(
                clientsVm: clientsVm, tenderVm: tenderVm, kanikkeVm: kanikkeVm);
          });
        } else {}
      } else {
        modalManager.hideLoadingModal();

        await jobUnavailable(
            clientsVm: clientsVm, tenderVm: tenderVm, kanikkeVm: kanikkeVm);
      }
    } else {
      modalManager.hideLoadingModal();

      await jobUnavailable(
          clientsVm: clientsVm, tenderVm: tenderVm, kanikkeVm: kanikkeVm);
    }
  }

  Future<void> jobUnavailable(
      {required ClientsViewModel clientsVm,
      required TenderFolderViewModel tenderVm,
      required KanIkkeSettingsResponseViewModel kanikkeVm}) async {
    final response =
        await jobUnavailableDialog(context: context, job: widget.job!);

    if (!mounted) return;

    if (response != null) {
      Navigator.of(context).pop();

      clientsVm.getYourCustomerData = true;
      String cancelStatusId = response['cancelStatusId'];
      String? reminderDate;

      if (response.containsKey('reminderDate')) {
        reminderDate = response['reminderDate'];
      }

      await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function: tenderVm.cancelJob(
          cancelStatusId: int.parse(cancelStatusId),
          jobId: widget.job?.id ?? 0,
          reminderDate: reminderDate,
        ),
      );

      await tryCatchWrapper(
          context: myGlobals.homeScaffoldKey!.currentContext!,
          function: Future.wait([
            clientsVm.getPartnerJobsV2(),
            kanikkeVm.getKanIkkeSettings(),
          ]));

      clientsVm.getYourCustomerData = false;
      commonSuccessOrFailedDialog(
          context: myGlobals.homeScaffoldKey!.currentContext!,
          isSuccess: (tenderVm.cancelJobResponse?.success ?? false));
    }
  }

  Future<void> onNewTask({
    required ClientsViewModel clientsVm,
    required TenderFolderViewModel tenderVm,
  }) async {
    Navigator.of(context).pop();
    clientsVm.getYourCustomerData = true;

    final updateStatus = await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext!,
        function:
            tenderVm.updateJobStatus(jobId: widget.job?.id ?? 0, statusId: 1));

    await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: clientsVm.getPartnerJobsV2())
        .whenComplete(() {
      clientsVm.getYourCustomerData = false;
      commonSuccessOrFailedDialog(
          context: myGlobals.homeScaffoldKey!.currentContext!,
          isSuccess: updateStatus ?? false);
    });
  }

  Future<void> onNegotiation({
    required ClientsViewModel clientsVm,
    required TenderFolderViewModel tenderVm,
  }) async {
    final survey = await jobSurveyDialog(context: context);
    if (!mounted || survey == null) return;

    Navigator.of(context).pop();

    clientsVm.getYourCustomerData = true;

    final jobUpdateSuccess = await tryCatchWrapper(
      context: myGlobals.homeScaffoldKey!.currentContext,
      function: tenderVm.jobUpdate(
        jobId: widget.job?.id ?? 0,
        note: survey,
      ),
    );

    if (jobUpdateSuccess ?? false) {
      await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function: Future.wait([
          tenderVm.updateJobStatus(
            jobId: widget.job?.id ?? 0,
            statusId: 2,
          ),
          clientsVm.getPartnerJobsV2(),
        ]),
      ).whenComplete(() {
        clientsVm.getYourCustomerData = false;
        commonSuccessOrFailedDialog(
            context: myGlobals.homeScaffoldKey!.currentContext!,
            isSuccess: jobUpdateSuccess ?? false);
      });
    }
  }

  Future<void> onJobWon({
    required ClientsViewModel clientsVm,
    required TenderFolderViewModel tenderVm,
  }) async {
    Navigator.of(context).pop();
    clientsVm.getYourCustomerData = true;

    final updateStatus = await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext!,
        function:
            tenderVm.updateJobStatus(jobId: widget.job?.id ?? 0, statusId: 13));

    await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: clientsVm.getPartnerJobsV2())
        .whenComplete(() {
      clientsVm.getYourCustomerData = false;
      commonSuccessOrFailedDialog(
          context: myGlobals.homeScaffoldKey!.currentContext!,
          isSuccess: updateStatus ?? false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<TenderFolderViewModel, AppDrawerViewModel>(
      builder: (_, tenderVm, appDrawerVm, __) {
        bool isDK = context.locale.languageCode == 'da';

        List<JobStatus> offerStatus = [];
        List<OldTilbudStatus> oldJobsStatus = [
          ...tenderVm.oldtilbudStatus.where(
              (element) => element.value == (widget.oldJob?.tilbudStatus ?? 0))
        ];

        String? offerStatusInitial;

        String customerName = '';
        String address = '';
        String mobileNum = '';
        String email = '';
        String launchEmailAddress = '';
        // bool showEmail = false;
        int differenceInDays = 0;
        String projectDeadlineOffer = '';
        String projectDeadlineOfferColorCode = '';
        // String projectDeadlineOfferColorApi = '';
        Color projectDeadlineOfferColor = PartnerAppColors.darkBlue;

        if (widget.job != null) {
          customerName = widget.job?.customerName ?? '';

          // checks if the job is a fast contract or if status ID of the job is greater than 3
          if ((widget.job!.offerType != null &&
                  widget.job!.offerType!.isNotEmpty &&
                  widget.job!.offerType == 'fastcontract') ||
              (widget.job!.statusId != null && widget.job!.statusId! > 3)) {
            address = widget.job?.projectAddress ?? '';
            email = widget.job?.customerEmail ?? '';
            if (widget.job!.contactRealPhone != null &&
                widget.job!.contactRealPhone!.isNotEmpty) {
              mobileNum = widget.job!.contactRealPhone!;
            } else {
              mobileNum = widget.job!.customerMobile ?? '';
            }
            // if ((appDrawerVm.bitrixCompanyModel?.companyEmailSetting ?? 0) ==
            //     1) {
            //   showEmail = true;
            // }
          } else {
            if ((appDrawerVm.bitrixCompanyModel?.companyVundetOpgaven ?? 0) ==
                1) {
              if (widget.job?.contactRealPhone != null) {
                mobileNum = '${widget.job?.contactRealPhone}';
              } else {
                mobileNum = '${widget.job?.customerMobile}';
              }
              address = widget.job?.projectAddress ?? '';
              email = widget.job?.customerEmail ?? '';
              launchEmailAddress = widget.job?.customerEmail ?? '';
            } else {
              if ((appDrawerVm.bitrixCompanyModel?.companyTrackingPhoneEmail ??
                          0) ==
                      1 &&
                  widget.job?.callDate == null) {
                address =
                    '${widget.job?.projectCity} ${widget.job?.projectZip}';
                mobileNum = '${widget.job?.contactFakePhone}';
                email = tr('send_email');
                launchEmailAddress = widget.job?.customerFakeEmail ?? '';
              } else {
                if (widget.job?.contactFakePhone != null &&
                    widget.job?.contactFakePhone != '' &&
                    widget.job?.callDate == null) {
                  mobileNum = '${widget.job?.contactFakePhone}';
                  address =
                      '${widget.job?.projectCity} ${widget.job?.projectZip}';
                  email = tr('send_email');
                  launchEmailAddress = widget.job?.customerFakeEmail ?? '';
                } else if (widget.job?.contactRealPhone != null) {
                  mobileNum = '${widget.job?.contactRealPhone}';
                  address = widget.job?.projectAddress ?? '';
                  email = tr('send_email');
                  launchEmailAddress = widget.job?.customerEmail ?? '';
                } else {
                  mobileNum = '${widget.job?.customerMobile}';
                  address = widget.job?.projectAddress ?? '';
                  email = tr('send_email');
                  launchEmailAddress = widget.job?.customerEmail ?? '';
                }
              }
            }
            // if ((widget.job?.customerFakeEmail ?? '').isNotEmpty &&
            //     (appDrawerVm.bitrixCompanyModel?.companyEmailSetting ?? 0) ==
            //         1 &&
            //     widget.job?.callDate != null) {
            //   showEmail = true;
            // }
          }

          if ((widget.job?.isDeleted ?? 0) == 1) {
            isArchived = true;
          }

          if ((widget.job?.projectStatus ?? '') == 'Paused') {
            offerStatus = [
              JobStatus(
                  titleEn: 'Deactivated user', titleDk: 'Deaktiveret bruger'),
              JobStatus(
                  titleDk: 'Arkiver kunden',
                  titleEn: 'Archive the customer',
                  isSelected: true)
            ];

            offerStatusInitial = 'Deactivated user';
          } else if ((widget.job?.statusNameEN ?? '') == 'Customer Deleted') {
            hideDetails = true;
            offerStatus = [
              JobStatus(titleEn: 'Deleted user', titleDk: 'Slettet bruger'),
              JobStatus(
                  titleDk: 'Arkiver kunden',
                  titleEn: 'Archive the customer',
                  isSelected: true)
            ];

            offerStatusInitial = 'Deleted user';
          } else if ((widget.job?.alertStatus ?? '') == 'pending') {
            offerStatus = [
              JobStatus(
                titleEn: 'Pending Review',
                titleDk: 'Afventer gennemsyn',
                isSelected: false,
              ),
            ];

            offerStatusInitial = context.locale.languageCode == 'da'
                ? 'Afventer gennemsyn'
                : 'Pending Review';
          } else if ((widget.job?.statusNameEN ?? '') == 'Offer sent') {
            offerStatus = [
              JobStatus(
                  titleEn: 'In negotiation',
                  titleDk: 'I dialog',
                  isSelected: false)
            ];

            offerStatusInitial = 'In negotiation';
          } else if ((widget.job?.statusNameEN ?? '') == 'Job is won' ||
              (widget.job?.statusNameEN ?? '') == 'Job is done') {
            offerStatus = [
              JobStatus(
                  titleEn: widget.job?.statusNameEN ?? '',
                  titleDk: widget.job?.statusNameDK ?? '',
                  isSelected: false),
              JobStatus(
                  titleDk: 'Arkiver kunden',
                  titleEn: 'Archive the customer',
                  isSelected: true)
            ];

            offerStatusInitial = widget.job?.statusNameEN;
          } else if ((widget.job?.statusNameEN ?? '') == 'Job Lost') {
            offerStatus = [
              JobStatus(titleEn: 'Job Lost', titleDk: 'Job mistet'),
            ];

            offerStatusInitial = 'Job Lost';
          } else if ((widget.job?.statusNameEN ?? '') == 'Invitation sent') {
            offerStatus = [
              JobStatus(
                  titleEn: 'Invitation sent', titleDk: 'Invitation sendt'),
            ];

            offerStatusInitial = 'Invitation sent';
          } else {
            offerStatus = [...(widget.job?.status ?? [])];

            offerStatusInitial = offerStatus
                .firstWhere(
                  (element) => element.id == (widget.job?.statusId ?? 0),
                  orElse: () => JobStatus(titleEn: null),
                )
                .titleEn;

            for (var element in offerStatus) {
              if (element.titleEn == offerStatusInitial) {
                element.isSelected = false;
              } else {
                element.isSelected = true;
              }
            }
          }

          if ((widget.job?.isDeleted ?? 0) == 1) {
            offerStatus.removeWhere(
                (element) => element.titleEn == 'Archive the customer');

            offerStatus.add(JobStatus(
                titleDk: 'Udpak kunden fra arkivet',
                titleEn: 'Unarchive the customer',
                isSelected: true));
          }

          if ((appDrawerVm.bitrixCompanyModel?.companyVundetOpgaven ?? 0) ==
                  1 &&
              offerStatus
                  .where((value) => value.titleDk == 'Vundet opgaven')
                  .isEmpty) {
            offerStatus.add(JobStatus(
                titleDk: 'Vundet opgaven',
                titleEn: 'Won the task',
                isSelected: true));
          }

          if ((widget.job?.statusId ?? 0) == 1 ||
              (widget.job?.statusId ?? 0) == 2) {
            projectDeadlineOffer = widget.job?.offerExpireDate ?? '';
            projectDeadlineOfferColorCode =
                widget.job?.offerExpireDateColorCode ?? '';
            // projectDeadlineOfferColorApi =
            //     widget.job?.offerExpireDateColor ?? '';
            if (projectDeadlineOffer.isNotEmpty) {
              tenderVm.formatDate(projectDeadlineOffer);
              final offerDueDate =
                  DateTime.parse(tenderVm.projectDeadlineOffer);
              differenceInDays = tenderVm.calculateDifference(offerDueDate);
            }
            if (projectDeadlineOfferColorCode.isNotEmpty) {
              projectDeadlineOfferColor = tenderVm
                  .projectDeadlineOfferColor(projectDeadlineOfferColorCode);
            }
            // Commented code out to let partners still use kan ikke even if they havent called the customer
            // if (projectDeadlineOfferColorApi.isNotEmpty) {
            //   if (differenceInDays < 0) {
            //     if (projectDeadlineOfferColorApi.contains(
            //         widget.job?.offerExpireDateColor as Pattern)) {
            //       try {
            //         JobStatus removeStatus =
            //             offerStatus.firstWhere((stat) => stat.id == 4);
            //         offerStatus.remove(removeStatus);
            //       } catch (e) {
            //         debugPrint(e.toString());
            //       }
            //     }
            //   }
            // }
          }

          // if ((context
          //             .read<AppDrawerViewModel>()
          //             .supplierProfileModel
          //             ?.bCrmCompany
          //             ?.companyTrackingPhoneEmail ??
          //         0) ==
          //     0) {
          //   mobileNum = widget.job?.customerMobile ?? '';
          // }
        } else {
          customerName =
              '${widget.oldJob?.firstName ?? ''} ${widget.oldJob?.lastName ?? ''}';
          address =
              '${widget.oldJob?.address ?? ''} ${widget.oldJob?.mailingCity ?? ''} ${widget.oldJob?.mailingZip ?? ''}';

          mobileNum = widget.oldJob?.mobile ?? '';

          offerStatusInitial = (widget.oldJob?.tilbudStatus ?? 0).toString();

          oldJobsStatus.add(OldTilbudStatus(
              value: 6, text: tr('archive_customer'), filterValue: 6));
        }

        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    SvgPicture.asset(
                      SvgIcons.calendarCheck,
                      width: 40,
                      height: 40,
                    ),
                    SmartGaps.gapH10,
                    if (context
                            .read<AppDrawerViewModel>()
                            .supplierProfileModel
                            ?.bCrmCompany
                            ?.deadlineOffer ==
                        1)
                      if (widget.job?.statusId == 1 ||
                          widget.job?.statusId == 2)
                        Text(
                          differenceInDays < 0
                              ? tr('awaiting_offer')
                              : differenceInDays == 0
                                  ? tr('due_today')
                                  : '$differenceInDays ${tr('days_left')}',
                          style: Theme.of(context)
                              .textTheme
                              .headlineLarge!
                              .copyWith(
                                  fontSize: 12,
                                  fontWeight: FontWeight.normal,
                                  color: projectDeadlineOfferColor),
                        ),
                  ],
                ),
                const Spacer(),
                Flexible(
                  child: CustomDropdownFormBuilder(
                    items: widget.job != null
                        ? [
                            ...offerStatus.map(
                              (e) => DropdownMenuItem(
                                value: e.titleEn,
                                enabled: e.isSelected ?? false,
                                child: Text(
                                  isDK ? (e.titleDk ?? '') : (e.titleEn ?? ''),
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyMedium!
                                      .copyWith(
                                          fontWeight: FontWeight.w500,
                                          color: PartnerAppColors.darkBlue),
                                ),
                              ),
                            ),
                          ]
                        : [
                            ...oldJobsStatus.map((old) => DropdownMenuItem(
                                  value: '${old.value}',
                                  enabled: old.value ==
                                      (widget.oldJob?.tilbudStatus ?? 0),
                                  child: Text(tr(old.text),
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium!
                                          .copyWith(
                                              fontWeight: FontWeight.w500)),
                                ))
                          ],
                    initialValue: offerStatusInitial,
                    name: 'offerStatus',
                    onChanged: (p0) => onChanged(value: p0),
                  ),
                ),
              ],
            ),
            SmartGaps.gapH10,
            Text(
              customerName,
              style: context.pttTitleMedium.copyWith(
                fontWeight: FontWeight.w600,
              ),
            ),
            SmartGaps.gapH20,
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset('assets/images/location.svg', width: 13),
                SmartGaps.gapW10,
                GestureDetector(
                  onTap: () async {},
                  child: Text(
                    address,
                    style: context.pttBodySmall.copyWith(
                      color: PartnerAppColors.darkBlue,
                    ),
                  ),
                )
              ],
            ),
            if (!hideDetails) ...[
              if (true) ...[
                SmartGaps.gapH15,
                GestureDetector(
                  onTap: () {
                    if (launchEmailAddress.isNotEmpty) {
                      Launcher().launchEmail(email);
                    }
                  },
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        'assets/images/mail-blue.svg',
                        width: 15,
                      ),
                      SmartGaps.gapW10,
                      Text(
                        email,
                        style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              height: 1,
                              color: PartnerAppColors.darkBlue,
                            ),
                      ),
                    ],
                  ),
                ),
              ],
              if (mobileNum.isNotEmpty) ...[
                SmartGaps.gapH15,
                GestureDetector(
                  onTap: () async {
                    final clientsVm = context.read<ClientsViewModel>();
                    clientsVm.calledCustomerJobId = widget.job?.id ?? 0;
                    Launcher().launchPhoneCall(mobileNum);
                    Navigator.of(context).pop();
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        'assets/images/phone-contact.svg',
                        width: 13,
                      ),
                      SmartGaps.gapW10,
                      Text(
                        '+45 $mobileNum',
                        style: context.pttBodySmall.copyWith(
                          color: PartnerAppColors.darkBlue,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
              if (widget.job != null) ...[
                SmartGaps.gapH15,
                InkWell(
                  onTap: () => viewJobTitleDescriptionDialog(
                      context: context, job: widget.job!),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        'assets/images/see_descriptions.svg',
                        width: 13,
                      ),
                      SmartGaps.gapW5,
                      Text(
                        tr('see_description'),
                        style: context.pttBodySmall.copyWith(
                          color: PartnerAppColors.darkBlue,
                        ),
                      ),
                    ],
                  ),
                ),
                SmartGaps.gapH30,
                Text(
                  widget.job?.projectName ?? "",
                  style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        height: 1,
                      ),
                ),
                SmartGaps.gapH20,
                Row(
                  children: [
                    Expanded(
                      child: CustomDesignTheme.flatButtonStyle(
                        height: 50,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(2),
                          side: BorderSide(
                            color:
                                Theme.of(context).colorScheme.onSurfaceVariant,
                          ),
                        ),
                        onPressed: () =>
                            jobNotesDialog(context: context, job: widget.job!),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                'assets/images/note.svg',
                                alignment: Alignment.centerRight,
                                width: 20,
                              ),
                              SmartGaps.gapW10,
                              Flexible(
                                child: FittedBox(
                                  fit: BoxFit.scaleDown,
                                  child: Text(
                                    tr('your_notes').toUpperCase(),
                                    textAlign: TextAlign.start,
                                    style: context.pttBodySmall.copyWith(
                                      fontWeight: FontWeight.w600,
                                      color: PartnerAppColors.darkBlue,
                                    ),
                                  ),
                                ),
                              )
                            ]),
                      ),
                    ),
                    SmartGaps.gapW10,
                    Expanded(
                      child: CustomDesignTheme.flatButtonStyle(
                        height: 50,
                        backgroundColor: Theme.of(context).colorScheme.primary,
                        onPressed: () {
                          Navigator.pop(context);
                          changeDrawerRoute(Routes.tenderFolder,
                              arguments: widget.job);
                        },
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                'assets/images/tender-folder.svg',
                                width: 20,
                                alignment: Alignment.centerLeft,
                                colorFilter: const ColorFilter.mode(
                                    Colors.white, BlendMode.srcIn),
                              ),
                              SmartGaps.gapW10,
                              Flexible(
                                child: FittedBox(
                                  fit: BoxFit.scaleDown,
                                  child: Text(
                                    tr('tender_folder').toUpperCase(),
                                    style: context.pttBodySmall.copyWith(
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              )
                            ]),
                      ),
                    )
                  ],
                ),
                SmartGaps.gapH10,
                Divider(
                  color: Theme.of(context)
                      .colorScheme
                      .onTertiaryContainer
                      .withValues(alpha: 0.4),
                  thickness: 1,
                ),
                SmartGaps.gapH10,
                if ((widget.job?.projectStatus ?? '') != 'Paused') ...[
                  dynamicButtons(),
                ],
                if ((widget.job?.callDate != null ||
                        (widget.job?.offerType ?? '') == 'fastcontract' ||
                        (widget.job?.statusId ?? 0) > 3) &&
                    ((widget.job?.statusNameEN ?? '') != 'Job Lost') &&
                    (widget.job?.statusNameEN ?? '') != 'Invitation sent' &&
                    widget.job?.statusId != 13) ...[
                  SmartGaps.gapH10,
                  CustomDesignTheme.flatButtonStyle(
                    height: 50,
                    onPressed: () async {
                      Navigator.pop(context);

                      changeDrawerRoute(Routes.createProjectMessage,
                          arguments: {
                            'customerId': widget.job?.customerId,
                          });
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(2),
                      side: BorderSide(
                        color: Theme.of(context).colorScheme.primary,
                      ),
                    ),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            tr('message').toUpperCase(),
                            style: context.pttBodySmall.copyWith(
                              fontWeight: FontWeight.w600,
                              color: PartnerAppColors.accentBlue,
                              letterSpacing: 0.5,
                            ),
                          )
                        ]),
                  ),
                  if (!isArchived &&
                      (widget.job?.projectStatus ?? '') == 'Paused') ...[
                    SmartGaps.gapH10,
                    CustomDesignTheme.flatButtonStyle(
                      height: 50,
                      onPressed: () async {
                        final clientsVm = context.read<ClientsViewModel>();
                        Navigator.of(context).pop();
                        clientsVm.getYourCustomerData = true;

                        await tryCatchWrapper(
                                context:
                                    myGlobals.homeScaffoldKey!.currentContext,
                                function: tenderVm.updateArchiveJob(
                                    jobId: widget.job?.id ?? 0, isDeleted: 1))
                            .then((updateStatus) async {
                          await clientsVm.getPartnerJobsV2();

                          clientsVm.getYourCustomerData = false;

                          commonSuccessOrFailedDialog(
                              context:
                                  myGlobals.homeScaffoldKey!.currentContext!,
                              isSuccess: (updateStatus ?? false));
                        });
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(2),
                        side: BorderSide(
                          color: Theme.of(context).colorScheme.primary,
                        ),
                      ),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              tr('archive').toUpperCase(),
                              style: context.pttBodySmall.copyWith(
                                fontWeight: FontWeight.w600,
                                color: PartnerAppColors.accentBlue,
                                letterSpacing: 0.5,
                              ),
                            )
                          ]),
                    ),
                  ],
                ] else if ((widget.job?.projectStatus ?? '') == 'Paused') ...[
                  SmartGaps.gapH10,
                  CustomDesignTheme.flatButtonStyle(
                    height: 50,
                    onPressed: () async {
                      Navigator.pop(context);

                      changeDrawerRoute(Routes.createProjectMessage,
                          arguments: {
                            'customerId': widget.job?.customerId,
                          });
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(2),
                      side: BorderSide(
                        color: Theme.of(context).colorScheme.primary,
                      ),
                    ),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            tr('message').toUpperCase(),
                            style: context.pttBodySmall.copyWith(
                              fontWeight: FontWeight.w600,
                              color: PartnerAppColors.accentBlue,
                              letterSpacing: 0.5,
                            ),
                          )
                        ]),
                  ),
                  if (!isArchived) ...[
                    SmartGaps.gapH10,
                    CustomDesignTheme.flatButtonStyle(
                      height: 50,
                      onPressed: () async {
                        final clientsVm = context.read<ClientsViewModel>();
                        Navigator.of(context).pop();
                        clientsVm.getYourCustomerData = true;

                        await tryCatchWrapper(
                                context:
                                    myGlobals.homeScaffoldKey!.currentContext,
                                function: tenderVm.updateArchiveJob(
                                    jobId: widget.job?.id ?? 0, isDeleted: 1))
                            .then((updateStatus) async {
                          await clientsVm.getPartnerJobsV2();

                          clientsVm.getYourCustomerData = false;

                          commonSuccessOrFailedDialog(
                              context:
                                  myGlobals.homeScaffoldKey!.currentContext!,
                              isSuccess: (updateStatus ?? false));
                        });
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(2),
                        side: BorderSide(
                          color: Theme.of(context).colorScheme.primary,
                        ),
                      ),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              tr('archive').toUpperCase(),
                              style: context.pttBodySmall.copyWith(
                                fontWeight: FontWeight.w600,
                                color: PartnerAppColors.accentBlue,
                                letterSpacing: 0.5,
                              ),
                            )
                          ]),
                    ),
                  ],
                ],
              ],
            ],
            if (hideDetails) ...[
              SmartGaps.gapH40,
              SmartGaps.gapH40,
              Center(
                child: SvgPicture.asset(
                  SvgIcons.deletedCustomer,
                  height: 120.0,
                  width: 120.0,
                  colorFilter: const ColorFilter.mode(
                    PartnerAppColors.spanishGrey,
                    BlendMode.srcIn,
                  ),
                ),
              ),
              SmartGaps.gapH40,
              if (!isArchived) ...[
                SmartGaps.gapH30,
                CustomDesignTheme.flatButtonStyle(
                  height: 50,
                  onPressed: () async {
                    final clientsVm = context.read<ClientsViewModel>();
                    Navigator.of(context).pop();
                    clientsVm.getYourCustomerData = true;

                    await tryCatchWrapper(
                            context: myGlobals.homeScaffoldKey!.currentContext,
                            function: tenderVm.updateArchiveJob(
                                jobId: widget.job?.id ?? 0, isDeleted: 1))
                        .then((updateStatus) async {
                      await clientsVm.getPartnerJobsV2();

                      clientsVm.getYourCustomerData = false;

                      commonSuccessOrFailedDialog(
                          context: myGlobals.homeScaffoldKey!.currentContext!,
                          isSuccess: (updateStatus ?? false));
                    });
                  },
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(2),
                    side: BorderSide(
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          tr('archive').toUpperCase(),
                          style: context.pttBodySmall.copyWith(
                            fontWeight: FontWeight.w600,
                            color: PartnerAppColors.accentBlue,
                            letterSpacing: 0.5,
                          ),
                        )
                      ]),
                ),
              ],
            ],
          ],
        );
      },
    );
  }

  Widget dynamicButtons() {
    final offerStatus = widget.job?.offerStatus;
    final projectStatus = (widget.job?.projectStatus ?? '').toLowerCase();

    if (widget.job?.statusId == 13) {
      return JobWonButtons(partnerJob: widget.job);
    } else {
      if ((widget.job?.statusNameEN ?? '') == 'Job Lost') {
        return JobLostButtons(partnerJob: widget.job);
      } else if ((widget.job?.statusNameEN ?? '') == 'Invitation sent') {
        if (widget.job?.offerType == 'fastcontract') {
          return InviteSentButton(partnerJob: widget.job);
        } else if (widget.job?.offerType == 'fast-offer' ||
            widget.job?.offerType == 'standard') {
          return PendingStatusButtons(partnerJob: widget.job);
        } else {
          return const SizedBox.shrink();
        }
      } else {
        if (offerStatus == 'accepted') {
          return OfferAcceptedButtons(partnerJob: widget.job);
        } else {
          if (offerStatus == null &&
              (projectStatus == 'pre marketplace' ||
                  projectStatus == 'in marketplace' ||
                  projectStatus == 'return marketplace' ||
                  projectStatus == 'new project' ||
                  projectStatus == 'finish marketplace' ||
                  projectStatus == 'out of marketplace')) {
            return NewJobButtons(partnerJob: widget.job);
          } else if (projectStatus == 'pre marketplace') {
            return const SizedBox.shrink();
          } else if (offerStatus.toString().toLowerCase() == 'pending' ||
              offerStatus.toString().toLowerCase() == 'failed') {
            return PendingStatusButtons(partnerJob: widget.job);
          } else {
            return const SizedBox.shrink();
          }
        }
      }
    }
  }
}
