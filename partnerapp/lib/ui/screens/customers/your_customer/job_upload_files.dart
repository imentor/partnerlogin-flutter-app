import 'dart:developer';
import 'dart:io';

import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/file_utils.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

Future<void> jobUploadFilesDialog({
  required BuildContext context,
  required PartnerJobModel job,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: JobUploadFiles(
              job: job,
            ),
          ),
        ),
      );
    },
  );
}

class JobUploadFiles extends StatefulWidget {
  const JobUploadFiles({
    super.key,
    required this.job,
  });
  final PartnerJobModel job;

  @override
  State<JobUploadFiles> createState() => _JobUploadFilesState();
}

class _JobUploadFilesState extends State<JobUploadFiles> {
  final formKey = GlobalKey<FormBuilderState>();

  bool isLoading = false;
  bool? isSuccessfull = false;

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (isLoading) ...[
            const SizedBox(
              height: 50,
              width: 50,
              child: CircularProgressIndicator(
                color: PartnerAppColors.blue,
              ),
            )
          ] else if ((isSuccessfull ?? false)) ...[
            Container(
              height: 65,
              width: 65,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: PartnerAppColors.malachite,
              ),
              child: const Center(
                  child: Icon(
                FeatherIcons.check,
                color: Colors.white,
                size: 50,
              )),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              tr('success'),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                    color: Colors.black,
                  ),
            )
          ] else ...[
            FormBuilderField<List<File>>(
                validator:
                    FormBuilderValidators.required(errorText: tr('required')),
                builder: (FormFieldState field) {
                  return InputDecorator(
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.zero,
                      errorText: field.errorText,
                    ),
                    child: Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width,
                          padding: const EdgeInsets.all(20),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                  color: PartnerAppColors.spanishGrey)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const Icon(
                                FeatherIcons.uploadCloud,
                                size: 30,
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Text(tr('upload_file'),
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(
                                          color: PartnerAppColors.darkBlue,
                                          fontWeight: FontWeight.bold)),
                              const SizedBox(
                                height: 10,
                              ),
                              TextButton(
                                  onPressed: () async {
                                    List<File> items =
                                        ((field.value as List<File>?) ?? []);

                                    final files = await FilePicker.platform
                                        .pickFiles(allowMultiple: true);

                                    if (!mounted) return;

                                    for (var element
                                        in (files?.files ?? <PlatformFile>[])) {
                                      items.add(File(element.path!));
                                    }

                                    field.didChange([...items]);
                                  },
                                  style: TextButton.styleFrom(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 20),
                                      shape: const RoundedRectangleBorder(
                                          side: BorderSide(
                                              color:
                                                  PartnerAppColors.darkBlue))),
                                  child: Text(
                                    tr('upload'),
                                    style: Theme.of(context)
                                        .textTheme
                                        .headlineMedium!
                                        .copyWith(
                                          color: PartnerAppColors.darkBlue,
                                        ),
                                  ))
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        ...((field.value as List<File>?) ?? []).map((file) {
                          return fileGridItem(
                              context: context, file: file, field: field);
                        })
                      ],
                    ),
                  );
                },
                name: 'jobFiles'),
            const SizedBox(
              height: 20,
            ),
            CustomDesignTheme.flatButtonStyle(
              height: 50,
              backgroundColor: PartnerAppColors.malachite,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),
              onPressed: () async {
                try {
                  await onUpload();
                } catch (e) {
                  log("message lalal $e");
                }
              },
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text(
                  'Upload',
                  style: Theme.of(context).textTheme.displayLarge!.copyWith(
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                      ),
                )
              ]),
            )
          ]
        ],
      ),
    );
  }

  Future<void> onUpload() async {
    final clientsVm = context.read<ClientsViewModel>();

    if (formKey.currentState!.validate()) {
      setState(() {
        isLoading = true;
      });

      await tryCatchWrapper(
              context: context,
              function: clientsVm.uploadJobFiles(
                  files: formKey.currentState!.fields['jobFiles']!.value,
                  job: widget.job))
          .then((value) {
        setState(() {
          isSuccessfull = value ?? false;
          isLoading = false;
        });
      });
    }
  }

  Widget fileGridItem(
      {required BuildContext context,
      required File file,
      required FormFieldState<dynamic> field}) {
    Widget leading = const SizedBox.shrink();

    if (isImageFile(file.path)) {
      leading = SizedBox(
        height: 80,
        width: 80,
        child: Image.file(
          file,
          fit: BoxFit.cover,
        ),
      );
    } else if (isDocumentFile(file.path)) {
      leading = Container(
        height: 80,
        width: 80,
        color: PartnerAppColors.blue,
        child: const Center(
          child: Icon(
            FeatherIcons.paperclip,
            color: Colors.white,
            size: 40,
          ),
        ),
      );
    } else {
      leading = const SizedBox.shrink();
    }

    return Column(
      children: [
        ListTile(
          contentPadding: EdgeInsets.zero,
          leading: leading,
          title: Text(
            getNameFromFile(file.path),
            style: Theme.of(context).textTheme.titleMedium,
          ),
          trailing: InkWell(
            onTap: () {
              List<File> items = [...((field.value as List<File>?) ?? [])];

              items.remove(file);

              field.didChange([...items]);
            },
            child: const Icon(
              FeatherIcons.trash2,
              color: PartnerAppColors.red,
            ),
          ),
        ),
        const SizedBox(
          height: 5,
        ),
        const Divider(
          color: PartnerAppColors.darkBlue,
        ),
        const SizedBox(
          height: 5,
        ),
      ],
    );
  }
}
