import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/kanikke_settings_response_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

Future<Map<String, dynamic>?> jobUnavailableDialog(
    {required BuildContext context, required PartnerJobModel job}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          GestureDetector(
            onTap: () => Navigator.of(dialogContext).pop(),
            child: Icon(
              Icons.close,
              size: 18,
              color: Colors.black.withValues(alpha: 0.4),
            ),
          )
        ]),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: JobUnavailable(job: job),
          ),
        ),
      );
    },
  );
}

class JobUnavailable extends StatefulWidget {
  const JobUnavailable({super.key, required this.job});

  final PartnerJobModel job;

  @override
  State<JobUnavailable> createState() => _JobUnavailableState();
}

class _JobUnavailableState extends State<JobUnavailable> {
  final formKey = GlobalKey<FormBuilderState>();

  bool showDate = false;
  bool confirmReason = false;
  bool isChecked = false;

  Map<String, dynamic> payload = {};

  @override
  Widget build(BuildContext context) {
    final kanIkkeVm = context.read<KanIkkeSettingsResponseViewModel>();
    return FormBuilder(
      key: formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (confirmReason) ...[
            HtmlWidget(
              kanIkkeVm.isWarningForBlackList
                  ? kanIkkeVm.kanikkeConfirmationWarningStage
                  : kanIkkeVm.kanikkeConfirmationAcceptableStage,
              textStyle:
                  context.pttTitleSmall.copyWith(fontWeight: FontWeight.w400),
            ),
            SmartGaps.gapH20,
            Divider(
              color: Colors.black.withValues(alpha: 0.5),
              thickness: 0.8,
            ),
            SmartGaps.gapH20,
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Checkbox(
                  value: isChecked,
                  activeColor: PartnerAppColors.blue,
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  onChanged: (val) {
                    setState(() {
                      isChecked = (val ?? false);
                    });
                  },
                ),
                SmartGaps.gapW10,
                Expanded(
                  child: Text(
                    tr('important_info_confirmation'),
                    style: context.pttTitleSmall.copyWith(
                      fontWeight: FontWeight.w400,
                      color: PartnerAppColors.grey1,
                    ),
                  ),
                ),
              ],
            ),
            SmartGaps.gapH30,
            CustomDesignTheme.flatButtonStyle(
              height: 50,
              backgroundColor: isChecked
                  ? PartnerAppColors.malachite
                  : PartnerAppColors.spanishGrey,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),
              onPressed: () async {
                modalManager.hideLoadingModal();
                Navigator.of(context).pop(payload);
              },
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text(
                  tr('submit'),
                  style: Theme.of(context).textTheme.displayLarge!.copyWith(
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                      ),
                ),
                SmartGaps.gapW10,
                const Icon(
                  Icons.arrow_forward,
                  color: Colors.white,
                )
              ]),
            )
          ] else ...[
            Text(
              tr('quality_assurance'),
              style: context.pttTitleMedium.copyWith(
                  color: PartnerAppColors.blue, fontWeight: FontWeight.bold),
            ),
            SmartGaps.gapH20,
            CustomDropdownFormBuilder(
                labelText: tr('select_why_an_offer_is_unavailable'),
                validator:
                    FormBuilderValidators.required(errorText: tr('required')),
                items: [
                  ...(widget.job.cancelStatus ?? []).map((value) {
                    return DropdownMenuItem(
                      value: '${value.id}',
                      child: Text(
                          context.locale.languageCode == 'da'
                              ? (value.titleDK ?? '')
                              : (value.titleEN ?? ''),
                          style: Theme.of(context)
                              .textTheme
                              .bodyMedium!
                              .copyWith(fontWeight: FontWeight.w500)),
                    );
                  })
                  // DropdownMenuItem(
                  //   value: '1',
                  //   child: Text(tr('customer_doesnt_want'),
                  //       style: Theme.of(context)
                  //           .textTheme
                  //           .bodyMedium!
                  //           .copyWith(fontWeight: FontWeight.w500)),
                  // ),
                  // DropdownMenuItem(
                  //   value: '2',
                  //   child: Text(tr('the_customer_does_not_to_have_it_made'),
                  //       style: Theme.of(context)
                  //           .textTheme
                  //           .bodyMedium!
                  //           .copyWith(fontWeight: FontWeight.w500)),
                  // ),
                  // DropdownMenuItem(
                  //   value: '3',
                  //   child: Text(tr('project_should_done_future'),
                  //       style: Theme.of(context)
                  //           .textTheme
                  //           .bodyMedium!
                  //           .copyWith(fontWeight: FontWeight.w500)),
                  // ),
                ],
                onChanged: (p0) {
                  setState(() {
                    showDate = p0 == '3';
                  });
                },
                name: 'reason'),
            if (showDate)
              CustomDatePickerFormBuilder(
                name: 'date',
                labelText: tr('date_job_reminder'),
                initialDate: DateTime.now(),
                dateFormat: DateFormatType.isoDate.formatter,
                validator: FormBuilderValidators.required(),
                prefixIcon: const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  child: Icon(Icons.event, color: PartnerAppColors.blue),
                ),
              ),
            SmartGaps.gapH30,
            CustomDesignTheme.flatButtonStyle(
              height: 50,
              backgroundColor: PartnerAppColors.malachite,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),
              onPressed: () async {
                if (formKey.currentState!.validate()) {
                  final tempPayload = {
                    'cancelStatusId':
                        formKey.currentState!.fields['reason']!.value
                  };
                  if (showDate) {
                    tempPayload.putIfAbsent(
                      'reminderDate',
                      () => Formatter.formatDateTime(
                          type: DateFormatType.isoDate,
                          date: (formKey.currentState!.fields['date']!.value
                              as DateTime)),
                    );
                  }

                  setState(() {
                    confirmReason = true;
                    payload = tempPayload;
                  });
                }
              },
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text(
                  tr('next'),
                  style: Theme.of(context).textTheme.displayLarge!.copyWith(
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                      ),
                ),
                SmartGaps.gapW10,
                const Icon(
                  Icons.arrow_forward,
                  color: Colors.white,
                )
              ]),
            )
          ]
        ],
      ),
    );
  }
}
