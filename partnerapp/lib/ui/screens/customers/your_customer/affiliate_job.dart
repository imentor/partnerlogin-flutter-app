import 'package:Haandvaerker.dk/model/affiliate_job/affiliate_job_model.dart';
import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/signature_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/job_notes.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/launcher.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

Future<void> affiliateJobDetailDialog({
  required BuildContext context,
  required AffiliateJob job,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: AffiliateJobDetail(
              job: job,
            ),
          ),
        ),
      );
    },
  );
}

class AffiliateJobDetail extends StatefulWidget {
  const AffiliateJobDetail({
    super.key,
    required this.job,
  });

  final AffiliateJob job;

  @override
  AffiliateJobDetailState createState() => AffiliateJobDetailState();
}

class AffiliateJobDetailState extends State<AffiliateJobDetail> {
  @override
  build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SvgPicture.asset(
              SvgIcons.calendarCheck,
              width: 40,
              height: 40,
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(
                  color: PartnerAppColors.darkBlue,
                ),
              ),
              child: Row(
                children: [
                  const SizedBox(
                    width: 20,
                  ),
                  Text(
                    widget.job.status ?? '',
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          color: PartnerAppColors.darkBlue,
                          fontWeight: FontWeight.w600,
                        ),
                  ),
                ],
              ),
            )
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        Container(
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
              border: Border.all(
                color: PartnerAppColors.blue,
              ),
              borderRadius: BorderRadius.circular(5),
              color: PartnerAppColors.blue.withValues(alpha: .1)),
          child: Text(
            'Dinero-Kunde',
            style: Theme.of(context)
                .textTheme
                .titleSmall!
                .copyWith(color: PartnerAppColors.blue),
          ),
        ),
        const SizedBox(
          height: 15,
        ),
        Text(
          widget.job.contactName ?? '',
          style: context.pttTitleMedium.copyWith(
            fontWeight: FontWeight.w600,
          ),
        ),
        if ((widget.job.mobile ?? '').isNotEmpty) ...[
          SmartGaps.gapH15,
          GestureDetector(
            onTap: () async {
              Launcher().launchPhoneCall((widget.job.mobile ?? ''));
              Navigator.of(context).pop();
            },
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  'assets/images/phone-contact.svg',
                  width: 13,
                ),
                SmartGaps.gapW10,
                Text(
                  '+45 ${(widget.job.mobile ?? '')}',
                  style: context.pttBodySmall.copyWith(
                    color: PartnerAppColors.darkBlue,
                  ),
                ),
              ],
            ),
          ),
        ],
        if ((widget.job.email ?? '').isNotEmpty) ...[
          const SizedBox(
            height: 15,
          ),
          GestureDetector(
            onTap: () {
              Launcher().launchEmail((widget.job.email ?? ''));
            },
            child: Row(
              children: [
                SvgPicture.asset(
                  'assets/images/mail-blue.svg',
                  width: 15,
                ),
                const SizedBox(
                  width: 10,
                ),
                Text(
                  tr('send_email'),
                  style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        height: 1,
                        color: PartnerAppColors.darkBlue,
                      ),
                ),
              ],
            ),
          ),
        ],
        SmartGaps.gapH30,
        Text(
          widget.job.offerTitle ?? '',
          style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                fontSize: 18,
                fontWeight: FontWeight.w500,
                height: 1,
              ),
        ),
        SmartGaps.gapH10,
        Divider(
          color: Theme.of(context)
              .colorScheme
              .onTertiaryContainer
              .withValues(alpha: 0.4),
          thickness: 1,
        ),
        SmartGaps.gapH10,
        CustomDesignTheme.flatButtonStyle(
          height: 50,
          backgroundColor: PartnerAppColors.darkBlue,
          onPressed: () async {
            final termVm = context.read<SharedDataViewmodel>();
            final clientsVm = context.read<ClientsViewModel>();

            if ((widget.job.project ?? '').isNotEmpty) {
              final signature = await signatureDialog(
                  context: context,
                  label: termVm.termsLabel,
                  htmlTerms: termVm.termsHtml);

              if (!context.mounted) return;

              if (signature != null) {
                Navigator.of(context).pop();

                clientsVm.getYourCustomerData = true;

                await tryCatchWrapper(
                        context: context,
                        function: clientsVm.acceptAffiliateJob(
                            affiliateProjectId: '${widget.job.id}'))
                    .then((value) async {
                  if ((value ?? false)) {
                    await clientsVm.getAffiliateJobs().whenComplete(() {
                      clientsVm.getYourCustomerData = false;

                      commonSuccessOrFailedDialog(
                          context: myGlobals.homeScaffoldKey!.currentContext!,
                          successMessage: tr('affiliate_accept_job'));
                    });
                  } else {
                    clientsVm.getYourCustomerData = false;
                  }
                });
              }
            }
          },
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Flexible(
              child: FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  'Send tilbud - Sikker betaling',
                  style: context.pttBodySmall.copyWith(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ]),
        ),
        const SizedBox(
          height: 10,
        ),
        CustomDesignTheme.flatButtonStyle(
          height: 50,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(2),
            side: BorderSide(
              color: Theme.of(context).colorScheme.onSurfaceVariant,
            ),
          ),
          onPressed: () {
            jobNotesDialog(
                context: context,
                job:
                    PartnerJobModel(id: widget.job.id, notes: widget.job.notes),
                isAffiliate: true);
          },
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            SvgPicture.asset(
              'assets/images/note.svg',
              alignment: Alignment.centerRight,
              width: 20,
            ),
            SmartGaps.gapW10,
            Flexible(
              child: FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  tr('your_notes').toUpperCase(),
                  textAlign: TextAlign.start,
                  style: context.pttBodySmall.copyWith(
                    fontWeight: FontWeight.w600,
                    color: PartnerAppColors.darkBlue,
                  ),
                ),
              ),
            )
          ]),
        ),
      ],
    );
  }
}

class AffiliateJobCard extends StatelessWidget {
  const AffiliateJobCard({
    super.key,
    required this.job,
  });

  final AffiliateJob job;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => affiliateJobDetailDialog(context: context, job: job),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
          side: BorderSide(
            color: PartnerAppColors.darkBlue.withValues(alpha: .2),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SvgPicture.asset(
                    SvgIcons.calendarCheck,
                    width: 26,
                  ),
                  Text(
                    job.status ?? '',
                    style: context.pttBodySmall.copyWith(
                      fontSize: 11,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.w600,
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                    border: Border.all(
                      color: PartnerAppColors.blue,
                    ),
                    borderRadius: BorderRadius.circular(5),
                    color: PartnerAppColors.blue.withValues(alpha: .1)),
                child: Text(
                  'Dinero-Kunde',
                  style: Theme.of(context)
                      .textTheme
                      .titleSmall!
                      .copyWith(color: PartnerAppColors.blue),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Text(
                job.contactName ?? '',
                style: context.pttBodySmall.copyWith(
                  color: PartnerAppColors.darkBlue,
                  fontWeight: FontWeight.w600,
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              Divider(
                color: Colors.black.withValues(alpha: 0.5),
                thickness: 0.8,
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                job.offerTitle ?? '',
                overflow: TextOverflow.ellipsis,
                maxLines: 3,
                style: context.pttBodySmall.copyWith(
                  color: const Color(0xff134553),
                  height: 1.5,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
