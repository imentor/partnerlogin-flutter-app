import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BlacklistPopUpReserve extends StatelessWidget {
  const BlacklistPopUpReserve(this.partnerName, {super.key});
  final String partnerName;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      backgroundColor: Colors.white,
      titlePadding: EdgeInsets.zero,
      contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 35),
      insetPadding: const EdgeInsets.fromLTRB(20, 190, 20, 330),
      title: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Align(
          alignment: Alignment.bottomRight,
          child: IconButton(
            onPressed: () {
              backDrawerRoute();
              Navigator.pop(context);
            },
            icon: Stack(
              alignment: Alignment.center,
              children: [
                Container(
                  width: 23,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black.withValues(alpha: 0.4),
                      width: 2,
                    ),
                    shape: BoxShape.circle,
                  ),
                ),
                Icon(Icons.close,
                    size: 18, color: Colors.black.withValues(alpha: 0.4)),
              ],
            ),
          ),
        ),
      ),
      content: SingleChildScrollView(
        child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
          SvgPicture.asset('assets/images/blocked.svg'),
          SmartGaps.gapH20,
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4),
            child: Text(
              // '${tr('blacklisted_text_marketplace').split(';').first} $partnerName ${tr('blacklisted_text_marketplace').split(';').last}',
              context.locale.languageCode == 'da'
                  ? 'Hej $partnerName${tr('blacklisted_text_my_customers')}'
                  : 'Hi $partnerName${tr('blacklisted_text_my_customers')}',
              textAlign: TextAlign.center,
              style: context.pttBodyLarge.copyWith(color: Colors.black),
            ),
          ),
          SmartGaps.gapH20,
          CustomDesignTheme.flatButtonStyle(
            width: MediaQuery.of(context).size.width,
            height: 50,
            backgroundColor: PartnerAppColors.malachite,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            onPressed: () {
              backDrawerRoute();
              Navigator.pop(context);
            },
            child: Text(tr('give_me_a_call'),
                style: Theme.of(context).textTheme.displayLarge!.copyWith(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                    )),
          ),
        ]),
      ),
    );
  }
}
