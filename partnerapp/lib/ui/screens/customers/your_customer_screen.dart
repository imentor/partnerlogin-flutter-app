import 'dart:async';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/number_pagination.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/components/blacklist_popup.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/affiliate_job.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/invite/invite_card.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/job_card.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/job_detail.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/your_customer_search.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/your_customer_sort_filter.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/newsfeed/latest_job_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/upload_offer/create_upload_offer/upload_offer_page.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/fake_data.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/app_links/app_links_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/kanikke_settings_response_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/widgets_recommendations_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:url_launcher/url_launcher.dart';

Future<void> getClientsGeneralFunction({required BuildContext context}) async {
  final clientsVm = context.read<ClientsViewModel>();
  final tenderFolderVm = context.read<TenderFolderViewModel>();
  final createOfferVm = context.read<CreateOfferViewModel>();

  await tryCatchWrapper(
      context: context,
      function: clientsVm.getClients(
        tenderVm: tenderFolderVm,
        createOfferVm: createOfferVm,
      ));

  // if (createOfferVm.isNewReserve && context.mounted) {
  //   await showDialog(
  //       context: context,
  //       barrierDismissible: false,
  //       barrierColor: Colors.black54,
  //       builder: (context) {
  //         return const CreateOfferVideoDialog();
  //       });
  //   createOfferVm.isNewReserve = false;
  // }
}

class YourCustomerScreen extends StatefulWidget {
  const YourCustomerScreen(
      {super.key,
      required this.isFromAppLink,
      this.jobId,
      required this.skipStory});

  final bool isFromAppLink;
  final int? jobId;
  final bool skipStory;

  @override
  YourCustomerScreenState createState() => YourCustomerScreenState();
}

class YourCustomerScreenState extends State<YourCustomerScreen> {
  //

  late ScrollController scroll;
  Timer? _debounce;
  final Duration _debouceDuration = const Duration(milliseconds: 600);

  Future<void> getPartnerJobsAndKanIkkeSettings() async {
    final clientsVm = context.read<ClientsViewModel>();
    final kanIkkeVm = context.read<KanIkkeSettingsResponseViewModel>();
    clientsVm.getYourCustomerData = true;

    await tryCatchWrapper(
      context: myGlobals.homeScaffoldKey?.currentContext ?? context,
      function: Future.wait([
        clientsVm.getNewAndOldJobsInitial(),
        kanIkkeVm.getKanIkkeSettings(),
      ]).whenComplete(() async {
        clientsVm.initialClientsLoad = false;
        clientsVm.getYourCustomerData = false;
      }),
    );
  }

  Future<void> dynamicStory() async {
    final appDrawerVm = context.read<AppDrawerViewModel>();
    final newsFeedVm = context.read<NewsFeedViewModel>();
    const dynamicStoryPage = 'mycustomer';
    final dynamicStoryEnable =
        (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

    if (dynamicStoryEnable) {
      await Future.value(newsFeedVm.getDynamicStory(page: dynamicStoryPage))
          .whenComplete(() {
        if (!mounted) return;

        newsFeedVm.dynamicStoryFunction(
            dynamicStoryPage: dynamicStoryPage,
            dynamicStoryEnable: dynamicStoryEnable,
            pageContext: context);
      });
    }
  }

  void launchUrlOnTap({required String redirectUrl}) async {
    await launchUrl(Uri.parse(redirectUrl)).whenComplete(() {
      if (!mounted) return;
      Navigator.of(context).pop();
    });
  }

  @override
  void initState() {
    scroll = ScrollController();
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final createOfferVm = context.read<CreateOfferViewModel>();
      final supplierInfo = context.read<WidgetsViewModel>().company;
      final clientsVm = context.read<ClientsViewModel>();
      final appLinksVm = context.read<AppLinksViewModel>();
      final offerVm = context.read<OfferViewModel>();
      final newsfeedVm = context.read<NewsFeedViewModel>();
      final appDrawerVm = context.read<AppDrawerViewModel>();

      //INITIALIZE SEARCH VALUES
      clientsVm.setSearchControllers();
      // INITIALIZE FILTER VALUES
      clientsVm.initializeSortValues();
      // Gets the partner jobs and kan ikke settings
      if (clientsVm.initialClientsLoad) {
        getPartnerJobsAndKanIkkeSettings();
      }
      // Gets the dynamic story
      dynamicStory();

      if (createOfferVm.popMyCustomer) {
        Navigator.pop(context);
        createOfferVm.setValuesToDefault();

        clientsVm.getYourCustomerData = true;
        await clientsVm.getPartnerJobsV2();
        clientsVm.getYourCustomerData = false;
      }

      if (supplierInfo != null) {
        final partnerName = supplierInfo.bCrmCompany?.companyName ?? '';
        if ((supplierInfo.bCrmCompany?.blackListed ?? 0) == 1 && mounted) {
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (context) {
                return PopScope(
                  canPop: true,
                  child: BlacklistPopUpReserve(partnerName),
                );
              });
        }
      }

      clientsVm.changeCustomerYearFilter(DateTime.now().year);
      clientsVm.currentPage = 1;

      // Offer Due Date - gets selected jobId from news feed and sets selected job in clientsVm
      if (widget.jobId != null) {
        clientsVm.getYourCustomerData = true;

        if (supplierInfo != null &&
            (supplierInfo.bCrmCompany?.blackListed ?? 0) != 1 &&
            mounted) {
          if (widget.jobId != null) {
            clientsVm.setJobIdFromNewsFeed(widget.jobId ?? 0);
            await clientsVm
                .getSelectedJobFromNewsFeed(
                    jobIdFromNewsFeed: widget.jobId ?? 0)
                .whenComplete(() async {
              if (!mounted) return;

              if (clientsVm.selectedJobFromNewsFeed?.id != null) {
                clientsVm.getYourCustomerData = false;
                jobDetailDialog(
                    context: context,
                    job: clientsVm.selectedJobFromNewsFeed,
                    jobIndex: 0,
                    oldJob: null);
              } else {
                clientsVm.getYourCustomerData = false;
              }
            });
          }
        } else {
          clientsVm.getYourCustomerData = false;
        }
      }

      if (!clientsVm.fromNewsFeed && mounted) {
        clientsVm.getYourCustomerData = true;
        await getClientsGeneralFunction(context: context);
        clientsVm.getYourCustomerData = false;
      }

      if (widget.isFromAppLink && appLinksVm.projectId.isNotEmpty) {
        clientsVm.getYourCustomerData = true;
        await clientsVm
            .getPartnerJobByProjectId(
                projectId: int.parse(appLinksVm.projectId))
            .then((value) {
          if (!mounted) return;

          if (value != null && value.isNotEmpty) {
            switch (appLinksVm.pageRedirect) {
              case 'minekunder':
                clientsVm.getYourCustomerData = false;
                createOfferVm.isNewOfferVersion = false;
                createOfferVm.toUpdateOffer = false;

                changeDrawerRoute(
                  Routes.createOffer,
                  arguments: {
                    'job': value.first,
                  },
                );

                appLinksVm.resetLoginAppLink();
                break;
              case 'offerByCs':
                chooseOfferDialog(
                    context: context,
                    createOffer: () {
                      clientsVm.getYourCustomerData = false;
                      createOfferVm.isNewOfferVersion = true;
                      createOfferVm.toUpdateOffer = false;
                      createOfferVm.csUserId = appLinksVm.csUserId;

                      if (appLinksVm.offerVersion.isNotEmpty) {
                        createOfferVm.offerVersionId =
                            int.parse(appLinksVm.offerVersion);
                      }

                      changeDrawerRoute(
                        Routes.createOffer,
                        arguments: {
                          'job': value.first,
                        },
                      );
                    },
                    uploadOffer: () {
                      offerVm.isUploadOfferUpdate = false;
                      offerVm.isNewOfferVersion = true;
                      offerVm.csUserId = appLinksVm.csUserId;

                      if (appLinksVm.offerVersion.isNotEmpty) {
                        offerVm.offerVersionId =
                            int.parse(appLinksVm.offerVersion);
                      }

                      changeDrawerRoute(Routes.uploadOffer,
                          arguments: value.first);
                    });
                break;
              default:
            }
          }
        });
      }

      if ((appDrawerVm.bitrixCompanyModel?.deadlineOffer ?? 0) == 1 &&
          newsfeedVm.latestJobFeedForPopup.isNotEmpty &&
          widget.jobId == null) {
        if (!mounted) return;

        await latestJobDialog(context: context, fromCustomer: true)
            .then((onValue) async {
          if (onValue != null) {
            clientsVm.getYourCustomerData = true;

            if (supplierInfo != null &&
                (supplierInfo.bCrmCompany?.blackListed ?? 0) != 1 &&
                mounted) {
              clientsVm.setJobIdFromNewsFeed(onValue);
              await clientsVm
                  .getSelectedJobFromNewsFeed(jobIdFromNewsFeed: onValue)
                  .whenComplete(() async {
                if (!mounted) return;

                if (clientsVm.selectedJobFromNewsFeed?.id != null) {
                  clientsVm.getYourCustomerData = false;
                  jobDetailDialog(
                      context: context,
                      job: clientsVm.selectedJobFromNewsFeed,
                      jobIndex: 0,
                      oldJob: null);
                } else {
                  clientsVm.getYourCustomerData = false;
                }
              });
            } else {
              clientsVm.getYourCustomerData = false;
            }
          }
        });
      }
    });
  }

  @override
  void dispose() {
    scroll.dispose();
    _debounce?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final appDrawerVm = context.watch<AppDrawerViewModel>();
    return Scaffold(
      body: NestedScrollView(
        controller: scroll,
        floatHeaderSlivers: true,
        headerSliverBuilder: (context, _) {
          return [
            const DrawerAppBar(
              isSliver: true,
            )
          ];
        },
        body: Consumer<ClientsViewModel>(builder: (_, clientsVm, __) {
          return GestureDetector(
            onTap: () => (clientsVm.searchFocusNode ?? FocusNode()).unfocus(),
            child: Container(
              height: double.infinity,
              padding: const EdgeInsets.only(top: 10, left: 20.0, right: 20.0),
              child: RefreshIndicator(
                onRefresh: () async {
                  clientsVm.getYourCustomerData = true;
                  await getClientsGeneralFunction(context: context);
                  clientsVm.getYourCustomerData = false;
                },
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      //

                      Tooltip(
                          message:
                              '${tr('your_customer_detail_1')}\n\n${tr('your_customer_detail_2')}',
                          triggerMode: TooltipTriggerMode.tap,
                          margin: const EdgeInsets.all(20),
                          padding: const EdgeInsets.all(20),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                color: PartnerAppColors.darkBlue,
                              ),
                              borderRadius: BorderRadius.circular(5)),
                          textStyle: Theme.of(context)
                              .textTheme
                              .headlineSmall!
                              .copyWith(
                                  color: PartnerAppColors.darkBlue,
                                  fontSize: 14,
                                  fontWeight: FontWeight.normal),
                          child: Row(children: [
                            Text(tr('my_customers'),
                                style: context.pttTitleLarge),
                            SmartGaps.gapW5,
                            const Icon(
                              FeatherIcons.alertCircle,
                              color: PartnerAppColors.spanishGrey,
                            )
                          ])),

                      SmartGaps.gapH20,

                      YourCustomerSearch(
                        onChanged: (val) {
                          if (_debounce?.isActive ?? false) _debounce?.cancel();
                          _debounce = Timer(_debouceDuration, () async {
                            clientsVm.getYourCustomerData = true;
                            clientsVm.currentPage = 1;

                            await tryCatchWrapper(
                                context: context,
                                function: Future.wait([
                                  clientsVm.getPartnerJobsV2(),
                                  clientsVm.getOldPartnerJobs(),
                                ]));

                            clientsVm.getYourCustomerData = false;

                            scroll.animateTo(0,
                                duration: const Duration(milliseconds: 700),
                                curve: Curves.easeOut);
                          });
                        },
                        onClear: () async {
                          clientsVm.searchController?.clear();
                          clientsVm.searchFocusNode?.unfocus();
                          clientsVm.currentPage = 1;
                          clientsVm.getYourCustomerData = true;

                          await tryCatchWrapper(
                            context: context,
                            function: Future.wait(
                              [
                                clientsVm.getPartnerJobsV2(),
                                clientsVm.getOldPartnerJobs(),
                              ],
                            ),
                          );

                          clientsVm.getYourCustomerData = false;

                          scroll.animateTo(0,
                              duration: const Duration(milliseconds: 700),
                              curve: Curves.easeOut);
                        },
                      ),

                      SmartGaps.gapH20,

                      const YourCustomerSortFilter(),

                      SmartGaps.gapH20,

                      // customer information container
                      Flexible(
                        child: jobs(
                            clientsVm: clientsVm, appDrawerVm: appDrawerVm),
                      ),

                      SmartGaps.gapH20,
                      NumberPagination(
                        currentPage: clientsVm.currentPage,
                        totalPages: clientsVm.lastPage,
                        onPageChanged: (p0) async {
                          if (clientsVm.fromNewsFeed) {
                            clientsVm.fromNewsFeed = false;
                          }

                          scroll.animateTo(0,
                              duration: const Duration(milliseconds: 700),
                              curve: Curves.easeOut);
                          clientsVm.currentPage = p0;
                          clientsVm.getYourCustomerData = true;
                          await tryCatchWrapper(
                            context:
                                myGlobals.homeScaffoldKey?.currentContext ??
                                    context,
                            function: clientsVm.getPartnerJobsV2(),
                          ).whenComplete(
                            () {
                              clientsVm.getYourCustomerData = false;
                            },
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }

  Widget jobs(
      {required ClientsViewModel clientsVm,
      required AppDrawerViewModel appDrawerVm}) {
    return Skeletonizer(
        enabled: clientsVm.getYourCustomerData,
        child: Column(
          children: [
            GridView.count(
              crossAxisCount: 2,
              crossAxisSpacing: 5,
              childAspectRatio: 0.9,
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              children: clientsVm.getYourCustomerData
                  ? List.generate(
                      10,
                      (index) => JobCard(
                            job: fakeData.partnerJob,
                            jobIndex: 1,
                          ))
                  : [
                      if (clientsVm.currentPage == 1) ...[
                        ...clientsVm.affiliateJobs.map((job) {
                          return AffiliateJobCard(job: job);
                        }),
                        ...clientsVm.subContractorInvitations.map((invite) {
                          return InviteCard(
                            invitation: invite,
                          );
                        }),
                      ],
                      ...clientsVm.partnerJobs.map(
                        (job) => JobCard(
                          job: job,
                          jobIndex: clientsVm.partnerJobs.indexOf(job),
                          // jobId: clientsVm.jobIdFromNewsFeed,
                        ),
                      ),
                      if (clientsVm.oldPartnerJobs.isNotEmpty &&
                          clientsVm.lastPage == clientsVm.currentPage)
                        ...clientsVm.oldPartnerJobs.map(
                          (oldJob) => JobCard(
                            oldJob: oldJob,
                            jobIndex: 0,
                          ),
                        ),
                    ],
            ),
            if (!clientsVm.getYourCustomerData &&
                clientsVm.partnerJobs.isEmpty &&
                clientsVm.oldPartnerJobs.isEmpty) ...[
              if ((appDrawerVm.bitrixCompanyModel?.companyPackge ?? '') ==
                  'affiliatepartner') ...[
                SizedBox(
                    height: 300,
                    child: Center(
                        child: Column(children: [
                      const EmptyListIndicator(),
                      Text(
                          '${tr('hey')} ${appDrawerVm.bitrixCompanyModel?.companyName ?? ''},${tr('dinero_empty_mine_kunder')}',
                          textAlign: TextAlign.center,
                          style: Theme.of(context)
                              .textTheme
                              .titleSmall!
                              .copyWith(
                                  color: Colors.grey[400],
                                  fontWeight: FontWeight.w600)),
                      SmartGaps.gapH25,
                      CustomDesignTheme.flatButtonStyle(
                          height: 50,
                          width: double.infinity,
                          backgroundColor:
                              Theme.of(context).colorScheme.onSurfaceVariant,
                          child: Text(tr('log_in_to_dinero'),
                              style: context.pttBodySmall.copyWith(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18,
                                  color: Colors.white,
                                  letterSpacing: 0.5)),
                          onPressed: () {
                            launchUrlOnTap(
                                redirectUrl:
                                    'https://connect.visma.com/?returnUrl=%2Fconnect%2Fauthorize%2Fcallback%3Fclient_id%3Ddinero%26redirect_uri%3Dhttps%253A%252F%252Fapp.dinero.dk%252Fsignin-oidc%26response_type%3Dcode%2520id_token%26scope%3Dopenid%2520profile%2520email%2520roles%26response_mode%3Dform_post%26nonce%3D638624180533000771.OWUwZTU5NGQtMDM4Zi00YmRjLWI3ZWMtYmRiMmMzYmZhZjBjOGRiOWM3MjYtOGVhOC00YWZjLTgxOTUtOTJmYjI4OWJjOTEx%26state%3DCfDJ8N0wv-Ekr1hBgNlqS3YOmQR-wALWwHBVqUw1HH0FiIOZmwByyUMhQgSgzueWiy_vAUoXhfAxiuJsUz9459CSKcUP5olbdzjSLXk9vFhXSimi6XreiMvooeczVoBJHvrsSBM4Rg8dfMk5JxMEx7dXdGlmj9ib_t6k6CyvWVnL55M2wmSot0F2wouP3-0oZafcRtvbaB33hYVGBNd8uYR7rs60Dd18dskv2z45o8zX4qN6SmZqrLK3qhR6Kpt8OvL_vPvqAgZ7_yoXW4a9Pt0muaMIXk9VHaYrgtHQUaRARzMk%26x-client-SKU%3DID_NET8_0%26x-client-ver%3D7.1.2.0');
                          })
                    ])))
              ] else ...[
                SizedBox(
                  height: 300,
                  child: Center(
                    child: EmptyListIndicator(
                      message: tr('empty_jobs'),
                    ),
                  ),
                )
              ]
            ],
          ],
        ));
  }
}
