import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/components/invitation_button_nav.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/subcontractor/steps/subcontractor_step_1.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/subcontractor/steps/subcontractor_step_2.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/subcontractor/steps/subcontractor_step_3.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/subcontractor/steps/subcontractor_step_4.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/subcontractor/steps/subcontractor_step_5.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/invitation_subcontractor_manufacturer/invitation_subcontractor_manufacturer_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class InvitationSubcontractorScreen extends StatefulWidget {
  const InvitationSubcontractorScreen({super.key});

  @override
  State<InvitationSubcontractorScreen> createState() =>
      _InvitationSubcontractorScreenState();
}

class _InvitationSubcontractorScreenState
    extends State<InvitationSubcontractorScreen> {
  final formKey = GlobalKey<FormBuilderState>();

  @override
  void dispose() {
    if (formKey.currentState != null) {
      formKey.currentState!.reset();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<InvitationSubcontractorManufacturerViewmodel>(
        builder: (_, invitationVm, __) {
      return Scaffold(
        appBar: const DrawerAppBar(),
        bottomNavigationBar: InvitationButtonNav(
          invitationVm: invitationVm,
          invitationType: 'subContractor',
          formKey: formKey,
        ),
        body: SingleChildScrollView(
          physics: (invitationVm.currentFormStep + 1) == 5
              ? const NeverScrollableScrollPhysics()
              : null,
          padding: const EdgeInsets.all(20),
          child: FormBuilder(
            key: formKey,
            child: Skeletonizer(
              enabled: invitationVm.busy,
              child: formSteps(invitationVm),
            ),
          ),
        ),
      );
    });
  }

  Widget formSteps(InvitationSubcontractorManufacturerViewmodel invitationVm) {
    switch (invitationVm.currentFormStep + 1) {
      case 1:
        return const SubcontractorStep1();
      case 2:
        return SubcontractorStep2(
          invitationVm: invitationVm,
          formKey: formKey,
        );
      case 3:
        return SubcontractorStep3(
          invitationVm: invitationVm,
        );
      case 4:
        return SubcontractorStep4(
          invitationVm: invitationVm,
        );
      case 5:
        return SubcontractorStep5(
          invitationVm: invitationVm,
        );
      default:
        return SizedBox.fromSize();
    }
  }
}
