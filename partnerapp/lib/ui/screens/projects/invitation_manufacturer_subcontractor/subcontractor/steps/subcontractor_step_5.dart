import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/invitation_subcontractor_manufacturer/invitation_subcontractor_manufacturer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:provider/provider.dart';

class SubcontractorStep5 extends StatelessWidget {
  const SubcontractorStep5({super.key, required this.invitationVm});

  final InvitationSubcontractorManufacturerViewmodel invitationVm;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('subcontractor_contract'),
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        const SizedBox(
          height: 20,
        ),
        Text(
          tr('subcontractor_contract_description'),
          style: Theme.of(context).textTheme.titleMedium,
        ),
        const SizedBox(
          height: 20,
        ),
        Container(
          height: MediaQuery.of(context).size.height / 2,
          padding: const EdgeInsets.all(10),
          color: PartnerAppColors.spanishGrey.withValues(alpha: .3),
          child: (invitationVm.formValues['inviteResponse'] != null &&
                  invitationVm.formValues['inviteResponse']['ID'] != null)
              ? InAppWebView(
                  initialSettings:
                      InAppWebViewSettings(transparentBackground: false),
                  initialUrlRequest: URLRequest(
                      url: WebUri(
                          '${applic.bitrixApiUrl}partner/subContractor/GetSubContractPDFDownloadT?SUB_CONTRACT_ID=${invitationVm.formValues['inviteResponse']['ID']}&FORMAT=PDF'),
                      headers: context.read<SharedDataViewmodel>().headers),
                )
              : null,
        ),
      ],
    );
  }
}
