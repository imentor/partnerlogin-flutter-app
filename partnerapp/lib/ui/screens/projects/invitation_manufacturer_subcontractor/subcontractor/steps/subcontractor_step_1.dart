import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class SubcontractorStep1 extends StatelessWidget {
  const SubcontractorStep1({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('subcontractor_information'),
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        const SizedBox(
          height: 20,
        ),
        CustomTextFieldFormBuilder(
          name: 'inviteFirstName',
          labelText: tr('first_name'),
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
        CustomTextFieldFormBuilder(
          name: 'inviteLastName',
          labelText: tr('last_name'),
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
        CustomTextFieldFormBuilder(
          name: 'inviteEmail',
          labelText: tr('email'),
          keyboardType: TextInputType.emailAddress,
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
      ],
    );
  }
}
