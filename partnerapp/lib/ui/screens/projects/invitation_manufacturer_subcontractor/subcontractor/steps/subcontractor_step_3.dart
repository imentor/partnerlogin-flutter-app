import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/invitation_subcontractor_manufacturer/invitation_subcontractor_manufacturer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class SubcontractorStep3 extends StatelessWidget {
  const SubcontractorStep3({super.key, required this.invitationVm});

  final InvitationSubcontractorManufacturerViewmodel invitationVm;

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      List<Map<String, dynamic>> filteredListPrice = [
        ...invitationVm.listPrices.where((value) => value['price'] > 0)
      ];

      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            tr('summary'),
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          const SizedBox(
            height: 20,
          ),
          ...filteredListPrice.map((listPrice) {
            final index = filteredListPrice.indexOf(listPrice);

            return Container(
              margin: const EdgeInsets.only(bottom: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '${index + 1}. ${listPrice['title']}',
                        style: Theme.of(context).textTheme.headlineMedium,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            Formatter.curencyFormat(amount: listPrice['price']),
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium!
                                .copyWith(
                                  fontWeight: FontWeight.normal,
                                ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            tr('total_price'),
                            style: Theme.of(context)
                                .textTheme
                                .titleSmall!
                                .copyWith(
                                  color: PartnerAppColors.spanishGrey,
                                ),
                          ),
                        ],
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  ...((listPrice['descriptions'] as List<Map<String, dynamic>>)
                          .where((description) => description['isSelected']))
                      .map((descriptionMap) {
                    final indexDescription = (listPrice['descriptions']
                            as List<Map<String, dynamic>>)
                        .indexOf(descriptionMap);

                    return Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${index + 1}.${indexDescription + 1} ${descriptionMap['title']}',
                            style: Theme.of(context).textTheme.titleSmall,
                          ),
                        ],
                      ),
                    );
                  }),
                  const SizedBox(
                    height: 10,
                  ),
                  const Divider(
                    color: PartnerAppColors.darkBlue,
                  ),
                ],
              ),
            );
          })
        ],
      );
    });
  }
}
