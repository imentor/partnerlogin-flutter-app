import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/debouncer.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/invitation_subcontractor_manufacturer/invitation_subcontractor_manufacturer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class SubcontractorStep2 extends StatefulWidget {
  const SubcontractorStep2(
      {super.key, required this.invitationVm, required this.formKey});

  final InvitationSubcontractorManufacturerViewmodel invitationVm;
  final GlobalKey<FormBuilderState> formKey;

  @override
  SubcontractorStep2State createState() => SubcontractorStep2State();
}

class SubcontractorStep2State extends State<SubcontractorStep2> {
  final Debouncer debouncer = Debouncer(milliseconds: 500);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('select_subtask_price'),
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        const SizedBox(
          height: 20,
        ),
        ...widget.invitationVm.listPrices.map((listPrice) {
          final index = widget.invitationVm.listPrices.indexOf(listPrice);

          return Container(
            margin: const EdgeInsets.only(bottom: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '${index + 1}. ${listPrice['title']}',
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          Formatter.curencyFormat(amount: listPrice['price']),
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(
                                fontWeight: FontWeight.normal,
                              ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          tr('total_price'),
                          style:
                              Theme.of(context).textTheme.titleSmall!.copyWith(
                                    color: PartnerAppColors.spanishGrey,
                                  ),
                        ),
                      ],
                    )
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                ...(listPrice['descriptions'] as List<Map<String, dynamic>>)
                    .map((descriptionMap) {
                  final indexDescription =
                      (listPrice['descriptions'] as List<Map<String, dynamic>>)
                          .indexOf(descriptionMap);

                  return Container(
                    margin: const EdgeInsets.symmetric(vertical: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                SizedBox(
                                  height: 24,
                                  width: 24,
                                  child: Checkbox(
                                      value:
                                          descriptionMap['isSelected'] as bool,
                                      side: WidgetStateBorderSide.resolveWith(
                                        (states) => BorderSide(
                                          width: 1.0,
                                          color: descriptionMap['isSelected']
                                              ? PartnerAppColors.darkBlue
                                                  .withValues(alpha: .3)
                                              : PartnerAppColors.spanishGrey,
                                        ),
                                      ),
                                      activeColor: PartnerAppColors.blue,
                                      onChanged: (value) {
                                        widget.invitationVm
                                            .updateSelectedDescriptionListPrice(
                                                index: index,
                                                indexDescription:
                                                    indexDescription,
                                                value: (value ?? false),
                                                whatToUpdate: 'select');
                                      }),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  tr('choose'),
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleSmall!
                                      .copyWith(
                                        color: descriptionMap['isSelected']
                                            ? PartnerAppColors.darkBlue
                                            : PartnerAppColors.spanishGrey,
                                      ),
                                )
                              ],
                            ),
                            SizedBox(
                              width: 150,
                              child: CustomTextFieldFormBuilder(
                                name: '${listPrice['id']}_$indexDescription',
                                textAlign: TextAlign.end,
                                enabled: descriptionMap['isSelected'],
                                initialValue: descriptionMap['price'],
                                keyboardType: TextInputType.number,
                                onChanged: (p0) {
                                  debouncer.run(() {
                                    if ((p0 ?? '').isNotEmpty) {
                                      widget.invitationVm
                                          .updateSelectedDescriptionListPrice(
                                              index: index,
                                              indexDescription:
                                                  indexDescription,
                                              value: p0,
                                              whatToUpdate: 'price');
                                    } else {
                                      widget.invitationVm
                                          .updateSelectedDescriptionListPrice(
                                              index: index,
                                              indexDescription:
                                                  indexDescription,
                                              value: '00',
                                              whatToUpdate: 'price');
                                    }
                                  });
                                },
                                suffixIcon: Container(
                                  padding: const EdgeInsets.only(
                                      right: 20, left: 10, bottom: 12, top: 12),
                                  child: Text(
                                    'kr.',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headlineMedium!
                                        .copyWith(
                                          fontSize: 18,
                                          fontWeight: FontWeight.normal,
                                          color: descriptionMap['isSelected']
                                              ? PartnerAppColors.darkBlue
                                              : PartnerAppColors.spanishGrey,
                                        ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        if (descriptionMap['isEditable'])
                          CustomTextFieldFormBuilder(
                            name:
                                '${listPrice['id']}_${indexDescription}_description',
                            initialValue: descriptionMap['title'],
                            prefixIcon: Container(
                              padding: const EdgeInsets.only(
                                  right: 10, left: 20, bottom: 12, top: 12),
                              child: Text(
                                '${index + 1}.${indexDescription + 1}',
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineMedium!
                                    .copyWith(
                                      fontSize: 18,
                                      fontWeight: FontWeight.normal,
                                      color: descriptionMap['isSelected']
                                          ? PartnerAppColors.darkBlue
                                          : PartnerAppColors.spanishGrey,
                                    ),
                              ),
                            ),
                          )
                        else
                          Text(
                            '${index + 1}.${indexDescription + 1} ${descriptionMap['title']}',
                            style: Theme.of(context).textTheme.titleSmall,
                          ),
                        const SizedBox(
                          height: 20,
                        ),
                        if (descriptionMap['isEditable'])
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              InkWell(
                                onTap: () {
                                  widget.invitationVm
                                      .updateSelectedDescriptionListPrice(
                                          index: index,
                                          indexDescription: indexDescription,
                                          value: false,
                                          whatToUpdate: 'descriptionEditable');
                                },
                                child: RichText(
                                  text: TextSpan(
                                    children: [
                                      const WidgetSpan(
                                          child: Icon(
                                            FeatherIcons.x,
                                            size: 16,
                                            color: PartnerAppColors.red,
                                          ),
                                          alignment:
                                              PlaceholderAlignment.middle),
                                      TextSpan(
                                        text: tr('cancel'),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodySmall!
                                            .copyWith(
                                                color: PartnerAppColors.red),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              InkWell(
                                onTap: () {
                                  widget.invitationVm
                                      .updateSelectedDescriptionListPrice(
                                          index: index,
                                          indexDescription: indexDescription,
                                          value: widget
                                              .formKey
                                              .currentState!
                                              .fields[
                                                  '${listPrice['id']}_${indexDescription}_description']!
                                              .value,
                                          whatToUpdate: 'editDescription');
                                },
                                child: RichText(
                                  text: TextSpan(
                                    children: [
                                      const WidgetSpan(
                                          child: Icon(
                                            FeatherIcons.check,
                                            size: 16,
                                            color: PartnerAppColors.blue,
                                          ),
                                          alignment:
                                              PlaceholderAlignment.middle),
                                      TextSpan(
                                        text: tr('update'),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodySmall!
                                            .copyWith(
                                                color: PartnerAppColors.blue),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                        else
                          InkWell(
                            onTap: () {
                              if (descriptionMap['isSelected']) {
                                widget.invitationVm
                                    .updateSelectedDescriptionListPrice(
                                        index: index,
                                        indexDescription: indexDescription,
                                        value: true,
                                        whatToUpdate: 'descriptionEditable');
                              }
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Icon(
                                  FeatherIcons.edit,
                                  color: descriptionMap['isSelected']
                                      ? PartnerAppColors.blue
                                      : PartnerAppColors.spanishGrey,
                                  size: 16,
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  tr('edit'),
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodySmall!
                                      .copyWith(
                                          color: descriptionMap['isSelected']
                                              ? PartnerAppColors.blue
                                              : PartnerAppColors.spanishGrey),
                                ),
                              ],
                            ),
                          ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Divider(
                          color: PartnerAppColors.darkBlue,
                        ),
                      ],
                    ),
                  );
                })
              ],
            ),
          );
        })
      ],
    );
  }
}
