import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/invitation_subcontractor_manufacturer/invitation_subcontractor_manufacturer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/widgets_recommendations_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

class SubcontractorStep4 extends StatelessWidget {
  const SubcontractorStep4({super.key, required this.invitationVm});

  final InvitationSubcontractorManufacturerViewmodel invitationVm;

  @override
  Widget build(BuildContext context) {
    return Consumer<WidgetsViewModel>(builder: (_, supplierInfo, __) {
      final partnerFirstName =
          supplierInfo.company!.bCrmCompany?.title?.split(' ')[0] ?? '';

      final partnerLastName =
          supplierInfo.company!.bCrmCompany!.title!.split(' ').length > 1
              ? supplierInfo.company!.bCrmCompany?.title?.split(' ')[1] ?? ''
              : '';
      final partnerEmail = supplierInfo.company!.bCrmCompany?.email ?? '';

      final partnerPhone =
          supplierInfo.company!.bCrmCompany?.mobile.toString() ?? '';

      final partnerAddress =
          supplierInfo.company!.bCrmCompany?.companyAddress ?? '';

      final partnerZip =
          supplierInfo.company!.bCrmCompany?.zipCode.toString() ?? '';

      final partnerCity = supplierInfo.company!.bCrmCompany?.city ?? '';

      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            tr('contract'),
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          const SizedBox(
            height: 20,
          ),
          Text(
            tr('contracting_party'),
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          const SizedBox(
            height: 20,
          ),
          Text(
            '1. ${tr('consumer')}',
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(fontWeight: FontWeight.normal),
          ),
          const SizedBox(
            height: 20,
          ),
          CustomTextFieldFormBuilder(
            name: 'partnerFirstName',
            labelText: tr('first_name'),
            initialValue: partnerFirstName,
            validator:
                FormBuilderValidators.required(errorText: tr('required')),
          ),
          CustomTextFieldFormBuilder(
            name: 'partnerLastName',
            labelText: tr('last_name'),
            initialValue: partnerLastName,
            validator:
                FormBuilderValidators.required(errorText: tr('required')),
          ),
          CustomTextFieldFormBuilder(
            name: 'partnerAddress',
            labelText: tr('address'),
            initialValue: partnerAddress,
            validator:
                FormBuilderValidators.required(errorText: tr('required')),
          ),
          CustomTextFieldFormBuilder(
            name: 'partnerZip',
            labelText: tr('zip_code'),
            keyboardType: TextInputType.number,
            initialValue: partnerZip,
            validator:
                FormBuilderValidators.required(errorText: tr('required')),
          ),
          CustomTextFieldFormBuilder(
            name: 'partnerCity',
            labelText: tr('city'),
            keyboardType: TextInputType.number,
            initialValue: partnerCity,
            validator:
                FormBuilderValidators.required(errorText: tr('required')),
          ),
          CustomTextFieldFormBuilder(
            name: 'partnerEmail',
            labelText: tr('email'),
            initialValue: partnerEmail,
            keyboardType: TextInputType.emailAddress,
            validator:
                FormBuilderValidators.required(errorText: tr('required')),
          ),
          CustomTextFieldFormBuilder(
            name: 'partnerMobile',
            labelText: tr('telephone'),
            initialValue: partnerPhone,
            keyboardType: TextInputType.number,
            validator:
                FormBuilderValidators.required(errorText: tr('required')),
          ),
          CustomTextFieldFormBuilder(
            name: 'inviteName',
            labelText: '2. Håndværker',
            keyboardType: TextInputType.number,
            validator:
                FormBuilderValidators.required(errorText: tr('required')),
            initialValue:
                '${invitationVm.formValues['inviteFirstName'] ?? ''} ${invitationVm.formValues['inviteLastName'] ?? ''}',
          ),
          const SizedBox(
            height: 20,
          ),
          Text(
            tr('time_schedule'),
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(fontWeight: FontWeight.normal),
          ),
          const SizedBox(
            height: 20,
          ),
          CustomDatePickerFormBuilder(
            name: 'startDate',
            labelText: tr('contract_start_date_description'),
            dateFormat: DateFormatType.shortDateInternational.formatter,
            initialDate: DateTime.now(),
            initialValue:
                DateTime.now().copyWith(year: DateTime.now().year + 1),
            validator:
                FormBuilderValidators.required(errorText: tr('required')),
          ),
          CustomDatePickerFormBuilder(
            name: 'endDate',
            labelText: tr('contract_end_date_description'),
            dateFormat: DateFormatType.shortDateInternational.formatter,
            initialDate: DateTime.now(),
            initialValue:
                DateTime.now().copyWith(year: DateTime.now().year + 1),
            validator:
                FormBuilderValidators.required(errorText: tr('required')),
          ),
        ],
      );
    });
  }
}
