import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/signature_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/manufacturer/manufacturer_price_type_dialog.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/invitation_subcontractor_manufacturer/invitation_subcontractor_manufacturer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';

class InvitationButtonNav extends StatefulWidget {
  const InvitationButtonNav(
      {super.key,
      required this.invitationVm,
      required this.invitationType,
      required this.formKey});

  final InvitationSubcontractorManufacturerViewmodel invitationVm;
  final GlobalKey<FormBuilderState> formKey;

  final String invitationType;

  @override
  InvitationButtonNavState createState() => InvitationButtonNavState();
}

class InvitationButtonNavState extends State<InvitationButtonNav> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (widget.invitationType == 'manufacturer')
            manufacturerButtons()
          else
            subContractorButtons()
        ],
      ),
    );
  }

  Widget manufacturerButtons() {
    return defaultFloatingButton();
  }

  Widget subContractorButtons() {
    if ((widget.invitationVm.currentFormStep + 1) == 5) {
      return TextButton(
        style: TextButton.styleFrom(
            fixedSize: Size(MediaQuery.of(context).size.width, 45),
            backgroundColor: PartnerAppColors.malachite),
        child: Text(
          tr('sign'),
          style: Theme.of(context)
              .textTheme
              .headlineMedium!
              .copyWith(color: Colors.white),
        ),
        onPressed: () => onSubContractorAddSignature(),
      );
    }

    return defaultFloatingButton();
  }

  Widget defaultFloatingButton() {
    return Row(
      children: [
        if (widget.invitationVm.currentFormStep > 0) ...[
          Expanded(
            child: TextButton(
              style: TextButton.styleFrom(
                fixedSize: Size(MediaQuery.of(context).size.width, 45),
                backgroundColor: Colors.white,
                shape: const RoundedRectangleBorder(
                    side: BorderSide(
                  color: PartnerAppColors.blue,
                )),
              ),
              child: Text(
                tr('back'),
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(color: PartnerAppColors.blue),
              ),
              onPressed: () {
                widget.invitationVm.currentFormStep =
                    widget.invitationVm.currentFormStep - 1;
              },
            ),
          ),
          const SizedBox(
            width: 10,
          ),
        ],
        Expanded(
          child: TextButton(
            style: TextButton.styleFrom(
                fixedSize: Size(MediaQuery.of(context).size.width, 45),
                backgroundColor: PartnerAppColors.malachite),
            child: Text(
              tr('next'),
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium!
                  .copyWith(color: Colors.white),
            ),
            onPressed: () {
              switch (widget.invitationType) {
                case 'manufacturer':
                  onNextManufacturer();

                  break;
                case 'subContractor':
                  onNextSubContractor();
                  break;
                default:
              }
            },
          ),
        )
      ],
    );
  }

  void onSubContractorAddSignature() async {
    final termsVm = context.read<SharedDataViewmodel>();

    final signature = await signatureDialog(
      context: context,
      label: termsVm.termsLabel,
      htmlTerms: termsVm.termsHtml,
    );

    if (!mounted) return;

    if (signature != null) {
      await tryCatchWrapper(
              context: context,
              function: widget.invitationVm
                  .subContractorAddSignature(signature: signature))
          .then((value) {
        if (!mounted) return;

        if ((value ?? false)) {
          backDrawerRoute();

          commonSuccessOrFailedDialog(
              context: context,
              successMessage: tr('offer_sent_to_subcontractor'));
        } else {
          commonSuccessOrFailedDialog(
            context: context,
            isSuccess: false,
          );
        }
      });
    }
  }

  void onNextManufacturer() async {
    final projectsVm = context.read<MyProjectsViewModel>();

    switch (widget.invitationVm.currentFormStep + 1) {
      case 1:
        if (widget.formKey.currentState!.validate()) {
          for (var element in widget.formKey.currentState!.fields.entries) {
            switch (element.key) {
              case 'manufacturerStartDate':
              case 'manufacturerEmail':
                widget.invitationVm.updatedFormValues(
                    key: element.key, value: element.value.value);
                break;
              default:
            }
          }

          final priceType = await manufacturerPriceTypeDialog(context: context);

          widget.invitationVm
              .updatedFormValues(key: 'priceType', value: priceType);

          widget.invitationVm.currentFormStep += 1;
        }
        break;
      case 2:
        if (widget.invitationVm.formValues['priceType'] == 'fixedPrice') {
          if (widget.formKey.currentState!.validate()) {
            for (var element in widget.formKey.currentState!.fields.entries) {
              widget.invitationVm.updatedFormValues(
                  key: element.key, value: element.value.value);
            }

            widget.invitationVm.currentFormStep += 1;
          }
        } else if (widget.invitationVm.formValues['priceType'] ==
            'maximumPrice') {
          switch (widget.invitationVm.getValidation(type: 'manufacturer')) {
            case 'NO_SELECT':
              showOkAlertDialog(
                  context: context, title: tr('select_one_to_continue'));
              break;
            case 'MORE_THAN':
              showOkAlertDialog(
                  context: context,
                  title:
                      '${tr('total_price_greater_than_allocated')} ${Formatter.curencyFormat(amount: widget.invitationVm.totalAllocatedBudget)}');
              break;
            default:
              if (widget.formKey.currentState!.validate()) {
                widget.invitationVm.updatedFormValues(
                    key: 'manufacturerMaximumPriceDescription',
                    value: widget.formKey.currentState!
                        .fields['manufacturerMaximumPriceDescription']!.value);

                widget.invitationVm.currentFormStep += 1;
              }
              break;
          }
        }
        break;
      case 3:
        final termsVm = context.read<SharedDataViewmodel>();

        final signature = await signatureDialog(
          context: context,
          label: termsVm.termsLabel,
          htmlTerms: termsVm.termsHtml,
        );

        if (!mounted) return;

        if (signature != null) {
          await tryCatchWrapper(
                  context: context,
                  function: widget.invitationVm.manufacturerAddRequistion(
                      projectId: projectsVm.activeProject.id!,
                      contractId: projectsVm.activeProject.contractId,
                      signature: signature))
              .then((value) {
            if (!mounted) return;

            backDrawerRoute();
            commonSuccessOrFailedDialog(
                context: context, isSuccess: value ?? false);
          });
        }
      default:
    }
  }

  void onNextSubContractor() async {
    final projectsVm = context.read<MyProjectsViewModel>();

    switch (widget.invitationVm.currentFormStep + 1) {
      case 1:
        if (widget.formKey.currentState!.validate()) {
          for (var element in widget.formKey.currentState!.fields.entries) {
            widget.invitationVm.updatedFormValues(
                key: element.key, value: element.value.value);
          }

          widget.invitationVm.currentFormStep =
              widget.invitationVm.currentFormStep + 1;
        }
        break;
      case 2:
        switch (widget.invitationVm.getValidation(type: 'subContractor')) {
          case 'NO_SELECT':
            showOkAlertDialog(
                context: context, title: tr('select_one_to_continue'));
            break;
          case 'MORE_THAN':
            showOkAlertDialog(
                context: context,
                title:
                    '${tr('total_price_greater_than_allocated')} ${Formatter.curencyFormat(amount: widget.invitationVm.totalAllocatedBudget)}');
            break;
          default:
            break;
        }
        break;
      case 3:
        widget.invitationVm.currentFormStep =
            widget.invitationVm.currentFormStep + 1;

      case 4:
        if (widget.formKey.currentState!.validate()) {
          for (var element in widget.formKey.currentState!.fields.entries) {
            widget.invitationVm.updatedFormValues(
                key: element.key, value: element.value.value);
          }

          await tryCatchWrapper(
                  context: context,
                  function: widget.invitationVm.subContractorInvite(
                      contractId: projectsVm.activeProject.contractId,
                      projectId: projectsVm.activeProject.id!))
              .then((value) {});
        }

        break;
      default:
    }
  }
}
