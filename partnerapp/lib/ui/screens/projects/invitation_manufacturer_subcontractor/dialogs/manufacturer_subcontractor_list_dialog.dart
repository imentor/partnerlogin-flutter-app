import 'package:Haandvaerker.dk/model/producent/producent_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/wallet/wallet_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';

Future<void> manufacturerSubcontractorListDialog({
  required BuildContext context,
  required List<Producent> list,
  required String type,
}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: const EdgeInsets.all(20),
          title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            InkWell(
              onTap: () {
                Navigator.of(dialogContext).pop();
              },
              child: const Icon(
                FeatherIcons.x,
                color: PartnerAppColors.spanishGrey,
              ),
            )
          ]),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: ListWidget(list: list, type: type),
            ),
          ),
        );
      });
}

class ListWidget extends StatelessWidget {
  const ListWidget({super.key, required this.list, required this.type});

  final List<Producent> list;
  final String type;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: MediaQuery.of(context).size.height / 2,
          child: ListView(
            shrinkWrap: true,
            children: [
              ...list.map((item) {
                return InkWell(
                  onTap: () async {
                    if ((item.project ?? '').isNotEmpty) {
                      final walletVm = context.read<WalletViewmodel>();
                      changeDrawerRoute(Routes.projectWallet, arguments: false);
                      Navigator.of(context).pop();

                      await tryCatchWrapper(
                          context: context,
                          function: walletVm
                              .getPaymentStageFromManufacturerSubcontractor(
                                  projectId: item.project!,
                                  type: type,
                                  producentId:
                                      type == 'producent' ? item.id : null));
                    }
                  },
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: PartnerAppColors.darkBlue
                                .withValues(alpha: .3))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          flex: 2,
                          child: Text(
                            item.name ?? '',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: Theme.of(context).textTheme.titleMedium,
                          ),
                        ),
                        Flexible(
                            child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              Formatter.curencyFormat(
                                  amount: double.parse(
                                      Formatter.sanitizeCurrency(
                                          item.amount ?? '0'))),
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Text(
                              tr('total_price'),
                              style: Theme.of(context).textTheme.titleMedium,
                            )
                          ],
                        ))
                      ],
                    ),
                  ),
                );
              })
            ],
          ),
          // child: ListView.builder(
          //   itemBuilder: (context, index) {
          //     final item = list[index];

          //     return ListTile(
          //       leading: Text(
          //         item.name ?? '',
          //         style: Theme.of(context).textTheme.titleMedium,
          //       ),
          //       trailing: Column(
          //         crossAxisAlignment: CrossAxisAlignment.end,
          //         children: [
          //           Text(
          //             Formatter.curencyFormat(
          //                 amount: double.parse(
          //                     Formatter.sanitizeCurrency(item.amount ?? '0'))),
          //             style: Theme.of(context).textTheme.titleMedium,
          //           ),
          //           const SizedBox(
          //             height: 5,
          //           ),
          //           Text(
          //             tr('total_price'),
          //             style: Theme.of(context).textTheme.titleMedium,
          //           )
          //         ],
          //       ),
          //     );
          //   },
          //   itemCount: list.length,
          //   shrinkWrap: true,
          // ),
        ),
      ],
    );
  }
}
