import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/debouncer.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/invitation_subcontractor_manufacturer/invitation_subcontractor_manufacturer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class MaximumPriceStep2 extends StatelessWidget {
  MaximumPriceStep2({super.key, required this.invitationVm});
  final InvitationSubcontractorManufacturerViewmodel invitationVm;

  final Debouncer debouncer = Debouncer(milliseconds: 500);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('enter_price'),
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        const SizedBox(
          height: 20,
        ),
        ...invitationVm.listPrices.map((listPrice) {
          final index = invitationVm.listPrices.indexOf(listPrice);

          return Container(
            margin: const EdgeInsets.only(bottom: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(
                          height: 24,
                          width: 24,
                          child: Checkbox(
                              value: listPrice['isSelected'] as bool,
                              side: WidgetStateBorderSide.resolveWith(
                                (states) => BorderSide(
                                  width: 1.0,
                                  color: listPrice['isSelected']
                                      ? PartnerAppColors.darkBlue
                                          .withValues(alpha: .3)
                                      : PartnerAppColors.spanishGrey,
                                ),
                              ),
                              activeColor: PartnerAppColors.blue,
                              onChanged: (value) {
                                invitationVm.updateSelectedListPirce(
                                    index: index,
                                    value: (value ?? false),
                                    whatToUpdate: 'select');
                              }),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Text(
                          '${index + 1}. ${listPrice['title']}',
                          style: Theme.of(context).textTheme.headlineMedium,
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 150,
                      child: CustomTextFieldFormBuilder(
                        name: '${listPrice['id']}_$index',
                        textAlign: TextAlign.end,
                        enabled: listPrice['isSelected'],
                        initialValue: '${listPrice['price']}',
                        keyboardType: TextInputType.number,
                        suffixIcon: Container(
                          padding: const EdgeInsets.only(
                              right: 20, left: 10, bottom: 12, top: 12),
                          child: Text(
                            'kr.',
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium!
                                .copyWith(
                                  fontSize: 18,
                                  fontWeight: FontWeight.normal,
                                  color: listPrice['isSelected']
                                      ? PartnerAppColors.darkBlue
                                      : PartnerAppColors.spanishGrey,
                                ),
                          ),
                        ),
                        onChanged: (p0) {
                          debouncer.run(() {
                            if ((p0 ?? '').isNotEmpty) {
                              invitationVm.updateSelectedListPirce(
                                  index: index,
                                  value: double.parse(
                                      Formatter.sanitizeCurrency(p0!)),
                                  whatToUpdate: 'price');
                            } else {
                              invitationVm.updateSelectedListPirce(
                                  index: index,
                                  value: 0,
                                  whatToUpdate: 'price');
                            }
                          });
                        },
                      ),
                    )
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                const Divider(
                  color: PartnerAppColors.darkBlue,
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          );
        }),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('total_price'),
              style: Theme.of(context)
                  .textTheme
                  .titleMedium!
                  .copyWith(fontWeight: FontWeight.normal),
            ),
            Text(
              Formatter.curencyFormat(
                  amount: invitationVm.listPrices
                      .where((value) => value['isSelected'])
                      .fold(0,
                          (sum, listPriceMap) => sum + listPriceMap['price'])),
              style: Theme.of(context)
                  .textTheme
                  .titleMedium!
                  .copyWith(fontWeight: FontWeight.normal),
            ),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        CustomTextFieldFormBuilder(
          name: 'manufacturerMaximumPriceDescription',
          minLines: 5,
          labelText: tr('product_description'),
          hintText: tr('enter_description'),
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
      ],
    );
  }
}
