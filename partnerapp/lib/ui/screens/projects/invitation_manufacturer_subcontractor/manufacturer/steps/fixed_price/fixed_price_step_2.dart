import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/invitation_subcontractor_manufacturer/invitation_subcontractor_manufacturer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class FixedPriceStep2 extends StatelessWidget {
  const FixedPriceStep2({
    super.key,
    required this.invitationVm,
  });

  final InvitationSubcontractorManufacturerViewmodel invitationVm;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('enter_price'),
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        CustomTextFieldFormBuilder(
          name: 'manufacturerFixedPrice',
          textAlign: TextAlign.end,
          initialValue: '00',
          keyboardType: TextInputType.number,
          validator: FormBuilderValidators.compose([
            FormBuilderValidators.required(errorText: tr('required')),
            (value) {
              if ((value ?? '').isNotEmpty) {
                if (double.parse(value!) > invitationVm.totalAllocatedBudget) {
                  return '${tr('total_price_greater_than_allocated')} ${Formatter.curencyFormat(amount: invitationVm.totalAllocatedBudget)}';
                }
              }
              return null;
            }
          ]),
          suffixIcon: Container(
            padding:
                const EdgeInsets.only(right: 20, left: 10, bottom: 12, top: 12),
            child: Text(
              'kr.',
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                  fontSize: 18,
                  fontWeight: FontWeight.normal,
                  color: PartnerAppColors.darkBlue),
            ),
          ),
        ),
        CustomTextFieldFormBuilder(
          name: 'manufacturerFixedPriceDescription',
          minLines: 5,
          labelText: tr('product_description'),
          hintText: tr('enter_description'),
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
        filePicker(context),
      ],
    );
  }

  Widget filePicker(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FormBuilderField<List<PlatformFile>>(
            name: 'manufacturerFiles',
            builder: (field) {
              return InputDecorator(
                decoration: InputDecoration(
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.zero,
                  errorText: field.errorText,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        TextButton(
                          onPressed: () async {
                            var fileResultList =
                                await FilePicker.platform.pickFiles(
                              type: FileType.any,
                              allowMultiple: true,
                            );

                            if (fileResultList != null) {
                              List<PlatformFile> files = [];

                              files.addAll(fileResultList.files);
                              field.didChange(files);
                            }
                          },
                          style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                              side: BorderSide(
                                color: PartnerAppColors.spanishGrey
                                    .withValues(alpha: .3),
                              ),
                            ),
                          ),
                          child: Text(
                            tr('upload_file'),
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium!
                                .copyWith(color: PartnerAppColors.spanishGrey),
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Text(
                          '(${tr('optional')})',
                          style:
                              Theme.of(context).textTheme.titleSmall!.copyWith(
                                    color: PartnerAppColors.spanishGrey,
                                  ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    ...(field.value ?? []).map((value) {
                      return Container(
                        margin: const EdgeInsets.only(bottom: 10),
                        padding: const EdgeInsets.all(10),
                        color:
                            PartnerAppColors.spanishGrey.withValues(alpha: .3),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                                child: Text(
                              value.name.split('_').last,
                              style: Theme.of(context).textTheme.titleSmall,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            )),
                            InkWell(
                              onTap: () {
                                List<PlatformFile> files = [
                                  ...(field.value ?? [])
                                ];
                                files.remove(value);

                                field.didChange(files);
                              },
                              child: const Icon(
                                FeatherIcons.x,
                                color: PartnerAppColors.spanishGrey,
                              ),
                            )
                          ],
                        ),
                      );
                    })
                  ],
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
