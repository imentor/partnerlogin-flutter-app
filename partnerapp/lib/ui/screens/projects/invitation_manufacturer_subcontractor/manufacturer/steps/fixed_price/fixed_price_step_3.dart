import 'dart:io';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/invitation_subcontractor_manufacturer/invitation_subcontractor_manufacturer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class FixedPriceStep3 extends StatelessWidget {
  const FixedPriceStep3({super.key, required this.invitationVm});
  final InvitationSubcontractorManufacturerViewmodel invitationVm;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('summary'),
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        const SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('total_price'),
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium!
                  .copyWith(fontWeight: FontWeight.normal),
            ),
            Text(
              Formatter.curencyFormat(
                amount: double.parse(
                  Formatter.sanitizeCurrency(
                      invitationVm.formValues['manufacturerFixedPrice']),
                ),
              ),
            )
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        const Divider(
          color: PartnerAppColors.darkBlue,
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          invitationVm.formValues['manufacturerFixedPriceDescription'],
          style: Theme.of(context).textTheme.titleSmall,
        ),
        const SizedBox(
          height: 20,
        ),
        showFiles(context),
      ],
    );
  }

  Widget showFiles(BuildContext context) {
    Widget imageWidget = const SizedBox.shrink();

    if (invitationVm.formValues.containsKey('manufacturerFiles') &&
        (invitationVm.formValues['manufacturerFiles'] as List<PlatformFile>)
            .isNotEmpty) {
      final file =
          (invitationVm.formValues['manufacturerFiles'] as List<PlatformFile>)
              .first;

      if ((file.name.contains('png') ||
              file.name.contains('jpg') ||
              file.name.contains('jpeg') ||
              file.name.contains('heic') ||
              file.name.contains('heif')) &&
          (file.path ?? '').isNotEmpty) {
        imageWidget = Container(
          color: PartnerAppColors.spanishGrey.withValues(alpha: .3),
          height: MediaQuery.of(context).size.height / 2.5,
          padding: const EdgeInsets.all(10),
          width: double.infinity,
          child: Image.file(
            File(file.path!),
            fit: BoxFit.cover,
          ),
        );
      } else if (file.name.contains('pdf') && (file.path ?? '').isNotEmpty) {
        imageWidget = Container(
          color: PartnerAppColors.spanishGrey.withValues(alpha: .3),
          height: MediaQuery.of(context).size.height / 2.5,
          padding: const EdgeInsets.all(10),
          width: double.infinity,
          child: SfPdfViewer.file(File(file.path!)),
        );
      } else {
        imageWidget = Container(
          margin: const EdgeInsets.only(bottom: 10),
          padding: const EdgeInsets.all(10),
          color: PartnerAppColors.spanishGrey.withValues(alpha: .3),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                  child: Text(
                file.name.split('_').last,
                style: Theme.of(context).textTheme.titleSmall,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              )),
            ],
          ),
        );
      }
    }

    return imageWidget;
  }
}
