import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/invitation_subcontractor_manufacturer/invitation_subcontractor_manufacturer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class MaximumPriceStep3 extends StatelessWidget {
  const MaximumPriceStep3({
    super.key,
    required this.invitationVm,
  });
  final InvitationSubcontractorManufacturerViewmodel invitationVm;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('summary'),
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        const SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('total_price_exl_vat'),
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium!
                  .copyWith(fontWeight: FontWeight.normal),
            ),
            Text(
              Formatter.curencyFormat(
                amount: invitationVm.listPrices
                    .where((value) => value['isSelected'])
                    .fold(0, (sum, listPrice) => sum + listPrice['price']),
              ),
            )
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        const Divider(
          color: PartnerAppColors.darkBlue,
        ),
        const SizedBox(
          height: 10,
        ),
        ...invitationVm.listPrices
            .where((value) => value['isSelected'])
            .map((listPriceMap) {
          return Container(
            margin: const EdgeInsets.only(bottom: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  listPriceMap['title'],
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      fontWeight: FontWeight.normal,
                      color: PartnerAppColors.spanishGrey),
                ),
                Text(
                  Formatter.curencyFormat(amount: listPriceMap['price']),
                  style: Theme.of(context).textTheme.titleMedium,
                )
              ],
            ),
          );
        }),
        const Divider(
          color: PartnerAppColors.darkBlue,
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          invitationVm.formValues['manufacturerMaximumPriceDescription'],
          style: Theme.of(context)
              .textTheme
              .titleSmall!
              .copyWith(color: PartnerAppColors.spanishGrey),
        )
      ],
    );
  }
}
