import 'package:Haandvaerker.dk/model/producent/producent_company_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/invitation_subcontractor_manufacturer/invitation_subcontractor_manufacturer_viewmodel.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class ManufacturerStep1 extends StatelessWidget {
  const ManufacturerStep1({
    super.key,
    required this.invitationVm,
    required this.formKey,
  });

  final InvitationSubcontractorManufacturerViewmodel invitationVm;
  final GlobalKey<FormBuilderState> formKey;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Leverandøropplysninger',
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        const SizedBox(
          height: 20,
        ),
        searchableTextFormField(context),
        const AbsorbPointer(
          absorbing: true,
          child: CustomTextFieldFormBuilder(
            name: 'cvrp',
            labelText: 'CVRP-nummer',
          ),
        ),
        const AbsorbPointer(
          absorbing: true,
          child: CustomTextFieldFormBuilder(
            name: 'cvr',
            labelText: 'CVR',
          ),
        ),
        const AbsorbPointer(
          absorbing: true,
          child: CustomTextFieldFormBuilder(
            name: 'address',
            labelText: 'Addresse',
          ),
        ),
        CustomDatePickerFormBuilder(
          name: 'manufacturerStartDate',
          labelText: 'Arbejdet starter den:',
          dateFormat: DateFormatType.isoDate.formatter,
          initialDate: DateTime.now(),
          initialValue: DateTime.now().copyWith(year: DateTime.now().year + 1),
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
        CustomTextFieldFormBuilder(
          name: 'manufacturerEmail',
          labelText: 'E-mail',
          keyboardType: TextInputType.emailAddress,
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
      ],
    );
  }

  Widget searchableTextFormField(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: 'Firmanavn',
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 18),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          DropdownSearch<ProducentCompany>(
            items: (filter, loadProps) async {
              return await invitationVm.getProducentCompanies(search: filter);
            },
            compareFn: (ProducentCompany i, ProducentCompany s) {
              return (i.fullName ?? '').toLowerCase() ==
                  (s.fullName ?? '').toLowerCase();
            },
            filterFn: (item, filter) {
              return true;
            },
            itemAsString: (item) => item.fullName ?? '',
            onChanged: (value) {
              invitationVm.updatedFormValues(
                  key: 'producentCompany', value: value);

              formKey.currentState!.patchValue({
                'cvrp': value?.cvprNumber ?? '',
                'cvr': value?.cvr ?? '',
                'address': value?.address ?? '',
              });
            },
            selectedItem: invitationVm.formValues['producentCompany']
                as ProducentCompany?,
            decoratorProps: DropDownDecoratorProps(
                baseStyle: Theme.of(context).textTheme.titleMedium),
            popupProps: PopupPropsMultiSelection.menu(
              showSelectedItems: true,
              showSearchBox: true,
              searchFieldProps: const TextFieldProps(
                decoration: InputDecoration(hintText: 'Firmanavn'),
              ),
              emptyBuilder: (context, searchEntry) {
                return Center(child: Text(tr('empty')));
              },
              errorBuilder: (context, searchEntry, exception) {
                return Center(child: Text(tr('empty')));
              },
              loadingBuilder: (context, searchEntry) {
                return Center(
                  child: Container(
                    height: 100,
                    width: 100,
                    color: Colors.white,
                    padding: const EdgeInsets.all(20),
                    child: CircularProgressIndicator(
                      color: PartnerAppColors.blue.withValues(alpha: .4),
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
