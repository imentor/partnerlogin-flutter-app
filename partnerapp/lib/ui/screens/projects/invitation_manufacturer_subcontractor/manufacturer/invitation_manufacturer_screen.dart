import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/components/invitation_button_nav.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/manufacturer/steps/fixed_price/fixed_price_step_2.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/manufacturer/steps/fixed_price/fixed_price_step_3.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/manufacturer/steps/manufacturer_step_1.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/manufacturer/steps/maximum_price/maximum_price_step_2.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/manufacturer/steps/maximum_price/maximum_price_step_3.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/invitation_subcontractor_manufacturer/invitation_subcontractor_manufacturer_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class InvitationManufacturerScreen extends StatefulWidget {
  const InvitationManufacturerScreen({super.key});

  @override
  State<InvitationManufacturerScreen> createState() =>
      _InvitationManufacturerScreenState();
}

class _InvitationManufacturerScreenState
    extends State<InvitationManufacturerScreen> {
  final formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Consumer<InvitationSubcontractorManufacturerViewmodel>(
        builder: (_, invitationVm, __) {
      return Scaffold(
        appBar: const DrawerAppBar(),
        bottomNavigationBar: InvitationButtonNav(
          invitationVm: invitationVm,
          invitationType: 'manufacturer',
          formKey: formKey,
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: FormBuilder(
            key: formKey,
            child: Skeletonizer(
              enabled: invitationVm.busy,
              child: formSteps(invitationVm),
            ),
          ),
        ),
      );
    });
  }

  Widget formSteps(InvitationSubcontractorManufacturerViewmodel invitationVm) {
    switch (invitationVm.currentFormStep + 1) {
      case 1:
        return ManufacturerStep1(
          invitationVm: invitationVm,
          formKey: formKey,
        );
      case 2:
      case 3:
        return buildPriceStep(
            step: invitationVm.currentFormStep + 1, invitationVm: invitationVm);
      default:
        return SizedBox.fromSize();
    }
  }

  Widget buildPriceStep(
      {required int step,
      required InvitationSubcontractorManufacturerViewmodel invitationVm}) {
    final priceType = invitationVm.formValues['priceType'];
    switch (priceType) {
      case 'fixedPrice':
        return getFixedPriceStep(step: step, invitationVm: invitationVm);
      case 'maximumPrice':
        return getMaximumPriceStep(step: step, invitationVm: invitationVm);
      default:
        return const SizedBox.shrink();
    }
  }

  Widget getFixedPriceStep(
      {required int step,
      required InvitationSubcontractorManufacturerViewmodel invitationVm}) {
    switch (step) {
      case 2:
        return FixedPriceStep2(invitationVm: invitationVm);
      case 3:
        return FixedPriceStep3(invitationVm: invitationVm);
      default:
        return const SizedBox.shrink();
    }
  }

  Widget getMaximumPriceStep(
      {required int step,
      required InvitationSubcontractorManufacturerViewmodel invitationVm}) {
    switch (step) {
      case 2:
        return MaximumPriceStep2(invitationVm: invitationVm);
      case 3:
        return MaximumPriceStep3(invitationVm: invitationVm);
      default:
        return const SizedBox.shrink();
    }
  }
}
