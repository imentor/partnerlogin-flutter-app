import 'package:Haandvaerker.dk/theme.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';

Future<String?> manufacturerPriceTypeDialog({
  required BuildContext context,
}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: const EdgeInsets.all(20),
          title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            InkWell(
              onTap: () {
                Navigator.of(dialogContext).pop();
              },
              child: const Icon(
                FeatherIcons.x,
                color: PartnerAppColors.spanishGrey,
              ),
            )
          ]),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: const SingleChildScrollView(
              child: ManufacturerPriceTypeDialog(),
            ),
          ),
        );
      });
}

class ManufacturerPriceTypeDialog extends StatefulWidget {
  const ManufacturerPriceTypeDialog({super.key});

  @override
  State<ManufacturerPriceTypeDialog> createState() =>
      _ManufacturerPriceTypeDialogState();
}

class _ManufacturerPriceTypeDialogState
    extends State<ManufacturerPriceTypeDialog> {
  String? priceType;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RadioListTile(
          value: 'fixedPrice',
          contentPadding: EdgeInsets.zero,
          activeColor: PartnerAppColors.blue,
          groupValue: priceType,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
              side: const BorderSide(
                color: PartnerAppColors.blue,
              )),
          onChanged: (value) => setState(
            () {
              priceType = value;
            },
          ),
          title: Text(
            tr('fixed_price'),
            style: Theme.of(context).textTheme.titleMedium!.copyWith(
                  color: priceType == 'fixedPrice'
                      ? PartnerAppColors.darkBlue
                      : PartnerAppColors.spanishGrey,
                ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        RadioListTile(
          value: 'maximumPrice',
          groupValue: priceType,
          contentPadding: EdgeInsets.zero,
          activeColor: PartnerAppColors.blue,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
              side: const BorderSide(
                color: PartnerAppColors.blue,
              )),
          onChanged: (value) => setState(
            () {
              priceType = value;
            },
          ),
          title: Text(
            tr('maximum_amount'),
            style: Theme.of(context).textTheme.titleMedium!.copyWith(
                  color: priceType == 'maximumPrice'
                      ? PartnerAppColors.darkBlue
                      : PartnerAppColors.spanishGrey,
                ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        TextButton(
          style: TextButton.styleFrom(
              fixedSize: Size(MediaQuery.of(context).size.width, 45),
              backgroundColor: priceType != null
                  ? PartnerAppColors.malachite
                  : PartnerAppColors.spanishGrey),
          child: Text(
            tr('next'),
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(color: Colors.white),
          ),
          onPressed: () {
            if (priceType != null) {
              Navigator.of(context).pop(priceType);
            }
          },
        )
      ],
    );
  }
}
