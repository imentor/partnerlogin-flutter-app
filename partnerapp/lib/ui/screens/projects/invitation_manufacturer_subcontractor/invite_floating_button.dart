import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/invitation_subcontractor_manufacturer/invitation_subcontractor_manufacturer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/wallet/wallet_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InviteFloatingButton extends StatefulWidget {
  const InviteFloatingButton({super.key});

  @override
  State<InviteFloatingButton> createState() => _InviteFloatingButtonState();
}

class _InviteFloatingButtonState extends State<InviteFloatingButton> {
  bool showInvitation = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        if (showInvitation) ...[
          Card(
            elevation: 1,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
                side: BorderSide(
                    color: PartnerAppColors.darkBlue.withValues(alpha: .2))),
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  InkWell(
                    onTap: () => onPressed(''),
                    child: Text(
                      tr('subcontractor'),
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(
                              color: PartnerAppColors.blue,
                              fontWeight: FontWeight.normal),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  InkWell(
                    onTap: () => onPressed('manufacturer'),
                    child: Text(
                      tr('supplier'),
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(
                              color: PartnerAppColors.blue,
                              fontWeight: FontWeight.normal),
                    ),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
        ],
        InkWell(
          onTap: () {
            setState(() {
              showInvitation = !showInvitation;
            });
          },
          child: SizedBox(
            height: 50,
            width: 100,
            child: Card(
              elevation: 1,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25),
                side: BorderSide(
                  color: PartnerAppColors.blue.withValues(alpha: .2),
                ),
              ),
              child: Center(
                child: Text(
                  tr('invite'),
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      color: PartnerAppColors.blue,
                      fontWeight: FontWeight.normal),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Future<void> onPressed(String inviteType) async {
    final invitationVm =
        context.read<InvitationSubcontractorManufacturerViewmodel>();
    final projectsVm = context.read<MyProjectsViewModel>();
    final walletVm = context.read<WalletViewmodel>();

    if (inviteType == 'manufacturer') {
      changeDrawerRoute(Routes.projectInviteManufacturer);
    } else {
      changeDrawerRoute(Routes.projectInviteSubContractor);
    }

    setState(() {
      showInvitation = false;
    });

    invitationVm.reset();

    tryCatchWrapper(
      context: context,
      function: invitationVm.getListPriceMinbolig(
        projectId: projectsVm.activeProject.contractType == 'Fagentreprise'
            ? projectsVm.activeProject.parentId!
            : projectsVm.activeProject.id!,
        contactId: projectsVm.activeProject.homeOwner!.id!,
        stages: [
          ...(walletVm.paymentStagesMain?.stages ?? []),
          ...(walletVm.paymentStagesSubContractor?.stages ?? []),
          ...(walletVm.paymentStagesProducent?.stages ?? []),
        ],
      ),
    );
  }
}
