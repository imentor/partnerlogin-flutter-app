import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/invitation_subcontractor_manufacturer/invitation_subcontractor_manufacturer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class SubcontractorAcceptInviteScreen extends StatefulWidget {
  const SubcontractorAcceptInviteScreen({
    super.key,
    required this.subContractId,
    required this.projectId,
  });

  final int subContractId;
  final int projectId;

  @override
  State<SubcontractorAcceptInviteScreen> createState() =>
      _SubcontractorAcceptInviteScreenState();
}

class _SubcontractorAcceptInviteScreenState
    extends State<SubcontractorAcceptInviteScreen> {
  final formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Consumer<InvitationSubcontractorManufacturerViewmodel>(
        builder: (_, inviteVm, __) {
      return Scaffold(
        appBar: const DrawerAppBar(),
        bottomNavigationBar: buttonNav(inviteVm),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: Skeletonizer(
            enabled: inviteVm.busy,
            child: FormBuilder(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Dine oplysninger',
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  CustomTextFieldFormBuilder(
                    name: 'companyName',
                    labelText: 'Firmanavn',
                    validator: FormBuilderValidators.required(
                        errorText: tr('required')),
                  ),
                  CustomTextFieldFormBuilder(
                    name: 'address',
                    labelText: 'Adresse',
                    validator: FormBuilderValidators.required(
                        errorText: tr('required')),
                  ),
                  CustomTextFieldFormBuilder(
                    name: 'email',
                    labelText: 'E-mail',
                    validator: FormBuilderValidators.required(
                        errorText: tr('required')),
                    keyboardType: TextInputType.emailAddress,
                  ),
                  CustomTextFieldFormBuilder(
                    name: 'phone',
                    labelText: 'Telefonnummer',
                    validator: FormBuilderValidators.required(
                        errorText: tr('required')),
                    keyboardType: TextInputType.number,
                  ),
                  CustomTextFieldFormBuilder(
                    name: 'contact',
                    labelText: 'Kontaktperson',
                    validator: FormBuilderValidators.required(
                        errorText: tr('required')),
                  ),
                  CustomTextFieldFormBuilder(
                    name: 'cvr',
                    labelText: 'CVR-nummer',
                    validator: FormBuilderValidators.required(
                        errorText: tr('required')),
                    keyboardType: TextInputType.number,
                  ),
                  CustomDatePickerFormBuilder(
                    name: 'date',
                    validator: FormBuilderValidators.required(
                        errorText: tr('required')),
                    initialDate: DateTime.now(),
                    initialValue: DateTime.now(),
                    dateFormat: DateFormatType.isoDate.formatter,
                  )
                ],
              ),
            ),
          ),
        ),
      );
    });
  }

  Widget buttonNav(InvitationSubcontractorManufacturerViewmodel inviteVm) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
      color: Colors.white,
      child: TextButton(
        style: TextButton.styleFrom(
            fixedSize: Size(MediaQuery.of(context).size.width, 45),
            backgroundColor: PartnerAppColors.malachite),
        child: Text(
          tr('next'),
          style: Theme.of(context)
              .textTheme
              .headlineMedium!
              .copyWith(color: Colors.white),
        ),
        onPressed: () async {
          if (formKey.currentState!.validate()) {
            Map<String, dynamic> payload = {
              'subcontractorId': widget.subContractId,
              'acceptedFrom': 'app'
            };

            for (var element in formKey.currentState!.fields.entries) {
              payload.putIfAbsent(
                element.key,
                () => '${element.value.value}',
              );
            }

            await tryCatchWrapper(
                    context: context,
                    function: inviteVm.acceptInviteSubContractor(payload))
                .then((value) {
              if (!mounted) return;

              if ((value ?? false)) {
                backDrawerRoute();
                changeDrawerRoute(Routes.subcontractorInviteContract,
                    arguments: {
                      'subContractId': widget.subContractId,
                      'projectId': widget.projectId,
                      'from': 'invite'
                    });
              } else {
                commonSuccessOrFailedDialog(context: context, isSuccess: false);
              }
            });
          }
        },
      ),
    );
  }
}
