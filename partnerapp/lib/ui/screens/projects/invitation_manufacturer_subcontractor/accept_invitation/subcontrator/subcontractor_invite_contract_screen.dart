import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/signature_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/invitation_subcontractor_manufacturer/invitation_subcontractor_manufacturer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class SubcontractorInviteContractScreen extends StatefulWidget {
  const SubcontractorInviteContractScreen({
    super.key,
    required this.subContractId,
    required this.projectId,
    this.from = 'invite',
  });

  final int subContractId;
  final int projectId;
  final String from;

  @override
  State<SubcontractorInviteContractScreen> createState() =>
      _SubcontractorInviteContractScreenState();
}

class _SubcontractorInviteContractScreenState
    extends State<SubcontractorInviteContractScreen> {
  @override
  Widget build(BuildContext context) {
    return Consumer<InvitationSubcontractorManufacturerViewmodel>(
        builder: (_, invitationVm, __) {
      return Scaffold(
        appBar: const DrawerAppBar(),
        bottomNavigationBar:
            widget.from == 'invite' ? buttonNav(invitationVm) : null,
        body: Padding(
          padding: const EdgeInsets.all(20),
          child: Skeletonizer(
            enabled: invitationVm.busy,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Entreprisekontrakt',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
                const SizedBox(
                  height: 20,
                ),
                if (widget.from == 'invite') ...[
                  InkWell(
                    onTap: () {
                      context.read<MyProjectsViewModel>().currentProjectId =
                          widget.projectId;

                      backDrawerRoute();
                      changeDrawerRoute(Routes.projectDashboard, arguments: {
                        'projectId': widget.projectId,
                        'pageRedirect': 'contract'
                      });
                    },
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: RichText(
                          text: TextSpan(children: [
                        const WidgetSpan(
                            child: Icon(
                              FeatherIcons.edit,
                              color: PartnerAppColors.blue,
                              size: 20,
                            ),
                            alignment: PlaceholderAlignment.middle),
                        TextSpan(
                          text: ' ${tr('edit_contract')}',
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium!
                              .copyWith(color: PartnerAppColors.blue),
                        )
                      ])),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    color: PartnerAppColors.spanishGrey.withValues(alpha: .3),
                    child: InAppWebView(
                      initialSettings: InAppWebViewSettings(
                        transparentBackground: true,
                      ),
                      initialUrlRequest: URLRequest(
                          url: WebUri(
                              '${applic.bitrixApiUrl}/partner/subContractor/GetSubContractPDFDownloadT?SUB_CONTRACT_ID=${widget.subContractId}&FORMAT=PDF'),
                          headers: context.read<SharedDataViewmodel>().headers),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      );
    });
  }

  Widget buttonNav(InvitationSubcontractorManufacturerViewmodel invitationVm) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
      color: Colors.white,
      child: TextButton(
        style: TextButton.styleFrom(
            fixedSize: Size(MediaQuery.of(context).size.width, 45),
            backgroundColor: PartnerAppColors.malachite),
        child: Text(
          tr('approve'),
          style: Theme.of(context)
              .textTheme
              .headlineMedium!
              .copyWith(color: Colors.white),
        ),
        onPressed: () async {
          final termsVm = context.read<SharedDataViewmodel>();

          final signature = await signatureDialog(
              context: context,
              label: termsVm.termsLabel,
              htmlTerms: termsVm.termsHtml);

          if (!mounted) return;

          if (signature != null) {
            await tryCatchWrapper(
                    context: context,
                    function: invitationVm.subContractorAddSignature(
                        signature: signature))
                .then((value) {
              if (!mounted) return;

              commonSuccessOrFailedDialog(
                      context: context, isSuccess: value ?? false)
                  .whenComplete(() {
                backDrawerRoute();
              });
            });
          }
        },
      ),
    );
  }
}
