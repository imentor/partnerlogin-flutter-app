import 'dart:io';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/cache_image.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/get_image_with_custom_picker.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/create_projects_viewmodel.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ManufactureTypeSearch extends StatefulWidget {
  const ManufactureTypeSearch({super.key});

  @override
  ManufactureTypeSearchState createState() => ManufactureTypeSearchState();
}

class ManufactureTypeSearchState extends State<ManufactureTypeSearch> {
  @override
  Widget build(BuildContext context) {
    return Consumer<CreateProjectsViewModel>(
      builder: (context, createProjectsVm, __) {
        if (createProjectsVm.busy || createProjectsVm.manufactures.isEmpty) {
          return Container();
        }

        return Container(
          decoration: CustomDesignTheme.greyBorderShadows,
          child: DropdownSearch<String>.multiSelection(
            items: (filter, loadProps) =>
                createProjectsVm.manufactures.map((e) => e.name!).toList(),
            selectedItems: createProjectsVm.selectedManufactures
                .map((e) => e.name!)
                .toList(),
            filterFn: (item, keyword) {
              return createProjectsVm.manufactures
                  .where((e) =>
                      e.name!.toLowerCase().startsWith(keyword.toLowerCase()))
                  .isNotEmpty;
            },
            decoratorProps: DropDownDecoratorProps(
              decoration: InputDecoration(
                label: Text(tr('tap_to_search')),
              ),
            ),
            onChanged: (values) {
              if (values.isEmpty) {
                createProjectsVm.clearManufactures();
              }
              createProjectsVm.selectedManufactures = createProjectsVm
                  .manufactures
                  .where((element) => values.contains(element.name!))
                  .toList();
            },
          ),
        );
      },
    );
  }
}

class JobTypeSearch extends StatefulWidget {
  const JobTypeSearch({super.key});

  @override
  JobTypeSearchState createState() => JobTypeSearchState();
}

class JobTypeSearchState extends State<JobTypeSearch> {
  @override
  Widget build(BuildContext context) {
    return _getJobTypeSearch();
  }

  Widget _getJobTypeSearch() {
    return Consumer<CreateProjectsViewModel>(
      builder: (context, createProjectsVm, __) {
        if (createProjectsVm.busy || createProjectsVm.jobTypes.isEmpty) {
          return Container();
        }

        return Container(
          decoration: CustomDesignTheme.greyBorderShadows,
          child: DropdownSearch<String>.multiSelection(
            items: (filter, loadProps) => createProjectsVm.jobTypes
                .map(
                  (e) => context.locale.languageCode == 'da'
                      ? e.subCategoryName!
                      : e.subCategoryNameEn!,
                )
                .toList(),
            selectedItems: createProjectsVm.selectedJobTypes
                .map(
                  (e) => context.locale.languageCode == 'da'
                      ? e.subCategoryName!
                      : e.subCategoryNameEn!,
                )
                .toList(),
            filterFn: (item, keyword) {
              return createProjectsVm.jobTypes
                  .where((e) => (context.locale.languageCode == 'da'
                          ? e.subCategoryName!
                          : e.subCategoryNameEn!)
                      .toLowerCase()
                      .startsWith(keyword.toLowerCase()))
                  .isNotEmpty;
            },
            decoratorProps: DropDownDecoratorProps(
              decoration: InputDecoration(
                label: Text(tr('tap_to_search')),
              ),
            ),
            onChanged: (values) {
              createProjectsVm.selectedJobTypes = createProjectsVm.jobTypes
                  .where((e) => values.contains(
                        context.locale.languageCode == 'da'
                            ? e.subCategoryName!
                            : e.subCategoryNameEn!,
                      ))
                  .toList();
            },
          ),
        );
      },
    );
  }
}

class EditProjectPictureList extends StatefulWidget {
  const EditProjectPictureList({super.key});

  @override
  EditProjectPictureListState createState() => EditProjectPictureListState();
}

class EditProjectPictureListState extends State<EditProjectPictureList> {
  @override
  Widget build(BuildContext context) {
    return Consumer<CreateProjectsViewModel>(
      builder: (_, vm, __) {
        return Padding(
          padding: const EdgeInsets.only(bottom: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(tr('pictures'),
                  style: Theme.of(context).textTheme.headlineSmall),
              SmartGaps.gapH20,
              Wrap(
                spacing: 10,
                runSpacing: 10,
                children: [
                  ...vm.currentProjectImages.map(
                    (e) => SizedBox(
                      width: 120,
                      height: 120,
                      child: Stack(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                            ),
                            clipBehavior: Clip.antiAlias,
                            child: CacheImage(
                              imageUrl: e.fullURL!,
                              width: 120,
                              height: 120,
                            ),
                          ),
                          Positioned(
                            right: 0,
                            width: 25,
                            height: 25,
                            child: GestureDetector(
                              onTap: () =>
                                  vm.removeExistingImage(fileId: e.id!),
                              child: Container(
                                decoration: const BoxDecoration(
                                  color: Colors.black,
                                  shape: BoxShape.circle,
                                ),
                                child: const Center(
                                  child: Icon(
                                    Icons.close_rounded,
                                    color: Colors.white,
                                    size: 15,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  ...vm.projectImages.map((e) {
                    final index = vm.projectImages.indexOf(e);
                    return SizedBox(
                      width: 120,
                      height: 120,
                      child: Stack(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                            ),
                            clipBehavior: Clip.antiAlias,
                            child: Image.file(e,
                                width: 120, height: 120, fit: BoxFit.cover),
                          ),
                          Positioned(
                            right: 0,
                            width: 25,
                            height: 25,
                            child: GestureDetector(
                              onTap: () => vm.removeAddedImages(index: index),
                              child: Container(
                                decoration: const BoxDecoration(
                                  color: Colors.black,
                                  shape: BoxShape.circle,
                                ),
                                child: const Center(
                                  child: Icon(
                                    Icons.close_rounded,
                                    color: Colors.white,
                                    size: 15,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  }),
                  GestureDetector(
                    onTap: () async {
                      final vm = context.read<CreateProjectsViewModel>();
                      File? response = await getImage(context);

                      if (response?.path != null) {
                        if (response != null && mounted) {
                          final images = <File>[...vm.projectImages];
                          images.add(response);
                          vm.projectImages = images;
                        }
                      }
                    },
                    child: Container(
                      width: 120,
                      height: 120,
                      decoration: BoxDecoration(
                        color:
                            PartnerAppColors.accentBlue.withValues(alpha: 0.2),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Center(
                        child: Icon(
                          Icons.add,
                          color: Colors.black.withValues(alpha: 0.6),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
