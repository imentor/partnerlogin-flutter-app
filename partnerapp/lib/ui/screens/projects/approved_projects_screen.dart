import 'package:Haandvaerker.dk/model/projects/approved_projects_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ApprovedProjectsScreen extends StatefulWidget {
  const ApprovedProjectsScreen({super.key});

  @override
  ApprovedProjectsScreenState createState() => ApprovedProjectsScreenState();
}

class ApprovedProjectsScreenState extends State<ApprovedProjectsScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance
        .addPostFrameCallback((timeStamp) => getApprovedProjects());
  }

  Future<void> getApprovedProjects() async {
    var viewModel = context.read<MyProjectsViewModel>();
    /* await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey.currentContext,
        function: viewModel.approvedProjects());*/
    await viewModel.approvedProjects();
  }

  @override
  Widget build(BuildContext context) {
    // var viewModel = Provider.of<MyProjectsViewModel>(context, listen: true);
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(tr('approved_projects'),
                  style: Theme.of(context).textTheme.headlineLarge),
              SmartGaps.gapH20,
              _getList(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _getList() {
    var viewModel = context.read<MyProjectsViewModel>();

    if (viewModel.busy) return loader();

    if (viewModel.approvedProjectList.isNotEmpty) {
      return ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: viewModel.approvedProjectList.length,
        itemBuilder: (context, index) {
          final item = viewModel.approvedProjectList.elementAt(index);
          return ApprovedProjectCard(
            approvedProject: item,
          );
        },
      );
    } else {
      return Container();
    }
  }
}

class ApprovedProjectCard extends StatelessWidget {
  const ApprovedProjectCard({super.key, this.approvedProject});

  final ApprovedProjectsModel? approvedProject;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: 10, bottom: 10),
        padding: const EdgeInsets.all(20),
        decoration: CustomDesignTheme.coldBlueBorderShadows.copyWith(
          border: Border.all(color: Colors.grey.withValues(alpha: 0.25)),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (approvedProject!.address != null)
              CachedNetworkImage(imageUrl: approvedProject!.address!.image!),
            SmartGaps.gapH10,
            Text(approvedProject!.name!,
                style: Theme.of(context).textTheme.headlineMedium),
            SmartGaps.gapH10,
            if (approvedProject!.address != null)
              Text(approvedProject!.address!.address!,
                  style: Theme.of(context).textTheme.bodyMedium),
            SmartGaps.gapH10,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(approvedProject!.homeOwner!.name!,
                    style: Theme.of(context).textTheme.bodyMedium),
                Text(
                    Formatter.formatDateStrings(
                        type: DateFormatType.standardDate,
                        dateString: approvedProject?.dateCreated),
                    style: Theme.of(context).textTheme.bodyMedium),
              ],
            ),
            ProjectTag(
              tags: approvedProject!.tags,
            ),
            SmartGaps.gapH10,
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                  border: Border.all(
                      color: Theme.of(context).colorScheme.secondary)),
              child: expandedButtonWidget(
                context,
                noExpanded: true,
                color: Theme.of(context).colorScheme.secondary,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      tr('standard_offer'),
                      style:
                          const TextStyle(fontSize: 14.0, color: Colors.white),
                    ),
                  ],
                ),
                onTap: () {},
              ),
            ),
            SmartGaps.gapH10,
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                  border: Border.all(
                      color: Theme.of(context).colorScheme.secondary)),
              child: expandedButtonWidget(
                context,
                noExpanded: true,
                color: Theme.of(context).colorScheme.secondary,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      tr('quick_offer'),
                      style:
                          const TextStyle(fontSize: 14.0, color: Colors.white),
                    ),
                  ],
                ),
                onTap: () {},
              ),
            ),
            SmartGaps.gapH10,
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                  border: Border.all(
                      color: Theme.of(context).colorScheme.secondary)),
              child: expandedButtonWidget(
                context,
                noExpanded: true,
                color: Theme.of(context).colorScheme.secondary,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      tr('upload_offer'),
                      style:
                          const TextStyle(fontSize: 14.0, color: Colors.white),
                    ),
                  ],
                ),
                onTap: () {},
              ),
            ),
          ],
        ));
  }

  Widget expandedButtonWidget(
    BuildContext context, {
    required Widget child,
    required Function() onTap,
    Color color = PartnerAppColors.darkBlue,
    EdgeInsets padding = const EdgeInsets.symmetric(vertical: 10.0),
    bool noExpanded = false,
    elevation = 0.0,
  }) {
    if (!noExpanded) {
      return Expanded(
        child: Material(
          elevation: elevation,
          color: color,
          child: InkWell(
              onTap: onTap,
              child: Padding(padding: padding, child: Center(child: child))),
        ),
      );
    } else {
      return Material(
        elevation: elevation,
        color: color,
        child: InkWell(
            onTap: onTap,
            child: Padding(padding: padding, child: Center(child: child))),
      );
    }
  }
}

class ProjectTag extends StatelessWidget {
  const ProjectTag({super.key, this.tags});
  final List<ApprovedProjectTag>? tags;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 10),
        child: RichText(
          textAlign: TextAlign.justify,
          text: TextSpan(children: [
            ...tags!.map(
              (tag) => WidgetSpan(
                child: Builder(builder: (context) {
                  return Container(
                    padding: const EdgeInsets.all(5),
                    margin:
                        const EdgeInsets.symmetric(horizontal: 2, vertical: 2),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey, width: 1),
                    ),
                    child: Text(tag.da!,
                        style: Theme.of(context).textTheme.bodyMedium),
                  );
                }),
              ),
            )
          ]),
        ));
  }
}
