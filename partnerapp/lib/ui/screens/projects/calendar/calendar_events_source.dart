import 'package:Haandvaerker.dk/model/calendar/calendar.dart';
import 'package:Haandvaerker.dk/viewmodel/calendar/calendar_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class CalendarEventsSource extends CalendarDataSource {
  final BuildContext context;
  CalendarEventsSource(List<Calendar> source, this.context) {
    appointments = source;
  }

  @override
  DateTime getStartTime(int index) {
    return DateTime.parse(
        '${(_getEventData(index).date ?? '').split(' ').first} ${_getEventData(index).startTime}');
  }

  @override
  DateTime getEndTime(int index) {
    return DateTime.parse((_getEventData(index).endDate ?? '').isEmpty
        ? '${(_getEventData(index).date ?? '').split(' ').first} ${_getEventData(index).endTime}'
        : '${(_getEventData(index).endDate ?? '').split(' ').first} ${_getEventData(index).endTime}');
  }

  @override
  String getSubject(int index) {
    return _getEventData(index).title ?? '';
  }

  @override
  Color getColor(int index) {
    try {
      final calendarType = _getEventData(index).type;

      final type = context.watch<CalendarViewmodel>().calendarTypes.firstWhere(
            (element) =>
                element.typeDa == calendarType || element.type == calendarType,
            orElse: () => CalendarTypes.defaults(),
          );

      return Color(int.parse('0xff${type.color}'));
    } catch (e) {
      return const Color(0xff003645);
    }
  }

  Calendar _getEventData(int index) {
    final dynamic event = appointments![index];
    late final Calendar eventData;
    if (event is Calendar) {
      eventData = event;
    }

    return eventData;
  }
}
