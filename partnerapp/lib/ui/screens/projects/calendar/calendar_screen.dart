import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/calendar/add_update_calendar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/calendar/calendar_detail.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/calendar/calendar_events_source.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/calendar/calendar_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class CalendarScreen extends StatefulWidget {
  const CalendarScreen({super.key});

  @override
  State<CalendarScreen> createState() => _CalendarScreenState();
}

class _CalendarScreenState extends State<CalendarScreen> {
  CalendarController calendarController = CalendarController();

  bool showButton = false;

  @override
  void initState() {
    calendarController.displayDate = DateTime.now();
    calendarController.selectedDate = DateTime.now();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final calendarVm = context.read<CalendarViewmodel>();
      final projectsVm = context.read<MyProjectsViewModel>();

      await calendarVm.getProjectCalendarEvents(
          projectId: projectsVm.activeProject.id ?? 0);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      floatingActionButton: FloatingActionButton(
        backgroundColor:
            showButton ? PartnerAppColors.blue : PartnerAppColors.spanishGrey,
        child: const Icon(FeatherIcons.plus, size: 30, color: Colors.white),
        onPressed: () {
          if (showButton) {
            addUpdateCalendarDialog(
                context: context,
                calendar: null,
                initialDate: calendarController.selectedDate!);
          }
        },
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(tr('calendar'),
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(color: PartnerAppColors.darkBlue)),
            SmartGaps.gapH10,
            Flexible(
              child: Consumer<CalendarViewmodel>(builder: (_, vm, __) {
                final projectsVm = context.read<MyProjectsViewModel>();

                return Skeletonizer(
                  enabled: vm.busy,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text('${tr('project')}: ',
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineSmall!
                                  .copyWith(
                                      color: PartnerAppColors.darkBlue,
                                      fontWeight: FontWeight.normal)),
                          SmartGaps.gapW10,
                          Flexible(
                            child: Text(projectsVm.activeProject.name ?? '',
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineSmall!
                                    .copyWith(
                                        color: PartnerAppColors.darkBlue,
                                        fontWeight: FontWeight.bold)),
                          ),
                        ],
                      ),
                      SmartGaps.gapH20,
                      Expanded(
                        child: SfCalendar(
                          controller: calendarController,
                          headerStyle: const CalendarHeaderStyle(
                              textAlign: TextAlign.center),
                          appointmentTextStyle:
                              TextStyle(locale: context.locale),
                          showNavigationArrow: true,
                          dataSource: CalendarEventsSource(
                              [...vm.calendarsEvents], context),
                          view: CalendarView.month,
                          todayTextStyle: const TextStyle(color: Colors.white),
                          monthViewSettings: MonthViewSettings(
                              showAgenda: true,
                              agendaStyle: AgendaStyle(
                                  appointmentTextStyle: Theme.of(context)
                                      .textTheme
                                      .headlineSmall!
                                      .copyWith(color: Colors.white)),
                              agendaViewHeight:
                                  MediaQuery.of(context).size.height / 4),
                          onTap: (calendarTapDetails) {
                            if (calendarTapDetails.targetElement.name ==
                                'appointment') {
                              if ((calendarTapDetails.appointments ?? [])
                                  .isNotEmpty) {
                                calendarDetailDialog(
                                    context: context,
                                    calendar:
                                        calendarTapDetails.appointments!.first);
                              }
                            } else if (calendarTapDetails.targetElement.name ==
                                'calendarCell') {
                              setState(() {
                                showButton = calendarTapDetails.date!.isAfter(
                                    DateTime.now()
                                        .copyWith(day: DateTime.now().day - 1));
                              });
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                );
              }),
            ),
          ],
        ),
      ),
    );
  }
}
