import 'package:Haandvaerker.dk/model/calendar/calendar.dart';
import 'package:Haandvaerker.dk/model/messages/contacts_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/calendar/add_update_calendar.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/calendar/calendar_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';

Future<void> calendarDetailDialog({
  required BuildContext context,
  required Calendar calendar,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          GestureDetector(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(Icons.close),
          )
        ]),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: CalendarDetail(
              calendar: calendar,
            ))),
      );
    },
  );
}

class CalendarDetail extends StatefulWidget {
  const CalendarDetail({super.key, required this.calendar});

  final Calendar calendar;

  @override
  State<CalendarDetail> createState() => _CalendarDetailState();
}

class _CalendarDetailState extends State<CalendarDetail> {
  @override
  Widget build(BuildContext context) {
    return Consumer<CalendarViewmodel>(builder: (_, vm, __) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(
                  Formatter.formatDateStrings(
                      type: DateFormatType.fullMonthDate,
                      dateString: widget.calendar.date),
                  style: Theme.of(context).textTheme.displayLarge!.copyWith(
                      color: PartnerAppColors.blue,
                      fontWeight: FontWeight.w600,
                      fontSize: 20),
                ),
              ),
              SmartGaps.gapW10,
              Row(
                children: [
                  if (DateTime.parse(widget.calendar.date!).isAfter(
                      DateTime.now()
                          .copyWith(day: DateTime.now().day - 1))) ...[
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                        addUpdateCalendarDialog(
                            context: context,
                            calendar: widget.calendar,
                            initialDate: DateTime.now());
                      },
                      child: const Icon(
                        FeatherIcons.edit3,
                        color: PartnerAppColors.blue,
                      ),
                    ),
                    SmartGaps.gapW10,
                  ],
                  GestureDetector(
                    onTap: () {
                      final projectsVm = context.read<MyProjectsViewModel>();
                      final navigator = Navigator.of(context);

                      showOkCancelAlertDialog(
                              context: context,
                              title: tr('are_you_sure_to_delete_event'),
                              message:
                                  '${tr('event')}: ${widget.calendar.title ?? ''}')
                          .then((value) async {
                        if (value == OkCancelResult.ok) {
                          navigator.pop();
                          await vm.deleteEvent(
                              calendarId: widget.calendar.id!,
                              projectId: projectsVm.activeProject.id!);
                        }
                      });
                    },
                    child: const Icon(FeatherIcons.trash2,
                        color: PartnerAppColors.red),
                  )
                ],
              )
            ],
          ),
          SmartGaps.gapH20,
          Text(
            widget.calendar.title ?? '',
            style: Theme.of(context).textTheme.displayLarge!.copyWith(
                color: const Color(0xff003645),
                fontWeight: FontWeight.w600,
                fontSize: 20),
          ),
          const Divider(),
          Text(
            widget.calendar.description ?? '',
            style: Theme.of(context).textTheme.displayLarge!.copyWith(
                color: const Color(0xff003645),
                fontWeight: FontWeight.w400,
                fontSize: 12),
          ),
          SmartGaps.gapH30,
          RichText(
              text: TextSpan(children: [
            const WidgetSpan(
                child: Icon(
                  FeatherIcons.clock,
                  color: PartnerAppColors.blue,
                  size: 20,
                ),
                alignment: PlaceholderAlignment.middle),
            TextSpan(
                text:
                    ' ${DateFormat.Hm().format(DateTime.parse('1970-01-01 ${widget.calendar.startTime}'))} - ${DateFormat.Hm().format(DateTime.parse('1970-01-01 ${widget.calendar.endTime}'))}',
                style: Theme.of(context).textTheme.displaySmall!.copyWith(
                    color: PartnerAppColors.darkBlue,
                    fontSize: 16,
                    fontWeight: FontWeight.w400))
          ])),
          if ((widget.calendar.contactId ?? 0) > 0) ...[
            SmartGaps.gapH10,
            RichText(
                text: TextSpan(children: [
              const WidgetSpan(
                  child: Icon(
                    FeatherIcons.share2,
                    color: PartnerAppColors.blue,
                    size: 20,
                  ),
                  alignment: PlaceholderAlignment.middle),
              TextSpan(
                  text: ' ${vm.contacts.firstWhere(
                        (element) => element.id == widget.calendar.contactId,
                        orElse: () => ContactsResponseModel.defaults(),
                      ).name}',
                  style: Theme.of(context).textTheme.displaySmall!.copyWith(
                      color: PartnerAppColors.darkBlue,
                      fontSize: 16,
                      fontWeight: FontWeight.bold))
            ]))
          ],
          SmartGaps.gapH20,
          RichText(
              text: TextSpan(children: [
            WidgetSpan(
                child: Container(
                  height: 20,
                  width: 20,
                  decoration: BoxDecoration(
                      color: getColor(
                          widget.calendar.type ?? '', vm.calendarTypes),
                      shape: BoxShape.circle),
                ),
                alignment: PlaceholderAlignment.middle),
            TextSpan(
                text:
                    ' ${parseCalendarType(widget.calendar.type ?? '', vm.calendarTypes)}',
                style: Theme.of(context).textTheme.displaySmall!.copyWith(
                    color: const Color(0xff003645),
                    fontSize: 16,
                    fontWeight: FontWeight.w400))
          ]))
        ],
      );
    });
  }

  Color getColor(String calenderType, List<CalendarTypes> calendarTypes) {
    final type = calendarTypes.firstWhere(
      (element) =>
          element.typeDa == calenderType || element.type == calenderType,
      orElse: () => CalendarTypes.defaults(),
    );

    return Color(int.parse('0xff${type.color}'));
  }

  String parseCalendarType(
      String calenderType, List<CalendarTypes> calendarTypes) {
    final isDanish = context.locale.languageCode == 'da';
    final type = calendarTypes.firstWhere(
      (element) =>
          element.typeDa == calenderType || element.type == calenderType,
      orElse: () => CalendarTypes.defaults(),
    );
    return isDanish ? (type.typeDa ?? '') : (type.type ?? '');
  }
}
