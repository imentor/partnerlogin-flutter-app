import 'package:Haandvaerker.dk/model/calendar/calendar.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/messages/components.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/calendar/calendar_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

Future<void> addUpdateCalendarDialog({
  required BuildContext context,
  Calendar? calendar,
  required DateTime initialDate,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text(tr('add_event'),
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium!
                  .copyWith(color: PartnerAppColors.darkBlue)),
          GestureDetector(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(Icons.close),
          )
        ]),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: AddUpdateCalendar(
              calendar: calendar,
              initialDate: initialDate,
            ))),
      );
    },
  );
}

class AddUpdateCalendar extends StatefulWidget {
  const AddUpdateCalendar(
      {super.key, this.calendar, required this.initialDate});

  final Calendar? calendar;
  final DateTime initialDate;

  @override
  State<AddUpdateCalendar> createState() => _AddUpdateCalendarState();
}

class _AddUpdateCalendarState extends State<AddUpdateCalendar> {
  final formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: formKey,
      child: Consumer<CalendarViewmodel>(builder: (_, vm, __) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomDatePickerFormBuilder(
              name: 'date',
              labelText: tr('date'),
              isRequired: true,
              validator:
                  FormBuilderValidators.required(errorText: tr('required')),
              initialValue: (widget.calendar?.date ?? '').isNotEmpty
                  ? DateTime.parse(widget.calendar!.date!)
                  : widget.initialDate,
            ),
            CustomDropdownFormBuilder(
              items: [
                ...vm.contacts.map((e) => DropdownMenuItem(
                    value: '${e.id}', child: Text(e.name ?? '')))
              ],
              name: 'contact',
              labelText: tr('contact_list'),
              initialValue: (widget.calendar?.contactId ?? 0) > 0
                  ? '${widget.calendar!.contactId!}'
                  : null,
            ),
            CustomTextFieldFormBuilder(
              name: 'event_name',
              labelText: tr('event_name'),
              isRequired: true,
              validator:
                  FormBuilderValidators.required(errorText: tr('required')),
              initialValue: widget.calendar?.title,
            ),
            CustomTextFieldFormBuilder(
              name: 'event_description',
              labelText: tr('event_description'),
              isRequired: true,
              validator:
                  FormBuilderValidators.required(errorText: tr('required')),
              initialValue: widget.calendar?.description,
            ),
            CustomDropdownFormBuilder(
              items: [
                ...vm.calendarTypes.map((e) => DropdownMenuItem(
                    value: '${e.type}',
                    child: Text(context.locale.languageCode == 'da'
                        ? e.typeDa ?? ''
                        : e.type ?? '')))
              ],
              name: 'event_type',
              labelText: tr('event_type'),
              validator:
                  FormBuilderValidators.required(errorText: tr('required')),
              isRequired: true,
              initialValue: (widget.calendar?.type ?? '').isNotEmpty
                  ? vm.calendarTypes
                      .firstWhere(
                          (element) => element.type == widget.calendar?.type)
                      .type
                  : null,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: CustomDatePickerFormBuilder(
                    name: 'start_time',
                    labelText: tr('start_time'),
                    isRequired: true,
                    inputType: InputType.time,
                    validator: FormBuilderValidators.required(
                        errorText: tr('required')),
                    initialValue: (widget.calendar?.startTime ?? '').isNotEmpty
                        ? DateTime.parse(
                            '0001-01-01 ${widget.calendar!.startTime!}')
                        : null,
                  ),
                ),
                SmartGaps.gapW10,
                Expanded(
                  child: CustomDatePickerFormBuilder(
                    name: 'end_time',
                    labelText: tr('end_time'),
                    isRequired: true,
                    inputType: InputType.time,
                    validator: FormBuilderValidators.required(
                        errorText: tr('required')),
                    initialValue: (widget.calendar?.endTime ?? '').isNotEmpty
                        ? DateTime.parse(
                            '0001-01-01 ${widget.calendar!.endTime!}')
                        : null,
                  ),
                ),
              ],
            ),
            SmartGaps.gapH20,
            CustomButton(
              text: widget.calendar == null ? tr('save') : tr('update'),
              onPressed: () async {
                final projectsVm = context.read<MyProjectsViewModel>();

                int projectId = 0;

                if (widget.calendar?.projectId == null) {
                  projectId = projectsVm.activeProject.id!;
                } else {
                  projectId = widget.calendar!.projectId!;
                }

                if (formKey.currentState!.validate()) {
                  Map<String, dynamic> payload = {
                    "title": formKey.currentState!.fields['event_name']!.value,
                    "description": formKey
                        .currentState!.fields['event_description']!.value,
                    "date": (formKey.currentState!.fields['date']!.value
                            as DateTime)
                        .toString()
                        .split(' ')
                        .first,
                    "startTime": (formKey.currentState!.fields['start_time']!
                            .value as DateTime)
                        .toString()
                        .split(' ')
                        .last
                        .split('.')
                        .first,
                    "endTime": (formKey.currentState!.fields['end_time']!.value
                            as DateTime)
                        .toString()
                        .split(' ')
                        .last
                        .split('.')
                        .first,
                    "type": formKey.currentState!.fields['event_type']!.value,
                    "addressId": projectsVm.activeProject.address!.id
                  };

                  if (formKey.currentState!.fields.containsKey('contact') &&
                      formKey.currentState!.fields['contact']!.value != null) {
                    payload['contactId'] =
                        formKey.currentState!.fields['contact']!.value;
                  }

                  if (widget.calendar == null) {
                    payload['projectId'] = projectsVm.activeProject.id!;
                  }

                  Navigator.of(context).pop();
                  await vm.addUpdateEvent(
                      payload: payload,
                      projectId: projectId,
                      calendarId: widget.calendar?.id);
                }
              },
            ),
          ],
        );
      }),
    );
  }
}
