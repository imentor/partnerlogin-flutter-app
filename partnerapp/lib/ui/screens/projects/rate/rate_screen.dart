import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/recommendation_read_answer_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:timeline_tile/timeline_tile.dart';

class RateScreen extends StatefulWidget {
  const RateScreen({super.key});

  @override
  State<RateScreen> createState() => _RateScreenState();
}

class _RateScreenState extends State<RateScreen> {
  int currentStep = 0;
  final ItemScrollController itemScrollController = ItemScrollController();
  final formKey = GlobalKey<FormBuilderState>();

  double ratingAccess = 0;
  double ratingCollaboration = 0;
  double ratingCommunication = 0;
  double ratingFinancialSecurity = 0;
  double ratingRealisticExpectations = 0;
  double ratingTime = 0;

  void onPressedSubmit() async {
    final recommendationVm = context.read<RecommendationReadAnswerViewModel>();
    final projectsVm = context.read<MyProjectsViewModel>();

    if (currentStep == 0) {
      setState(() {
        currentStep += 1;
      });
    } else if (currentStep == 1) {
      if (formKey.currentState!.validate()) {
        final result = await showOkCancelAlertDialog(
            context: context, title: tr('submit_this_review'));

        if (mounted && result == OkCancelResult.ok) {
          modalManager.showLoadingModal();

          await tryCatchWrapper(
              context: context,
              function: recommendationVm.savePartnerReviewOnHomeowner(
                comment: formKey.currentState!.fields['comment']!.value,
                customerId: projectsVm.activeProject.homeOwner!.id!,
                enterpriseSum: 0,
                headline: formKey.currentState!.fields['title']!.value,
                rating: ((ratingAccess +
                            ratingCollaboration +
                            ratingCommunication +
                            ratingFinancialSecurity +
                            ratingRealisticExpectations +
                            ratingTime) /
                        6)
                    .ceil(),
                ratingAccess: ratingAccess.ceil(),
                ratingCollaboration: ratingCollaboration.ceil(),
                ratingCommunication: ratingCommunication.ceil(),
                ratingFinancialSecurity: ratingFinancialSecurity.ceil(),
                ratingRealisticExpectations: ratingRealisticExpectations.ceil(),
                ratingTime: ratingTime.ceil(),
              ));

          if (!mounted) return;

          modalManager.hideLoadingModal();

          showOkAlertDialog(context: context, title: tr('review_sent'))
              .whenComplete(() {
            changeDrawerRoute(Routes.sentRecommendations);
          });
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      bottomNavigationBar: Container(
        padding:
            const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
        color: Colors.white,
        child: Row(children: [
          if (currentStep > 0) ...[
            Expanded(
              child: TextButton(
                style: TextButton.styleFrom(
                    fixedSize: Size(MediaQuery.of(context).size.width, 45),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                        side: const BorderSide(
                            color: PartnerAppColors.darkBlue))),
                onPressed: () {
                  setState(() {
                    currentStep -= 1;
                  });
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(
                      FeatherIcons.arrowLeft,
                      color: PartnerAppColors.darkBlue,
                    ),
                    SmartGaps.gapW5,
                    Text(
                      tr('back'),
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(color: PartnerAppColors.darkBlue),
                    ),
                  ],
                ),
              ),
            ),
            SmartGaps.gapW20,
          ],
          Expanded(
            child: TextButton(
              style: TextButton.styleFrom(
                  fixedSize: Size(MediaQuery.of(context).size.width, 45),
                  backgroundColor: PartnerAppColors.malachite,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  )),
              onPressed: () => onPressedSubmit(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    currentStep == 0 ? tr('next') : tr('submit'),
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium!
                        .copyWith(color: Colors.white),
                  ),
                  SmartGaps.gapW5,
                  const Icon(
                    FeatherIcons.arrowRight,
                    color: Colors.white,
                  )
                ],
              ),
            ),
          ),
        ]),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: FormBuilder(
          key: formKey,
          child: Column(
            children: [
              Text(tr('thank_you_for_giving_recommendation'),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headlineLarge),
              SmartGaps.gapH20,
              Text(tr('create_recommendation_description'),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyMedium),
              SizedBox(
                height: 65,
                width: MediaQuery.of(context).size.width,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TimelineTile(
                        axis: TimelineAxis.horizontal,
                        alignment: TimelineAlign.center,
                        isFirst: true,
                        isLast: false,
                        indicatorStyle: IndicatorStyle(
                          width: 30.0,
                          height: 30.0,
                          indicator: DecoratedBox(
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(color: PartnerAppColors.blue),
                              color: currentStep == 0
                                  ? PartnerAppColors.blue
                                  : Colors.white,
                            ),
                            child: Center(
                                child: Text('1',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodySmall!
                                        .copyWith(
                                            color: currentStep == 0
                                                ? Colors.white
                                                : PartnerAppColors.blue,
                                            fontWeight: FontWeight.bold))),
                          ),
                        ),
                        beforeLineStyle: const LineStyle(
                            thickness: 1.0,
                            color: PartnerAppColors.spanishGrey),
                        endChild: Container(
                          margin: const EdgeInsets.only(top: 5),
                          constraints: const BoxConstraints(
                            maxHeight: 30,
                            minWidth: 50,
                          ),
                          child: Text('${tr('step')} 1',
                              style: Theme.of(context).textTheme.bodySmall,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.center),
                        ),
                      ),
                      TimelineTile(
                        axis: TimelineAxis.horizontal,
                        alignment: TimelineAlign.center,
                        isFirst: false,
                        isLast: true,
                        indicatorStyle: IndicatorStyle(
                          width: 30.0,
                          height: 30.0,
                          indicator: DecoratedBox(
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(color: PartnerAppColors.blue),
                              color: currentStep == 1
                                  ? PartnerAppColors.blue
                                  : Colors.white,
                            ),
                            child: Center(
                                child: Text('2',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodySmall!
                                        .copyWith(
                                            color: currentStep == 1
                                                ? Colors.white
                                                : PartnerAppColors.blue,
                                            fontWeight: FontWeight.bold))),
                          ),
                        ),
                        beforeLineStyle: const LineStyle(
                            thickness: 1.0,
                            color: PartnerAppColors.spanishGrey),
                        endChild: Container(
                          margin: const EdgeInsets.only(top: 5),
                          constraints: const BoxConstraints(
                            maxHeight: 30,
                            minWidth: 50,
                          ),
                          child: Text('${tr('step')} 2',
                              style: Theme.of(context).textTheme.bodySmall,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.center),
                        ),
                      )
                    ]),
              ),
              SmartGaps.gapH20,
              if (currentStep == 0) ...[
                Container(
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      border: Border.all(color: PartnerAppColors.blue),
                      borderRadius: BorderRadius.circular(5)),
                  child: Column(children: [
                    ratingBar(
                      title: tr('communication'),
                      description: tr('communication_description'),
                      onRatingUpdate: (p0) {
                        setState(() {
                          ratingAccess = p0;
                        });
                      },
                    ),
                    SmartGaps.gapH10,
                    ratingBar(
                      title: tr('meet_the_schedules'),
                      description: tr('schedules_description'),
                      onRatingUpdate: (p0) {
                        setState(() {
                          ratingCollaboration = p0;
                        });
                      },
                    ),
                    SmartGaps.gapH10,
                    ratingBar(
                      title: tr('realistic_expectation'),
                      description: tr('realistic_description'),
                      onRatingUpdate: (p0) {
                        setState(() {
                          ratingCommunication = p0;
                        });
                      },
                    ),
                    SmartGaps.gapH10,
                    ratingBar(
                      title: tr('cooperation'),
                      description: tr('cooperation_description'),
                      onRatingUpdate: (p0) {
                        setState(() {
                          ratingFinancialSecurity = p0;
                        });
                      },
                    ),
                    SmartGaps.gapH10,
                    ratingBar(
                      title: tr('financial_security'),
                      description: tr('financial_description'),
                      onRatingUpdate: (p0) {
                        setState(() {
                          ratingRealisticExpectations = p0;
                        });
                      },
                    ),
                    SmartGaps.gapH10,
                    ratingBar(
                      title: tr('access_to_work'),
                      description: tr('access_description'),
                      onRatingUpdate: (p0) {
                        setState(() {
                          ratingTime = p0;
                        });
                      },
                    ),
                  ]),
                ),
                SmartGaps.gapH20,
                Builder(builder: (context) {
                  final overAllRating = (ratingAccess +
                          ratingCollaboration +
                          ratingCommunication +
                          ratingFinancialSecurity +
                          ratingRealisticExpectations +
                          ratingTime) /
                      6;
                  return Container(
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        border: Border.all(color: PartnerAppColors.blue),
                        borderRadius: BorderRadius.circular(5)),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(tr('overall_assesment'),
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .copyWith(fontWeight: FontWeight.normal)),
                          SmartGaps.gapW20,
                          RatingBar(
                            itemSize: 30,
                            initialRating: overAllRating,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            ratingWidget: RatingWidget(
                              full: Image.asset(ImagePaths.singleHouse),
                              half: Image.asset(ImagePaths.halfHouse),
                              empty: Opacity(
                                opacity: 0.1,
                                child: Image.asset(ImagePaths.singleHouse),
                              ),
                            ),
                            itemPadding:
                                const EdgeInsets.symmetric(horizontal: 1.5),
                            onRatingUpdate: (value) {},
                          )
                        ]),
                  );
                })
              ] else if (currentStep == 1) ...[
                CustomTextFieldFormBuilder(
                  name: 'title',
                  labelText: tr('title'),
                  validator:
                      FormBuilderValidators.required(errorText: tr('required')),
                ),
                CustomTextFieldFormBuilder(
                  name: 'comment',
                  labelText: tr('comment'),
                  minLines: 5,
                  maxLines: 5,
                  validator:
                      FormBuilderValidators.required(errorText: tr('required')),
                ),
              ]
            ],
          ),
        ),
      ),
    );
  }

  Widget ratingBar(
      {required String title,
      required String description,
      required void Function(double) onRatingUpdate}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(children: [
          Text(title,
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium!
                  .copyWith(fontWeight: FontWeight.normal)),
          SmartGaps.gapW5,
          Tooltip(
            showDuration: const Duration(seconds: 3),
            triggerMode: TooltipTriggerMode.tap,
            // margin: const EdgeInsets.all(20),
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(
                  color: PartnerAppColors.darkBlue,
                ),
                borderRadius: BorderRadius.circular(5)),
            textStyle: Theme.of(context).textTheme.headlineSmall!.copyWith(
                color: PartnerAppColors.darkBlue,
                fontSize: 14,
                fontWeight: FontWeight.normal),
            message: description,
            child: const Icon(FeatherIcons.info, size: 16),
          )
        ]),
        SmartGaps.gapW20,
        RatingBar(
          itemSize: 30,
          initialRating: 1,
          direction: Axis.horizontal,
          allowHalfRating: false,
          itemCount: 5,
          ratingWidget: RatingWidget(
            full: Image.asset(ImagePaths.singleHouse),
            half: Image.asset(ImagePaths.halfHouse),
            empty: Opacity(
              opacity: 0.1,
              child: Image.asset(ImagePaths.singleHouse),
            ),
          ),
          itemPadding: const EdgeInsets.symmetric(horizontal: 1.5),
          onRatingUpdate: onRatingUpdate,
        )
      ],
    );
  }
}
