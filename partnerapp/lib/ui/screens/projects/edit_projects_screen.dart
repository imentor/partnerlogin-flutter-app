import 'package:Haandvaerker.dk/model/partner/supplier_info_file_model.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/create_project/create_project_components.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/edit_projects_components.dart';
import 'package:Haandvaerker.dk/utils/extension/snackbar.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/create_projects_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:show_up_animation/show_up_animation.dart';

class EditProjectsScreen extends StatefulWidget {
  const EditProjectsScreen({super.key});

  @override
  EditProjectsScreenState createState() => EditProjectsScreenState();
}

class EditProjectsScreenState extends State<EditProjectsScreen> {
  final loadingKey = GlobalKey<NavigatorState>();

  final firstFormKey = GlobalKey<FormState>();
  late TextEditingController title;
  late TextEditingController description;
  late TextEditingController price;
  late TextEditingController projectCreated;
  late TextEditingController time;
  late TextEditingController zipController;
  bool validate = false;

  @override
  void initState() {
    title = TextEditingController();
    description = TextEditingController();
    price = TextEditingController();
    projectCreated = TextEditingController();
    time = TextEditingController();
    zipController = TextEditingController();

    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(
      (_) async {
        final createProjectVm = context.read<CreateProjectsViewModel>();
        createProjectVm.setValuesToDefault();

        final currentProject = createProjectVm.currentProject;

        // ASSIGNING THE VALUES INSIDE
        title.text = currentProject?.title ?? '';
        description.text = currentProject?.description ?? '';
        price.text = currentProject?.amount?.split('.').first ?? '';
        projectCreated.text = currentProject?.createdOn?.split(' ').first ?? '';
        time.text = currentProject?.time ?? '';
        zipController.text = currentProject?.zip ?? '';

        if (currentProject?.subTaskType != null &&
            currentProject?.subTaskType.isNotEmpty) {
          createProjectVm.selectedJobTypes = createProjectVm.jobTypes
              .where((element) => currentProject?.subTaskType.runtimeType == int
                  ? currentProject?.subTaskType.contains(element.subcategoryId)
                  : currentProject?.subTaskType
                      .contains(element.subcategoryId.toString()))
              .toList();
        }

        if (currentProject?.manufactureIds != null &&
            currentProject?.manufactureIds.isNotEmpty) {
          createProjectVm.selectedManufactures = createProjectVm.manufactures
              .where((element) => currentProject?.manufactureIds
                  .contains(element.manufactureId))
              .toList();
        }

        final projectPictures =
            currentProject?.pictures ?? <SupplierInfoFileModel>[];

        if (projectPictures.isNotEmpty) {
          createProjectVm.currentProjectImages = projectPictures;
        }
      },
    );
  }

  @override
  void dispose() {
    title.dispose();
    description.dispose();
    price.dispose();
    projectCreated.dispose();
    time.dispose();
    zipController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Container(
        padding: const EdgeInsets.only(top: 0, left: 20.0, right: 20.0),
        child: SingleChildScrollView(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SmartGaps.gapH20,
                Text(
                  tr('edit_project'),
                  style: Theme.of(context).textTheme.headlineLarge,
                ),
                SmartGaps.gapH10,
                Text(tr('update_your_projects_desc'),
                    style: Theme.of(context).textTheme.titleSmall),
                SmartGaps.gapH30,
                context.watch<CreateProjectsViewModel>().busy
                    ? Align(
                        alignment: Alignment.center,
                        child: loader(),
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(tr('job_type'),
                                style:
                                    Theme.of(context).textTheme.headlineSmall),
                          ),
                          SmartGaps.gapH10,
                          const JobTypeSearch(),
                          SmartGaps.gapH10,
                          JobTypeList(
                            filter: '',
                            isReadOnly: true,
                            existingTaskTypes: [
                              '${context.watch<CreateProjectsViewModel>().currentProject?.taskTypeIdsText}'
                            ],
                          ),
                          SmartGaps.gapH10,
                          _getForm(),
                          SmartGaps.gapH10,
                          Consumer<CreateProjectsViewModel>(
                            builder: (context, vm, __) {
                              return const EditProjectPictureList();
                            },
                          ),
                          if (validate)
                            Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: ShowUpAnimation(
                                animationDuration:
                                    const Duration(milliseconds: 300),
                                curve: Curves.ease,
                                direction: Direction.vertical,
                                offset: -0.5,
                                child: Container(
                                  width: double.infinity,
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 10),
                                  child: Text(
                                    tr('required'),
                                    textAlign: TextAlign.start,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyMedium!
                                        .copyWith(
                                            color: Colors.red,
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w200),
                                  ),
                                ),
                              ),
                            ),
                          SmartGaps.gapH20,
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(tr('manufacturers'),
                                style:
                                    Theme.of(context).textTheme.headlineSmall),
                          ),
                          SmartGaps.gapH10,
                          const ManufactureTypeSearch(),
                          SmartGaps.gapH10,
                          const ManufactureList(
                            isReadOnly: true,
                            filter: '',
                            chosenIndex: 'a',
                          ),
                          SmartGaps.gapH30,
                          SizedBox(
                            height: 60,
                            width: double.infinity,
                            child: CustomDesignTheme.flatButtonStyle(
                              backgroundColor:
                                  Theme.of(context).colorScheme.secondary,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(tr('save'),
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleMedium!
                                          .copyWith(
                                              color: Colors.white, height: 1)),
                                ],
                              ),
                              onPressed: () async {
                                final vm =
                                    context.read<CreateProjectsViewModel>();
                                if ((vm.currentProject?.pictures ??
                                                <SupplierInfoFileModel>[])
                                            .length ==
                                        vm.deletedProjectPictures.length &&
                                    vm.projectImages.isEmpty) {
                                  setState(() {
                                    validate = true;
                                  });
                                } else {
                                  setState(() {
                                    validate = false;
                                  });

                                  if (vm.selectedJobTypes.isEmpty) {
                                    vm.validateJobTypes = true;
                                  } else {
                                    vm.validateJobTypes = false;
                                  }
                                  if (firstFormKey.currentState!.validate() &&
                                      !vm.validateJobTypes) {
                                    vm.price = price.text.trim();
                                    vm.description = description.text.trim();
                                    vm.projectCreated =
                                        projectCreated.text.trim();
                                    vm.time = time.text.trim();
                                    vm.title = title.text.trim();
                                    vm.post = zipController.text.trim();

                                    await editProject();
                                  }
                                }
                              },
                            ),
                          ),
                        ],
                      ),
              ]),
        ),
      ),
    );
  }

  Widget _getForm() {
    return Form(
      key: firstFormKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SmartGaps.gapH20,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              RichText(
                maxLines: 2,
                text: TextSpan(
                  text: '${tr('name_of_project')}:',
                  style: Theme.of(context).textTheme.titleSmall,
                  children: const <TextSpan>[
                    TextSpan(
                      text: '*',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.red),
                    ),
                  ],
                ),
              ),
              const Icon(Icons.edit_outlined, color: Colors.black, size: 20),
            ],
          ),
          SmartGaps.gapH5,
          TextFormField(
              controller: title,
              style: Theme.of(context).textTheme.titleSmall,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withAlpha(100)),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withAlpha(100)),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withAlpha(100)),
                  ),
                  filled: true,
                  fillColor: Colors.transparent,
                  hintStyle: Theme.of(context)
                      .textTheme
                      .titleSmall!
                      .copyWith(fontWeight: FontWeight.w700),
                  hintText: tr('name')),
              validator: (value) {
                if (value!.isEmpty) {
                  return tr('please_enter_some_text');
                }
                return null;
              }),
          SmartGaps.gapH20,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              RichText(
                maxLines: 3,
                text: TextSpan(
                  text: tr('description'),
                  style: Theme.of(context)
                      .textTheme
                      .titleSmall!
                      .copyWith(height: 1.2),
                  children: const <TextSpan>[
                    TextSpan(
                      text: '*',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.red),
                    ),
                  ],
                ),
              ),
              const Icon(Icons.edit_outlined, color: Colors.black, size: 20),
            ],
          ),
          SmartGaps.gapH5,
          TextFormField(
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.multiline,
              maxLines: null,
              controller: description,
              style: Theme.of(context).textTheme.titleSmall,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withAlpha(100)),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withAlpha(100)),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withAlpha(100)),
                  ),
                  hintStyle: Theme.of(context).textTheme.bodyMedium,
                  hintText: tr('description')),
              validator: (value) {
                if (value!.isEmpty) {
                  return tr('please_enter_some_text');
                }
                return null;
              }),
          SmartGaps.gapH20,
          RichText(
            maxLines: 2,
            text: TextSpan(
              text: '${tr('date_of_the_project')}:',
              style:
                  Theme.of(context).textTheme.titleSmall!.copyWith(height: 1),
              children: const <TextSpan>[
                TextSpan(
                  text: '*',
                  style:
                      TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
                ),
              ],
            ),
          ),
          SmartGaps.gapH5,
          InkWell(
            onTap: () async {
              await showDatePicker(
                      context: context,
                      firstDate: DateTime(1900),
                      initialDate: projectCreated.text.isEmpty
                          ? DateTime.now()
                          : DateTime.parse(projectCreated.text),
                      lastDate: DateTime(2100))
                  .then((value) {
                if (value != null) {
                  setState(() {
                    projectCreated.text = Formatter.formatDateTime(
                        type: DateFormatType.isoDate, date: value);
                  });
                }
              });
            },
            child: TextFormField(
                controller: projectCreated,
                enabled: false,
                style: Theme.of(context).textTheme.titleSmall,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withAlpha(100)),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withAlpha(100)),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withAlpha(100)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withAlpha(100)),
                    ),
                    filled: true,
                    fillColor: Colors.transparent,
                    hintStyle: Theme.of(context)
                        .textTheme
                        .titleSmall!
                        .copyWith(fontWeight: FontWeight.w700),
                    hintText: tr('name')),
                validator: (value) {
                  if (value!.isEmpty) {
                    return tr('please_enter_some_text');
                  }
                  return null;
                }),
          ),
          SmartGaps.gapH20,
          RichText(
            maxLines: 2,
            text: TextSpan(
              text: '${tr('time_number_of_days')}:',
              style:
                  Theme.of(context).textTheme.titleSmall!.copyWith(height: 1),
              children: const <TextSpan>[
                TextSpan(
                  text: '*',
                  style:
                      TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
                ),
              ],
            ),
          ),
          SmartGaps.gapH5,
          TextFormField(
              controller: time,
              style: Theme.of(context).textTheme.titleSmall,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withAlpha(100)),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withAlpha(100)),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withAlpha(100)),
                  ),
                  hintStyle: Theme.of(context).textTheme.titleSmall!.copyWith(
                        color: Colors.black.withValues(alpha: 0.5),
                      ),
                  hintText: tr('eg_4_6_weeks')),
              validator: (value) {
                return null;
              }),
          SmartGaps.gapH20,
          RichText(
            maxLines: 2,
            text: TextSpan(
              text: '${tr('price')} (kr.)',
              style:
                  Theme.of(context).textTheme.titleSmall!.copyWith(height: 1),
              children: const <TextSpan>[
                TextSpan(
                  text: '*',
                  style:
                      TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
                ),
              ],
            ),
          ),
          SmartGaps.gapH5,
          SmartGaps.gapH5,
          TextFormField(
              controller: price,
              keyboardType: TextInputType.number,
              style:
                  Theme.of(context).textTheme.titleSmall!.copyWith(height: 1),
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withAlpha(100)),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withAlpha(100)),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withAlpha(100)),
                  ),
                  hintStyle: Theme.of(context).textTheme.titleSmall,
                  hintText: tr('dkk_incl_vat')),
              validator: (value) {
                return null;
              }),
          SmartGaps.gapH20,
          RichText(
            maxLines: 2,
            text: TextSpan(
              text: tr('zip_code'),
              style:
                  Theme.of(context).textTheme.titleSmall!.copyWith(height: 1),
              children: const <TextSpan>[
                TextSpan(
                  text: '*',
                  style:
                      TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
                ),
              ],
            ),
          ),
          SmartGaps.gapH5,
          TextFormField(
              controller: zipController,
              keyboardType: TextInputType.number,
              style:
                  Theme.of(context).textTheme.titleSmall!.copyWith(height: 1),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                      color:
                          Theme.of(context).colorScheme.primary.withAlpha(100)),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color:
                          Theme.of(context).colorScheme.primary.withAlpha(100)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color:
                          Theme.of(context).colorScheme.primary.withAlpha(100)),
                ),
                hintStyle: Theme.of(context).textTheme.titleSmall!.copyWith(
                      color: Colors.black.withValues(alpha: 0.5),
                    ),
                hintText: '2300',
              ),
              validator: (value) {
                return null;
              }),
          SmartGaps.gapH20,
        ],
      ),
    );
  }

  Future<void> editProject() async {
    var viewModel = context.read<CreateProjectsViewModel>();
    final myProjectsVm = context.read<MyProjectsViewModel>();
    final userVm = context.read<UserViewModel>();

    showLoadingDialog(context, loadingKey);

    try {
      await viewModel.editProject().whenComplete(() async {
        Navigator.of(loadingKey.currentContext!, rootNavigator: true).pop();
        showSuccessAnimationDialog(
            myGlobals.homeScaffoldKey!.currentContext!,
            true,
            '${tr('hey')} ${userVm.userModel?.user?.name ?? ''}\n\n${tr('photo_available_24_hours')}');
        backDrawerRoute();

        myProjectsVm.setBusy(true);
        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: myProjectsVm.getProjects());
        myProjectsVm.setBusy(false);
      });
    } on ApiException catch (_) {
      Navigator.of(loadingKey.currentContext!, rootNavigator: true).pop();
      myGlobals.homeScaffoldKey!.currentContext!.showErrorSnackBar();
    }
  }
}
