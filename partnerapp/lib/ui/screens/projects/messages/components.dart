import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:html_editor_enhanced/html_editor.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({
    super.key,
    this.text = '',
    this.onPressed,
    this.backgroundColor,
    this.splashColor,
    this.textColor,
    this.textSize,
    this.fontWeight,
    this.icon,
    this.decoration = const BoxDecoration(),
    this.padding = const EdgeInsets.all(20),
    this.borderRadius,
  });

  final Function()? onPressed;
  final Color? backgroundColor;
  final Color? splashColor;
  final Color? textColor;
  final String text;
  final double? textSize;
  final FontWeight? fontWeight;
  final Widget? icon;
  final BoxDecoration decoration;
  final EdgeInsets padding;
  final BorderRadiusGeometry? borderRadius;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: backgroundColor ?? Theme.of(context).colorScheme.secondary,
      borderRadius: borderRadius ?? BorderRadius.circular(3),
      child: InkWell(
        highlightColor: Colors.transparent,
        splashColor: splashColor,
        onTap: onPressed,
        child: Container(
          padding: padding,
          width: double.infinity,
          alignment: Alignment.center,
          decoration: decoration,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (text != '')
                Flexible(
                  child: Text(text,
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.fade,
                      style: TextStyle(
                          fontSize: textSize ?? 20,
                          fontWeight: fontWeight ?? FontWeight.bold,
                          color: textColor ?? Colors.white,
                          height: 1)),
                ),
              if (text != '') SmartGaps.gapW8,
              if (icon != null) icon ?? const Icon(Icons.arrow_forward_ios),
            ],
          ),
        ),
      ),
    );
  }
}

/// Render a fullwidth button with customizable border.
///
/// An [Inkwell] widget inside of a [Material] widget is
/// rendered that can be customized by passing values on
/// the specified parameters.
class CustomBorderButton extends StatelessWidget {
  const CustomBorderButton({
    super.key,
    this.onPressed,
    required this.text,
    this.backgroundColor,
    this.textColor,
    this.textSize,
    this.fontWeight,
    this.padding = const EdgeInsets.all(20),
    this.borderColor,
    this.icon,
  });

  final Function()? onPressed;
  final Color? backgroundColor;
  final Color? textColor;
  final String text;
  final double? textSize;
  final FontWeight? fontWeight;
  final EdgeInsets padding;
  final Color? borderColor;
  final Widget? icon;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: backgroundColor ?? Theme.of(context).colorScheme.secondary,
      child: InkWell(
        onTap: onPressed,
        child: Container(
          padding: padding,
          width: double.infinity,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            border: Border.all(
                color: borderColor ?? Theme.of(context).colorScheme.primary),
            borderRadius: BorderRadius.circular(3),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.fade,
                  style: TextStyle(
                    fontSize: textSize ?? 20,
                    fontWeight: fontWeight ?? FontWeight.bold,
                    color: textColor ?? Colors.white,
                    height: 1,
                  ),
                ),
              ),
              if (icon != null) SmartGaps.gapW8,
              if (icon != null) icon!,
            ],
          ),
        ),
      ),
    );
  }
}

/// A widget with a customizable title that can expands or collapses
/// the widget to reveal or hide the [content].
class ExpansionWidget extends StatefulWidget {
  /// Creates a  widget with a customizable title that can expands or collapses
  /// the widget to reveal or hide the [content].
  const ExpansionWidget({
    super.key,
    required this.titleBuilder,
    this.onExpansionWillChange,
    this.onExpansionChanged,
    required this.content,
    this.initiallyExpanded = false,
    this.maintainState = false,
    this.expandedAlignment = Alignment.center,
    this.onSaveState,
    this.onRestoreState,
    this.duration = const Duration(milliseconds: 200),
  });

  /// The builder of title.
  ///
  /// Typically a [Button] widget that call [toggleFunction] when pressed.
  final Widget Function(double animationValue, double easeInValue,
      bool isExpanded, Function({bool? animated}) toggleFunction) titleBuilder;

  /// Function to save expansion state
  /// Called when expansion state changed
  final void Function(bool isExpanded)? onSaveState;

  /// function to restore expansion state.
  /// Return null if there is no state to store;
  /// in this case, [initiallyExpanded] will be used
  final bool Function()? onRestoreState;

  /// The length of time of animation
  final Duration duration;

  /// Called when the widget expands or collapses.
  ///
  /// When the widget starts expanding, this function is called with the value
  /// true. When the tile starts collapsing, this function is called with
  /// the value false.
  final void Function(bool)? onExpansionChanged;

  /// Called when the widget will change expanded state.
  ///
  /// When the widget is going to start expanding/collapsing, this function is
  /// called with the value true/false.
  ///
  /// Return false to prevent expanded state to change.
  /// Return true(default) to allow expanded state changing.
  final bool Function(bool)? onExpansionWillChange;

  /// The widget that are displayed when the expansionWidget expands.
  final Widget content;

  /// Specifies if the expansionWidget is initially expanded (true) or collapsed (false, the default).
  final bool initiallyExpanded;

  /// Specifies whether the state of the content is maintained when the expansionWidget expands and collapses.
  ///
  /// When true, the content are kept in the tree while the expansionWidget is collapsed.
  /// When false (default), the content are removed from the tree when the expansionWidget is
  /// collapsed and recreated upon expansion.
  final bool maintainState;

  /// Specifies the alignment of [content], which are arranged in a column when
  /// the expansionWidget is expanded.
  ///
  /// The internals of the expanded expansionWidget make use of a [Column] widget for
  /// [content], and [Align] widget to align the column. The `expandedAlignment`
  /// parameter is passed directly into the [Align].
  ///
  /// Modifying this property controls the alignment of the column within the
  /// expanded expansionWidget.
  final Alignment expandedAlignment;

  @override
  ExpansionWidgetState createState() => ExpansionWidgetState();
}

class ExpansionWidgetState extends State<ExpansionWidget>
    with SingleTickerProviderStateMixin {
  static final Animatable<double> _easeInTween =
      CurveTween(curve: Curves.easeIn);

  late AnimationController _controller;
  late Animation<double> _heightFactor;

  bool _isExpanded = false;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(duration: widget.duration, vsync: this);
    _heightFactor = _controller.drive(_easeInTween);

    _isExpanded = widget.onRestoreState?.call() ?? widget.initiallyExpanded;
    if (_isExpanded) _controller.value = 1.0;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void expand({bool animated = true}) {
    _setExpanded(true, true);
  }

  void collapse({bool animated = true}) {
    _setExpanded(false, true);
  }

  void toggle({bool? animated = true}) {
    _setExpanded(!_isExpanded, animated);
  }

  void _setExpanded(bool isExpanded, bool? animated) {
    if (_isExpanded == isExpanded) {
      return;
    }
    if (!(widget.onExpansionWillChange?.call(isExpanded) ?? true)) {
      return;
    }
    setState(() {
      _isExpanded = isExpanded;
      if (animated!) {
        if (_isExpanded) {
          _controller.forward();
        } else {
          _controller.reverse().then<void>((value) {
            setState(() {
              // Rebuild without widget.children.
            });
          });
        }
      } else {
        if (_isExpanded) {
          _controller.value = 1.0;
        } else {
          _controller.value = 0.0;
        }
      }
      widget.onSaveState?.call(_isExpanded);
    });
    widget.onExpansionChanged?.call(_isExpanded);
  }

  Widget _buildChildren(BuildContext context, Widget? child) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        widget.titleBuilder(
            _controller.value, _heightFactor.value, _isExpanded, toggle),
        ClipRect(
          child: Align(
            alignment: widget.expandedAlignment,
            heightFactor: _heightFactor.value,
            child: child,
          ),
        ),
      ],
    );
  }

  @override
  void didUpdateWidget(ExpansionWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.duration != widget.duration) {
      _controller.duration = widget.duration;
    }
    final expand = widget.onRestoreState?.call();
    if (expand != null && expand != _isExpanded) {
      toggle();
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final expand = widget.onRestoreState?.call();
    if (expand != null && expand != _isExpanded) {
      toggle();
    }
  }

  @override
  Widget build(BuildContext context) {
    final bool closed = !_isExpanded && _controller.isDismissed;
    final bool shouldRemoveChildren = closed && !widget.maintainState;

    final Widget result = Offstage(
        offstage: closed,
        child: TickerMode(
          enabled: !closed,
          child: widget.content,
        ));

    return AnimatedBuilder(
      animation: _controller.view,
      builder: _buildChildren,
      child: shouldRemoveChildren ? null : result,
    );
  }
}

class CustomHtmlEditor extends StatefulWidget {
  const CustomHtmlEditor({super.key, required this.messageController});

  final HtmlEditorController messageController;

  @override
  State<CustomHtmlEditor> createState() => _CustomHtmlEditorState();
}

class _CustomHtmlEditorState extends State<CustomHtmlEditor> {
  @override
  Widget build(BuildContext context) {
    return HtmlEditor(
      controller: widget.messageController,
      htmlEditorOptions: HtmlEditorOptions(
        hint: '${tr('message')}...',
        shouldEnsureVisible: true,
      ),
      htmlToolbarOptions: const HtmlToolbarOptions(
        toolbarPosition: ToolbarPosition.aboveEditor,
        dropdownBoxDecoration: BoxDecoration(),
        defaultToolbarButtons: [
          FontButtons(bold: true, italic: true, underline: true),
          ParagraphButtons(),
          ListButtons()
        ],
      ),
    );
  }
}
