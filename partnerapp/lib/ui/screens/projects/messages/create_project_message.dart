import 'dart:io' as io;
import 'dart:io';

import 'package:Haandvaerker.dk/model/messages/contacts_response_model.dart';
import 'package:Haandvaerker.dk/ui/components/cache_image.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/messages/components.dart';
//import 'package:Haandvaerker.dk/ui/components/show_snackbar.dart';
import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:Haandvaerker.dk/utils/extension/snackbar.dart';
import 'package:Haandvaerker.dk/utils/extensions.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/messages/message_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/project_messages_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_icon/file_icon.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

/// Page for composing messages sent to a partner.
///
/// If the message is a reply to another message, the
/// [partnerId] and the [subject] must be provided.
class CreateProjectMessage extends StatefulWidget {
  const CreateProjectMessage({
    super.key,
    this.threadId,
    this.partnerId,
    this.subject,
    this.message,
    this.customerId,
  });

  final int? threadId;
  final int? partnerId;
  final String? subject;
  final String? message;
  final int? customerId;

  @override
  CreateMessagePageState createState() => CreateMessagePageState();
}

class CreateMessagePageState extends State<CreateProjectMessage> {
  int? selectedPartnerId;

  List<io.File> selectedFiles = [];
  final _formKey = GlobalKey<FormState>();
  final subjectController = TextEditingController();
  late TextEditingController searchController;
  final loadingKey = GlobalKey<NavigatorState>();
  final messageController = HtmlEditorController();

  @override
  void initState() {
    super.initState();
    selectedPartnerId = widget.partnerId;
    subjectController.text = widget.subject ?? '';
    searchController = TextEditingController();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      showLoadingDialog(context, loadingKey);

      final messageV2Vm = context.read<MessageV2ViewModel>();
      await messageV2Vm.getContacts();
      final contacts = messageV2Vm.contacts;
      contacts.add(
        ContactsResponseModel(
          email: 'info@haandvaerker.dk',
          id: 1,
          name: 'Info@haandvaerker.dk',
          isActive: 1,
          isVerified: 1,
          mobile: '',
          package: '',
          profilePicture: '',
          type: '',
          zip: '',
        ),
      );
      messageV2Vm.contacts = contacts;

      if (selectedPartnerId != null) {
        final partner = messageV2Vm.contacts.firstWhere(
            (e) => e.id == widget.customerId,
            orElse: () => ContactsResponseModel.defaults());

        setState(() {
          searchController.text =
              partner.name.isNullEmptyOrFalse ? '' : '${partner.name}';
          selectedPartnerId = partner.id;
        });
      }

      Navigator.of(loadingKey.currentContext!).pop();
    });
  }

  @override
  void dispose() {
    subjectController.dispose();
    searchController.dispose();
    super.dispose();
  }

  void onTapSend() async {
    final projectMessageVm = context.read<ProjectMessagesViewModel>();
    final messageV2Vm = context.read<MessageV2ViewModel>();

    final body = await messageController.getText();

    if (!mounted) return;

    if (body.isNotEmpty) {
      if (!_formKey.currentState!.validate()) {
        return;
      }

      if (selectedPartnerId == null || selectedPartnerId == 0) {
        context.showSnackBar(message: tr('please_select_recipient'));
        return;
      }

      if (selectedFiles.any((f) => f.lengthSync() > 4194304)) {
        context.showSnackBar(
            message: context.tr('each_attachment_should_be_less_4mb'));
        return;
      }

      if (selectedFiles.length > 5) {
        context.showSnackBar(message: tr('limited_to_5_attachments'));
        return;
      }

      if (!mounted) return;

      showLoadingDialog(context, loadingKey);

      bool isInAThread = widget.threadId != null && widget.threadId != 0;

      await tryCatchWrapper(
        context: context,
        function: isInAThread
            ? projectMessageVm.sendProjectMessageThread(
                threadId: widget.threadId,
                body: body,
                contactId: selectedPartnerId,
                title: subjectController.text,
              )
            : projectMessageVm.sendNewProjectMessage(
                body: body,
                contactId: selectedPartnerId,
                title: subjectController.text,
              ),
      ).then((value) async {
        Navigator.of(loadingKey.currentContext!).pop();
        if (value!.success!) {
          await showSuccessAnimationDialog(
            myGlobals.homeScaffoldKey!.currentContext!,
            value.success!,
            value.message,
          ).whenComplete(() async {
            backDrawerRoute();
            if (isInAThread) {
              await tryCatchWrapper(
                  context: myGlobals.homeScaffoldKey!.currentContext,
                  function: messageV2Vm.getMessageThread(
                      messageId: widget.threadId!));
            }
          });
        }
      }).catchError((onError) {
        Navigator.of(loadingKey.currentContext!).pop();
      });
    } else {
      context.showSnackBar(message: tr('message_required'));
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DrawerAppBar(
        actions: [
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: GestureDetector(
              onTap: () async {
                final files =
                    await FilePicker.platform.pickFiles(allowMultiple: true);

                if (files != null) {
                  setState(() {
                    selectedFiles.addAll(
                        files.files.map((element) => File(element.path!)));
                  });
                }
              },
              child: SvgPicture.asset('assets/images/attachment-alt.svg',
                  height: 16.0),
            ),
          ),
          SmartGaps.gapW8,
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: GestureDetector(
              onTap: () => onTapSend(),
              child:
                  SvgPicture.asset('assets/images/send-alt.svg', height: 36.0),
            ),
          ),
          SmartGaps.gapW14,
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.fromLTRB(15, 16, 15, 15),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: double.infinity,
                child: partnersDropdown(),
              ),
              SmartGaps.gapH20,
              TextFormField(
                minLines: 1,
                maxLines: 4,
                controller: subjectController,
                keyboardType: TextInputType.multiline,
                validator: (value) => value!.isEmpty ? tr('required') : null,
                decoration: _customDecoration(labelText: tr('subject')),
              ),
              SmartGaps.gapH20,
              CustomHtmlEditor(
                messageController: messageController,
              ),
              if (selectedFiles.isNotEmpty)
                ...selectedFiles.map(
                  (file) {
                    final fileName = file.path.split('/').last;
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 4.0),
                      child: Card(
                        child: ListTile(
                          leading: FileIcon(fileName, size: 50),
                          title: Text(
                            fileName,
                            overflow: TextOverflow.ellipsis,
                            style: Theme.of(context).textTheme.titleMedium,
                          ),
                          trailing: IconButton(
                            icon: const Icon(ElementIcons.delete,
                                color: Colors.red),
                            onPressed: () {
                              setState(() {
                                selectedFiles.remove(file);
                              });
                            },
                          ),
                        ),
                      ),
                    );
                  },
                )
            ],
          ),
        ),
      ),
    );
  }

  Widget partnersDropdown() {
    return Consumer<MessageV2ViewModel>(
      builder: (context, messageV2Vm, __) {
        if (messageV2Vm.busy) {
          return SizedBox(
            child: Skeletonizer(
              enabled: true,
              child: Column(
                children: [
                  Container(
                      width: double.infinity,
                      height: 40.0,
                      color: Colors.white),
                ],
              ),
            ),
          );
        }

        //return Container();
        return TypeAheadField<ContactsResponseModel>(
          builder: (context, controller, focusNode) {
            return TextField(
              controller: searchController,
              decoration: _customDecoration(labelText: tr('to')),
            );
          },
          suggestionsCallback: (keyword) async => [
            ...messageV2Vm.contacts.where(
                (c) => c.name!.toLowerCase().contains(keyword.toLowerCase()))
          ],
          errorBuilder: (_, __) => Padding(
            padding: const EdgeInsets.all(12),
            child: Text(
              tr('empty'),
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .bodyLarge!
                  .copyWith(color: Theme.of(context).colorScheme.primary),
            ),
          ),
          emptyBuilder: (context) {
            return Padding(
              padding: const EdgeInsets.all(12),
              child: Text(
                tr('empty'),
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .bodyLarge!
                    .copyWith(color: Colors.grey),
              ),
            );
          },
          itemBuilder: (context, partner) {
            return ListTile(
              leading: CircleAvatar(
                radius: 22.0,
                backgroundColor: Theme.of(context).colorScheme.primary,
                child: ClipOval(
                  child:
                      CacheImage(imageUrl: partner.profilePicture, width: 60.0),
                ),
              ),
              subtitle: Text(partner.email!,
                  style: Theme.of(context)
                      .textTheme
                      .bodyLarge!
                      .copyWith(color: Colors.grey)),
              title: Text(partner.name!,
                  style: Theme.of(context)
                      .textTheme
                      .titleSmall!
                      .copyWith(color: Theme.of(context).colorScheme.primary)),
            );
          },
          onSelected: (partner) {
            setState(() {
              searchController.text = '${partner.name}';
              selectedPartnerId = partner.id;
            });
          },
        );
      },
    );
  }

  InputDecoration _customDecoration({String? hintText, String? labelText}) {
    return InputDecoration(
      hintText: hintText ?? '',
      labelText: labelText ?? '',
      contentPadding: const EdgeInsets.only(left: 4),
      enabledBorder: const UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.grey)),
      disabledBorder: const UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.grey)),
      focusedBorder: const UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.grey)),
      errorBorder: const UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.grey)),
    );
  }
}
