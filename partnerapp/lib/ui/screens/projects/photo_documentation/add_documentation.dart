part of 'photo_documentation_screen.dart';

class AddDocumentation extends StatelessWidget {
  const AddDocumentation({super.key, required this.image});

  final File image;

  @override
  Widget build(BuildContext context) {
    ///

    final formKey = context
        .select((PhotoDocumentationViewModel value) => value.addFormKey!);
    final photoDocVm = context.read<PhotoDocumentationViewModel>();

    return GestureDetector(
      onTap: () => photoDocVm.unFocusFields(),
      child: Scaffold(
        appBar: const DrawerAppBar(),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  ///

                  SmartGaps.gapH20,

                  const AddDocumentationTitle(),

                  SmartGaps.gapH20,

                  const AddDocumentationDescription(),

                  SmartGaps.gapH20,

                  AddDocumentationImage(image: image),

                  SmartGaps.gapH20,

                  ///
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class AddDocumentationTitle extends StatelessWidget {
  const AddDocumentationTitle({super.key});

  @override
  Widget build(BuildContext context) {
    ///

    final controller = context
        .select((PhotoDocumentationViewModel value) => value.titleController)!;
    final focus = context
        .select((PhotoDocumentationViewModel value) => value.focusTitle)!;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ///

        Text(
          tr('title'),
          style: context.pttTitleSmall.copyWith(
            color: PartnerAppColors.grey1,
          ),
        ),

        SmartGaps.gapH15,

        DocumentationTextField(
          controller: controller,
          focus: focus,
          hintText: tr('enter_title'),
          textValidator: tr('title_validator'),
        )

        ///
      ],
    );
  }
}

class AddDocumentationDescription extends StatelessWidget {
  const AddDocumentationDescription({super.key});

  @override
  Widget build(BuildContext context) {
    ///

    final controller = context.select(
        (PhotoDocumentationViewModel value) => value.descriptionController)!;
    final focus = context
        .select((PhotoDocumentationViewModel value) => value.focusDescription)!;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ///

        Text(
          tr('description'),
          style: context.pttTitleSmall.copyWith(
            color: PartnerAppColors.grey1,
          ),
        ),

        SmartGaps.gapH15,

        DocumentationTextField(
          controller: controller,
          focus: focus,
          minLines: 7,
          maxLines: 7,
          hintText: tr('enter_description'),
          textValidator: tr('description_validator'),
        ),

        ///
      ],
    );
  }
}

class AddDocumentationImage extends StatefulWidget {
  const AddDocumentationImage({super.key, required this.image});

  final File image;

  @override
  State<AddDocumentationImage> createState() => _AddDocumentationImageState();
}

class _AddDocumentationImageState extends State<AddDocumentationImage> {
  late File image;

  @override
  void initState() {
    image = widget.image;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ///

    final photoDocVm = context.read<PhotoDocumentationViewModel>();
    final offerVm = context.read<MyProjectsViewModel>();

    return Column(
      children: [
        ///

        _imageButtons(
          context: context,
          label: tr('replace_image'),
          onTap: () async {
            final newImage =
                await photoDocVm.selectPhoto(context: context, replace: true);

            if (newImage != null) {
              photoDocVm.editIndex = 1;
              setState(() {
                image = newImage;
              });
            }
          },
        ),

        SmartGaps.gapH15,

        _imageButtons(
          context: context,
          label: tr('edit_image'),
          onTap: () async {
            ///

            final editedImage =
                await photoDocVm.editImageView(path: image.path, image: image);

            if (editedImage != null) {
              photoDocVm.editIndex += 1;
              setState(() {
                image = editedImage;
              });
            }

            ///
          },
        ),

        SmartGaps.gapH25,

        Image.file(
          image,
          fit: BoxFit.contain,
        ),

        SmartGaps.gapH25,

        _imageButtons(
          context: context,
          label: tr('save_image'),
          onTap: () async => await photoDocVm.saveImage(
              context: context,
              image: image,
              jobIndustry: offerVm.activeProject.jobIndustry ?? 0),
        ),

        ///
      ],
    );
  }

  _imageButtons({
    required BuildContext context,
    required String label,
    required VoidCallback onTap,
  }) {
    return CustomDesignTheme.flatButtonStyle(
      onPressed: onTap,
      height: 50,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
        side: const BorderSide(color: PartnerAppColors.grey1),
      ),
      child: Center(
        child: Text(
          label,
          style: context.pttTitleMedium.copyWith(
            color: PartnerAppColors.grey1,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
