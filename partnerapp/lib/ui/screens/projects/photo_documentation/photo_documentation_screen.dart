library photo_doc;

import 'dart:io';

import 'package:Haandvaerker.dk/model/file/file_data_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/cache_image.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/extension/colors.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/photo_documentation_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

part 'add_documentation.dart';
part 'components/add_photo.dart';
part 'components/documentation_photo.dart';
part 'components/documentation_text_field.dart';
part 'view_documentation.dart';

class PhotoDocumentationScreen extends StatefulWidget {
  const PhotoDocumentationScreen({super.key, required this.projectId});

  final int projectId;

  @override
  State<PhotoDocumentationScreen> createState() =>
      _PhotoDocumentationScreenState();
}

class _PhotoDocumentationScreenState extends State<PhotoDocumentationScreen> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) async => await tryCatchWrapper(
          context: myGlobals.homeScaffoldKey!.currentContext!,
          function: context
              .read<PhotoDocumentationViewModel>()
              .getDocumentationPhotos(projectId: widget.projectId)),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ///

    final myProjectsVm = context.watch<MyProjectsViewModel>();
    final docPhotos = context
        .select((PhotoDocumentationViewModel value) => value.documentation);
    final isLoading =
        context.select((PhotoDocumentationViewModel value) => value.busy);

    final filteredDocPhotos = docPhotos
        .where((element) =>
            (element.tagUserIds ?? ['']).isEmpty ||
            ((element.tagUserIds ?? ['']).first ==
                myProjectsVm.activeProject.jobIndustry.toString()))
        .toList();

    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Theme(
        data: Theme.of(context).copyWith(
          colorScheme: context.colorScheme.copyWith(
            secondary: PartnerAppColors.accentBlue.withValues(alpha: 0.3),
          ),
        ),
        child: Skeletonizer(
          enabled: isLoading,
          child: GridView.builder(
            physics: const ClampingScrollPhysics(),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, childAspectRatio: 1.05),
            itemCount: isLoading ? 10 : filteredDocPhotos.length + 1,
            itemBuilder: (context, index) {
              if (index == 0) {
                return const Skeleton.shade(
                  child: Padding(
                    padding: EdgeInsets.only(left: 20, bottom: 15, right: 7.5),
                    child: AddPhoto(),
                  ),
                );
              }
              return DocumentationPhoto(
                image: isLoading
                    ? FileData(downloadUrl: '')
                    : filteredDocPhotos[index - 1],
                padding: (index - 1) % 2 != 0
                    ? const EdgeInsets.only(left: 20, bottom: 15, right: 7.5)
                    : const EdgeInsets.only(left: 7.5, bottom: 15, right: 20),
              );
            },
          ),
        ),
      ),
    );

    ///
  }
}
