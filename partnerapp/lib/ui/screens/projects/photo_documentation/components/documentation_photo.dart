part of '../photo_documentation_screen.dart';

class DocumentationPhoto extends StatelessWidget {
  const DocumentationPhoto({
    super.key,
    required this.image,
    required this.padding,
  });

  final FileData image;
  final EdgeInsets padding;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final photoDocVm = context.read<PhotoDocumentationViewModel>();
        if (photoDocVm.isEditing) {
          photoDocVm.isEditing = false;
        }
        photoDocVm.currentPhoto = image;
        changeDrawerRoute(Routes.viewDocumentation);
      },
      child: Padding(
        padding: padding,
        child: CacheImage(imageUrl: image.downloadUrl),
      ),
    );
  }
}
