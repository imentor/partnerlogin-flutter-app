part of '../photo_documentation_screen.dart';

class DocumentationTextField extends StatelessWidget {
  const DocumentationTextField({
    super.key,
    required this.controller,
    required this.focus,
    this.minLines = 1,
    this.maxLines = 1,
    this.labelText = '',
    this.hintText = '',
    this.textValidator = '',
  });

  final TextEditingController controller;
  final FocusNode focus;
  final int minLines;
  final int maxLines;
  final String labelText;
  final String hintText;
  final String textValidator;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: TextFormField(
        controller: controller,
        focusNode: focus,
        style: context.pttBodySmall.copyWith(
          color: Colors.black,
          fontSize: 14,
        ),
        validator: (value) {
          if (value!.isEmpty) {
            return textValidator;
          }
          return null;
        },
        minLines: minLines,
        maxLines: maxLines,
        decoration: InputDecoration(
          labelText: labelText,
          hintText: hintText,
          hintStyle: context.pttBodyLarge.copyWith(
            color: const Color(0xffCECECE),
            fontWeight: FontWeight.w400,
          ),
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: const BorderSide(
              color: PartnerAppColors.grey,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: const BorderSide(
              color: PartnerAppColors.grey,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: const BorderSide(
              color: PartnerAppColors.grey,
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: const BorderSide(
              color: PartnerAppColors.red,
            ),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: const BorderSide(
              color: PartnerAppColors.red,
            ),
          ),
        ),
      ),
    );
  }
}
