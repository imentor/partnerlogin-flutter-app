part of '../photo_documentation_screen.dart';

class AddPhoto extends StatelessWidget {
  const AddPhoto({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async => context
          .read<PhotoDocumentationViewModel>()
          .selectPhoto(context: context),
      child: Container(
        decoration: BoxDecoration(
            color: const Color(0xff19A3C0).withValues(alpha: 0.2)),
        child: Center(
          child: Container(
            width: 35,
            height: 35,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(color: const Color(0xff19A3C0)),
            ),
            child: const Icon(
              Icons.add,
              color: Color(0xff19A3C0),
            ),
          ),
        ),
      ),
    );
  }
}

Future<String?> showSelectImageSource({required BuildContext context}) async {
  final source = await showDialog(
    context: context,
    builder: (context) => const SelectImageSourceDialog(),
  );

  return source as String?;
}

class SelectImageSourceDialog extends StatelessWidget {
  const SelectImageSourceDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        backgroundColor: Colors.white,
        titlePadding: EdgeInsets.zero,
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
        content: SingleChildScrollView(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ///

              CustomDesignTheme.flatButtonStyle(
                onPressed: () => Navigator.pop(context, 'camera'),
                height: 70,
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: Column(
                  children: [
                    const Icon(
                      Icons.camera,
                      size: 50,
                    ),
                    Text(
                      'Camera',
                      style: context.pttTitleSmall.copyWith(
                        color: const Color(0xff707070),
                      ),
                    ),
                  ],
                ),
              ),

              SmartGaps.gapW20,

              CustomDesignTheme.flatButtonStyle(
                onPressed: () => Navigator.pop(context, 'gallery'),
                height: 70,
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: Column(
                  children: [
                    const Icon(
                      Icons.photo_library,
                      size: 50,
                    ),
                    Text(
                      'Gallery',
                      style: context.pttTitleSmall.copyWith(
                        color: const Color(0xff707070),
                      ),
                    ),
                  ],
                ),
              ),

              ///
            ],
          ),
        ));
  }
}
