part of 'photo_documentation_screen.dart';

class ViewDocumentation extends StatelessWidget {
  const ViewDocumentation({super.key});

  @override
  Widget build(BuildContext context) {
    ///

    final isEditing =
        context.select((PhotoDocumentationViewModel value) => value.isEditing);
    final downloadUrl = context.select(
        (PhotoDocumentationViewModel value) => value.currentPhoto!.downloadUrl);
    final formKey = context
        .select((PhotoDocumentationViewModel value) => value.editFormKey!);
    final photoDocVm = context.read<PhotoDocumentationViewModel>();

    return GestureDetector(
      onTap: () => photoDocVm.unFocusFields(),
      child: Scaffold(
        appBar: const DrawerAppBar(),
        body: Theme(
          data: Theme.of(context).copyWith(
            colorScheme: context.colorScheme.copyWith(
              secondary: PartnerAppColors.accentBlue.withValues(alpha: 0.3),
            ),
          ),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Form(
                key: formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ///
                    SmartGaps.gapH10,

                    const ViewDocumentationHeader(),

                    SmartGaps.gapH25,

                    CacheImage(
                      imageUrl: downloadUrl!,
                      fit: BoxFit.cover,
                      height: 350,
                    ),

                    SmartGaps.gapH25,

                    const ViewDocumentationDescription(),

                    if (isEditing) const ViewDocumentationButtons(),

                    ///
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ViewDocumentationHeader extends StatelessWidget {
  const ViewDocumentationHeader({super.key});

  @override
  Widget build(BuildContext context) {
    ///

    final isEditing =
        context.select((PhotoDocumentationViewModel value) => value.isEditing);
    final controller = context
        .select((PhotoDocumentationViewModel value) => value.titleController)!;
    final options =
        context.select((PhotoDocumentationViewModel value) => value.options);
    final title = context.select(
        (PhotoDocumentationViewModel value) => value.currentPhoto!.title)!;
    final focus = context
        .select((PhotoDocumentationViewModel value) => value.focusTitle)!;

    if (isEditing) {
      return DocumentationTextField(
        controller: controller..text = title,
        focus: focus,
        maxLines: 2,
        labelText: tr('title'),
        textValidator: tr('title_validator'),
      );
    }
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ///

        Expanded(
          child: Text(
            title,
            style: context.pttTitleSmall.copyWith(
              color: const Color(0xff707070),
            ),
          ),
        ),

        SmartGaps.gapW10,

        DropdownButtonHideUnderline(
          child: DropdownButton2<String>(
            isDense: false,
            customButton: const Icon(
              Icons.more_horiz_rounded,
              size: 35,
              color: PartnerAppColors.grey1,
            ),
            value: 'edit',
            items: options
                .map(
                  (e) => DropdownMenuItem(
                    value: e,
                    child: Text(tr(e), style: context.pttTitleSmall),
                  ),
                )
                .toList(),
            onChanged: (val) {
              final photoDocVm = context.read<PhotoDocumentationViewModel>();
              if (val == 'edit') {
                photoDocVm.isEditing = true;
              }
            },
            dropdownStyleData: const DropdownStyleData(
              width: 150,
              offset: Offset(-115, 0),
              padding: EdgeInsets.zero,
            ),
          ),
        ),

        ///
      ],
    );

    ///
  }
}

class ViewDocumentationDescription extends StatelessWidget {
  const ViewDocumentationDescription({super.key});

  @override
  Widget build(BuildContext context) {
    ///

    final isEditing =
        context.select((PhotoDocumentationViewModel value) => value.isEditing);
    final controller = context.select(
        (PhotoDocumentationViewModel value) => value.descriptionController)!;
    final description = context.select((PhotoDocumentationViewModel value) =>
        value.currentPhoto!.description)!;
    final focus = context
        .select((PhotoDocumentationViewModel value) => value.focusDescription)!;

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ///

        Text(
          tr('description'),
          style: context.pttTitleSmall.copyWith(
            color: PartnerAppColors.grey1,
          ),
        ),

        SmartGaps.gapH10,

        if (isEditing)
          DocumentationTextField(
            controller: controller..text = description,
            focus: focus,
            maxLines: 5,
            textValidator: tr('description_validator'),
          )
        else
          Text(
            description,
            style: context.pttBodySmall.copyWith(
              color: Colors.black,
              fontSize: 14,
            ),
          ),

        ///
      ],
    );

    ///
  }
}

class ViewDocumentationButtons extends StatelessWidget {
  const ViewDocumentationButtons({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30, bottom: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ///

          Expanded(
            child: CustomDesignTheme.flatButtonStyle(
              onPressed: () async {
                final result = await showOkCancelAlertDialog(
                  context: context,
                  title: tr('cancel_editing'),
                  message: tr('edit_cancel_warning_message'),
                  cancelLabel: tr('keep_editing'),
                  okLabel: tr('cancel_editing'),
                );
                if (context.mounted) {
                  if (result == OkCancelResult.ok) {
                    context.read<PhotoDocumentationViewModel>().isEditing =
                        false;
                  }
                }
              },
              backgroundColor: PartnerAppColors.red,
              height: 50,
              child: Text(
                tr('cancel'),
                style: context.pttBodySmall.copyWith(
                  color: Colors.white,
                  fontSize: 14,
                ),
              ),
            ),
          ),

          SmartGaps.gapW20,

          Expanded(
            child: CustomDesignTheme.flatButtonStyle(
              onPressed: () async {
                final result = await showOkCancelAlertDialog(
                  context: context,
                  title: tr('save_edit'),
                  message: tr('edit_save_message'),
                  cancelLabel: tr('cancel'),
                  okLabel: tr('save_changes'),
                );
                if (context.mounted) {
                  if (result == OkCancelResult.ok) {
                    ///UPDATE API HERE
                  }
                }
              },
              backgroundColor: PartnerAppColors.darkBlue,
              height: 50,
              child: Text(
                tr('save'),
                style: context.pttBodySmall.copyWith(
                  color: Colors.white,
                  fontSize: 14,
                ),
              ),
            ),
          ),

          ///
        ],
      ),
    );
  }
}
