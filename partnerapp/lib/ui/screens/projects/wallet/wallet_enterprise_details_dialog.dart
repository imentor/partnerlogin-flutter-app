import 'package:Haandvaerker.dk/model/payment/payment_stages_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';

Future<void> walletEnterpriseDetailsDialog({
  required BuildContext context,
  required PaymentStages paymentStage,
}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: const EdgeInsets.all(20),
          title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            InkWell(
              onTap: () {
                Navigator.of(dialogContext).pop();
              },
              child: const Icon(
                FeatherIcons.x,
                color: PartnerAppColors.spanishGrey,
              ),
            )
          ]),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: WalletEnterpriseDetailsDialog(
                paymentStage: paymentStage,
              ),
            ),
          ),
        );
      });
}

class WalletEnterpriseDetailsDialog extends StatelessWidget {
  const WalletEnterpriseDetailsDialog({
    super.key,
    required this.paymentStage,
  });

  final PaymentStages paymentStage;

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) {
        double totalVatPrice = paymentStage.contract?.vatPrice ?? 0;

        List<Stage> extraWork = [
          ...(paymentStage.stages ?? []).where((value) =>
              value.contractType == 'extra_work' && value.type == 'normal')
        ];

        totalVatPrice +=
            extraWork.fold(0, (sum, stage) => sum + (stage.priceVat ?? 0));

        return Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  tr('contract_sum'),
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium!
                      .copyWith(fontWeight: FontWeight.normal),
                ),
                Flexible(
                  child: Text(
                    Formatter.curencyFormat(
                        amount: paymentStage.contract?.vatPrice ?? 0),
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        fontWeight: FontWeight.normal,
                        color: PartnerAppColors.spanishGrey),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            const Divider(
              color: PartnerAppColors.darkBlue,
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Total',
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium!
                      .copyWith(fontWeight: FontWeight.normal),
                ),
                Flexible(
                  child: Text(
                    Formatter.curencyFormat(amount: totalVatPrice),
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium!
                        .copyWith(fontWeight: FontWeight.bold),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            ...extraWork.map((stage) {
              return Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          Formatter.formatDateStrings(
                              type: DateFormatType.fullMonthDate,
                              dateString: stage.createdAt),
                          style: Theme.of(context)
                              .textTheme
                              .titleSmall!
                              .copyWith(color: PartnerAppColors.spanishGrey),
                        ),
                        Flexible(
                          child: Text(
                            '+ ${Formatter.curencyFormat(amount: stage.priceVat ?? 0)}',
                            style: Theme.of(context)
                                .textTheme
                                .titleSmall!
                                .copyWith(color: PartnerAppColors.malachite),
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      (stage.description ?? ''),
                      style: Theme.of(context).textTheme.titleSmall,
                    ),
                  ],
                ),
              );
            })
          ],
        );
      },
    );
  }
}
