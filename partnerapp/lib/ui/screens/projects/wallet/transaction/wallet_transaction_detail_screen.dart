import 'package:Haandvaerker.dk/model/payment/payment_stages_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/component/status_chips.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/transaction/payment_stage_transaction_detail.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/wallet/wallet_viewmodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';

class WalletTransactionDetailScreen extends StatefulWidget {
  const WalletTransactionDetailScreen({
    super.key,
    required this.title,
  });

  final String title;

  @override
  WalletTransactionDetailScreenState createState() =>
      WalletTransactionDetailScreenState();
}

class WalletTransactionDetailScreenState
    extends State<WalletTransactionDetailScreen> {
  PaymentTransaction? transaction;

  @override
  Widget build(BuildContext context) {
    return Consumer<WalletViewmodel>(builder: (_, walletVm, __) {
      return Scaffold(
        appBar: DrawerAppBar(
          overrideBackFunction: () {
            if (transaction != null) {
              setState(() {
                transaction = null;
              });
            } else {
              backDrawerRoute();
            }
          },
        ),
        bottomNavigationBar: Container(
          padding:
              const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
          color: Colors.white,
          child: buttonNav(walletVm),
        ),
        body: SingleChildScrollView(
            padding: const EdgeInsets.all(20),
            child: transaction == null
                ? stageDetail(walletVm)
                : PaymentStageTransactionDetail(
                    transaction: transaction!, walletVm: walletVm)),
      );
    });
  }

  Widget paymentTransactionDetail(WalletViewmodel walletVm) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
                flex: 2,
                child: Text(
                  transaction!.amount != null
                      ? transaction!.contact?.name ?? ''
                      : transaction!.company?.company ?? '',
                  style: Theme.of(context).textTheme.titleLarge,
                )),
            Flexible(
              flex: 1,
              child: Text(
                transaction!.amount != null
                    ? Formatter.curencyFormat(
                        amount: (transaction!.amount ?? '').isEmpty
                            ? 0
                            : num.parse(transaction!.amount!))
                    : Formatter.curencyFormat(
                        amount: double.parse(
                            (transaction!.requestedAmount ?? '0'))),
                style: Theme.of(context)
                    .textTheme
                    .titleMedium!
                    .copyWith(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              Formatter.formatDateStrings(
                  type: DateFormatType.transactionDetailDateTime,
                  dateString: transaction?.transactionDate),
              style: Theme.of(context).textTheme.titleSmall!.copyWith(
                    color: PartnerAppColors.spanishGrey,
                  ),
            ),
            PaymentTransactionStatusChip(
              paymentTransaction: transaction!,
              stage: walletVm.currentViewedStage!,
            ),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        Text(
          transaction!.amount != null
              ? transaction!.homeownersComment ?? ''
              : transaction!.partnersComment ?? '',
          style: Theme.of(context).textTheme.titleMedium!.copyWith(
                color: PartnerAppColors.spanishGrey,
              ),
        ),
      ],
    );
  }

  Widget stageDetail(WalletViewmodel walletVm) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          decoration: BoxDecoration(
              border: Border.all(
                color: PartnerAppColors.darkBlue.withValues(alpha: .4),
              ),
              borderRadius: BorderRadius.circular(5)),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 20, bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Text(
                        tr('project_price'),
                        style:
                            Theme.of(context).textTheme.titleMedium!.copyWith(
                                  color: PartnerAppColors.spanishGrey,
                                ),
                      ),
                    ),
                    Flexible(
                      child: Text(
                        Formatter.curencyFormat(
                            amount:
                                walletVm.currentViewedStage?.priceDeducted ??
                                    0),
                        style:
                            Theme.of(context).textTheme.titleMedium!.copyWith(
                                  color: PartnerAppColors.spanishGrey,
                                ),
                      ),
                    ),
                  ],
                ),
              ),
              const Divider(
                color: PartnerAppColors.darkBlue,
              ),
              const SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 10, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Text(
                            widget.title,
                            style: Theme.of(context)
                                .textTheme
                                .titleLarge!
                                .copyWith(),
                          ),
                        ),
                        Flexible(
                          child: Text(
                            Formatter.curencyFormat(
                                amount:
                                    walletVm.currentViewedStage?.balance ?? 0),
                            style: Theme.of(context)
                                .textTheme
                                .titleLarge!
                                .copyWith(),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    if (walletVm.paymentStageType != 'normal') ...[
                      Text(
                        walletVm.currentViewedStage?.description ?? '',
                        style: Theme.of(context)
                            .textTheme
                            .titleSmall!
                            .copyWith(color: PartnerAppColors.spanishGrey),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TransactionStatusChip(
                          stage: walletVm.currentViewedStage!,
                        ),
                        if (walletVm.paymentStageType == 'normal')
                          Text(
                            '${tr('project')} balance',
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium!
                                .copyWith(
                                  color: PartnerAppColors.spanishGrey,
                                ),
                          )
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        paymentTransactions(walletVm),
      ],
    );
  }

  Widget paymentTransactions(WalletViewmodel walletVm) {
    return ListView.builder(
      itemCount:
          (walletVm.currentViewedStage!.paymentTransactions?.length ?? 0),
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        BorderRadiusGeometry? borderRadius;

        final isFirst = index == 0;
        final isLast = index ==
            (walletVm.currentViewedStage!.paymentTransactions?.length ?? 0) - 1;

        final transactionStage =
            walletVm.currentViewedStage!.paymentTransactions!.toList()[index];

        if (isFirst && isLast) {
          borderRadius = BorderRadius.circular(5);
        } else if (isFirst) {
          borderRadius = const BorderRadius.only(
              topLeft: Radius.circular(5), topRight: Radius.circular(5));
        } else if (isLast) {
          borderRadius = const BorderRadius.only(
              bottomLeft: Radius.circular(5), bottomRight: Radius.circular(5));
        }

        return Container(
          decoration: BoxDecoration(
              border: Border.all(
                color: PartnerAppColors.darkBlue.withValues(alpha: .4),
              ),
              borderRadius: borderRadius),
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                      flex: 2,
                      child: Text(
                        transactionStage.amount != null
                            ? transactionStage.contact?.name ?? ''
                            : transactionStage.company?.company ?? '',
                        style: Theme.of(context).textTheme.titleLarge,
                      )),
                  Flexible(
                    flex: 1,
                    child: Text(
                      transactionStage.amount != null
                          ? Formatter.curencyFormat(
                              amount: (transactionStage.amount ?? '').isEmpty
                                  ? 0
                                  : num.parse(transactionStage.amount!))
                          : Formatter.curencyFormat(
                              amount: double.parse(
                                  (transactionStage.requestedAmount ?? '0'))),
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium!
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 3,
                    child: Text(
                      Formatter.formatDateStrings(
                          type: DateFormatType.transactionDetailDateTime,
                          dateString: transactionStage.transactionDate),
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            color: PartnerAppColors.spanishGrey,
                          ),
                    ),
                  ),
                  Flexible(
                    flex: 2,
                    child: PaymentTransactionStatusChip(
                      paymentTransaction: transactionStage,
                      stage: walletVm.currentViewedStage!,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    transaction = transactionStage;
                  });
                },
                child: Text(
                  'Se oversigt',
                  style: Theme.of(context)
                      .textTheme
                      .titleSmall!
                      .copyWith(color: PartnerAppColors.blue),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  Widget fileCards({required String name, required String url}) {
    Widget showFile = const SizedBox.shrink();

    String extension = name.split('.').last;

    switch (extension) {
      case 'png':
      case 'jpg':
      case 'jpeg':
      case 'heic':
      case 'heif':
        showFile = CachedNetworkImage(
          imageUrl: url,
          height: 150,
          width: 150,
          fit: BoxFit.cover,
        );
        break;

      default:
        showFile = const SizedBox(
          height: 150,
          width: 150,
          child: Center(
            child: Icon(
              FeatherIcons.fileText,
              color: PartnerAppColors.blue,
              size: 100,
            ),
          ),
        );
    }

    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: [
          Expanded(
            child: Card(
              margin: EdgeInsets.zero,
              child: showFile,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            name,
            style: Theme.of(context).textTheme.titleMedium,
          )
        ],
      ),
    );
  }

  Widget buttonNav(WalletViewmodel walletVm) {
    switch (walletVm.currentViewedStage?.status) {
      case 'paid':
      case 'partially_paid':
        final deficiencyStage = (walletVm.stagesList['paymentStage']
                    ['${walletVm.currentViewedStage!.contractId}']['']
                as List<Stage>)
            .first;

        if (deficiencyStage.status == 'pending' && deficiencyStage.id != null) {
          return TextButton(
            style: TextButton.styleFrom(
                fixedSize: Size(MediaQuery.of(context).size.width, 50),
                backgroundColor: PartnerAppColors.blue),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  width: 10,
                ),
                Text(
                  tr('request_security_amount'),
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        color: Colors.white,
                      ),
                ),
                const Icon(
                  FeatherIcons.arrowRight,
                  color: Colors.white,
                ),
              ],
            ),
            onPressed: () {
              backDrawerRoute();

              walletVm.currentViewedStage = deficiencyStage;

              changeDrawerRoute(Routes.projectWalletTransactionRequest,
                  arguments: tr('security_amount'));
            },
          );
        } else {
          return const SizedBox.shrink();
        }

      case 'requested':
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              tr('payment_is_being_processed'),
              style: Theme.of(context)
                  .textTheme
                  .titleMedium!
                  .copyWith(color: PartnerAppColors.spanishGrey),
            ),
            const SizedBox(
              height: 10,
            ),
            TextButton(
              style: TextButton.styleFrom(
                  fixedSize: Size(MediaQuery.of(context).size.width, 50),
                  backgroundColor: Colors.white,
                  shape: const RoundedRectangleBorder(
                      side: BorderSide(color: PartnerAppColors.blue))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Icon(
                    FeatherIcons.arrowLeft,
                    color: PartnerAppColors.blue,
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Text(
                    tr('back_to_account_overview'),
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium!
                        .copyWith(color: PartnerAppColors.blue),
                  ),
                ],
              ),
              onPressed: () {
                backDrawerRoute();
              },
            )
          ],
        );

      case 'pending':
        if (walletVm.paymentStageType != 'normal') {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextButton(
                style: TextButton.styleFrom(
                  fixedSize: Size(MediaQuery.of(context).size.width, 50),
                  backgroundColor: PartnerAppColors.malachite,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Foretag betaling',
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(color: Colors.white),
                    ),
                  ],
                ),
                onPressed: () {
                  changeDrawerRoute(Routes.projectWalletTransactionRequest,
                      arguments: widget.title);
                },
              ),
              TextButton(
                  onPressed: () {
                    changeDrawerRoute(Routes.projectWalletTransactionDecline,
                        arguments: walletVm.currentViewedStage);
                  },
                  child: Text(
                    'Afvis',
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        color: PartnerAppColors.red,
                        fontWeight: FontWeight.normal),
                  ))
            ],
          );
        } else {
          return const SizedBox.shrink();
        }
      default:
        return const SizedBox.shrink();
    }
  }
}
