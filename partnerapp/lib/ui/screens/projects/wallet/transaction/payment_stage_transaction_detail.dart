import 'package:Haandvaerker.dk/model/payment/payment_stages_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/component/status_chips.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/wallet/wallet_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class PaymentStageTransactionDetail extends StatefulWidget {
  const PaymentStageTransactionDetail({
    super.key,
    required this.transaction,
    required this.walletVm,
  });

  final PaymentTransaction transaction;
  final WalletViewmodel walletVm;

  @override
  State<PaymentStageTransactionDetail> createState() =>
      _PaymentStageTransactionDetailState();
}

class _PaymentStageTransactionDetailState
    extends State<PaymentStageTransactionDetail> {
  bool showImages = true;

  Uint8List? _mergedPdfBytes;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await widget.walletVm
          .mergeUrlPdfs((widget.transaction.uploadedFiles ?? []))
          .then((file) {
        setState(() {
          _mergedPdfBytes = file;
        });
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      String title = '';
      String amount = '';
      String comment = '';

      if (widget.transaction.amount != null) {
        title = widget.transaction.contact?.name ?? '';
        amount = Formatter.curencyFormat(
            amount: (widget.transaction.amount ?? '').isEmpty
                ? 0
                : num.parse(widget.transaction.amount!));
        comment = widget.transaction.homeownersComment ?? '';
      } else {
        title = widget.transaction.company?.company ?? '';
        amount = Formatter.curencyFormat(
            amount: double.parse((widget.transaction.requestedAmount ?? '0')));
        comment = widget.transaction.partnersComment ?? '';
      }

      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                  flex: 2,
                  child: Text(
                    title,
                    style: Theme.of(context).textTheme.titleLarge,
                  )),
              Flexible(
                flex: 1,
                child: Text(
                  amount,
                  style: Theme.of(context)
                      .textTheme
                      .titleMedium!
                      .copyWith(fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                Formatter.formatDateStrings(
                    type: DateFormatType.transactionDetailDateTime,
                    dateString: widget.transaction.transactionDate),
                style: Theme.of(context).textTheme.titleSmall!.copyWith(
                      color: PartnerAppColors.spanishGrey,
                    ),
              ),
              PaymentTransactionStatusChip(
                paymentTransaction: widget.transaction,
                stage: widget.walletVm.currentViewedStage!,
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Text(
            comment,
            style: Theme.of(context).textTheme.titleMedium!.copyWith(
                  color: PartnerAppColors.spanishGrey,
                ),
          ),
          const SizedBox(
            height: 20,
          ),
          showFiles(),
        ],
      );
    });
  }

  Widget showFiles() {
    List<Map<String, dynamic>> images =
        (widget.transaction.uploadedFiles ?? []).where((value) {
      final name = ((value)['NAME'] as String);

      return name.contains('png') ||
          name.contains('jpg') ||
          name.contains('jpeg') ||
          name.contains('heic') ||
          name.contains('heif');
    }).toList();

    List<Map<String, dynamic>> pdfs =
        (widget.transaction.uploadedFiles ?? []).where((value) {
      final name = ((value)['NAME'] as String);

      return name.contains('pdf');
    }).toList();

    List<Map<String, dynamic>> otherFiles =
        (widget.transaction.uploadedFiles ?? []).where((value) {
      final name = ((value)['NAME'] as String);

      return !name.contains('pdf') &&
          !name.contains('png') &&
          !name.contains('jpg') &&
          !name.contains('jpeg') &&
          !name.contains('heic') &&
          !name.contains('heif');
    }).toList();

    return Column(
      children: [
        Row(
          children: [
            InkWell(
              onTap: () => setState(() {
                showImages = true;
              }),
              child: Text(
                tr('images'),
                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                    color: showImages
                        ? PartnerAppColors.blue
                        : PartnerAppColors.spanishGrey),
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            InkWell(
              onTap: () => setState(() {
                showImages = false;
              }),
              child: Text(
                tr('files'),
                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                    color: showImages
                        ? PartnerAppColors.spanishGrey
                        : PartnerAppColors.blue),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        if (showImages)
          transactionImages(images)
        else
          transactionsFiles(otherFiles: otherFiles, pdfs: pdfs)
      ],
    );
  }

  Widget transactionImages(List<Map<String, dynamic>> images) {
    if (images.isEmpty) {
      return Padding(
        padding: const EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              tr('empty'),
              style: Theme.of(context).textTheme.titleMedium,
            )
          ],
        ),
      );
    }

    return Container(
      height: MediaQuery.of(context).size.height / 2.3,
      width: double.infinity,
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: PartnerAppColors.spanishGrey.withValues(alpha: .3)),
      child: ListView(
        shrinkWrap: true,
        children: [
          ...images.map((value) {
            final file = value;

            return Container(
              height: 200,
              width: double.infinity,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: CachedNetworkImageProvider(
                      file['URL'],
                    ),
                    fit: BoxFit.cover),
              ),
            );
          })
        ],
      ),
    );
  }

  Widget transactionsFiles(
      {required List<Map<String, dynamic>> otherFiles,
      required List<Map<String, dynamic>> pdfs}) {
    if (pdfs.isEmpty && otherFiles.isEmpty) {
      return Padding(
        padding: const EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              tr('empty'),
              style: Theme.of(context).textTheme.titleMedium,
            )
          ],
        ),
      );
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (otherFiles.isNotEmpty) ...[
          SizedBox(
            height: 110,
            child: ListView(
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              children: [
                ...otherFiles.map((value) {
                  final file = value;

                  return InkWell(
                    onTap: () async {
                      final fileName = file['NAME'] as String;
                      final name = fileName.split('.').first;
                      final extension = fileName.split('.').last;

                      final response = await showOkCancelAlertDialog(
                          context: context,
                          title: tr('download_file'),
                          message: file['NAME'],
                          okLabel: 'Download');

                      if (response == OkCancelResult.ok) {
                        modalManager.showLoadingModal();

                        final isSuccessful = await widget.walletVm.downloadFile(
                            name: name, url: file['URL'], extension: extension);

                        if (!mounted) return;

                        modalManager.hideLoadingModal();

                        if ((isSuccessful ?? false)) {
                          showOkAlertDialog(
                              context: context,
                              message: tr('file_downloaded_success'));
                        }
                      }
                    },
                    child: Container(
                      margin: const EdgeInsets.only(right: 5),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 80,
                            width: 80,
                            child: Card(
                              child: Padding(
                                padding: EdgeInsets.all(10),
                                child: Icon(
                                  FeatherIcons.fileText,
                                  color: PartnerAppColors.blue,
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          SizedBox(
                            width: 80,
                            child: Text(
                              file['NAME'],
                              style: Theme.of(context).textTheme.titleSmall,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                })
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
        ],
        Container(
          height: MediaQuery.of(context).size.height / 2.3,
          width: double.infinity,
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: PartnerAppColors.spanishGrey.withValues(alpha: .3)),
          child: _mergedPdfBytes == null
              ? const Center(child: CircularProgressIndicator())
              : SfPdfViewer.memory(_mergedPdfBytes!),
        )
      ],
    );
  }
}
