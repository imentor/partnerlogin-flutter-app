import 'package:Haandvaerker.dk/model/payment/payment_stages_model.dart';
import 'package:Haandvaerker.dk/model/projects/extra_work_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/component/stage_card.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/wallet/wallet_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class WalletTransactionScreen extends StatelessWidget {
  const WalletTransactionScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          gradient: LinearGradient(colors: [
            PartnerAppColors.blue,
            PartnerAppColors.blue.withValues(alpha: .2)
          ], begin: Alignment.bottomRight, end: Alignment.topLeft),
        ),
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: Consumer<WalletViewmodel>(builder: (_, walletVm, __) {
            return Skeletonizer(
              enabled: walletVm.busy,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      backDrawerRoute();
                    },
                    child: RichText(
                      text: TextSpan(children: [
                        const WidgetSpan(
                          alignment: PlaceholderAlignment.middle,
                          child: Icon(
                            FeatherIcons.arrowLeft,
                            color: PartnerAppColors.blue,
                            size: 18,
                          ),
                        ),
                        TextSpan(
                          text: ' ${tr('back')}',
                          style: Theme.of(context)
                              .textTheme
                              .titleSmall!
                              .copyWith(color: PartnerAppColors.blue),
                          children: const [],
                        )
                      ]),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    tr('overview'),
                    style: Theme.of(context)
                        .textTheme
                        .headlineLarge!
                        .copyWith(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Card(
                    elevation: 0,
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: Text(
                                  tr('total_price'),
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(
                                        color: PartnerAppColors.spanishGrey,
                                      ),
                                ),
                              ),
                              Flexible(
                                child: Text(
                                  Formatter.curencyFormat(
                                      amount:
                                          walletVm.stagesList['totalVatPrice']),
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(
                                        color: PartnerAppColors.spanishGrey,
                                      ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Divider(
                            color: PartnerAppColors.darkBlue,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: Text(
                                  '${tr('project')} balance',
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(
                                        color: PartnerAppColors.spanishGrey,
                                      ),
                                ),
                              ),
                              Flexible(
                                child: Text(
                                  Formatter.curencyFormat(
                                      amount: (walletVm
                                              .stagesList['totalVatPrice']) -
                                          (walletVm.currentViewedPaymentStage
                                                  ?.totalAmountApprovedMinusExtraWork ??
                                              0)),
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(
                                        color: PartnerAppColors.spanishGrey,
                                      ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    tr('transaction_summary'),
                    style: Theme.of(context)
                        .textTheme
                        .headlineLarge!
                        .copyWith(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Card(
                    elevation: 0,
                    child: stages(context: context, walletVm: walletVm),
                  ),
                  if (walletVm.paymentStageType == 'normal') ...[
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      tr('extra_work'),
                      style: Theme.of(context)
                          .textTheme
                          .headlineLarge!
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    if ((walletVm.stagesList['extraWork'] as List<Stage>)
                        .isNotEmpty)
                      extraWorkStage(context: context, walletVm: walletVm)
                    // Card(
                    //   elevation: 0,
                    //   child: extraWorkStage(
                    //       context: context, walletVm: walletVm),
                    // ),
                  ]
                ],
              ),
            );
          }),
        ),
      ),
    );
  }

  Widget extraWorkStage({
    required BuildContext context,
    required WalletViewmodel walletVm,
  }) {
    List<Widget> extraWorks = [];

    for (var value in (walletVm.stagesList['extraWork'] as List<Stage>)) {
      extraWorks.add(StageExtraWork(
        stage: value,
      ));
    }

    for (var extraWork
        in (walletVm.stagesList['extraWorkMinus'] as List<ExtraWorkItems>)) {
      extraWorks.add(StageExtraWork(
        extraWork: extraWork,
      ));
    }

    return Column(
      children: [...extraWorks],
    );
  }

  Widget stages({
    required BuildContext context,
    required WalletViewmodel walletVm,
  }) {
    List<Widget> stages = [];

    if ((walletVm.stagesList['paymentStage'] as Map).isEmpty) {
      return Padding(
        padding: const EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              tr('no_payments_yet'),
              style: Theme.of(context).textTheme.titleMedium,
            )
          ],
        ),
      );
    } else {
      for (final i in (walletVm.stagesList['paymentStage'] as Map).entries) {
        for (var industry in (i.value as Map).entries) {
          final jobIndustryIndex =
              (i.value as Map).keys.toList().indexOf(industry.key);

          for (var value in (industry.value as List<Stage>)) {
            final stageIndex = (industry.value as List<Stage>).indexOf(value);

            String title = '';

            if ((industry.key as String).isNotEmpty) {
              title =
                  '${jobIndustryIndex + 1}.${stageIndex + 1} ${industry.key} ${value.type == 'deficiency' ? tr('security_amount') : value.industry ?? ''}';
            } else {
              title = tr('security_amount');
            }

            if (walletVm.paymentStageType == 'normal') {
              stages.add(
                StageCard(walletVm: walletVm, stage: value, title: title),
              );
            } else {
              stages.add(
                ManufacturerSubcontractorStageCard(
                  walletVm: walletVm,
                  stage: value,
                  title: tr('project_price'),
                ),
              );
            }
          }
        }
      }
    }

    return Column(
      children: stages,
    );
  }
}
