import 'package:Haandvaerker.dk/theme.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class TransactionDeclineConfirmation extends StatelessWidget {
  const TransactionDeclineConfirmation({super.key, required this.formPayload});

  final Map<String, dynamic> formPayload;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('provide_reason_for_rejection'),
          style: Theme.of(context)
              .textTheme
              .titleMedium!
              .copyWith(fontWeight: FontWeight.bold),
        ),
        const SizedBox(
          height: 10,
        ),
        const Divider(
          color: PartnerAppColors.darkBlue,
        ),
        Text(
          tr('your_comment'),
          style: Theme.of(context).textTheme.titleMedium,
        ),
        const SizedBox(
          height: 20,
        ),
        Text(
          formPayload['comment'] as String? ?? '',
          style: Theme.of(context).textTheme.titleMedium!.copyWith(
              fontWeight: FontWeight.normal,
              color: PartnerAppColors.spanishGrey),
        ),
      ],
    );
  }
}
