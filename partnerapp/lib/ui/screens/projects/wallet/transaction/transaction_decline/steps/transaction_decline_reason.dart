import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class TransactionDeclineReason extends StatefulWidget {
  const TransactionDeclineReason({super.key});

  @override
  State<TransactionDeclineReason> createState() =>
      _TransactionDeclineReasonState();
}

class _TransactionDeclineReasonState extends State<TransactionDeclineReason> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('provide_reason_for_rejection'),
          style: Theme.of(context)
              .textTheme
              .titleMedium!
              .copyWith(fontWeight: FontWeight.bold),
        ),
        const SizedBox(
          height: 20,
        ),
        CustomTextFieldFormBuilder(
          name: 'comment',
          minLines: 5,
          labelText: tr('add_comment'),
          hintText: tr('insert_comment'),
          validator: FormBuilderValidators.required(errorText: tr('reqquired')),
        ),
      ],
    );
  }
}
