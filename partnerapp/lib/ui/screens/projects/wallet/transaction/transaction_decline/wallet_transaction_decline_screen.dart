import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/component/payment_slider_button.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/transaction/transaction_decline/steps/transaction_decline_confirmation.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/transaction/transaction_decline/steps/transaction_decline_reason.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/wallet/wallet_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';

class WalletTransactionDeclineScreen extends StatefulWidget {
  const WalletTransactionDeclineScreen({super.key});
  @override
  State<WalletTransactionDeclineScreen> createState() =>
      _WalletTransactionDeclineScreenState();
}

class _WalletTransactionDeclineScreenState
    extends State<WalletTransactionDeclineScreen> {
  final formKey = GlobalKey<FormBuilderState>();

  int step = 0;

  Map<String, dynamic> formPayload = {};

  @override
  Widget build(BuildContext context) {
    return Consumer<WalletViewmodel>(builder: (_, walletVm, __) {
      return Scaffold(
        appBar: DrawerAppBar(
          overrideBackFunction: () {
            switch (step + 1) {
              case 1:
                backDrawerRoute();
                break;
              case 2:
              case 3:
                setState(() {
                  step = step - 1;
                });
                break;

              default:
            }
          },
        ),
        bottomNavigationBar: Container(
          padding:
              const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
          color: Colors.white,
          child: buttonNav(walletVm),
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: FormBuilder(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [bodyForm(walletVm)],
            ),
          ),
        ),
      );
    });
  }

  Widget bodyForm(WalletViewmodel walletVm) {
    switch (step + 1) {
      case 1:
        return const TransactionDeclineReason();
      case 2:
        return TransactionDeclineConfirmation(formPayload: formPayload);
      default:
        return const SizedBox.shrink();
    }
  }

  Widget buttonNav(WalletViewmodel walletVm) {
    if ((step + 1) == 2) {
      return PaymentSliderButton(
        label: Text(
          tr('decline'),
          textAlign: TextAlign.center,
          style: Theme.of(context)
              .textTheme
              .titleMedium!
              .copyWith(color: Colors.white, fontSize: 20),
        ),
        action: () async {
          return true;
        },
        showTerms: true,
      );
    }
    return Row(
      children: [
        Expanded(
            child: TextButton(
                style: TextButton.styleFrom(
                    fixedSize: Size(MediaQuery.of(context).size.width, 45),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                        side: const BorderSide(
                            color: PartnerAppColors.darkBlue))),
                child: Text(
                  tr('back'),
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium!
                      .copyWith(color: PartnerAppColors.darkBlue),
                ),
                onPressed: () {
                  switch (step + 1) {
                    case 1:
                      backDrawerRoute();
                      break;
                    case 2:
                      setState(() {
                        step = step - 1;
                      });
                      break;

                    default:
                  }
                })),
        const SizedBox(
          width: 10,
        ),
        Expanded(
          child: TextButton(
            style: TextButton.styleFrom(
                fixedSize: Size(MediaQuery.of(context).size.width, 45),
                backgroundColor: PartnerAppColors.malachite,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                )),
            child: Text(
              tr('next'),
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium!
                  .copyWith(color: Colors.white),
            ),
            onPressed: () async {
              switch (step + 1) {
                case 1:
                  if (formKey.currentState!.validate()) {
                    setState(() {
                      for (final i in formKey.currentState!.fields.entries) {
                        formPayload[i.key] = i.value.value;
                      }

                      step = step + 1;
                    });
                  }
                  break;
                default:
              }
            },
          ),
        )
      ],
    );
  }

  void onDecline(WalletViewmodel walletVm) async {
    modalManager.showLoadingModal();

    await tryCatchWrapper(
            context: context,
            function: walletVm.declinePayout(
                reason: formPayload['comment'] as String))
        .then((value) async {
      if (!mounted) return;
      modalManager.hideLoadingModal();

      await tryCatchWrapper(
          context: context,
          function: walletVm.initWallet(
              projectId: walletVm.currentViewedProject!.id!,
              contractId: walletVm.currentViewedProject!.contractId ?? 0));

      if (!mounted) return;

      if ((value ?? false)) {
        commonSuccessOrFailedDialog(
                context: context,
                successMessage: 'Info',
                message: 'Betalingsanmodningen er blevet afvist.')
            .whenComplete(() async {
          if (!mounted) return;

          backDrawerRoute();
        });
      } else {
        backDrawerRoute();
      }
    });
  }
}
