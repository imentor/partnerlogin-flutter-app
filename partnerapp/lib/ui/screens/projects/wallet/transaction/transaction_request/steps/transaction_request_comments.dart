import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/wallet/wallet_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class TransactionRequestComments extends StatelessWidget {
  const TransactionRequestComments({super.key, required this.walletVm});

  final WalletViewmodel walletVm;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomTextFieldFormBuilder(
          name: 'comment',
          minLines: 10,
          hintText: tr('enter_comment'),
          labelText: tr('add_comment'),
        ),
        const SizedBox(
          height: 20,
        ),
        CustomFilePickerFormBuilder(
          name: 'files',
          labelText: tr('upload_file'),
          description: tr('upload_file_description'),
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
      ],
    );
  }
}
