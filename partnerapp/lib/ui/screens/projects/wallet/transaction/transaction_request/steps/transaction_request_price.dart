import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/component/status_chips.dart';
import 'package:Haandvaerker.dk/utils/debouncer.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/wallet/wallet_viewmodel.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class TransactionRequestPrice extends StatelessWidget {
  TransactionRequestPrice({
    super.key,
    required this.walletVm,
    required this.formKey,
    required this.title,
    required this.currencyFormatter,
  });
  final WalletViewmodel walletVm;
  final GlobalKey<FormBuilderState> formKey;
  final String title;

  final Debouncer debouncer = Debouncer(milliseconds: 500);
  final CurrencyTextInputFormatter currencyFormatter;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('request_payment'),
          style: Theme.of(context)
              .textTheme
              .headlineLarge!
              .copyWith(fontWeight: FontWeight.bold),
        ),
        const SizedBox(
          height: 20,
        ),
        Container(
          decoration: BoxDecoration(
              border: Border.all(
                color: PartnerAppColors.darkBlue.withValues(alpha: .4),
              ),
              borderRadius: BorderRadius.circular(5)),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 20, bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Text(
                        tr('project_price'),
                        style:
                            Theme.of(context).textTheme.titleMedium!.copyWith(
                                  color: PartnerAppColors.spanishGrey,
                                ),
                      ),
                    ),
                    Flexible(
                      child: Text(
                        Formatter.curencyFormat(
                            amount:
                                walletVm.currentViewedStage?.priceDeducted ??
                                    0),
                        style:
                            Theme.of(context).textTheme.titleMedium!.copyWith(
                                  color: PartnerAppColors.spanishGrey,
                                ),
                      ),
                    ),
                  ],
                ),
              ),
              const Divider(
                color: PartnerAppColors.darkBlue,
              ),
              const SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 10, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          flex: 2,
                          child: Text(
                            title,
                            style: Theme.of(context)
                                .textTheme
                                .titleLarge!
                                .copyWith(),
                          ),
                        ),
                        Flexible(
                          flex: 1,
                          child: Text(
                            Formatter.curencyFormat(
                                amount: walletVm
                                        .currentViewedStage!.priceDeducted ??
                                    0),
                            style: Theme.of(context)
                                .textTheme
                                .titleLarge!
                                .copyWith(),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TransactionStatusChip(
                          stage: walletVm.currentViewedStage!,
                          isChip: false,
                        ),
                        Text(
                          '${tr('project')} balance',
                          style:
                              Theme.of(context).textTheme.titleMedium!.copyWith(
                                    color: PartnerAppColors.spanishGrey,
                                  ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('adjust_price'),
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium!
                  .copyWith(fontWeight: FontWeight.normal),
            ),
            const Spacer(),
            Expanded(
              child: CustomTextFieldFormBuilder(
                name: 'paymentField',
                textAlign: TextAlign.right,
                keyboardType: TextInputType.number,
                initialValue: '${walletVm.currentViewedStage!.priceDeducted}',
                inputFormatters: [currencyFormatter],
                validator: FormBuilderValidators.compose([
                  (p0) {
                    if ((p0 ?? '').isNotEmpty) {
                      if (num.parse(
                              Formatter.sanitizeCurrencyFromFormatter(p0!)) >
                          (walletVm.currentViewedStage!.priceDeducted ?? 0)) {
                        return '${tr('price_couldnt_higher_than')} ${walletVm.currentViewedStage!.priceDeducted}';
                      }

                      if (num.parse(
                              Formatter.sanitizeCurrencyFromFormatter(p0)) <=
                          0) {
                        return tr('required');
                      }
                    }
                    return null;
                  },
                  FormBuilderValidators.required(errorText: tr('required'))
                ]),
                suffixIcon: Container(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, bottom: 4, top: 12),
                  child: Text(
                    'kr.',
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        fontSize: 18,
                        fontWeight: FontWeight.normal,
                        color: PartnerAppColors.darkBlue),
                  ),
                ),
                onChanged: (p0) {
                  debouncer.run(() {
                    if ((p0 ?? '').isEmpty) {
                      formKey.currentState!.patchValue({'paymentSlider': 0.0});
                    } else {
                      if (num.parse(
                              Formatter.sanitizeCurrencyFromFormatter(p0!)) >
                          (walletVm.currentViewedStage!.priceDeducted ?? 0.0)) {
                        formKey.currentState!.fields['paymentField']!
                            .validate();
                      } else {
                        formKey.currentState!.fields['paymentField']!
                            .validate();
                        formKey.currentState!.patchValue({
                          'paymentSlider': double.parse(
                              Formatter.sanitizeCurrencyFromFormatter(p0))
                        });
                      }
                    }
                  });
                },
              ),
            )
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        CustomSliderFormBuilder(
          name: 'paymentSlider',
          max: (walletVm.currentViewedStage!.priceDeducted ?? 0),
          initialValue: (walletVm.currentViewedStage!.priceDeducted ?? 0),
          min: 0,
          onChangeEnd: (p0) {
            if (p0 > 0) {
              formKey.currentState!.patchValue({
                'paymentField':
                    Formatter.curencyFormat(amount: p0, withoutSymbol: true)
              });
            } else {
              formKey.currentState!.patchValue({'paymentField': '0'});
            }
          },
        ),
      ],
    );
  }
}
