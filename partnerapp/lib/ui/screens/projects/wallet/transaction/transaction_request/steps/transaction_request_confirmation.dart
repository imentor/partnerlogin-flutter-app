import 'dart:io';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/wallet/wallet_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class TransactionRequestConfirmation extends StatefulWidget {
  const TransactionRequestConfirmation({
    super.key,
    required this.walletVm,
    required this.formPayload,
  });

  final WalletViewmodel walletVm;
  final Map<String, dynamic> formPayload;

  @override
  TransactionRequestConfirmationState createState() =>
      TransactionRequestConfirmationState();
}

class TransactionRequestConfirmationState
    extends State<TransactionRequestConfirmation> {
  bool showImages = true;

  File? mergedPdfs;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await widget.walletVm.mergePdfs([
        ...(widget.formPayload['files'] as List<PlatformFile>)
            .where((value) => value.extension == 'pdf')
      ]).then((value) {
        setState(() {
          mergedPdfs = value;
        });
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      List<PlatformFile> otherFiles = [
        ...(widget.formPayload['files'] as List<PlatformFile>)
            .where((value) => value.extension != 'pdf')
      ];

      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(
                  tr('your_requested_price'),
                  style: Theme.of(context)
                      .textTheme
                      .titleLarge!
                      .copyWith(fontWeight: FontWeight.normal),
                ),
              ),
              Flexible(
                child: Text(
                  Formatter.curencyFormat(
                      amount: num.parse(Formatter.sanitizeCurrencyFromFormatter(
                          widget.formPayload['paymentField']))),
                  style: Theme.of(context).textTheme.titleLarge!.copyWith(),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          const Divider(
            color: PartnerAppColors.darkBlue,
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            tr('add_comment'),
            style: Theme.of(context).textTheme.titleMedium,
          ),
          const SizedBox(
            height: 20,
          ),
          Text(
            widget.formPayload['comment'] ?? '',
            style: Theme.of(context).textTheme.titleMedium!.copyWith(
                fontWeight: FontWeight.normal,
                color: PartnerAppColors.spanishGrey),
          ),
          const SizedBox(
            height: 30,
          ),
          showImagesFiles(otherFiles),
        ],
      );
    });
  }

  Widget showImagesFiles(List<PlatformFile> otherFiles) {
    final images = [
      ...otherFiles.where((value) {
        return value.name.contains('png') ||
            value.name.contains('jpg') ||
            value.name.contains('jpeg') ||
            value.name.contains('heic') ||
            value.name.contains('heif');
      })
    ];

    final files = [
      ...otherFiles.where((value) {
        return !value.name.contains('pdf') &&
            !value.name.contains('png') &&
            !value.name.contains('jpg') &&
            !value.name.contains('jpeg') &&
            !value.name.contains('heic') &&
            !value.name.contains('heif');
      })
    ];

    return Column(
      children: [
        Row(
          children: [
            InkWell(
              onTap: () => setState(() {
                showImages = true;
              }),
              child: Text(
                tr('images'),
                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                    color: showImages
                        ? PartnerAppColors.blue
                        : PartnerAppColors.spanishGrey),
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            InkWell(
              onTap: () => setState(() {
                showImages = false;
              }),
              child: Text(
                tr('files'),
                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                    color: showImages
                        ? PartnerAppColors.spanishGrey
                        : PartnerAppColors.blue),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        if (showImages) selectedImages(images) else selectedFiles(files)
      ],
    );
  }

  Widget selectedFiles(List<PlatformFile> files) {
    if (files.isEmpty && mergedPdfs == null) {
      return Padding(
        padding: const EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              tr('empty'),
              style: Theme.of(context).textTheme.titleMedium,
            )
          ],
        ),
      );
    }

    return Column(
      children: [
        if (files.isNotEmpty) ...[
          SizedBox(
            height: 110,
            child: ListView(
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              children: [
                ...files.map((value) {
                  return Container(
                    margin: const EdgeInsets.only(right: 5),
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 80,
                          width: 80,
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.all(10),
                              child: Icon(
                                FeatherIcons.fileText,
                                color: PartnerAppColors.blue,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        SizedBox(
                          width: 80,
                          child: Text(
                            value.name,
                            style: Theme.of(context).textTheme.titleSmall,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        )
                      ],
                    ),
                  );
                })
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
        ],
        if (mergedPdfs != null)
          Container(
            height: MediaQuery.of(context).size.height / 2.3,
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: PartnerAppColors.spanishGrey.withValues(alpha: .3)),
            child: SfPdfViewer.file(mergedPdfs!),
          ),
      ],
    );
  }

  Widget selectedImages(List<PlatformFile> images) {
    if (images.isEmpty) {
      return Padding(
        padding: const EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              tr('empty'),
              style: Theme.of(context).textTheme.titleMedium,
            )
          ],
        ),
      );
    }

    return Container(
      height: MediaQuery.of(context).size.height / 2.3,
      width: double.infinity,
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: PartnerAppColors.spanishGrey.withValues(alpha: .3)),
      child: ListView(
        shrinkWrap: true,
        children: [
          ...images.map((value) {
            return Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: Image.file(
                File(value.path!),
                height: 200,
                width: double.infinity,
                fit: BoxFit.cover,
              ),
            );
          })
        ],
      ),
    );
  }
}
