import 'package:Haandvaerker.dk/model/payment/payment_stages_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/component/status_chips.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';

class TransactionRequestDeficiency extends StatelessWidget {
  const TransactionRequestDeficiency({
    super.key,
    required this.stage,
  });

  final Stage stage;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 2,
              child: Text(
                '${tr('security_amount')} 15%',
                style: Theme.of(context)
                    .textTheme
                    .titleLarge!
                    .copyWith(fontWeight: FontWeight.bold),
              ),
            ),
            Flexible(
              flex: 1,
              child: Text(
                Formatter.curencyFormat(amount: stage.priceDeducted ?? 0),
                style: Theme.of(context)
                    .textTheme
                    .titleLarge!
                    .copyWith(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TransactionStatusChip(stage: stage),
            Text(
              tr('balance'),
              style: Theme.of(context)
                  .textTheme
                  .titleMedium!
                  .copyWith(color: PartnerAppColors.spanishGrey),
            )
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        const Divider(
          color: PartnerAppColors.spanishGrey,
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          tr('security_amount_description'),
          style: Theme.of(context)
              .textTheme
              .titleMedium!
              .copyWith(color: PartnerAppColors.spanishGrey),
        ),
        const SizedBox(
          height: 10,
        ),
        InkWell(
          onTap: () {
            backDrawerRoute();
            changeDrawerRoute(Routes.projectDeficiency);
          },
          child: RichText(
            text: TextSpan(
              text: 'Start ${tr('deficiency_list')}',
              style: Theme.of(context)
                  .textTheme
                  .titleMedium!
                  .copyWith(color: PartnerAppColors.blue),
              children: const [
                WidgetSpan(
                  alignment: PlaceholderAlignment.middle,
                  child: Icon(
                    FeatherIcons.arrowRight,
                    color: PartnerAppColors.blue,
                    size: 18,
                  ),
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}
