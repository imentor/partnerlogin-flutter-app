import 'dart:developer';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/component/payment_slider_button.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/transaction/transaction_request/steps/transaction_request_comments.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/transaction/transaction_request/steps/transaction_request_confirmation.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/transaction/transaction_request/steps/transaction_request_price.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/transaction/transaction_request/transaction_request_deficiency.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/transaction/transaction_request/transaction_request_manufacturer_subcontractor.dart';
import 'package:Haandvaerker.dk/utils/mitid.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/wallet/wallet_viewmodel.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';

class WalletTransactionRequestScreen extends StatefulWidget {
  const WalletTransactionRequestScreen({
    super.key,
    required this.title,
  });

  final String title;

  @override
  State<WalletTransactionRequestScreen> createState() =>
      _WalletTransactionRequestScreenState();
}

class _WalletTransactionRequestScreenState
    extends State<WalletTransactionRequestScreen> {
  final formKey = GlobalKey<FormBuilderState>();

  int step = 0;

  Map<String, dynamic> formPayload = {};

  final CurrencyTextInputFormatter currencyFormatter =
      CurrencyTextInputFormatter(
          NumberFormat.currency(locale: 'da', decimalDigits: 2, symbol: ''));

  @override
  Widget build(BuildContext context) {
    return Consumer<WalletViewmodel>(builder: (_, walletVm, __) {
      return Scaffold(
        appBar: DrawerAppBar(
          overrideBackFunction: () {
            switch (step + 1) {
              case 1:
                backDrawerRoute();
                break;
              case 2:
              case 3:
                setState(() {
                  step = step - 1;
                });
                break;

              default:
            }
          },
        ),
        bottomNavigationBar: Container(
          padding:
              const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
          color: Colors.white,
          child: buttonNav(walletVm),
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: FormBuilder(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [bodyForm(walletVm)],
            ),
          ),
        ),
      );
    });
  }

  Widget buttonNav(WalletViewmodel walletVm) {
    if (walletVm.paymentStageType != 'normal') {
      return PaymentSliderButton(
        label: Text(
          tr('send_request'),
          textAlign: TextAlign.center,
          style: Theme.of(context)
              .textTheme
              .titleMedium!
              .copyWith(color: Colors.white, fontSize: 20),
        ),
        action: () async {
          onRequest(walletVm);

          return true;
        },
        showTerms: true,
      );
    } else {
      if (walletVm.currentViewedStage?.type == 'deficiency') {
        return PaymentSliderButton(
          label: Text(
            tr('request_release_security_amount'),
            textAlign: TextAlign.center,
            style: Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(color: Colors.white, fontSize: 20),
          ),
          action: () async {
            onRequest(walletVm);

            return true;
          },
        );
      } else {
        if ((step + 1) == 3) {
          return PaymentSliderButton(
              label: Text(
                tr('send_request'),
                style: Theme.of(context)
                    .textTheme
                    .titleLarge!
                    .copyWith(color: Colors.white),
              ),
              action: () async {
                onRequest(walletVm);

                return true;
              });
        } else {
          return Row(
            children: [
              Expanded(
                  child: TextButton(
                      style: TextButton.styleFrom(
                          fixedSize:
                              Size(MediaQuery.of(context).size.width, 45),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                              side: const BorderSide(
                                  color: PartnerAppColors.darkBlue))),
                      child: Text(
                        tr('back'),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(color: PartnerAppColors.darkBlue),
                      ),
                      onPressed: () {
                        switch (step + 1) {
                          case 1:
                            backDrawerRoute();
                            break;
                          case 2:
                            setState(() {
                              step = step - 1;
                            });
                            break;

                          default:
                        }
                      })),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: TextButton(
                  style: TextButton.styleFrom(
                      fixedSize: Size(MediaQuery.of(context).size.width, 45),
                      backgroundColor: PartnerAppColors.malachite,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      )),
                  child: Text(
                    tr('next'),
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium!
                        .copyWith(color: Colors.white),
                  ),
                  onPressed: () async {
                    switch (step + 1) {
                      case 1:
                      case 2:
                        try {
                          if (formKey.currentState!.validate()) {
                            setState(() {
                              for (final i
                                  in formKey.currentState!.fields.entries) {
                                formPayload[i.key] = i.value.value;
                              }

                              step = step + 1;
                            });
                          }
                        } catch (e) {
                          log("message lala $e");
                        }
                        break;

                      default:
                    }
                  },
                ),
              )
            ],
          );
        }
      }
    }
  }

  Widget bodyForm(WalletViewmodel walletVm) {
    if (walletVm.paymentStageType != 'normal') {
      return TransactionRequestManufacturerSubcontractor(
          stage: walletVm.currentViewedStage!);
    } else {
      if (walletVm.currentViewedStage?.type == 'deficiency') {
        return TransactionRequestDeficiency(
          stage: walletVm.currentViewedStage!,
        );
      } else {
        switch (step + 1) {
          case 1:
            return TransactionRequestPrice(
              walletVm: walletVm,
              formKey: formKey,
              title: widget.title,
              currencyFormatter: currencyFormatter,
            );
          case 2:
            return TransactionRequestComments(
              walletVm: walletVm,
            );
          case 3:
            return TransactionRequestConfirmation(
              walletVm: walletVm,
              formPayload: formPayload,
            );
          default:
            return TransactionRequestPrice(
              walletVm: walletVm,
              formKey: formKey,
              title: widget.title,
              currencyFormatter: currencyFormatter,
            );
        }
      }
    }
  }

  void onRequest(WalletViewmodel walletVm) async {
    modalManager.showLoadingModal();

    await tryCatchWrapper(
            context: context,
            function: walletVm.requestPartial(payload: formPayload))
        .then((value) async {
      if (!mounted) return;
      modalManager.hideLoadingModal();

      await tryCatchWrapper(
          context: context,
          function: walletVm.initWallet(
              projectId: walletVm.currentViewedProject!.id!,
              contractId: walletVm.currentViewedProject!.contractId ?? 0));

      if (!mounted) return;

      if ((value ?? false)) {
        commonSuccessOrFailedDialog(
                context: context,
                successMessage: tr('payment_completed'),
                message: tr('payment_completed_description'))
            .whenComplete(() async {
          if (!mounted) return;

          backDrawerRoute();
        });
      } else {
        backDrawerRoute();
      }
    });
  }

  void onForcePartialPayout(WalletViewmodel walletVm) async {
    final mitidResponse = await mitidCallback();

    if (!mounted) return;

    modalManager.showLoadingModal();

    await tryCatchWrapper(
        context: context,
        function: walletVm.forcePartialPayout(
          mitIdToken: mitidResponse['token']['access_token'] as String,
        )).then((value) async {
      if (!mounted) return;
      modalManager.hideLoadingModal();

      await tryCatchWrapper(
          context: context,
          function: walletVm.initWallet(
              projectId: walletVm.currentViewedProject!.id!,
              contractId: walletVm.currentViewedProject!.contractId ?? 0));

      if (!mounted) return;

      if ((value ?? false)) {
        commonSuccessOrFailedDialog(
                context: context,
                successMessage: tr('payment_completed'),
                message: tr('payment_completed_description'))
            .whenComplete(() async {
          if (!mounted) return;

          backDrawerRoute();
        });
      } else {
        backDrawerRoute();
      }
    });
  }
}
