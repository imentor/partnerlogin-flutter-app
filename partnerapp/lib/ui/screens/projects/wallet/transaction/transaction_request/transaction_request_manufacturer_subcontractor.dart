import 'package:Haandvaerker.dk/model/payment/payment_stages_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/component/status_chips.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:flutter/material.dart';

class TransactionRequestManufacturerSubcontractor extends StatelessWidget {
  const TransactionRequestManufacturerSubcontractor({
    super.key,
    required this.stage,
  });

  final Stage stage;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          stage.description ?? '',
          style: Theme.of(context)
              .textTheme
              .titleSmall!
              .copyWith(color: PartnerAppColors.spanishGrey),
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TransactionStatusChip(stage: stage),
            Text(
              Formatter.curencyFormat(amount: stage.priceDeducted ?? 0),
              style: Theme.of(context).textTheme.titleLarge!.copyWith(),
            )
          ],
        )
      ],
    );
  }
}
