import 'package:Haandvaerker.dk/model/projects/extra_work_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/component/stage_card.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/wallet/wallet_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class WalletExtraWorkScreen extends StatelessWidget {
  const WalletExtraWorkScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<WalletViewmodel>(builder: (_, walletVm, __) {
      return Scaffold(
        // floatingActionButton: const ExtraWorkFloatingButton(),
        floatingActionButton: FloatingActionButton.extended(
          backgroundColor: Colors.white,
          icon: const Icon(Icons.add, color: PartnerAppColors.blue),
          onPressed: () {
            changeDrawerRoute(Routes.projectWalletExtraWorkRequest);
          },
          label: Text(
            'Opret aftaleseddel',
            style: Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(color: PartnerAppColors.blue),
          ),
        ),
        body: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              PartnerAppColors.blue,
              PartnerAppColors.blue.withValues(alpha: .2)
            ], begin: Alignment.bottomRight, end: Alignment.topLeft),
          ),
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(20),
            child: Skeletonizer(
              enabled: walletVm.busy,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      backDrawerRoute();
                    },
                    child: RichText(
                      text: TextSpan(children: [
                        const WidgetSpan(
                          alignment: PlaceholderAlignment.middle,
                          child: Icon(
                            FeatherIcons.arrowLeft,
                            color: PartnerAppColors.blue,
                            size: 18,
                          ),
                        ),
                        TextSpan(
                          text: ' ${tr('back')}',
                          style: Theme.of(context)
                              .textTheme
                              .titleSmall!
                              .copyWith(color: PartnerAppColors.blue),
                          children: const [],
                        )
                      ]),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    tr('overview'),
                    style: Theme.of(context)
                        .textTheme
                        .headlineLarge!
                        .copyWith(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Card(
                    elevation: 0,
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                flex: 2,
                                child: Text(
                                  tr('extra_work_pending_approval'),
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(
                                        color: PartnerAppColors.spanishGrey,
                                      ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                child: Text(
                                  Formatter.curencyFormat(
                                      amount: walletVm.extraWork
                                                  ?.totalExtraWorkPending?[
                                              'priceWithVat'] ??
                                          0),
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(
                                        color: PartnerAppColors.spanishGrey,
                                      ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Divider(
                            color: PartnerAppColors.darkBlue,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                flex: 2,
                                child: Text(
                                  tr('total_approved_extra_work'),
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(
                                        color: PartnerAppColors.spanishGrey,
                                      ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                child: Text(
                                  Formatter.curencyFormat(
                                      amount: walletVm.extraWork
                                                  ?.totalExtraWorkApproved?[
                                              'priceWithVat'] ??
                                          0),
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(
                                        color: PartnerAppColors.spanishGrey,
                                      ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Card(
                    elevation: 0,
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                flex: 2,
                                child: Text(
                                  'Annullering af ekstraarbejde venter',
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(
                                        color: PartnerAppColors.spanishGrey,
                                      ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                child: Text(
                                  Formatter.curencyFormat(
                                      amount: walletVm.extraWork
                                              ?.totalPendingMinusExtraWork ??
                                          0,
                                      sign: 'negative'),
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(
                                        color: PartnerAppColors.red,
                                      ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Divider(
                            color: PartnerAppColors.darkBlue,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                flex: 2,
                                child: Text(
                                  'Godkendt beløb efter annullering',
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(
                                        color: PartnerAppColors.spanishGrey,
                                      ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                child: Text(
                                  Formatter.curencyFormat(
                                      amount: walletVm.extraWork
                                              ?.totalApprovedMinusExtraWork ??
                                          0,
                                      sign: 'negative'),
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(
                                        color: PartnerAppColors.red,
                                      ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    tr('extra_work'),
                    style: Theme.of(context)
                        .textTheme
                        .headlineLarge!
                        .copyWith(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  extraWorkItems(context: context, walletVm: walletVm),
                  const SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }

  Widget extraWorkItems(
      {required BuildContext context, required WalletViewmodel walletVm}) {
    if ((walletVm.extraWork?.items?['EXTRA_WORKS'] ?? <ExtraWorkItems>[])
        .isEmpty) {
      return Card(
        elevation: 0,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                tr('no_extra_work_yet'),
                style: Theme.of(context).textTheme.titleMedium,
              )
            ],
          ),
        ),
      );
    } else {
      return Column(
        children: [
          ...((walletVm.extraWork?.items?['EXTRA_WORKS'] ?? <ExtraWorkItems>[]))
              .map((extraWork) {
            return StageExtraWork(
              extraWork: extraWork,
            );
            // final bool isTypeMinus = extraWork.type == 'minus';

            // return InkWell(
            //   onTap: () {

            //   },
            //   child: Container(
            //     padding: const EdgeInsets.all(20),
            //     decoration: BoxDecoration(
            //         border: Border(
            //             bottom: BorderSide(
            //                 color: PartnerAppColors.darkBlue
            //                     .withValues(alpha: .3)))),
            //     child: Column(
            //       crossAxisAlignment: CrossAxisAlignment.start,
            //       children: [
            //         Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [
            //             Text(
            //               Formatter.formatDateStrings(
            //                   type: DateFormatType.fullMonthDate,
            //                   dateString: extraWork.endDate),
            //               style:
            //                   Theme.of(context).textTheme.titleMedium!.copyWith(
            //                         color: PartnerAppColors.spanishGrey,
            //                       ),
            //             ),
            //             Flexible(
            //               // child: Text(
            //               //   Formatter.curencyFormat(
            //               //     amount: num.parse(extraWork.totalVat ?? '0'),
            //               //   ),
            //               //   style: Theme.of(context)
            //               //       .textTheme
            //               //       .titleMedium!
            //               //       .copyWith(color: PartnerAppColors.darkBlue),
            //               // ),
            //               child: Text(
            //                 Formatter.curencyFormat(
            //                     amount: num.parse(isTypeMinus
            //                         ? extraWork.amount ?? '0'
            //                         : extraWork.totalVat ?? '0'),
            //                     sign: isTypeMinus ? 'negative' : ''),
            //                 style: Theme.of(context)
            //                     .textTheme
            //                     .titleMedium!
            //                     .copyWith(
            //                         color: isTypeMinus
            //                             ? PartnerAppColors.red
            //                             : PartnerAppColors.darkBlue),
            //               ),
            //             )
            //           ],
            //         ),
            //         const SizedBox(
            //           height: 10,
            //         ),
            //         Text(
            //           extraWork.description ?? '',
            //           style: Theme.of(context).textTheme.titleMedium,
            //         ),
            //         const SizedBox(
            //           height: 20,
            //         ),
            //         Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [
            //             ExtraWorkStatusChip(extraWork: extraWork),
            //             RichText(
            //               text: TextSpan(
            //                 children: [
            //                   TextSpan(
            //                       text: tr('open'),
            //                       style: Theme.of(context)
            //                           .textTheme
            //                           .titleMedium!
            //                           .copyWith(color: PartnerAppColors.blue)),
            //                   const WidgetSpan(
            //                     alignment: PlaceholderAlignment.middle,
            //                     child: Icon(
            //                       FeatherIcons.arrowRight,
            //                       size: 16,
            //                       color: PartnerAppColors.blue,
            //                     ),
            //                   ),
            //                 ],
            //               ),
            //             ),
            //           ],
            //         )
            //       ],
            //     ),
            //   ),
            // );
          })
        ],
      );
    }
  }
}
