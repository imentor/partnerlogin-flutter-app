import 'package:Haandvaerker.dk/model/projects/extra_work_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/components/signature_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/component/payment_slider_button.dart';
import 'package:Haandvaerker.dk/utils/custom_input_formatters.dart';
import 'package:Haandvaerker.dk/utils/debouncer.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/wallet/wallet_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

Future<void> createExtraWorkTypeInfoDialog({
  required BuildContext context,
}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: const EdgeInsets.all(20),
          title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            InkWell(
              onTap: () {
                Navigator.of(dialogContext).pop();
              },
              child: const Icon(
                FeatherIcons.x,
                color: PartnerAppColors.spanishGrey,
              ),
            )
          ]),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Text(
                    'Aftaleseddel',
                    style: Theme.of(context)
                        .textTheme
                        .titleMedium!
                        .copyWith(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    'En aftaleseddel dokumenterer ændringer i en eksisterende aftale og kan omfatte enten en reduktion eller en tilføjelse af arbejde.',
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .titleMedium!
                        .copyWith(color: PartnerAppColors.spanishGrey),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Divider(
                    color: PartnerAppColors.darkBlue,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                        text: 'Fradragspris:',
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(fontWeight: FontWeight.bold),
                        children: [
                          TextSpan(
                              text:
                                  ' Prisreduktion for arbejde, der fjernes fra aftalen.',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(
                                    color: PartnerAppColors.spanishGrey,
                                  ))
                        ]),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                        text: 'Tillægspris:',
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(fontWeight: FontWeight.bold),
                        children: [
                          TextSpan(
                              text:
                                  ' Ekstra omkostning for arbejde, der tilføjes til aftalen.',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(
                                    color: PartnerAppColors.spanishGrey,
                                  ))
                        ]),
                  )
                ],
              ),
            ),
          ),
        );
      });
}

class ExtraWorkRequestScreen extends StatefulWidget {
  const ExtraWorkRequestScreen({super.key, this.extraWork});

  final ExtraWorkItems? extraWork;

  @override
  State<ExtraWorkRequestScreen> createState() => _ExtraWorkRequestScreenState();
}

class _ExtraWorkRequestScreenState extends State<ExtraWorkRequestScreen> {
  final formKey = GlobalKey<FormBuilderState>();

  final Debouncer debouncer = Debouncer(milliseconds: 500);

  // final CurrencyTextInputFormatter currencyFormatter =
  //     CurrencyTextInputFormatter(
  //         NumberFormat.currency(
  //           locale: 'da',
  //           decimalDigits: 2,
  //           symbol: '',
  //         ),
  //         enableNegative: true);

  bool isLoadingPrices = false;
  bool isTermsChecked = false;

  double totalVat = 0;
  double totalVatWithFee = 0;

  int step = 0;

  Map<String, dynamic> payload = {};

  String type = 'addon';

  @override
  void initState() {
    final walletVm = context.read<WalletViewmodel>();

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if (widget.extraWork != null && widget.extraWork?.total != null) {
        switch (widget.extraWork?.type) {
          case 'minus':
            setState(() {
              type = 'minus';
              isLoadingPrices = false;
              totalVat = double.parse(widget.extraWork?.total ?? '0');
              totalVatWithFee = double.parse(widget.extraWork?.total ?? '0');
            });
            break;
          case 'addon':
            setState(() {
              isLoadingPrices = true;
            });
            await tryCatchWrapper(
                    context: context,
                    function: walletVm.calculatePrices(
                        price: double.parse(widget.extraWork?.total ?? '0')))
                .then((value) {
              setState(() {
                isLoadingPrices = false;
                type = 'addon';
                if (value != null) {
                  totalVat = value['amount'];
                  totalVatWithFee = value['totalVatWithFee'];
                }
              });
            });
            break;
        }
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<WalletViewmodel>(builder: (_, walletVm, __) {
      return Scaffold(
        appBar: DrawerAppBar(
          overrideBackFunction: () async {
            switch (step) {
              case 0:
                backDrawerRoute();
                break;
              case 1:
                setState(() {
                  step = 0;
                });
                break;
              default:
            }
          },
        ),
        bottomNavigationBar: Container(
          padding:
              const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
          color: Colors.white,
          child: buttonNav(walletVm),
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: FormBuilder(
            clearValueOnUnregister: true,
            key: formKey,
            child: body(walletVm),
          ),
        ),
      );
    });
  }

  Widget body(WalletViewmodel walletVm) {
    String initialValueDescription = '';
    DateTime initialValueDate = DateTime.now();
    num initialValuePrice = 0;

    if (payload.containsKey('description')) {
      initialValueDescription = payload['description'];
    } else {
      initialValueDescription = widget.extraWork?.description ?? '';
    }

    if (payload.containsKey('deadline')) {
      initialValueDate = payload['deadline'];
    } else if ((widget.extraWork?.endDate ?? '').isNotEmpty) {
      initialValueDate = DateTime.parse(widget.extraWork!.endDate!);
    }

    if (payload.containsKey('price')) {
      initialValuePrice = payload['price'];
    } else {
      initialValuePrice = num.parse(widget.extraWork?.total ?? '0');
    }

    switch (step) {
      case 0:
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomTextFieldFormBuilder(
              minLines: 7,
              name: 'description',
              hintText: tr('enter_description'),
              labelText: tr('description'),
              initialValue: initialValueDescription,
              validator:
                  FormBuilderValidators.required(errorText: tr('required')),
            ),
            CustomDatePickerFormBuilder(
              name: 'deadline',
              dateFormat: DateFormatType.standardDate.formatter,
              suffixIcon: const Icon(
                FeatherIcons.calendar,
                color: PartnerAppColors.blue,
                size: 28,
              ),
              initialDate: DateTime.now(),
              initialValue: initialValueDate,
              validator:
                  FormBuilderValidators.required(errorText: tr('required')),
            ),
            CustomTextFieldFormBuilder(
              name: 'price',
              textAlign: TextAlign.right,
              keyboardType: const TextInputType.numberWithOptions(signed: true),
              labelText: tr('adjust_price'),
              initialValue: Formatter.curencyFormat(
                  amount: initialValuePrice, withoutSymbol: true),
              textColor: type == 'addon'
                  ? PartnerAppColors.darkBlue
                  : PartnerAppColors.red,
              inputFormatters: [
                CustomCurrencyFormatter(isPositive: type == 'addon')
              ],
              validator: FormBuilderValidators.compose([
                (value) {
                  if ((value ?? '').isEmpty) {
                    return tr('required');
                  } else if (double.parse(
                          Formatter.sanitizeCurrencyFromFormatter(
                              (value!).split('-').last)) <=
                      50) {
                    return tr('amount_should_not_less_50');
                  }

                  return null;
                }
              ]),
              suffixIcon: Container(
                padding: const EdgeInsets.only(
                    left: 10, right: 10, bottom: 4, top: 12),
                child: Text(
                  'kr.',
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      fontSize: 18,
                      fontWeight: FontWeight.normal,
                      color: PartnerAppColors.darkBlue),
                ),
              ),
              onChanged: (p0) {
                debouncer.run(() async {
                  if ((p0 ?? '').isNotEmpty) {
                    String amountValue =
                        Formatter.sanitizeCurrencyFromFormatter(
                            (p0!).split('-').last);

                    formKey.currentState!.fields['price']!.invalidate('');

                    if (num.parse(amountValue) > 100) {
                      switch (type) {
                        case 'minus':
                          setState(() {
                            totalVat = double.parse(amountValue);
                            totalVatWithFee = double.parse(amountValue);
                          });
                          break;
                        case 'addon':
                          setState(() {
                            isLoadingPrices = true;
                          });

                          await tryCatchWrapper(
                                  context: context,
                                  function: walletVm.calculatePrices(
                                      price: double.parse(Formatter
                                          .sanitizeCurrencyFromFormatter(p0))))
                              .then((value) {
                            setState(() {
                              isLoadingPrices = false;
                              if (value != null) {
                                totalVat = value['amount'];
                                totalVatWithFee = value['totalVatWithFee'];
                              }
                            });
                          });
                          break;
                      }
                    } else {
                      formKey.currentState!.fields['price']!
                          .invalidate(tr('amount_should_not_less_50'));
                    }
                  }
                });
              },
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Text(
                  'Type',
                  style: Theme.of(context)
                      .textTheme
                      .titleSmall!
                      .copyWith(color: PartnerAppColors.spanishGrey),
                ),
                const SizedBox(
                  width: 5,
                ),
                InkWell(
                  onTap: () => createExtraWorkTypeInfoDialog(context: context),
                  child: const Icon(
                    FeatherIcons.helpCircle,
                    color: PartnerAppColors.spanishGrey,
                    size: 22,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.max,
              children: [
                Row(
                  children: [
                    Radio(
                      value: 'minus',
                      groupValue: type,
                      activeColor: PartnerAppColors.blue,
                      onChanged: (value) {
                        setState(() {
                          type = value ?? '';
                          totalVat = 0;
                          totalVatWithFee = 0;
                        });
                        formKey.currentState!.fields['price']!.reset();
                      },
                    ),
                    Text(
                      'Fradrags-pris',
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium!
                          .copyWith(color: PartnerAppColors.spanishGrey),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Radio(
                      value: 'addon',
                      groupValue: type,
                      activeColor: PartnerAppColors.blue,
                      onChanged: (value) {
                        setState(() {
                          type = value ?? '';
                          totalVat = 0;
                          totalVatWithFee = 0;
                        });
                        formKey.currentState!.fields['price']!.reset();
                      },
                    ),
                    Text(
                      'Tillægs-pris',
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium!
                          .copyWith(color: PartnerAppColors.spanishGrey),
                    ),
                  ],
                ),
              ],
            ),
            const Divider(
              color: PartnerAppColors.darkBlue,
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  tr('total_price_exl_vat'),
                  style: Theme.of(context)
                      .textTheme
                      .titleMedium!
                      .copyWith(color: PartnerAppColors.spanishGrey),
                ),
                isLoadingPrices
                    ? const SizedBox(
                        width: 25,
                        height: 25,
                        child: CircularProgressIndicator(
                          color: PartnerAppColors.darkBlue,
                        ),
                      )
                    : Text(
                        Formatter.curencyFormat(amount: totalVat),
                        style: Theme.of(context).textTheme.titleMedium,
                      )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  tr('total_price_with_vat_fee'),
                  style: Theme.of(context)
                      .textTheme
                      .titleMedium!
                      .copyWith(color: PartnerAppColors.spanishGrey),
                ),
                isLoadingPrices
                    ? const SizedBox(
                        width: 25,
                        height: 25,
                        child: CircularProgressIndicator(
                          color: PartnerAppColors.darkBlue,
                        ),
                      )
                    : Text(
                        Formatter.curencyFormat(
                            amount: totalVatWithFee,
                            sign: type == 'minus' ? 'negative' : ''),
                        style:
                            Theme.of(context).textTheme.titleMedium!.copyWith(
                                  color: type == 'minus'
                                      ? PartnerAppColors.red
                                      : PartnerAppColors.darkBlue,
                                ),
                      )
              ],
            )
          ],
        );
      case 1:
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              payload['description'] ?? '',
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              Formatter.formatDateStrings(
                  type: DateFormatType.standardDate,
                  dateString: (payload['deadline'] as DateTime).toString()),
              style: Theme.of(context)
                  .textTheme
                  .titleSmall!
                  .copyWith(color: PartnerAppColors.spanishGrey),
            ),
            const SizedBox(
              height: 10,
            ),
            const Divider(
              color: PartnerAppColors.darkBlue,
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  type == 'minus' ? 'Fradrags-pris' : 'Tillægs-pris',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                Text(
                  Formatter.curencyFormat(
                      amount: payload['price'],
                      sign: type == 'minus' ? 'negative' : ''),
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      color: type == 'minus'
                          ? PartnerAppColors.red
                          : PartnerAppColors.darkBlue),
                ),
              ],
            )
          ],
        );
      default:
        return const SizedBox.shrink();
    }
  }

  Widget buttonNav(WalletViewmodel walletVm) {
    switch (step) {
      case 0:
        return TextButton(
          style: TextButton.styleFrom(
              fixedSize: Size(MediaQuery.of(context).size.width, 45),
              backgroundColor: PartnerAppColors.malachite),
          child: Text(
            tr('next'),
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(color: Colors.white),
          ),
          onPressed: () {
            if (formKey.currentState!.validate() && !isLoadingPrices) {
              setState(() {
                for (var element in formKey.currentState!.fields.entries) {
                  if (element.key == 'price') {
                    payload[element.key] = num.parse(
                        Formatter.sanitizeCurrencyFromFormatter(
                            (element.value.value as String?)!.split('-').last));
                  } else {
                    payload[element.key] = element.value.value;
                  }
                }
                step = step + 1;
              });
            }
          },
        );
      case 1:
        return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: double.infinity,
              child: PaymentSliderButton(
                label: Text(
                  tr("send_to_craftsman"),
                  style: Theme.of(context)
                      .textTheme
                      .titleLarge!
                      .copyWith(color: Colors.white),
                ),
                action: () async {
                  final termsVm = context.read<SharedDataViewmodel>();

                  final signature = await signatureDialog(
                      context: context,
                      label: termsVm.termsLabel,
                      htmlTerms: termsVm.termsHtml);

                  if (!mounted) return false;

                  if (signature != null) {
                    modalManager.showLoadingModal();

                    await tryCatchWrapper(
                      context: context,
                      function: walletVm.addOrUpdateExtraWork(
                        contractId:
                            walletVm.currentViewedProject!.contractId ?? 0,
                        price: payload['price'],
                        endDate: '${payload['deadline']}',
                        description: payload['description'],
                        extraWorkId: widget.extraWork?.id,
                        signature: signature,
                        type: type,
                      ),
                    ).then((value) {
                      if (!mounted) return;

                      modalManager.hideLoadingModal();

                      if ((value ?? false)) {
                        commonSuccessOrFailedDialog(
                                context: context,
                                successMessage: tr('sent'),
                                message: tr('extra_work_sent'))
                            .whenComplete(() async {
                          if (!mounted) return;

                          backDrawerRoute();

                          await tryCatchWrapper(
                              context: context,
                              function: walletVm.initWallet(
                                  projectId: walletVm.currentViewedProject!.id!,
                                  contractId: walletVm
                                          .currentViewedProject!.contractId ??
                                      0));
                        });
                      }
                    });
                  }

                  return false;
                },
              ),
            ),
            if (widget.extraWork == null) ...[
              const SizedBox(
                height: 10,
              ),
              TextButton(
                style: TextButton.styleFrom(
                    fixedSize: Size(MediaQuery.of(context).size.width, 60),
                    backgroundColor: Colors.white,
                    shape: const RoundedRectangleBorder(
                        side: BorderSide(color: PartnerAppColors.blue))),
                child: Text(
                  tr('save_as_draft'),
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium!
                      .copyWith(color: PartnerAppColors.blue),
                ),
                onPressed: () async {
                  modalManager.showLoadingModal();

                  await tryCatchWrapper(
                      context: context,
                      function: walletVm.addOrUpdateExtraWork(
                        contractId:
                            walletVm.currentViewedProject!.contractId ?? 0,
                        price: payload['price'],
                        endDate: '${payload['deadline']}',
                        description: payload['description'],
                        signature: '',
                        type: type,
                      )).then((value) {
                    if (!mounted) return;

                    modalManager.hideLoadingModal();

                    if ((value ?? false)) {
                      commonSuccessOrFailedDialog(
                              context: context,
                              successMessage: tr('sent'),
                              message: tr('extra_work_sent'))
                          .whenComplete(() async {
                        if (!mounted) return;

                        backDrawerRoute();

                        await tryCatchWrapper(
                            context: context,
                            function: walletVm.initWallet(
                                projectId: walletVm.currentViewedProject!.id!,
                                contractId:
                                    walletVm.currentViewedProject!.contractId ??
                                        0));
                      });
                    }
                  });
                },
              )
            ]
          ],
        );
      default:
        return const SizedBox.shrink();
    }
  }
}
