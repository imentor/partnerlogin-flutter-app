import 'package:Haandvaerker.dk/model/projects/extra_work_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/components/signature_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/component/status_chips.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/wallet/wallet_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

Future<bool?> extraWorkDeleteReject({
  required BuildContext context,
  required String title,
}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: const EdgeInsets.all(20),
          title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            InkWell(
              onTap: () {
                Navigator.of(dialogContext).pop();
              },
              child: const Icon(
                FeatherIcons.x,
                color: PartnerAppColors.spanishGrey,
              ),
            )
          ]),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Text(
                    title,
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: ElevatedButton(
                          onPressed: () => Navigator.of(dialogContext).pop(),
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white,
                              shape: const RoundedRectangleBorder(
                                  side: BorderSide(
                                      color: PartnerAppColors.blue))),
                          child: Text(
                            tr('no'),
                            style: Theme.of(context)
                                .textTheme
                                .titleSmall!
                                .copyWith(color: PartnerAppColors.blue),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: ElevatedButton(
                          onPressed: () =>
                              Navigator.of(dialogContext).pop(true),
                          style: ElevatedButton.styleFrom(
                              backgroundColor: PartnerAppColors.red),
                          child: Text(
                            tr('yes'),
                            style: Theme.of(context)
                                .textTheme
                                .titleSmall!
                                .copyWith(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      });
}

class ExtraWorkDetailScreen extends StatefulWidget {
  const ExtraWorkDetailScreen({super.key, required this.extraWork});

  final ExtraWorkItems extraWork;

  @override
  ExtraWorkDetailScreenState createState() => ExtraWorkDetailScreenState();
}

class ExtraWorkDetailScreenState extends State<ExtraWorkDetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      bottomNavigationBar: Container(
        padding:
            const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
        color: Colors.white,
        child: buttonNav(context),
      ),
      body: widget.extraWork.stage == 'approved'
          ? approved(context)
          : body(context),
    );
  }

  Widget approved(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(20),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: PartnerAppColors.spanishGrey.withValues(alpha: .2),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          Expanded(
              child: SfPdfViewer.network(
            '${applic.bitrixApiUrl}/partner/extra_work/${widget.extraWork.id}/preview',
            headers: context.read<SharedDataViewmodel>().headers,
          ))
        ],
      ),
    );
  }

  Widget body(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.extraWork.description ?? '',
            style: Theme.of(context).textTheme.titleMedium,
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ExtraWorkStatusChip(extraWork: widget.extraWork),
              Text(
                Formatter.formatDateStrings(
                    type: DateFormatType.fullMonthDate,
                    dateString: widget.extraWork.endDate),
                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      color: PartnerAppColors.spanishGrey,
                    ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          const Divider(
            color: PartnerAppColors.darkBlue,
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                tr('project_price'),
                style: Theme.of(context).textTheme.titleMedium,
              ),
              Text(
                Formatter.curencyFormat(
                    amount: num.parse(widget.extraWork.amount ?? '0')),
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget buttonNav(BuildContext context) {
    final walletVm = context.read<WalletViewmodel>();

    if ((widget.extraWork.stage == 'for_approval' &&
            widget.extraWork.lastUpdatedBy == 'partner') ||
        widget.extraWork.stage == 'signed_by_partner') {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextButton(
            style: TextButton.styleFrom(
                fixedSize: Size(MediaQuery.of(context).size.width, 45),
                backgroundColor: Colors.white,
                shape: const RoundedRectangleBorder(
                    side: BorderSide(color: PartnerAppColors.blue))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Icon(
                  FeatherIcons.edit,
                  color: PartnerAppColors.blue,
                ),
                const SizedBox(
                  width: 5,
                ),
                Text(
                  tr('edit'),
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium!
                      .copyWith(color: PartnerAppColors.blue),
                )
              ],
            ),
            onPressed: () {
              backDrawerRoute();

              changeDrawerRoute(Routes.projectWalletExtraWorkRequest,
                  arguments: widget.extraWork);
            },
          ),
          const SizedBox(
            height: 10,
          ),
          InkWell(
            onTap: () async {
              extraWorkDeleteReject(
                      context: context,
                      title: 'Vil du slette dette ekstra arbejde?')
                  .then(
                (value) async {
                  if (!context.mounted) return;

                  if (value ?? false) {
                    modalManager.showLoadingModal();

                    await tryCatchWrapper(
                            context: context,
                            function: walletVm.deleteExtraWork(
                                extraWorkId: widget.extraWork.id!))
                        .then((value) {
                      if (!context.mounted) return;

                      modalManager.hideLoadingModal();

                      if ((value ?? false)) {
                        commonSuccessOrFailedDialog(
                          context: context,
                          successMessage: tr('extra_work_deleted'),
                        ).whenComplete(() async {
                          if (!context.mounted) return;

                          backDrawerRoute();

                          await tryCatchWrapper(
                              context: context,
                              function: walletVm.initWallet(
                                  projectId: walletVm.currentViewedProject!.id!,
                                  contractId: walletVm
                                          .currentViewedProject!.contractId ??
                                      0));
                        });
                      }
                    });
                  }
                },
              );
            },
            child: Text(
              'Slet kladde for ekstra arbejde',
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    color: PartnerAppColors.red,
                    fontWeight: FontWeight.normal,
                  ),
            ),
          )
        ],
      );
    } else if ((widget.extraWork.stage == 'for_approval' &&
            widget.extraWork.lastUpdatedBy == 'homeowner') ||
        widget.extraWork.stage == 'signed_by_customer') {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          // TextButton(
          //   style: TextButton.styleFrom(
          //       fixedSize: Size(MediaQuery.of(context).size.width, 45),
          //       backgroundColor: Colors.white,
          //       shape: const RoundedRectangleBorder(
          //           side: BorderSide(color: PartnerAppColors.blue))),
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.center,
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     children: [
          //       const Icon(
          //         FeatherIcons.edit,
          //         color: PartnerAppColors.blue,
          //       ),
          //       const SizedBox(
          //         width: 5,
          //       ),
          //       Text(
          //         tr('edit'),
          //         style: Theme.of(context)
          //             .textTheme
          //             .headlineMedium!
          //             .copyWith(color: PartnerAppColors.blue),
          //       )
          //     ],
          //   ),
          //   onPressed: () {
          //     backDrawerRoute();

          //     changeDrawerRoute(Routes.projectWalletExtraWorkRequest,
          //         arguments: extraWork);
          //   },
          // ),
          // const SizedBox(
          //   height: 10,
          // ),

          TextButton(
            style: TextButton.styleFrom(
              fixedSize: Size(MediaQuery.of(context).size.width, 45),
              backgroundColor: PartnerAppColors.malachite,
            ),
            child: Text(
              tr('approve'),
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium!
                  .copyWith(color: Colors.white),
            ),
            onPressed: () => onApprove(walletVm),
          ),
          const SizedBox(
            height: 10,
          ),
          TextButton(
            style: TextButton.styleFrom(
                fixedSize: Size(MediaQuery.of(context).size.width, 45),
                backgroundColor: Colors.white,
                shape: const RoundedRectangleBorder(
                    side: BorderSide(color: PartnerAppColors.blue))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  tr('create_additional_work'),
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium!
                      .copyWith(color: PartnerAppColors.blue),
                )
              ],
            ),
            onPressed: () {
              backDrawerRoute();

              changeDrawerRoute(Routes.projectWalletExtraWorkRequest,
                  arguments: ExtraWorkItems(
                    endDate: widget.extraWork.endDate,
                    description: widget.extraWork.description,
                    total: widget.extraWork.total,
                    type: widget.extraWork.type,
                  ));
            },
          ),
          const SizedBox(
            height: 10,
          ),
          InkWell(
            onTap: () async {
              extraWorkDeleteReject(
                      context: context,
                      title: tr('extra_work_reject_confirmation'))
                  .then(
                (value) async {
                  if (!context.mounted) return;

                  if (value ?? false) {
                    modalManager.showLoadingModal();

                    await tryCatchWrapper(
                            context: context,
                            function: walletVm.rejectExtraWork(
                                extraWorkId: widget.extraWork.id!))
                        .then((value) {
                      if (!context.mounted) return;

                      modalManager.hideLoadingModal();

                      if ((value ?? false)) {
                        commonSuccessOrFailedDialog(
                          context: context,
                          successMessage: tr('rejected'),
                        ).whenComplete(() async {
                          if (!context.mounted) return;

                          backDrawerRoute();

                          await tryCatchWrapper(
                              context: context,
                              function: walletVm.initWallet(
                                  projectId: walletVm.currentViewedProject!.id!,
                                  contractId: walletVm
                                          .currentViewedProject!.contractId ??
                                      0));
                        });
                      }
                    });
                  }
                },
              );
            },
            child: Text(
              tr('reject_extra_work'),
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    color: PartnerAppColors.red,
                    fontWeight: FontWeight.normal,
                  ),
            ),
          )
        ],
      );
    } else if (widget.extraWork.stage == 'declined') {
      return const SizedBox.shrink();
      // return TextButton(
      //   style: TextButton.styleFrom(
      //       fixedSize: Size(MediaQuery.of(context).size.width, 45),
      //       backgroundColor: Colors.white,
      //       shape: const RoundedRectangleBorder(
      //           side: BorderSide(color: PartnerAppColors.blue))),
      //   child: Row(
      //     mainAxisAlignment: MainAxisAlignment.center,
      //     crossAxisAlignment: CrossAxisAlignment.center,
      //     children: [
      //       Text(
      //         tr('create_additional_work'),
      //         style: Theme.of(context)
      //             .textTheme
      //             .headlineMedium!
      //             .copyWith(color: PartnerAppColors.blue),
      //       )
      //     ],
      //   ),
      //   onPressed: () {
      //     backDrawerRoute();

      //     changeDrawerRoute(Routes.projectWalletExtraWorkRequest,
      //         arguments: ExtraWorkItems(
      //           endDate: widget.extraWork.endDate,
      //           description: widget.extraWork.description,
      //           total: widget.extraWork.total,
      //           type: 'addon',
      //         ));
      //   },
      // );
    } else if ((widget.extraWork.stage == 'draft' &&
            widget.extraWork.createdBy == 'partner') ||
        widget.extraWork.stage == 'drafted_by_partner') {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextButton(
            style: TextButton.styleFrom(
              fixedSize: Size(MediaQuery.of(context).size.width, 45),
              backgroundColor: PartnerAppColors.malachite,
            ),
            child: Text(
              tr('approve'),
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium!
                  .copyWith(color: Colors.white),
            ),
            onPressed: () => onApprove(walletVm),
          ),
          const SizedBox(
            height: 10,
          ),
          TextButton(
            style: TextButton.styleFrom(
                fixedSize: Size(MediaQuery.of(context).size.width, 45),
                backgroundColor: Colors.white,
                shape: const RoundedRectangleBorder(
                    side: BorderSide(color: PartnerAppColors.blue))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Icon(
                  FeatherIcons.edit,
                  color: PartnerAppColors.blue,
                ),
                const SizedBox(
                  width: 5,
                ),
                Text(
                  tr('edit'),
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium!
                      .copyWith(color: PartnerAppColors.blue),
                )
              ],
            ),
            onPressed: () {
              backDrawerRoute();

              changeDrawerRoute(Routes.projectWalletExtraWorkRequest,
                  arguments: widget.extraWork);
            },
          ),
          const SizedBox(
            height: 10,
          ),
          InkWell(
            onTap: () async {
              extraWorkDeleteReject(
                      context: context,
                      title: 'Vil du slette dette ekstra arbejde?')
                  .then(
                (value) async {
                  if (!context.mounted) return;

                  if (value ?? false) {
                    modalManager.showLoadingModal();

                    await tryCatchWrapper(
                            context: context,
                            function: walletVm.deleteExtraWork(
                                extraWorkId: widget.extraWork.id!))
                        .then((value) {
                      if (!context.mounted) return;

                      modalManager.hideLoadingModal();

                      if ((value ?? false)) {
                        commonSuccessOrFailedDialog(
                          context: context,
                          successMessage: tr('extra_work_deleted'),
                        ).whenComplete(() async {
                          if (!context.mounted) return;

                          backDrawerRoute();

                          await tryCatchWrapper(
                              context: context,
                              function: walletVm.initWallet(
                                  projectId: walletVm.currentViewedProject!.id!,
                                  contractId: walletVm
                                          .currentViewedProject!.contractId ??
                                      0));
                        });
                      }
                    });
                  }
                },
              );
            },
            child: Text(
              'Slet kladde for ekstra arbejde',
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    color: PartnerAppColors.red,
                    fontWeight: FontWeight.normal,
                  ),
            ),
          )
        ],
      );
    } else {
      return const SizedBox.shrink();
    }
  }

  Future<void> onApprove(WalletViewmodel walletVm) async {
    final termsVm = context.read<SharedDataViewmodel>();

    final signature = await signatureDialog(
        context: context,
        label: termsVm.termsLabel,
        htmlTerms: termsVm.termsHtml);

    if (!mounted) return;

    if (signature != null) {
      modalManager.showLoadingModal();

      await tryCatchWrapper(
              context: context,
              function: walletVm.approveExtraWork(
                  extraWork: widget.extraWork, signature: signature))
          .then((value) {
        if (!mounted) return;

        modalManager.hideLoadingModal();

        if ((value ?? false)) {
          commonSuccessOrFailedDialog(
                  context: context,
                  successMessage: tr('sent'),
                  message: tr('extra_work_sent'))
              .whenComplete(() async {
            if (!mounted) return;

            backDrawerRoute();

            await tryCatchWrapper(
                context: context,
                function: walletVm.initWallet(
                    projectId: walletVm.currentViewedProject!.id!,
                    contractId:
                        walletVm.currentViewedProject!.contractId ?? 0));
          });
        }
      });
    }
  }
}
