import 'package:Haandvaerker.dk/model/projects/extra_work_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/payproff/payproff_iban_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/payproff/payproff_request_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/wallet_enterprise_details_dialog.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/payproff/payproff_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/wallet/wallet_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:url_launcher/url_launcher.dart';

class WalletScreen extends StatefulWidget {
  const WalletScreen({super.key, this.isFromMain = true});

  final bool isFromMain;

  @override
  State<WalletScreen> createState() => _WalletScreenState();
}

class _WalletScreenState extends State<WalletScreen> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final payproffVm = context.read<PayproffViewModel>();
      final projectsVm = context.read<MyProjectsViewModel>();
      final wallet = context.read<WalletViewmodel>();

      if (widget.isFromMain && wallet.paymentStageType != 'normal') {
        await tryCatchWrapper(
            context: context,
            function: wallet.initWallet(
                projectId: projectsVm.activeProject.id!,
                contractId: projectsVm.activeProject.contractId));
      }

      if (!(payproffVm.payproff?.exists ?? false)) {
        if (!(payproffVm.payproff?.payproffVerified ?? false) && mounted) {
          Navigator.pop(context);
          payproffVerifyDialog(
            context: context,
            verify: () async {
              final statusUpdated = payproffVm.statusUpdated ?? DateTime.now();
              if (!payproffVm.isPayproffVerified &&
                  payproffVm.statusUpdated != null &&
                  DateTime.now().difference(statusUpdated).inHours < 48) {
                Navigator.pop(context);
                await showPayproffRequestDialog(context: context);
              } else {
                final payproffUrl = payproffVm.payproff?.payproffUrl;
                if (payproffUrl != null) {
                  launchUrl(Uri.parse(payproffUrl));
                  await payproffVm.updatePayproffStatus();
                }
              }
            },
          );
        } else {
          final iban = payproffVm.payproffWallet?.iban ?? '';
          if (iban.isEmpty) {
            payproffVm.isShowBanner = true;
            payproffVm.isPreIbanForm = true;
            payproffVm.ibanController = TextEditingController();
            if (mounted) {
              await showPayproffIbanDialog(context: context);
            }
          }
        }
      } else if (payproffVm.payproff?.exists ?? false) {
        if (!(payproffVm.payproff?.payproffVerified ?? false) && mounted) {
          Navigator.pop(context);
          payproffVerifyDialog(
            context: context,
            verify: () async {
              final statusUpdated = payproffVm.statusUpdated ?? DateTime.now();
              final paproffUrl = payproffVm.payproff?.payproffUrl ?? '';
              if (!payproffVm.isPayproffVerified &&
                  DateTime.now().difference(statusUpdated).inHours < 48) {
                Navigator.pop(context);
                await showPayproffRequestDialog(context: context);
              } else {
                if (paproffUrl.isNotEmpty) {
                  launchUrl(Uri.parse(paproffUrl));
                  await payproffVm.updatePayproffStatus();
                }
              }
            },
          );
        } else {
          final iban = payproffVm.payproffWallet?.iban ?? '';
          if (iban.isEmpty) {
            payproffVm.isShowBanner = true;
            payproffVm.isPreIbanForm = true;
            payproffVm.ibanController = TextEditingController();
            if (mounted) {
              await showPayproffIbanDialog(context: context);
            }
          }
        }
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          gradient: LinearGradient(colors: [
            PartnerAppColors.blue,
            PartnerAppColors.blue.withValues(alpha: .2)
          ], begin: Alignment.bottomRight, end: Alignment.topLeft),
        ),
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: Consumer<WalletViewmodel>(builder: (_, walletVm, __) {
            int forApprovalExtraWorkCount =
                ((walletVm.extraWork?.items?['EXTRA_WORKS'] ??
                        <ExtraWorkItems>[]))
                    .where((value) =>
                        value.stage == 'for_approval' &&
                        value.createdBy == 'homeowner')
                    .length;
            return Column(
              children: [
                Align(
                  alignment: Alignment.centerRight,
                  child: InkWell(
                    onTap: () {
                      backDrawerRoute();
                    },
                    child: RichText(
                      text: TextSpan(
                        text: tr('go_to_project_management'),
                        style: Theme.of(context)
                            .textTheme
                            .titleSmall!
                            .copyWith(color: PartnerAppColors.blue),
                        children: const [
                          WidgetSpan(
                            alignment: PlaceholderAlignment.middle,
                            child: Icon(
                              FeatherIcons.arrowRight,
                              color: PartnerAppColors.blue,
                              size: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Skeletonizer(
                  enabled: walletVm.busy,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Card(
                        elevation: 0,
                        child: Padding(
                          padding: const EdgeInsets.all(20),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    tr('contract_sum'),
                                    style: Theme.of(context)
                                        .textTheme
                                        .headlineMedium!
                                        .copyWith(
                                            fontWeight: FontWeight.normal),
                                  ),
                                  Text(
                                    Formatter.curencyFormat(
                                        amount: walletVm
                                                .currentViewedPaymentStage
                                                ?.contract
                                                ?.vatPrice ??
                                            0),
                                    style: Theme.of(context)
                                        .textTheme
                                        .headlineMedium!
                                        .copyWith(fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              InkWell(
                                onTap: () => walletEnterpriseDetailsDialog(
                                    context: context,
                                    paymentStage:
                                        walletVm.currentViewedPaymentStage!),
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    tr('see_details'),
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleMedium!
                                        .copyWith(
                                          color: PartnerAppColors.blue,
                                        ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              const Divider(
                                color: PartnerAppColors.darkBlue,
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  SvgPicture.asset(
                                    height: 60,
                                    width: 60,
                                    SvgIcons.walletDkk,
                                    colorFilter: const ColorFilter.mode(
                                      PartnerAppColors.blue,
                                      BlendMode.srcIn,
                                    ),
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Text(
                                        Formatter.curencyFormat(
                                            amount: walletVm.stagesList[
                                                'paymentFromCustomerTotal']),
                                        style: Theme.of(context)
                                            .textTheme
                                            .headlineLarge!
                                            .copyWith(
                                                fontWeight: FontWeight.bold),
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        tr('payment_from_customer'),
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleMedium!
                                            .copyWith(
                                              color:
                                                  PartnerAppColors.spanishGrey,
                                            ),
                                      ),
                                    ],
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Text(
                        tr('transaction_summary'),
                        style: Theme.of(context)
                            .textTheme
                            .headlineLarge!
                            .copyWith(fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      InkWell(
                        onTap: () {
                          changeDrawerRoute(Routes.projectWalletTransaction);
                        },
                        child: Card(
                          elevation: 0,
                          child: Padding(
                            padding: const EdgeInsets.all(20),
                            child: Column(
                              children: [
                                Text(
                                  tr('transaction_summary_description'),
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineMedium!
                                      .copyWith(
                                          fontWeight: FontWeight.normal,
                                          color: PartnerAppColors.spanishGrey),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    SvgPicture.asset(
                                      SvgIcons.paidExchange,
                                      width: 50,
                                      height: 50,
                                    ),
                                    RichText(
                                      text: TextSpan(
                                          text: tr('request_now'),
                                          style: Theme.of(context)
                                              .textTheme
                                              .headlineMedium!
                                              .copyWith(
                                                  fontWeight: FontWeight.bold,
                                                  color: PartnerAppColors.blue),
                                          children: const [
                                            WidgetSpan(
                                                alignment:
                                                    PlaceholderAlignment.middle,
                                                child: Icon(
                                                  FeatherIcons.arrowRight,
                                                  color: PartnerAppColors.blue,
                                                ))
                                          ]),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      if (walletVm.paymentStageType == 'normal') ...[
                        const SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: [
                            Text(
                              tr('extra_work'),
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineLarge!
                                  .copyWith(fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Tooltip(
                                triggerMode: TooltipTriggerMode.tap,
                                showDuration: const Duration(seconds: 2),
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                padding: const EdgeInsets.all(20),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                      color: PartnerAppColors.darkBlue,
                                    ),
                                    borderRadius: BorderRadius.circular(5)),
                                textStyle: Theme.of(context)
                                    .textTheme
                                    .headlineSmall!
                                    .copyWith(
                                        color: PartnerAppColors.darkBlue,
                                        fontSize: 14,
                                        fontWeight: FontWeight.normal),
                                message: tr('extra_work_description'),
                                child: const Icon(FeatherIcons.helpCircle)),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        InkWell(
                          onTap: () {
                            changeDrawerRoute(Routes.projectWalletExtraWork);
                          },
                          child: Card(
                            elevation: 0,
                            child: Padding(
                              padding: const EdgeInsets.all(20),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Flexible(
                                        child: Text(
                                          'Aftaleseddel afventer godkendelse: ',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headlineMedium!
                                              .copyWith(
                                                  fontWeight: FontWeight.normal,
                                                  color: PartnerAppColors
                                                      .spanishGrey),
                                        ),
                                      ),
                                      Flexible(
                                        child: Container(
                                          padding: const EdgeInsets.all(10),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            color: PartnerAppColors.orangeApple,
                                          ),
                                          child: FittedBox(
                                            child: Text(
                                              '$forApprovalExtraWorkCount afventende godkendelse',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .titleSmall!
                                                  .copyWith(
                                                    color: Colors.white,
                                                  ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  Align(
                                    alignment: Alignment.centerRight,
                                    child: RichText(
                                      text: TextSpan(
                                        text: tr('see_agreement_sheets'),
                                        style: Theme.of(context)
                                            .textTheme
                                            .headlineMedium!
                                            .copyWith(
                                                fontWeight: FontWeight.bold,
                                                color: PartnerAppColors.blue),
                                        children: const [
                                          WidgetSpan(
                                            alignment:
                                                PlaceholderAlignment.middle,
                                            child: Icon(
                                              FeatherIcons.arrowRight,
                                              color: PartnerAppColors.blue,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ]
                    ],
                  ),
                ),
              ],
            );
          }),
        ),
      ),
    );
  }
}
