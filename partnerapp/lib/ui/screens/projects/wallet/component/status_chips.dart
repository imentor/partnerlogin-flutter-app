import 'package:Haandvaerker.dk/model/payment/payment_stages_model.dart';
import 'package:Haandvaerker.dk/model/projects/extra_work_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class TransactionStatusChip extends StatelessWidget {
  const TransactionStatusChip(
      {super.key, required this.stage, this.isChip = true});

  final Stage stage;
  final bool isChip;

  @override
  Widget build(BuildContext context) {
    Color statusColor = PartnerAppColors.malachite;

    final isDk = context.locale.languageCode == 'da';

    String status = isDk ? (stage.statusDa ?? '') : (stage.statusEn ?? '');

    switch (stage.status) {
      case 'requested':
      case 'approved_wallet_not_updated':
        statusColor = PartnerAppColors.orangeApple;
        break;
      case 'paid':
      case 'approved':
      case 'partially_paid':
        if ((stage.paymentReleased ?? 0) <= (stage.priceDeducted ?? 0)) {
          statusColor = PartnerAppColors.approveGreen;
          status = tr('payment_stage_paid');
        } else {
          statusColor = PartnerAppColors.partialYellow;
        }
        break;
      case 'pending':
        statusColor = PartnerAppColors.pendingBlue;
        break;
      case 'declined':
        statusColor = PartnerAppColors.declinedRed;
        break;
      case 'payment_refunded':
        statusColor = PartnerAppColors.refundBlue;
        break;
      default:
    }

    if (!isChip) {
      return Text(
        status,
        style: Theme.of(context)
            .textTheme
            .titleMedium!
            .copyWith(color: statusColor, fontWeight: FontWeight.normal),
      );
    }

    return Container(
      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: statusColor.withValues(alpha: .1)),
      child: Center(
        child: Text(
          status,
          style: Theme.of(context)
              .textTheme
              .titleMedium!
              .copyWith(color: statusColor, fontWeight: FontWeight.normal),
        ),
      ),
    );
  }
}

class ExtraWorkStatusChip extends StatelessWidget {
  const ExtraWorkStatusChip({super.key, required this.extraWork});

  final ExtraWorkItems? extraWork;

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      Color statusColor = PartnerAppColors.malachite;
      String stat = '';

      switch (extraWork?.stage) {
        case 'approved':
          statusColor = PartnerAppColors.malachite;
          stat = 'approved';
          break;
        case 'for_approval':
          statusColor = PartnerAppColors.orangeApple;
          if (extraWork?.createdBy == 'homeowner') {
            stat = 'extra_work_request_pending';
          } else {
            stat = 'request_pending';
          }
          break;
        case 'signed_by_partner':
          statusColor = PartnerAppColors.orangeApple;
          stat = 'request_pending';
          break;
        case 'signed_by_customer':
          statusColor = PartnerAppColors.orangeApple;
          stat = 'extra_work_request_pending';
          break;
        case 'declined':
          statusColor = PartnerAppColors.red;
          stat = 'rejected';
          break;
        case 'draft':
        case 'drafted_by_partner':
          statusColor = PartnerAppColors.blue;
          stat = 'draft';
          break;
        default:
      }

      return Container(
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: statusColor.withValues(alpha: .1)),
        child: Center(
          child: Text(
            tr(stat),
            style: Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(color: statusColor, fontWeight: FontWeight.normal),
          ),
        ),
      );
    });
  }
}

class PaymentTransactionStatusChip extends StatelessWidget {
  const PaymentTransactionStatusChip(
      {super.key, required this.stage, required this.paymentTransaction});

  final Stage stage;
  final PaymentTransaction paymentTransaction;

  @override
  Widget build(BuildContext context) {
    Color statusColor = PartnerAppColors.malachite;
    String newStatus = '';

    switch (paymentTransaction.status) {
      case 'requested':
        statusColor = PartnerAppColors.orangeApple;
        newStatus = tr('payment_stage_requested');
        break;
      case 'approved_wallet_not_updated':
        statusColor = PartnerAppColors.orangeApple;
        newStatus = tr('payment_stage_approved_wallet_not_updated');
        break;
      case 'paid':
      case 'approved':
      case 'partially_paid':
        if (double.parse((paymentTransaction.amount ?? '0')) <=
            (stage.priceDeducted ?? 0)) {
          statusColor = PartnerAppColors.approveGreen;
          newStatus = tr('payment_stage_paid');
        } else {
          statusColor = PartnerAppColors.lightYellow;
          newStatus = tr('payment_stage_partially_paid');
        }
        break;
      case 'pending':
        statusColor = PartnerAppColors.pendingBlue;
        newStatus = tr('payment_stage_pending');
        break;
      case 'declined':
        statusColor = PartnerAppColors.declinedRed;
        newStatus = tr('payment_stage_decline');
        break;
      case 'payment_refunded':
        statusColor = PartnerAppColors.refundBlue;
        newStatus = tr('payment_stage_payment_refunded');
        break;
      default:
    }

    return Text(
      newStatus,
      style: Theme.of(context)
          .textTheme
          .titleSmall!
          .copyWith(color: statusColor, fontWeight: FontWeight.normal),
      textAlign: TextAlign.end,
    );
  }
}
