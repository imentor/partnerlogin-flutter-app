import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/create_offer_wizard/sign_submit_offer/create_offer.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';
import 'package:slider_button/slider_button.dart';

class PaymentSliderButton extends StatefulWidget {
  const PaymentSliderButton({
    super.key,
    required this.label,
    required this.action,
    this.showTerms = false,
  });

  final Widget label;
  final Future<bool?> Function() action;
  final bool showTerms;

  @override
  State<PaymentSliderButton> createState() => _PaymentSliderButtonState();
}

class _PaymentSliderButtonState extends State<PaymentSliderButton> {
  bool termsChecked = false;

  @override
  Widget build(BuildContext context) {
    return Consumer<SharedDataViewmodel>(builder: (_, termsVm, __) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (widget.showTerms) ...[
            Row(
              children: [
                SizedBox(
                  height: 24,
                  width: 24,
                  child: Checkbox(
                      value: termsChecked,
                      onChanged: (value) => setState(() {
                            termsChecked = value ?? false;
                          })),
                ),
                const SizedBox(
                  width: 5,
                ),
                RichText(
                  text: TextSpan(
                    text: '${tr('accept_ours')} ',
                    style: Theme.of(context).textTheme.titleSmall,
                    children: [
                      TextSpan(
                        text: tr('conditions'),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            termsDialog(
                              context: context,
                              termsHTML: termsVm.termsHtml,
                            );
                          },
                        style: Theme.of(context)
                            .textTheme
                            .titleSmall!
                            .copyWith(color: PartnerAppColors.blue),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
          ],
          SizedBox(
            height: 80,
            child: SliderButton(
              action: widget.action,
              backgroundColor: PartnerAppColors.malachite,
              buttonColor: Colors.white,
              icon: termsChecked || !widget.showTerms
                  ? const Icon(
                      FeatherIcons.chevronRight,
                      color: PartnerAppColors.malachite,
                      size: 50,
                    )
                  : null,
              buttonSize: 60,
              label: widget.label,
              shimmer: false,
              alignLabel: Alignment.center,
              width: MediaQuery.of(context).size.width,
              disable: widget.showTerms && !termsChecked,
            ),
          ),
        ],
      );
    });
  }
}
