import 'package:Haandvaerker.dk/model/payment/payment_stages_model.dart';
import 'package:Haandvaerker.dk/model/projects/extra_work_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/component/status_chips.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/wallet/wallet_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';

class StageCard extends StatelessWidget {
  const StageCard({
    super.key,
    required this.walletVm,
    required this.stage,
    required this.title,
  });

  final WalletViewmodel walletVm;
  final Stage stage;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) {
        return InkWell(
          onTap: () {
            walletVm.currentViewedStage = stage;

            if (stage.status == 'pending' || stage.status == 'partial_paid') {
              if ((stage.stashed ?? false)) {
                changeDrawerRoute(Routes.projectWalletTransactionRequest,
                    arguments: title);
              } else {
                changeDrawerRoute(Routes.projectWalletTransactionDetail,
                    arguments: title);
              }
              // if (walletVm.isFullDeposit) {
              //   if ((stage.priceDeducted ?? 0) > (stage.paymentReleased ?? 0)) {
              //     changeDrawerRoute(Routes.projectWalletTransactionRequest,
              //         arguments: title);
              //   } else {
              //     changeDrawerRoute(Routes.projectWalletTransactionDetail,
              //         arguments: title);
              //   }
              // } else {
              //   changeDrawerRoute(Routes.projectWalletTransactionDetail,
              //       arguments: title);
              // }
            } else {
              changeDrawerRoute(Routes.projectWalletTransactionDetail,
                  arguments: title);
            }
          },
          child: Container(
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(
                        color:
                            PartnerAppColors.darkBlue.withValues(alpha: .3)))),
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Text(
                        title,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(
                                fontWeight: FontWeight.normal,
                                color: stage.type == 'deficiency'
                                    ? PartnerAppColors.blue
                                    : PartnerAppColors.darkBlue),
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Text(
                        Formatter.curencyFormat(
                            amount: stage.priceDeducted ?? 0),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(
                                fontWeight: FontWeight.normal,
                                color: stage.type == 'deficiency'
                                    ? PartnerAppColors.blue
                                    : PartnerAppColors.darkBlue),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TransactionStatusChip(
                      stage: stage,
                    ),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                              text: tr('open'),
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(color: PartnerAppColors.blue)),
                          const WidgetSpan(
                            alignment: PlaceholderAlignment.middle,
                            child: Icon(
                              FeatherIcons.arrowRight,
                              size: 16,
                              color: PartnerAppColors.blue,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

class ManufacturerSubcontractorStageCard extends StatelessWidget {
  const ManufacturerSubcontractorStageCard({
    super.key,
    required this.walletVm,
    required this.stage,
    required this.title,
  });

  final WalletViewmodel walletVm;
  final Stage stage;
  final String title;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        walletVm.currentViewedStage = stage;

        changeDrawerRoute(Routes.projectWalletTransactionDetail,
            arguments: title);
      },
      child: Container(
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    color: PartnerAppColors.darkBlue.withValues(alpha: .3)))),
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 2,
                  child: Text(
                    title,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        fontWeight: FontWeight.bold,
                        color: PartnerAppColors.darkBlue),
                  ),
                ),
                Flexible(
                  flex: 1,
                  child: Text(
                    Formatter.curencyFormat(amount: stage.priceDeducted ?? 0),
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        fontWeight: FontWeight.bold,
                        color: stage.type == 'deficiency'
                            ? PartnerAppColors.blue
                            : PartnerAppColors.darkBlue),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              stage.description ?? '',
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context)
                  .textTheme
                  .titleSmall!
                  .copyWith(color: PartnerAppColors.spanishGrey),
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TransactionStatusChip(
                  stage: stage,
                ),
                RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                          text: tr('open'),
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium!
                              .copyWith(color: PartnerAppColors.blue)),
                      const WidgetSpan(
                        alignment: PlaceholderAlignment.middle,
                        child: Icon(
                          FeatherIcons.arrowRight,
                          size: 16,
                          color: PartnerAppColors.blue,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class StageExtraWork extends StatelessWidget {
  const StageExtraWork({super.key, this.stage, this.extraWork});

  final Stage? stage;
  final ExtraWorkItems? extraWork;

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      String description = '';
      String date = '';

      num price = 0;

      bool isDeficiency = stage?.type == 'deficiency';
      bool isMinus = extraWork?.type == 'minus';

      Widget statusChips = const SizedBox.shrink();

      if (stage != null) {
        date = stage?.createdAt ?? '';
        price = stage?.priceDeducted ?? 0;

        if (isDeficiency) {
          description = tr('security_amount');
        } else {
          description = stage?.description ?? '';
        }
        statusChips = TransactionStatusChip(
          stage: stage!,
        );
      } else if (extraWork != null) {
        date = extraWork?.createdOn ?? '';
        description = extraWork?.description ?? '';

        price = num.parse(extraWork?.amount ?? '0');

        statusChips = ExtraWorkStatusChip(extraWork: extraWork);
      }

      return InkWell(
        onTap: () {
          final walletVm = context.read<WalletViewmodel>();

          if (stage != null) {
            walletVm.currentViewedStage = stage;

            if (stage!.status == 'pending' || stage!.status == 'partial_paid') {
              if ((stage!.stashed ?? false)) {
                changeDrawerRoute(Routes.projectWalletTransactionRequest,
                    arguments: description);
              } else {
                changeDrawerRoute(Routes.projectWalletTransactionDetail,
                    arguments: description);
              }
            } else {
              changeDrawerRoute(Routes.projectWalletTransactionDetail,
                  arguments: description);
            }
          } else if (extraWork != null) {
            if (isMinus) {
              changeDrawerRoute(Routes.projectWalletExtraWorkDetail,
                  arguments: extraWork);
            } else {
              if (extraWork!.stage == 'draft') {
                changeDrawerRoute(Routes.projectWalletExtraWorkRequest,
                    arguments: extraWork);
              } else {
                changeDrawerRoute(Routes.projectWalletExtraWorkDetail,
                    arguments: extraWork);
              }
            }
          }
        },
        child: Card(
          color: Colors.white,
          margin: const EdgeInsets.only(bottom: 10),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Text(
                        Formatter.formatDateStrings(
                            type: DateFormatType.fullMonthDate,
                            dateString: date),
                        style: Theme.of(context)
                            .textTheme
                            .titleSmall!
                            .copyWith(color: PartnerAppColors.spanishGrey),
                      ),
                    ),
                    Flexible(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            Formatter.curencyFormat(
                              amount: price,
                              sign: isMinus ? 'negative' : '',
                            ),
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium!
                                .copyWith(
                                    color: isDeficiency
                                        ? PartnerAppColors.blue
                                        : isMinus
                                            ? PartnerAppColors.red
                                            : PartnerAppColors.darkBlue),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(
                            isMinus ? 'Fradrags-pris' : 'Tillægs-pris',
                            style: Theme.of(context)
                                .textTheme
                                .titleSmall!
                                .copyWith(
                                    color: PartnerAppColors.spanishGrey,
                                    fontStyle: FontStyle.italic),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                const Divider(
                  color: PartnerAppColors.darkBlue,
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  description,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      fontWeight: FontWeight.normal,
                      color: isDeficiency
                          ? PartnerAppColors.blue
                          : PartnerAppColors.darkBlue),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    statusChips,
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                              text: tr('open'),
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(color: PartnerAppColors.blue)),
                          const WidgetSpan(
                            alignment: PlaceholderAlignment.middle,
                            child: Icon(
                              FeatherIcons.arrowRight,
                              size: 16,
                              color: PartnerAppColors.blue,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      );
    });
  }
}
