import 'package:Haandvaerker.dk/model/partner/case_project_model.dart';
import 'package:Haandvaerker.dk/model/partner/supplier_info_file_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/cache_image.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:Haandvaerker.dk/utils/extension/snackbar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/facebook/facebook_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/create_projects_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_share_me/flutter_share_me.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class MyProjectsScreen extends StatefulWidget {
  const MyProjectsScreen({super.key});

  @override
  MyProjectScreenState createState() => MyProjectScreenState();
}

class MyProjectScreenState extends State<MyProjectsScreen> {
  Map<String?, ObjectKey> attachmentExpansionKeyMap = <String?, ObjectKey>{};

  Future<void> getMyProjects() async {
    final myProjectsVm = context.read<MyProjectsViewModel>();
    final createProjectVm = context.read<CreateProjectsViewModel>();
    final currentContext = myGlobals.homeScaffoldKey?.currentContext ?? context;

    myProjectsVm.setBusy(true);
    await tryCatchWrapper(
      context: currentContext,
      function: Future.wait(
        [
          myProjectsVm.getProjects(),
          createProjectVm.getJobTypes(),
          createProjectVm.getAllManufactures(),
        ],
      ),
    ).whenComplete(() {
      myProjectsVm.setBusy(false);
    });
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final appDrawerVm = context.read<AppDrawerViewModel>();
      final newsFeedVm = context.read<NewsFeedViewModel>();
      const dynamicStoryPage = 'projekter';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      getMyProjects();
      if (dynamicStoryEnable) {
        await Future.value(newsFeedVm.getDynamicStory(page: dynamicStoryPage))
            .whenComplete(() {
          if (!mounted) return;

          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Consumer<MyProjectsViewModel>(
        builder: (context, myProjectsVm, _) {
          return RefreshIndicator(
            onRefresh: () => Future.sync(() async {
              myProjectsVm.setBusy(true);
              await tryCatchWrapper(
                context: myGlobals.homeScaffoldKey?.currentContext ?? context,
                function: myProjectsVm.getProjects(),
              );
              myProjectsVm.setBusy(false);
            }),
            child: SingleChildScrollView(
                key: const Key(Keys.myProjectsScrollView),
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      // header
                      Text(
                        tr('update_your_projects'),
                        style: Theme.of(context).textTheme.headlineLarge,
                      ),
                      SmartGaps.gapH10,
                      Text(tr('update_your_projects_desc'),
                          style: Theme.of(context).textTheme.titleSmall),
                      SmartGaps.gapH30,
                      myProjectsVm.busy
                          ? const Center(child: ConnectivityLoader())
                          : (myProjectsVm.projects.isEmpty)
                              ? const EmptyListIndicator(
                                  route: Routes.myProjects)
                              : ListView.builder(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: myProjectsVm.projects.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    final model = myProjectsVm.projects[index];
                                    final projectId = model.id ?? 0;

                                    attachmentExpansionKeyMap.putIfAbsent(
                                        projectId.toString(),
                                        () => ObjectKey(model));
                                    return MyProjectNewCard(project: model);
                                  },
                                ),
                    ],
                  ),
                )),
          );
        },
      ),
    );
  }
}

class ShareProjectToFbButton extends StatelessWidget {
  const ShareProjectToFbButton({super.key, required this.project});
  final CaseProjectModel project;

  @override
  Widget build(BuildContext context) {
    return Consumer<FacebookViewModel>(
      builder: (_, fbVm, widget) {
        if (fbVm.isIntegrated) {
          return Container(
            margin: const EdgeInsets.only(bottom: 10),
            width: double.infinity,
            height: 50,
            child: CustomDesignTheme.flatButtonStyle(
              backgroundColor: Colors.blue,
              onPressed: () async {
                /*var response = await showOkCancelAlertDialog(
                    context: context,
                    okLabel: tr('boost'),
                    cancelLabel: tr('no_thanks'),
                    title: tr('facebook_ad'),
                    message: tr('do_you_want_to_boost_it'));*/

                //if (response == OkCancelResult.ok) {
                //showPostProjectToFbAdDialog(context, project.projectid);
                //} else {
                var url =
                    'https://www.xn--hndvrker-9zan.dk/projekt/arbejdsopgaver22/${project.id}';
                await openFb(Uri.parse(url).toString());
                //}
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const FaIcon(FontAwesomeIcons.facebook, color: Colors.white),
                  SmartGaps.gapW5,
                  Flexible(
                    child: Text(
                      tr('share_your_project_on_facebook'),
                      style: Theme.of(context)
                          .textTheme
                          .titleSmall!
                          .copyWith(color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }
}

class MyProjectNewCard extends StatefulWidget {
  const MyProjectNewCard({super.key, required this.project});

  final CaseProjectModel project;

  @override
  MyProjectNewCardState createState() => MyProjectNewCardState();
}

class MyProjectNewCardState extends State<MyProjectNewCard> {
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();
  bool isActive = true;

  @override
  void initState() {
    isActive = widget.project.stage == 'active';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final createProjectVm = context.read<CreateProjectsViewModel>();
    return projectCard(widget.project, createProjectVm);
  }

  Widget projectCard(
      CaseProjectModel model, CreateProjectsViewModel createProjectVm) {
    return Dismissible(
      key: UniqueKey(),
      onDismissed: (_) async => delProject(projectId: widget.project.id ?? 0),
      child: Container(
        margin: const EdgeInsets.only(top: 10, bottom: 10),
        decoration: CustomDesignTheme.greyBorderShadows,
        child: Consumer<MyProjectsViewModel>(
          builder: (context, vm, child) {
            String picture = '';

            if ((model.pictures ?? <SupplierInfoFileModel>[]).isNotEmpty) {
              picture =
                  (model.pictures ?? <SupplierInfoFileModel>[]).first.fullURL ??
                      '';
            }

            return Container(
              padding: const EdgeInsets.all(30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // project title
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(child: projectHeader(model)),
                      // Spacer(),
                      /*Align(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        onPressed: () =>
                            {}, //delProject(projectId: model.projectid),
                        icon: Icon(ElementIcons.delete, color: Colors.red),
                      ),
                    )*/
                    ],
                  ),
                  SmartGaps.gapH20,
                  photoCard(picture, model.id.toString()),
                  SmartGaps.gapH10,
                  radios(),
                  SmartGaps.gapH10,

                  ShareProjectToFbButton(project: model),

                  Container(
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.secondary,
                    ),
                    height: 50,
                    width: double.infinity,
                    child: CustomDesignTheme.flatButtonStyle(
                      onPressed: widget.project.reviewId != ''
                          ? null
                          : () {
                              createProjectVm.currentProject = model;
                              changeDrawerRoute(Routes.projectEdit);
                            },
                      disabledBackgroundColor: Colors.grey,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          const Icon(Icons.edit, color: Colors.white),
                          SmartGaps.gapW5,
                          Text(
                            tr('edit'),
                            style: Theme.of(context)
                                .textTheme
                                .titleSmall!
                                .copyWith(
                                  color: Colors.white,
                                ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SmartGaps.gapH10,
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.red,
                        border: Border.all(color: Colors.red, width: 2)),
                    height: 50,
                    width: double.infinity,
                    child: CustomDesignTheme.flatButtonStyle(
                      onPressed: () async =>
                          delProject(projectId: widget.project.id ?? 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          const Icon(ElementIcons.delete, color: Colors.white),
                          SmartGaps.gapW5,
                          Text(
                            tr('delete'),
                            style: Theme.of(context)
                                .textTheme
                                .titleSmall!
                                .copyWith(
                                  color: Colors.white,
                                ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Future<void> delProject({required int projectId}) async {
    final viewModel = context.read<MyProjectsViewModel>();
    final response = await showOkCancelAlertDialog(
        context: context,
        message: tr('are_you_sure_you_want_to_delete_project'),
        title: tr('delete_project'),
        okLabel: tr('delete'),
        cancelLabel: tr('cancel'));

    if (!mounted) return;

    if (response == OkCancelResult.ok && mounted) {
      showLoadingDialog(context, loadingKey);

      await tryCatchWrapper(
          context: myGlobals.homeScaffoldKey?.currentContext ?? context,
          function: viewModel.deleteShowCaseProject(
            projectId: projectId,
          )).then((value) async {
        if (!mounted) return;

        Navigator.of(loadingKey.currentContext!, rootNavigator: true).pop();
        if (mounted) {
          await showSuccessAnimationDialog(
              myGlobals.homeScaffoldKey?.currentContext ?? context,
              true,
              tr('delete_project_success'));
        }
      }).catchError((onError) {
        if (!mounted) return;

        Navigator.of(loadingKey.currentContext!, rootNavigator: true).pop();
        context.showErrorSnackBar();
      });
    } else {
      setState(() {});
    }
  }

  Future<void> toggleProject({
    required bool active,
    required int projectId,
    required String stage,
  }) async {
    var viewModel = context.read<MyProjectsViewModel>();

    var response = await showOkCancelAlertDialog(
        context: context,
        title: stage == 'active'
            ? tr('show_on_profile')
            : tr('do_not_show_on_profile'),
        message: stage == 'active'
            ? tr('want_to_show_project_on_profile')
            : tr('want_to_hide_project_on_profile'),
        okLabel: tr('proceed'),
        cancelLabel: tr('cancel'));

    if (!mounted) return;

    if (response == OkCancelResult.ok && mounted) {
      showLoadingDialog(context, loadingKey);

      await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey?.currentContext ?? context,
        function:
            viewModel.updateShowCaseStatus(projectId: projectId, stage: stage),
      ).then((value) async {
        if (!mounted) return;

        Navigator.of(loadingKey.currentContext!, rootNavigator: true).pop();
        await showSuccessAnimationDialog(context, true, tr('success'));
        setState(() {
          isActive = active;
        });
      }).catchError((onError) {
        if (!mounted) return;

        Navigator.of(loadingKey.currentContext!, rootNavigator: true).pop();
        context.showErrorSnackBar();
      });
    }
  }

  Widget projectHeader(CaseProjectModel model) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Flexible(
          child: Text(
            model.title ?? '',
            overflow: TextOverflow.fade,
            style: Theme.of(context).textTheme.headlineMedium,
          ),
        ),
        SmartGaps.gapH10,
        Text(model.description ?? '',
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.titleSmall),
      ],
    );
  }

  Widget photoCard(String? imageURL, String? projectId) {
    return SizedBox(
      height: 200,
      child: CacheImage(
        imageUrl: imageURL,
      ),
    );
  }

  Widget radios() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          children: [
            SizedBox(
              width: 30,
              child: Radio(
                groupValue: isActive,
                onChanged: (dynamic value) async {
                  if (value != isActive) {
                    toggleProject(
                      active: value,
                      projectId: widget.project.projectId,
                      stage: 'active',
                    );
                  }
                },
                value: true,
              ),
            ),
            Text(
              tr('show_on_profile'),
              style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: isActive
                        ? PartnerAppColors.green
                        : PartnerAppColors.black,
                  ),
            ),
          ],
        ),
        Row(
          children: [
            SizedBox(
              width: 30,
              child: Radio(
                groupValue: isActive,
                onChanged: (dynamic value) async {
                  if (value != isActive) {
                    toggleProject(
                      active: value,
                      projectId: widget.project.projectId,
                      stage: 'deactive',
                    );
                  }
                },
                value: false,
              ),
            ),
            Text(
              tr('do_not_show_on_profile'),
              style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: isActive
                        ? PartnerAppColors.black
                        : PartnerAppColors.green,
                  ),
            ),
          ],
        ),
      ],
    );
  }
}

Future<void> openFb(String link) async {
  // try {
  //   final flutterShareMe = FlutterShareMe();
  //   flutterShareMe.shareToFacebook(msg: '', url: link);
  // } catch (e) {
  //   await launchUrlString('https://www.facebook.com/sharer/sharer.php?u=$link');
  // }
}
