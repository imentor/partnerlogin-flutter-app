import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/model/payment/payment_stages_model.dart';
import 'package:Haandvaerker.dk/model/producent/producent_model.dart';
import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/dialogs/manufacturer_subcontractor_list_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/invite_floating_button.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app_links/app_links_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/messages/message_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/payproff/payproff_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/photo_documentation_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/project_messages_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/wallet/wallet_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/request_payment/payment_viewmodel.dart';
import 'package:badges/badges.dart' as badge;
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class ProjectDashboard extends StatefulWidget {
  const ProjectDashboard(
      {super.key, required this.projectId, this.pageRedirect});

  final int projectId;
  final String? pageRedirect;

  @override
  State<ProjectDashboard> createState() => _ProjectDashboardState();
}

class _ProjectDashboardState extends State<ProjectDashboard> {
  final ibanController = TextEditingController();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final projectVm = context.read<MyProjectsViewModel>();
      // final paymentVm = context.read<PaymentViewModel>();
      final payproffVm = context.read<PayproffViewModel>();
      final appLinkVm = context.read<AppLinksViewModel>();
      final paymentViewModel = context.read<PaymentViewModel>();
      final jobsVm = context.read<ClientsViewModel>();
      final walletVm = context.read<WalletViewmodel>();

      projectVm.setBusy(true);

      await tryCatchWrapper(
          context: context,
          function: Future.wait([
            projectVm.getProjectId(
                projectid: widget.projectId, pageRedirect: widget.pageRedirect),
            payproffVm.checkPayproffAcount()
          ])).whenComplete(() async {
        if (!mounted) {
          return;
        }

        if (widget.pageRedirect == 'wallet_subcontractor') {
          await tryCatchWrapper(
              context: context,
              function: walletVm.getPaymentStageFromSubcontractor(
                  project: projectVm.activeProject));
        } else {
          await tryCatchWrapper(
              context: context,
              function: walletVm.initWallet(
                  projectId: projectVm.activeProject.id!,
                  contractId: projectVm.activeProject.contractId ?? 0));
        }

        bool isPaymentActive = false;

        final paymentStages = paymentViewModel.paymentStages;

        PaymentStageTransaction? paymentStageTransaction =
            (paymentStages.transaction ?? []).isNotEmpty
                ? paymentStages.transaction!.first
                : null;

        if (paymentStageTransaction != null) {
          isPaymentActive = (((paymentStageTransaction.amount ?? 0) -
                  (paymentStageTransaction.depositedAmount ?? 0))) !=
              (paymentStageTransaction.amount ?? 0);
        }

        projectVm.setBusy(false);

        if (appLinkVm.pageRedirect.isNotEmpty &&
            appLinkVm.pageRedirect != 'projectdashboard') {
          switch (appLinkVm.pageRedirect) {
            case 'directcontract':
            case 'contractdraft':
              bool didPartnerSigned = projectVm.activeProject.contract !=
                      null &&
                  (projectVm.activeProject.contract!['DATE_SIGNED_BY_PARTNER']
                              as String?)
                          ?.isNotEmpty ==
                      true;

              if (didPartnerSigned) {
                if (payproffVm.payproff!.exists &&
                    !payproffVm.payproff!.payproffVerified) {
                  payproffVm.ibanController = ibanController;
                  payproffVm.isPreIbanForm = true;
                  changeDrawerRoute(Routes.payproffIbanScreen);
                  appLinkVm.resetLoginAppLink();
                } else {
                  changeDrawerRoute(Routes.contractPreview);
                }
              } else {
                changeDrawerRoute(Routes.contractPreview);
              }
              break;
            case 'directcontractver':
              bool viewContract = false;

              if ((projectVm.activeProject.isVersion ?? false)) {
                final acceptedOffers = [
                  ...(projectVm.activeProject.offers ?? [])
                      .where((e) => e.status == 'accepted')
                ];
                if (acceptedOffers.isNotEmpty) {
                  viewContract =
                      (acceptedOffers.first.contractId ?? '').isNotEmpty;
                }
              } else {
                viewContract = projectVm.activeProject.contractId != null;
              }

              if (viewContract) {
                bool didPartnerSigned = projectVm.activeProject.contract !=
                        null &&
                    (projectVm.activeProject.contract!['DATE_SIGNED_BY_PARTNER']
                                as String?)
                            ?.isNotEmpty ==
                        true;

                if (didPartnerSigned) {
                  if (payproffVm.payproff!.exists &&
                      !payproffVm.payproff!.payproffVerified) {
                    payproffVm.ibanController = ibanController;
                    payproffVm.isPreIbanForm = true;
                    changeDrawerRoute(Routes.payproffIbanScreen);
                  } else {
                    changeDrawerRoute(Routes.contractPreview);
                  }
                } else {
                  changeDrawerRoute(Routes.contractPreview);
                }
              } else {
                changeDrawerRoute(Routes.contractWizard);
              }
              break;
            case 'paymentstage':
              if (isPaymentActive) {
                walletVm.currentViewedProject = projectVm.activeProject;
                changeDrawerRoute(Routes.projectWallet, arguments: true);
              }
              break;
            case 'directextrawork':
              if (isPaymentActive) {
                changeDrawerRoute(Routes.projectWalletExtraWork);
              }
              break;
            case 'uploadinvoice':
              final jobList = jobsVm.partnerJobs.where((element) =>
                  element.projectId == int.parse(appLinkVm.projectId));

              if (jobList.isEmpty) {
                modalManager.showLoadingModal();

                await jobsVm
                    .getPartnerJobByProjectId(
                        projectId: int.parse(appLinkVm.projectId))
                    .then((value) {
                  modalManager.hideLoadingModal();
                  if ((value ?? []).isNotEmpty) {
                    changeDrawerRoute(Routes.tenderFolder,
                        arguments: value!.first);
                  }
                });
              } else {
                changeDrawerRoute(Routes.tenderFolder,
                    arguments: jobList.first);
              }

              break;
            default:
          }

          appLinkVm.resetLoginAppLink();
        } else {
          appLinkVm.resetLoginAppLink();

          if (widget.pageRedirect != null) {
            switch (widget.pageRedirect) {
              case 'contract':
                bool didPartnerSigned = projectVm.activeProject.contract !=
                        null &&
                    (projectVm.activeProject.contract!['DATE_SIGNED_BY_PARTNER']
                                as String?)
                            ?.isNotEmpty ==
                        true;

                if (didPartnerSigned) {
                  if (payproffVm.payproff!.exists &&
                      !payproffVm.payproff!.payproffVerified) {
                    payproffVm.ibanController = ibanController;
                    payproffVm.isPreIbanForm = true;
                    changeDrawerRoute(Routes.payproffIbanScreen);
                  } else {
                    changeDrawerRoute(Routes.contractPreview);
                  }
                } else {
                  changeDrawerRoute(Routes.contractPreview);
                }
                break;
              case 'deficiency':
                changeDrawerRoute(Routes.projectDeficiency);
                break;
              case 'edit_offer_contract':
                bool viewContract = false;

                if ((projectVm.activeProject.isVersion ?? false)) {
                  final acceptedOffers = [
                    ...(projectVm.activeProject.offers ?? [])
                        .where((e) => e.status == 'accepted')
                  ];
                  if (acceptedOffers.isNotEmpty) {
                    viewContract =
                        (acceptedOffers.first.contractId ?? '').isNotEmpty;
                  }
                } else {
                  viewContract = projectVm.activeProject.contractId != null;
                }

                if (viewContract) {
                  bool didPartnerSigned =
                      projectVm.activeProject.contract != null &&
                          (projectVm.activeProject
                                          .contract!['DATE_SIGNED_BY_PARTNER']
                                      as String?)
                                  ?.isNotEmpty ==
                              true;

                  if (didPartnerSigned) {
                    if (payproffVm.payproff!.exists &&
                        !payproffVm.payproff!.payproffVerified) {
                      payproffVm.ibanController = ibanController;
                      payproffVm.isPreIbanForm = true;
                      changeDrawerRoute(Routes.payproffIbanScreen);
                    } else {
                      changeDrawerRoute(Routes.contractPreview);
                    }
                  } else {
                    changeDrawerRoute(Routes.contractPreview);
                  }
                } else {
                  changeDrawerRoute(Routes.contractWizard);
                }

                break;

              case 'wallet_subcontractor':
                changeDrawerRoute(Routes.projectWallet, arguments: false);

                break;
              default:
            }
          }
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      floatingActionButton: const InviteFloatingButton(),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: Consumer<MyProjectsViewModel>(
          builder: (_, vm, __) {
            return Skeletonizer(
              enabled: vm.busy,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(tr('manage_project'),
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(color: PartnerAppColors.darkBlue)),
                  SmartGaps.gapH30,
                  Text('${tr('current_project')}:',
                      style: Theme.of(context)
                          .textTheme
                          .headlineSmall!
                          .copyWith(
                              color: PartnerAppColors.grey,
                              fontWeight: FontWeight.normal)),
                  SmartGaps.gapH5,
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                            text:
                                '${vm.activeProject.address?.addressTitle ?? ''} - ',
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium!
                                .copyWith(
                                    color: PartnerAppColors.darkBlue,
                                    fontWeight: FontWeight.normal)),
                        TextSpan(
                            text: vm.activeProject.name ?? '',
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium!
                                .copyWith(
                                    color: PartnerAppColors.blue,
                                    fontWeight: FontWeight.bold))
                      ],
                    ),
                  ),
                  SmartGaps.gapH30,
                  gridView(vm: vm),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Widget gridView({required MyProjectsViewModel vm}) {
    return Consumer<WalletViewmodel>(builder: (_, walletVm, __) {
      final paymentStagesTransactions =
          walletVm.paymentStagesMain?.transaction ?? [];

      bool didPartnerSigned = false;
      bool selectOfferIsDone = false;
      bool isPaymentActive = false;

      bool isRateActive = false;

      bool isContractSignedBoth = false;

      int tenderFolderId = 0;
      int projectId = 0;
      int photoDocId = 0;
      int? contractId = 0;

      bool viewContract = false;

      if (vm.activeProject.id != null) {
        final activeProject = vm.activeProject;
        projectId = activeProject.id ?? 0;
        tenderFolderId =
            (activeProject.parentPhotoDocumentationFolderId ?? 0) == 0
                ? activeProject.photoDocumentationFolderId ?? 0
                : activeProject.parentPhotoDocumentationFolderId ?? 0;
        contractId = activeProject.contractId;
        photoDocId = ((activeProject.parentId ?? 0) != 0)
            ? activeProject.parentId ?? 0
            : activeProject.id ?? 0;

        didPartnerSigned = activeProject.contract != null &&
            (activeProject.contract!['DATE_SIGNED_BY_PARTNER'] as String?)
                    ?.isNotEmpty ==
                true;

        selectOfferIsDone = activeProject.todos!
                .firstWhere((element) => element.code == 'SELECT_AN_OFFER',
                    orElse: () => Todo.defaults())
                .isDone ??
            false;

        if (paymentStagesTransactions.isNotEmpty) {
          final transaction = paymentStagesTransactions.first;

          isPaymentActive = (((transaction.amount ?? 0) -
                  (transaction.depositedAmount ?? 0))) !=
              (transaction.amount ?? 0);
        }

        isContractSignedBoth = ((activeProject
                        .contract!['DATE_SIGNED_BY_PARTNER'] as String?) ??
                    '')
                .isNotEmpty &&
            ((activeProject.contract!['DATE_SIGNED_BY_CONTACT'] as String?) ??
                    '')
                .isNotEmpty;

        if ((vm.activeProject.todos ?? [])
            .where((element) => element.code == 'FUND_WALLET')
            .isNotEmpty) {
          isRateActive = ((vm.activeProject.todos ?? [])
                  .firstWhere((element) => element.code == 'FUND_WALLET')
                  .isDone ??
              false);
        }

        final acceptedOffers = [
          ...(vm.activeProject.offers ?? [])
              .where((e) => e.status == 'accepted')
        ];
        if (acceptedOffers.isNotEmpty) {
          viewContract = (acceptedOffers.first.contractId ?? '').isNotEmpty;
        }

        if ((widget.pageRedirect ?? '').contains('subcontractor') ||
            (widget.pageRedirect ?? '').contains('producent')) {
          isPaymentActive = true;
        }
      }

      return GridView(
        physics: const NeverScrollableScrollPhysics(),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, crossAxisSpacing: 20, mainAxisSpacing: 20),
        shrinkWrap: true,
        children: [
          if ((walletVm.paymentStagesProducent?.producent ?? []).isNotEmpty)
            producentTile((walletVm.paymentStagesProducent?.producent ?? [])),
          if ((walletVm.paymentStagesSubContractor?.subcontractors ?? [])
              .isNotEmpty)
            subcontractorTile(
                (walletVm.paymentStagesSubContractor?.subcontractors ?? [])),
          contractTile(
            isActive: (selectOfferIsDone),
            didPartnerSigned: didPartnerSigned,
            viewContract: viewContract,
            project: vm.activeProject,
          ),
          walletTile(
            isActive: isPaymentActive,
            project: vm.activeProject,
            walletVm: walletVm,
          ),
          if (!(widget.pageRedirect ?? '').contains('subcontractor'))
            extraWorkTile(
                isActive: isContractSignedBoth && isPaymentActive,
                isPaymentActive: isPaymentActive,
                projectId: projectId.toString(),
                contractId: contractId),
          filesTile(projectId: projectId),
          photoDocTile(
              tenderFolderId: tenderFolderId,
              projectId: projectId,
              photoDocId: photoDocId),
          deficiencyTile(),
          calendarTile(),
          messageTile(projectId: projectId),
          rateTile(isActive: isRateActive),
          offerTile(),
        ],
      );
    });
  }

  // Widget gridView({required MyProjectsViewModel vm}) {
  //   final paymentStages =
  //       Provider.of<PaymentViewModel>(context, listen: true).paymentStages;

  //   final projectId = vm.activeProject.id!;
  //   final tenderFolderId = vm.activeProject.contractType == 'Fagentreprise'
  //       ? vm.activeProject.parentId!
  //       : vm.activeProject.id!;

  //   bool contractIsDone = vm.activeProject.todos!
  //           .firstWhere((element) => element.code == 'SIGN_CONTRACT')
  //           .isDone ??
  //       false;

  //   bool didPartnerSigned = vm.activeProject.contract != null &&
  //       vm.activeProject.contract!['DATE_SIGNED_BY_PARTNER'] != null &&
  //       (vm.activeProject.contract!['DATE_SIGNED_BY_PARTNER'] as String)
  //           .isNotEmpty;

  //   bool selectOfferIsDone = vm.activeProject.todos!
  //           .firstWhere((element) => element.code == 'SELECT_AN_OFFER')
  //           .isDone ??
  //       false;

  //   bool isPaymentActive = contractIsDone &&
  //       paymentStages.transaction != null &&
  //       (paymentStages.transaction!.currentStatus! == 'PaymentStashed' ||
  //           paymentStages.transaction!.currentStatus! == 'PaymentReleased');

  //   return GridView(
  //     physics: const NeverScrollableScrollPhysics(),
  //     gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
  //         crossAxisCount: 2, crossAxisSpacing: 20, mainAxisSpacing: 20),
  //     shrinkWrap: true,
  //     children: [
  //       contractTile(
  //           isActive: selectOfferIsDone,
  //           didPartnerSigned: didPartnerSigned,
  //           viewContract: vm.activeProject.contractId != null),
  //       walletTile(isActive: isPaymentActive),
  //       extraWorkTile(isPaymentActive: isPaymentActive),
  //       filesTile(projectId: projectId),
  //       photoDocTile(tenderFolderId: tenderFolderId, projectId: projectId),
  //       deficiencyTile(),
  //       calendarTile(),
  //       messageTile(projectId: projectId),
  //       rateTile(),
  //     ],
  //   );
  // }

  Widget contractTile({
    required bool isActive,
    required bool viewContract,
    required bool didPartnerSigned,
    required PartnerProjectsModel project,
  }) {
    return Container(
      decoration: CustomDesignTheme.coldBlueBorderShadows.copyWith(
        color: isActive ? null : PartnerAppColors.grey.withValues(alpha: 0.3),
        border: Border.all(color: Colors.grey.withValues(alpha: 0.25)),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: InkWell(
        onTap: isActive
            ? () async {
                if ((widget.pageRedirect ?? '').contains('subcontractor')) {
                  int subContractId = 0;
                  if ((project.paymentStages ?? []).isNotEmpty) {
                    subContractId =
                        (project.paymentStages ?? []).first.subcontractorId ??
                            0;
                  }
                  changeDrawerRoute(Routes.subcontractorInviteContract,
                      arguments: {
                        'subContractId': subContractId,
                        'projectId': project.id,
                        'from': 'project_dashboard'
                      });
                } else {
                  var payproffVm = context.read<PayproffViewModel>();

                  if (viewContract) {
                    if (didPartnerSigned) {
                      if (payproffVm.payproff!.exists &&
                          !payproffVm.payproff!.payproffVerified) {
                        payproffVm.ibanController = ibanController;
                        payproffVm.isPreIbanForm = true;
                        changeDrawerRoute(Routes.payproffIbanScreen);
                      } else {
                        changeDrawerRoute(Routes.contractPreview);
                      }
                    } else {
                      changeDrawerRoute(Routes.contractPreview);
                    }
                  } else {
                    changeDrawerRoute(Routes.contractWizard);
                  }
                }
              }
            : null,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(SvgIcons.viewContract,
                colorFilter:
                    const ColorFilter.mode(Colors.black, BlendMode.srcIn),
                height: 40),
            SmartGaps.gapH10,
            Text(
              tr('contract'),
              textAlign: TextAlign.center,
              style: context.pttTitleSmall.copyWith(color: Colors.black),
            ),
            SmartGaps.gapH10,
            Text(
              tr('contract_tile_description'),
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .bodySmall!
                  .copyWith(color: PartnerAppColors.spanishGrey),
            )
          ],
        ),
      ),
    );
  }

  Widget walletTile({
    required bool isActive,
    required PartnerProjectsModel project,
    required WalletViewmodel walletVm,
  }) {
    return Container(
      decoration: CustomDesignTheme.coldBlueBorderShadows.copyWith(
        color: isActive ? null : PartnerAppColors.grey.withValues(alpha: 0.3),
        border: Border.all(color: Colors.grey.withValues(alpha: 0.25)),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: InkWell(
        onTap: () async {
          if (isActive) {
            if ((widget.pageRedirect ?? '').contains('subcontractor')) {
              tryCatchWrapper(
                  context: context,
                  function: walletVm.getPaymentStageFromSubcontractor(
                      project: project));

              changeDrawerRoute(Routes.projectWallet, arguments: false);
            } else {
              walletVm.currentViewedProject = project;
              changeDrawerRoute(Routes.projectWallet, arguments: true);
            }
          }
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(SvgIcons.walletDkk,
                colorFilter:
                    const ColorFilter.mode(Colors.black, BlendMode.srcIn),
                height: 40),
            SmartGaps.gapH10,
            Text(
              tr('byggewallet'),
              textAlign: TextAlign.center,
              style: context.pttTitleSmall.copyWith(color: Colors.black),
            ),
            SmartGaps.gapH10,
            Text(
              tr('wallet_tile_description'),
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .bodySmall!
                  .copyWith(color: PartnerAppColors.spanishGrey),
            )
          ],
        ),
      ),
    );
  }

  Widget extraWorkTile(
      {required bool isActive,
      required bool isPaymentActive,
      required String projectId,
      int? contractId}) {
    return Container(
      decoration: CustomDesignTheme.coldBlueBorderShadows.copyWith(
        color: isActive ? null : PartnerAppColors.grey.withValues(alpha: 0.3),
        border: Border.all(color: Colors.grey.withValues(alpha: 0.25)),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: InkWell(
        onTap: () {
          if (isActive) {
            changeDrawerRoute(Routes.projectWalletExtraWork);
          }
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(SvgIcons.extraWorkIcon,
                colorFilter:
                    const ColorFilter.mode(Colors.black, BlendMode.srcIn),
                height: 40),
            SmartGaps.gapH10,
            Text(
              tr('new_agreement_note'),
              textAlign: TextAlign.center,
              style: context.pttTitleSmall.copyWith(color: Colors.black),
            ),
            SmartGaps.gapH10,
            Text(
              tr('extra_work_tile_description'),
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .bodySmall!
                  .copyWith(color: PartnerAppColors.spanishGrey),
            )
          ],
        ),
      ),
    );
  }

  Widget filesTile({required int projectId}) {
    return Container(
      decoration: CustomDesignTheme.coldBlueBorderShadows.copyWith(
        border: Border.all(color: Colors.grey.withValues(alpha: 0.25)),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: InkWell(
        onTap: () {
          final jobsVm = context.read<ClientsViewModel>();

          changeDrawerRoute(Routes.tenderFolder,
              arguments: jobsVm.partnerJobs.firstWhere(
                  (element) => element.projectId == projectId,
                  orElse: () => PartnerJobModel.defaults()));
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(SvgIcons.tenderFolder,
                colorFilter:
                    const ColorFilter.mode(Colors.black, BlendMode.srcIn),
                height: 40),
            SmartGaps.gapH10,
            Text(
              tr('tender_folder'),
              textAlign: TextAlign.center,
              style: context.pttTitleSmall.copyWith(color: Colors.black),
            ),
          ],
        ),
      ),
    );
  }

  Widget photoDocTile(
      {required int tenderFolderId,
      required int projectId,
      required int photoDocId}) {
    return Container(
      decoration: CustomDesignTheme.coldBlueBorderShadows.copyWith(
        border: Border.all(color: Colors.grey.withValues(alpha: 0.25)),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: InkWell(
        onTap: () {
          final photoDocVm = context.read<PhotoDocumentationViewModel>();
          photoDocVm.initValues();
          photoDocVm.setDsocumentIds(
              folderId: tenderFolderId, projectId: photoDocId);
          changeDrawerRoute(Routes.photoDocumentation, arguments: photoDocId);
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(SvgIcons.photoDoc,
                colorFilter:
                    const ColorFilter.mode(Colors.black, BlendMode.srcIn),
                height: 40),
            SmartGaps.gapH10,
            FittedBox(
              child: Text(
                tr('photo_doc'),
                textAlign: TextAlign.center,
                style: context.pttTitleSmall.copyWith(color: Colors.black),
              ),
            ),
            SmartGaps.gapH10,
            Text(
              tr('photo_doc_tile_description'),
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .bodySmall!
                  .copyWith(color: PartnerAppColors.spanishGrey),
            )
          ],
        ),
      ),
    );
  }

  Widget deficiencyTile() {
    return Container(
      decoration: CustomDesignTheme.coldBlueBorderShadows.copyWith(
        border: Border.all(color: Colors.grey.withValues(alpha: 0.25)),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: InkWell(
        onTap: () {
          changeDrawerRoute(Routes.projectDeficiency);
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(SvgIcons.deficiency,
                colorFilter:
                    const ColorFilter.mode(Colors.black, BlendMode.srcIn),
                height: 40),
            SmartGaps.gapH10,
            Text(
              tr('deficiency_list'),
              textAlign: TextAlign.center,
              style: context.pttTitleSmall.copyWith(color: Colors.black),
            ),
            SmartGaps.gapH10,
            Text(
              tr('deficiency_tile_description'),
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .bodySmall!
                  .copyWith(color: PartnerAppColors.spanishGrey),
            )
          ],
        ),
      ),
    );
  }

  Widget calendarTile() {
    return Container(
      decoration: CustomDesignTheme.coldBlueBorderShadows.copyWith(
        border: Border.all(color: Colors.grey.withValues(alpha: 0.25)),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: InkWell(
        onTap: () {
          changeDrawerRoute(Routes.projectCalendar);
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(SvgIcons.timeline,
                colorFilter:
                    const ColorFilter.mode(Colors.black, BlendMode.srcIn),
                height: 40),
            SmartGaps.gapH10,
            Text(
              tr('calendar'),
              textAlign: TextAlign.center,
              style: context.pttTitleSmall.copyWith(color: Colors.black),
            ),
            SmartGaps.gapH10,
            Text(
              tr('calendar_tile_description'),
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .bodySmall!
                  .copyWith(color: PartnerAppColors.spanishGrey),
            )
          ],
        ),
      ),
    );
  }

  Widget messageTile({required int projectId}) {
    return Consumer<ProjectMessagesViewModel>(builder: (_, vm, __) {
      final unread =
          vm.tempMessages.where((element) => element.seen == 0).toList();

      return Container(
        decoration: CustomDesignTheme.coldBlueBorderShadows.copyWith(
          border: Border.all(color: Colors.grey.withValues(alpha: 0.25)),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: InkWell(
          onTap: () {
            context.read<MessageV2ViewModel>().defaultValues();
            changeDrawerRoute(Routes.messageScreenV2, arguments: projectId);
          },
          child: badge.Badge(
            showBadge: unread.isNotEmpty,
            badgeAnimation: const badge.BadgeAnimation.fade(),
            badgeStyle: const badge.BadgeStyle(
              shape: badge.BadgeShape.circle,
              badgeColor: Colors.red,
            ),
            position: badge.BadgePosition.topEnd(top: -2, end: -8),
            badgeContent: Center(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 2),
                child: Text(
                  '${unread.length}',
                  style: const TextStyle(color: Colors.white, fontSize: 10),
                ),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(SvgIcons.projectMessages,
                    colorFilter:
                        const ColorFilter.mode(Colors.black, BlendMode.srcIn),
                    height: 40),
                SmartGaps.gapH10,
                Text(
                  tr('project_messages'),
                  textAlign: TextAlign.center,
                  style: context.pttTitleSmall.copyWith(color: Colors.black),
                ),
                SmartGaps.gapH10,
                Text(
                  tr('project_messages_desc'),
                  textAlign: TextAlign.center,
                  style: Theme.of(context)
                      .textTheme
                      .bodySmall!
                      .copyWith(color: PartnerAppColors.spanishGrey),
                )
              ],
            ),
          ),
        ),
      );
    });
  }

  Widget rateTile({required bool isActive}) {
    return Container(
      decoration: CustomDesignTheme.coldBlueBorderShadows.copyWith(
        color: isActive ? null : PartnerAppColors.grey.withValues(alpha: 0.3),
        border: Border.all(color: Colors.grey.withValues(alpha: 0.25)),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: InkWell(
        onTap: isActive
            ? () {
                changeDrawerRoute(Routes.projectRate);
              }
            : null,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(SvgIcons.rateYourCustomer,
                colorFilter:
                    const ColorFilter.mode(Colors.black, BlendMode.srcIn),
                height: 40),
            SmartGaps.gapH10,
            Text(
              tr('rate_customer'),
              textAlign: TextAlign.center,
              style: context.pttTitleSmall.copyWith(color: Colors.black),
            ),
            SmartGaps.gapH10,
            Text(
              tr('review_tile_description'),
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .bodySmall!
                  .copyWith(color: PartnerAppColors.spanishGrey),
            )
          ],
        ),
      ),
    );
  }

  Widget offerTile() {
    return Container(
      decoration: CustomDesignTheme.coldBlueBorderShadows.copyWith(
        border: Border.all(color: Colors.grey.withValues(alpha: 0.25)),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: InkWell(
        onTap: () {
          changeDrawerRoute(Routes.projectViewOffer);
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(SvgIcons.createOfferIconUrl,
                colorFilter:
                    const ColorFilter.mode(Colors.black, BlendMode.srcIn),
                height: 40),
            SmartGaps.gapH10,
            Text(
              tr('offer'),
              textAlign: TextAlign.center,
              style: context.pttTitleSmall.copyWith(color: Colors.black),
            ),
            SmartGaps.gapH10,
            Text(
              tr('offer_tile_description'),
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .bodySmall!
                  .copyWith(color: PartnerAppColors.spanishGrey),
            )
          ],
        ),
      ),
    );
  }

  Widget producentTile(List<Producent> producents) {
    return Container(
      decoration: CustomDesignTheme.coldBlueBorderShadows.copyWith(
        border: Border.all(color: Colors.grey.withValues(alpha: 0.25)),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: InkWell(
        onTap: () {
          manufacturerSubcontractorListDialog(
              context: context, list: producents, type: 'producent');
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(SvgIcons.producentIcon,
                colorFilter:
                    const ColorFilter.mode(Colors.black, BlendMode.srcIn),
                height: 40),
            SmartGaps.gapH10,
            Text(
              'Leverandør',
              textAlign: TextAlign.center,
              style: context.pttTitleSmall.copyWith(color: Colors.black),
            ),
            SmartGaps.gapH10,
            Text(
              'Her får du overblik over dine betalinger og transaktioner',
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .bodySmall!
                  .copyWith(color: PartnerAppColors.spanishGrey),
            )
          ],
        ),
      ),
    );
  }

  Widget subcontractorTile(List<Producent> subcontractors) {
    return Container(
      decoration: CustomDesignTheme.coldBlueBorderShadows.copyWith(
        border: Border.all(color: Colors.grey.withValues(alpha: 0.25)),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: InkWell(
        onTap: () {
          manufacturerSubcontractorListDialog(
              context: context, list: subcontractors, type: 'subcontractor');
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(SvgIcons.partnerIcon,
                colorFilter:
                    const ColorFilter.mode(Colors.black, BlendMode.srcIn),
                height: 40),
            SmartGaps.gapH10,
            Text(
              'Subcontractor',
              textAlign: TextAlign.center,
              style: context.pttTitleSmall.copyWith(color: Colors.black),
            ),
            SmartGaps.gapH10,
            Text(
              'Her får du overblik over dine betalinger og transaktioner',
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .bodySmall!
                  .copyWith(color: PartnerAppColors.spanishGrey),
            )
          ],
        ),
      ),
    );
  }
}
