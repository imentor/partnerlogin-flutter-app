import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/signature_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/components/customer_info.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/components/timeline_step.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/offer_types/create_offer/step_2/create_offer_form_step_2.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/offer_types/create_offer/step_3/create_offer_form_step_3.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/offer_types/create_offer/step_4/create_offer_form_step_4.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/offer_types/create_offer/step_5/create_offer_form_step_5.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/offer_types/quick_offer/step_2/quick_offer_form_step_2.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/offer_types/quick_offer/step_3/quick_offer_form_step_3.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/offer_types/select_invite_type.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/offer_types/upload_offer/step_2/upload_offer_form_step_2.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/offer_types/upload_offer/step_3/upload_offer_form_step_3.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/offer_types/upload_offer/step_4/upload_offer_form_step_4.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/offer_types/upload_offer/step_5/upload_offer_form_step_5.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/offer_types/upload_offer/step_6/upload_offer_form_step_6.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/offer_types/upload_offer/step_7/upload_offer_form_step_7.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class InviteCustomerScreen extends StatefulWidget {
  const InviteCustomerScreen({super.key});

  @override
  State<InviteCustomerScreen> createState() => _InviteCustomerScreenState();
}

class _InviteCustomerScreenState extends State<InviteCustomerScreen> {
  final formKey = GlobalKey<FormBuilderState>();
  final CurrencyTextInputFormatter currencyFormatter =
      CurrencyTextInputFormatter(
          NumberFormat.currency(locale: 'da', decimalDigits: 2, symbol: ''));
  late PageController pageController;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final inviteCustomerVm = context.read<InviteCustomerViewmodel>();

      pageController = PageController(
          initialPage: inviteCustomerVm.currentProductQuestionnaireStep);

      await tryCatchWrapper(context: context, function: inviteCustomerVm.init())
          .whenComplete(() {
        inviteCustomerVm.setBusy(false);
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<InviteCustomerViewmodel>(
        builder: (_, inviteCustomerVm, __) {
      return Scaffold(
        appBar: DrawerAppBar(
          overrideBackFunction: () => onBack(inviteCustomerVm),
        ),
        bottomNavigationBar: buttonNav(inviteCustomerVm),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: FormBuilder(
            key: formKey,
            clearValueOnUnregister: true,
            child: Skeletonizer(
              enabled: inviteCustomerVm.busy,
              child: Column(
                children: [formSteps(inviteCustomerVm)],
              ),
            ),
          ),
        ),
      );
    });
  }

  Widget formSteps(InviteCustomerViewmodel inviteCustomerVm) {
    if (inviteCustomerVm.currentStep == 0) {
      return SelectInviteType(
          inviteCustomerVm: inviteCustomerVm, formKey: formKey);
    } else {
      switch (
          inviteCustomerVm.formValues[InviteCustomerFormKeys.invitationType]) {
        case InviteCustomerType.createOffer:
          return createOfferSteps(
              inviteCustomerVm: inviteCustomerVm,
              currentStep: (inviteCustomerVm.currentStep));
        case InviteCustomerType.uploadOffer:
          return uploadOfferSteps(
              inviteCustomerVm: inviteCustomerVm,
              currentStep: (inviteCustomerVm.currentStep));
        case InviteCustomerType.quickOffer:
          return quickOfferSteps(
              inviteCustomerVm: inviteCustomerVm,
              currentStep: (inviteCustomerVm.currentStep));
        default:
          return const SizedBox.shrink();
      }
    }
  }

  Widget createOfferSteps(
      {required InviteCustomerViewmodel inviteCustomerVm,
      required double currentStep}) {
    Widget widget = const SizedBox.shrink();

    switch (currentStep) {
      case 1:
        widget = CustomerInfo(
          inviteCustomerVm: inviteCustomerVm,
          formKey: formKey,
          title: tr('create_extended_offer'),
        );
        break;
      case 2:
      case 2.1:
        widget = CreateOfferFormStep2(
          inviteCustomerVm: inviteCustomerVm,
          pageController: pageController,
        );
        break;

      case 3:
        widget = CreateOfferFormStep3(
          inviteCustomerVm: inviteCustomerVm,
          formKey: formKey,
          currencyFormatter: currencyFormatter,
        );
        break;

      case 4:
        widget = CreateOfferFormStep4(
          inviteCustomerVm: inviteCustomerVm,
        );
        break;

      case 5:
        widget = CreateOfferFormStep5(
          inviteCustomerVm: inviteCustomerVm,
          formKey: formKey,
        );
        break;

      default:
        widget = const SizedBox.shrink();
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        if (inviteCustomerVm.currentStep < 5)
          TimelineStep(inviteCustomerVm: inviteCustomerVm, totalSteps: 4),
        widget
      ],
    );
  }

  Widget uploadOfferSteps(
      {required InviteCustomerViewmodel inviteCustomerVm,
      required double currentStep}) {
    Widget widget = const SizedBox.shrink();

    switch (currentStep) {
      case 1:
        widget = CustomerInfo(
          inviteCustomerVm: inviteCustomerVm,
          formKey: formKey,
          title: tr('create_file_upload'),
        );

        break;

      case 2:
        widget = UploadOfferFormStep2(inviteCustomerVm: inviteCustomerVm);
        break;

      case 3:
        widget = UploadOfferFormStep3(
          inviteCustomerVm: inviteCustomerVm,
          formKey: formKey,
          currencyFormatter: currencyFormatter,
        );
        break;

      case 4:
        widget = UploadOfferFormStep4(inviteCustomerVm: inviteCustomerVm);
        break;

      case 5:
        widget = UploadOfferFormStep5(
          inviteCustomerVm: inviteCustomerVm,
          formKey: formKey,
          currencyFormatter: currencyFormatter,
        );
        break;

      case 6:
        widget = UploadOfferFormStep6(
          inviteCustomerVm: inviteCustomerVm,
        );
        break;

      case 7:
        widget = UploadOfferFormStep7(inviteCustomerVm: inviteCustomerVm);
        break;

      default:
        widget = const SizedBox.shrink();
        break;
    }

    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          if (inviteCustomerVm.currentStep < 7)
            TimelineStep(inviteCustomerVm: inviteCustomerVm, totalSteps: 6),
          widget
        ]);
  }

  Widget quickOfferSteps(
      {required InviteCustomerViewmodel inviteCustomerVm,
      required double currentStep}) {
    switch (currentStep) {
      case 1:
        return CustomerInfo(
          inviteCustomerVm: inviteCustomerVm,
          formKey: formKey,
          title: tr('create_offers_quickly'),
        );

      case 2:
        return QuickOfferFormStep2(
          inviteCustomerVm: inviteCustomerVm,
          formKey: formKey,
          currencyFormatter: currencyFormatter,
        );

      case 3:
        return QuickOfferFormStep3(inviteCustomerVm: inviteCustomerVm);

      default:
        return const SizedBox.shrink();
    }
  }

  Widget buttonNav(InviteCustomerViewmodel inviteCustomerVm) {
    final appDrawerVm = context.read<AppDrawerViewModel>();
    final termsVm = context.read<SharedDataViewmodel>();
    final userVm = context.read<UserViewModel>();

    if (inviteCustomerVm.currentStep == 0) {
      return const SizedBox.shrink();
    }

    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextButton(
            style: TextButton.styleFrom(
                fixedSize: Size(MediaQuery.of(context).size.width, 45),
                backgroundColor: PartnerAppColors.malachite),
            child: Text(
              buttonName(inviteCustomerVm),
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium!
                  .copyWith(color: Colors.white),
            ),
            onPressed: () => onNext(
                appDrawerVm: appDrawerVm,
                inviteCustomerVm: inviteCustomerVm,
                termsVm: termsVm,
                userVm: userVm),
          )
        ],
      ),
    );
  }

  String buttonName(InviteCustomerViewmodel inviteCustomerVm) {
    if (inviteCustomerVm.formValues[InviteCustomerFormKeys.invitationType] ==
            InviteCustomerType.createOffer &&
        inviteCustomerVm.currentStep == 6) {
      return tr('done');
    } else if (inviteCustomerVm
                .formValues[InviteCustomerFormKeys.invitationType] ==
            InviteCustomerType.uploadOffer &&
        inviteCustomerVm.currentStep == 8) {
      return tr('done');
    } else if (inviteCustomerVm
            .formValues[InviteCustomerFormKeys.invitationType] ==
        InviteCustomerType.quickOffer) {
      if (inviteCustomerVm.currentStep == 2) {
        return tr('preview');
      } else if (inviteCustomerVm.currentStep == 3) {
        return tr('send_to_customer');
      } else {
        return tr('next');
      }
    } else {
      return tr('next');
    }
  }

  Future<void> onNext(
      {required AppDrawerViewModel appDrawerVm,
      required SharedDataViewmodel termsVm,
      required InviteCustomerViewmodel inviteCustomerVm,
      required UserViewModel userVm}) async {
    if (!inviteCustomerVm.busy) {
      final bool isAffiliate =
          (appDrawerVm.bitrixCompanyModel?.companyPackge ?? '') ==
              'affiliatepartner';

      if (inviteCustomerVm.currentStep == 0) {
        if (formKey.currentState!.validate()) {
          for (var element in formKey.currentState!.fields.entries) {
            inviteCustomerVm.updateFormValues(
                key: element.key, values: element.value.value);
          }

          if (inviteCustomerVm
                  .formValues[InviteCustomerFormKeys.invitationType] ==
              InviteCustomerType.quickOffer) {
            await inviteCustomerVm.initQuickOffer(
                companyName: userVm.userModel?.user?.name ?? '');
          } else {
            inviteCustomerVm.currentStep += 1;
          }
        }
      } else {
        switch (inviteCustomerVm
            .formValues[InviteCustomerFormKeys.invitationType]) {
          case InviteCustomerType.createOffer:
            onNextCreateOffer(
                appDrawerVm: appDrawerVm,
                termsVm: termsVm,
                inviteCustomerVm: inviteCustomerVm,
                isAffiliate: isAffiliate);
            break;
          case InviteCustomerType.uploadOffer:
            onNextUploadOffer(
                appDrawerVm: appDrawerVm,
                termsVm: termsVm,
                inviteCustomerVm: inviteCustomerVm,
                isAffiliate: isAffiliate);
            break;
          case InviteCustomerType.quickOffer:
            onNextQuickOffer(
                appDrawerVm: appDrawerVm,
                termsVm: termsVm,
                inviteCustomerVm: inviteCustomerVm,
                isAffiliate: isAffiliate);
            break;
          default:
        }
      }
    }
  }

  Future<void> onNextQuickOffer(
      {required AppDrawerViewModel appDrawerVm,
      required SharedDataViewmodel termsVm,
      required InviteCustomerViewmodel inviteCustomerVm,
      required bool isAffiliate}) async {
    switch (inviteCustomerVm.currentStep) {
      case 0:
      case 1:
        if (formKey.currentState!.validate()) {
          for (var element in formKey.currentState!.fields.entries) {
            inviteCustomerVm.updateFormValues(
                key: element.key, values: element.value.value);
          }

          inviteCustomerVm.currentStep += 1;
        }
        break;

      case 2:
        if (formKey.currentState!.validate()) {
          if ((inviteCustomerVm
                      .formValues[InviteCustomerFormKeys.quickOfferPriceDetails]
                  as List<Map<String, dynamic>>)
              .any((value) => (value['price'] as double) <= 0)) {
            showOkAlertDialog(
                context: context, message: tr('please_enter_correct_price'));
          } else {
            if (inviteCustomerVm.formValues[
                    InviteCustomerFormKeys.industryTotalPriceWoVat] <
                800) {
              showOkAlertDialog(
                  context: context,
                  message: tr('total_price_not_less_than_800'));
            } else {
              for (var element in formKey.currentState!.fields.entries) {
                inviteCustomerVm.updateFormValues(
                    key: element.key,
                    values: element.value.value is DateTime
                        ? (element.value.value as DateTime)
                            .toString()
                            .split(' ')
                            .first
                        : element.value.value);
              }

              await tryCatchWrapper(
                context: context,
                function: inviteCustomerVm.previewOfferPayload(
                  isAffiliate: isAffiliate,
                  companyIndustry:
                      appDrawerVm.bitrixCompanyModel?.primaryIndustryId ?? '',
                ),
              ).whenComplete(() {
                inviteCustomerVm.setBusy(false);
              });
            }
          }
        }
        break;

      case 3:
        final signature = await signatureDialog(
            context: context,
            label: termsVm.termsLabel,
            htmlTerms: termsVm.termsHtml,
            buttonLabel: tr('done_send_offer'));

        if (!mounted) return;

        if (signature != null) {
          Future<bool?>? function;

          if (inviteCustomerVm
                  .formValues[InviteCustomerFormKeys.recipientType] ==
              InviteCustomerFormKeys.recipientTypeNew) {
            function = inviteCustomerVm.createQuickOffer(
                isAffiliate: isAffiliate,
                signature: signature,
                companyIndustry:
                    appDrawerVm.bitrixCompanyModel?.primaryIndustryId ?? '');
          } else {
            function = inviteCustomerVm.quickOfferExistingProject(
                companyId:
                    context.read<UserViewModel>().userModel?.user?.id ?? 0,
                signature: signature,
                isAffiliate: isAffiliate,
                companyIndustry:
                    appDrawerVm.bitrixCompanyModel?.primaryIndustryId ?? '');
          }

          await tryCatchWrapper(context: context, function: function)
              .then((value) async {
            if (!mounted) return;

            inviteCustomerVm.setBusy(false);

            await commonSuccessOrFailedDialog(
                    context: context,
                    isSuccess: value ?? false,
                    message: (value ?? false)
                        ? tr('offer_is_sent_to_customer')
                        : null)
                .whenComplete(() async {
              inviteCustomerVm.reset();
              backDrawerRoute();

              await appDrawerVm.getPrisenResult();
            });
          });
        }
        break;
      default:
    }
  }

  Future<void> onNextUploadOffer(
      {required AppDrawerViewModel appDrawerVm,
      required SharedDataViewmodel termsVm,
      required InviteCustomerViewmodel inviteCustomerVm,
      required bool isAffiliate}) async {
    switch (inviteCustomerVm.currentStep) {
      case 0:
      case 1:
      case 6:
        if (formKey.currentState!.validate()) {
          for (var element in formKey.currentState!.fields.entries) {
            inviteCustomerVm.updateFormValues(
                key: element.key, values: element.value.value);
          }

          inviteCustomerVm.currentStep += 1;
        }
        break;
      case 2:
        if (formKey.currentState!.validate()) {
          for (var element in formKey.currentState!.fields.entries) {
            inviteCustomerVm.updateFormValues(
                key: element.key, values: element.value.value);
          }

          await tryCatchWrapper(
                  context: context,
                  function: inviteCustomerVm.mergeQuickOfferFiles())
              .whenComplete(() {
            inviteCustomerVm.setBusy(false);
          });
        }
        break;
      case 3:
        if (formKey.currentState!.validate()) {
          for (var element in formKey.currentState!.fields.entries) {
            inviteCustomerVm.updateFormValues(
                key: element.key, values: element.value.value);
          }

          if (inviteCustomerVm
                  .formValues[InviteCustomerFormKeys.recipientType] ==
              InviteCustomerFormKeys.recipientTypeNew) {
            inviteCustomerVm.currentStep += 1;
          } else {
            inviteCustomerVm.updateUploadIndustryPrices();
          }
        }
        break;
      case 4:
        if ((formKey
                .currentState!
                .fields[InviteCustomerFormKeys.selectedIndustry]!
                .value as List<AllWizardText>)
            .isEmpty) {
          formKey.currentState!.fields[InviteCustomerFormKeys.selectedIndustry]!
              .invalidate('');
        } else {
          for (var element in formKey.currentState!.fields.entries) {
            inviteCustomerVm.updateFormValues(
                key: element.key, values: element.value.value);
          }

          await tryCatchWrapper(
                  context: context, function: inviteCustomerVm.getListPrice())
              .whenComplete(() {
            inviteCustomerVm.setBusy(false);
          });
        }
        break;
      case 5:
        if ((inviteCustomerVm
                .formValues[InviteCustomerFormKeys.industryTotalPriceWoVat]) !=
            (inviteCustomerVm.formValues[
                InviteCustomerFormKeys.tempIndustryTotalPriceWoVat])) {
          showOkAlertDialog(
              context: context,
              message:
                  '${tr('price_must_be_equal_to_project_price')} ${Formatter.curencyFormat(amount: (inviteCustomerVm.formValues[InviteCustomerFormKeys.tempIndustryTotalPriceWoVat]))}');
        } else {
          if ((inviteCustomerVm
                      .formValues[InviteCustomerFormKeys.industryDescriptions]
                  as List<Map<String, dynamic>>)
              .expand((value) => value['subIndustry'])
              .any((value) => (value['price'] as double) <= 0)) {
            showOkAlertDialog(
                context: context, message: tr('please_enter_correct_price'));
          } else {
            if (inviteCustomerVm.formValues[
                    InviteCustomerFormKeys.industryTotalPriceWoVat] <
                800) {
              showOkAlertDialog(
                  context: context,
                  message: tr('total_price_not_less_than_800'));
            } else {
              inviteCustomerVm.currentStep += 1;
            }
          }
        }
        break;

      case 7:
        final signature = await signatureDialog(
            context: context,
            label: termsVm.termsLabel,
            htmlTerms: termsVm.termsHtml,
            buttonLabel: tr('done_send_offer'));

        if (!mounted) return;

        if (signature != null) {
          Future<bool>? function;

          if (inviteCustomerVm
                  .formValues[InviteCustomerFormKeys.recipientType] ==
              InviteCustomerFormKeys.recipientTypeNew) {
            function = inviteCustomerVm.submitUploadOffer(
                signature: signature, isAffiliate: isAffiliate);
          } else {
            function = inviteCustomerVm.uploadOfferExistingProject(
                companyId:
                    context.read<UserViewModel>().userModel?.user?.id ?? 0,
                signature: signature,
                isAffiliate: isAffiliate);
          }

          await tryCatchWrapper(context: context, function: function)
              .then((value) {
            if (!mounted) return;

            inviteCustomerVm.setBusy(false);

            commonSuccessOrFailedDialog(
                    context: context,
                    isSuccess: value ?? false,
                    message: (value ?? false)
                        ? tr('offer_is_sent_to_customer')
                        : null)
                .whenComplete(() {
              inviteCustomerVm.reset();
              backDrawerRoute();
            });
          });
        }
        break;
      default:
        break;
    }
  }

  Future<void> onNextCreateOffer(
      {required AppDrawerViewModel appDrawerVm,
      required SharedDataViewmodel termsVm,
      required InviteCustomerViewmodel inviteCustomerVm,
      required bool isAffiliate}) async {
    switch (inviteCustomerVm.currentStep) {
      case 0:
        if (formKey.currentState!.validate()) {
          for (var element in formKey.currentState!.fields.entries) {
            inviteCustomerVm.updateFormValues(
                key: element.key, values: element.value.value);
          }

          inviteCustomerVm.currentStep += 1;
        }
        break;

      case 1:
        if (formKey.currentState!.validate()) {
          for (var element in formKey.currentState!.fields.entries) {
            inviteCustomerVm.updateFormValues(
                key: element.key, values: element.value.value);
          }

          inviteCustomerVm.currentStep += 1;
        }
        break;
      case 2:
        if ((formKey
                .currentState!
                .fields[InviteCustomerFormKeys.selectedIndustry]!
                .value as List<AllWizardText>)
            .isEmpty) {
          formKey.currentState!.fields[InviteCustomerFormKeys.selectedIndustry]!
              .invalidate('');
        } else {
          for (var element in formKey.currentState!.fields.entries) {
            inviteCustomerVm.updateFormValues(
                key: element.key, values: element.value.value);
          }

          await tryCatchWrapper(
                  context: context,
                  function: inviteCustomerVm.getBookerWizardQuestions())
              .whenComplete(() {
            inviteCustomerVm.setBusy(false);
          });
        }
        break;

      case 2.1:
        if (formKey.currentState!.validate()) {
          for (var element in formKey.currentState!.fields.entries) {
            if (element.key.contains('checkbox')) {
              if (element.value.value is List) {
                for (final val in element.value.value as List) {
                  inviteCustomerVm.updateWizardProductAnswers(
                      key: '${element.key}$val', value: '$val');
                }
              }
            } else {
              inviteCustomerVm.updateWizardProductAnswers(
                  key: element.key, value: '${element.value.value}');
            }
          }

          if (inviteCustomerVm.currentProductQuestionnaireStep ==
              inviteCustomerVm.productQuestionnaires.length - 1) {
            await tryCatchWrapper(
                    context: context,
                    function: inviteCustomerVm.submitBookerWizardAnswers())
                .whenComplete(() {
              inviteCustomerVm.setBusy(false);
            });
          } else {
            inviteCustomerVm.currentProductQuestionnaireStep += 1;
            pageController.animateToPage(
              inviteCustomerVm.currentProductQuestionnaireStep,
              duration: const Duration(milliseconds: 800),
              curve: Curves.easeOutQuart,
            );
          }
        }
        break;
      case 3:
        if ((inviteCustomerVm
                    .formValues[InviteCustomerFormKeys.industryDescriptions]
                as List<Map<String, dynamic>>)
            .expand((value) => value['subIndustry'])
            .any((value) => (value['price'] as double) <= 0)) {
          showOkAlertDialog(
              context: context, message: tr('please_enter_correct_price'));
        } else {
          if (inviteCustomerVm
                  .formValues[InviteCustomerFormKeys.industryTotalPriceWoVat] <
              800) {
            showOkAlertDialog(
                context: context, message: tr('total_price_not_less_than_800'));
          } else {
            inviteCustomerVm.currentStep += 1;
          }
        }
        break;

      case 4:
        if (formKey.currentState!.validate()) {
          if (inviteCustomerVm
                  .formValues[InviteCustomerFormKeys.recipientType] ==
              InviteCustomerFormKeys.recipientTypeNew) {
            for (var element in formKey.currentState!.fields.entries) {
              inviteCustomerVm.updateFormValues(
                  key: element.key, values: element.value.value);
            }

            inviteCustomerVm.currentStep += 1;

            await tryCatchWrapper(
                    context: context,
                    function: inviteCustomerVm.createProject(
                        isAffiliate: (appDrawerVm.supplierProfileModel
                                    ?.bCrmCompany?.companyPackge ??
                                '') ==
                            'affiliatepartner'))
                .whenComplete(() {
              inviteCustomerVm.setBusy(false);
            });
          } else {
            for (var element in formKey.currentState!.fields.entries) {
              inviteCustomerVm.updateFormValues(
                  key: element.key, values: element.value.value);
            }

            inviteCustomerVm.currentStep += 1;
          }
        }
        break;
      case 5:
        final signature = await signatureDialog(
            context: context,
            label: termsVm.termsLabel,
            htmlTerms: termsVm.termsHtml,
            buttonLabel: tr('done_send_offer'));

        if (!mounted) return;

        if (signature != null) {
          Future<bool>? function;

          if (inviteCustomerVm
                  .formValues[InviteCustomerFormKeys.recipientType] ==
              InviteCustomerFormKeys.recipientTypeNew) {
            function = inviteCustomerVm.submitOffer(
                isAffiliate: isAffiliate, signature: signature);
          } else {
            function = inviteCustomerVm.createOfferExistingProject(
                companyId:
                    context.read<UserViewModel>().userModel?.user?.id ?? 0,
                signature: signature,
                isAffiliate: isAffiliate);
          }

          await tryCatchWrapper(context: context, function: function)
              .then((value) async {
            if (!mounted) return;

            inviteCustomerVm.setBusy(false);

            await commonSuccessOrFailedDialog(
                    context: context,
                    isSuccess: value ?? false,
                    message: (value ?? false)
                        ? tr('offer_is_sent_to_customer')
                        : null)
                .whenComplete(() async {
              inviteCustomerVm.reset();
              backDrawerRoute();

              await appDrawerVm.getPrisenResult();
            });
          });
        }

        break;
      default:
    }
  }

  Future<void> onBack(InviteCustomerViewmodel inviteCustomerVm) async {
    if (!inviteCustomerVm.busy) {
      switch (
          inviteCustomerVm.formValues[InviteCustomerFormKeys.invitationType]) {
        case InviteCustomerType.createOffer:
          onBackCreateOffer(inviteCustomerVm);
          break;
        case InviteCustomerType.uploadOffer:
          onBackUploadOffer(inviteCustomerVm);
          break;
        case InviteCustomerType.quickOffer:
          onBackQuickOffer(inviteCustomerVm);
          break;
        default:
          backDrawerRoute();
      }
    }
  }

  Future<void> onBackQuickOffer(
      InviteCustomerViewmodel inviteCustomerVm) async {
    switch (inviteCustomerVm.currentStep) {
      case 0:
        backDrawerRoute();
        break;
      case 1:
        inviteCustomerVm.currentStep = inviteCustomerVm.currentStep - 1;
        inviteCustomerVm.reset();
        break;
      case 2:
      case 3:
      case 4:
      case 5:
        inviteCustomerVm.currentStep = inviteCustomerVm.currentStep - 1;
        break;
    }
  }

  Future<void> onBackUploadOffer(
      InviteCustomerViewmodel inviteCustomerVm) async {
    switch (inviteCustomerVm.currentStep) {
      case 0:
        backDrawerRoute();
        break;
      case 1:
        inviteCustomerVm.currentStep = inviteCustomerVm.currentStep - 1;
        inviteCustomerVm.reset();
        break;
      case 3:
        inviteCustomerVm.currentStep = inviteCustomerVm.currentStep - 1;
        inviteCustomerVm.resetPrices();
        break;
      case 5:
        inviteCustomerVm.currentStep = 3;
        break;
      case 2:
        if (inviteCustomerVm.formValues[InviteCustomerFormKeys.recipientType] ==
            InviteCustomerFormKeys.recipientTypeNew) {
          inviteCustomerVm.currentStep = inviteCustomerVm.currentStep - 1;
        } else {
          inviteCustomerVm.currentStep = 0;
        }
        break;
      case 4:
      case 6:
      case 7:
      case 8:
        inviteCustomerVm.currentStep = inviteCustomerVm.currentStep - 1;
        break;
    }
  }

  Future<void> onBackCreateOffer(
      InviteCustomerViewmodel inviteCustomerVm) async {
    switch (inviteCustomerVm.currentStep) {
      case 0:
        backDrawerRoute();
        break;
      case 1:
        inviteCustomerVm.currentStep = inviteCustomerVm.currentStep - 1;
        inviteCustomerVm.reset();
        break;
      case 3:
        if (inviteCustomerVm.formValues[InviteCustomerFormKeys.recipientType] ==
            InviteCustomerFormKeys.recipientTypeNew) {
          inviteCustomerVm.currentStep = inviteCustomerVm.currentStep - 1;
        } else {
          inviteCustomerVm.currentStep = 0;
          inviteCustomerVm.reset();
        }
        break;
      case 2:
      case 2.1:
      case 4:
      case 5:
      case 6:
        inviteCustomerVm.currentStep = inviteCustomerVm.currentStep - 1;

        break;
      default:
    }
  }
}
