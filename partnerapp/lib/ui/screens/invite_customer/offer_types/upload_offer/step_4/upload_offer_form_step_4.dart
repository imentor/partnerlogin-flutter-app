import 'package:Haandvaerker.dk/ui/screens/invite_customer/components/dropdown_select_job_industry.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class UploadOfferFormStep4 extends StatelessWidget {
  const UploadOfferFormStep4({super.key, required this.inviteCustomerVm});

  final InviteCustomerViewmodel inviteCustomerVm;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('select_subjects'),
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        const SizedBox(
          height: 10,
        ),
        DropdownSelectJobIndustry(inviteCustomerVm: inviteCustomerVm),
      ],
    );
  }
}
