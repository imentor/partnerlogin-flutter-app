import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class UploadOfferFormStep7 extends StatefulWidget {
  const UploadOfferFormStep7({
    super.key,
    required this.inviteCustomerVm,
  });

  final InviteCustomerViewmodel inviteCustomerVm;

  @override
  UploadOfferFormStep7State createState() => UploadOfferFormStep7State();
}

class UploadOfferFormStep7State extends State<UploadOfferFormStep7> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('summary'),
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          tr('project_name'),
          style: Theme.of(context)
              .textTheme
              .titleSmall!
              .copyWith(color: PartnerAppColors.spanishGrey),
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          widget.inviteCustomerVm
                  .formValues[InviteCustomerFormKeys.projectName] ??
              '',
          style: Theme.of(context).textTheme.titleMedium,
        ),
        const SizedBox(
          height: 20,
        ),
        Container(
          padding: const EdgeInsets.all(10),
          color: PartnerAppColors.spanishGrey.withValues(alpha: .2),
          height: MediaQuery.of(context).size.height / 2,
          width: MediaQuery.of(context).size.width,
          child: SfPdfViewer.file(
            widget.inviteCustomerVm
                .formValues[InviteCustomerFormKeys.uploadOfferMergeFile],
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        ...(widget.inviteCustomerVm
                    .formValues[InviteCustomerFormKeys.industryDescriptions]
                as List<Map<String, dynamic>>)
            .map((value) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              ...(value['subIndustry'] as List<Map<String, dynamic>>)
                  .map((subIndustryMap) {
                return Container(
                  margin: const EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '${subIndustryMap['subIndustryName']} (${subIndustryMap['productTypeName']})',
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(fontWeight: FontWeight.normal),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Text(
                        Formatter.curencyFormat(amount: subIndustryMap['price'])
                            .trim(),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(fontWeight: FontWeight.normal),
                      ),
                    ],
                  ),
                );
              })
            ],
          );
        }),
        const Divider(
          color: PartnerAppColors.darkBlue,
        ),
        const SizedBox(
          height: 5,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('unit_price_ex_vat'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: widget.inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalPriceWoVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('vat_value'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: widget.inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('total_with_vat'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: widget.inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalPriceVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
        const SizedBox(
          height: 5,
        ),
        const Divider(
          color: PartnerAppColors.darkBlue,
        ),
        const SizedBox(
          height: 5,
        ),
        Text(
          tr('project_day_estimate'),
          style: Theme.of(context).textTheme.titleMedium,
        ),
        const SizedBox(
          height: 10,
        ),
        RichText(
          text: TextSpan(
            children: [
              const WidgetSpan(
                  child: Icon(
                    FeatherIcons.calendar,
                    color: PartnerAppColors.blue,
                  ),
                  alignment: PlaceholderAlignment.middle),
              TextSpan(
                  text:
                      ' ${widget.inviteCustomerVm.formValues[InviteCustomerFormKeys.projectEstimate]} ${tr('days')}',
                  style: Theme.of(context)
                      .textTheme
                      .titleSmall!
                      .copyWith(fontWeight: FontWeight.bold))
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        Text(
          tr('project_start_date'),
          style: Theme.of(context).textTheme.titleMedium,
        ),
        const SizedBox(
          height: 10,
        ),
        RichText(
          text: TextSpan(
            children: [
              const WidgetSpan(
                  child: Icon(
                    FeatherIcons.calendar,
                    color: PartnerAppColors.blue,
                  ),
                  alignment: PlaceholderAlignment.middle),
              TextSpan(
                  text:
                      ' ${DateFormatType.fullMonthDate.formatter.format(widget.inviteCustomerVm.formValues[InviteCustomerFormKeys.projectStartDate] as DateTime)}',
                  style: Theme.of(context)
                      .textTheme
                      .titleSmall!
                      .copyWith(fontWeight: FontWeight.bold))
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        RichText(
          text: TextSpan(
            text: '${tr('your_offer_is_valid')} ',
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                letterSpacing: 1.0,
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.normal,
                fontSize: 18),
            children: [
              TextSpan(
                  text:
                      '${widget.inviteCustomerVm.formValues[InviteCustomerFormKeys.offerValidity] ?? ''}',
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.bold,
                      fontSize: 18)),
              TextSpan(
                  text: ' ${tr('days_valid')}',
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 18)),
            ],
          ),
        ),
      ],
    );
  }
}
