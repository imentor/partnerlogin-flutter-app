import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/debouncer.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class UploadOfferFormStep5 extends StatelessWidget {
  UploadOfferFormStep5(
      {super.key,
      required this.inviteCustomerVm,
      required this.formKey,
      required this.currencyFormatter});

  final InviteCustomerViewmodel inviteCustomerVm;
  final GlobalKey<FormBuilderState> formKey;
  final Debouncer debouncer = Debouncer(milliseconds: 500);
  final CurrencyTextInputFormatter currencyFormatter;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('allocate_total_amount'),
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        const SizedBox(
          height: 10,
        ),
        ...(inviteCustomerVm
                    .formValues[InviteCustomerFormKeys.industryDescriptions]
                as List<Map<String, dynamic>>)
            .map((value) {
          final industryIndex = (inviteCustomerVm
                      .formValues[InviteCustomerFormKeys.industryDescriptions]
                  as List<Map<String, dynamic>>)
              .indexOf(value);

          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              ...(value['subIndustry'] as List<Map<String, dynamic>>)
                  .map((subIndustryMap) {
                final subIndustryIndex =
                    (value['subIndustry'] as List<Map<String, dynamic>>)
                        .indexOf(subIndustryMap);

                return Container(
                  padding: const EdgeInsets.all(10),
                  margin: const EdgeInsets.symmetric(vertical: 10),
                  decoration: BoxDecoration(
                      border: Border.all(color: PartnerAppColors.spanishGrey),
                      borderRadius: BorderRadius.circular(5)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width / 2.5,
                        child: Text(
                          '${subIndustryMap['subIndustryName']} (${subIndustryMap['productTypeName']})',
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium!
                              .copyWith(fontWeight: FontWeight.normal),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: CustomTextFieldFormBuilder(
                            // key: Key(
                            //     '${subIndustryMap['productTypeId']}_${subIndustryMap['subIndustryId']}_${UniqueKey()}'),
                            name:
                                '${subIndustryMap['productTypeId']}_${subIndustryMap['subIndustryId']}_$subIndustryIndex',
                            initialValue: (subIndustryMap['price'] as double)
                                .toStringAsFixed(2),
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.end,
                            inputFormatters: [currencyFormatter],
                            suffixIcon: Container(
                              padding: const EdgeInsets.only(
                                  left: 10, right: 10, bottom: 4, top: 12),
                              child: Text(
                                'kr.',
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineMedium!
                                    .copyWith(
                                        fontSize: 18,
                                        fontWeight: FontWeight.normal,
                                        color: PartnerAppColors.darkBlue),
                              ),
                            ),
                            onChanged: (p0) async {
                              debouncer.run(() async {
                                if ((p0 ?? '').isNotEmpty) {
                                  await inviteCustomerVm.updateIndustryPrice(
                                      industryIndex: industryIndex,
                                      subIndustryIndex: subIndustryIndex,
                                      price: double.parse(Formatter
                                          .sanitizeCurrencyFromFormatter(p0!)),
                                      calculateFee: false);
                                } else {
                                  inviteCustomerVm.updateIndustryPrice(
                                      industryIndex: industryIndex,
                                      subIndustryIndex: subIndustryIndex,
                                      price: 0);
                                }
                              });
                            }),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      InkWell(
                        onTap: () {
                          inviteCustomerVm.removeIndustry(
                              industryIndex: industryIndex,
                              subIndustryIndex: subIndustryIndex);
                        },
                        child: const Icon(
                          FeatherIcons.trash2,
                          color: PartnerAppColors.red,
                        ),
                      )
                    ],
                  ),
                );
              })
            ],
          );
        }),
        const Divider(
          color: PartnerAppColors.darkBlue,
        ),
        const SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('total_price_exl_vat'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalPriceWoVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('vat_value'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('total_include_vat'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalPriceVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
      ],
    );
  }
}
