import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class UploadOfferFormStep6 extends StatelessWidget {
  const UploadOfferFormStep6({super.key, required this.inviteCustomerVm});

  final InviteCustomerViewmodel inviteCustomerVm;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomTextFieldFormBuilder(
          name: InviteCustomerFormKeys.projectEstimate,
          labelText: tr('project_day_estimate'),
          hintText: tr('days_hint_text'),
          keyboardType: TextInputType.number,
          initialValue: inviteCustomerVm
              .formValues[InviteCustomerFormKeys.projectEstimate],
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
        CustomDatePickerFormBuilder(
          name: InviteCustomerFormKeys.projectStartDate,
          labelText: tr('date_to_start_project'),
          dateFormat: DateFormatType.isoDate.formatter,
          initialDate: DateTime.now(),
          initialValue: inviteCustomerVm
                  .formValues[InviteCustomerFormKeys.projectStartDate] ??
              DateTime.now(),
          suffixIcon: const Icon(
            FeatherIcons.calendar,
            color: PartnerAppColors.blue,
          ),
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
        const SizedBox(
          height: 10,
        ),
        RichText(
          text: TextSpan(
            text: '${tr('your_offer_is_valid')} ',
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                letterSpacing: 1.0,
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.normal,
                fontSize: 18),
            children: [
              TextSpan(
                  text:
                      '${inviteCustomerVm.formValues[InviteCustomerFormKeys.offerValidity] ?? ''}',
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.bold,
                      fontSize: 18)),
              TextSpan(
                  text: ' ${tr('days_valid')}',
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 18)),
            ],
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        DropdownButtonHideUnderline(
          child: ButtonTheme(
            alignedDropdown: true,
            child: CustomDropdownFormBuilder(
              items: [
                ...List.generate(30, (index) {
                  return DropdownMenuItem(
                      value: '${index + 1}', child: Text('${index + 1}'));
                })
              ],
              name: InviteCustomerFormKeys.offerValidity,
              validator:
                  FormBuilderValidators.required(errorText: tr('required')),
              initialValue: inviteCustomerVm
                  .formValues[InviteCustomerFormKeys.offerValidity],
              onChanged: (p0) {
                if ((p0 ?? '').isNotEmpty) {
                  inviteCustomerVm.updateFormValues(
                      key: InviteCustomerFormKeys.offerValidity, values: p0);
                }
              },
            ),
          ),
        )
      ],
    );
  }
}
