import 'dart:io';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/files_selector_dialog.dart';
import 'package:Haandvaerker.dk/utils/file_utils.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class UploadOfferFormStep2 extends StatelessWidget {
  const UploadOfferFormStep2({
    super.key,
    required this.inviteCustomerVm,
  });

  final InviteCustomerViewmodel inviteCustomerVm;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(tr('upload_offer_here'),
            style: Theme.of(context).textTheme.titleMedium!.copyWith(
                color: PartnerAppColors.darkBlue, fontWeight: FontWeight.bold)),
        const SizedBox(
          height: 10,
        ),
        FormBuilderField<List<File>>(
            validator:
                FormBuilderValidators.required(errorText: tr('required')),
            initialValue: [
              ...(inviteCustomerVm
                      .formValues[InviteCustomerFormKeys.offerFiles] ??
                  [])
            ],
            builder: (FormFieldState field) {
              return InputDecorator(
                decoration: InputDecoration(
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.zero,
                  errorText: field.errorText,
                ),
                child: Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: const EdgeInsets.all(20),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border:
                              Border.all(color: PartnerAppColors.spanishGrey)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Icon(
                            FeatherIcons.uploadCloud,
                            size: 30,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(tr('upload_file'),
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(
                                      color: PartnerAppColors.darkBlue,
                                      fontWeight: FontWeight.bold)),
                          const SizedBox(
                            height: 10,
                          ),
                          TextButton(
                              onPressed: () async {
                                List<File> items =
                                    ((field.value as List<File>?) ?? []);

                                await filesSelectorDialog(context: context)
                                    .then((documents) {
                                  items.insertAll(0, [...(documents ?? [])]);
                                });

                                field.didChange([...items]);
                              },
                              style: TextButton.styleFrom(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20),
                                  shape: const RoundedRectangleBorder(
                                      side: BorderSide(
                                          color: PartnerAppColors.darkBlue))),
                              child: Text(
                                tr('upload'),
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineMedium!
                                    .copyWith(
                                      color: PartnerAppColors.darkBlue,
                                    ),
                              ))
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    ...((field.value as List<File>?) ?? []).map((file) {
                      return fileGridItem(
                          context: context, file: file, field: field);
                    })
                  ],
                ),
              );
            },
            name: InviteCustomerFormKeys.offerFiles),
      ],
    );
  }

  Widget fileGridItem(
      {required BuildContext context,
      required File file,
      required FormFieldState<dynamic> field}) {
    Widget leading = const SizedBox.shrink();

    if (isImageFile(file.path)) {
      leading = SizedBox(
        height: 80,
        width: 80,
        child: Image.file(
          file,
          fit: BoxFit.cover,
        ),
      );
    } else if (isDocumentFile(file.path)) {
      leading = Container(
        height: 80,
        width: 80,
        color: PartnerAppColors.blue,
        child: const Center(
          child: Icon(
            FeatherIcons.paperclip,
            color: Colors.white,
            size: 40,
          ),
        ),
      );
    } else {
      leading = const SizedBox.shrink();
    }

    return Column(
      children: [
        ListTile(
          contentPadding: EdgeInsets.zero,
          leading: leading,
          title: Text(
            getNameFromFile(file.path),
            style: Theme.of(context).textTheme.titleMedium,
          ),
          trailing: InkWell(
            onTap: () {
              List<File> items = [...((field.value as List<File>?) ?? [])];

              items.remove(file);

              field.didChange([...items]);
            },
            child: const Icon(
              FeatherIcons.trash2,
              color: PartnerAppColors.red,
            ),
          ),
        ),
        const SizedBox(
          height: 5,
        ),
        const Divider(
          color: PartnerAppColors.darkBlue,
        ),
        const SizedBox(
          height: 5,
        ),
      ],
    );
  }
}
