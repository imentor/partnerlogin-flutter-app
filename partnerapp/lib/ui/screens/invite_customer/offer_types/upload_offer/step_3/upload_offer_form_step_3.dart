import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/debouncer.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class UploadOfferFormStep3 extends StatelessWidget {
  UploadOfferFormStep3({
    super.key,
    required this.inviteCustomerVm,
    required this.formKey,
    required this.currencyFormatter,
  });

  final InviteCustomerViewmodel inviteCustomerVm;
  final GlobalKey<FormBuilderState> formKey;
  final CurrencyTextInputFormatter currencyFormatter;

  final Debouncer debouncer = Debouncer(milliseconds: 500);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomTextFieldFormBuilder(
          name: InviteCustomerFormKeys.projectName,
          labelText: tr('project_name'),
          enabled: inviteCustomerVm
                  .formValues[InviteCustomerFormKeys.recipientTypeExisting] ==
              InviteCustomerFormKeys.recipientTypeNew,
          validator: FormBuilderValidators.required(errorText: tr('required')),
          initialValue: inviteCustomerVm.formValues[
                      InviteCustomerFormKeys.recipientTypeExisting] ==
                  InviteCustomerFormKeys.recipientTypeNew
              ? inviteCustomerVm.formValues[InviteCustomerFormKeys.projectName]
              : (inviteCustomerVm.formValues[InviteCustomerFormKeys
                          .customerProject] as PartnerProjectsModel?)
                      ?.name ??
                  '',
        ),
        CustomTextFieldFormBuilder(
            textAlign: TextAlign.end,
            labelText: tr('total_project_price'),
            name: InviteCustomerFormKeys.totalProjectPrice,
            initialValue: inviteCustomerVm
                    .formValues[InviteCustomerFormKeys.totalProjectPrice] ??
                '0',
            keyboardType: TextInputType.number,
            inputFormatters: [currencyFormatter],
            validator: (p0) {
              if ((p0 ?? '').isNotEmpty) {
                if (double.parse(Formatter.sanitizeCurrencyFromFormatter(p0!)) <
                    800) {
                  return tr('must_be_greater_than_800');
                }
              }
              return null;
            },
            suffixIcon: Container(
                padding: const EdgeInsets.only(
                    left: 10, right: 10, bottom: 4, top: 12),
                child: Text('kr.',
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        fontSize: 18,
                        fontWeight: FontWeight.normal,
                        color: PartnerAppColors.darkBlue))),
            onChanged: (p0) {
              debouncer.run(() {
                if ((p0 ?? '').isNotEmpty) {
                  inviteCustomerVm.updatePrice(
                      price: double.parse(
                          Formatter.sanitizeCurrencyFromFormatter(p0!)));
                } else {
                  inviteCustomerVm.updatePrice(price: 0);
                }
              });
            }),
        const SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('total_price_exl_vat'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalPriceWoVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('vat_value'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('total_include_vat'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalPriceVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
      ],
    );
  }
}
