import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/reviews/review_list.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/components/radio_save_conditions.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

class CreateOfferFormStep4 extends StatefulWidget {
  const CreateOfferFormStep4({super.key, required this.inviteCustomerVm});
  final InviteCustomerViewmodel inviteCustomerVm;

  @override
  State<CreateOfferFormStep4> createState() => _CreateOfferFormStep4State();
}

class _CreateOfferFormStep4State extends State<CreateOfferFormStep4> {
  bool showCondition = false;
  bool saveOfferCondition = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomTextFieldFormBuilder(
          name: InviteCustomerFormKeys.projectEstimate,
          labelText: tr('project_day_estimate'),
          hintText: tr('days_hint_text'),
          keyboardType: TextInputType.number,
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
        CustomDatePickerFormBuilder(
          name: InviteCustomerFormKeys.projectStartDate,
          labelText: tr('date_to_start_project'),
          dateFormat: DateFormatType.isoDate.formatter,
          initialDate: DateTime.now(),
          initialValue: DateTime.now(),
          suffixIcon: const Icon(
            FeatherIcons.calendar,
            color: PartnerAppColors.blue,
          ),
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
        const SizedBox(
          height: 10,
        ),
        RichText(
          text: TextSpan(
            text: '${tr('your_offer_is_valid')} ',
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                letterSpacing: 1.0,
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.normal,
                fontSize: 18),
            children: [
              TextSpan(
                  text:
                      '${widget.inviteCustomerVm.formValues[InviteCustomerFormKeys.offerValidity] ?? ''}',
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.bold,
                      fontSize: 18)),
              TextSpan(
                  text: ' ${tr('days_valid')}',
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 18)),
            ],
          ),
        ),
        DropdownButtonHideUnderline(
          child: ButtonTheme(
            alignedDropdown: true,
            child: CustomDropdownFormBuilder(
              items: [
                ...List.generate(30, (index) {
                  return DropdownMenuItem(
                      value: '${index + 1}', child: Text('${index + 1}'));
                })
              ],
              name: InviteCustomerFormKeys.offerValidity,
              validator:
                  FormBuilderValidators.required(errorText: tr('required')),
              initialValue: widget.inviteCustomerVm
                  .formValues[InviteCustomerFormKeys.offerValidity],
              onChanged: (p0) {
                if ((p0 ?? '').isNotEmpty) {
                  widget.inviteCustomerVm.updateFormValues(
                      key: InviteCustomerFormKeys.offerValidity, values: p0);
                }
              },
            ),
          ),
        ),
        if (showCondition) ...[
          CustomTextFieldFormBuilder(
            name: InviteCustomerFormKeys.offerConditions,
            labelText: tr('add_reservations_to_offer'),
            hintText: tr('add_conditions_to_offer'),
            minLines: 5,
            initialValue: widget.inviteCustomerVm
                    .formValues[InviteCustomerFormKeys.offerConditions] ??
                context
                    .read<AppDrawerViewModel>()
                    .supplierProfileModel
                    ?.bCrmCompany
                    ?.companyOfferNote,
          ),
          const SizedBox(
            height: 10,
          ),
          RadioSaveConditions(
            initialValue: widget.inviteCustomerVm
                .formValues[InviteCustomerFormKeys.saveOfferConditions],
          ),
        ],
        const SizedBox(
          height: 20,
        ),
        Row(
          children: [
            InkWell(
              onTap: () {
                setState(() {
                  showCondition = !showCondition;
                });
              },
              child: RichText(
                text: TextSpan(
                  children: [
                    WidgetSpan(
                      child: Icon(
                        showCondition ? Icons.remove : Icons.add,
                        color: PartnerAppColors.blue,
                      ),
                      alignment: PlaceholderAlignment.middle,
                    ),
                    TextSpan(
                        text:
                            ' ${showCondition ? tr('remove') : tr('add_betingelser')}',
                        style: Theme.of(context)
                            .textTheme
                            .titleSmall!
                            .copyWith(color: PartnerAppColors.blue))
                  ],
                ),
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            InkWell(
              onTap: () async {
                await chooseReviewListDialog(
                        context: context,
                        alreadySelectedId: widget.inviteCustomerVm.formValues[
                                InviteCustomerFormKeys.offerRecommendations] ??
                            [])
                    .then((reviewIds) {
                  widget.inviteCustomerVm.updateFormValues(
                      key: InviteCustomerFormKeys.offerRecommendations,
                      values: reviewIds ?? []);
                });
              },
              child: RichText(
                text: TextSpan(
                  children: [
                    const WidgetSpan(
                      child: Icon(
                        Icons.add,
                        color: PartnerAppColors.blue,
                      ),
                      alignment: PlaceholderAlignment.middle,
                    ),
                    TextSpan(
                        text: ' ${tr('add_your_recommendation')}',
                        style: Theme.of(context)
                            .textTheme
                            .titleSmall!
                            .copyWith(color: PartnerAppColors.blue))
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
