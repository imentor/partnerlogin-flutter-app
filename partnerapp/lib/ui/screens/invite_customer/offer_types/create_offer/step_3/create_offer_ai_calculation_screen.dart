import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class CreateOfferAiCalculationScreen extends StatefulWidget {
  const CreateOfferAiCalculationScreen({
    super.key,
    required this.title,
    required this.industryIndex,
    required this.subIndustryIndex,
    required this.productTypeId,
    required this.subIndustryId,
    required this.descriptions,
    required this.formKey,
  });

  final String title;
  final int industryIndex;
  final int subIndustryIndex;
  final String productTypeId;
  final String subIndustryId;
  final List<dynamic> descriptions;
  final GlobalKey<FormBuilderState> formKey;

  @override
  State<CreateOfferAiCalculationScreen> createState() =>
      _CreateOfferAiCalculationScreenState();
}

class _CreateOfferAiCalculationScreenState
    extends State<CreateOfferAiCalculationScreen> {
  Map<String, dynamic>? pricesMap;

  final CurrencyTextInputFormatter currencyFormatter =
      CurrencyTextInputFormatter(
          NumberFormat.currency(locale: 'da', decimalDigits: 2, symbol: ''));

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final inviteCustomerVm = context.read<InviteCustomerViewmodel>();

      await tryCatchWrapper(
              context: context,
              function: inviteCustomerVm.getAiCalculation(
                  industry: widget.subIndustryId,
                  productType: widget.productTypeId))
          .then((value) {
        if (value != null) {
          pricesMap = value;
        }

        inviteCustomerVm.setBusy(false);
      });
    });
    super.initState();
  }

  Widget buttonNav(InviteCustomerViewmodel inviteCustomerVm) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextButton(
            style: TextButton.styleFrom(
                fixedSize: Size(MediaQuery.of(context).size.width, 45),
                backgroundColor: PartnerAppColors.malachite),
            child: Text(
              tr('save_ai_pricing'),
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium!
                  .copyWith(color: Colors.white),
            ),
            onPressed: () {
              if (!inviteCustomerVm.busy) {
                widget.formKey.currentState!.patchValue({
                  '${widget.productTypeId}_${widget.subIndustryId}':
                      currencyFormatter.formatString(
                          (pricesMap?['price'] as num? ?? 0).toStringAsFixed(2))
                });

                backDrawerRoute();
              }
            },
          ),
          const SizedBox(
            height: 10,
          ),
          InkWell(
            onTap: () {
              if (!inviteCustomerVm.busy) {
                backDrawerRoute();
              }
            },
            child: Text(
              tr('insert_own_prices'),
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                  color: PartnerAppColors.blue, fontWeight: FontWeight.normal),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<InviteCustomerViewmodel>(
        builder: (_, inviteCustomerVm, __) {
      return Scaffold(
        appBar: DrawerAppBar(
          overrideBackFunction: () {
            if (!inviteCustomerVm.busy) {
              backDrawerRoute();
            }
          },
        ),
        bottomNavigationBar: buttonNav(inviteCustomerVm),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: Skeletonizer(
            enabled: inviteCustomerVm.busy,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  tr('ai_price_calculator'),
                  style: Theme.of(context)
                      .textTheme
                      .titleMedium!
                      .copyWith(fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  tr('ai_price_description'),
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                const SizedBox(
                  height: 10,
                ),
                const Divider(
                  color: PartnerAppColors.darkBlue,
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  tr('your_ai_estimate'),
                  style: Theme.of(context)
                      .textTheme
                      .titleMedium!
                      .copyWith(fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Text(
                        widget.title,
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(fontWeight: FontWeight.normal),
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Text(
                      Formatter.curencyFormat(amount: pricesMap?['price'] ?? 0),
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium!
                          .copyWith(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                ...widget.descriptions.map((value) {
                  final descriptionIndex = widget.descriptions.indexOf(value);

                  return Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          '${widget.title.split(' ').first}${descriptionIndex + 1}',
                          style: Theme.of(context).textTheme.titleSmall,
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          flex: 2,
                          child: Text(
                            value,
                            style: Theme.of(context).textTheme.titleSmall,
                          ),
                        ),
                      ],
                    ),
                  );
                })
              ],
            ),
          ),
        ),
      );
    });
  }
}
