import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/debouncer.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/string_utils.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';

class CreateOfferFormStep3 extends StatelessWidget {
  CreateOfferFormStep3({
    super.key,
    required this.inviteCustomerVm,
    required this.formKey,
    required this.currencyFormatter,
  });
  final InviteCustomerViewmodel inviteCustomerVm;
  final Debouncer debouncer = Debouncer(milliseconds: 500);
  final GlobalKey<FormBuilderState> formKey;
  final CurrencyTextInputFormatter currencyFormatter;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('offer_list'),
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          tr('enter_offer_price_description'),
          style: Theme.of(context)
              .textTheme
              .titleSmall!
              .copyWith(color: PartnerAppColors.spanishGrey),
        ),
        const SizedBox(
          height: 10,
        ),
        ...(inviteCustomerVm
                    .formValues[InviteCustomerFormKeys.industryDescriptions]
                as List<Map<String, dynamic>>)
            .map((industryDescriptionMap) {
          final industryIndex = (inviteCustomerVm
                      .formValues[InviteCustomerFormKeys.industryDescriptions]
                  as List<Map<String, dynamic>>)
              .indexOf(industryDescriptionMap);

          return Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                    '${industryIndex + 1}. ${(industryDescriptionMap['productTypeName'] as String).toCapitalizedFirst()}',
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal)),
                const SizedBox(
                  height: 10,
                ),
                ...(industryDescriptionMap['subIndustry']
                        as List<Map<String, dynamic>>)
                    .map((subIndustryMap) {
                  final subIndustryIndex =
                      (industryDescriptionMap['subIndustry']
                              as List<Map<String, dynamic>>)
                          .indexOf(subIndustryMap);

                  return Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                                '${industryIndex + 1}.${subIndustryIndex + 1} ${(subIndustryMap['subIndustryName'] as String).toCapitalizedFirst()}',
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium!
                                    .copyWith(
                                        color: PartnerAppColors.darkBlue,
                                        fontWeight: FontWeight.normal)),
                            InkWell(
                              onTap: () {
                                inviteCustomerVm.removeIndustry(
                                    industryIndex: industryIndex,
                                    subIndustryIndex: subIndustryIndex);
                              },
                              child: const Icon(
                                FeatherIcons.trash2,
                                color: PartnerAppColors.red,
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        CustomTextFieldFormBuilder(
                            textAlign: TextAlign.end,
                            name:
                                '${subIndustryMap['productTypeId']}_${subIndustryMap['subIndustryId']}',
                            initialValue: Formatter.curencyFormat(
                                    amount: subIndustryMap['price'],
                                    withoutSymbol: true)
                                .trim(),
                            keyboardType: TextInputType.number,
                            suffixIcon: Container(
                                padding: const EdgeInsets.only(
                                    left: 10, right: 10, bottom: 4, top: 12),
                                child: Text('kr.',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headlineMedium!
                                        .copyWith(
                                            fontSize: 18,
                                            fontWeight: FontWeight.normal,
                                            color: PartnerAppColors.darkBlue))),
                            inputFormatters: [currencyFormatter],
                            onChanged: (p0) {
                              debouncer.run(() {
                                if ((p0 ?? '').isNotEmpty || p0 == '0.0') {
                                  inviteCustomerVm.updateIndustryPrice(
                                      industryIndex: industryIndex,
                                      subIndustryIndex: subIndustryIndex,
                                      price: double.parse(Formatter
                                          .sanitizeCurrencyFromFormatter(p0!)));
                                } else {
                                  inviteCustomerVm.updateIndustryPrice(
                                      industryIndex: industryIndex,
                                      subIndustryIndex: subIndustryIndex,
                                      price: 0);
                                }
                              });
                            }),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            if (!(subIndustryMap['viewDescriptions'] ??
                                false)) ...[
                              InkWell(
                                onTap: () {
                                  inviteCustomerVm
                                      .updateIndustryDescriptionView(
                                          industryIndex: industryIndex,
                                          subIndustryIndex: subIndustryIndex);
                                },
                                child: RichText(
                                  text: TextSpan(
                                    children: [
                                      TextSpan(
                                        text:
                                            '${tr('see_and_correct_description')} ',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleSmall!
                                            .copyWith(
                                              color: PartnerAppColors.blue,
                                            ),
                                      ),
                                      const WidgetSpan(
                                        alignment: PlaceholderAlignment.middle,
                                        child: Icon(
                                          FeatherIcons.chevronDown,
                                          size: 16,
                                          color: PartnerAppColors.blue,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                            ] else
                              Container(),
                            if ((context
                                        .read<AppDrawerViewModel>()
                                        .supplierProfileModel
                                        ?.bCrmCompany
                                        ?.companyPrisberegning ??
                                    0) ==
                                1)
                              InkWell(
                                onTap: () async {
                                  changeDrawerRoute(
                                      Routes.inviteCustomerAiCalculation,
                                      arguments: {
                                        'industryIndex': industryIndex,
                                        'subIndustryIndex': subIndustryIndex,
                                        'descriptions':
                                            subIndustryMap['descriptions'],
                                        'title':
                                            '${industryIndex + 1}.${subIndustryIndex + 1} ${(subIndustryMap['subIndustryName'] as String).toCapitalizedFirst()}',
                                        'productTypeId': industryDescriptionMap[
                                            'productTypeId'],
                                        'subIndustryId':
                                            subIndustryMap['subIndustryId'],
                                        'formKey': formKey,
                                      });
                                },
                                child: RichText(
                                  text: TextSpan(
                                    children: [
                                      TextSpan(
                                        text:
                                            '${tr('use_ai_price_calculator')} ',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleSmall!
                                            .copyWith(
                                              color: PartnerAppColors.blue,
                                            ),
                                      ),
                                      const WidgetSpan(
                                        alignment: PlaceholderAlignment.middle,
                                        child: Icon(
                                          FeatherIcons.info,
                                          size: 16,
                                          color: PartnerAppColors.blue,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            else
                              Container()
                          ],
                        ),
                        if ((subIndustryMap['viewDescriptions'] ?? false)) ...[
                          const SizedBox(
                            height: 20,
                          ),
                          ListView.builder(
                            itemCount:
                                (subIndustryMap['descriptions'] as List).length,
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (context, descriptionIndex) {
                              return Container(
                                margin:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Flexible(
                                        flex: 2,
                                        child: Text(
                                          '${industryIndex + 1}.${subIndustryIndex + 1}.${descriptionIndex + 1}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleSmall!
                                              .copyWith(
                                                  color: PartnerAppColors
                                                      .spanishGrey),
                                        )),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Expanded(
                                      flex: 10,
                                      child: Text(
                                        (subIndustryMap['descriptions']
                                            as List)[descriptionIndex],
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleSmall!
                                            .copyWith(
                                                color: PartnerAppColors
                                                    .spanishGrey),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                          if ((subIndustryMap['viewDescriptions'] ??
                              false)) ...[
                            const SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                InkWell(
                                  onTap: () {
                                    inviteCustomerVm
                                        .updateIndustryDescriptionView(
                                            industryIndex: industryIndex,
                                            subIndustryIndex: subIndustryIndex);
                                  },
                                  child: RichText(
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                          text: '${tr('hide_description')} ',
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleSmall!
                                              .copyWith(
                                                color: PartnerAppColors.blue,
                                              ),
                                        ),
                                        const WidgetSpan(
                                          alignment:
                                              PlaceholderAlignment.middle,
                                          child: Icon(
                                            FeatherIcons.chevronUp,
                                            size: 16,
                                            color: PartnerAppColors.blue,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                InkWell(
                                  onTap: () {
                                    changeDrawerRoute(
                                        Routes.inviteCustomerEditDescription,
                                        arguments: {
                                          'industryIndex': industryIndex,
                                          'subIndustryIndex': subIndustryIndex,
                                          'descriptions':
                                              subIndustryMap['descriptions'],
                                          'originalDescriptions':
                                              subIndustryMap[
                                                  'originalDescriptions'],
                                          'title':
                                              '${industryIndex + 1}.${subIndustryIndex + 1} ${(subIndustryMap['subIndustryName'] as String).toCapitalizedFirst()}'
                                        });
                                  },
                                  child: RichText(
                                    text: TextSpan(
                                      children: [
                                        const WidgetSpan(
                                          alignment:
                                              PlaceholderAlignment.middle,
                                          child: Icon(
                                            FeatherIcons.edit,
                                            size: 16,
                                            color: PartnerAppColors.blue,
                                          ),
                                        ),
                                        TextSpan(
                                          text: ' ${tr('edit_description')}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleSmall!
                                              .copyWith(
                                                color: PartnerAppColors.blue,
                                              ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ]
                        ],
                        const SizedBox(
                          height: 10,
                        ),
                        const Divider(
                          color: PartnerAppColors.darkBlue,
                        ),
                      ],
                    ),
                  );
                }),
              ],
            ),
          );
        }),
        const SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('unit_price_ex_vat'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalPriceWoVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('vat_value'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('total_with_vat'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalPriceVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
      ],
    );
  }
}
