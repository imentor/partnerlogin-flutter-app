import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class CreateOfferFormStep5 extends StatelessWidget {
  const CreateOfferFormStep5({
    super.key,
    required this.inviteCustomerVm,
    required this.formKey,
  });
  final InviteCustomerViewmodel inviteCustomerVm;
  final GlobalKey<FormBuilderState> formKey;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('summary'),
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('project_name'),
              style: Theme.of(context)
                  .textTheme
                  .titleSmall!
                  .copyWith(color: PartnerAppColors.spanishGrey),
            ),
            Row(
              children: [
                InkWell(
                  onTap: () async {
                    if ((inviteCustomerVm.formValues[
                            InviteCustomerFormKeys.customerProjectEditTitle] ??
                        false)) {
                      await inviteCustomerVm.editProjectTItleOrDescription(
                          title: formKey
                              .currentState!
                              .fields[InviteCustomerFormKeys
                                  .customerProjectEditTitle]!
                              .value);
                    } else {
                      inviteCustomerVm.updateFormValues(
                          key: InviteCustomerFormKeys.customerProjectEditTitle,
                          values: true);
                    }
                  },
                  child: Text(
                    (inviteCustomerVm.formValues[InviteCustomerFormKeys
                                .customerProjectEditTitle] ??
                            false)
                        ? tr('save')
                        : tr('edit'),
                    style: Theme.of(context)
                        .textTheme
                        .titleSmall!
                        .copyWith(color: PartnerAppColors.blue),
                  ),
                ),
                if (inviteCustomerVm.formValues[
                        InviteCustomerFormKeys.customerProjectEditTitle] ??
                    false) ...[
                  const SizedBox(
                    width: 5,
                  ),
                  InkWell(
                    onTap: () {
                      inviteCustomerVm.updateFormValues(
                          key: InviteCustomerFormKeys.customerProjectEditTitle,
                          values: false);
                    },
                    child: const Icon(
                      FeatherIcons.x,
                      size: 18,
                      color: PartnerAppColors.blue,
                    ),
                  ),
                ]
              ],
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        if ((inviteCustomerVm
                .formValues[InviteCustomerFormKeys.customerProjectEditTitle] ??
            false))
          CustomTextFieldFormBuilder(
            name: InviteCustomerFormKeys.customerProjectEditTitle,
            key: Key(
                '${InviteCustomerFormKeys.customerProjectEditTitle}_${UniqueKey()}'),
            initialValue: (inviteCustomerVm
                        .formValues[InviteCustomerFormKeys.customerProject]
                    as PartnerProjectsModel)
                .name,
          )
        else
          Text(
            (inviteCustomerVm.formValues[InviteCustomerFormKeys.customerProject]
                        as PartnerProjectsModel?)
                    ?.name ??
                '',
            style: Theme.of(context).textTheme.titleMedium,
          ),
        const SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('project_overview'),
              style: Theme.of(context)
                  .textTheme
                  .titleSmall!
                  .copyWith(color: PartnerAppColors.spanishGrey),
            ),
            Row(
              children: [
                InkWell(
                  onTap: () async {
                    if ((inviteCustomerVm.formValues[InviteCustomerFormKeys
                            .customerProjectEditDescription] ??
                        false)) {
                      await inviteCustomerVm.editProjectTItleOrDescription(
                          description: formKey
                              .currentState!
                              .fields[InviteCustomerFormKeys
                                  .customerProjectEditDescription]!
                              .value);
                    } else {
                      inviteCustomerVm.updateFormValues(
                          key: InviteCustomerFormKeys
                              .customerProjectEditDescription,
                          values: true);
                    }
                  },
                  child: Text(
                    (inviteCustomerVm.formValues[InviteCustomerFormKeys
                                .customerProjectEditDescription] ??
                            false)
                        ? tr('save')
                        : tr('edit'),
                    style: Theme.of(context)
                        .textTheme
                        .titleSmall!
                        .copyWith(color: PartnerAppColors.blue),
                  ),
                ),
                if (inviteCustomerVm.formValues[InviteCustomerFormKeys
                        .customerProjectEditDescription] ??
                    false) ...[
                  const SizedBox(
                    width: 5,
                  ),
                  InkWell(
                    onTap: () {
                      inviteCustomerVm.updateFormValues(
                          key: InviteCustomerFormKeys
                              .customerProjectEditDescription,
                          values: false);
                    },
                    child: const Icon(
                      FeatherIcons.x,
                      size: 18,
                      color: PartnerAppColors.blue,
                    ),
                  ),
                ]
              ],
            )
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        if ((inviteCustomerVm.formValues[
                InviteCustomerFormKeys.customerProjectEditDescription] ??
            false))
          CustomTextFieldFormBuilder(
            name: InviteCustomerFormKeys.customerProjectEditDescription,
            key: Key(
                '${InviteCustomerFormKeys.customerProjectEditDescription}_${UniqueKey()}'),
            initialValue: (inviteCustomerVm
                        .formValues[InviteCustomerFormKeys.customerProject]
                    as PartnerProjectsModel)
                .description,
          )
        else
          Text(
            (inviteCustomerVm.formValues[InviteCustomerFormKeys.customerProject]
                        as PartnerProjectsModel?)
                    ?.description ??
                '',
            style: Theme.of(context).textTheme.titleMedium,
          ),
        const SizedBox(
          height: 10,
        ),
        const Divider(
          color: PartnerAppColors.darkBlue,
        ),
        const SizedBox(
          height: 10,
        ),
        ...(inviteCustomerVm
                    .formValues[InviteCustomerFormKeys.industryDescriptions]
                as List<Map<String, dynamic>>)
            .map((value) {
          final index = (inviteCustomerVm
                      .formValues[InviteCustomerFormKeys.industryDescriptions]
                  as List<Map<String, dynamic>>)
              .indexOf(value);

          return ListView.separated(
              itemBuilder: (context, subIndustryIndex) {
                Map<String, dynamic> subIndustryMap =
                    (value['subIndustry'] as List)[subIndustryIndex];

                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '${index + 1}.${subIndustryIndex + 1} ${subIndustryMap['subIndustryName']}',
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium!
                              .copyWith(fontWeight: FontWeight.normal),
                        ),
                        Text(
                          Formatter.curencyFormat(
                              amount: subIndustryMap['price']),
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium!
                              .copyWith(fontWeight: FontWeight.normal),
                        ),
                      ],
                    ),
                    if (subIndustryMap['viewDescriptions']) ...[
                      const SizedBox(
                        height: 20,
                      ),
                      ...(subIndustryMap['descriptions'] as List).map((value) {
                        final descriptionIndex =
                            (subIndustryMap['descriptions'] as List)
                                .indexOf(value);

                        return Container(
                          margin: const EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Flexible(
                                  flex: 1,
                                  child: Text(
                                    '${index + 1}.${subIndustryIndex + 1}.${descriptionIndex + 1}',
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleSmall!
                                        .copyWith(
                                            color:
                                                PartnerAppColors.spanishGrey),
                                  )),
                              const SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                flex: 10,
                                child: Text(
                                  (subIndustryMap['descriptions']
                                      as List)[descriptionIndex],
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleSmall!
                                      .copyWith(
                                          color: PartnerAppColors.spanishGrey),
                                ),
                              ),
                            ],
                          ),
                        );
                      })
                    ],
                    const SizedBox(
                      height: 20,
                    ),
                    InkWell(
                      onTap: () {
                        inviteCustomerVm.updateIndustryDescriptionView(
                            industryIndex: index,
                            subIndustryIndex: subIndustryIndex);
                      },
                      child: RichText(
                          text: TextSpan(
                              text:
                                  "${subIndustryMap['viewDescriptions'] ? tr('hide_description') : tr('see_description')} ",
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(color: PartnerAppColors.blue),
                              children: [
                            WidgetSpan(
                                child: Icon(
                                  subIndustryMap['viewDescriptions']
                                      ? FeatherIcons.chevronUp
                                      : FeatherIcons.chevronDown,
                                  color: PartnerAppColors.blue,
                                ),
                                alignment: PlaceholderAlignment.middle)
                          ])),
                    )
                  ],
                );
              },
              separatorBuilder: (context, index) {
                return Container(
                  margin: const EdgeInsets.symmetric(vertical: 10),
                  child: const Divider(
                    color: PartnerAppColors.darkBlue,
                  ),
                );
              },
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: (value['subIndustry'] as List).length);
        }),
        const SizedBox(
          height: 10,
        ),
        const Divider(
          color: PartnerAppColors.darkBlue,
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('unit_price_ex_vat'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalPriceWoVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('vat_value'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('total_with_vat'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalPriceVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        const Divider(
          color: PartnerAppColors.darkBlue,
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          tr('project_day_estimate'),
          style: Theme.of(context).textTheme.titleMedium,
        ),
        const SizedBox(
          height: 10,
        ),
        RichText(
          text: TextSpan(
            children: [
              const WidgetSpan(
                  child: Icon(
                    FeatherIcons.calendar,
                    color: PartnerAppColors.blue,
                  ),
                  alignment: PlaceholderAlignment.middle),
              TextSpan(
                  text:
                      ' ${inviteCustomerVm.formValues[InviteCustomerFormKeys.projectEstimate]} ${tr('days')}',
                  style: Theme.of(context)
                      .textTheme
                      .titleSmall!
                      .copyWith(fontWeight: FontWeight.bold))
            ],
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          tr('project_start_date'),
          style: Theme.of(context).textTheme.titleMedium,
        ),
        const SizedBox(
          height: 10,
        ),
        RichText(
          text: TextSpan(
            children: [
              const WidgetSpan(
                  child: Icon(
                    FeatherIcons.calendar,
                    color: PartnerAppColors.blue,
                  ),
                  alignment: PlaceholderAlignment.middle),
              TextSpan(
                  text:
                      ' ${DateFormatType.fullMonthDate.formatter.format(inviteCustomerVm.formValues[InviteCustomerFormKeys.projectStartDate] as DateTime)}',
                  style: Theme.of(context)
                      .textTheme
                      .titleSmall!
                      .copyWith(fontWeight: FontWeight.bold))
            ],
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        const Divider(
          color: PartnerAppColors.darkBlue,
        ),
        const SizedBox(
          height: 10,
        ),
        if (inviteCustomerVm
                .formValues[InviteCustomerFormKeys.offerConditions] !=
            null) ...[
          Text(
            tr('standard_text'),
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
              inviteCustomerVm
                  .formValues[InviteCustomerFormKeys.offerConditions],
              style: Theme.of(context)
                  .textTheme
                  .titleSmall!
                  .copyWith(fontWeight: FontWeight.bold))
        ]
      ],
    );
  }
}
