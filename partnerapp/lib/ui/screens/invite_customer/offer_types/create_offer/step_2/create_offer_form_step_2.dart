import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/components/dropdown_select_job_industry.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/components/product_dynamic_form.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:expandable_page_view/expandable_page_view.dart';
import 'package:flutter/material.dart';

class CreateOfferFormStep2 extends StatefulWidget {
  const CreateOfferFormStep2(
      {super.key,
      required this.inviteCustomerVm,
      required this.pageController});
  final InviteCustomerViewmodel inviteCustomerVm;
  final PageController pageController;

  @override
  State<CreateOfferFormStep2> createState() => _CreateOfferFormStep2State();
}

class _CreateOfferFormStep2State extends State<CreateOfferFormStep2> {
  @override
  Widget build(BuildContext context) {
    return subSteps();
  }

  Widget subSteps() {
    switch (widget.inviteCustomerVm.currentStep) {
      case 2:
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              tr('choose_job'),
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              tr('choose_job_description'),
              style: Theme.of(context)
                  .textTheme
                  .titleSmall!
                  .copyWith(color: PartnerAppColors.spanishGrey),
            ),
            const SizedBox(
              height: 10,
            ),
            DropdownSelectJobIndustry(
                inviteCustomerVm: widget.inviteCustomerVm),
          ],
        );
      case 2.1:
        return questionnaire();
      default:
        return const SizedBox();
    }
  }

  Widget questionnaire() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Builder(builder: (context) {
          final List<Widget> body = [];

          for (var i = 0;
              i < widget.inviteCustomerVm.productQuestionnaires.length;
              i++) {
            body.add(ProductDynamicForm(
                inviteCustomerVm: widget.inviteCustomerVm,
                label: widget.inviteCustomerVm.productQuestionnaires[i]
                        .producttype ??
                    'N/A',
                productWizard: widget.inviteCustomerVm.productQuestionnaires[i],
                currentStep:
                    widget.inviteCustomerVm.currentProductQuestionnaireStep + 1,
                totalStep:
                    widget.inviteCustomerVm.productQuestionnaires.length));
          }

          return ExpandablePageView(
            physics: const NeverScrollableScrollPhysics(),
            controller: widget.pageController,
            children: body,
          );
        })
      ],
    );
  }
}
