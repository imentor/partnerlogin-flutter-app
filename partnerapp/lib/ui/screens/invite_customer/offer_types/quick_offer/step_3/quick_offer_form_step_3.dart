import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class QuickOfferFormStep3 extends StatelessWidget {
  const QuickOfferFormStep3({
    super.key,
    required this.inviteCustomerVm,
  });

  final InviteCustomerViewmodel inviteCustomerVm;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: MediaQuery.of(context).size.height / 1.8,
          color: PartnerAppColors.spanishGrey.withValues(alpha: .1),
          padding: const EdgeInsets.all(10),
          child: SfPdfViewer.memory(inviteCustomerVm
              .formValues[InviteCustomerFormKeys.previewOfferPdf] as Uint8List),
        )
      ],
    );
  }
}
