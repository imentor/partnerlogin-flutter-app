import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/debouncer.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class IndustryQuickOfferPrice extends StatefulWidget {
  const IndustryQuickOfferPrice({
    super.key,
    required this.inviteCustomerVm,
    required this.index,
    required this.currencyFormatter,
    required this.industryName,
    required this.descriptionName,
    required this.descriptionInitialValue,
    required this.priceName,
    required this.priceInitialValue,
    required this.quantityName,
    required this.quantityInitialValue,
  });

  final InviteCustomerViewmodel inviteCustomerVm;

  final String industryName;

  final String descriptionName;
  final String descriptionInitialValue;

  final String quantityName;
  final String quantityInitialValue;

  final String priceName;
  final String priceInitialValue;

  final int index;

  final CurrencyTextInputFormatter currencyFormatter;

  @override
  State<IndustryQuickOfferPrice> createState() =>
      _IndustryQuickOfferPriceState();
}

class _IndustryQuickOfferPriceState extends State<IndustryQuickOfferPrice> {
  final Debouncer debouncer = Debouncer(milliseconds: 500);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomTextFieldFormBuilder(
          name: widget.descriptionName,
          labelText: tr('description'),
          hintText: tr('enter_description'),
          initialValue: widget.descriptionInitialValue,
          validator: FormBuilderValidators.required(errorText: tr('required')),
          onChanged: (p0) {
            debouncer.run(() {
              widget.inviteCustomerVm.updateQuickOfferPriceDetails(
                  index: widget.index, name: 'title', value: p0 ?? '');
            });
          },
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                child: CustomTextFieldFormBuilder(
              name: widget.quantityName,
              labelText: tr('quantity'),
              keyboardType: TextInputType.number,
              initialValue: widget.quantityInitialValue,
              textAlign: TextAlign.end,
              validator: (p0) {
                if ((p0 ?? '').isNotEmpty) {
                  if (num.parse(Formatter.sanitizeCurrency(p0!)) <= 0) {
                    return tr('please_enter_correct_price');
                  }
                }
                return null;
              },
              onChanged: (p0) {
                debouncer.run(() {
                  if ((p0 ?? '').isNotEmpty) {
                    widget.inviteCustomerVm.updateQuickOfferPriceDetails(
                        index: widget.index,
                        name: 'quantity',
                        value: int.parse(Formatter.sanitizeCurrency(p0!)));
                  } else {
                    widget.inviteCustomerVm.updateQuickOfferPriceDetails(
                        index: widget.index, name: 'quantity', value: 0);
                  }
                });
              },
            )),
            const SizedBox(
              width: 10,
            ),
            Expanded(
              child: CustomTextFieldFormBuilder(
                name: widget.priceName,
                labelText: tr('price'),
                textAlign: TextAlign.end,
                keyboardType: TextInputType.number,
                inputFormatters: [widget.currencyFormatter],
                initialValue: widget.priceInitialValue,
                validator: (p0) {
                  if ((p0 ?? '').isNotEmpty) {
                    if (num.parse(
                            Formatter.sanitizeCurrencyFromFormatter(p0!)) <=
                        0) {
                      return tr('please_enter_correct_price');
                    }
                  }
                  return null;
                },
                suffixIcon: Container(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, bottom: 4, top: 12),
                  child: Text(
                    'kr.',
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        fontSize: 18,
                        fontWeight: FontWeight.normal,
                        color: PartnerAppColors.darkBlue),
                  ),
                ),
                onChanged: (p0) {
                  debouncer.run(() {
                    if ((p0 ?? '').isNotEmpty) {
                      widget.inviteCustomerVm.updateQuickOfferPriceDetails(
                          index: widget.index,
                          name: 'price',
                          value: double.parse(
                              Formatter.sanitizeCurrencyFromFormatter(p0!)));
                    } else {
                      widget.inviteCustomerVm.updateQuickOfferPriceDetails(
                          index: widget.index, name: 'price', value: 0.0);
                    }
                  });
                },
              ),
            ),
          ],
        ),
      ],
    );
  }
}
