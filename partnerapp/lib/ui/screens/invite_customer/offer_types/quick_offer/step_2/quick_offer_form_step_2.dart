import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/components/radio_save_conditions.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/offer_types/quick_offer/step_2/industry_quick_offer_price.dart';
import 'package:Haandvaerker.dk/utils/debouncer.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

class QuickOfferFormStep2 extends StatefulWidget {
  const QuickOfferFormStep2(
      {super.key,
      required this.inviteCustomerVm,
      required this.formKey,
      required this.currencyFormatter});

  final InviteCustomerViewmodel inviteCustomerVm;
  final GlobalKey<FormBuilderState> formKey;
  final CurrencyTextInputFormatter currencyFormatter;

  @override
  State<QuickOfferFormStep2> createState() => _QuickOfferFormStep2State();
}

class _QuickOfferFormStep2State extends State<QuickOfferFormStep2> {
  final Debouncer debouncer = Debouncer(milliseconds: 500);

  bool showCondition = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('create_offers_quickly'),
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        const SizedBox(
          height: 20,
        ),
        RichText(
          text: TextSpan(
            text: tr('your_company'),
            style: Theme.of(context)
                .textTheme
                .titleSmall!
                .copyWith(color: PartnerAppColors.spanishGrey),
            children: [
              const WidgetSpan(
                  child: SizedBox(
                width: 5,
              )),
              TextSpan(
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      if ((widget.inviteCustomerVm.formValues[
                              InviteCustomerFormKeys.companyNameEdit] ??
                          false)) {
                        widget.inviteCustomerVm.updateFormValues(
                            key: InviteCustomerFormKeys.companyName,
                            values: widget
                                .formKey
                                .currentState!
                                .fields[InviteCustomerFormKeys.companyName]!
                                .value);
                        widget.inviteCustomerVm.updateFormValues(
                            key: InviteCustomerFormKeys.companyNameEdit,
                            values: false);
                      } else {
                        widget.inviteCustomerVm.updateFormValues(
                            key: InviteCustomerFormKeys.companyNameEdit,
                            values: true);
                      }
                    },
                  text: (widget.inviteCustomerVm.formValues[
                              InviteCustomerFormKeys.companyNameEdit] ??
                          false)
                      ? tr('save')
                      : tr('edit'),
                  style: Theme.of(context)
                      .textTheme
                      .titleSmall!
                      .copyWith(color: PartnerAppColors.blue))
            ],
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        companyName(),
        const SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('date'),
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                  letterSpacing: 1.0,
                  color: PartnerAppColors.darkBlue,
                  fontWeight: FontWeight.normal,
                  fontSize: 18),
            ),
            Container(),
          ],
        ),
        Builder(builder: (context) {
          DateTime initialValue = DateTime.now();

          if (widget.inviteCustomerVm
                  .formValues[InviteCustomerFormKeys.projectStartDate] !=
              null) {
            if (widget.inviteCustomerVm
                    .formValues[InviteCustomerFormKeys.projectStartDate]
                is String) {
              initialValue = DateTime.parse(widget.inviteCustomerVm
                  .formValues[InviteCustomerFormKeys.projectStartDate]);
            } else if (widget.inviteCustomerVm
                    .formValues[InviteCustomerFormKeys.projectStartDate]
                is DateTime) {
              initialValue = widget.inviteCustomerVm
                  .formValues[InviteCustomerFormKeys.projectStartDate];
            }
          }

          return CustomDatePickerFormBuilder(
            name: InviteCustomerFormKeys.projectStartDate,
            initialDate: DateTime.now(),
            initialValue: initialValue,
            validator:
                FormBuilderValidators.required(errorText: tr('required')),
            dateFormat: DateFormatType.standardDate2.formatter,
            suffixIcon: const Icon(
              FeatherIcons.calendar,
              color: PartnerAppColors.blue,
              size: 30,
            ),
          );
        }),
        CustomTextFieldFormBuilder(
          name: InviteCustomerFormKeys.offerComments,
          labelText: tr('enter_any_comments_here'),
          hintText: tr('enter_comments'),
          validator: FormBuilderValidators.required(errorText: tr('required')),
          initialValue: widget.inviteCustomerVm
              .formValues[InviteCustomerFormKeys.offerComments],
          minLines: 5,
        ),
        ListView.separated(
          itemBuilder: (context, index) {
            final valueMap = (widget.inviteCustomerVm
                    .formValues[InviteCustomerFormKeys.quickOfferPriceDetails]
                as List<Map<String, dynamic>>)[index];

            return Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              child: IndustryQuickOfferPrice(
                  inviteCustomerVm: widget.inviteCustomerVm,
                  index: index,
                  currencyFormatter: widget.currencyFormatter,
                  industryName: 'offer_industry_$index',
                  descriptionName: 'offer_description_$index',
                  descriptionInitialValue: valueMap['title'],
                  priceName: 'offer_price_$index',
                  priceInitialValue:
                      (valueMap['price'] as double).toStringAsFixed(2),
                  quantityName: 'offer_quantity_$index',
                  quantityInitialValue: '${valueMap['quantity']}'),
            );
          },
          separatorBuilder: (context, index) {
            return const Divider(
              color: PartnerAppColors.darkBlue,
            );
          },
          itemCount: (widget.inviteCustomerVm
                      .formValues[InviteCustomerFormKeys.quickOfferPriceDetails]
                  as List<Map<String, dynamic>>)
              .length,
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            InkWell(
              onTap: () {
                List<Map<String, dynamic>> tempList = [
                  ...widget.inviteCustomerVm
                      .formValues[InviteCustomerFormKeys.quickOfferPriceDetails]
                ];

                tempList.add({"title": '', "quantity": 0, "price": 0.0});

                widget.inviteCustomerVm.updateFormValues(
                    key: InviteCustomerFormKeys.quickOfferPriceDetails,
                    values: tempList);
              },
              child: RichText(
                text: TextSpan(
                  children: [
                    const WidgetSpan(
                      alignment: PlaceholderAlignment.middle,
                      child: Icon(
                        FeatherIcons.plus,
                        color: PartnerAppColors.blue,
                      ),
                    ),
                    TextSpan(
                        text: ' ${tr('add_description')}',
                        style: Theme.of(context)
                            .textTheme
                            .titleSmall!
                            .copyWith(color: PartnerAppColors.blue))
                  ],
                ),
              ),
            ),
            if ((widget.inviteCustomerVm.formValues[InviteCustomerFormKeys
                        .quickOfferPriceDetails] as List<Map<String, dynamic>>)
                    .length >
                1)
              InkWell(
                onTap: () {
                  widget.inviteCustomerVm.removeQuickOfferPriceDetails();
                },
                child: RichText(
                  text: TextSpan(
                    children: [
                      const WidgetSpan(
                        alignment: PlaceholderAlignment.middle,
                        child: Icon(
                          FeatherIcons.minus,
                          color: PartnerAppColors.blue,
                        ),
                      ),
                      TextSpan(
                          text: ' ${tr('remove')}',
                          style: Theme.of(context)
                              .textTheme
                              .titleSmall!
                              .copyWith(color: PartnerAppColors.blue))
                    ],
                  ),
                ),
              )
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        const SizedBox(
          height: 10,
        ),
        const Divider(
          color: PartnerAppColors.darkBlue,
        ),
        const SizedBox(
          height: 10,
        ),
        if (showCondition) ...[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(
                  tr('add_conditions_to_offer'),
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 18),
                ),
              ),
              const SizedBox(
                width: 20,
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    showCondition = false;
                  });
                },
                child: Text(
                  tr('remove'),
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.blue,
                      fontWeight: FontWeight.normal,
                      fontSize: 18),
                ),
              ),
            ],
          ),
          CustomTextFieldFormBuilder(
            name: InviteCustomerFormKeys.offerConditions,
            minLines: 5,
            hintText: tr('enter_conditions'),
            initialValue: widget.inviteCustomerVm
                    .formValues[InviteCustomerFormKeys.offerConditions] ??
                context
                    .read<AppDrawerViewModel>()
                    .supplierProfileModel
                    ?.bCrmCompany
                    ?.companyOfferNote,
          ),
          RadioSaveConditions(
            initialValue: widget.inviteCustomerVm
                .formValues[InviteCustomerFormKeys.saveOfferConditions],
          ),
        ] else
          TextButton(
            style: TextButton.styleFrom(
              fixedSize: Size(MediaQuery.of(context).size.width, 45),
              shape: const RoundedRectangleBorder(
                side: BorderSide(
                  color: PartnerAppColors.blue,
                ),
              ),
            ),
            child: Text(
              tr('add_betingelser'),
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    color: PartnerAppColors.blue,
                  ),
            ),
            onPressed: () {
              setState(() {
                showCondition = true;
              });
            },
          ),
        const SizedBox(
          height: 10,
        ),
        const Divider(
          color: PartnerAppColors.darkBlue,
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('total_price_exl_vat'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: widget.inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalPriceWoVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('vat_value'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: widget.inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tr('total_include_vat'),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              Formatter.curencyFormat(
                  amount: widget.inviteCustomerVm.formValues[
                          InviteCustomerFormKeys.industryTotalPriceVat] ??
                      0),
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
      ],
    );
  }

  Widget companyName() {
    if ((widget.inviteCustomerVm
            .formValues[InviteCustomerFormKeys.companyNameEdit] ??
        false)) {
      return CustomTextFieldFormBuilder(
        name: InviteCustomerFormKeys.companyName,
        initialValue: widget.inviteCustomerVm
                .formValues[InviteCustomerFormKeys.companyName] ??
            '',
      );
    } else {
      return Text(
        widget.inviteCustomerVm
                .formValues[InviteCustomerFormKeys.companyName] ??
            '',
        style: Theme.of(context)
            .textTheme
            .titleMedium!
            .copyWith(fontWeight: FontWeight.bold),
      );
    }
  }
}
