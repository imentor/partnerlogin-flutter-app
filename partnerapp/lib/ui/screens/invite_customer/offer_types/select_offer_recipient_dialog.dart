import 'package:Haandvaerker.dk/model/contact/contact_model.dart';
import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

Future<void> selectOfferRecipientDialog(
    {required BuildContext context, required String offerType}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          GestureDetector(
              onTap: () {
                Navigator.of(dialogContext).pop();
              },
              child: const Icon(Icons.close))
        ]),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: SelectOfferRecipientDialog(
              offerType: offerType,
            ),
          ),
        ),
      );
    },
  );
}

class SelectOfferRecipientDialog extends StatefulWidget {
  const SelectOfferRecipientDialog({super.key, required this.offerType});

  final String offerType;

  @override
  State<SelectOfferRecipientDialog> createState() =>
      _SelectOfferRecipientDialogState();
}

class _SelectOfferRecipientDialogState
    extends State<SelectOfferRecipientDialog> {
  String? recipientType;

  final TextEditingController _controller = TextEditingController();
  final FocusNode _focusNode = FocusNode();

  Contact? homeowner;
  PartnerProjectsModel? project;

  @override
  void dispose() {
    _controller.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<InviteCustomerViewmodel>(
      builder: (_, inviteCustomerVm, __) {
        if (recipientType == InviteCustomerFormKeys.recipientTypeExisting) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              searchableHomeownerTextField(
                  context: context, inviteCustomerVm: inviteCustomerVm),
              searchableHomeownerProjectsTextField(
                  context: context, inviteCustomerVm: inviteCustomerVm),
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Expanded(
                    child: TextButton(
                      onPressed: () {
                        setState(() {
                          recipientType = null;
                          homeowner = null;
                        });

                        inviteCustomerVm.homeownerProjects = [];
                      },
                      style: TextButton.styleFrom(
                          fixedSize:
                              Size(MediaQuery.of(context).size.width, 55),
                          shape: const RoundedRectangleBorder(
                              side: BorderSide(
                                  color: PartnerAppColors.darkBlue))),
                      child: Text(
                        'Tilbage',
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(color: PartnerAppColors.darkBlue),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: TextButton(
                      onPressed: () =>
                          redirectExistingProject(inviteCustomerVm),
                      style: TextButton.styleFrom(
                          fixedSize:
                              Size(MediaQuery.of(context).size.width, 55),
                          backgroundColor: PartnerAppColors.malachite),
                      child: Text(
                        tr('next'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(color: Colors.white),
                      ),
                    ),
                  )
                ],
              )
            ],
          );
        }
        return Column(
          children: [
            Text(
              'Vælg modtager af tilbud',
              style: Theme.of(context)
                  .textTheme
                  .titleMedium!
                  .copyWith(fontWeight: FontWeight.normal),
            ),
            const SizedBox(
              height: 20,
            ),
            TextButton(
              onPressed: () {
                final inviteVm = context.read<InviteCustomerViewmodel>();

                Navigator.of(context).pop();
                inviteVm.updateFormValues(
                    key: InviteCustomerFormKeys.invitationType,
                    values: widget.offerType);
                inviteVm.updateFormValues(
                    key: InviteCustomerFormKeys.recipientType,
                    values: recipientType);
                inviteCustomerVm.currentStep += 1;
              },
              style: TextButton.styleFrom(
                  fixedSize: Size(MediaQuery.of(context).size.width, 55),
                  shape: const RoundedRectangleBorder(
                      side: BorderSide(color: PartnerAppColors.darkBlue))),
              child: Text(
                'Ny kunde',
                style: Theme.of(context)
                    .textTheme
                    .titleMedium!
                    .copyWith(color: PartnerAppColors.darkBlue),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            TextButton(
              onPressed: () => setState(() {
                recipientType = InviteCustomerFormKeys.recipientTypeExisting;
              }),
              style: TextButton.styleFrom(
                  fixedSize: Size(MediaQuery.of(context).size.width, 55),
                  shape: const RoundedRectangleBorder(
                      side: BorderSide(color: PartnerAppColors.darkBlue))),
              child: Text(
                'Fra “Mine kunder”',
                style: Theme.of(context)
                    .textTheme
                    .titleMedium!
                    .copyWith(color: PartnerAppColors.darkBlue),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget searchableHomeownerTextField(
      {required BuildContext context,
      required InviteCustomerViewmodel inviteCustomerVm}) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: 'Vælg kunde',
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.normal,
                    fontSize: 18),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        SizedBox(
          // margin: const EdgeInsets.symmetric(vertical: 10),
          child: FormBuilderField<Contact?>(
            name: InviteCustomerFormKeys.recipientTypeHomeowner,
            validator:
                FormBuilderValidators.required(errorText: tr('required')),
            builder: (FormFieldState field) {
              return InputDecorator(
                decoration: InputDecoration(
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.zero,
                  errorText: field.errorText,
                ),
                child: SizedBox(
                  height: 48,
                  child: TypeAheadField<Contact>(
                    builder: (context, controller, focusNode) {
                      return TextField(
                        controller: controller,
                        focusNode: focusNode,
                        decoration: InputDecoration(
                          hintStyle: Theme.of(context)
                              .textTheme
                              .headlineSmall!
                              .copyWith(
                                  fontWeight: FontWeight.normal,
                                  color: Colors.grey.shade600),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: PartnerAppColors.darkBlue
                                    .withValues(alpha: .4)),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(3)),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: PartnerAppColors.darkBlue
                                    .withValues(alpha: .4),
                                width: 0.8),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(3)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: PartnerAppColors.darkBlue
                                    .withValues(alpha: .4)),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(3)),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context).colorScheme.error,
                                width: 0.8),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(3)),
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: PartnerAppColors.darkBlue
                                    .withValues(alpha: .4),
                                width: 0.8),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(3)),
                          ),
                        ),
                      );
                    },
                    itemBuilder: (context, value) {
                      return ListTile(
                        title: Text(value.name ?? ''),
                      );
                    },
                    onSelected: (value) async {
                      field.didChange(value);
                      _controller.text = value.name ?? '';
                      _focusNode.unfocus();

                      setState(() {
                        homeowner = value;
                      });

                      inviteCustomerVm.getHomeownersProjects(
                          contactId: value.id!);
                    },
                    suggestionsCallback: (search) =>
                        inviteCustomerVm.searchHomeowner(query: search),
                    controller: _controller,
                    focusNode: _focusNode,
                    itemSeparatorBuilder: (context, index) => const Divider(
                      color: PartnerAppColors.darkBlue,
                    ),
                    emptyBuilder: (context) => Container(
                      padding: const EdgeInsets.all(10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              tr('empty'),
                              style: Theme.of(context).textTheme.titleMedium,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ]),
    );
  }

  Widget searchableHomeownerProjectsTextField(
      {required BuildContext context,
      required InviteCustomerViewmodel inviteCustomerVm}) {
    return Skeletonizer(
      enabled: inviteCustomerVm.busy,
      child: CustomDropdownFormBuilder(
          items: [
            ...inviteCustomerVm.homeownerProjects.map((value) {
              return DropdownMenuItem(
                  value: (value.id ?? 0).toString(),
                  child: Text(value.name ?? ''));
            })
          ],
          labelText: 'Project',
          hintText: 'Opret projekt',
          onChanged: (p0) => setState(() {
                project = inviteCustomerVm.homeownerProjects.firstWhere(
                  (value) => (value.id ?? 0).toString() == p0,
                  orElse: () => PartnerProjectsModel(),
                );
              }),
          name: InviteCustomerFormKeys.recipientTypeProject),
    );
  }

  Future<void> redirectExistingProject(
      InviteCustomerViewmodel inviteCustomerVm) async {
    final userVm = context.read<UserViewModel>();
    if (homeowner != null && project != null) {
      await inviteCustomerVm
          .initExistingProject(
              homeowner: homeowner!,
              project: project!,
              type: widget.offerType,
              companyName: userVm.userModel?.user?.name ?? '')
          .whenComplete(() {
        if (!mounted) return;
        Navigator.of(context).pop();
      });
    }
  }
}
