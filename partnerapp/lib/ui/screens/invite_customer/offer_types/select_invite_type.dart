import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/offer_types/select_offer_recipient_dialog.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:skeletonizer/skeletonizer.dart';

class SelectInviteType extends StatelessWidget {
  const SelectInviteType({
    super.key,
    required this.formKey,
    required this.inviteCustomerVm,
  });
  final InviteCustomerViewmodel inviteCustomerVm;
  final GlobalKey<FormBuilderState> formKey;

  @override
  Widget build(BuildContext context) {
    return Skeleton.keep(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            tr('create_offer'),
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            tr('3_ways_to_invite'),
            style: Theme.of(context)
                .textTheme
                .titleSmall!
                .copyWith(color: PartnerAppColors.spanishGrey),
          ),
          const SizedBox(
            height: 20,
          ),
          optionCard(context: context, type: InviteCustomerType.createOffer),
          const SizedBox(
            height: 10,
          ),
          optionCard(context: context, type: InviteCustomerType.uploadOffer),
          const SizedBox(
            height: 10,
          ),
          optionCard(context: context, type: InviteCustomerType.quickOffer),

          // CustomRadioGroupFormBuilder(
          //     optionsOrientation: OptionsOrientation.vertical,
          //     controlAffinity: ControlAffinity.trailing,
          //     itemDecoration: BoxDecoration(
          //       borderRadius: BorderRadius.circular(5),
          //       border: Border.all(
          //         color: PartnerAppColors.spanishGrey.withValues(alpha: .3),
          //       ),
          //     ),
          //     wrapSpacing: 10,
          //     initialValue: inviteCustomerVm
          //         .formValues[InviteCustomerFormKeys.invitationType],
          //     validator:
          //         FormBuilderValidators.required(errorText: tr('required')),
          //     onChanged: (p0) {
          //       if ((p0 ?? '').isNotEmpty) {
          //         selectOfferRecipientDialog(context: context, offerType: p0!);
          //       }
          //     },
          //     options: [
          //       FormBuilderFieldOption(
          //         value: InviteCustomerType.createOffer,
          //         child: optionCard(
          //             context: context, type: InviteCustomerType.createOffer),
          //       ),
          //       FormBuilderFieldOption(
          //         value: InviteCustomerType.uploadOffer,
          //         child: optionCard(
          //             context: context, type: InviteCustomerType.uploadOffer),
          //       ),
          //       FormBuilderFieldOption(
          //         value: InviteCustomerType.quickOffer,
          //         child: optionCard(
          //             context: context, type: InviteCustomerType.quickOffer),
          //       ),
          //     ],
          //     name: InviteCustomerFormKeys.invitationType),
        ],
      ),
    );
  }

  Widget optionCard({required BuildContext context, required String type}) {
    String typeSvg = '';
    String title = '';
    String description = '';

    switch (type) {
      case InviteCustomerType.createOffer:
        title = tr('extended_offer');
        typeSvg = SvgIcons.createOfferIcon;
        description = tr('invite_create_offer_description');
        break;
      case InviteCustomerType.uploadOffer:
        typeSvg = SvgIcons.photoDoc;
        title = tr('upload_file');
        description = tr('invite_upload_offer_description');
        break;
      case InviteCustomerType.quickOffer:
        typeSvg = SvgIcons.viewContract;
        title = tr('quick_offer');
        description = tr('invite_quick_offer_description');
        break;
      default:
    }

    return InkWell(
      onTap: () {
        selectOfferRecipientDialog(context: context, offerType: type);
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
            color: PartnerAppColors.spanishGrey.withValues(alpha: .3),
          ),
        ),
        padding: const EdgeInsets.all(20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SvgPicture.asset(
              typeSvg,
              fit: BoxFit.cover,
              height: 40,
              width: 40,
              colorFilter: const ColorFilter.mode(
                PartnerAppColors.blue,
                BlendMode.srcIn,
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    description,
                    style: Theme.of(context)
                        .textTheme
                        .titleSmall!
                        .copyWith(color: PartnerAppColors.spanishGrey),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
