import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:timeline_tile/timeline_tile.dart';

class TimelineStep extends StatelessWidget {
  const TimelineStep(
      {super.key, required this.inviteCustomerVm, required this.totalSteps});

  final InviteCustomerViewmodel inviteCustomerVm;
  final int totalSteps;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(
          height: 65,
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                ...List.generate(
                  totalSteps,
                  (index) {
                    final isCurrentPage =
                        (index + 1) <= inviteCustomerVm.currentStep;

                    return TimelineTile(
                      axis: TimelineAxis.horizontal,
                      alignment: TimelineAlign.manual,
                      isFirst: index == 0,
                      isLast: index == totalSteps - 1,
                      indicatorStyle: IndicatorStyle(
                        width: 30.0,
                        height: 30.0,
                        indicator: DecoratedBox(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(color: PartnerAppColors.blue),
                            color: isCurrentPage
                                ? PartnerAppColors.blue
                                : Colors.white,
                          ),
                          child: Center(
                              child: Text('${index + 1}',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodySmall!
                                      .copyWith(
                                          color: isCurrentPage
                                              ? Colors.white
                                              : PartnerAppColors.blue,
                                          fontWeight: FontWeight.bold))),
                        ),
                      ),
                      beforeLineStyle: const LineStyle(
                          thickness: 1.0, color: PartnerAppColors.spanishGrey),
                      lineXY: .1,
                      endChild: Container(
                        constraints: BoxConstraints(
                            minWidth:
                                MediaQuery.of(context).size.width / totalSteps),
                        margin: const EdgeInsets.only(top: 5),
                        child: Text('${tr('step')} ${index + 1}',
                            style: Theme.of(context).textTheme.bodySmall,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.center),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
