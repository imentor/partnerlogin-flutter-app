import 'package:Haandvaerker.dk/model/wizard/wizard_response_new_version_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class ProductDynamicForm extends StatelessWidget {
  final ProductWizardModelV2 productWizard;
  final String label;
  final int currentStep;
  final int totalStep;
  final InviteCustomerViewmodel inviteCustomerVm;

  const ProductDynamicForm({
    super.key,
    required this.label,
    required this.productWizard,
    required this.currentStep,
    required this.totalStep,
    required this.inviteCustomerVm,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(label,
            style: Theme.of(context).textTheme.titleMedium!.copyWith(
                color: PartnerAppColors.darkBlue, fontWeight: FontWeight.bold)),
        const SizedBox(
          height: 10,
        ),
        Text('${tr('step')} $currentStep / $totalStep',
            style: Theme.of(context)
                .textTheme
                .headlineSmall!
                .copyWith(color: PartnerAppColors.spanishGrey)),
        const SizedBox(
          height: 10,
        ),
        ...formsV2(context)
      ],
    );
  }

  List<Widget> formsV2(BuildContext context) {
    if (productWizard.productFields == null) {
      return [const SizedBox.shrink()];
    }

    return productWizard.productFields!.map((producField) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          buildFormField(context: context, producField: producField),
          const Divider(
            color: PartnerAppColors.darkBlue,
          )
        ],
      );
    }).toList();
  }

  Widget buildFormField(
      {required BuildContext context,
      required ProductWizardFieldV2 producField}) {
    switch (producField.productField) {
      case 1:
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Text(
            producField.productName ?? '',
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                letterSpacing: 1.0,
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.normal,
                fontSize: 18),
          ),
        );
      case 2:
        return buildTextFieldFormBuilder(
          key: 'text_2_${productWizard.productId}_${producField.id}',
          producField: producField,
        );
      case 3:
        return buildRadioGroupFormBuilder(
          context: context,
          key: 'radio_3_${productWizard.productId}_${producField.id}',
          producField: producField,
        );
      case 4:
        return buildCheckboxGroupFormBuilder(
          context: context,
          key: 'checkbox_4_${productWizard.productId}_',
          producField: producField,
        );
      default:
        return const SizedBox.shrink();
    }
  }

  Widget buildTextFieldFormBuilder(
      {required String key, required ProductWizardFieldV2 producField}) {
    String? initialValue;

    if (inviteCustomerVm.formValues
        .containsKey(InviteCustomerFormKeys.productAnswers)) {
      initialValue = inviteCustomerVm
              .formValues[InviteCustomerFormKeys.productAnswers]?[key] ??
          '';
    }

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: CustomTextFieldFormBuilder(
        labelText: producField.productName,
        isRequired: producField.mandatory == 1,
        validator: producField.mandatory == 1
            ? FormBuilderValidators.required(errorText: tr('required'))
            : null,
        name: key,
        initialValue: initialValue,
        keyboardType: producField.onlyDescription == 1
            ? TextInputType.text
            : TextInputType.number,
      ),
    );
  }

  Widget buildRadioGroupFormBuilder({
    required BuildContext context,
    required String key,
    required ProductWizardFieldV2 producField,
  }) {
    String? initialValue;

    if (inviteCustomerVm.formValues
        .containsKey(InviteCustomerFormKeys.productAnswers)) {
      initialValue = inviteCustomerVm
              .formValues[InviteCustomerFormKeys.productAnswers]?[key] ??
          '';
    }

    List<FormBuilderFieldOption<String>> options =
        producField.values?.map((childValue) {
              return FormBuilderFieldOption(
                value: childValue.id!,
                child: Text(
                  childValue.productName!,
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                      ),
                ),
              );
            }).toList() ??
            [];

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomRadioGroupFormBuilder(
            labelText: producField.productName,
            isRequired: producField.mandatory == 1,
            validator: producField.mandatory == 1
                ? FormBuilderValidators.required(errorText: tr('required'))
                : null,
            options: options,
            name: key,
            initialValue: initialValue,
            onChanged: (p0) {
              if (p0 != null) {
                inviteCustomerVm.updateWizardProductAnswers(
                    key: key, value: p0);
              }
            },
          ),
          if (producField.children != null && producField.children!.isNotEmpty)
            ...producField.children!.map((child) {
              if (!inviteCustomerVm.formValues
                  .containsKey(InviteCustomerFormKeys.productAnswers)) {
                return const SizedBox.shrink();
              }

              return (inviteCustomerVm
                              .formValues[InviteCustomerFormKeys.productAnswers]
                          as Map)
                      .containsValue(child.parentproductid.toString())
                  ? buildFormField(
                      context: context,
                      producField: child,
                    )
                  : const SizedBox.shrink();
            })
        ],
      ),
    );
  }

  Widget buildCheckboxGroupFormBuilder({
    required BuildContext context,
    required String key,
    required ProductWizardFieldV2 producField,
  }) {
    List<String>? initialValue;

    if (inviteCustomerVm.formValues
            .containsKey(InviteCustomerFormKeys.productAnswers) &&
        ((inviteCustomerVm.formValues[InviteCustomerFormKeys.productAnswers]
                    ?[key] as String?) ??
                '')
            .isNotEmpty) {
      initialValue = [
        inviteCustomerVm.formValues[InviteCustomerFormKeys.productAnswers]
                ?[key] ??
            ''
      ];
    }

    List<FormBuilderFieldOption<String>> options =
        producField.values?.map((childValue) {
              return FormBuilderFieldOption(
                value: childValue.id!,
                child: Text(
                  childValue.productName!,
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                      ),
                ),
              );
            }).toList() ??
            [];

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomCheckboxGroupFormBuilder(
            labelText: producField.productName,
            isRequired: producField.mandatory == 1,
            optionsOrientation: OptionsOrientation.vertical,
            validator: producField.mandatory == 1
                ? FormBuilderValidators.required(errorText: tr('required'))
                : null,
            options: options,
            name: key,
            initialValue: initialValue,
            onChanged: (p0) {
              if (p0 != null) {
                inviteCustomerVm.updateWizardProductAnswers(
                    key: key,
                    value: '',
                    options: [
                      ...options.map((optionValue) => optionValue.value)
                    ],
                    selectedChecks: p0);
              }
            },
          ),
          if (producField.children != null && producField.children!.isNotEmpty)
            ...producField.children!.map((child) {
              if (!inviteCustomerVm.formValues
                  .containsKey(InviteCustomerFormKeys.productAnswers)) {
                return const SizedBox.shrink();
              }

              return (inviteCustomerVm
                              .formValues[InviteCustomerFormKeys.productAnswers]
                          as Map)
                      .containsValue(child.parentproductid.toString())
                  ? buildFormField(context: context, producField: child)
                  : const SizedBox.shrink();
            })
        ],
      ),
    );
  }
}
