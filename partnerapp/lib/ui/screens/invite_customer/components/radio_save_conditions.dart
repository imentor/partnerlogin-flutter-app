import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class RadioSaveConditions extends StatelessWidget {
  const RadioSaveConditions({super.key, this.initialValue});

  final bool? initialValue;

  @override
  Widget build(BuildContext context) {
    return FormBuilderField<bool?>(
        name: InviteCustomerFormKeys.saveOfferConditions,
        initialValue: initialValue,
        builder: (FormFieldState field) {
          return InputDecorator(
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.zero,
              errorText: field.errorText,
            ),
            child: Row(
              children: [
                InkWell(
                  onTap: () {
                    field.didChange(!(field.value ?? false));
                  },
                  child: SizedBox(
                    height: 24,
                    width: 24,
                    child: AbsorbPointer(
                      absorbing: true,
                      child: Radio(
                        value: true,
                        groupValue: field.value,
                        toggleable: false,
                        activeColor: PartnerAppColors.blue,
                        onChanged: (value) {
                          // field.didChange(value);
                        },
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Text(
                  tr('save_as_default_text'),
                  style: Theme.of(context)
                      .textTheme
                      .titleSmall!
                      .copyWith(color: PartnerAppColors.spanishGrey),
                ),
              ],
            ),
          );
        });
  }
}
