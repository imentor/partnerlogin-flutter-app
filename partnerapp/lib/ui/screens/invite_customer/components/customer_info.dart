import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class CustomerInfo extends StatefulWidget {
  const CustomerInfo(
      {super.key,
      required this.inviteCustomerVm,
      required this.title,
      required this.formKey});

  final InviteCustomerViewmodel inviteCustomerVm;
  final String title;
  final GlobalKey<FormBuilderState> formKey;

  @override
  State<CustomerInfo> createState() => _CustomerInfoState();
}

class _CustomerInfoState extends State<CustomerInfo> {
  final TextEditingController _controller = TextEditingController();
  final FocusNode _focusNode = FocusNode();

  @override
  void dispose() {
    _controller.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.title,
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          tr('enter_customer_info_here'),
          style: Theme.of(context)
              .textTheme
              .titleSmall!
              .copyWith(color: PartnerAppColors.spanishGrey),
        ),
        const SizedBox(
          height: 20,
        ),
        CustomTextFieldFormBuilder(
          name: InviteCustomerFormKeys.customerFirstName,
          labelText: tr('first_name'),
          initialValue: widget.inviteCustomerVm
              .formValues[InviteCustomerFormKeys.customerFirstName],
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
        CustomTextFieldFormBuilder(
          name: InviteCustomerFormKeys.customerLastName,
          labelText: tr('last_name'),
          initialValue: widget.inviteCustomerVm
              .formValues[InviteCustomerFormKeys.customerLastName],
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
        CustomTextFieldFormBuilder(
          name: InviteCustomerFormKeys.customerMobile,
          initialValue: widget.inviteCustomerVm
              .formValues[InviteCustomerFormKeys.customerMobile],
          labelText: tr('phone_number'),
          validator: (value) {
            if ((value ?? '').isEmpty) {
              return tr('required');
            } else if ((value ?? '').length < 8) {
              return tr('please_enter_valid_mobile_number');
            }

            return null;
          },
          keyboardType: TextInputType.phone,
          inputFormatters: [
            LengthLimitingTextInputFormatter(8),
          ],
        ),
        searchableTextField(context),
        CustomTextFieldFormBuilder(
          name: InviteCustomerFormKeys.customerEmail,
          labelText: 'E-mail',
          initialValue: widget.inviteCustomerVm
              .formValues[InviteCustomerFormKeys.customerEmail],
          validator: FormBuilderValidators.compose([
            FormBuilderValidators.required(errorText: tr('required')),
            FormBuilderValidators.email(errorText: tr('enter_valid_email'))
          ]),
        ),
      ],
    );
  }

  Widget searchableTextField(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: tr('address'),
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.normal,
                    fontSize: 18),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        SizedBox(
          // margin: const EdgeInsets.symmetric(vertical: 10),
          child: FormBuilderField<Address?>(
            name: InviteCustomerFormKeys.customerAddress,
            initialValue: widget.inviteCustomerVm
                .formValues[InviteCustomerFormKeys.customerAddress],
            validator:
                FormBuilderValidators.required(errorText: tr('required')),
            builder: (FormFieldState field) {
              return InputDecorator(
                decoration: InputDecoration(
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.zero,
                  errorText: field.errorText,
                ),
                child: SizedBox(
                  height: 48,
                  child: TypeAheadField<Address>(
                    builder: (context, controller, focusNode) {
                      return TextField(
                        controller: controller,
                        focusNode: focusNode,
                        decoration: InputDecoration(
                          hintStyle: Theme.of(context)
                              .textTheme
                              .headlineSmall!
                              .copyWith(
                                  fontWeight: FontWeight.normal,
                                  color: Colors.grey.shade600),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: PartnerAppColors.darkBlue
                                    .withValues(alpha: .4)),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(3)),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: PartnerAppColors.darkBlue
                                    .withValues(alpha: .4),
                                width: 0.8),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(3)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: PartnerAppColors.darkBlue
                                    .withValues(alpha: .4)),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(3)),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context).colorScheme.error,
                                width: 0.8),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(3)),
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: PartnerAppColors.darkBlue
                                    .withValues(alpha: .4),
                                width: 0.8),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(3)),
                          ),
                        ),
                      );
                    },
                    itemBuilder: (context, value) {
                      return ListTile(
                        title: Text(value.address ?? ''),
                      );
                    },
                    onSelected: (value) {
                      field.didChange(value);
                      _controller.text = value.address ?? '';
                    },
                    suggestionsCallback: (search) =>
                        widget.inviteCustomerVm.searchAddress(query: search),
                    controller: _controller,
                    focusNode: _focusNode,
                    itemSeparatorBuilder: (context, index) => const Divider(
                      color: PartnerAppColors.darkBlue,
                    ),
                    emptyBuilder: (context) => Container(
                      padding: const EdgeInsets.all(10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              tr('empty'),
                              style: Theme.of(context).textTheme.titleMedium,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ]),
    );
  }
}
