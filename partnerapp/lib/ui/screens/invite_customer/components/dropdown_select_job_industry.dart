import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/string_utils.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class DropdownSelectJobIndustry extends StatefulWidget {
  const DropdownSelectJobIndustry({super.key, required this.inviteCustomerVm});
  final InviteCustomerViewmodel inviteCustomerVm;

  @override
  State<DropdownSelectJobIndustry> createState() =>
      _DropdownSelectJobIndustryState();
}

class _DropdownSelectJobIndustryState extends State<DropdownSelectJobIndustry> {
  final _formKey = GlobalKey<FormState>();
  final _popupCustomValidationKeyWizard =
      GlobalKey<DropdownSearchState<AllWizardText>>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: dropdownIndustryList(),
    );
  }

  Widget dropdownIndustryList() {
    return FormBuilderField<List<AllWizardText>?>(
      name: InviteCustomerFormKeys.selectedIndustry,
      initialValue: widget.inviteCustomerVm
              .formValues[InviteCustomerFormKeys.selectedIndustry] ??
          [],
      validator: (value) {
        if ((value ?? []).isEmpty) {
          return tr('required');
        }
        return null;
      },
      builder: (field) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            DropdownSearch<AllWizardText>.multiSelection(
              mode: Mode.form,
              items: (filter, loadProps) =>
                  widget.inviteCustomerVm.industryList,
              compareFn: (item1, item2) => identical(item1, item2),
              filterFn: null,
              itemAsString: (item) => (item.producttypeDa ?? '').toTitleCase(),
              decoratorProps: DropDownDecoratorProps(
                  decoration: InputDecoration(
                hintText: tr('search'),
                filled: true,
                isDense: true,
                errorMaxLines: 3,
                prefixIconConstraints: const BoxConstraints(),
                fillColor: Colors.white,
                prefixIcon: const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Icon(
                    FeatherIcons.search,
                    size: 25,
                    color: PartnerAppColors.spanishGrey,
                  ),
                ),
                hintStyle: Theme.of(context).textTheme.headlineSmall!.copyWith(
                    fontWeight: FontWeight.normal, color: Colors.grey.shade600),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: PartnerAppColors.darkBlue.withValues(alpha: .4)),
                  borderRadius: const BorderRadius.all(Radius.circular(3)),
                ),
                disabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: PartnerAppColors.darkBlue.withValues(alpha: .4),
                      width: 0.8),
                  borderRadius: const BorderRadius.all(Radius.circular(3)),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: PartnerAppColors.darkBlue.withValues(alpha: .4)),
                  borderRadius: const BorderRadius.all(Radius.circular(3)),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context).colorScheme.error, width: 0.8),
                  borderRadius: const BorderRadius.all(Radius.circular(3)),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: PartnerAppColors.darkBlue.withValues(alpha: .4),
                      width: 0.8),
                  borderRadius: const BorderRadius.all(Radius.circular(3)),
                ),
              )),
              key: _popupCustomValidationKeyWizard,
              selectedItems: widget.inviteCustomerVm
                      .formValues[InviteCustomerFormKeys.selectedIndustry] ??
                  [],
              dropdownBuilder: (context, selectedItems) {
                return Wrap(
                  children: [
                    ...selectedItems.map((item) {
                      return Container(
                        padding: const EdgeInsets.all(10),
                        margin: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          color: PartnerAppColors.blue.withValues(alpha: .2),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: RichText(
                          text: TextSpan(
                            text:
                                (item.producttypeDa ?? '').toCapitalizedFirst(),
                            style: Theme.of(context)
                                .textTheme
                                .titleSmall!
                                .copyWith(
                                  color: PartnerAppColors.blue,
                                  fontWeight: FontWeight.bold,
                                ),
                            children: [
                              const WidgetSpan(
                                child: SizedBox(
                                  width: 10,
                                ),
                              ),
                              WidgetSpan(
                                  child: InkWell(
                                    onTap: () {
                                      _popupCustomValidationKeyWizard
                                          .currentState!
                                          .removeItem(item);
                                      field.didChange(
                                          _popupCustomValidationKeyWizard
                                              .currentState!.getSelectedItems);
                                    },
                                    child: const Icon(
                                      FeatherIcons.x,
                                      size: 20,
                                      color: PartnerAppColors.blue,
                                    ),
                                  ),
                                  alignment: PlaceholderAlignment.middle),
                            ],
                          ),
                        ),
                      );
                    })
                  ],
                );
              },
              popupProps: PopupPropsMultiSelection.menu(
                validationBuilder: (context, items) => const SizedBox.shrink(),
                interceptCallBacks: true,
                showSelectedItems: true,
                itemBuilder: (context, value, isDisabled, isSelected) {
                  return InkWell(
                    onTap: () {
                      _popupCustomValidationKeyWizard.currentState!
                          .popupSelectItems([value]);
                      _popupCustomValidationKeyWizard.currentState
                          ?.popupOnValidate();
                      field.didChange(_popupCustomValidationKeyWizard
                          .currentState!.getSelectedItems);
                    },
                    child: Container(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        children: [
                          SizedBox(
                            height: 50,
                            width: 50,
                            child: CachedNetworkImage(
                              imageUrl:
                                  'https://newcrm.kundematch.dk/upload/products/${value.mpic}',
                              fit: BoxFit.fill,
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  value.producttypeDa ?? 'N/A',
                                  style: Theme.of(context)
                                      .textTheme
                                      .displaySmall!
                                      .copyWith(
                                          fontSize: 14,
                                          color: isSelected
                                              ? PartnerAppColors.blue
                                              : PartnerAppColors.darkBlue),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  value.generalDescription ?? 'N/A',
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: Theme.of(context)
                                      .textTheme
                                      .displaySmall!
                                      .copyWith(
                                          fontSize: 12,
                                          color: isSelected
                                              ? PartnerAppColors.blue
                                              : PartnerAppColors.spanishGrey),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                },
                checkBoxBuilder: (context, item, isDisabled, isSelected) =>
                    const SizedBox.shrink(),
                showSearchBox: false,
              ),
            ),
            if (field.errorText != null)
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  (field.errorText ?? ''),
                  style: Theme.of(context)
                      .textTheme
                      .titleSmall!
                      .copyWith(color: PartnerAppColors.red),
                ),
              )
          ],
        );
      },
    );
  }
}
