import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class IntroScreen extends StatefulWidget {
  const IntroScreen({super.key});

  @override
  IntroScreenState createState() => IntroScreenState();
}

class IntroScreenState extends State<IntroScreen> {
  var coverUrl = 'assets/images/intro_cover.jpeg';
  var bannerLogo = 'assets/images/new-haandvaerker-logo.png';

  /// Load images initially for the intro screen
  Future<void> cacheImages() async {
    await Future.wait([
      precacheImage(AssetImage(coverUrl), context),
      precacheImage(AssetImage(bannerLogo), context)
    ]);
  }

  /// Methods inside here are executed under the `WidgetsBinding.instance.addPostFrameCallback`
  Future initialise(BuildContext context) async {
    await cacheImages();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      initialise(context);
      FlutterNativeSplash.remove();
    });
  }

  @override
  Widget build(BuildContext context) {
    modalManager.setContext(context);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: <Widget>[
                Image.asset(
                  coverUrl,
                  height: screenSize.height * .40,
                  width: double.infinity,
                  fit: BoxFit.cover,
                ),
                Positioned(
                  child: SafeArea(
                    child: Center(
                      child: Image.asset(
                        bannerLogo,
                        width: 130,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: ListView(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                children: [
                  Container(
                    width: screenSize.width,
                    padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        const Icon(
                          FontAwesomeIcons.quoteLeft,
                          color: Color(0xffafe3e5),
                        ),
                        SmartGaps.gapH10,
                        RichText(
                          textAlign: TextAlign.justify,
                          text: TextSpan(
                            text:
                                'Det gode ved Håndværker.dk er, at kunderne har ',
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.normal,
                                ),
                            children: const [
                              TextSpan(
                                text: 'tillid',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                  text:
                                      ' til mig, allerede inden jeg kommer ud til dem, fordi de har læst mine'),
                              TextSpan(
                                text: ' anbefalinger',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              TextSpan(text: '. Det er lettere at få'),
                              TextSpan(
                                text: ' salget i hus',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              TextSpan(text: ', fordi det giver mine kunder'),
                              TextSpan(
                                text: ' tryghed.',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        SmartGaps.gapH16,
                        Text(
                          'Jan Jørgensen',
                          style: Theme.of(context)
                              .textTheme
                              .titleSmall!
                              .copyWith(fontSize: 16.0),
                        ),
                        Text(
                          'Jysk Flise & Fugeteknik',
                          style: Theme.of(context)
                              .textTheme
                              .bodyMedium!
                              .copyWith(fontSize: 16.0),
                        ),
                        SmartGaps.gapH16,
                      ],
                    ),
                  ),
                  SmartGaps.gapH10,
                  SizedBox(
                    height: screenSize.height * 0.08,
                    width: double.infinity,
                    child: CustomDesignTheme.flatButtonStyle(
                      backgroundColor: Theme.of(context).colorScheme.primary,
                      onPressed: () async {
                        Navigator.pushNamed(context, Routes.login);
                      },
                      child: Text(
                        tr('login').toUpperCase(),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(color: Colors.white),
                      ),
                    ),
                  ),
                  SmartGaps.gapH15,
                  // if (!Platform.isIOS)
                  //   Container(
                  //     height: screenSize.height * 0.08,
                  //     width: double.infinity,
                  //     decoration: BoxDecoration(
                  //         border: Border.all(
                  //             color: Theme.of(context).colorScheme.primary)),
                  //     child: CustomDesignTheme.flatButtonStyle(
                  //       onPressed: () async {
                  //         Navigator.pushNamed(context, Routes.leadForm);
                  //       },
                  //       child: Text(
                  //         tr('try_now').toUpperCase(),
                  //         style: Theme.of(context)
                  //             .textTheme
                  //             .headlineMedium!
                  //             .copyWith(
                  //                 color: Theme.of(context).colorScheme.primary),
                  //       ),
                  //     ),
                  //   ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CarouselContent1 extends StatelessWidget {
  const CarouselContent1({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        SmartGaps.gapH40,
        Text(
          'Med +1 mio. besøgende husejere on året er Håndværker.dk den største håndværker-portal i Danmark.',
          overflow: TextOverflow.fade,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyMedium,
        ),
        SmartGaps.gapH20,
      ],
    );
  }
}

class CarouselContent2 extends StatelessWidget {
  const CarouselContent2({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SmartGaps.gapH40,
        Text(
          'Med Håndværker.dk’s app i hånden kan du nemt tilgå dine nuværende arbejdsopgaver, men også nye potentielle opgaver, som ligger og venter på dig.',
          overflow: TextOverflow.fade,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyMedium,
        ),
      ],
    );
  }
}

class CarouselContent3 extends StatelessWidget {
  const CarouselContent3({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SmartGaps.gapH40,
        Text(
          'Brug Mester-til-Mester både når du har tid til overs og kan tage ekstra arbejde ind, eller når du har travlt  og har brug for mere arbejdskraft.',
          overflow: TextOverflow.fade,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyMedium,
        ),
        SmartGaps.gapH20,
      ],
    );
  }
}
