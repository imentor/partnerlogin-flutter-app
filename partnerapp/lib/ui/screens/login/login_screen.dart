import 'dart:developer';

import 'package:Haandvaerker.dk/model/partner/partner_login_model.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/custom_button.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/version_update/update_overlay.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/version_checker/version_checker_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:show_up_animation/show_up_animation.dart';
import 'package:validators/validators.dart';

Future<OtherAccounts?> chooseOtherAccounts(
    {required BuildContext context,
    required List<OtherAccounts> otherAccounts}) {
  return showDialog(
    barrierDismissible: false,
    context: context,
    builder: (dialogContext) {
      return Dialog.fullscreen(
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                Flexible(
                  child: Text(
                    '${tr('welcome_back')}, ${otherAccounts.first.company}!',
                    style: Theme.of(context)
                        .textTheme
                        .headlineLarge!
                        .copyWith(fontWeight: FontWeight.bold),
                  ),
                ),
              ]),
              SmartGaps.gapH20,
              Text(
                tr('select_account_continue'),
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: PartnerAppColors.grey1,
                    fontWeight: FontWeight.normal),
              ),
              SmartGaps.gapH20,
              ...otherAccounts.map(
                (e) => InkWell(
                  onTap: () {
                    Navigator.of(context).pop(e);
                  },
                  child: Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              e.company ?? '',
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineMedium!
                                  .copyWith(fontWeight: FontWeight.normal),
                            ),
                            SmartGaps.gapW10,
                            const Icon(
                              FeatherIcons.chevronRight,
                              color: PartnerAppColors.darkBlue,
                            ),
                          ],
                        ),
                        SmartGaps.gapH10,
                        const Divider(color: PartnerAppColors.grey1),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if (context
          .read<VersionCheckerViewmodel>()
          .appUpgrader
          .isUpdateAvailable()) {
        showUpdateOverlay(context);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final isFetching =
        Provider.of<UserViewModel>(context, listen: true).isFetching;
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    return Scaffold(
        body: SafeArea(
      child: SingleChildScrollView(
        // physics: NeverScrollableScrollPhysics(),
        child: Container(
            constraints: const BoxConstraints(maxWidth: 400),
            margin: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SmartGaps.gapH10,
                GestureDetector(
                  onTap: () {
                    Navigator.pushReplacementNamed(context, Routes.intro);
                  },
                  child: Text(
                    tr('back'),
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        color: Theme.of(context).colorScheme.primary,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                Container(height: 80.0),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      child: Image.asset(
                        ImagePaths.logo,
                        height: 60.0,
                        width: 60.0,
                      ),
                    ),
                    SmartGaps.gapW10,
                    Text(
                      'Håndværker.dk',
                      style: Theme.of(context)
                          .textTheme
                          .headlineLarge!
                          .copyWith(fontSize: 36.0),
                    ),
                  ],
                ),
                SmartGaps.gapH50,
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        tr('login'),
                        textAlign: TextAlign.left,
                        style: Theme.of(context).textTheme.headlineLarge,
                      ),
                    ),
                  ],
                ),
                SmartGaps.gapH10,
                LoginForm(isFetching: isFetching),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, Routes.forgotPassword);
                  },
                  child: Text(
                    tr('forgot_your_password'),
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        color: Theme.of(context).colorScheme.primary,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                SmartGaps.gapH15,
                isFetching
                    ? Center(
                        child: SizedBox(height: 25, width: 25, child: loader()),
                      )
                    : Container()
                // if (!Platform.isIOS)
                //   Stack(
                //     alignment: AlignmentDirectional.center,
                //     children: <Widget>[
                //       Container(
                //         height: 50,
                //         width: double.infinity,
                //         alignment: Alignment.centerLeft,
                //         child: InkWell(
                //           onTap: () {
                //             Navigator.pushNamed(context, Routes.leadForm);
                //           },
                //           child: Text(
                //             tr('not_registered'),
                //             style: const TextStyle(
                //                 fontSize: 15, color: Colors.black54),
                //           ),
                //         ),
                //       ),
                //     ],
                //   ),
              ],
            )),
      ),
    ));
  }
}

class LoginForm extends StatefulWidget {
  const LoginForm({super.key, required this.isFetching});
  final bool isFetching;

  @override
  LoginFormState createState() => LoginFormState();
}

class LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();

  final _emailController = TextEditingController(text: '');
  final _passwordController = TextEditingController(text: '');
  bool _showPassword = false;
  bool _showErrorBorder = false;
  bool _validated = false;

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final vm = context.read<UserViewModel>();

      await vm.checkRememberMe().then((value) {
        _emailController.text = value['userName'] ?? '';
        _passwordController.text = value['password'] ?? '';
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passwordController.dispose();
  }

  void onPressLogin({required UserViewModel userVm}) async {
    if (!widget.isFetching) {
      showBorderAndValidate(false);
      userVm.startFetching();

      if (_formKey.currentState!.validate()) {
        final user = _emailController.value.text.trim();
        final pass = _passwordController.value.text.trim();

        try {
          await userVm.login(user, pass).whenComplete(() async {
            if (!mounted) return;

            userVm.stopFetching();

            if (userVm.otherAccounts.isEmpty) {
              if (userVm.isFetching == false && userVm.isSuccess == true) {
                Navigator.pushNamedAndRemoveUntil(
                    context, Routes.loadingScreen, (r) => false);
              } else if (userVm.isFetching == false &&
                  userVm.isSuccess == false) {
                setState(() {
                  _showErrorBorder = true;
                  _validated = true;
                });
              }
            } else {
              await chooseOtherAccounts(
                      context: context, otherAccounts: userVm.otherAccounts)
                  .then((value) async {
                if (value != null) {
                  userVm.startFetching();
                  await userVm
                      .loginCvr(cvr: value.cvr ?? '', password: pass)
                      .whenComplete(() {
                    userVm.stopFetching();
                    if (userVm.isFetching == false &&
                        userVm.isSuccess == true &&
                        mounted) {
                      Navigator.pushNamedAndRemoveUntil(
                          context, Routes.loadingScreen, (r) => false);
                    } else if (userVm.isFetching == false &&
                        userVm.isSuccess == false) {
                      if (!mounted) return;
                      setState(() {
                        _showErrorBorder = true;
                        _validated = true;
                      });
                    }
                  });
                }
              });
            }
          });
        } on ApiException catch (e) {
          userVm.stopFetching();
          if (e.message.contains('credentials')) {
            if (!(userVm.isFetching) && !(userVm.isSuccess ?? false)) {
              setState(() {
                _showErrorBorder = true;
                _validated = true;
              });
            }
          } else {
            final errorMessage = e.message;
            await Sentry.captureMessage('TypeError: ${e.message}')
                .then((_) async {
              if (!mounted) return;

              commonSuccessOrFailedDialog(
                  context: context,
                  isSuccess: false,
                  showTitle: false,
                  message: errorMessage);

              // await showOkAlertDialog(
              //   context: context,
              //   message: errorMessage,
              //   okLabel: tr('ok'),
              // );
            });
          }
        }
      }
    } else {
      log('Still fetching data, please wait ...');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(children: <Widget>[
        Container(
          decoration: BoxDecoration(
              border: Border.all(
                  color: _showErrorBorder ? Colors.red : Colors.transparent)),
          child: TextFormField(
              maxLength: 320,
              onChanged: (value) {
                showBorderAndValidate(false);
              },
              key: const Key(Keys.loginEmailTF),
              controller: _emailController,
              style: const TextStyle(
                  fontSize: 18, color: Color.fromRGBO(0, 54, 69, 1)),
              decoration: InputDecoration(
                  counterText: '',
                  filled: true,
                  fillColor: const Color.fromRGBO(223, 230, 233, 0.8),
                  border: InputBorder.none,
                  hintStyle: const TextStyle(fontSize: 18),
                  hintText: tr('email')),
              keyboardType: TextInputType.emailAddress,
              validator: (value) {
                if (value == null || value.trim().isEmpty) {
                  return tr('please_enter_some_text');
                }
                if (isEmail(value.trim()) == false) {
                  return tr('enter_valid_email');
                }
                return null;
              }),
        ),
        SmartGaps.gapH16,
        Container(
          decoration: BoxDecoration(
              border: Border.all(
                  color: _showErrorBorder ? Colors.red : Colors.transparent)),
          child: TextFormField(
              maxLength: 64,
              key: const Key(Keys.loginPasswordTF),
              controller: _passwordController,
              obscureText: !_showPassword,
              style: const TextStyle(
                  fontSize: 18, color: Color.fromRGBO(0, 54, 69, 1)),
              onChanged: (value) {
                showBorderAndValidate(false);
              },
              decoration: InputDecoration(
                  counterText: '',
                  border: InputBorder.none,
                  suffixIcon: IconButton(
                      icon: Icon(
                        _showPassword ? Icons.visibility_off : Icons.visibility,
                        color: Theme.of(context).colorScheme.primary,
                      ),
                      onPressed: () {
                        setState(() {
                          _showPassword = !_showPassword;
                        });
                      }),
                  filled: true,
                  fillColor: const Color.fromRGBO(223, 230, 233, 0.8),
                  hintStyle: const TextStyle(fontSize: 18),
                  hintText: tr('password')),
              validator: (value) {
                if (value!.isEmpty) {
                  return tr('please_enter_some_text');
                }
                return null;
              }),
        ),
        Consumer<UserViewModel>(builder: (_, userVm, widget) {
          if (_validated &&
              !userVm.isFetching &&
              ((userVm.isSuccess ?? false) == false ||
                  userVm.isSuccess == null)) {
            return ShowUpAnimation(
                //delayStart: Duration(seconds: 1),
                animationDuration: const Duration(milliseconds: 300),
                curve: Curves.ease,
                direction: Direction.vertical,
                offset: -0.5,
                child: Container(
                  width: double.infinity,
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    tr('invalid_username_or_password'),
                    textAlign: TextAlign.start,
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        color: Colors.red,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w200),
                  ),
                ));
          } else {
            return Container();
          }
        }),
        const RememberMeSection(),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: SizedBox(
              height: 70,
              width: double.infinity,
              child: Consumer<UserViewModel>(
                builder: (context, userVm, child) {
                  return SubmitButton(
                    backgroundColor: Theme.of(context).colorScheme.primary,
                    text: tr('login'),
                    onPressed: () => onPressLogin(userVm: userVm),
                  );
                },
              )),
        ),
      ]),
    );
  }

  void showBorderAndValidate(bool value) {
    if (!mounted) return;

    if (_showErrorBorder) {
      setState(() {
        _showErrorBorder = value;
      });
    }

    if (_validated) {
      setState(() {
        _validated = value;
      });
    }
  }
}

class RememberMeSection extends StatelessWidget {
  const RememberMeSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 20),
        child: Row(
          children: [
            SizedBox(
              height: 20,
              width: 20,
              child: Consumer<UserViewModel>(
                builder: (_, vm, widget) {
                  return Checkbox(
                      activeColor: Theme.of(context).colorScheme.secondary,
                      onChanged: (value) {
                        vm.rememberMe = value;
                      },
                      value: vm.rememberMe);
                },
              ),
            ),
            SmartGaps.gapW10,
            Text(tr('remember_me'),
                style: Theme.of(context)
                    .textTheme
                    .bodyMedium!
                    .copyWith(fontWeight: FontWeight.w600)),
          ],
        ));
  }
}
