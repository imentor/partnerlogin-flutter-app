import 'package:Haandvaerker.dk/model/faq/faq_model.dart';
import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/faq_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactScreen extends StatelessWidget {
  const ContactScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      appBar: DrawerAppBar(),
      body: SingleChildScrollView(child: ContactUs()),
    );
  }
}

class FAQScreen extends StatefulWidget {
  const FAQScreen({super.key});
  @override
  FAQScreenState createState() => FAQScreenState();
}

class FAQScreenState extends State<FAQScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SingleChildScrollView(child: _FaqSection()),
    );
  }
}

class ContactUs extends StatelessWidget {
  final String phoneIconUrl = 'assets/images/blue_phone.svg'; // used
  final String sendMailIconUrl = 'assets/images/send.svg';

  const ContactUs({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20, right: 20, bottom: 40, left: 20),
      padding: const EdgeInsets.all(10),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SvgPicture.asset(
                phoneIconUrl,
                width: 40.0,
                height: 40.0,
              ),
              SmartGaps.gapH10,
              Text(tr('ring'),
                  style: Theme.of(context)
                      .textTheme
                      .headlineLarge!
                      .copyWith(color: Theme.of(context).colorScheme.primary)),
              SmartGaps.gapH20,
              Text(tr('call_detail'),
                  style: Theme.of(context).textTheme.titleSmall),
              SmartGaps.gapH10,
              GestureDetector(
                onTap: () async {
                  await launchUrl(Uri.parse(
                      Uri.encodeFull('tel:${Strings.kundematchTelNumber}')));
                },
                child: Text('${tr('ring')} ${Strings.kundematchTelNumber}',
                    style: Theme.of(context).textTheme.titleSmall!.copyWith(
                        color: Theme.of(context).colorScheme.primary,
                        fontWeight: FontWeight.w700)),
              ),
            ],
          ),
          SmartGaps.gapH50,
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SvgPicture.asset(
                sendMailIconUrl,
                height: 54,
                colorFilter: ColorFilter.mode(
                  Theme.of(context).colorScheme.primary,
                  BlendMode.srcIn,
                ),
              ),
              Text(
                tr('write'),
                style: Theme.of(context)
                    .textTheme
                    .headlineLarge!
                    .copyWith(color: Theme.of(context).colorScheme.primary),
              ),
              SmartGaps.gapH20,
              Text(tr('email_detail'),
                  style: Theme.of(context).textTheme.bodyMedium),
              SmartGaps.gapH10,
              GestureDetector(
                onTap: () {
                  changeDrawerRoute(Routes.createProjectMessage, arguments: {
                    'customerId': 1,
                  });
                },
                child: Text(
                  tr('send_us_an_email'),
                  style: Theme.of(context).textTheme.titleSmall!.copyWith(
                      fontWeight: FontWeight.w700,
                      color: Theme.of(context).colorScheme.primary),
                ),
              ),
            ],
          ),
          //_AppVersionSection(),
        ],
      ),
    );
  }
}

// class _AppVersionSection extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Consumer<VersionViewModel>(builder: (_, _vm, _widget) {
//       return Container(
//         margin: const EdgeInsets.only(top: 50),
//         child: Text('App version v${_vm.appVersion}', textAlign: TextAlign.start, style: Theme.of(context).textTheme.titleSmall.copyWith(color: Colors.grey)),
//       );
//     });
//   }
// }

class _FaqSection extends StatefulWidget {
  @override
  _FaqSectionState createState() => _FaqSectionState();
}

class _FaqSectionState extends State<_FaqSection> {
  TextEditingController editingController = TextEditingController();

  Future<void> initFaq() async {
    final viewmodel = context.read<FaqViewModel>();
    await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function: Future.value(viewmodel.initFaqItems()));
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final appDrawerVm = context.read<AppDrawerViewModel>();
      final newsFeedVm = context.read<NewsFeedViewModel>();
      const dynamicStoryPage = 'faq';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      initFaq();
      if (dynamicStoryEnable) {
        await Future.value(newsFeedVm.getDynamicStory(page: dynamicStoryPage))
            .whenComplete(() {
          if (!mounted) return;

          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      }
    });
  }

  @override
  void dispose() {
    editingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  tr('frequently_asked_questions'),
                  style: const TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.w700,
                    color: Color.fromRGBO(0, 54, 69, 1),
                  ),
                ),
                SmartGaps.gapH30,
                Material(
                  elevation: 10.0,
                  shadowColor: Colors.white,
                  child: TextField(
                    onChanged: (value) {
                      setState(() {});
                    },
                    controller: editingController,
                    decoration: InputDecoration(
                        enabledBorder: const OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.white,
                            width: 1.0,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(0.0)),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.grey[300]!, width: 1.0),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(0.0)),
                        ),
                        focusColor: const Color.fromRGBO(0, 54, 69, 1),
                        hintText: tr('search_for'),
                        prefixIcon: const Icon(
                          Icons.search,
                          color: Colors.grey,
                        ),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey[300]!),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(0.0)))),
                  ),
                ),
              ],
            ),
          ),
          Consumer<FaqViewModel>(builder: (_, vm, widget) {
            if (vm.busy) {
              return Center(
                  child: Container(
                      margin: const EdgeInsets.only(top: 20), child: loader()));
            }
            if (vm.faqItems.isEmpty) {
              return const Column(
                children: [
                  SmartGaps.gapH40,
                  EmptyListIndicator(),
                ],
              );
            }
            return FaqItemList(filter: editingController.text);
          }),
        ]);
  }
}

class FaqItemList extends StatefulWidget {
  const FaqItemList({super.key, this.filter});

  final String? filter;

  @override
  FaqItemListState createState() => FaqItemListState();
}

class FaqItemListState extends State<FaqItemList> {
  @override
  Widget build(BuildContext context) {
    var viewmodel = Provider.of<FaqViewModel>(context, listen: true);
    var filterList = (widget.filter!.isNotEmpty || widget.filter != null)
        ? viewmodel.filter(widget.filter)
        : viewmodel.faqItems;

    return Theme(
      data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
      child: Container(
        margin: const EdgeInsets.only(
          top: 20,
        ),
        child: ListView.builder(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: filterList.length,
            itemBuilder: (BuildContext context, int index) {
              FAQModel key = filterList.elementAt(index);
              return ExpansionTile(
                title: Text('${key.questionDA}',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.primary,
                      fontWeight: FontWeight.w700,
                      fontSize: 18,
                    )),
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(left: 15.0, right: 15),
                    child: Container(
                        alignment: Alignment.centerLeft,
                        child: _getAnswer(item: key)),
                  ),
                  SmartGaps.gapH10,
                ],
              );
            }),
      ),
    );
  }

  Widget _getAnswer({required FAQModel item}) {
    String itemText = (item.answerDA ?? '')
        .replaceAll(r'\n', '<br>')
        .replaceAll(r'\r', '')
        .replaceAll(r'\t', '&nbsp;&nbsp;&nbsp;&nbsp;');

    return HtmlWidget(
      itemText,
      textStyle: const TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w600,
        color: Color.fromRGBO(0, 54, 69, 1),
        height: 1.618,
      ),
    );
  }
}
