import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:Haandvaerker.dk/services/analytics/analytics_service.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/app_bottom_navigationbar.dart';
import 'package:Haandvaerker.dk/ui/components/m2m_options_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/message_appbar.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/components/version_update/update_overlay.dart';
import 'package:Haandvaerker.dk/ui/routing/route_observer.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/routing/routing.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/components/create_offer_video_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/job_detail.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_drawer_v2.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/location_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/onboarding_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/payment_dialog.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/launcher.dart';
import 'package:Haandvaerker.dk/utils/reset_viewmodel_states.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/app/connectivity_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/app_links/app_links_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/location/location_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/market_place_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/messages/message_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/mester-mester/mester_partner_profile_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/fast_track_offer/fast_track_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/payproff/payproff_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/price_calculation/price_calculation_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/widgets_recommendations_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/version_checker/version_checker_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:badges/badges.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart' hide Badge;
import 'package:flutter/services.dart';
import 'package:flutter_exit_app/flutter_exit_app.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key, this.isFromAppLink = false});

  final bool isFromAppLink;

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  final Connectivity _connectivity = Connectivity();
  // late StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    try {
      if (state == AppLifecycleState.resumed) {
        final clientsVm = context.read<ClientsViewModel>();
        final userVm = context.read<UserViewModel>();
        final appDrawerVm = context.read<AppDrawerViewModel>();

        if (clientsVm.calledCustomerJobId != 0) {
          clientsVm.getYourCustomerData = true;
          Future.wait([
            clientsVm.getPartnerJobsV2(),
            clientsVm.getSelectedJobFromNewsFeed(
                jobIdFromNewsFeed: clientsVm.calledCustomerJobId),
          ]).then((_) async {
            if (!mounted) return;

            if (clientsVm.selectedJobFromNewsFeed?.id != null) {
              clientsVm.calledCustomerJobId = 0;
              clientsVm.getYourCustomerData = false;
              clientsVm.fromNewsFeed = true;
              jobDetailDialog(
                  context: context,
                  job: clientsVm.selectedJobFromNewsFeed,
                  jobIndex: 0,
                  oldJob: null);
            } else {
              clientsVm.calledCustomerJobId = 0;
              clientsVm.getYourCustomerData = false;
              clientsVm.fromNewsFeed = true;
            }
          });
        }

        if (userVm.clickFromNotPaid) {
          userVm.clickFromNotPaid = false;
          modalManager.showLoadingModal();

          await userVm.checkNotPaidSettings().whenComplete(() {
            modalManager.hideLoadingModal();
            if ((userVm.notPaidSetting?.notPaid ?? false) &&
                (userVm.notPaidSetting?.showPackages ?? []).contains(appDrawerVm
                        .supplierProfileModel?.supplierPackage?.packageHandle ??
                    '')) {
              if ((userVm.notPaidSetting?.showPopup ?? false) &&
                  !userVm.notPaidDialogShowed) {
                userVm.notPaidDialogShowed = true;
                showNotPaidPopup(userVm: userVm);
              }

              if ((userVm.notPaidSetting?.showBanner ?? false)) {
                showNotPaidBanner(userVm: userVm);
              }
            }
          });
        }
      }
    } catch (e) {
      log("main.dart didChangeAppLifecycleState $e");
    }
  }

  Future<void> initConnectivity() async {
    List<ConnectivityResult> result = [];

    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      log(e.toString());
    }

    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(List<ConnectivityResult> result) async {
    var connectVm = context.read<ConnectivityViewModel>();

    if (result.contains(ConnectivityResult.wifi)) {
      connectVm.connectionStatus = 'wifi';
    } else if (result.contains(ConnectivityResult.mobile)) {
      connectVm.connectionStatus = 'mobile';
    } else if (result.contains(ConnectivityResult.none)) {
      connectVm.connectionStatus = 'none';
    } else {
      connectVm.connectionStatus = 'Failed to get connectivity';
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      initializeData();
    });
  }

  void initializeData() async {
    final userVm = context.read<UserViewModel>();
    final analyticsVm = context.read<AnalyticsService>();
    final locationVm = context.read<LocationViewModel>();
    final appDrawerVm = context.read<AppDrawerViewModel>();
    final widgetsVm = context.read<WidgetsViewModel>();
    final payproffVm = context.read<PayproffViewModel>();
    final newsFeedVm = context.read<NewsFeedViewModel>();
    final clientsVm = context.read<ClientsViewModel>();
    final tenderVm = context.read<TenderFolderViewModel>();
    final marketplaceVm = context.read<MarketPlaceV2ViewModel>();
    final mesterVm = context.read<MesterPartnerProfileViewModel>();
    final createOfferVm = context.read<CreateOfferViewModel>();
    final appLinksVm = context.read<AppLinksViewModel>();

    final fastTrackVm = context.read<FastTrackOfferViewModel>();
    final offerVm = context.read<OfferViewModel>();
    final messageVm = context.read<MessageV2ViewModel>();

    final versionVm = context.read<VersionCheckerViewmodel>();
    final sharedDataVm = context.read<SharedDataViewmodel>();
    final inviteCustomerVm = context.read<InviteCustomerViewmodel>();

    final priceCalculationVm = context.read<PriceCalculationViewmodel>();

    userVm.initPushNotification();
    analyticsVm.logSession();

    await context
        .read<LocationViewModel>()
        .configureLocation()
        .whenComplete(() async {
      if (!mounted) return;

      if (versionVm.appUpgrader.isUpdateAvailable()) {
        showUpdateOverlay(context);
      } else {
        await locationVm.checkLocationDialog().whenComplete(() async {
          if (!mounted) return;

          if (!locationVm.locationDialogShowed) {
            locationDialog(context: context).whenComplete(() async {
              if (!mounted) return;

              locationVm.setLocationDialog();
              newsFeedVm.setBusy(true);

              await locationVm.requestPermissions().whenComplete(() async {
                if (!mounted) return;

                // await locationVm.checkPermission();
                if (widget.isFromAppLink) {
                  await initializeUserData(
                    userVm: userVm,
                    analyticsVm: analyticsVm,
                    locationVm: locationVm,
                    appDrawerVm: appDrawerVm,
                    widgetsVm: widgetsVm,
                    payproffVm: payproffVm,
                    newsFeedVm: newsFeedVm,
                    clientsVm: clientsVm,
                    tenderVm: tenderVm,
                    marketplaceVm: marketplaceVm,
                    mesterVm: mesterVm,
                    createOfferVm: createOfferVm,
                    appLinksVm: appLinksVm,
                    fastTrackVm: fastTrackVm,
                    offerVm: offerVm,
                    messageVm: messageVm,
                    sharedDataVm: sharedDataVm,
                    inviteCustomerVm: inviteCustomerVm,
                    priceCalculationVm: priceCalculationVm,
                  );
                } else {
                  await userVm.validateToken().then((value) async {
                    if (value) {
                      await initializeUserData(
                        userVm: userVm,
                        analyticsVm: analyticsVm,
                        locationVm: locationVm,
                        appDrawerVm: appDrawerVm,
                        widgetsVm: widgetsVm,
                        payproffVm: payproffVm,
                        newsFeedVm: newsFeedVm,
                        clientsVm: clientsVm,
                        tenderVm: tenderVm,
                        marketplaceVm: marketplaceVm,
                        mesterVm: mesterVm,
                        createOfferVm: createOfferVm,
                        appLinksVm: appLinksVm,
                        fastTrackVm: fastTrackVm,
                        offerVm: offerVm,
                        messageVm: messageVm,
                        sharedDataVm: sharedDataVm,
                        inviteCustomerVm: inviteCustomerVm,
                        priceCalculationVm: priceCalculationVm,
                      );
                    } else {
                      handleLogout(locationVm: locationVm, userVm: userVm);
                    }
                  });
                }
              });
            });
          } else {
            newsFeedVm.setBusy(true);

            await locationVm.requestPermissions().whenComplete(() async {
              // await locationVm.checkPermission();

              if (widget.isFromAppLink) {
                await initializeUserData(
                  userVm: userVm,
                  analyticsVm: analyticsVm,
                  locationVm: locationVm,
                  appDrawerVm: appDrawerVm,
                  widgetsVm: widgetsVm,
                  payproffVm: payproffVm,
                  newsFeedVm: newsFeedVm,
                  clientsVm: clientsVm,
                  tenderVm: tenderVm,
                  marketplaceVm: marketplaceVm,
                  mesterVm: mesterVm,
                  createOfferVm: createOfferVm,
                  appLinksVm: appLinksVm,
                  fastTrackVm: fastTrackVm,
                  offerVm: offerVm,
                  messageVm: messageVm,
                  sharedDataVm: sharedDataVm,
                  inviteCustomerVm: inviteCustomerVm,
                  priceCalculationVm: priceCalculationVm,
                );
              } else {
                await userVm.validateToken().then((value) async {
                  if (value) {
                    await initializeUserData(
                      userVm: userVm,
                      analyticsVm: analyticsVm,
                      locationVm: locationVm,
                      appDrawerVm: appDrawerVm,
                      widgetsVm: widgetsVm,
                      payproffVm: payproffVm,
                      newsFeedVm: newsFeedVm,
                      clientsVm: clientsVm,
                      tenderVm: tenderVm,
                      marketplaceVm: marketplaceVm,
                      mesterVm: mesterVm,
                      createOfferVm: createOfferVm,
                      appLinksVm: appLinksVm,
                      fastTrackVm: fastTrackVm,
                      offerVm: offerVm,
                      messageVm: messageVm,
                      sharedDataVm: sharedDataVm,
                      inviteCustomerVm: inviteCustomerVm,
                      priceCalculationVm: priceCalculationVm,
                    );
                  } else {
                    handleLogout(locationVm: locationVm, userVm: userVm);
                  }
                });
              }
            });
          }
        });
      }
    });
  }

  Future<void> initializeUserData({
    required UserViewModel userVm,
    required AnalyticsService analyticsVm,
    required LocationViewModel locationVm,
    required AppDrawerViewModel appDrawerVm,
    required WidgetsViewModel widgetsVm,
    required PayproffViewModel payproffVm,
    required NewsFeedViewModel newsFeedVm,
    required ClientsViewModel clientsVm,
    required TenderFolderViewModel tenderVm,
    required MarketPlaceV2ViewModel marketplaceVm,
    required MesterPartnerProfileViewModel mesterVm,
    required CreateOfferViewModel createOfferVm,
    required AppLinksViewModel appLinksVm,
    required FastTrackOfferViewModel fastTrackVm,
    required OfferViewModel offerVm,
    required MessageV2ViewModel messageVm,
    required SharedDataViewmodel sharedDataVm,
    required InviteCustomerViewmodel inviteCustomerVm,
    required PriceCalculationViewmodel priceCalculationVm,
  }) async {
    await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey?.currentContext ?? context,
        function: Future.wait([
          appDrawerVm.getPrisenResult(),
          newsFeedVm.initNewsfeed(),
        ])).then((_) async {
      if (!mounted) return;

      clientsVm.fromNewsFeed = true;
      marketplaceVm.fromNewsFeed = true;
      mesterVm.fromNewsFeed = true;
      newsFeedVm.setBusy(false);

      payproffVm.checkPayproffAcount();
      messageVm.getNotifAndMessagesInitial();
      userVm.checkNotPaidSettings();
      await sharedDataVm.init().whenComplete(() {
        inviteCustomerVm.setIndustry(
            list1: sharedDataVm.industryList,
            list2: sharedDataVm.industryFullList);
        fastTrackVm.setIndustry(
            list1: sharedDataVm.industryList,
            list2: sharedDataVm.industryFullList,
            list3: sharedDataVm.taskTypes);
        offerVm.setIndustry(
            list1: sharedDataVm.industryList,
            list2: sharedDataVm.industryFullList,
            list3: sharedDataVm.taskTypes);
        priceCalculationVm.setIndustry(
            list1: sharedDataVm.industryList,
            list2: sharedDataVm.industryFullList,
            list3: sharedDataVm.taskTypes);
      });
      if (!mounted) return;

      offerVm.filteredIndustries = tenderVm.wizardTexts;
      offerVm.industryList = tenderVm.wizardTexts;

      widgetsVm.company = appDrawerVm.supplierProfileModel;

      if ((userVm.notPaidSetting?.notPaid ?? false) &&
          (userVm.notPaidSetting?.showPackages ?? []).contains(appDrawerVm
                  .supplierProfileModel?.supplierPackage?.packageHandle ??
              '')) {
        if ((userVm.notPaidSetting?.showPopup ?? false) &&
            !userVm.notPaidDialogShowed) {
          userVm.notPaidDialogShowed = true;
          showNotPaidPopup(userVm: userVm);
        }

        if ((userVm.notPaidSetting?.showBanner ?? false)) {
          showNotPaidBanner(userVm: userVm);
        }
      }

      if (appDrawerVm
              .supplierProfileModel?.supplierPackage?.pendingOnboarding ??
          false) {
        onboardingDialog(
          context: context,
          onboardingUrl: appDrawerVm
                  .supplierProfileModel?.supplierPackage?.onboardingUrl ??
              '',
        );
      }

      if (createOfferVm.isNewReserve) {
        await showDialog(
            context: context,
            barrierDismissible: false,
            barrierColor: Colors.black54,
            builder: (context) {
              return const CreateOfferVideoDialog();
            });
        createOfferVm.isNewReserve = false;
      }

      handleAppLinks(appLinksVm, offerVm);

      const dynamicStoryPage = 'newsfeed';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      if (dynamicStoryEnable) {
        await Future.value(newsFeedVm.getDynamicStory(page: dynamicStoryPage))
            .whenComplete(() {
          if (!mounted) return;

          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      }

      // bool isAfter = true;
      // final ibanUpdatedAt = payproffVm.payproffWallet?.updatedAt ?? '';
      // final accountExists = payproffVm.payproff?.exists ?? false;
      // final verified = payproffVm.payproff?.payproffVerified ?? false;

      // if (ibanUpdatedAt.isNotEmpty) {
      //   final updatedDate = DateTime.parse(ibanUpdatedAt);

      //   Duration difference = DateTime.now().difference(updatedDate);

      //   isAfter = difference.inDays >= 3;
      // }
      // if (accountExists && !verified && isAfter) {
      //   payproffIbanVerifyDialog(context);
      // }

      if ((appDrawerVm.bitrixCompanyModel?.companyTracking ?? 0) == 1 ||
          (appDrawerVm.bitrixCompanyModel?.isVaekstPlus ?? false)) {
        await locationVm.startLocation();
      }

      // if ((appDrawerVm.bitrixCompanyModel?.deadlineOffer ?? 0) ==
      //         1 &&
      //     (newsFeedVm.latestJobFeed ?? []).isNotEmpty) {
      //   if (!mounted) return;

      //   latestJobDialog(context: context);
      // }
    });
  }

  void showNotPaidPopup({required UserViewModel userVm}) {
    needToPay(
        context: context,
        allowClose: (userVm.notPaidSetting?.allowClosePopup ?? false),
        header: (userVm.notPaidSetting?.popupHeader ?? ''),
        body: (userVm.notPaidSetting?.popupBody ?? ''),
        buttonLabel: (userVm.notPaidSetting?.popupButton ?? ''),
        url: (userVm.notPaidSetting?.popupUrl ?? ''),
        onTap: () {
          userVm.notPaidDialogShowed = false;
          userVm.clickFromNotPaid = true;
          Launcher().launchUrlLocal((userVm.notPaidSetting?.popupUrl ?? ''));
        }).then((onValue) async {
      if (onValue == null) {
        if ((userVm.notPaidSetting?.createTicketOnClosePopup ?? false)) {
          await userVm.createTaskFromNotPaidPopup();
        }
      } else if (onValue == true) {
        if ((userVm.notPaidSetting?.createTicketOnPayNow ?? false)) {
          await userVm.createTaskFromNotPaidPopup();
        }
      }
    });
  }

  void showNotPaidBanner({required UserViewModel userVm}) {
    ScaffoldMessenger.of(context).hideCurrentMaterialBanner();
    ScaffoldMessenger.of(context).showMaterialBanner(
      MaterialBanner(
        leading: const Icon(Icons.warning, color: Colors.white),
        content: Html(
          data: (userVm.notPaidSetting?.bannerBody ?? ''),
          shrinkWrap: true,
          style: {
            "p": Style(
              color: Colors.white,
              margin: Margins.zero,
              padding: HtmlPaddings.zero,
            )
          },
        ),
        contentTextStyle: const TextStyle(color: Colors.black),
        elevation: 2,
        backgroundColor: PartnerAppColors.red,
        actions: [
          if ((userVm.notPaidSetting?.bannerUrl ?? '').isNotEmpty)
            TextButton(
              onPressed: () async {
                userVm.clickFromNotPaid = true;
                Launcher()
                    .launchUrlLocal((userVm.notPaidSetting?.bannerUrl ?? ''));
                ScaffoldMessenger.of(context).hideCurrentMaterialBanner();
                await userVm.createTaskFromNotPaidPopup();
              },
              style: TextButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      side: const BorderSide(
                        color: Colors.white,
                      ),
                      borderRadius: BorderRadius.circular(20))),
              child: Text(
                (userVm.notPaidSetting?.bannerButton ?? ''),
                style: Theme.of(context).textTheme.titleSmall!.copyWith(
                      color: Colors.white,
                    ),
              ),
            ),
        ],
      ),
    );
  }

  void handleLogout(
      {required LocationViewModel locationVm,
      required UserViewModel userVm}) async {
    await Future.wait([
      locationVm.cancelLocationListener(),
      userVm.clearStorage(),
    ]);

    if (!mounted) return;

    resetViewmodelStates(context: context);
    Sentry.configureScope((scope) => scope.setUser(null));
    Navigator.pushNamedAndRemoveUntil(context, Routes.login, (r) => false);
  }

  void handleAppLinks(AppLinksViewModel appLinksVm, OfferViewModel offerVm) {
    if (appLinksVm.pageRedirect.isNotEmpty && appLinksVm.appLinkLogin != null) {
      switch (appLinksVm.pageRedirect) {
        case 'projectdashboard':
        case 'directcontract':
        case 'paymentstage':
        case 'directextrawork':
        case 'uploadinvoice':
        case 'contractdraft':
        case 'directcontractver':
          if (appLinksVm.projectId.isNotEmpty) {
            changeDrawerRoute(Routes.projectDashboard,
                arguments: {'projectId': int.parse(appLinksVm.projectId)});
          }
          break;
        case 'market':
          if (appLinksVm.projectId.isNotEmpty) {
            changeDrawerRoute(Routes.marketPlaceScreen, arguments: true);
          }
          break;
        case 'minekunder':
          if (appLinksVm.projectId.isNotEmpty &&
              appLinksVm.contactId.isNotEmpty) {
            changeDrawerRoute(Routes.yourClient,
                arguments: {'isFromAppLink': true});
          }
          break;
        case 'priceCalculator':
          changeDrawerRoute(Routes.priceCalculator);
          appLinksVm.resetLoginAppLink();
          break;
        case 'cancelContract':
          changeDrawerRoute(Routes.cancelContract);
          break;
        case 'newofferversion':
          if (appLinksVm.offerId.isNotEmpty) {
            changeDrawerRoute(Routes.editOfferLanding);
          }
          break;
        case 'opretoffer':
        case 'uploadoffer':
          if (appLinksVm.signOffer) {
            if (appLinksVm.offerVersion.isNotEmpty) {
              offerVm.viewSignatureOfferVersionId =
                  int.parse(appLinksVm.offerVersion);
            }
            changeDrawerRoute(Routes.signatureOffer);
          }
          break;
        case 'offerByCs':
          if (appLinksVm.projectId.isNotEmpty) {
            changeDrawerRoute(Routes.yourClient,
                arguments: {'isFromAppLink': true});
          }
          break;
        case 'subcontract':
          if (appLinksVm.projectId.isNotEmpty &&
              appLinksVm.subcontractorId.isNotEmpty) {
            changeDrawerRoute(Routes.subcontractorAcceptInvite, arguments: {
              'subContractId': int.parse(appLinksVm.subcontractorId),
              'projectId': int.parse(appLinksVm.projectId),
            });
          }
          break;
        default:
      }
    }
  }

  @override
  void dispose() {
    // _connectivitySubscription.cancel();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  didChangeDependencies() {
    initConnectivity();
    // _connectivitySubscription =
    //     _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    modalManager.setContext(context);
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool isPopped, _) async {
        var vm = context.read<AppDrawerViewModel>();

        if (isPopped) {
          return;
        }

        if (myGlobals.drawerRouteKey!.currentState!.canPop()) {
          myGlobals.innerScreenScaffoldKey!.currentState!.openEndDrawer();
          myGlobals.drawerRouteKey!.currentState!.pop();
          vm.update();
          isPopped = false;
        } else {
          var response = await showOkCancelAlertDialog(
              context: context,
              title: tr('are_you_sure'),
              message: tr('do_you_want_to_exit_the_app'),
              okLabel: tr('exit'),
              cancelLabel: tr('cancel'));

          if (response == OkCancelResult.ok) {
            if (Platform.isAndroid) {
              SystemNavigator.pop();
            } else {
              FlutterExitApp.exitApp(iosForceExit: true);
            }
          }
        }
      },
      child: Scaffold(
        key: myGlobals.homeScaffoldKey,
        appBar: AppBar(
          elevation: 10,
          toolbarHeight: 70,
          shadowColor: const Color(0xffFCFCFC).withValues(alpha: 0.5),
          backgroundColor: Colors.white,
          automaticallyImplyLeading: false,
          leading: null,
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Consumer<AppDrawerViewModel>(builder: (_, appDrawerVm, __) {
                return InkWell(
                  onTap: () {
                    if (myGlobals
                        .innerScreenScaffoldKey!.currentState!.isDrawerOpen) {
                      // Navigator.of(context).pop();
                      myGlobals.innerScreenScaffoldKey!.currentState!
                          .closeDrawer();
                    } else {
                      myGlobals.innerScreenScaffoldKey!.currentState!
                          .openDrawer();
                    }
                    appDrawerVm.isDrawerOpen = myGlobals
                        .innerScreenScaffoldKey!.currentState!.isDrawerOpen;
                  },
                  child: SizedBox(
                    width: 32,
                    height: 32,
                    child: SvgPicture.asset(appDrawerVm.isDrawerOpen
                        ? SvgIcons.menuIconOpen
                        : SvgIcons.menuIconClose),
                  ),
                );
              }),
              // SmartGaps.gapW10,
              // SizedBox(
              //   child: Text(
              //     'Håndværker.dk',
              //     style: Theme.of(context).textTheme.headlineSmall,
              //   ),
              // ),
            ],
          ),
          actions: <Widget>[
            //
            Consumer<MessageV2ViewModel>(builder: (_, vm, __) {
              return SizedBox(
                width: 45,
                child: IconButton(
                    icon: Badge(
                      showBadge: (vm.notificationPagination.unread ?? 0) > 0,
                      badgeAnimation: const BadgeAnimation.fade(),
                      position: BadgePosition.topEnd(),
                      badgeStyle: BadgeStyle(
                        badgeColor: Theme.of(context).colorScheme.error,
                        shape: BadgeShape.circle,
                      ),
                      badgeContent: Center(
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 2),
                          child: Text(
                            '${vm.notificationPagination.unread}',
                            style: const TextStyle(
                                color: Colors.white, fontSize: 10),
                          ),
                        ),
                      ),
                      child: const Icon(FeatherIcons.bell, size: 30),
                    ),
                    onPressed: () {
                      changeDrawerRoute(Routes.notification);
                    }),
              );
            }),
            const MessageAppBarIcon(),

            SmartGaps.gapW10,
          ],
          systemOverlayStyle: SystemUiOverlayStyle.dark,
        ),
        body: Scaffold(
          key: myGlobals.innerScreenScaffoldKey,
          drawer: context.watch<NewsFeedViewModel>().busy
              ? null
              : const AppDrawerV2(),
          body: Container(
            color: Colors.black12,
            child: Navigator(
              key: myGlobals.drawerRouteKey,
              //initialRoute: Routes.newsFeed,
              onGenerateRoute: RouteGenerator.generateDrawerRoute,
              observers: [DrawerRouteObserver()],
            ),
          ),
        ),
        bottomNavigationBar: const AppBottomNavigationBar(),
      ),
    );
  }
}

// M2mOptionsDialog
void showOptionDialog(BuildContext context) {
  Dialog simpleDialog = const Dialog(
    child: M2mOptionsDialog(),
  );
  showDialog(context: context, builder: (BuildContext context) => simpleDialog);
}
