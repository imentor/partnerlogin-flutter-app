import 'dart:developer';

import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class DrawerAppBar extends StatelessWidget implements PreferredSizeWidget {
  final List<Widget>? actions;
  final VoidCallback? onBack;
  final Color? backColor;
  final VoidCallback? overrideBackFunction;
  final bool isSliver;
  final PreferredSizeWidget? bottom;

  const DrawerAppBar(
      {super.key,
      this.actions,
      this.onBack,
      this.backColor,
      this.overrideBackFunction,
      this.isSliver = false,
      this.bottom});

  @override
  Widget build(BuildContext context) {
    return Navigator.of(context).canPop()
        ? isSliver
            ? SliverAppBar(
                backgroundColor: Colors.transparent,
                automaticallyImplyLeading: false,
                leadingWidth: 120,
                leading: Container(
                  width: 80,
                  margin: const EdgeInsets.only(left: 20, top: 15),
                  child: TextButton(
                    onPressed: overrideBackFunction ??
                        () {
                          try {
                            APPLIC.cancelToken.cancel('cancel requests');

                            if (APPLIC.cancelToken.isCancelled) {
                              log('Cancelled successfully');
                              APPLIC.cancelToken = CancelToken();
                              log('Renewed cancel token');
                            } else {
                              log('request was not cancelled');
                            }
                          } catch (e) {
                            log("message SliverAppBar catch $e");
                          }

                          if (onBack != null) {
                            onBack!();
                          }

                          backDrawerRoute();
                        },
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Transform.translate(
                          offset: const Offset(0, 1),
                          child: Icon(Icons.arrow_back,
                              color: backColor ??
                                  Theme.of(context).colorScheme.primary,
                              size: 15),
                        ),
                        SmartGaps.gapW5,
                        Text(tr('back')),
                      ],
                    ),
                  ),
                ),
                bottom: bottom,
                titleSpacing: 0,
                elevation: 0,
                floating: true,
                expandedHeight: 50,
                collapsedHeight: 50,
                toolbarHeight: 50,
                actions: actions,
              )
            : AppBar(
                backgroundColor: Colors.transparent,
                automaticallyImplyLeading: false,
                leadingWidth: 120,
                leading: Container(
                  width: 80,
                  margin: const EdgeInsets.only(left: 20, top: 15),
                  child: TextButton(
                    onPressed: overrideBackFunction ??
                        () {
                          try {
                            APPLIC.cancelToken.cancel('cancel requests');

                            if (APPLIC.cancelToken.isCancelled) {
                              log('Cancelled successfully');
                              APPLIC.cancelToken = CancelToken();
                              log('Renewed cancel token');
                            } else {
                              log('request was not cancelled');
                            }

                            if (onBack != null) {
                              onBack!();
                            }
                          } catch (e) {
                            log("message AppBar catch $e");
                          }
                          backDrawerRoute();
                        },
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Transform.translate(
                          offset: const Offset(0, 1),
                          child: Icon(Icons.arrow_back,
                              color: backColor ??
                                  Theme.of(context).colorScheme.primary,
                              size: 15),
                        ),
                        SmartGaps.gapW5,
                        Text(tr('back')),
                      ],
                    ),
                  ),
                ),
                bottom: bottom,
                titleSpacing: 0,
                elevation: 0,
                toolbarHeight: 50,
                actions: actions,
              )
        : Container();
  }

  @override
  Size get preferredSize => Size.fromHeight(
      myGlobals.drawerRouteKey!.currentState!.canPop() ? 50 : 0);
}
