import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_html/flutter_html.dart';

Future<bool?> needToPay({
  required BuildContext context,
  required bool allowClose,
  required String header,
  required String body,
  required String buttonLabel,
  required String url,
  void Function()? onTap,
}) {
  return showDialog(
    barrierDismissible: allowClose,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: allowClose
            ? Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                InkWell(
                  onTap: () {
                    Navigator.of(dialogContext).pop();
                  },
                  child: const Icon(
                    FeatherIcons.x,
                    color: PartnerAppColors.spanishGrey,
                  ),
                )
              ])
            : null,
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Icon(
                  Icons.warning_rounded,
                  color: PartnerAppColors.red,
                  size: 80,
                ),
                SmartGaps.gapH20,
                Html(
                  data: header,
                  shrinkWrap: true,
                  style: {
                    "p": Style(
                      margin: Margins.zero,
                      padding: HtmlPaddings.zero,
                    ),
                  },
                ),
                Html(
                  data: body,
                  shrinkWrap: true,
                  style: {
                    "p": Style(
                      margin: Margins.zero,
                      padding: HtmlPaddings.zero,
                    ),
                  },
                ),
                SmartGaps.gapH20,
                CustomDesignTheme.flatButtonStyle(
                  height: 50,
                  backgroundColor: PartnerAppColors.malachite,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  onPressed: url.isNotEmpty
                      ? () async {
                          Navigator.of(dialogContext).pop(true);
                          onTap!();
                        }
                      : () {},
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          buttonLabel,
                          style: Theme.of(context)
                              .textTheme
                              .displayLarge!
                              .copyWith(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                                color: Colors.white,
                              ),
                        )
                      ]),
                ),
                SmartGaps.gapH20,
                Text(
                  tr('not_paid_popup_description'),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.displaySmall!.copyWith(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: PartnerAppColors.darkBlue,
                      ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}
