import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/reset_viewmodel_states.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/location/location_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/market_place_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/payproff/payproff_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_background_geolocation/flutter_background_geolocation.dart'
    as bg;
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

Future<void> additionalDrawerTiles({
  required BuildContext context,
  required String title,
  required List<Widget> addtionalTiles,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: Theme.of(context).textTheme.titleLarge!.copyWith(
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                      ),
                ),
                const SizedBox(
                  height: 10,
                ),
                const Divider(
                  color: PartnerAppColors.grey1,
                ),
                const SizedBox(
                  height: 10,
                ),
                ...addtionalTiles
              ],
            ),
          ),
        ),
      );
    },
  );
}

class AppDrawerV2 extends StatefulWidget {
  const AppDrawerV2({super.key});

  @override
  State<AppDrawerV2> createState() => _AppDrawerV2State();
}

class _AppDrawerV2State extends State<AppDrawerV2> {
  bool isCustomerExpanded = false;
  bool isProfileExpanded = false;

  void redirectMyCustomer({required AppDrawerViewModel appDrawerVm}) async {
    final locationVm = context.read<LocationViewModel>();
    final newsfeedVm = context.read<NewsFeedViewModel>();
    final userVm = context.read<UserViewModel>();

    newsfeedVm.storyPage = 'mycustomer';

    final isGrowthPlusPackage =
        appDrawerVm.bitrixCompanyModel?.isVaekstPlus ?? false;
    final hasAccountIntegration =
        (appDrawerVm.bitrixCompanyModel?.accountIntegration ?? false);

    if ((appDrawerVm.bitrixCompanyModel?.companyTracking ?? 0) == 1) {
      await locationVm.checkPermission();

      if (!mounted) return;

      final isLocationDenied =
          locationVm.locationPermission == LocationPermission.denied;
      final isLocationDeniedForever =
          locationVm.locationPermission == LocationPermission.deniedForever;
      final isLocationUnableToDetermine =
          locationVm.locationPermission == LocationPermission.unableToDetermine;
      final isLocationWhileInUse =
          locationVm.locationPermission == LocationPermission.whileInUse;

      if ((!hasAccountIntegration &&
              (isLocationDenied ||
                  isLocationDeniedForever ||
                  isLocationUnableToDetermine ||
                  isLocationWhileInUse)) &&
          isGrowthPlusPackage) {
        await locationVm.updateLocationInfo(gpsLocation: 0);

        if (!mounted) return;

        disabledDialog(
                context: context,
                message: tr('no_gps_access_my_customer'),
                companyName: userVm.userModel?.user?.name ?? '')
            .whenComplete(() async {
          await locationVm.checkPermission();

          if (locationVm.locationPermission == LocationPermission.always) {
            bg.BackgroundGeolocation.stop();
            locationVm.startLocation();

            locationVm.updateLocationInfo(gpsLocation: 1);
          } else if (locationVm.locationPermission ==
                  LocationPermission.deniedForever ||
              locationVm.locationPermission == LocationPermission.denied ||
              locationVm.locationPermission == LocationPermission.whileInUse) {
            locationVm.updateLocationInfo(gpsLocation: 0);
          }
        });
      } else {
        locationVm.updateLocationInfo(gpsLocation: 1).whenComplete(() {
          changeDrawerRoute(Routes.yourClient);
        });
      }
    } else {
      locationVm.updateLocationInfo(gpsLocation: 1).whenComplete(() {
        changeDrawerRoute(Routes.yourClient);
      });
    }
  }

  void logout() async {
    final locationVm = context.read<LocationViewModel>();
    final userVm = context.read<UserViewModel>();

    await showOkCancelAlertDialog(
            context: context,
            title: tr('log_out_title'),
            message: '',
            okLabel: tr('log_out'))
        .then((selected) async {
      if (!mounted) return;

      if (selected == OkCancelResult.ok) {
        modalManager.showLoadingModal(message: tr('signing_out'));

        await Future.wait([
          locationVm.cancelLocationListener(),
          userVm.logout(),
        ]).whenComplete(() {
          if (!mounted) return;

          ScaffoldMessenger.of(context).hideCurrentMaterialBanner();
          resetViewmodelStates(context: context);
          Sentry.configureScope((scope) => scope.setUser(null));
          modalManager.hideLoadingModal();
          Navigator.pushNamedAndRemoveUntil(
              context, Routes.login, (r) => false);
        });

        userVm.isSuccess = false;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
      child: Consumer<AppDrawerViewModel>(builder: (_, appDrawerVm, __) {
        return Drawer(
          width: MediaQuery.of(context).size.width * 1.05,
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: ListView(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    InkWell(
                      onTap: () {
                        myGlobals.innerScreenScaffoldKey!.currentState!
                            .closeDrawer();
                      },
                      child: const Icon(
                        FeatherIcons.x,
                        size: 28,
                      ),
                    )
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  context.read<UserViewModel>().userModel?.user?.name ?? '',
                  style: Theme.of(context).textTheme.titleLarge!.copyWith(
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.bold,
                      ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  context.read<UserViewModel>().userModel?.user?.email ?? '',
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        color: PartnerAppColors.grey1,
                        fontWeight: FontWeight.normal,
                      ),
                ),
                const SizedBox(
                  height: 10,
                ),
                const Divider(
                  color: PartnerAppColors.grey1,
                ),
                const SizedBox(
                  height: 10,
                ),
                drawerListTiles(
                  title: tr('front_page'),
                  leadingIcon: SvgIcons.homeV2,
                  isMainTile: true,
                  onTap: () {
                    changeDrawerRoute(Routes.newsFeed);
                  },
                  companyPackage:
                      (appDrawerVm.bitrixCompanyModel?.companyPackge ?? ''),
                ),
                const SizedBox(
                  height: 10,
                ),
                expandableListTile(
                  leadingIcon: SvgIcons.customer,
                  isExpanded: isCustomerExpanded,
                  title: tr('your_customer'),
                  onExpansionChanged: (p0) {
                    setState(() {
                      isCustomerExpanded = p0;
                    });
                  },
                  children: [
                    drawerListTiles(
                      title: tr('marketplace'),
                      leadingIcon: '',
                      isMainTile: true,
                      isSubMenu: true,
                      onTap: () {
                        final marketplaceVm =
                            context.read<MarketPlaceV2ViewModel>();
                        if (!marketplaceVm.fromNewsFeed) {
                          marketplaceVm.setBusy(true);
                        }
                        changeDrawerRoute(Routes.marketPlaceScreen);
                      },
                      companyPackage: (appDrawerVm.supplierProfileModel
                              ?.bCrmCompany?.companyPackge ??
                          ''),
                    ),
                    drawerListTiles(
                      title: tr('my_customers'),
                      leadingIcon: '',
                      isMainTile: true,
                      isSubMenu: true,
                      onTap: () => redirectMyCustomer(appDrawerVm: appDrawerVm),
                      companyPackage: (appDrawerVm.supplierProfileModel
                              ?.bCrmCompany?.companyPackge ??
                          ''),
                    ),
                    if ((appDrawerVm.bitrixCompanyModel?.companyBookerWizard ??
                            0) ==
                        1) ...[
                      drawerListTiles(
                        title: tr('create_offer'),
                        leadingIcon: '',
                        isMainTile: true,
                        isSubMenu: true,
                        onTap: () => changeDrawerRoute(Routes.inviteCustomer),
                        companyPackage: (appDrawerVm.supplierProfileModel
                                ?.bCrmCompany?.companyPackge ??
                            ''),
                      ),
                    ],
                    if ((context
                                .read<AppDrawerViewModel>()
                                .supplierProfileModel
                                ?.bCrmCompany
                                ?.companyFastTrack ??
                            0) ==
                        1) ...[
                      drawerListTiles(
                        title: tr('make_enterprise_contract'),
                        leadingIcon: '',
                        isMainTile: true,
                        isSubMenu: true,
                        onTap: () {
                          changeDrawerRoute(Routes.contructionWallet);
                        },
                        companyPackage: (appDrawerVm.supplierProfileModel
                                ?.bCrmCompany?.companyPackge ??
                            ''),
                      )
                    ],
                    if ((appDrawerVm.bitrixCompanyModel?.companyPrisberegning ??
                            0) ==
                        1) ...[
                      drawerListTiles(
                        title: tr('price_calculation'),
                        leadingIcon: '',
                        isMainTile: true,
                        isSubMenu: true,
                        onTap: () {
                          changeDrawerRoute(Routes.priceCalculator);
                        },
                        companyPackage: (appDrawerVm.supplierProfileModel
                                ?.bCrmCompany?.companyPackge ??
                            ''),
                      )
                    ],
                    drawerListTiles(
                      title: tr('teacher_to_teacher'),
                      leadingIcon: '',
                      isMainTile: true,
                      isSubMenu: true,
                      onTap: () {
                        additionalDrawerTiles(
                            context: context,
                            title: tr('teacher_to_teacher'),
                            addtionalTiles: [
                              drawerListTiles(
                                title: tr('labor'),
                                leadingIcon: '',
                                isMainTile: true,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                trailing: const Icon(
                                  FeatherIcons.chevronRight,
                                  color: PartnerAppColors.darkBlue,
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                  changeDrawerRoute(Routes.m2mLaborScreen);
                                },
                                companyPackage: (appDrawerVm
                                        .supplierProfileModel
                                        ?.bCrmCompany
                                        ?.companyPackge ??
                                    ''),
                              ),
                              drawerListTiles(
                                title: tr('tasks'),
                                leadingIcon: '',
                                isMainTile: true,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                trailing: const Icon(
                                  FeatherIcons.chevronRight,
                                  color: PartnerAppColors.darkBlue,
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                  changeDrawerRoute(Routes.m2mTaskScreen);
                                },
                                companyPackage: (appDrawerVm
                                        .supplierProfileModel
                                        ?.bCrmCompany
                                        ?.companyPackge ??
                                    ''),
                              ),
                            ]);
                      },
                      companyPackage: (appDrawerVm.supplierProfileModel
                              ?.bCrmCompany?.companyPackge ??
                          ''),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                expandableListTile(
                  leadingIcon: SvgIcons.accountUser,
                  isExpanded: isProfileExpanded,
                  title: tr('profile'),
                  onExpansionChanged: (p0) {
                    setState(() {
                      isProfileExpanded = p0;
                    });
                  },
                  children: [
                    drawerListTiles(
                      title: tr('my_projects'),
                      leadingIcon: '',
                      isMainTile: true,
                      isSubMenu: true,
                      onTap: () {
                        additionalDrawerTiles(
                            context: context,
                            title: tr('my_projects'),
                            addtionalTiles: [
                              drawerListTiles(
                                title: tr('create_project'),
                                leadingIcon: '',
                                isMainTile: true,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                trailing: const Icon(
                                  FeatherIcons.chevronRight,
                                  color: PartnerAppColors.darkBlue,
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                  changeDrawerRoute(Routes.createProject);
                                },
                                companyPackage: (appDrawerVm
                                        .supplierProfileModel
                                        ?.bCrmCompany
                                        ?.companyPackge ??
                                    ''),
                              ),
                              drawerListTiles(
                                title: tr('my_projects'),
                                leadingIcon: '',
                                isMainTile: true,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                trailing: const Icon(
                                  FeatherIcons.chevronRight,
                                  color: PartnerAppColors.darkBlue,
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                  changeDrawerRoute(Routes.myProjects);
                                },
                                companyPackage: (appDrawerVm
                                        .supplierProfileModel
                                        ?.bCrmCompany
                                        ?.companyPackge ??
                                    ''),
                              )
                            ]);
                      },
                      companyPackage: (appDrawerVm.supplierProfileModel
                              ?.bCrmCompany?.companyPackge ??
                          ''),
                    ),
                    drawerListTiles(
                      title: tr('recommendations'),
                      leadingIcon: '',
                      isMainTile: true,
                      isSubMenu: true,
                      onTap: () {
                        additionalDrawerTiles(
                            context: context,
                            title: tr('recommendations'),
                            addtionalTiles: [
                              drawerListTiles(
                                title: tr('get_recommendations'),
                                leadingIcon: '',
                                isMainTile: true,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                trailing: const Icon(
                                  FeatherIcons.chevronRight,
                                  color: PartnerAppColors.darkBlue,
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                  changeDrawerRoute(Routes.getRecommendations);
                                },
                                companyPackage: (appDrawerVm
                                        .supplierProfileModel
                                        ?.bCrmCompany
                                        ?.companyPackge ??
                                    ''),
                              ),
                              drawerListTiles(
                                title:
                                    tr('read_and_answer_your_recommendations'),
                                leadingIcon: '',
                                isMainTile: true,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                trailing: const Icon(
                                  FeatherIcons.chevronRight,
                                  color: PartnerAppColors.darkBlue,
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                  changeDrawerRoute(
                                      Routes.readAndAnswerRecommendations);
                                },
                                companyPackage: (appDrawerVm
                                        .supplierProfileModel
                                        ?.bCrmCompany
                                        ?.companyPackge ??
                                    ''),
                              ),
                              drawerListTiles(
                                title: tr('recommendations_on_auto_pilot'),
                                leadingIcon: '',
                                isMainTile: true,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                trailing: const Icon(
                                  FeatherIcons.chevronRight,
                                  color: PartnerAppColors.darkBlue,
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                  changeDrawerRoute(
                                      Routes.autoPilotRecommendations);
                                },
                                companyPackage: (appDrawerVm
                                        .supplierProfileModel
                                        ?.bCrmCompany
                                        ?.companyPackge ??
                                    ''),
                              ),
                              drawerListTiles(
                                title: tr('banner_for_your_website'),
                                leadingIcon: '',
                                isMainTile: true,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                trailing: const Icon(
                                  FeatherIcons.chevronRight,
                                  color: PartnerAppColors.darkBlue,
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                  changeDrawerRoute(
                                      Routes.readAndAnswerRecommendations);
                                },
                                companyPackage: (appDrawerVm
                                        .supplierProfileModel
                                        ?.bCrmCompany
                                        ?.companyPackge ??
                                    ''),
                              ),
                            ]);
                      },
                      companyPackage: (appDrawerVm.supplierProfileModel
                              ?.bCrmCompany?.companyPackge ??
                          ''),
                    ),
                    drawerListTiles(
                      title: tr('membership_benefits'),
                      leadingIcon: '',
                      isMainTile: true,
                      isSubMenu: true,
                      onTap: () {
                        changeDrawerRoute(Routes.membershipBenefits);
                      },
                      companyPackage: (appDrawerVm.supplierProfileModel
                              ?.bCrmCompany?.companyPackge ??
                          ''),
                    ),
                    drawerListTiles(
                      title: tr('digital_package'),
                      leadingIcon: '',
                      isMainTile: true,
                      isSubMenu: true,
                      onTap: () {
                        changeDrawerRoute(Routes.digitalPackage);
                      },
                      companyPackage: (appDrawerVm.supplierProfileModel
                              ?.bCrmCompany?.companyPackge ??
                          ''),
                    ),
                    drawerListTiles(
                      title: tr('about_us'),
                      leadingIcon: '',
                      isMainTile: true,
                      isSubMenu: true,
                      onTap: () {
                        additionalDrawerTiles(
                            context: context,
                            title: tr('about_us'),
                            addtionalTiles: [
                              drawerListTiles(
                                title: tr('terms_of_use'),
                                leadingIcon: '',
                                isMainTile: true,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                trailing: const Icon(
                                  FeatherIcons.chevronRight,
                                  color: PartnerAppColors.darkBlue,
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                  changeDrawerRoute(
                                    Routes.inAppWebView,
                                    arguments: Uri.parse(Links.termsOfUseUrl),
                                  );
                                },
                                companyPackage: (appDrawerVm
                                        .supplierProfileModel
                                        ?.bCrmCompany
                                        ?.companyPackge ??
                                    ''),
                              ),
                              drawerListTiles(
                                title: tr('privacy_policy'),
                                leadingIcon: '',
                                isMainTile: true,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                trailing: const Icon(
                                  FeatherIcons.chevronRight,
                                  color: PartnerAppColors.darkBlue,
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                  changeDrawerRoute(
                                    Routes.inAppWebView,
                                    arguments:
                                        Uri.parse(Links.privacyPolicyUrl),
                                  );
                                },
                                companyPackage: (appDrawerVm
                                        .supplierProfileModel
                                        ?.bCrmCompany
                                        ?.companyPackge ??
                                    ''),
                              ),
                            ]);
                      },
                      companyPackage: (appDrawerVm.supplierProfileModel
                              ?.bCrmCompany?.companyPackge ??
                          ''),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                const Divider(
                  color: PartnerAppColors.grey1,
                ),
                const SizedBox(
                  height: 10,
                ),
                drawerListTiles(
                  title: tr('settings'),
                  leadingIcon: SvgIcons.settings,
                  onTap: () {
                    additionalDrawerTiles(
                        context: context,
                        title: tr('settings'),
                        addtionalTiles: [
                          drawerListTiles(
                            title: tr('your_master_information'),
                            leadingIcon: '',
                            isMainTile: true,
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            trailing: const Icon(
                              FeatherIcons.chevronRight,
                              color: PartnerAppColors.darkBlue,
                            ),
                            onTap: () {
                              Navigator.of(context).pop();
                              changeDrawerRoute(Routes.yourMasterInformation);
                            },
                            companyPackage: (appDrawerVm.supplierProfileModel
                                    ?.bCrmCompany?.companyPackge ??
                                ''),
                          ),
                          drawerListTiles(
                            title: tr('your_profile_page'),
                            leadingIcon: '',
                            isMainTile: true,
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            trailing: const Icon(
                              FeatherIcons.chevronRight,
                              color: PartnerAppColors.darkBlue,
                            ),
                            onTap: () {
                              Navigator.of(context).pop();
                              changeDrawerRoute(Routes.yourProfilePage);
                            },
                            companyPackage: (appDrawerVm.supplierProfileModel
                                    ?.bCrmCompany?.companyPackge ??
                                ''),
                          ),
                          drawerListTiles(
                            title: tr('job_types'),
                            leadingIcon: '',
                            isMainTile: true,
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            trailing: const Icon(
                              FeatherIcons.chevronRight,
                              color: PartnerAppColors.darkBlue,
                            ),
                            onTap: () {
                              Navigator.of(context).pop();
                              changeDrawerRoute(Routes.jobTypes);
                            },
                            companyPackage: (appDrawerVm.supplierProfileModel
                                    ?.bCrmCompany?.companyPackge ??
                                ''),
                          ),
                          drawerListTiles(
                            title: tr('holiday_periods'),
                            leadingIcon: '',
                            isMainTile: true,
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            trailing: const Icon(
                              FeatherIcons.chevronRight,
                              color: PartnerAppColors.darkBlue,
                            ),
                            onTap: () {
                              Navigator.of(context).pop();
                              changeDrawerRoute(Routes.holidays);
                            },
                            companyPackage: (appDrawerVm.supplierProfileModel
                                    ?.bCrmCompany?.companyPackge ??
                                ''),
                          ),
                          drawerListTiles(
                            title: tr('change_password'),
                            leadingIcon: '',
                            isMainTile: true,
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            trailing: const Icon(
                              FeatherIcons.chevronRight,
                              color: PartnerAppColors.darkBlue,
                            ),
                            onTap: () {
                              Navigator.of(context).pop();
                              changeDrawerRoute(Routes.changePassword);
                            },
                            companyPackage: (appDrawerVm.supplierProfileModel
                                    ?.bCrmCompany?.companyPackge ??
                                ''),
                          ),
                          drawerListTiles(
                            title: tr('integrations'),
                            leadingIcon: '',
                            isMainTile: true,
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            trailing: const Icon(
                              FeatherIcons.chevronRight,
                              color: PartnerAppColors.darkBlue,
                            ),
                            onTap: () {
                              Navigator.of(context).pop();
                              changeDrawerRoute(Routes.integration);
                            },
                            companyPackage: (appDrawerVm.supplierProfileModel
                                    ?.bCrmCompany?.companyPackge ??
                                ''),
                          ),
                          drawerListTiles(
                            title: tr('education_proof'),
                            leadingIcon: '',
                            isMainTile: true,
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            trailing: const Icon(
                              FeatherIcons.chevronRight,
                              color: PartnerAppColors.darkBlue,
                            ),
                            onTap: () {
                              Navigator.of(context).pop();
                              changeDrawerRoute(Routes.education);
                            },
                            companyPackage: (appDrawerVm.supplierProfileModel
                                    ?.bCrmCompany?.companyPackge ??
                                ''),
                          ),
                          drawerListTiles(
                            title: tr('your_phone_numbers'),
                            leadingIcon: '',
                            isMainTile: true,
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            trailing: const Icon(
                              FeatherIcons.chevronRight,
                              color: PartnerAppColors.darkBlue,
                            ),
                            onTap: () {
                              Navigator.of(context).pop();
                              changeDrawerRoute(Routes.phoneNumber);
                            },
                            companyPackage: (appDrawerVm.supplierProfileModel
                                    ?.bCrmCompany?.companyPackge ??
                                ''),
                          ),
                          drawerListTiles(
                            title: tr('your_email'),
                            leadingIcon: '',
                            isMainTile: true,
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            trailing: const Icon(
                              FeatherIcons.chevronRight,
                              color: PartnerAppColors.darkBlue,
                            ),
                            onTap: () {
                              Navigator.of(context).pop();
                              changeDrawerRoute(Routes.yourEmail);
                            },
                            companyPackage: (appDrawerVm.supplierProfileModel
                                    ?.bCrmCompany?.companyPackge ??
                                ''),
                          ),
                          Consumer<PayproffViewModel>(
                              builder: (_, payproffVm, __) {
                            return (payproffVm.payproff?.exists ?? false)
                                ? drawerListTiles(
                                    title: tr('bank_verification'),
                                    leadingIcon: '',
                                    isMainTile: true,
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20),
                                    trailing: const Icon(
                                      FeatherIcons.chevronRight,
                                      color: PartnerAppColors.darkBlue,
                                    ),
                                    onTap: () {
                                      Navigator.of(context).pop();
                                      changeDrawerRoute(
                                          Routes.payproffIbanScreen);
                                    },
                                    companyPackage: (appDrawerVm
                                            .supplierProfileModel
                                            ?.bCrmCompany
                                            ?.companyPackge ??
                                        ''),
                                  )
                                : const SizedBox.shrink();
                          }),
                          drawerListTiles(
                            title: tr('language'),
                            leadingIcon: '',
                            isMainTile: true,
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            trailing: const Icon(
                              FeatherIcons.chevronRight,
                              color: PartnerAppColors.darkBlue,
                            ),
                            onTap: () {
                              Navigator.of(context).pop();
                              changeDrawerRoute(Routes.settings);
                            },
                            companyPackage: (appDrawerVm.supplierProfileModel
                                    ?.bCrmCompany?.companyPackge ??
                                ''),
                          ),
                        ]);
                  },
                  companyPackage:
                      (appDrawerVm.bitrixCompanyModel?.companyPackge ?? ''),
                ),
                const SizedBox(
                  height: 10,
                ),
                drawerListTiles(
                  title: tr('frequently_asked_questions'),
                  leadingIcon: SvgIcons.infoV2,
                  onTap: () {
                    changeDrawerRoute(Routes.faq);
                  },
                  companyPackage:
                      (appDrawerVm.bitrixCompanyModel?.companyPackge ?? ''),
                ),
                const SizedBox(
                  height: 10,
                ),
                drawerListTiles(
                  title: tr('contact_us'),
                  leadingIcon: SvgIcons.phoneCall,
                  onTap: () {
                    changeDrawerRoute(Routes.contactUs);
                  },
                  companyPackage:
                      (appDrawerVm.bitrixCompanyModel?.companyPackge ?? ''),
                ),
                const SizedBox(
                  height: 10,
                ),
                drawerListTiles(
                  title: tr('logout'),
                  leadingIcon: SvgIcons.logout,
                  onTap: () => logout(),
                  companyPackage:
                      (appDrawerVm.bitrixCompanyModel?.companyPackge ?? ''),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }

  Widget expandableListTile(
      {required String leadingIcon,
      required bool isExpanded,
      required String title,
      List<Widget> children = const [],
      void Function(bool)? onExpansionChanged}) {
    return ExpansionTile(
      tilePadding: EdgeInsets.zero,
      // childrenPadding: const EdgeInsets.symmetric(horizontal: 15),
      leading: SizedBox(
        height: 25,
        width: 25,
        child: SvgPicture.asset(leadingIcon),
      ),
      trailing: Icon(
        isExpanded ? FeatherIcons.chevronDown : FeatherIcons.chevronRight,
        color: PartnerAppColors.darkBlue,
      ),
      title: Text(
        title,
        style: Theme.of(context).textTheme.titleLarge!.copyWith(
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.normal,
            ),
      ),
      onExpansionChanged: onExpansionChanged,
      children: children,
    );
  }

  Widget drawerListTiles({
    EdgeInsetsGeometry? padding = EdgeInsets.zero,
    required String title,
    required String leadingIcon,
    required String companyPackage,
    bool isMainTile = false,
    void Function()? onTap,
    Widget? trailing,
    bool isSubMenu = false,
  }) {
    if ((title == tr('recommendations') ||
            title == tr('my_projects') ||
            title == tr('marketplace') ||
            title == tr('teacher_to_teacher') ||
            title == tr('membership_benefits') ||
            title == tr('membership_benefits')) &&
        companyPackage == 'affiliatepartner') {
      return const SizedBox.shrink();
    }

    return ListTile(
      onTap: onTap,
      contentPadding: padding,
      leading: leadingIcon.isNotEmpty
          ? SizedBox(
              height: 25,
              width: 25,
              child: SvgPicture.asset(
                leadingIcon,
                colorFilter: ColorFilter.mode(
                  isMainTile
                      ? PartnerAppColors.darkBlue
                      : PartnerAppColors.grey1,
                  BlendMode.srcIn,
                ),
              ),
            )
          : isSubMenu
              ? const SizedBox(
                  height: 25,
                  width: 25,
                )
              : null,
      title: Text(
        title,
        style: Theme.of(context).textTheme.titleLarge!.copyWith(
              color: isMainTile
                  ? PartnerAppColors.darkBlue
                  : PartnerAppColors.grey1,
              fontWeight: FontWeight.normal,
            ),
      ),
      trailing: trailing,
    );
  }
}
