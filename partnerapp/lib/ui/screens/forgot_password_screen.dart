import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/main/main_service.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
// import 'package:Haandvaerker.dk/services/main_service.dart';
import 'package:easy_localization/easy_localization.dart';
// import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
// import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
// import 'package:provider/provider.dart';
import 'package:validators/validators.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({super.key});

  @override
  ForgotPasswordScreenState createState() => ForgotPasswordScreenState();
}

class ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final _formKey = GlobalKey<FormState>();

  final textController = TextEditingController();

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  void onPressed({required UserViewModel userVm}) async {
    final mainService = context.read<MainService>();

    userVm.isRequesting = true;
    userVm.setIsInvalidEmail = false;
    if (_formKey.currentState!.validate()) {
      final email = textController.value.text.trim();

      try {
        await mainService.forgotPassword(email: email).then((value) {
          if (!mounted) return;

          userVm.isRequesting = false;
          if (value?.success != null) {
            userVm.isForgotPasswordSuccess = (value?.success ?? false);
            userVm.setIsInvalidEmail = false;
          }
          if (userVm.isForgotPasswordSuccess) {
            showSuccessAnimationDialog(
                    context, true, tr('login_data_will_be_sent_to_email'))
                .then((value) {
              if (!mounted) return;

              Navigator.of(context).pop();
            });
          }
        });
      } on ApiException catch (e) {
        userVm.isRequesting = false;
        if (e.message.contains('email')) {
          userVm.setIsInvalidEmail = true;
        } else {
          userVm.setIsInvalidEmail = false;
          final errorMessage = e.message;
          await Sentry.captureMessage('TypeError: ${e.message}')
              .then((a) async {
            if (!mounted) return;

            await showOkAlertDialog(
              context: context,
              title: tr('error'),
              message: errorMessage,
              okLabel: tr('ok'),
            );
          });
        }
      }
    } else {
      userVm.isRequesting = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      top: true,
      bottom: true,
      child: Consumer<UserViewModel>(builder: (_, userVm, __) {
        return SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          padding: const EdgeInsets.only(bottom: 150),
          child: Container(
            constraints: const BoxConstraints(maxWidth: 400),
            margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Text(
                    tr('back'),
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        color: Theme.of(context).colorScheme.primary,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                SmartGaps.gapH100,
                Text(
                  tr('forgot_your_password'),
                  textAlign: TextAlign.start,
                  style: const TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 22,
                      color: Color.fromRGBO(0, 54, 69, 1)),
                ),
                SmartGaps.gapH10,
                Text(
                  tr('forgot_password_info'),
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                      fontSize: 16,
                      height: 1.618,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(0, 54, 69, 1)),
                ),
                SmartGaps.gapH40,
                Form(
                    key: _formKey,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    child: Column(children: <Widget>[
                      TextFormField(
                          controller: textController,
                          style: const TextStyle(
                              fontSize: 22,
                              letterSpacing: -0.5,
                              color: Color.fromRGBO(0, 54, 69, 1)),
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintStyle: const TextStyle(fontSize: 18),
                              filled: true,
                              fillColor:
                                  const Color.fromRGBO(223, 230, 233, 0.8),
                              hintText: tr('email')),
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            if (value == null || value.trim().isEmpty) {
                              return tr('please_enter_some_text');
                            }
                            if (isEmail(value.trim()) == false ||
                                (userVm.isInvalidEmail != null &&
                                    userVm.isInvalidEmail == true)) {
                              return tr('enter_valid_email');
                            }
                            return null;
                          }),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        child: SizedBox(
                            height: 70,
                            width: double.infinity,
                            child: CustomDesignTheme.flatButtonStyle(
                              backgroundColor:
                                  Theme.of(context).colorScheme.primary,
                              onPressed: () => onPressed(userVm: userVm),
                              child: Text(
                                tr('send_login_data'),
                                style: const TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white,
                                  letterSpacing: -1,
                                ),
                              ),
                            )),
                      ),
                      Provider.of<UserViewModel>(context, listen: true)
                              .isRequesting
                          ? const Center(
                              child: SizedBox(
                                  height: 25,
                                  width: 25,
                                  child: CircularProgressIndicator(
                                      strokeWidth: 2)),
                            )
                          : Container()
                    ]))
              ],
            ),
          ),
        );
      }),
    ));
  }
}
