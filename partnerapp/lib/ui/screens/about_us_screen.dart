import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class TermsAndPolicies extends StatelessWidget {
  const TermsAndPolicies({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Container(
        padding: const EdgeInsets.all(10),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
            child: Text(tr('about_us'),
                style: Theme.of(context).textTheme.headlineMedium),
          ),
          const Divider(),
          ListTile(
            onTap: () {
              launchUrl(Uri.parse(Links.termsOfUseUrl));
            },
            leading: const Icon(Icons.notes),
            title: Text(tr('terms_of_use'),
                style: Theme.of(context).textTheme.headlineMedium),
            subtitle: Text(tr('terms_of_use_desc'),
                style: Theme.of(context).textTheme.bodyMedium),
          ),
          const Divider(),
          ListTile(
            onTap: () {
              launchUrl(Uri.parse(Links.privacyPolicyUrl));
            },
            leading: const Icon(Icons.lock),
            title: Text(tr('privacy_policy'),
                style: Theme.of(context).textTheme.headlineMedium),
            subtitle: Text(tr('privacy_policy_desc'),
                style: Theme.of(context).textTheme.bodyMedium),
          ),
        ]),
      ),
    );
  }
}
