import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/messages/components.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app_links/app_links_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/edit_offer/edit_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class LandingScreen extends StatefulWidget {
  const LandingScreen({super.key});

  @override
  State<LandingScreen> createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final appLinksVm = context.read<AppLinksViewModel>();
      final editOfferVm = context.read<EditOfferViewmodel>();

      if (appLinksVm.offerId.isEmpty) {
        backDrawerRoute();
      } else {
        await tryCatchWrapper(
                context: myGlobals.homeScaffoldKey!.currentContext!,
                function: editOfferVm.getOfferId(
                    offerId: int.parse(appLinksVm.offerId)))
            .whenComplete(() async {
          if ((editOfferVm.viewedOffer?.status ?? '') == 'accepted') {
            backDrawerRoute();

            if ((editOfferVm.viewedOffer?.projectId ?? 0) > 0) {
              changeDrawerRoute(
                Routes.projectDashboard,
                arguments: {
                  'projectId': editOfferVm.viewedOffer!.projectId,
                  'pageRedirect': 'edit_offer_contract'
                },
              );
            }
          } else {
            if ((editOfferVm.viewedOffer?.createdBy ?? '') != 'homeowner') {
              backDrawerRoute();
            }
          }
        });
      }

      appLinksVm.resetLoginAppLink();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      bottomNavigationBar: Container(
          padding:
              const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
          color: Colors.white,
          child: SizedBox(
            height: 60,
            child: CustomButton(
              onPressed: () {
                changeDrawerRoute(Routes.editOffer);
              },
              text: tr('see_updated_offer'),
            ),
          )),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: Consumer<EditOfferViewmodel>(
          builder: (_, vm, __) {
            return Skeletonizer(
              enabled: vm.busy,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                      text: TextSpan(children: [
                    TextSpan(
                      text: '${tr('welcome')}, ',
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(fontWeight: FontWeight.normal),
                    ),
                    TextSpan(
                      text:
                          context.read<UserViewModel>().userModel?.user?.name ??
                              '',
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(fontWeight: FontWeight.bold),
                    )
                  ])),
                  SmartGaps.gapH10,
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: "${(vm.viewedOffer?.homeowner?.name ?? '')}  ",
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                          text: "${tr('offer_changes_description_1')}  ",
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(fontWeight: FontWeight.normal),
                        ),
                        TextSpan(
                          text: Formatter.formatDateStrings(
                              type: DateFormatType.standardDate,
                              dateString: vm.viewedOffer?.updatedAt),
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                          text: "${tr('for_project')} ",
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(fontWeight: FontWeight.normal),
                        ),
                        TextSpan(
                          text: vm.activeProject.projectName ?? '',
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                  SmartGaps.gapH10,
                  Text("${tr('offer_changes_description_2')} ",
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(fontWeight: FontWeight.normal)),
                  SmartGaps.gapH10,
                  Text(tr('offer_changes_description_3'),
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(fontWeight: FontWeight.normal)),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
