import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/components/signature_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/messages/components.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/iterable_utils.dart';
import 'package:Haandvaerker.dk/utils/string_utils.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/edit_offer/edit_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/payproff/payproff_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/request_payment/payment_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

Future<String?> successDialog({
  required BuildContext context,
  required bool showButton,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Text(
                  tr('you_have_accepted_changes_offer'),
                  style: Theme.of(context).textTheme.displayLarge!.copyWith(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: PartnerAppColors.darkBlue,
                      ),
                ),
                if (showButton) ...[
                  SmartGaps.gapH20,
                  CustomButton(
                    backgroundColor: PartnerAppColors.blue,
                    text: tr('create_contract_now'),
                    onPressed: () {
                      Navigator.of(dialogContext).pop('create_contract');
                    },
                  ),
                ]
              ],
            ),
          ),
        ),
      );
    },
  );
}

class EditOffer extends StatefulWidget {
  const EditOffer({super.key});

  @override
  State<EditOffer> createState() => _EditOfferState();
}

class _EditOfferState extends State<EditOffer> {
  bool seeMore = false;

  void onPressNext() async {
    final editOfferVm = context.read<EditOfferViewmodel>();
    final projectVm = context.read<MyProjectsViewModel>();
    final paymentVm = context.read<PaymentViewModel>();
    final payproffVm = context.read<PayproffViewModel>();
    final termsVm = context.read<SharedDataViewmodel>();

    final signature = await signatureDialog(
        context: context,
        label: termsVm.termsLabel,
        htmlTerms: termsVm.termsHtml);

    if (!mounted && signature == null) return;

    modalManager.showLoadingModal();

    final isAccepted = await tryCatchWrapper(
        context: context, function: editOfferVm.acceptOfferVersion());

    if (!mounted) return;

    if ((isAccepted ?? false)) {
      await tryCatchWrapper(
          context: context,
          function: Future.wait([
            paymentVm.getPaymentStages(
                projectId: editOfferVm.viewedOffer!.projectId),
            projectVm.getProjectId(
                projectid: editOfferVm.viewedOffer!.projectId!),
            payproffVm.checkPayproffAcount()
          ]));

      if (!mounted) return;

      modalManager.hideLoadingModal();
      projectVm.currentProjectId = editOfferVm.viewedOffer!.projectId;

      final acceptedOffers = [
        ...(projectVm.activeProject.offers ?? [])
            .where((e) => e.status == 'accepted')
      ];

      if (acceptedOffers.isNotEmpty) {
        if (acceptedOffers.first.contractId == null) {
          await successDialog(context: context, showButton: true).then((a) {
            editOfferVm.reset();
            backDrawerRoute();
            backDrawerRoute();

            if (a == 'create_contract') {
              changeDrawerRoute(Routes.contractWizard);
            } else {
              changeDrawerRoute(Routes.yourClient);
            }
          });
        } else {
          changeDrawerRoute(Routes.contractPreview,
              arguments: {'isFrom': 'editOffer'});
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      bottomNavigationBar: Container(
          padding:
              const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
          color: Colors.white,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 60,
                child: CustomButton(
                  onPressed: () => onPressNext(),
                  text: tr('next'),
                ),
              ),
              SmartGaps.gapH10,
              TextButton(
                  onPressed: () async {
                    final createOfferVm = context.read<CreateOfferViewModel>();
                    final editOfferVm = context.read<EditOfferViewmodel>();
                    final offerVm = context.read<OfferViewModel>();

                    modalManager.showLoadingModal();

                    await tryCatchWrapper(
                            context: myGlobals.homeScaffoldKey!.currentContext!,
                            function: editOfferVm.getJobId())
                        .then((job) {
                      modalManager.hideLoadingModal();
                      if (job != null) {
                        if (editOfferVm.viewedOffer?.offerType == 'normal') {
                          createOfferVm.toUpdateOffer = false;
                          createOfferVm.isNewOfferVersion = true;

                          backDrawerRoute();
                          backDrawerRoute();
                          changeDrawerRoute(
                            Routes.createOffer,
                            arguments: {
                              'job': job,
                            },
                          );
                        } else {
                          offerVm.isUploadOfferUpdate = false;
                          offerVm.isNewOfferVersion = true;

                          backDrawerRoute();
                          backDrawerRoute();
                          changeDrawerRoute(Routes.uploadOffer, arguments: job);
                        }

                        editOfferVm.reset();
                      }
                    });
                  },
                  child: Text(tr('make_a_new_offer'),
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.fade,
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(
                              fontWeight: FontWeight.normal,
                              color: PartnerAppColors.blue)))
            ],
          )),
      body: SingleChildScrollView(
        physics: seeMore
            ? const NeverScrollableScrollPhysics()
            : const AlwaysScrollableScrollPhysics(),
        padding: const EdgeInsets.all(20),
        child: Consumer<EditOfferViewmodel>(
          builder: (_, vm, __) {
            return Skeletonizer(
              enabled: vm.busy,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (vm.viewedOffer?.offerType == 'normal') ...[
                    ...vm.viewedOfferListPrice
                        .groupBy((value) => value.industryName ?? '')
                        .entries
                        .map((map) {
                      final index = vm.viewedOfferListPrice
                          .groupBy((value) => value.industryName ?? '')
                          .keys
                          .toList()
                          .indexOf(map.key);

                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: const EdgeInsets.only(bottom: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${index + 1}. ${(map.key)}",
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineMedium!
                                      .copyWith(
                                          fontWeight: FontWeight.bold,
                                          color: PartnerAppColors.blue),
                                ),
                                SmartGaps.gapH10,
                                ...map.value.map((listPrice) {
                                  final typeIndex =
                                      map.value.indexOf(listPrice);

                                  return Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.all(20),
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: PartnerAppColors.darkBlue
                                                    .withValues(alpha: .4)),
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text(
                                                  "${index + 1}.${typeIndex + 1} ${(listPrice.jobIndustryName ?? '')}",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headlineMedium!
                                                      .copyWith(
                                                          fontWeight:
                                                              FontWeight.bold),
                                                ),
                                                SmartGaps.gapH10,
                                                Text(
                                                  Formatter.curencyFormat(
                                                      amount: num.parse(
                                                          listPrice.total ??
                                                              '0')),
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headlineMedium!
                                                      .copyWith(
                                                          fontWeight:
                                                              FontWeight.bold),
                                                ),
                                              ],
                                            ),
                                            SmartGaps.gapH5,
                                            if (listPrice.series != null ||
                                                listPrice.series is List) ...[
                                              ...(listPrice.series as List)
                                                  .map((description) {
                                                final descriptionIndex =
                                                    (listPrice.series as List)
                                                        .indexOf(description);

                                                return Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(
                                                      vertical: 5,
                                                      horizontal: 20),
                                                  child: Text(
                                                      "${index + 1}.${typeIndex + 1}.${descriptionIndex + 1}  $description",
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .titleSmall!
                                                          .copyWith(
                                                              fontSize: 14,
                                                              color:
                                                                  PartnerAppColors
                                                                      .darkBlue,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal)),
                                                );
                                              })
                                            ] else ...[
                                              ...(listPrice.seriesOriginal
                                                      as List)
                                                  .map((description) {
                                                final descriptionIndex =
                                                    (listPrice.seriesOriginal
                                                            as List)
                                                        .indexOf(description);

                                                return Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(vertical: 5),
                                                  child: Text(
                                                      "1.1.${descriptionIndex + 1}  $description",
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .titleSmall!
                                                          .copyWith(
                                                              fontSize: 14,
                                                              color:
                                                                  PartnerAppColors
                                                                      .darkBlue,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal)),
                                                );
                                              })
                                            ],
                                          ],
                                        ),
                                      ),
                                      if ((listPrice.note ?? '')
                                          .isNotEmpty) ...[
                                        const SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          tr('craftsman_standard_text'),
                                          style: Theme.of(context)
                                              .textTheme
                                              .headlineMedium!
                                              .copyWith(
                                                fontWeight: FontWeight.bold,
                                              ),
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Container(
                                          width: double.infinity,
                                          padding: const EdgeInsets.all(20),
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: PartnerAppColors
                                                      .darkBlue
                                                      .withValues(alpha: .4)),
                                              borderRadius:
                                                  BorderRadius.circular(5)),
                                          child: Text(listPrice.note ?? '',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .titleSmall!
                                                  .copyWith(
                                                      fontSize: 14,
                                                      color: PartnerAppColors
                                                          .darkBlue,
                                                      fontWeight:
                                                          FontWeight.normal)),
                                        )
                                      ],
                                    ],
                                  );
                                })
                              ],
                            ),
                          )
                        ],
                      );
                    })
                  ] else ...[
                    if ((vm.viewedOffer?.offerFile ?? '').isNotEmpty)
                      Container(
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: PartnerAppColors.spanishGrey
                                  .withValues(alpha: .3),
                              borderRadius: BorderRadius.circular(5)),
                          height: seeMore
                              ? (MediaQuery.of(context).size.height / 2)
                              : 300,
                          child: Column(
                            children: [
                              Expanded(
                                  child: SfPdfViewer.network(
                                      vm.viewedOffer!.offerFile!)),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    seeMore = !seeMore;
                                  });
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Center(
                                    child: Text(
                                        seeMore ? tr('hide') : tr('see_more'),
                                        style: Theme.of(context)
                                            .textTheme
                                            .headlineMedium!
                                            .copyWith(
                                                letterSpacing: 1.0,
                                                color: PartnerAppColors.blue,
                                                fontWeight: FontWeight.normal,
                                                fontSize: 18)),
                                  ),
                                ),
                              )
                            ],
                          ))
                  ],
                  SmartGaps.gapH10,
                  const Divider(),
                  SmartGaps.gapH10,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Text(
                          tr('unit_price_ex_vat'),
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(
                                  letterSpacing: 1.0,
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 18),
                        ),
                      ),
                      Text(
                        Formatter.curencyFormat(
                            amount: vm.viewedOffer?.subtotalPrice ?? 0),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                letterSpacing: 1.0,
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 18),
                      )
                    ],
                  ),
                  SmartGaps.gapH10,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Text(
                          tr('vat_value'),
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(
                                  letterSpacing: 1.0,
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 18),
                        ),
                      ),
                      Text(
                        Formatter.curencyFormat(
                            amount: vm.viewedOffer?.vat ?? 0),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                letterSpacing: 1.0,
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 18),
                      )
                    ],
                  ),
                  SmartGaps.gapH10,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Text(
                          tr('total_price_incl_vat'),
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(
                                  letterSpacing: 1.0,
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 18),
                        ),
                      ),
                      Text(
                        Formatter.curencyFormat(
                            amount: vm.viewedOffer?.totalVat ?? 0),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                letterSpacing: 1.0,
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 18),
                      )
                    ],
                  ),
                  SmartGaps.gapH10,
                  const Divider(),
                  SmartGaps.gapH20,
                  Text(tr('project_day_estimate'),
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(
                              letterSpacing: 1.0,
                              color: PartnerAppColors.darkBlue,
                              fontWeight: FontWeight.w500,
                              fontSize: 18)),
                  SmartGaps.gapH10,
                  RichText(
                      text: TextSpan(children: [
                    const WidgetSpan(
                        alignment: PlaceholderAlignment.middle,
                        child: Icon(
                          FeatherIcons.calendar,
                          color: PartnerAppColors.blue,
                        )),
                    TextSpan(
                        text:
                            '${vm.viewedOffer?.expiry ?? 0} ${tr('days').toCapitalizedFirst()}',
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                letterSpacing: 1.0,
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.w500,
                                fontSize: 16))
                  ])),
                  SmartGaps.gapH20,
                  Text(tr('date_to_start_project'),
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(
                              letterSpacing: 1.0,
                              color: PartnerAppColors.darkBlue,
                              fontWeight: FontWeight.w500,
                              fontSize: 18)),
                  SmartGaps.gapH10,
                  RichText(
                      text: TextSpan(children: [
                    const WidgetSpan(
                        alignment: PlaceholderAlignment.middle,
                        child: Icon(
                          FeatherIcons.calendar,
                          color: PartnerAppColors.blue,
                        )),
                    TextSpan(
                        text: Formatter.formatDateStrings(
                            type: DateFormatType.standardDate,
                            dateString: vm.viewedOffer?.startDate),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                letterSpacing: 1.0,
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.w500,
                                fontSize: 16))
                  ])),
                  SmartGaps.gapH20,
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: tr('your_offer_is_valid'),
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(
                                  letterSpacing: 1.0,
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 18),
                        ),
                        TextSpan(
                          text: ' ${vm.viewedOffer?.expiry ?? 0} ',
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(
                                  letterSpacing: 1.0,
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                        ),
                        TextSpan(
                          text: tr('days_valid'),
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(
                                  letterSpacing: 1.0,
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 18),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
