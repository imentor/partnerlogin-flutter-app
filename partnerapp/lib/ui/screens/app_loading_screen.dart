import 'dart:async';

import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:show_up_animation/show_up_animation.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';

class AppSplashScreen extends StatefulWidget {
  const AppSplashScreen({super.key, this.isFromAppLink = false});

  final bool isFromAppLink;

  @override
  AppSplashScreenState createState() => AppSplashScreenState();
}

class AppSplashScreenState extends State<AppSplashScreen> {
  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      FlutterNativeSplash.remove();
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedLoadingScreen(
      isFromAppLink: widget.isFromAppLink,
    );
  }
}

class AnimatedLoadingScreen extends StatefulWidget {
  const AnimatedLoadingScreen({super.key, this.isFromAppLink = false});

  final bool isFromAppLink;

  @override
  AnimatedLoadingScreenState createState() => AnimatedLoadingScreenState();
}

class AnimatedLoadingScreenState extends State<AnimatedLoadingScreen>
    with TickerProviderStateMixin {
  late AnimationController animationController;
  double? height;
  double? width;
  double? containerHeight;
  double? containerWidth;
  double? positiontop;
  double? positionleft;
  bool showBiggerContainer = true;
  double? nameHeight;
  double? nameWidth;
  double? houseHeight;
  double? houseWidth;
  double? logoSize;

  @override
  void initState() {
    containerHeight = 1000;
    containerWidth = 1000;
    nameHeight = 0;
    nameWidth = 0;
    logoSize = 50;
    houseHeight = 80;
    houseWidth = 80;
    positiontop = 0;
    positionleft = 0;
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 5),
    )
      ..forward()
      ..repeat(reverse: false)
      ..addListener(() {
        if (animationController.isCompleted) {
          route();
        }
      });
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      setState(() {
        containerHeight = MediaQuery.of(context).size.height -
            MediaQuery.of(context).padding.top;
        containerWidth = MediaQuery.of(context).size.height -
            MediaQuery.of(context).padding.top;
        positiontop = ((MediaQuery.of(context).size.height -
                    (MediaQuery.of(context).padding.top +
                        MediaQuery.of(context).padding.bottom)) /
                2) -
            25;
        positionleft = (MediaQuery.of(context).size.width / 2) - 25;
        nameWidth = MediaQuery.of(context).size.width * 0.60;
      });
      runContainer();
      runPosition();
      startTime();
    });
  }

  startTime() async {
    var duration = const Duration(seconds: 3, milliseconds: 600);
    return Timer(duration, route);
  }

  runContainer() async {
    var duration = const Duration(seconds: 1);
    return Timer(duration, changeContainer);
  }

  changeContainer() {
    if (mounted) {
      setState(() {
        containerHeight = 50;
        containerWidth = 50;
        houseWidth = 30;
        houseHeight = 30;
      });
    }
  }

  runPosition() async {
    var duration = const Duration(seconds: 3);
    return Timer(duration, changePosition);
  }

  changePosition() async {
    if (mounted) {
      setState(() {
        showBiggerContainer = false;
        positiontop = 12;
        positionleft = 17;
        logoSize = 32;
      });
    }
  }

  route() {
    if (mounted) {
      Navigator.pushReplacementNamed(context, Routes.home,
          arguments: widget.isFromAppLink);
    }
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        appBarTheme:
            const AppBarTheme(systemOverlayStyle: SystemUiOverlayStyle.dark),
        primaryColor: Colors.white,
      ),
      child: SafeArea(
        child: Scaffold(
          body: Container(
            color: Colors.white,
            child: Stack(
              children: <Widget>[
                showBiggerContainer
                    ? Align(
                        alignment: Alignment.center,
                        child: AnimatedContainer(
                          curve: Curves.ease,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(2),
                            gradient: const LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  Color(0xFF0090B8),
                                  Color(0xFF13A2C0),
                                  Color(0xFF2CB1C4),
                                  Color(0xFF49C2CA),
                                  Color(0xFF68C8CA),
                                ]),
                          ),
                          height: containerHeight,
                          width: containerWidth,
                          duration: const Duration(seconds: 1),
                        ))
                    : Container(),
                showBiggerContainer && houseHeight != 0 && houseWidth != 0
                    ? Align(
                        alignment: Alignment.center,
                        child: AnimatedContainer(
                          curve: Curves.ease,
                          height: houseHeight,
                          width: houseWidth,
                          duration: const Duration(seconds: 1),
                          child: Image.asset(
                            ImagePaths.houseLogo,
                            color: Colors.white,
                          ),
                        ))
                    : Container(),
                nameHeight == 0 && nameWidth == 0
                    ? Container()
                    : Align(
                        alignment: Alignment.center,
                        child: Container(
                          margin: const EdgeInsets.only(top: 150),
                          child: ShowUpAnimation(
                            //delayStart: Duration(seconds: 1),
                            animationDuration: const Duration(seconds: 1),
                            curve: Curves.ease,
                            direction: Direction.vertical,
                            offset: -0.5,
                            child: Image.asset(
                              ImagePaths.haandvaerkerDk,
                              color: Colors.white,
                              width: nameWidth,
                            ),
                          ),
                        ),
                      ),
                AnimatedPositioned(
                  curve: Curves.easeIn,
                  top: positiontop,
                  left: positionleft,
                  duration: const Duration(milliseconds: 500),
                  child: Opacity(
                    opacity: showBiggerContainer ? 0.0 : 1.0,
                    child: AnimatedContainer(
                      duration: const Duration(milliseconds: 500),
                      height: logoSize,
                      width: logoSize,
                      child: Image.asset(
                        ImagePaths.logo,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
