import 'package:Haandvaerker.dk/ui/components/cache_image.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/viewmodel/badges/badges_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BadgeView extends StatelessWidget {
  const BadgeView({
    super.key,
    required this.badge,
    required this.isImage,
    required this.badgeName,
    required this.download,
  });
  final String badge;
  final bool isImage;
  final String badgeName;
  final VoidCallback download;

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: context.watch<BadgesViewModel>().isDownloading,
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 50, 20, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    splashRadius: 20,
                    onPressed: context.watch<BadgesViewModel>().isDownloading
                        ? null
                        : () {
                            ScaffoldMessenger.of(context).clearSnackBars();
                            Navigator.pop(context);
                          },
                    icon: const Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                      size: 30,
                    ),
                  ),
                  Text(
                    !isImage ? tr(badgeName) : badgeName,
                    style: context.pttBodySmall.copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.w700,
                      fontSize: 14,
                    ),
                  ),
                  IconButton(
                    splashRadius: 20,
                    onPressed: context.watch<BadgesViewModel>().isDownloading
                        ? null
                        : download,
                    icon: const Icon(
                      Icons.download_rounded,
                      color: Colors.white,
                      size: 30,
                    ),
                  ),
                ],
              ),
            ),
            if (isImage)
              Expanded(
                child: CacheImage(
                  fit: BoxFit.contain,
                  imageUrl: badge,
                ),
              )
            else
              const Expanded(
                child: Icon(
                  Icons.file_present_rounded,
                  color: Colors.white,
                  size: 150,
                ),
              ),
          ],
        ),
      ),
    );
  }
}
