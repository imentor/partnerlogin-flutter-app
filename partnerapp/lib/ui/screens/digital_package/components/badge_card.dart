import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/cache_image.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class BadgeCard extends StatelessWidget {
  const BadgeCard({
    super.key,
    required this.badge,
    required this.badgeName,
    this.isPdf,
  });

  final String badge;
  final String badgeName;
  final bool? isPdf;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
        side: BorderSide(
          color: PartnerAppColors.grey.withValues(alpha: 0.5),
        ),
      ),
      clipBehavior: Clip.antiAlias,
      margin: const EdgeInsets.only(bottom: 10),
      child: Column(
        children: [
          Expanded(
            child: isPdf == null
                ? const Icon(
                    Icons.file_present_rounded,
                    color: PartnerAppColors.darkBlue,
                    size: 50,
                  )
                : isPdf!
                    ? const Icon(
                        Icons.picture_as_pdf,
                        color: PartnerAppColors.darkBlue,
                        size: 50,
                      )
                    : CacheImage(
                        fit: BoxFit.cover,
                        imageUrl: badge,
                      ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  isPdf == null
                      ? Icons.file_present_rounded
                      : isPdf!
                          ? Icons.picture_as_pdf
                          : Icons.image,
                  color: PartnerAppColors.darkBlue,
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 5),
                    child: Text(
                      isPdf ?? true ? tr(badgeName) : badgeName,
                      overflow: TextOverflow.ellipsis,
                      style: context.pttBodySmall.copyWith(
                        color: PartnerAppColors.darkBlue,
                        fontSize: 12,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
