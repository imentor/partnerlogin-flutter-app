import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/pdf_viewer.dart';
import 'package:Haandvaerker.dk/ui/screens/digital_package/components/badge_card.dart';
import 'package:Haandvaerker.dk/ui/screens/digital_package/components/badge_view.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/messages/components.dart';
import 'package:Haandvaerker.dk/utils/extension/snackbar.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/badges/badges_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/widgets_recommendations_viewmodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class DigitalPackageScreen extends StatefulWidget {
  const DigitalPackageScreen({super.key});

  @override
  State<DigitalPackageScreen> createState() => _DigitalPackageScreenState();
}

class _DigitalPackageScreenState extends State<DigitalPackageScreen> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final newsFeedVm = context.read<NewsFeedViewModel>();
      final appDrawerVm = context.read<AppDrawerViewModel>();
      const dynamicStoryPage = 'badges';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      if (dynamicStoryEnable) {
        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: Future.wait([
              context.read<BadgesViewModel>().getPrisenBadges(),
              newsFeedVm.getDynamicStory(page: 'badges'),
            ])).whenComplete(() {
          if (!mounted) return;

          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      } else {
        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: Future.value(
                context.read<BadgesViewModel>().getPrisenBadges()));
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ///
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: Consumer<BadgesViewModel>(
            builder: (_, badgesVm, __) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SmartGaps.gapH10,
                  Text(tr('digital_package'), style: context.pttTitleLarge),
                  SmartGaps.gapH20,
                  Builder(builder: (context) {
                    final widgetsVm = context.watch<WidgetsViewModel>();
                    final userVm = context.watch<UserViewModel>();

                    if (badgesVm.busy || badgesVm.badgeImages.isEmpty) {
                      return Skeletonizer(
                        enabled: true,
                        child: GridView.count(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          crossAxisCount: 2,
                          childAspectRatio: 0.9,
                          crossAxisSpacing: 10,
                          children: List.generate(
                            10,
                            (index) => const BadgeCard(
                              badge: '',
                              badgeName: 'sample.jpg',
                              isPdf: false,
                            ),
                          ),
                        ),
                      );
                    }

                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if ((widgetsVm.company?.otherBadges ?? [])
                            .isNotEmpty) ...[
                          if ((widgetsVm.company?.otherBadges!.first.type ==
                                  1997) &&
                              (widgetsVm.company?.otherBadges!.first.week ==
                                  21) &&
                              (widgetsVm.company?.otherBadges!.first.month ==
                                  5) &&
                              (widgetsVm.company?.otherBadges!.first.year ==
                                  2024)) ...[
                            InkWell(
                              onTap: () async {
                                await saveFile(
                                    url:
                                        'https://widget5.haandvaerker.dk/klima/certificate/klima_certificate_${userVm.userModel?.user?.id}.pdf',
                                    filename: 'diplom-pdf.pdf',
                                    isDocs: true,
                                    badgesVm: badgesVm);
                              },
                              child: RichText(
                                text: TextSpan(
                                  children: [
                                    WidgetSpan(
                                      alignment: PlaceholderAlignment.middle,
                                      child: Container(
                                        margin: const EdgeInsets.only(right: 5),
                                        child: const Icon(
                                          FeatherIcons.fileText,
                                          color: PartnerAppColors.malachite,
                                        ),
                                      ),
                                    ),
                                    TextSpan(
                                        text: 'Download diplom-pdf',
                                        style: context.pttTitleMedium.copyWith(
                                            color: PartnerAppColors.malachite,
                                            fontWeight: FontWeight.bold))
                                  ],
                                ),
                              ),
                            ),
                            SmartGaps.gapH10,
                          ],
                          if ((widgetsVm.company?.otherBadges!.first.type ==
                              1997)) ...[
                            InkWell(
                              onTap: () async {
                                await saveFile(
                                    url: 'assets/images/signatur_skabelon.jpg',
                                    filename: 'signatur skabelon.jpg',
                                    isDocs: true,
                                    isFromAsset: true,
                                    badgesVm: badgesVm);
                              },
                              child: RichText(
                                text: TextSpan(
                                  children: [
                                    WidgetSpan(
                                      alignment: PlaceholderAlignment.middle,
                                      child: Container(
                                        margin: const EdgeInsets.only(right: 5),
                                        child: const Icon(
                                          FeatherIcons.fileText,
                                          color: PartnerAppColors.malachite,
                                        ),
                                      ),
                                    ),
                                    TextSpan(
                                        text:
                                            'Download email signatur skabelon',
                                        style: context.pttTitleMedium.copyWith(
                                            color: PartnerAppColors.malachite,
                                            fontWeight: FontWeight.bold))
                                  ],
                                ),
                              ),
                            ),
                            SmartGaps.gapH10,
                            InkWell(
                              onTap: () async {
                                await saveFile(
                                    url: 'assets/images/vejledningen.jpg',
                                    filename: 'vejledningen.png',
                                    isDocs: true,
                                    badgesVm: badgesVm,
                                    isFromAsset: true);
                              },
                              child: RichText(
                                text: TextSpan(
                                  children: [
                                    WidgetSpan(
                                      alignment: PlaceholderAlignment.middle,
                                      child: Container(
                                        margin: const EdgeInsets.only(right: 5),
                                        child: const Icon(
                                          FeatherIcons.fileText,
                                          color: PartnerAppColors.malachite,
                                        ),
                                      ),
                                    ),
                                    TextSpan(
                                        text: 'Download vejledningen',
                                        style: context.pttTitleMedium.copyWith(
                                            color: PartnerAppColors.malachite,
                                            fontWeight: FontWeight.bold))
                                  ],
                                ),
                              ),
                            ),
                            SmartGaps.gapH20,
                            Container(
                              height: 200,
                              width: 200,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: CachedNetworkImageProvider(
                                          'https://widget5.haandvaerker.dk/klima/rendered/${userVm.userModel?.user?.id}/klima_${userVm.userModel?.user?.id}.png'))),
                            ),
                            SmartGaps.gapH10,
                            SizedBox(
                              width: 200,
                              child: CustomButton(
                                text: 'Download klimamærke',
                                textSize: 16,
                                onPressed: () async {
                                  await saveFile(
                                      url:
                                          'https://widget5.haandvaerker.dk/klima/rendered/${userVm.userModel?.user?.id}/klima_${userVm.userModel?.user?.id}.png',
                                      filename: 'klimamærke.png',
                                      isDocs: true,
                                      badgesVm: badgesVm);
                                },
                              ),
                            )
                          ],
                          SmartGaps.gapH30,
                        ],
                        GridView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            crossAxisSpacing: 10,
                            childAspectRatio: 0.85,
                          ),
                          shrinkWrap: true,
                          itemCount: badgesVm.badgeImages.length,
                          itemBuilder: (context, index) {
                            final badge =
                                badgesVm.badgeImages.values.toList()[index];

                            final badgeName =
                                badgesVm.badgeImages.keys.toList()[index];

                            bool isDocs = false;
                            bool isPdf = false;
                            if (!badgeName.contains('.jpg')) {
                              isDocs = true;
                            }

                            if (badge.contains('.pdf')) {
                              isPdf = true;
                            }

                            return GestureDetector(
                              onTap: () async {
                                if (isDocs && isPdf) {
                                  await showPdf(
                                    context: context,
                                    pdfData: badgesVm.pdfData[badgeName],
                                    path:
                                        badge.contains('assets') ? badge : null,
                                    downloadPdf: badgesVm.isDownloading
                                        ? () {}
                                        : () async {
                                            saveFile(
                                                url: badge,
                                                filename:
                                                    '$badgeName-${badge.split('/').last}',
                                                isDocs: isDocs,
                                                badgesVm: badgesVm,
                                                isFromAsset:
                                                    badge.contains('assets'));
                                          },
                                    popRoute:
                                        badgesVm.isDownloading ? () {} : null,
                                  );
                                } else {
                                  await showDialog(
                                    useSafeArea: false,
                                    context: context,
                                    builder: (context) => BadgeView(
                                      badge: badge,
                                      isImage: !isDocs,
                                      badgeName: badgeName,
                                      download: () async {
                                        saveFile(
                                            url: badge,
                                            filename: !isDocs
                                                ? badgeName
                                                : '$badgeName-${badge.split('/').last}',
                                            isDocs: isDocs,
                                            badgesVm: badgesVm,
                                            isFromAsset:
                                                badge.contains('assets'));
                                      },
                                    ),
                                  );
                                }
                              },
                              child: BadgeCard(
                                badge: badge,
                                badgeName: badgeName,
                                isPdf: isDocs
                                    ? isPdf
                                        ? true
                                        : null
                                    : false,
                              ),
                            );
                          },
                        ),
                      ],
                    );
                  })
                ],
              );
            },
          )),
    );
  }

  Future<void> saveFile({
    required String url,
    required String filename,
    bool isDocs = false,
    bool isFromAsset = false,
    required BadgesViewModel badgesVm,
  }) async {
    await badgesVm
        .saveFiles(
            url: url,
            filename: filename,
            isDocs: isDocs,
            isFromAsset: isFromAsset)
        .then((value) {
      if (!mounted) return;
      if ((value ?? false)) {
        context.showSnackBar(
          content: Text(
            context
                .tr(isDocs ? 'doc_download_success' : 'badge_download_success'),
            textAlign: TextAlign.center,
            style: context.pttBodySmall.copyWith(
              color: Colors.white,
              fontWeight: FontWeight.w700,
              fontSize: 14,
            ),
          ),
          isFloating: true,
          margin: const EdgeInsets.fromLTRB(80, 10, 80, 100),
        );
      } else {
        context.showErrorSnackBar(
          content: Text(
            context
                .tr(isDocs ? 'doc_download_failed' : 'badge_download_failed'),
            textAlign: TextAlign.center,
            style: context.pttBodySmall.copyWith(
              color: Colors.white,
              fontWeight: FontWeight.w700,
              fontSize: 14,
            ),
          ),
          isFloating: true,
          margin: const EdgeInsets.fromLTRB(80, 10, 80, 100),
        );
      }
    });
  }
}
