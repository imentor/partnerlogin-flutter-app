import 'dart:io';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/mitid.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app_links/app_links_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/contract/contract_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

Future<void> viewContractDialog({
  required BuildContext context,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                tr('contract'),
                style: Theme.of(context).textTheme.displayLarge!.copyWith(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: PartnerAppColors.darkBlue,
                    ),
              ),
              SmartGaps.gapH20,
              Expanded(
                child: Container(
                    decoration: BoxDecoration(
                      color: PartnerAppColors.spanishGrey.withValues(alpha: .2),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    padding: const EdgeInsets.all(10),
                    child: SfPdfViewer.file(
                        File(context.read<ContractViewModel>().contractPdf!))),
              )
            ],
          ),
        ),
      );
    },
  );
}

class CancelContractPage extends StatefulWidget {
  const CancelContractPage({super.key});

  @override
  State<CancelContractPage> createState() => _CancelContractPageState();
}

class _CancelContractPageState extends State<CancelContractPage> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final contractVm = context.read<ContractViewModel>();
      final appLinkVm = context.read<AppLinksViewModel>();
      final contractId = int.parse(appLinkVm.contractId);

      modalManager.showLoadingModal();

      await contractVm
          .getCancelContractDetails(contractId: contractId)
          .then((value) {
        modalManager.hideLoadingModal();
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      bottomNavigationBar: Container(
          padding:
              const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
          color: Colors.white,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              GestureDetector(
                onTap: () {
                  viewContractDialog(context: context);
                },
                child: Text(
                  tr('view_contract'),
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      color: PartnerAppColors.blue,
                      fontWeight: FontWeight.normal),
                ),
              ),
              SmartGaps.gapH20,
              TextButton(
                  style: TextButton.styleFrom(
                      fixedSize: Size(MediaQuery.of(context).size.width, 50),
                      backgroundColor: PartnerAppColors.mitidBlue,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      )),
                  onPressed: () async {
                    final contractVm = context.read<ContractViewModel>();
                    final appLinksVm = context.read<AppLinksViewModel>();

                    final navigator = Navigator.of(context);

                    await mitidCallback().then((value) async {
                      if (!mounted) return;

                      modalManager.showLoadingModal();
                      final contractId = int.parse(appLinksVm.contractId);

                      await tryCatchWrapper(
                        context:
                            myGlobals.innerScreenScaffoldKey!.currentContext,
                        function: contractVm
                            .cancelContract(
                                contractId: contractId, forms: value)
                            .then((cancelSuccess) {
                          if (!mounted) return;

                          modalManager.hideLoadingModal();

                          if (cancelSuccess) {
                            showOkAlertDialog(
                                    context: navigator.context,
                                    title: tr('cancellation_of_contract'))
                                .then((value) {
                              backDrawerRoute();
                            });
                          }
                        }),
                      );
                    });
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        tr('sign_here'),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(color: Colors.white),
                      ),
                      SmartGaps.gapW5,
                      SvgPicture.asset(
                        'assets/images/mitid.svg',
                        height: 15,
                      )
                    ],
                  ))
            ],
          )),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: Consumer<ContractViewModel>(
          builder: (_, vm, __) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  tr('cancellation_of_contract'),
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                SmartGaps.gapH20,
                Html(
                  data: vm.cancelContractTemplate,
                  shrinkWrap: true,
                  style: {
                    "body": Style(
                      fontSize: FontSize(16.0),
                      fontWeight: FontWeight.normal,
                    ),
                  },
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  Future<Map<String, dynamic>> mitidResponse() async {
    return await mitidCallback();
  }
}
