import 'package:Haandvaerker.dk/model/messages/message_v2_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/cache_image.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/upload_offer/create_upload_offer/upload_offer_page.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/messages/message_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:provider/provider.dart';

Future<void> viewNotificationDialog({
  required BuildContext context,
  required MessageV2Items selectedMessage,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: ViewNotification(
              selectedMessage: selectedMessage,
            ))),
      );
    },
  );
}

class ViewNotification extends StatefulWidget {
  const ViewNotification({super.key, required this.selectedMessage});

  final MessageV2Items selectedMessage;

  @override
  State<ViewNotification> createState() => _ViewNotificationState();
}

class _ViewNotificationState extends State<ViewNotification> {
  List<MessageV2Items> messageThreads = [];
  bool isLoading = false;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final messageVm = context.read<MessageV2ViewModel>();
      setState(() {
        isLoading = true;
      });
      if (widget.selectedMessage.threadId != null) {
        await messageVm
            .getNotificationThread(
          threadId: widget.selectedMessage.threadId!,
        )
            .then((value) {
          setState(() {
            isLoading = false;
            messageThreads = [...value];
          });
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final clientsVm = context.read<ClientsViewModel>();
    final createOfferVm = context.read<CreateOfferViewModel>();
    final offerVm = context.read<OfferViewModel>();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.selectedMessage.title ?? '',
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              letterSpacing: 1.0,
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18),
        ),
        SmartGaps.gapH30,
        if (isLoading)
          const Center(
            child: CircularProgressIndicator(),
          )
        else if (messageThreads.isNotEmpty) ...[
          ...messageThreads.map((e) {
            return Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(child: fromTitle(message: e)),
                    SmartGaps.gapW10,
                    Text(
                      Formatter.formatDateStrings(
                          type: DateFormatType.dateTimeWithTwelveHour,
                          dateString: e.sendDate),
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                            color:
                                PartnerAppColors.black.withValues(alpha: 0.7),
                            fontSize: 14.0,
                            height: 1,
                          ),
                    )
                  ],
                ),
                SmartGaps.gapH10,
                HtmlWidget(
                  e.message ?? '',
                  textStyle: Theme.of(context)
                      .textTheme
                      .headlineMedium!
                      .copyWith(
                          color: PartnerAppColors.darkBlue,
                          fontWeight: FontWeight.normal,
                          fontSize: 14),
                ),
                // Text(
                //   e.message ?? '',
                //   style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                //       color: PartnerAppColors.darkBlue,
                //       fontWeight: FontWeight.normal,
                //       fontSize: 14),
                // ),
                SmartGaps.gapH10,
                if ((e.actions?.items ?? []).isNotEmpty) ...[
                  SmartGaps.gapH20,
                  ...?e.actions?.items?.map((action) {
                    return CustomDesignTheme.flatButtonStyle(
                      height: 50,
                      backgroundColor: PartnerAppColors.malachite,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                      onPressed: () {
                        actionButton(
                            action: action,
                            message: e,
                            clientsVm: clientsVm,
                            createOfferVm: createOfferVm,
                            offerVm: offerVm);
                      },
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              tr('${action.action}'),
                              style: Theme.of(context)
                                  .textTheme
                                  .displayLarge!
                                  .copyWith(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.white,
                                  ),
                            )
                          ]),
                    );
                  })
                ]
              ],
            );
          })
        ]
      ],
    );
  }

  void onEditJobDescription(
      {required MessageV2ActionItem action,
      required MessageV2Items message,
      ClientsViewModel? clientsVm,
      CreateOfferViewModel? createOfferVm,
      OfferViewModel? offerVm}) async {
    modalManager.showLoadingModal();
    if (message.projectId != null &&
        clientsVm != null &&
        createOfferVm != null &&
        offerVm != null) {
      await clientsVm
          .getPartnerJobByProjectId(projectId: (message.projectId ?? 0))
          .then((value) {
        if (!mounted) return;

        clientsVm.getYourCustomerData = false;
        modalManager.hideLoadingModal();
        if (value != null && value.isNotEmpty) {
          if (value.first.offerId != null) {
            chooseOfferDialog(
                context: context,
                createOffer: () {
                  createOfferVm.toUpdateOffer = false;
                  createOfferVm.isNewOfferVersion = true;

                  Navigator.of(context).pop();
                  changeDrawerRoute(
                    Routes.createOffer,
                    arguments: {
                      'job': value.first,
                    },
                  );
                },
                uploadOffer: () {
                  offerVm.isUploadOfferUpdate = false;
                  offerVm.isNewOfferVersion = true;
                  Navigator.pop(context);
                  changeDrawerRoute(
                    Routes.uploadOffer,
                    arguments: value.first,
                  );
                });
          } else {
            chooseOfferDialog(
                context: context,
                createOffer: () {
                  createOfferVm.toUpdateOffer = false;
                  createOfferVm.isNewOfferVersion = false;

                  Navigator.of(context).pop();
                  changeDrawerRoute(
                    Routes.createOffer,
                    arguments: {
                      'job': value.first,
                    },
                  );
                },
                uploadOffer: () {
                  offerVm.isUploadOfferUpdate = false;
                  offerVm.isNewOfferVersion = false;
                  Navigator.pop(context);
                  changeDrawerRoute(
                    Routes.uploadOffer,
                    arguments: value.first,
                  );
                });
          }
        } else {
          modalManager.hideLoadingModal();
        }
      });
    } else {
      modalManager.hideLoadingModal();
    }
  }

  actionButton(
      {required MessageV2ActionItem action,
      required MessageV2Items message,
      ClientsViewModel? clientsVm,
      CreateOfferViewModel? createOfferVm,
      OfferViewModel? offerVm}) async {
    // final messageVm = context.read<MessageV2ViewModel>();

    switch (action.action) {
      case 'view_deficiency':
        Navigator.of(context).pop();

        if (message.projectId != null) {
          changeDrawerRoute(
            Routes.projectDashboard,
            arguments: {
              'projectId': message.projectId!,
              'pageRedirect': 'deficiency'
            },
          );
        }

        // await messageVm
        //     .postMessageAction(messageActionUrl: action.data)
        //     .then((value) async {
        //   if (message.projectId != null) {
        //     modalManager.hideLoadingModal();
        //     changeDrawerRoute(
        //       Routes.projectDashboard,
        //       arguments: {
        //         'projectId': message.projectId!,
        //         'pageRedirect': 'deficiency'
        //       },
        //     );
        //   } else {
        //     modalManager.hideLoadingModal();
        //   }
        // });

        break;
      case 'edit_contract':
        Navigator.of(context).pop();

        if (message.projectId != null) {
          changeDrawerRoute(
            Routes.projectDashboard,
            arguments: {
              'projectId': message.projectId!,
              'pageRedirect': 'contract'
            },
          );
        }

        // await messageVm
        //     .postMessageAction(messageActionUrl: action.data)
        //     .then((value) async {
        //   if (message.projectId != null) {
        //     modalManager.hideLoadingModal();
        //     changeDrawerRoute(
        //       Routes.projectDashboard,
        //       arguments: {
        //         'projectId': message.projectId!,
        //         'pageRedirect': 'contract'
        //       },
        //     );
        //   } else {
        //     modalManager.hideLoadingModal();
        //   }
        // });
        break;
      case 'edit_job_description':
        onEditJobDescription(
            action: action,
            message: message,
            clientsVm: clientsVm,
            createOfferVm: createOfferVm,
            offerVm: offerVm);
        break;
      case 'rate_partner':
      case 'contract_document_url':
        Navigator.of(context).pop();
        changeDrawerRoute(Routes.inAppWebView,
            arguments: Uri.parse(action.data!));

        // await messageVm
        //     .postMessageAction(messageActionUrl: action.data)
        //     .then((value) async {
        //   modalManager.hideLoadingModal();
        //   changeDrawerRoute(Routes.inAppWebView,
        //       arguments: Uri.parse(action.data!));
        // });
        break;
      default:
        Navigator.of(context).pop();

        // await messageVm
        //     .postMessageAction(messageActionUrl: action.data)
        //     .then((value) async {
        //   modalManager.hideLoadingModal();
        // });

        break;
    }
  }

  Widget fromTitle({required MessageV2Items message}) {
    String title = '';
    String imageUrl = '';

    if ((message.isFromUser ?? false)) {
      title = message.contact?.name ?? '';
      imageUrl = message.contact?.avatar ?? '';
    } else if ((message.isFromPartner ?? false)) {
      title = message.partner?.name ?? '';
      imageUrl = message.partner?.avatar ?? '';
    }

    return Row(
      children: [
        CircleAvatar(
          radius: 24.0,
          child: ClipOval(
            child: CacheImage(
              imageUrl: imageUrl,
            ),
          ),
        ),
        SmartGaps.gapW10,
        Text(
          title,
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.bold,
              fontSize: 16),
        ),
      ],
    );
  }
}
