import 'package:Haandvaerker.dk/model/messages/message_v2_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/message/view_message_detail.dart';
import 'package:Haandvaerker.dk/ui/screens/notification/view_notification.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/messages/message_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:provider/provider.dart';
import 'package:timeago_flutter/timeago_flutter.dart' as timeago;

class NotificationPage extends StatefulWidget {
  const NotificationPage({super.key});

  @override
  State<NotificationPage> createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  final PagingController<int, MessageV2Items> _pagingController =
      PagingController(firstPageKey: 1);

  @override
  void initState() {
    timeago.setLocaleMessages('da', timeago.DaMessages());
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final appDrawerVm = context.read<AppDrawerViewModel>();
      final newsFeedVm = context.read<NewsFeedViewModel>();
      const dynamicStoryPage = 'notifications';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      if (dynamicStoryEnable) {
        await Future.value(newsFeedVm.getDynamicStory(page: dynamicStoryPage))
            .whenComplete(() {
          if (!mounted) return;

          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      }
    });
  }

  void onTapMarkRead(
      {required MessageV2Items item,
      required MessageV2ViewModel messageVm}) async {
    if (item.seen == 0) {
      modalManager.showLoadingModal();

      await messageVm
          .markAsSeenNotification(
              currentItems: _pagingController.itemList ?? [],
              messageId: item.id!)
          .then((value) {
        if (!mounted) return;

        modalManager.hideLoadingModal();

        _pagingController.itemList = value;

        viewNotificationDialog(context: context, selectedMessage: item);
      });
    } else {
      viewNotificationDialog(context: context, selectedMessage: item);
    }
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final messageVm = context.read<MessageV2ViewModel>();

      await messageVm.getNotification(page: pageKey).then((value) {
        final isLastPage = messageVm.notificationPagination.currentPage ==
            messageVm.notificationPagination.lastPage;

        if (isLastPage) {
          _pagingController
              .appendLastPage(messageVm.notificationPagination.items ?? []);
        } else {
          final nextPageKey = pageKey + 1;
          _pagingController.appendPage(
              messageVm.notificationPagination.items ?? [], nextPageKey);
        }
      });
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Consumer<MessageV2ViewModel>(builder: (_, vm, __) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    tr('notifications'),
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        letterSpacing: 1.0,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                  InkWell(
                    onTap: () async {
                      modalManager.showLoadingModal();
                      await vm
                          .markAllAsSeen(
                              currentItems: _pagingController.itemList ??
                                  <MessageV2Items>[])
                          .then((value) {
                        _pagingController.itemList = value;
                        vm.getNotification(page: 1);
                        modalManager.hideLoadingModal();
                      });
                    },
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            tr('mark_all_read'),
                            style: context.pttBodySmall.copyWith(
                              fontWeight: FontWeight.w600,
                              color: PartnerAppColors.accentBlue,
                              letterSpacing: 0.5,
                            ),
                          )
                        ]),
                  )
                ],
              ),
              SmartGaps.gapH20,
              Expanded(
                child: Scrollbar(
                  child: PagedListView<int, MessageV2Items>.separated(
                    pagingController: _pagingController,
                    shrinkWrap: true,
                    builderDelegate: PagedChildBuilderDelegate(
                      transitionDuration: const Duration(milliseconds: 1800),
                      itemBuilder: (context, item, index) {
                        final id = item.id ?? 0;
                        final sendDate = item.sendDate ?? '';
                        return InkWell(
                          onTap: () => onTapMarkRead(item: item, messageVm: vm),
                          child: Container(
                            padding: const EdgeInsets.all(10),
                            color: item.seen == 0
                                ? PartnerAppColors.blue.withValues(alpha: .2)
                                : null,
                            child: Row(children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      (item.title ?? '').replaceAll('\n', ''),
                                      style: Theme.of(context)
                                          .textTheme
                                          .headlineMedium!
                                          .copyWith(
                                              color: PartnerAppColors.darkBlue,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16),
                                    ),
                                    SmartGaps.gapH10,
                                    HtmlWidget(
                                      item.message ?? '',
                                      textStyle: Theme.of(context)
                                          .textTheme
                                          .headlineMedium!
                                          .copyWith(
                                              color: PartnerAppColors.darkBlue,
                                              fontWeight: FontWeight.normal,
                                              fontSize: 14),
                                    ),
                                    // Text(
                                    //   (item.message ?? '').replaceAll('\n', ''),
                                    //   style: Theme.of(context)
                                    //       .textTheme
                                    //       .headlineMedium!
                                    //       .copyWith(
                                    //           color: PartnerAppColors.darkBlue,
                                    //           fontWeight: FontWeight.normal,
                                    //           fontSize: 14),
                                    // )
                                  ],
                                ),
                              ),
                              Column(
                                children: [
                                  GestureDetector(
                                      onTap: () async {
                                        await deleteMessageDialog(
                                                context: context)
                                            .then((value) async {
                                          if ((value ?? false)) {
                                            modalManager.showLoadingModal();

                                            await vm
                                                .deleteNotification(
                                                    currentItems:
                                                        _pagingController
                                                                .itemList ??
                                                            [],
                                                    messageId: id)
                                                .then((value) {
                                              modalManager.hideLoadingModal();
                                              _pagingController.itemList =
                                                  value;
                                            });
                                          }
                                        });
                                      },
                                      child: const Icon(FeatherIcons.trash2,
                                          color: PartnerAppColors.grey1)),
                                  SmartGaps.gapH5,
                                  timeago.Timeago(
                                    date: DateTime.parse(sendDate),
                                    locale: context.locale.languageCode == 'da'
                                        ? 'da'
                                        : 'en',
                                    builder: (context, value) => Text(
                                      value,
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium!
                                          .copyWith(
                                            color: PartnerAppColors.grey1,
                                            fontWeight: FontWeight.normal,
                                            fontSize: 12.0,
                                          ),
                                    ),
                                  ),
                                ],
                              )
                            ]),
                          ),
                        );
                      },
                    ),
                    separatorBuilder: (context, index) {
                      return const Divider();
                    },
                  ),
                ),
              ),
            ],
          );
        }),
      ),
    );
  }
}
