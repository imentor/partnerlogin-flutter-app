part of '../reserve_dialog.dart';

class ReservingDialog extends StatelessWidget {
  const ReservingDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      backgroundColor: Colors.white,
      titlePadding: EdgeInsets.zero,
      insetPadding: const EdgeInsets.symmetric(horizontal: 20),
      title: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 300,
        child: const Center(
          child: ConnectivityLoader(),
        ),
      ),
    );
  }
}
