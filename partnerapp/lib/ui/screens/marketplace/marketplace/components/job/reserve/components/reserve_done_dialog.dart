part of '../reserve_dialog.dart';

class ReserveDoneDialog extends StatelessWidget {
  const ReserveDoneDialog({super.key});

  @override
  Widget build(BuildContext context) {
    //
    return Consumer<MarketPlaceV2ViewModel>(builder: (_, marketplaceVm, __) {
      bool isSuccess =
          marketplaceVm.isReserveSuccess == RequestResponse.success;

      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        backgroundColor: Colors.white,
        titlePadding: EdgeInsets.zero,
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),

        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              if (isSuccess) {
                Navigator.pop(context);
                if (marketplaceVm.reserved['success']) {
                  final createOfferVm = context.read<CreateOfferViewModel>();
                  final vm = context.read<ClientsViewModel>();
                  if (!marketplaceVm.isVideoDone) {
                    createOfferVm.isNewReserve = true;
                  }
                  vm.fromNewsFeed = false;
                  backDrawerRoute();
                  changeDrawerReplaceRoute(Routes.yourClient);
                  marketplaceVm.setMarketPlaceToDefault();
                }
              } else {
                Navigator.pop(context);
                marketplaceVm.setMarketPlaceToDefault();
              }
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                    color: isSuccess
                        ? PartnerAppColors.malachite
                        : PartnerAppColors.red,
                    shape: BoxShape.circle,
                  ),
                  padding: const EdgeInsets.all(5),
                  child: Center(
                    child: Icon(
                      isSuccess ? Icons.check : Icons.close,
                      color: Colors.white,
                      size: 35,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                if (isSuccess) ...[
                  Text(
                    tr('task_is_reserved'),
                    style: Theme.of(context)
                        .textTheme
                        .titleMedium!
                        .copyWith(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    tr('marketplace_reserved_message'),
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .titleSmall!
                        .copyWith(color: PartnerAppColors.spanishGrey),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextButton(
                    style: TextButton.styleFrom(
                        fixedSize: Size(MediaQuery.of(context).size.width, 45),
                        backgroundColor: PartnerAppColors.darkBlue),
                    child: Text(
                      tr('see_customer_information'),
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(color: Colors.white),
                    ),
                    onPressed: () {
                      final marketplace =
                          context.read<MarketPlaceV2ViewModel>();
                      Navigator.pop(context);
                      if (marketplace.reserved['success']) {
                        final createOfferVm =
                            context.read<CreateOfferViewModel>();
                        final vm = context.read<ClientsViewModel>();
                        if (!marketplace.isVideoDone) {
                          createOfferVm.isNewReserve = true;
                        }
                        vm.fromNewsFeed = false;
                        backDrawerRoute();
                        changeDrawerReplaceRoute(Routes.yourClient);
                        marketplace.setMarketPlaceToDefault();
                      }
                    },
                  )
                ] else
                  Text(
                    marketplaceVm.reserveFailedMessage,
                    style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                          fontSize: 26,
                          fontWeight: FontWeight.w700,
                        ),
                  ),
              ],
            ),
          ),
        ),
        // insetPadding: const EdgeInsets.symmetric(horizontal: 20),
        // title: Container(
        //   padding: const EdgeInsets.all(20),
        //   width: MediaQuery.of(context).size.width,
        //   height: 300,
        //   child: Column(
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     children: [
        //       Container(
        //         height: 50,
        //         width: 50,
        //         decoration: const BoxDecoration(
        //           color: PartnerAppColors.malachite,
        //           shape: BoxShape.circle,
        //         ),
        //         padding: const EdgeInsets.all(5),
        //         child: const Center(
        //           child: Icon(
        //             Icons.check,
        //             color: PartnerAppColors.malachite,
        //           ),
        //         ),
        //       )
        //       // Row(
        //       //   children: [
        //       //     const Spacer(),
        //       //     IconButton(
        //       //       onPressed: () async {
        //       //         final marketplace = context.read<MarketPlaceV2ViewModel>();
        //       //         Navigator.pop(context);
        //       //         if (marketplace.reserved['success']) {
        //       //           final createOfferVm =
        //       //               context.read<CreateOfferViewModel>();
        //       //           final vm = context.read<ClientsViewModel>();
        //       //           if (!marketplace.isVideoDone) {
        //       //             createOfferVm.isNewReserve = true;
        //       //           }
        //       //           vm.fromNewsFeed = false;
        //       //           backDrawerRoute();
        //       //           changeDrawerReplaceRoute(Routes.yourClient);
        //       //           marketplace.setMarketPlaceToDefault();
        //       //         }
        //       //       },
        //       //       icon: Stack(
        //       //         alignment: Alignment.center,
        //       //         children: [
        //       //           Container(
        //       //             width: 23,
        //       //             decoration: BoxDecoration(
        //       //               border: Border.all(
        //       //                 color: Colors.black.withValues(alpha:0.4),
        //       //                 width: 2,
        //       //               ),
        //       //               shape: BoxShape.circle,
        //       //             ),
        //       //           ),
        //       //           Icon(
        //       //             Icons.close,
        //       //             size: 18,
        //       //             color: Colors.black.withValues(alpha:0.4),
        //       //           ),
        //       //         ],
        //       //       ),
        //       //     ),
        //       //   ],
        //       // ),
        //       // Expanded(
        //       //   child: Center(
        //       //     child: Column(
        //       //       mainAxisAlignment: MainAxisAlignment.start,
        //       //       crossAxisAlignment: CrossAxisAlignment.center,
        //       //       children: [
        //       //         SmartGaps.gapH60,
        //       //         Text(
        //       //           tr('reserve_success'),
        //       //           style:
        //       //               Theme.of(context).textTheme.headlineLarge!.copyWith(
        //       //                     fontSize: 26,
        //       //                     fontWeight: FontWeight.w700,
        //       //                   ),
        //       //         ),
        //       //         SmartGaps.gapH20,
        //       //         // if (marketplaceVm.isReserveSuccess ==
        //       //         //     RequestResponse.failed)
        //       //         //   Icon(
        //       //         //     Icons.close,
        //       //         //     color: Theme.of(context).colorScheme.error,
        //       //         //     size: 100,
        //       //         //   )
        //       //         // else
        //       //         Icon(
        //       //           Icons.check_circle_outline,
        //       //           color: Theme.of(context).colorScheme.secondary,
        //       //           size: 100,
        //       //         )
        //       //       ],
        //       //     ),
        //       //   ),
        //       // ),
        //     ],
        //   ),
        // ),
      );
    });

    //
  }
}
