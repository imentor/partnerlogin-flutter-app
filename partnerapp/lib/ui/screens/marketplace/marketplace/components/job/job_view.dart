import 'package:Haandvaerker.dk/model/marketplace/marketplace_v2_response_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/cache_image.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer/components/kanikke_limit_popup.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/invitations/accept_decline_invitation.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/job/job_descriptions_template.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/job/job_info.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/job/reserve/semi_blacklisted_pop_up_reserve.dart';
import 'package:Haandvaerker.dk/utils/extension/colors.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/location/location_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/job_invitations_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/market_place_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/packages/packages_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/kanikke_settings_response_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_background_geolocation/flutter_background_geolocation.dart'
    as bg;
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class ViewJobItem extends StatefulWidget {
  const ViewJobItem({
    super.key,
    required this.jobItem,
    required this.fromMarketPlace,
  });

  final MarketPlaceV2Items? jobItem;
  final bool? fromMarketPlace;

  @override
  State<ViewJobItem> createState() => _ViewJobItemState();
}

class _ViewJobItemState extends State<ViewJobItem>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;
  List<String> importantKeys = [];

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      //

      final marketplaceVm = context.read<MarketPlaceV2ViewModel>();
      final packageVm = context.read<PackagesViewModel>();
      final kanIkkeVm = context.read<KanIkkeSettingsResponseViewModel>();

      if ((widget.jobItem?.startDate ?? '').isNotEmpty) {
        marketplaceVm.formatStartDate(widget.jobItem?.startDate ?? '');
      } else {
        marketplaceVm.startDate = '';
      }

      if ((widget.jobItem?.endDate ?? '').isNotEmpty) {
        marketplaceVm.formatEndDate(widget.jobItem?.endDate ?? '');
      } else {
        marketplaceVm.endDate = '';
      }

      int? id = (widget.jobItem?.parentId ?? 0) == 0
          ? (widget.jobItem?.id ?? 0)
          : (widget.jobItem?.parentId ?? 0);

      if (widget.jobItem != null) {
        await Future.wait([
          marketplaceVm.getListPriceAi(
              parentProjectId: id,
              item: widget.jobItem!,
              jobIndustryId: widget.jobItem?.jobIndustry ?? 0),
          packageVm.getPackages(),
          kanIkkeVm.getKanIkkeSettings(),
        ]);
      }

      for (final info in (widget.jobItem?.personalInformation ??
          <Map<dynamic, dynamic>>[])) {
        importantKeys.addAll(info.keys as Iterable<String>);
      }

      //
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Theme(
        data: Theme.of(context).copyWith(
          colorScheme: context.colorScheme.copyWith(
            secondary: PartnerAppColors.accentBlue.withValues(alpha: 0.3),
          ),
        ),
        child: Column(
          children: [
            //

            TabBar(
              controller: _tabController,
              labelStyle: context.pttTitleSmall.copyWith(color: Colors.black),
              unselectedLabelStyle:
                  context.pttTitleSmall.copyWith(color: PartnerAppColors.grey1),
              indicatorColor: PartnerAppColors.accentBlue,
              tabs: [
                Tab(
                  child: Text(
                    tr('see_details'),
                  ),
                ),
                Tab(
                  child: Text(
                    tr('job_descriptions'),
                  ),
                ),
              ],
            ),

            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: [
                  SeeDetailsTab(
                    jobItem: widget.jobItem,
                    importantKeys: importantKeys,
                    fromMarketPlace: widget.fromMarketPlace,
                  ),
                  JobDescriptionsTab(
                    contractType: widget.jobItem?.contractType ?? '',
                    jobIndustry: widget.jobItem?.jobIndustry ?? 0,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SeeDetailsTab extends StatefulWidget {
  const SeeDetailsTab({
    super.key,
    required this.jobItem,
    required this.importantKeys,
    required this.fromMarketPlace,
  });
  final MarketPlaceV2Items? jobItem;
  final List<String> importantKeys;
  final bool? fromMarketPlace;

  @override
  SeeDetailsTabState createState() => SeeDetailsTabState();
}

class SeeDetailsTabState extends State<SeeDetailsTab> {
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final marketplaceVm = context.read<MarketPlaceV2ViewModel>();

      // Offer Due Date - sets deadline offer values
      String deadlineOffer = '';
      String deadlineOfferColorCode = '';
      MarketPlaceV2Items jobItem = MarketPlaceV2Items();

      if (widget.jobItem != null) {
        jobItem = widget.jobItem!;
        deadlineOffer = jobItem.deadlineOffer ?? '';
        deadlineOfferColorCode = jobItem.deadlineOfferColorCode ?? '';
        if (deadlineOffer.isNotEmpty) {
          marketplaceVm.formatDate(deadlineOffer);
          deadlineOffer = marketplaceVm.deadlineOffer;
        }
        if (deadlineOfferColorCode.isNotEmpty) {
          marketplaceVm.formatDeadlineOfferColorCode(deadlineOfferColorCode);
        }
      }
      //
    });
  }

  void onPressReserve(
      {required MarketPlaceV2ViewModel marketplaceVm,
      required MarketPlaceV2Items jobItem}) async {
    final locationVm = context.read<LocationViewModel>();

    final appDrawerVm = context.read<AppDrawerViewModel>();
    final packagesVm = context.read<PackagesViewModel>();
    final usersVm = context.read<UserViewModel>();
    final kanIkkeVm = context.read<KanIkkeSettingsResponseViewModel>();

    final isGrowthPlusPackage =
        appDrawerVm.bitrixCompanyModel?.isVaekstPlus ?? false;

    final hasAccountIntegration =
        (appDrawerVm.bitrixCompanyModel?.accountIntegration ?? false);

    modalManager.showLoadingModal();

    // Semi-Blacklisted - Checks if partner is semi-blacklisted or blacklisted. If either is true, it will show a dialog.
    if (((packagesVm.packagesResponse?.data?.semiBlacklist ?? 0) == 1) ||
        (appDrawerVm.bitrixCompanyModel?.blackListed ?? 0) == 1) {
      modalManager.hideLoadingModal();
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return PopScope(
              canPop: true,
              child: SemiBlacklistedPopUpReserve(
                  appDrawerVm.bitrixCompanyModel?.companyName ?? ''),
            );
          });
    } else {
      if ((appDrawerVm.bitrixCompanyModel?.companyTracking ?? 0) == 1) {
        await locationVm.checkPermission();

        if (!mounted) return;

        final isLocationDenied =
            locationVm.locationPermission == LocationPermission.denied;
        final isLocationDeniedForever =
            locationVm.locationPermission == LocationPermission.deniedForever;
        final isLocationUnableToDetermine = locationVm.locationPermission ==
            LocationPermission.unableToDetermine;
        final isLocationWhileInUse =
            locationVm.locationPermission == LocationPermission.whileInUse;

        if ((!hasAccountIntegration &&
                (isLocationDenied ||
                    isLocationDeniedForever ||
                    isLocationUnableToDetermine ||
                    isLocationWhileInUse)) &&
            isGrowthPlusPackage) {
          modalManager.hideLoadingModal();

          locationVm.updateLocationInfo(gpsLocation: 0);

          disabledDialog(
                  context: context,
                  message: tr('no_gps_access_marketplace_reserve'),
                  companyName: usersVm.userModel?.user?.name ?? '')
              .whenComplete(() async {
            modalManager.showLoadingModal();
            await locationVm.checkPermission();

            if (locationVm.locationPermission == LocationPermission.always) {
              bg.BackgroundGeolocation.stop();
              locationVm.startLocation();

              modalManager.hideLoadingModal();
              locationVm.updateLocationInfo(gpsLocation: 1);
            } else if (locationVm.locationPermission ==
                    LocationPermission.deniedForever ||
                locationVm.locationPermission == LocationPermission.denied ||
                locationVm.locationPermission ==
                    LocationPermission.whileInUse) {
              modalManager.hideLoadingModal();

              locationVm.updateLocationInfo(gpsLocation: 0);
            }
          });
          modalManager.hideLoadingModal();
        } else {
          locationVm.updateLocationInfo(gpsLocation: 1);

          if ((appDrawerVm.bitrixCompanyModel?.kanIkkeTilbud ?? 0) == 1) {
            if (kanIkkeVm.isBlackListed) {
              if (kanIkkeVm.featuresLimitation.isNotEmpty &&
                  kanIkkeVm.featuresLimitation.contains('marketplace')) {
                modalManager.hideLoadingModal();
                kanikkeLimitPopupDialog(
                    context: context, message: kanIkkeVm.popupText);
              } else {
                modalManager.hideLoadingModal();
                marketplaceVm.initReservationDialogs(
                    context: context, jobItem: jobItem);
              }
            } else if (kanIkkeVm.isWarningForBlackList) {
              if (kanIkkeVm.featuresLimitation.isNotEmpty &&
                  kanIkkeVm.featuresLimitation.contains('marketplace')) {
                modalManager.hideLoadingModal();
                kanikkeLimitPopupDialog(
                        context: context, message: kanIkkeVm.warningPopupText)
                    .whenComplete(() {
                  if (!mounted) return;

                  marketplaceVm.initReservationDialogs(
                      context: context, jobItem: jobItem);
                });
              } else {
                modalManager.hideLoadingModal();
                marketplaceVm.initReservationDialogs(
                    context: context, jobItem: jobItem);
              }
            } else {
              modalManager.hideLoadingModal();
              marketplaceVm.initReservationDialogs(
                  context: context, jobItem: jobItem);
            }
          } else {
            modalManager.hideLoadingModal();
            marketplaceVm.initReservationDialogs(
                context: context, jobItem: jobItem);
          }
        }
      } else {
        if ((appDrawerVm.bitrixCompanyModel?.kanIkkeTilbud ?? 0) == 1) {
          if (kanIkkeVm.isBlackListed) {
            if (kanIkkeVm.featuresLimitation.isNotEmpty &&
                kanIkkeVm.featuresLimitation.contains('marketplace')) {
              modalManager.hideLoadingModal();
              kanikkeLimitPopupDialog(
                  context: context, message: kanIkkeVm.popupText);
            } else {
              modalManager.hideLoadingModal();
              marketplaceVm.initReservationDialogs(
                  context: context, jobItem: jobItem);
            }
          } else if (kanIkkeVm.isWarningForBlackList) {
            if (kanIkkeVm.featuresLimitation.isNotEmpty &&
                kanIkkeVm.featuresLimitation.contains('marketplace')) {
              modalManager.hideLoadingModal();
              kanikkeLimitPopupDialog(
                      context: context, message: kanIkkeVm.warningPopupText)
                  .whenComplete(() {
                if (!mounted) return;

                marketplaceVm.initReservationDialogs(
                    context: context, jobItem: jobItem);
              });
            } else {
              modalManager.hideLoadingModal();
              marketplaceVm.initReservationDialogs(
                  context: context, jobItem: jobItem);
            }
          } else {
            modalManager.hideLoadingModal();
            marketplaceVm.initReservationDialogs(
                context: context, jobItem: jobItem);
          }
        } else {
          modalManager.hideLoadingModal();
          marketplaceVm.initReservationDialogs(
              context: context, jobItem: jobItem);
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    MarketPlaceV2Items jobItem = MarketPlaceV2Items();
    if (widget.jobItem != null) {
      jobItem = widget.jobItem!;
    }
    return Consumer<MarketPlaceV2ViewModel>(
      builder: (context, marketplaceVm, _) {
        ///

        final wizardTexts = context.watch<TenderFolderViewModel>().wizardTexts;

        String enterprise = '';
        if (jobItem.contractType == 'Hovedentreprise') {
          enterprise = 'Hovedentreprise';
        } else {
          if (widget.fromMarketPlace ?? false) {
            enterprise = context.locale.languageCode == 'da'
                ? '${jobItem.industryInfoNew?.industry?.first.name ?? ''} Enterprise'
                : '${jobItem.industryInfoNew?.industry?.first.nameEn ?? ''} Enterprise';
          } else {
            enterprise = wizardTexts.any((element) =>
                    (element.producttypeid ?? '') ==
                    jobItem.enterpriseIndustry?.toString())
                ? context.locale.languageCode == 'da'
                    ? '${wizardTexts.firstWhere((element) => (element.producttypeid ?? '') == jobItem.enterpriseIndustry?.toString(), orElse: () => AllWizardText.defaults()).producttypeDa} Enterprise'
                    : '${wizardTexts.firstWhere((element) => (element.producttypeid ?? '') == jobItem.enterpriseIndustry?.toString(), orElse: () => AllWizardText.defaults()).producttypeEn} Enterprise'
                : '';
          }
        }

        return Skeletonizer(
          enabled: marketplaceVm.isGettingListPrice ||
              context.watch<PackagesViewModel>().busy,
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //

                    SmartGaps.gapH20,

                    SizedBox(
                      height: 200,
                      width: MediaQuery.of(context).size.width,
                      child: Stack(
                        children: [
                          SizedBox(
                            height: 200,
                            width: double.infinity,
                            child: CacheImage(
                              imageUrl: jobItem.address?.image ?? '',
                            ),
                          ),
                          Positioned(
                            bottom: 0,
                            child: Skeleton.shade(
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                height: 30,
                                decoration: BoxDecoration(
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onTertiaryContainer
                                      .withValues(alpha: 0.5),
                                ),
                                padding: const EdgeInsets.only(left: 10),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      '${jobItem.address?.city ?? ''}, ${jobItem.address?.zip ?? ''}',
                                      style: context.pttBodyLarge.copyWith(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w400,
                                        height: 1,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    SmartGaps.gapH20,

                    Text(
                      enterprise,
                      style: context.pttTitleSmall.copyWith(
                        fontWeight: FontWeight.w400,
                        color: PartnerAppColors.accentBlue,
                      ),
                    ),

                    SmartGaps.gapH10,

                    Text(
                      jobItem.name ?? '',
                      style: context.pttTitleSmall.copyWith(fontSize: 14),
                    ),

                    SmartGaps.gapH20,

                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        ///

                        // Offer Due Date - displays the deadline offer
                        if (context
                                .read<AppDrawerViewModel>()
                                .supplierProfileModel
                                ?.bCrmCompany
                                ?.deadlineOffer ==
                            1)
                          if (widget.jobItem != null &&
                              (jobItem.deadlineOffer ?? '').isNotEmpty)
                            Row(
                              children: [
                                const Icon(
                                  Icons.calendar_today_outlined,
                                  color: PartnerAppColors.grey1,
                                ),
                                SmartGaps.gapW15,
                                Text(
                                  '${tr('offer_due_date')} ${marketplaceVm.deadlineOffer}',
                                  style: context.pttBodySmall.copyWith(
                                      fontSize: 14,
                                      color:
                                          marketplaceVm.deadlineOfferColorCode),
                                ),
                              ],
                            ),

                        const Spacer(),

                        GestureDetector(
                          onTap: () async {
                            await showDialog(
                              context: context,
                              builder: (context) => JobInfo(
                                maximumOffer: jobItem.maximumOffer,
                                importantInfo: jobItem.personalInformation,
                                importantKeys: widget.importantKeys,
                                startDate: marketplaceVm.startDate,
                                endDate: marketplaceVm.endDate,
                              ),
                            );
                          },
                          child: const Icon(
                            Icons.info_outlined,
                            size: 30,
                            color: PartnerAppColors.grey1,
                          ),
                        ),
                      ],
                    ),

                    SmartGaps.gapH20,

                    ConstrainedBox(
                      constraints: const BoxConstraints(
                        minHeight: 170,
                      ),
                      child: Text(
                        jobItem.description ?? '',
                        style: context.pttBodySmall.copyWith(
                          fontSize: 12,
                          color: Colors.black,
                          height: 1.83,
                        ),
                      ),
                    ),

                    SmartGaps.gapH20,

                    if (widget.fromMarketPlace ?? false)
                      Skeleton.shade(
                        child: CustomDesignTheme.flatButtonStyle(
                          onPressed: (jobItem.totalOffers ?? 0) <
                                  (jobItem.maximumOffer ?? 3)
                              ? () => onPressReserve(
                                  marketplaceVm: marketplaceVm,
                                  jobItem: jobItem)
                              : null,
                          backgroundColor: (jobItem.totalOffers ?? 0) <
                                  (jobItem.maximumOffer ?? 3)
                              ? PartnerAppColors.green
                              : PartnerAppColors.grey1,
                          height: 50,
                          width: MediaQuery.of(context).size.width,
                          child: Center(
                            child: Text(
                              tr('reserve'),
                              style: context.pttBodyLarge.copyWith(
                                fontSize: 14,
                                color: Colors.white,
                                height: 1,
                              ),
                            ),
                          ),
                        ),
                      )
                    else if ((context
                                    .watch<JobInvitationsViewModel>()
                                    .currentInvitationStatus ??
                                '')
                            .toLowerCase() ==
                        'pending')
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ///

                          Expanded(
                            flex: 2,
                            child: Skeleton.shade(
                              child: CustomDesignTheme.flatButtonStyle(
                                onPressed: () async {
                                  marketplaceVm.setReserveValuesToDefault();
                                  await showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) {
                                      return const AcceptDeclineInvitation(
                                        isAccept: false,
                                      );
                                    },
                                  );
                                },
                                height: 50,
                                backgroundColor: PartnerAppColors.red,
                                child: Center(
                                  child: Text(
                                    tr('decline'),
                                    style: context.pttBodyLarge.copyWith(
                                      fontSize: 14,
                                      color: Colors.white,
                                      height: 1,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          SmartGaps.gapW10,

                          Expanded(
                            flex: 2,
                            child: Skeleton.shade(
                              child: CustomDesignTheme.flatButtonStyle(
                                onPressed: () async {
                                  marketplaceVm.setReserveValuesToDefault();
                                  context
                                      .read<JobInvitationsViewModel>()
                                      .isResponding = RequestResponse.idle;
                                  await showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) {
                                      return const AcceptDeclineInvitation(
                                        isAccept: true,
                                      );
                                    },
                                  );
                                },
                                height: 50,
                                backgroundColor: PartnerAppColors.green,
                                child: Center(
                                  child: Text(
                                    tr('accept'),
                                    style: context.pttBodyLarge.copyWith(
                                      fontSize: 14,
                                      color: Colors.white,
                                      height: 1,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),

                    SmartGaps.gapH20,
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    if (widget.jobItem != null &&
        widget.jobItem?.address != null &&
        widget.jobItem?.address?.image != null) {
      CachedNetworkImageProvider(widget.jobItem!.address!.image!).evict();
    }
  }
}

class JobDescriptionsTab extends StatelessWidget {
  const JobDescriptionsTab({
    super.key,
    required this.contractType,
    required this.jobIndustry,
  });

  final String? contractType;
  final int? jobIndustry;

  @override
  Widget build(BuildContext context) {
    return Consumer<MarketPlaceV2ViewModel>(
      builder: (context, marketplaceVm, child) {
        if ((!marketplaceVm.isGettingListPrice ||
                !context.watch<PackagesViewModel>().busy) &&
            marketplaceVm.listPriceAi.isEmpty) {
          return const Center(
            child: EmptyListIndicator(route: Routes.viewJobItem),
          );
        }

        return Skeletonizer(
          enabled: marketplaceVm.isGettingListPrice ||
              context.watch<PackagesViewModel>().busy,
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ///

                  SmartGaps.gapH20,

                  if (marketplaceVm.listPriceAi.isNotEmpty)
                    ...marketplaceVm.listPriceAi.map(
                      (e) {
                        final enterpriseIndex = marketplaceVm.listPriceAi
                                .indexWhere((element) =>
                                    element.enterpriseId == e.enterpriseId) +
                            1;

                        return JobDescriptionTemplate(
                          enterpriseIndustry: e,
                          enterpriseIndex: enterpriseIndex,
                        );
                      },
                    ),
                  if (contractType == 'Fagentreprise' &&
                      marketplaceVm.subListPriceAi.isNotEmpty)
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 20, bottom: 10, top: 20),
                          child: Text(
                            tr('other_enterprise_desc'),
                          ),
                        ),
                        ...marketplaceVm.subListPriceAi.map(
                          (e) {
                            final enterpriseIndex = marketplaceVm.subListPriceAi
                                    .indexWhere((element) =>
                                        element.enterpriseId ==
                                        e.enterpriseId) +
                                1;

                            return JobDescriptionTemplate(
                              enterpriseIndustry: e,
                              enterpriseIndex: enterpriseIndex,
                            );
                          },
                        ),
                      ],
                    ),
                  SmartGaps.gapH20,
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
