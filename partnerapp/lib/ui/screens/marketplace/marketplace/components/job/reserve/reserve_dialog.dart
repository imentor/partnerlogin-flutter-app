library reserve_dialog;

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/components/create_offer_video_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/payment_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/job/reserve/close_pop_up_reserve.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/job/reserve/components/video_offer_extra_action.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/job/reserve/maxed_out_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/job/reserve/reserve_more_dialog.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/launcher.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/market_place_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/packages/packages_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';

part 'components/reserve_done_dialog.dart';
part 'components/reserve_extra_dialog.dart';
part 'components/reserve_idle_dialog.dart';
part 'components/reserving_dialog.dart';

class ReserveDialog extends StatefulWidget {
  const ReserveDialog({super.key});

  @override
  State<ReserveDialog> createState() => _ReserveDialogState();
}

class _ReserveDialogState extends State<ReserveDialog> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) async => context
          .read<MarketPlaceV2ViewModel>()
          .updateReservationDialogs(context: context),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final marketplaceVideoDone =
        context.watch<PackagesViewModel>().playMarketPlaceVideo;
    return Consumer<MarketPlaceV2ViewModel>(
      builder: (context, marketplaceVm, child) {
        if (marketplaceVm.isReserving) {
          return const ReservingDialog();
        } else {
          switch (marketplaceVm.isReserveSuccess) {
            case RequestResponse.idle:
              return const ReserveIdleDialog();
            case RequestResponse.extra:
              return const ReserveExtraDialog();
            case RequestResponse.pre:
              if (marketplaceVm.cancelDialog) {
                return const ClosePopUpReserve();
              } else {
                if (marketplaceVideoDone) {
                  return const ReserveMoreDialog();
                } else {
                  if (marketplaceVm.toVideoDialog) {
                    return const CreateOfferVideoDialog(
                      extraAction: VideoOfferExtraAction(),
                    );
                  } else {
                    return const MaxedOutDialog();
                  }
                }
              }
            default:
              return const ReserveDoneDialog();
          }
        }
      },
    );
  }
}
