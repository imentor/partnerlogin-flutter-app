import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/market_place_v2_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class VideoOfferExtraAction extends StatelessWidget {
  const VideoOfferExtraAction({super.key});

  @override
  Widget build(BuildContext context) {
    return CustomDesignTheme.flatButtonStyle(
      onPressed: context.watch<MarketPlaceV2ViewModel>().isVideoDone
          ? () async {
              final marketplaceVm = context.read<MarketPlaceV2ViewModel>();
              if (marketplaceVm.currentJobItem != null &&
                  marketplaceVm.currentJobItem?.id != null) {
                marketplaceVm.normalReserve(context: context, fromVideo: true);
              }
            }
          : null,
      backgroundColor: context.watch<MarketPlaceV2ViewModel>().isVideoDone
          ? PartnerAppColors.green
          : const Color(0xffCECECE),
      child: SizedBox(
        height: 50,
        child: Center(
          child: Text(
            tr('reserve_now'),
            style: context.pttBodyLarge
                .copyWith(color: Colors.white, fontSize: 14, height: 1),
          ),
        ),
      ),
    );
  }
}
