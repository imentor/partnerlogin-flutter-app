import 'package:Haandvaerker.dk/model/marketplace/marketplace_v2_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/pagination/custom_pagination.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/job/job_item_card.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/job/marketplace_sort.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/marketplace_header.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/onboarding_dialog.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/app_links/app_links_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/market_place_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/packages/packages_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/kanikke_settings_response_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:visibility_detector/visibility_detector.dart';

class MarketplaceJobSceen extends StatefulWidget {
  const MarketplaceJobSceen({super.key, required this.isFromAppLink});

  final bool isFromAppLink;

  @override
  MarketplaceJobSceenState createState() => MarketplaceJobSceenState();
}

class MarketplaceJobSceenState extends State<MarketplaceJobSceen> {
  late ScrollController scroll;

  bool showSeeMore = true;

  @override
  void initState() {
    scroll = ScrollController();
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      initMarketplace();
    });

    scroll.addListener(_handleControllerNotification);
  }

  void _handleControllerNotification() {
    if (scroll.offset >= scroll.position.maxScrollExtent &&
        !scroll.position.outOfRange) {
      setState(() {
        showSeeMore = false;
      });
    } else {
      setState(() {
        showSeeMore = true;
      });
    }
  }

  Future<void> initMarketplace() async {
    final packageVm = context.read<PackagesViewModel>();
    final appLinksVm = context.read<AppLinksViewModel>();
    final kanikkeVm = context.read<KanIkkeSettingsResponseViewModel>();
    final marketplaceVm = context.read<MarketPlaceV2ViewModel>();
    final appDrawerVm = context.read<AppDrawerViewModel>();

    marketplaceVm.currentPage = 1;

    if (packageVm.packagesResponse == null) {
      marketplaceVm.busy = true;
      await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function: Future.value(packageVm.getPackages()),
      );
    }

    if (marketplaceVm.initialMarketplaceLoad || !marketplaceVm.fromNewsFeed) {
      marketplaceVm.busy = true;

      await tryCatchWrapper(
          context: myGlobals.homeScaffoldKey!.currentContext,
          function: Future.wait([
            kanikkeVm.getKanIkkeSettings(),
            marketplaceVm.getSupplierInfoIndustry(
                isInit: true,
                enterprise: appDrawerVm.bitrixCompanyModel?.enterprise),
          ]).whenComplete(() async {
            if (!mounted) return;

            marketplaceVm.busy = false;

            if (marketplaceVm.marketPlaceFilterId > 0) {
              hasFilterDialog(context: context);
            }

            final onboardCheck = packageVm.packagesResponse?.data?.onboarding;

            if (onboardCheck?['url'] != null && onboardCheck?['step'] != null) {
              if (onboardCheck!['url'].toString().isNotEmpty &&
                  onboardCheck['step'].toString().isNotEmpty) {
                if (packageVm.packagesResponse!.data!.packageHandle ==
                        'Trialjobsfree' &&
                    int.parse(onboardCheck['step'].toString()) < 7) {
                  if (mounted) {
                    await showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (context) {
                        return OnboardingDialog(
                          url: onboardCheck['url'],
                        );
                      },
                    );
                  }
                }
              }
            }

            if (widget.isFromAppLink && appLinksVm.projectId.isNotEmpty) {
              marketplaceVm.setBusy(true);

              await marketplaceVm
                  .getMarketplaceById(
                      projectId: int.parse(appLinksVm.projectId))
                  .then((value) async {
                marketplaceVm.setBusy(false);
                if (value != null && value.isNotEmpty) {
                  changeDrawerRoute(
                    Routes.viewJobItem,
                    arguments: {
                      'jobItem': value.first,
                      'fromMarketPlace': true,
                    },
                  );

                  await marketplaceVm.jobsView(projectId: value.first.id!);

                  appLinksVm.resetLoginAppLink();
                }
              });
            }
          }));
    } else {
      if (marketplaceVm.marketPlaceFilterId > 0) {
        if (!mounted) return;
        hasFilterDialog(context: context);
      }

      final onboardCheck = packageVm.packagesResponse?.data?.onboarding;

      if (onboardCheck?['url'] != null && onboardCheck?['step'] != null) {
        if (onboardCheck!['url'].toString().isNotEmpty &&
            onboardCheck['step'].toString().isNotEmpty) {
          if (packageVm.packagesResponse!.data!.packageHandle ==
                  'Trialjobsfree' &&
              int.parse(onboardCheck['step'].toString()) < 7) {
            if (mounted) {
              await showDialog(
                barrierDismissible: false,
                context: context,
                builder: (context) {
                  return OnboardingDialog(
                    url: onboardCheck['url'],
                  );
                },
              );
            }
          }
        }
      }

      if (widget.isFromAppLink && appLinksVm.projectId.isNotEmpty) {
        marketplaceVm.setBusy(true);

        await marketplaceVm
            .getMarketplaceById(projectId: int.parse(appLinksVm.projectId))
            .then((value) async {
          marketplaceVm.setBusy(false);
          if (value != null && value.isNotEmpty) {
            changeDrawerRoute(
              Routes.viewJobItem,
              arguments: {
                'jobItem': value.first,
                'fromMarketPlace': true,
              },
            );

            await marketplaceVm.jobsView(projectId: value.first.id!);

            appLinksVm.resetLoginAppLink();
          }
        });
      }
    }
  }

  @override
  void dispose() {
    scroll.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: RefreshIndicator(
        onRefresh: () async {
          //

          final marketplaceVm = context.read<MarketPlaceV2ViewModel>();
          final kanikkeVm = context.read<KanIkkeSettingsResponseViewModel>();

          marketplaceVm.setBusy(true);

          await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: Future.wait([
              marketplaceVm.getSupplierInfoIndustry(),
              kanikkeVm.getKanIkkeSettings()
            ]),
          );

          marketplaceVm.setBusy(false);
        },
        child: Stack(
          children: [
            ListView(
              physics: context.watch<MarketPlaceV2ViewModel>().busy
                  ? const NeverScrollableScrollPhysics()
                  : const ClampingScrollPhysics(),
              key: const Key(Keys.marketplaceScrollView),
              controller: scroll,
              children: [
                //
                SmartGaps.gapH10,
                const MarketplaceHeader(),
                JobScreenBody(scroll: scroll),

                //
              ],
            ),
            // if (showSeeMore)
            //   Align(
            //       alignment: Alignment.bottomCenter,
            //       child: Container(
            //         height: 50,
            //         width: 50,
            //         margin: const EdgeInsets.only(bottom: 10),
            //         decoration: const BoxDecoration(
            //           shape: BoxShape.circle,
            //           color: PartnerAppColors.blue,
            //         ),
            //         child: const Center(
            //           child: Icon(
            //             Icons.arrow_downward,
            //             size: 30,
            //             color: Colors.white,
            //           ),
            //         ),
            //       )),
          ],
        ),
      ),
    );
  }
}

class JobScreenBody extends StatelessWidget {
  const JobScreenBody({super.key, required this.scroll});

  final ScrollController scroll;

  void _onItemVisibilityChanged(
      {required int index,
      required double visibleFraction,
      required MarketPlaceV2ViewModel marketplaceVm}) {
    if (visibleFraction > 0.5) {
      marketplaceVm.addReadMarketplaceJobs(index);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<MarketPlaceV2ViewModel>(
      builder: (context, marketplaceVm, _) {
        //

        if (!marketplaceVm.busy && marketplaceVm.marketJobs.isEmpty) {
          if (marketplaceVm.marketPlaceFilterId > 0) {
            return Column(
              children: [
                SmartGaps.gapH30,
                Text(
                  tr('no_jobs_in_filter'),
                  textAlign: TextAlign.center,
                  style: Theme.of(context)
                      .textTheme
                      .titleMedium!
                      .copyWith(height: 1.6, fontWeight: FontWeight.normal),
                ),
                SmartGaps.gapH20,
                CustomDesignTheme.flatButtonStyle(
                  height: 50,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                      side: const BorderSide(color: PartnerAppColors.darkBlue)),
                  onPressed: () async {
                    await tryCatchWrapper(
                        context: myGlobals.homeScaffoldKey!.currentContext,
                        function: marketplaceVm.removeMarketplaceFilter());
                  },
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          tr('clear_filter'),
                          style: Theme.of(context)
                              .textTheme
                              .displayLarge!
                              .copyWith(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                                color: PartnerAppColors.darkBlue,
                              ),
                        )
                      ]),
                )
              ],
            );
          } else {
            return const SizedBox(
              height: 250,
              child: Center(
                child: EmptyListIndicator(route: Routes.marketPlaceScreen),
              ),
            );
          }
        }

        return Skeletonizer(
          enabled: marketplaceVm.busy,
          child: Column(
            children: [
              GridView.count(
                crossAxisCount: 2,
                crossAxisSpacing: 13,
                childAspectRatio: 0.65,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                children: marketplaceVm.busy
                    ? [
                        ...List.generate(
                            10,
                            (index) => JobItemCard(
                                  jobItem: MarketPlaceV2Items(
                                      address: MarketPlaceV2Address(
                                        zip: '7700',
                                        image:
                                            "https://pictures.haandvaerker.dk/photos/address/18152/address-18152-teglvaerksvej-26-biersted-9440-aabybro.png",
                                      ),
                                      description:
                                          "TEST TEST JAY FAGEN CONTRACT",
                                      name: "TEST TEST JAY FAGEN CONTRACT",
                                      matching: 100,
                                      contractType: 'Hovedentreprise'),
                                  fromMarketPlace: true,
                                ))
                      ]
                    : [
                        ...marketplaceVm.marketJobs.map((e) {
                          final index = marketplaceVm.marketJobs.indexOf(e);
                          return VisibilityDetector(
                            key: Key(index.toString()),
                            onVisibilityChanged: (info) {
                              _onItemVisibilityChanged(
                                  index: index,
                                  visibleFraction: info.visibleFraction,
                                  marketplaceVm: marketplaceVm);
                            },
                            child: JobItemCard(
                              jobItem: e,
                              fromMarketPlace: true,
                            ),
                          );
                        })
                      ],
                // itemCount: marketplaceVm.marketJobs.length,
                // itemBuilder: (context, index) {
                //   return ;
                // },
              ),

              Opacity(
                opacity: !marketplaceVm.busy ? 1 : 0,
                child: Padding(
                  padding: const EdgeInsets.only(top: 30, bottom: 10),
                  child: CustomNumberPaginator(
                    numberPages: marketplaceVm.lastPage!,
                    height: 50,
                    initialPage: marketplaceVm.currentPage - 1,
                    buttonSelectedBackgroundColor: PartnerAppColors.accentBlue,
                    buttonUnselectedForegroundColor: Colors.black,
                    onPageChange: (page) async {
                      //

                      if (marketplaceVm.fromNewsFeed) {
                        marketplaceVm.fromNewsFeed = false;
                      }

                      scroll.animateTo(0,
                          duration: const Duration(milliseconds: 700),
                          curve: Curves.easeOut);

                      marketplaceVm.currentPage = page + 1;
                      await tryCatchWrapper(
                        context: myGlobals.homeScaffoldKey!.currentContext,
                        function: marketplaceVm.nextPageMarketplace(),
                      );

                      //
                    },
                  ),
                ),
              ),

              //
            ],
          ),
        );
      },
    );
  }
}
