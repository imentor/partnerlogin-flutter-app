import 'package:Haandvaerker.dk/model/marketplace/marketplace_v2_response_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/ui/components/cache_image.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/job_invitations_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/market_place_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class JobItemCard extends StatefulWidget {
  const JobItemCard({
    super.key,
    required this.jobItem,
    this.fromMarketPlace = false,
    this.invitationId = 0,
    this.status = '',
  });

  final MarketPlaceV2Items? jobItem;
  final bool fromMarketPlace;
  final int? invitationId;
  final String? status;

  @override
  JobItemCardState createState() => JobItemCardState();
}

class JobItemCardState extends State<JobItemCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 4,
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //

          // HEADER
          Container(
            decoration: const BoxDecoration(
              color: Color(0xff42CBC8),
            ),
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    //

                    Row(
                      children: [
                        Text(
                          widget.jobItem?.address?.zip ?? '',
                          style:
                              Theme.of(context).textTheme.bodyMedium!.copyWith(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.white,
                                  ),
                        ),
                        Text(
                          ' ${widget.jobItem?.address?.city}',
                          style:
                              Theme.of(context).textTheme.bodyMedium!.copyWith(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.white,
                                  ),
                        ),
                      ],
                    ),

                    const Stack(
                      children: [
                        Icon(
                          Icons.circle,
                          color: Colors.white,
                          size: 24,
                        ),
                        Icon(
                          Icons.star_border_rounded,
                          color: Color(0xff42CBC8),
                          size: 24,
                        ),
                      ],
                    ),

                    //
                  ],
                ),

                if (widget.jobItem!.matching != null)
                  Text(
                    '${widget.jobItem!.matching.toString()}% Match',
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          fontSize: 10,
                          fontWeight: FontWeight.w500,
                          color: Colors.black,
                        ),
                  ),

                //
              ],
            ),
          ),

          Expanded(
            child: GestureDetector(
              onTap: () async {
                //

                if (!widget.fromMarketPlace) {
                  context.read<JobInvitationsViewModel>().currentInvitationId =
                      widget.invitationId;
                  context
                      .read<JobInvitationsViewModel>()
                      .currentInvitationStatus = widget.status;
                }
                changeDrawerRoute(
                  Routes.viewJobItem,
                  arguments: {
                    'jobItem': widget.jobItem,
                    'fromMarketPlace': widget.fromMarketPlace,
                  },
                );

                await context
                    .read<MarketPlaceV2ViewModel>()
                    .jobsView(projectId: widget.jobItem!.id!);

                //
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // IMAGE

                  Skeleton.shade(
                    child: Container(
                      height: 60,
                      width: double.infinity,
                      decoration: const BoxDecoration(color: Colors.grey),
                      child: CacheImage(
                        imageUrl: widget.jobItem?.address?.image,
                      ),
                    ),
                  ),

                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        //

                        Builder(builder: (context) {
                          final industries = context
                              .watch<TenderFolderViewModel>()
                              .wizardTexts;
                          String industry = '';

                          if (widget.jobItem!.contractType ==
                              'Hovedentreprise') {
                            industry = 'Hovedentreprise';
                          } else {
                            if (widget.fromMarketPlace) {
                              industry = context.locale.languageCode == 'da'
                                  ? '${widget.jobItem!.industryInfoNew!.industry!.first.name} Enterprise'
                                  : '${widget.jobItem!.industryInfoNew!.industry!.first.nameEn} Enterprise';
                            } else {
                              industry = industries.any((element) =>
                                      element.producttypeid! ==
                                      widget.jobItem!.enterpriseIndustry
                                          ?.toString())
                                  ? context.locale.languageCode == 'da'
                                      ? '${industries.firstWhere(
                                            (element) =>
                                                element.producttypeid! ==
                                                widget
                                                    .jobItem!.enterpriseIndustry
                                                    ?.toString(),
                                            orElse: () =>
                                                AllWizardText.defaults(),
                                          ).producttypeDa} Enterprise'
                                      : '${industries.firstWhere(
                                            (element) =>
                                                element.producttypeid! ==
                                                widget
                                                    .jobItem!.enterpriseIndustry
                                                    ?.toString(),
                                            orElse: () =>
                                                AllWizardText.defaults(),
                                          ).producttypeEn} Enterprise'
                                  : '';
                            }
                          }

                          return Padding(
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: Text(
                              industry,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .copyWith(
                                    fontSize: 16,
                                    height: 1.1,
                                    fontWeight: FontWeight.w700,
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onTertiaryContainer,
                                  ),
                            ),
                          );
                        }),

                        //TITLE
                        Padding(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 5),
                          child: Text(
                            widget.jobItem!.name?.split('-').first ?? '',
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(
                                  fontSize: 16,
                                  height: 1.1,
                                  fontWeight: FontWeight.w700,
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onTertiaryContainer,
                                ),
                          ),
                        ),

                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: Text(
                              widget.jobItem?.description ?? '',
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .copyWith(
                                    fontSize: 14,
                                    height: 1.1,
                                    fontWeight: FontWeight.w500,
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onTertiaryContainer,
                                  ),
                            ),
                          ),
                        ),

                        Align(
                          alignment: Alignment.center,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 10),
                            child: Text(
                              tr('see_details'),
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .copyWith(
                                    fontSize: 16,
                                    height: 1.1,
                                    fontWeight: FontWeight.w500,
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                            ),
                          ),
                        ),

                        //
                      ],
                    ),
                  ),

                  //
                ],
              ),
            ),
          ),

          //
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    if (widget.jobItem != null &&
        widget.jobItem?.address != null &&
        widget.jobItem?.address?.image != null) {
      CachedNetworkImageProvider(widget.jobItem!.address!.image!).evict();
    }
  }
}
