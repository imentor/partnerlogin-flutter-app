part of '../reserve_dialog.dart';

class ReserveExtraDialog extends StatelessWidget {
  const ReserveExtraDialog({super.key});

  @override
  Widget build(BuildContext context) {
    //

    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      backgroundColor: Colors.white,
      titlePadding: EdgeInsets.zero,
      insetPadding: const EdgeInsets.fromLTRB(20, 0, 20, 40),
      title: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            //

            Row(
              children: [
                const Spacer(),
                IconButton(
                  onPressed: () async {
                    Navigator.pop(context);
                  },
                  icon: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        width: 23,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.black.withValues(alpha: 0.4),
                            width: 2,
                          ),
                          shape: BoxShape.circle,
                        ),
                      ),
                      Icon(
                        Icons.close,
                        size: 18,
                        color: Colors.black.withValues(alpha: 0.4),
                      ),
                    ],
                  ),
                ),
              ],
            ),

            Consumer<UserViewModel>(
              builder: (_, userVM, __) {
                return Text(
                  "${tr('hey')}  ${userVM.userModel?.user?.name ?? ''}",
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        fontSize: 22,
                        fontWeight: FontWeight.w700,
                      ),
                );
              },
            ),

            //
          ],
        ),
      ),
      content: const CanReserveMore(),
    );
  }
}

class CanReserveMore extends StatelessWidget {
  const CanReserveMore({super.key});

  @override
  Widget build(BuildContext context) {
    final marketplaceVm = context.watch<MarketPlaceV2ViewModel>();
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Text(
              tr('used_up_reservation'),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    height: 1,
                  ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Builder(builder: (context) {
              final first = tr('increase_reserve_option').split(';').first;
              final last = tr('increase_reserve_option').split(';').last;
              final price = context
                  .watch<MarketPlaceV2ViewModel>()
                  .checkReserveResponse!
                  .price
                  .toString();
              final text = '$first $price $last';
              return Text(
                text,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      height: 1,
                    ),
              );
            }),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Text(
              tr('increase_reserve_option_1'),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    height: 1,
                  ),
            ),
          ),
          SmartGaps.gapH30,
          CustomDesignTheme.flatButtonStyle(
            height: 50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            backgroundColor:
                marketplaceVm.checkReserveResponse?.success ?? false
                    ? PartnerAppColors.green
                    : PartnerAppColors.grey,
            onPressed: marketplaceVm.checkReserveResponse?.success ?? false
                ? () async {
                    final marketplaceVm =
                        context.read<MarketPlaceV2ViewModel>();
                    if (marketplaceVm.checkReserveResponse!.success!) {
                      marketplaceVm.isReserveSuccess = RequestResponse.idle;
                      marketplaceVm.toReserveMore = true;
                    } else {
                      marketplaceVm.canReserveMore = false;
                    }
                  }
                : null,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  tr('yes_please'),
                  style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                      ),
                ),
                SmartGaps.gapW5,
                const Icon(
                  Icons.arrow_forward,
                  color: Colors.white,
                  size: 24,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
