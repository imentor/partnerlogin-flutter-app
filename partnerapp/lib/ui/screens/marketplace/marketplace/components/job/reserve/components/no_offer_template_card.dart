import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class NoOfferTemplateCard extends StatelessWidget {
  const NoOfferTemplateCard({super.key, required this.job});

  final PartnerJobModel job;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
        side: BorderSide(
          color: PartnerAppColors.grey.withValues(alpha: 0.7),
        ),
      ),
      margin: const EdgeInsets.only(bottom: 10),
      child: Container(
        height: 60,
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ///

            Text(job.customerName ?? '',
                style: context.pttBodyLarge.copyWith(height: 0.8)),

            GestureDetector(
              onTap: () {
                context.read<CreateOfferViewModel>().toUpdateOffer = false;

                if (context.mounted) {
                  Navigator.pop(context);
                  changeDrawerRoute(
                    Routes.createOffer,
                    arguments: {
                      'job': job,
                    },
                  );
                }
              },
              child: Text(
                tr('make_offer'),
                style: context.pttBodyLarge
                    .copyWith(color: PartnerAppColors.accentBlue, height: 0.8),
              ),
            )

            ///
          ],
        ),
      ),
    );
  }
}
