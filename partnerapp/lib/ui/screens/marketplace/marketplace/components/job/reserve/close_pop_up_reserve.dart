import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/market_place_v2_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class ClosePopUpReserve extends StatelessWidget {
  const ClosePopUpReserve({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      backgroundColor: Colors.white,
      titlePadding: EdgeInsets.zero,
      contentPadding: const EdgeInsets.fromLTRB(16, 60, 16, 30),
      insetPadding: const EdgeInsets.fromLTRB(20, 190, 20, 330),
      content: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ///

              SvgPicture.asset('assets/images/warning.svg'),

              SmartGaps.gapH20,

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 62),
                child: Text(
                  tr('close_pop_up_reserve_desc'),
                  textAlign: TextAlign.center,
                  style: context.pttBodyLarge.copyWith(color: Colors.black),
                ),
              ),

              SmartGaps.gapH65,

              Row(
                children: [
                  ///

                  Expanded(
                    child: CustomDesignTheme.flatButtonStyle(
                      onPressed: () => context
                          .read<MarketPlaceV2ViewModel>()
                          .cancelDialog = false,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                        side:
                            const BorderSide(color: PartnerAppColors.darkBlue),
                      ),
                      child: SizedBox(
                        height: 50,
                        child: Center(
                          child: Text(
                            tr('back'),
                            style: context.pttBodyLarge
                                .copyWith(fontSize: 14, height: 1),
                          ),
                        ),
                      ),
                    ),
                  ),

                  SmartGaps.gapW10,

                  Expanded(
                    child: CustomDesignTheme.flatButtonStyle(
                      onPressed: () => Navigator.pop(context),
                      backgroundColor: PartnerAppColors.red,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: SizedBox(
                        height: 50,
                        child: Center(
                          child: Text(
                            tr('ok').toUpperCase(),
                            style: context.pttBodyLarge.copyWith(
                                color: Colors.white, fontSize: 14, height: 1),
                          ),
                        ),
                      ),
                    ),
                  ),

                  ///
                ],
              ),

              ///
            ],
          ),
        ),
      ),
    );
  }
}
