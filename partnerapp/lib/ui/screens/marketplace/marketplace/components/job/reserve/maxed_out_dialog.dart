import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/market_place_v2_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class MaxedOutDialog extends StatelessWidget {
  const MaxedOutDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        backgroundColor: Colors.white,
        titlePadding: EdgeInsets.zero,
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 16, vertical: 35),
        insetPadding: const EdgeInsets.fromLTRB(20, 190, 20, 330),
        title: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Align(
            alignment: Alignment.bottomRight,
            child: IconButton(
              onPressed: () =>
                  context.read<MarketPlaceV2ViewModel>().cancelDialog = true,
              icon: Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    width: 23,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black.withValues(alpha: 0.4),
                        width: 2,
                      ),
                      shape: BoxShape.circle,
                    ),
                  ),
                  Icon(
                    Icons.close,
                    size: 18,
                    color: Colors.black.withValues(alpha: 0.4),
                  ),
                ],
              ),
            ),
          ),
        ),
        content: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ///

              SvgPicture.asset('assets/images/warning.svg'),

              SmartGaps.gapH20,

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4),
                child: Text(
                  tr('maxed_out_two_step_desc'),
                  textAlign: TextAlign.center,
                  style: context.pttBodyLarge.copyWith(color: Colors.black),
                ),
              ),

              SmartGaps.gapH50,

              CustomDesignTheme.flatButtonStyle(
                onPressed: () =>
                    context.read<MarketPlaceV2ViewModel>().toVideoDialog = true,
                backgroundColor: PartnerAppColors.darkBlue,
                child: SizedBox(
                  height: 50,
                  child: Center(
                    child: Text(
                      tr('next'),
                      style: context.pttBodyLarge.copyWith(
                          color: Colors.white, fontSize: 14, height: 1),
                    ),
                  ),
                ),
              )

              ///
            ],
          ),
        ));
  }
}
