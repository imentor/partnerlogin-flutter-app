part of '../reserve_dialog.dart';

// class ReserveIdleDialog extends StatelessWidget {
//   const ReserveIdleDialog({super.key});

//   }

class ReserveIdleDialog extends StatefulWidget {
  const ReserveIdleDialog({super.key});

  @override
  State<ReserveIdleDialog> createState() => _ReserveIdleDialogState();
}

class _ReserveIdleDialogState extends State<ReserveIdleDialog> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final marketplaceVm = context.read<MarketPlaceV2ViewModel>();

      marketplaceVm.agreeAcceptOfferDeadline = false;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<MarketPlaceV2ViewModel>(
      builder: (context, marketplaceVm, _) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
          backgroundColor: Colors.white,
          titlePadding: EdgeInsets.zero,
          insetPadding: const EdgeInsets.symmetric(horizontal: 20),
          title: Container(
            padding: const EdgeInsets.all(20),
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                //

                Row(
                  children: [
                    const Spacer(),
                    IconButton(
                      onPressed: () => Navigator.pop(context),
                      icon: Stack(
                        alignment: Alignment.center,
                        children: [
                          Container(
                            width: 23,
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black.withValues(alpha: 0.4),
                                width: 2,
                              ),
                              shape: BoxShape.circle,
                            ),
                          ),
                          Icon(
                            Icons.close,
                            size: 18,
                            color: Colors.black.withValues(alpha: 0.4),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),

                Icon(Icons.info, color: Colors.yellow[700], size: 70),

                SmartGaps.gapH10,

                Text(
                  marketplaceVm.toReserveMore
                      ? tr('buy_reservation_confirmation')
                      : tr('marketplace_reservation_confirmation_deadline'),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                        fontSize: 24,
                        fontWeight: FontWeight.w700,
                      ),
                ),
                SmartGaps.gapH20,

                // Offer Due Date - displays offer due date
                if (context
                        .read<AppDrawerViewModel>()
                        .supplierProfileModel
                        ?.bCrmCompany
                        ?.deadlineOffer ==
                    1)
                  if (!marketplaceVm.toReserveMore)
                    if (marketplaceVm.deadlineOffer.isNotEmpty)
                      Text(
                        '${tr('offer_due_date')} ${marketplaceVm.deadlineOffer}',
                        style: Theme.of(context)
                            .textTheme
                            .headlineLarge!
                            .copyWith(
                                fontSize: 15,
                                fontWeight: FontWeight.w700,
                                color: marketplaceVm.deadlineOfferColorCode),
                      ),

                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                    children: [
                      Transform.scale(
                        scale: 1.5,
                        child: Checkbox(
                            activeColor: PartnerAppColors.darkBlue,
                            value: marketplaceVm.agreeAcceptOfferDeadline,
                            onChanged: (value) {
                              marketplaceVm.agreeAcceptOfferDeadline =
                                  value ?? false;
                            }),
                      ),
                      Flexible(
                        child: Text(
                          tr('marketplace_checkbox_reservation_confirmation_deadline'),
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      )
                    ],
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: CustomDesignTheme.flatButtonStyle(
                          height: 50,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                            side: BorderSide(
                                color: Theme.of(context)
                                    .colorScheme
                                    .onTertiaryContainer),
                          ),
                          onPressed: () => Navigator.pop(context),
                          child: Text(
                            tr('no'),
                            style: Theme.of(context)
                                .textTheme
                                .headlineLarge!
                                .copyWith(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onTertiaryContainer,
                                ),
                          ),
                        ),
                      ),
                      SmartGaps.gapW20,
                      Expanded(
                        child: CustomDesignTheme.flatButtonStyle(
                          height: 50,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                          backgroundColor:
                              marketplaceVm.agreeAcceptOfferDeadline
                                  ? PartnerAppColors.malachite
                                  : PartnerAppColors.grey1,
                          onPressed: () async {
                            final userVm = context.read<UserViewModel>();

                            if (marketplaceVm.agreeAcceptOfferDeadline) {
                              reserve(
                                  context: context,
                                  userVm: userVm,
                                  marketplaceVm: marketplaceVm);
                            }
                          },
                          child: Text(
                            tr('yes'),
                            style: Theme.of(context)
                                .textTheme
                                .headlineLarge!
                                .copyWith(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white,
                                ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                SmartGaps.gapH30,

                //
              ],
            ),
          ),
        );
      },
    );
  }

  void reserve(
      {required BuildContext context,
      required UserViewModel userVm,
      required MarketPlaceV2ViewModel marketplaceVm}) async {
    if (marketplaceVm.currentJobItem != null &&
        marketplaceVm.currentJobItem?.id != null) {
      marketplaceVm.isReserving = true;
      await tryCatchWrapper(
              context: myGlobals.innerScreenScaffoldKey!.currentContext!,
              function: marketplaceVm.checkPackage())
          .then((package) async {
        marketplaceVm.isReserveSuccess = RequestResponse.idle;
        marketplaceVm.isReserving = false;

        if ((package?.marketplaceNotPaid ?? false)) {
          marketplaceVm.isReserving = true;

          await userVm.checkNotPaidSettings().whenComplete(() async {
            if (!context.mounted) return;

            await needToPay(
                context: myGlobals.innerScreenScaffoldKey!.currentContext!,
                allowClose: (userVm.notPaidSetting?.allowClosePopup ?? false),
                header: (userVm.notPaidSetting?.popupHeader ?? ''),
                body: (userVm.notPaidSetting?.popupBody ?? ''),
                buttonLabel: (userVm.notPaidSetting?.popupButton ?? ''),
                url: (userVm.notPaidSetting?.popupUrl ?? ''),
                onTap: () {
                  userVm.notPaidDialogShowed = false;
                  userVm.clickFromNotPaid = true;
                  Launcher()
                      .launchUrlLocal((userVm.notPaidSetting?.popupUrl ?? ''));
                }).then((onValue) async {
              if (!context.mounted) return;

              if (onValue == null) {
                if ((userVm.notPaidSetting?.createTicketOnClosePopup ??
                    false)) {
                  await userVm.createTaskFromNotPaidPopup();
                }
              } else if (onValue == true) {
                if ((userVm.notPaidSetting?.createTicketOnPayNow ?? false)) {
                  await userVm.createTaskFromNotPaidPopup();
                }
              }
            });
          });
        } else {
          await tryCatchWrapper(
                  context: myGlobals.innerScreenScaffoldKey!.currentContext!,
                  function: marketplaceVm.normalReserve(
                      context:
                          myGlobals.innerScreenScaffoldKey!.currentContext!))
              .then((a) {
            if (marketplaceVm.isReserveSuccess != RequestResponse.success) {
              marketplaceVm.isReserveSuccess = RequestResponse.idle;
              marketplaceVm.isReserving = false;
            }
          });
        }
      });
    }
  }
}
