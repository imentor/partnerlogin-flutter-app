import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class JobInfo extends StatefulWidget {
  const JobInfo({
    super.key,
    required this.maximumOffer,
    required this.importantInfo,
    required this.importantKeys,
    required this.endDate,
    required this.startDate,
  });

  final int? maximumOffer;
  final List<Map<dynamic, dynamic>>? importantInfo;
  final List<String> importantKeys;
  final String startDate;
  final String endDate;

  @override
  JobInfoState createState() => JobInfoState();
}

class JobInfoState extends State<JobInfo> {
  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      String timeTitle = '';

      if (widget.startDate.isNotEmpty && widget.endDate.isNotEmpty) {
        timeTitle =
            '${tr('wanted_done')} \n ${Formatter.formatDateStrings(type: DateFormatType.fullMonthDate, dateString: widget.startDate)} \n ${Formatter.formatDateStrings(type: DateFormatType.fullMonthDate, dateString: widget.endDate)}';
      } else {
        if (widget.importantKeys.contains('352787')) {
          timeTitle = tr('job_info_1_0');
        } else if (widget.importantKeys.contains('352788')) {
          timeTitle = tr('job_info_1_1');
        } else if (widget.importantKeys.contains('352789')) {
          timeTitle = tr('job_info_1_2');
        } else if (!widget.importantKeys.contains('352789') &&
            !widget.importantKeys.contains('352788') &&
            !widget.importantKeys.contains('352787')) {
          timeTitle = tr('no_information_available');
        } else {
          timeTitle = tr('no_information_available');
        }
      }
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        backgroundColor: Colors.white,
        titlePadding: EdgeInsets.zero,
        title: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  children: [
                    const Spacer(),
                    IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Stack(
                        alignment: Alignment.center,
                        children: [
                          Container(
                            width: 23,
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black.withValues(alpha: 0.4),
                                width: 2,
                              ),
                              shape: BoxShape.circle,
                            ),
                          ),
                          Icon(
                            Icons.close,
                            size: 18,
                            color: Colors.black.withValues(alpha: 0.4),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Text(
                  tr('important_info'),
                  style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        fontSize: 20,
                        height: 1.1,
                        fontWeight: FontWeight.w700,
                        color:
                            Theme.of(context).colorScheme.onTertiaryContainer,
                      ),
                ),
                SmartGaps.gapH20,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        JobInfoCard(
                          label: timeTitle,
                          image: 'assets/images/clock_ic.svg',
                        ),
                        SmartGaps.gapW15,
                        JobInfoCard(
                          label: widget.importantKeys.contains('352785')
                              ? widget.importantInfo!
                                          .firstWhere((element) =>
                                              element.keys.first == '352785')
                                          .values
                                          .first !=
                                      1
                                  ? tr('job_info_2_0')
                                  : tr('job_info_2_1')
                              : tr('job_info_2_1'),
                          image: 'assets/images/Path 4016.svg',
                        ),
                      ],
                    ),
                    SmartGaps.gapH15,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        JobInfoCard(
                          label: widget.importantKeys.contains('353795')
                              ? tr('job_info_3_1')
                              : tr('job_info_3_0'),
                          image: 'assets/images/Path 4017.svg',
                        ),
                        SmartGaps.gapW15,
                        JobInfoCard(
                          label:
                              '${tr('job_info_4').split(';').first} ${widget.maximumOffer.toString()} ${tr('job_info_4').split(';').last}',
                          image: widget.maximumOffer == 1
                              ? 'assets/images/Path 4018.svg'
                              : widget.maximumOffer == 2
                                  ? 'assets/images/tilbud-2.svg'
                                  : 'assets/images/tilbud-3.svg',
                        ),
                      ],
                    ),
                    SmartGaps.gapH15,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        JobInfoCard(
                          label: widget.importantKeys.contains('352798') &&
                                  widget.importantKeys.contains('352800')
                              ? widget.importantInfo!.firstWhere((element) => element.keys.first == '352798').values.first == 1 &&
                                      widget.importantInfo!.firstWhere((element) => element.keys.first == '352800').values.first ==
                                          1
                                  ? tr('job_info_5_0')
                                  : widget.importantInfo!.firstWhere((element) => element.keys.first == '352798').values.first ==
                                              0 &&
                                          widget.importantInfo!
                                                  .firstWhere((element) =>
                                                      element.keys.first ==
                                                      '352800')
                                                  .values
                                                  .first ==
                                              1
                                      ? tr('job_info_5_1')
                                      : widget.importantInfo!
                                                      .firstWhere((element) =>
                                                          element.keys.first ==
                                                          '352798')
                                                      .values
                                                      .first ==
                                                  0 &&
                                              widget.importantInfo!
                                                      .firstWhere((element) => element.keys.first == '352800')
                                                      .values
                                                      .first ==
                                                  1
                                          ? tr('job_info_5_2')
                                          : tr('job_info_5_1')
                              : tr('job_info_5_1'),
                          image: 'assets/images/Path 4019.svg',
                        ),
                        SmartGaps.gapW15,
                        JobInfoCard(
                          label: widget.importantKeys.contains('352801')
                              ? tr('job_info_6_0')
                              : widget.importantKeys.contains('352803')
                                  ? tr('job_info_6_1')
                                  : tr('job_info_6_2'),
                          image: 'assets/images/Group 6787.svg',
                        ),
                      ],
                    ),
                    SmartGaps.gapH30,
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}

class JobInfoCard extends StatelessWidget {
  const JobInfoCard({
    super.key,
    required this.label,
    required this.image,
  });

  final String label;
  final String image;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 130,
      width: MediaQuery.of(context).size.width / 3,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(
            color: Theme.of(context).colorScheme.onTertiaryContainer),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: ListView(
        // crossAxisAlignment: CrossAxisAlignment.center,
        // mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset(
            image,
            width: 40,
          ),
          SmartGaps.gapH10,
          SizedBox(
            width: MediaQuery.of(context).size.width / 4,
            child: Text(
              label,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    fontSize: 14,
                    height: 1.1,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).colorScheme.onTertiaryContainer,
                  ),
            ),
          ),
        ],
      ),
    );
  }
}
