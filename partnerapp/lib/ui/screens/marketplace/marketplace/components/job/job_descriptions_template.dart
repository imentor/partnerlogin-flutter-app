import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/extensions.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class JobDescriptionTemplate extends StatelessWidget {
  const JobDescriptionTemplate({
    super.key,
    required this.enterpriseIndustry,
    required this.enterpriseIndex,
  });

  final EnterpriseIndustry enterpriseIndustry;
  final int enterpriseIndex;

  @override
  Widget build(BuildContext context) {
    final tenderVm = context.read<TenderFolderViewModel>();

    // SOME PRODUCTTYPEENs are NULL
    // SET DEFAULT TO DA

    final enterprise = context.locale.languageCode == 'da'
        ? tenderVm.wizardTexts
                .firstWhere(
                  (element) =>
                      element.producttypeid == enterpriseIndustry.enterpriseId,
                  orElse: () => AllWizardText.defaults(),
                )
                .producttypeDa ??
            ''
        : tenderVm.wizardTexts
                .firstWhere(
                    (element) =>
                        element.producttypeid ==
                        enterpriseIndustry.enterpriseId,
                    orElse: () => AllWizardText.defaults())
                .producttypeEn ??
            tenderVm.wizardTexts
                .firstWhere(
                    (element) =>
                        element.producttypeid ==
                        enterpriseIndustry.enterpriseId,
                    orElse: () => AllWizardText.defaults())
                .producttypeDa ??
            '';

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: ExpansionTile(
          title: Text(
            enterprise.isNotEmpty
                ? '$enterpriseIndex. ${enterprise.trim().toLowerCase().capitalize()}'
                : '',
            style: context.pttTitleSmall
                .copyWith(color: Colors.black, fontSize: 18),
          ),
          children: enterpriseIndustry.jobIndustries?.map((jobIndustry) {
                final industry = context.locale.languageCode == 'da'
                    ? tenderVm.fullIndustries
                            .firstWhere(
                              (element) =>
                                  element.brancheId.toString() ==
                                  jobIndustry.jobIndustryId,
                              orElse: () => FullIndustry.defaults(),
                            )
                            .brancheDa ??
                        ''
                    : tenderVm.fullIndustries
                            .firstWhere(
                                (element) =>
                                    element.brancheId.toString() ==
                                    jobIndustry.jobIndustryId,
                                orElse: () => FullIndustry.defaults())
                            .brancheEn ??
                        '';
                final industryIndex = enterpriseIndustry.jobIndustries!
                        .indexWhere((element) =>
                            element.jobIndustryId ==
                            jobIndustry.jobIndustryId) +
                    1;
                return ExpansionTile(
                  title: Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Text(
                      '$enterpriseIndex.$industryIndex. ${industry.trim()}',
                      style: context.pttTitleSmall.copyWith(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                  expandedCrossAxisAlignment: CrossAxisAlignment.start,
                  children: jobIndustry.descriptions?.map((description) {
                        final decriptionIndex = jobIndustry.descriptions!
                                .indexWhere(
                                    (element) => element == description) +
                            1;
                        return Padding(
                          padding: const EdgeInsets.fromLTRB(60, 10, 0, 10),
                          child: Text(
                            '$enterpriseIndex.$industryIndex.$decriptionIndex. ${description.trim()}',
                            style: context.pttBodySmall
                                .copyWith(color: Colors.black, fontSize: 14),
                          ),
                        );
                      }).toList() ??
                      [],
                );
              }).toList() ??
              []),
    );
  }
}
