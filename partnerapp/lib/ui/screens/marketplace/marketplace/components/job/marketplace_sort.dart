import 'package:Haandvaerker.dk/model/cities/denmark_cities.dart';
import 'package:Haandvaerker.dk/model/get_types_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/market_place_v2_viewmodel.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';

Future<void> hasFilterDialog({
  required BuildContext context,
}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: const EdgeInsets.all(20),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(
                onTap: () {
                  Navigator.of(dialogContext).pop();
                },
                child: const Icon(
                  FeatherIcons.x,
                  color: PartnerAppColors.spanishGrey,
                ),
              )
            ],
          ),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Text(
                    tr('new_jobs_available'),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        letterSpacing: 1.0,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                        fontSize: 18),
                  ),
                  SmartGaps.gapH10,
                  Text(
                    tr('old_filter_new_job'),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        letterSpacing: 1.0,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                        fontSize: 18),
                  ),
                  SmartGaps.gapH10,
                  Text(
                    tr('would_like_to_use_old_filter'),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        letterSpacing: 1.0,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                        fontSize: 18),
                  ),
                  SmartGaps.gapH30,
                  Row(
                    children: [
                      Expanded(
                        child: CustomDesignTheme.flatButtonStyle(
                          height: 50,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                              side: const BorderSide(
                                  color: PartnerAppColors.red)),
                          onPressed: () async {
                            Navigator.of(dialogContext).pop();
                            marketplaceSortDialog(context: context);
                          },
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  tr('yes_thank_you'),
                                  style: Theme.of(context)
                                      .textTheme
                                      .displayLarge!
                                      .copyWith(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400,
                                        color: PartnerAppColors.red,
                                      ),
                                )
                              ]),
                        ),
                      ),
                      SmartGaps.gapW10,
                      Expanded(
                        child: CustomDesignTheme.flatButtonStyle(
                          height: 50,
                          backgroundColor: PartnerAppColors.malachite,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                          onPressed: () async {
                            Navigator.of(dialogContext).pop();
                            context
                                .read<MarketPlaceV2ViewModel>()
                                .removeMarketplaceFilter();
                          },
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  tr('no_thanks'),
                                  style: Theme.of(context)
                                      .textTheme
                                      .displayLarge!
                                      .copyWith(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white,
                                      ),
                                )
                              ]),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      });
}

Future<void> marketplaceSortDialog({
  required BuildContext context,
}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(tr('your_current_filter'),
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(color: PartnerAppColors.darkBlue)),
            InkWell(
              onTap: () {
                Navigator.of(dialogContext).pop();
              },
              child: const Icon(
                FeatherIcons.x,
                color: PartnerAppColors.spanishGrey,
              ),
            )
          ],
        ),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: const SingleChildScrollView(child: MarketplaceSort())),
      );
    },
  );
}

class MarketplaceSort extends StatefulWidget {
  const MarketplaceSort({super.key});

  @override
  State<MarketplaceSort> createState() => _MarketplaceSortState();
}

class _MarketplaceSortState extends State<MarketplaceSort> {
  final formKey = GlobalKey<FormBuilderState>();

  List<DenmarkCities> selectedCities = [];

  bool activateAllIndustry = false;
  bool activateAllRegion = false;

  bool sortLoader = false;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final vm = context.read<MarketPlaceV2ViewModel>();

      if (vm.fullIndustries.isEmpty ||
          vm.denmarkCities.isEmpty ||
          vm.regionList.isEmpty) {
        setState(() {
          sortLoader = true;
        });

        await vm.getSortFilters().whenComplete(() {
          setState(() {
            sortLoader = false;
          });

          if (vm.savedFilterFullIndustries.length == vm.fullIndustries.length) {
            setState(() {
              activateAllIndustry = true;
            });
          }

          if (vm.savedFilterRegionList.length == vm.regionList.length) {
            setState(() {
              activateAllRegion = true;
            });
          }
        });
      } else {
        if (vm.savedFilterFullIndustries.length == vm.fullIndustries.length) {
          setState(() {
            activateAllIndustry = true;
          });
        }

        if (vm.savedFilterRegionList.length == vm.regionList.length) {
          setState(() {
            activateAllRegion = true;
          });
        }
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<MarketPlaceV2ViewModel>(builder: (_, vm, __) {
      final hasFilter = vm.marketPlaceFilterId > 0;

      return SingleChildScrollView(
        padding: const EdgeInsets.all(10),
        child: FormBuilder(
          key: formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (sortLoader) ...[
                const Center(
                  child: CircularProgressIndicator(),
                )
              ] else ...[
                Row(
                  children: [
                    Checkbox(
                        value: activateAllIndustry,
                        activeColor: PartnerAppColors.blue,
                        onChanged: (val) {
                          // if (!hasFilter) {
                          if (val != null) {
                            setState(() {
                              activateAllIndustry = val;
                            });

                            if (val) {
                              int middleIndex = vm.fullIndustries.length ~/ 2;
                              final groupOne =
                                  vm.fullIndustries.sublist(0, middleIndex);
                              final groupTwo =
                                  vm.fullIndustries.sublist(middleIndex);

                              formKey.currentState!.patchValue(
                                {
                                  'industry': [
                                    ...groupOne.map((e) => '${e.branchId}')
                                  ],
                                  'industry2': [
                                    ...groupTwo.map((e) => '${e.branchId}')
                                  ]
                                },
                              );
                            } else {
                              List<String> empty = [];
                              formKey.currentState!.patchValue(
                                  {'industry': empty, 'industry2': empty});
                            }
                          }
                          // }
                        }),
                    Text(
                      tr('industry'),
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(
                              letterSpacing: 1.0,
                              color: PartnerAppColors.darkBlue,
                              fontWeight: FontWeight.normal,
                              fontSize: 18),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Builder(builder: (context) {
                    int middleIndex = vm.fullIndustries.length ~/ 2;
                    final groupOne = vm.fullIndustries.sublist(0, middleIndex);
                    final groupTwo = vm.fullIndustries.sublist(middleIndex);

                    final groupOneInitialValue = _initializeGroupValues(
                        group: groupOne,
                        savedFilterFullIndustries:
                            vm.savedFilterFullIndustries);
                    final groupTwoInitialValue = _initializeGroupValues(
                        group: groupTwo,
                        savedFilterFullIndustries:
                            vm.savedFilterFullIndustries);

                    return Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            _buildCheckboxGroup(
                                group: groupOne,
                                initialValue: groupOneInitialValue,
                                name: 'industry',
                                hasFilter: hasFilter),
                            _buildCheckboxGroup(
                                group: groupTwo,
                                initialValue: groupTwoInitialValue,
                                name: 'industry2',
                                hasFilter: hasFilter),
                          ],
                        ),
                      ],
                    );
                  }),
                ),
                Row(
                  children: [
                    Checkbox(
                        value: activateAllRegion,
                        activeColor: PartnerAppColors.blue,
                        onChanged: (val) {
                          // if (!hasFilter) {
                          if (val != null) {
                            setState(() {
                              activateAllRegion = val;
                            });

                            if (val) {
                              int middleIndex = vm.regionList.length ~/ 2;
                              final groupOne =
                                  vm.regionList.sublist(0, middleIndex);
                              final groupTwo =
                                  vm.regionList.sublist(middleIndex);

                              formKey.currentState!.patchValue({
                                'region': [
                                  ...groupOne.map(
                                      (e) => (e.name ?? '').split(' ').last)
                                ],
                                'region2': [
                                  ...groupTwo.map(
                                      (e) => (e.name ?? '').split(' ').last)
                                ]
                              });
                            } else {
                              List<String> empty = [];
                              formKey.currentState!.patchValue(
                                  {'region': empty, 'region2': empty});
                            }
                          }
                          // }
                        }),
                    Text(
                      'Region',
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(
                              letterSpacing: 1.0,
                              color: PartnerAppColors.darkBlue,
                              fontWeight: FontWeight.normal,
                              fontSize: 18),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Builder(builder: (context) {
                    int middleIndex = vm.regionList.length ~/ 2;
                    final groupOne = vm.regionList.sublist(0, middleIndex);
                    final groupTwo = vm.regionList.sublist(middleIndex);

                    final groupOneInitialValue = vm.savedFilterRegionList
                        .where((sub) => groupOne.any((e) =>
                            (e.name ?? '').split(' ').last ==
                            (sub.name ?? '').split(' ').last))
                        .map((l) => (l.name ?? '').split(' ').last)
                        .toList();

                    final groupTwoInitialValue = vm.savedFilterRegionList
                        .where((sub) => groupTwo.any((e) =>
                            (e.name ?? '').split(' ').last ==
                            (sub.name ?? '').split(' ').last))
                        .map((l) => (l.name ?? '').split(' ').last)
                        .toList();

                    return Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Flexible(
                          child: CustomCheckboxGroupFormBuilder(
                            options: [
                              ...groupOne.map((e) {
                                return FormBuilderFieldOption(
                                  value: (e.name ?? '').split(' ').last,
                                  child: Text(
                                    (e.name ?? '').split(' ').last,
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleMedium!
                                        .copyWith(
                                          color: PartnerAppColors.darkBlue,
                                          fontWeight: FontWeight.normal,
                                        ),
                                  ),
                                );
                              })
                            ],
                            name: 'region',
                            initialValue: [...groupOneInitialValue],
                            // enable: !hasFilter,
                          ),
                        ),
                        Flexible(
                          child: CustomCheckboxGroupFormBuilder(
                            options: [
                              ...groupTwo.map((e) {
                                return FormBuilderFieldOption(
                                  value: (e.name ?? '').split(' ').last,
                                  child: Text(
                                    (e.name ?? '').split(' ').last,
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleMedium!
                                        .copyWith(
                                          color: PartnerAppColors.darkBlue,
                                          fontWeight: FontWeight.normal,
                                        ),
                                  ),
                                );
                              })
                            ],
                            name: 'region2',
                            initialValue: [...groupTwoInitialValue],
                            // enable: !hasFilter,
                          ),
                        )
                      ],
                    );
                  }),
                ),
                Text(
                  tr('city'),
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 18),
                ),
                SmartGaps.gapH10,
                DropdownSearch<DenmarkCities>.multiSelection(
                  items: (filter, loadProps) => vm.denmarkCities,
                  selectedItems: vm.savedFilterDenmarkCities,
                  // enabled: !hasFilter,
                  compareFn: (DenmarkCities i, DenmarkCities s) {
                    return identical(i, s);
                  },
                  itemAsString: (item) => item.name ?? '',
                  filterFn: (item, filter) {
                    return (item.name ?? '')
                        .toLowerCase()
                        .contains(filter.toLowerCase());
                  },
                  popupProps: PopupPropsMultiSelection.menu(
                    showSearchBox: true,
                    emptyBuilder: (context, searchEntry) {
                      return Center(child: Text(tr('empty')));
                    },
                    fit: FlexFit.tight,
                    loadingBuilder: (context, searchEntry) {
                      return const CircularProgressIndicator(
                        color: PartnerAppColors.darkBlue,
                      );
                    },
                    searchDelay: const Duration(milliseconds: 0),
                  ),
                  onChanged: (value) {
                    setState(() {
                      selectedCities = [...value];
                    });
                  },
                ),
                SmartGaps.gapH30,
                CustomDesignTheme.flatButtonStyle(
                  height: 50,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                      side: const BorderSide(color: PartnerAppColors.darkBlue)),
                  onPressed: () async {
                    if ((formKey.currentState?.fields['industry']?.value
                                    as List? ??
                                [])
                            .isEmpty &&
                        (formKey.currentState?.fields['industry2']
                                    ?.value as List? ??
                                [])
                            .isEmpty &&
                        (formKey.currentState?.fields['region']?.value
                                    as List? ??
                                [])
                            .isEmpty &&
                        (formKey.currentState?.fields['region2']?.value
                                    as List? ??
                                [])
                            .isEmpty &&
                        selectedCities.isEmpty) {
                      Navigator.of(context).pop();

                      await tryCatchWrapper(
                          context: myGlobals.homeScaffoldKey!.currentContext,
                          function: vm.removeMarketplaceFilter());
                    } else {
                      Map<String, dynamic> tempPayload = {};
                      List jobIndustryList = [];
                      List regionList = [];

                      if (formKey.currentState!.fields['industry']!.value !=
                          null) {
                        jobIndustryList.addAll([
                          ...(formKey.currentState!.fields['industry']!.value
                              as List)
                        ]);
                      }

                      if (formKey.currentState!.fields['industry2']!.value !=
                          null) {
                        jobIndustryList.addAll([
                          ...(formKey.currentState!.fields['industry2']!.value
                              as List)
                        ]);
                      }

                      if (jobIndustryList.isNotEmpty) {
                        tempPayload
                            .addAll({"jobIndustry": jobIndustryList.join(',')});
                      }

                      if (formKey.currentState!.fields['region']!.value !=
                          null) {
                        regionList.addAll([
                          ...(formKey.currentState!.fields['region']!.value
                              as List)
                        ]);
                      }

                      if (formKey.currentState!.fields['region2']!.value !=
                          null) {
                        regionList.addAll([
                          ...(formKey.currentState!.fields['region2']!.value
                              as List)
                        ]);
                      }

                      if (regionList.isNotEmpty) {
                        tempPayload.addAll({"region": regionList.join(',')});
                      }

                      if (selectedCities.isNotEmpty) {
                        tempPayload.addAll({
                          "zip":
                              selectedCities.map((e) => e.postalCode).join(',')
                        });
                      }

                      Navigator.of(context).pop();

                      await tryCatchWrapper(
                          context: myGlobals.homeScaffoldKey!.currentContext,
                          function:
                              vm.saveMarketplaceFilter(payload: tempPayload));
                    }
                  },
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          hasFilter ? 'Opdater filter' : tr('save_filter'),
                          style: Theme.of(context)
                              .textTheme
                              .displayLarge!
                              .copyWith(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                                color: PartnerAppColors.darkBlue,
                              ),
                        )
                      ]),
                ),
              ]
            ],
          ),
        ),
      );
    });
  }

  List<String> _initializeGroupValues(
      {required List<Data> group,
      required List<Data> savedFilterFullIndustries}) {
    return savedFilterFullIndustries
        .where(
            (sub) => group.any((element) => element.branchId == sub.branchId))
        .map((e) => e.branchId.toString())
        .toList();
  }

  Widget _buildCheckboxGroup(
      {required List<Data> group,
      required List<String> initialValue,
      required String name,
      required bool hasFilter}) {
    return Flexible(
      child: CustomCheckboxGroupFormBuilder(
        options: group.map((e) {
          return FormBuilderFieldOption(
            value: '${e.branchId}',
            child: Text(
              e.branche ?? '',
              style: Theme.of(context).textTheme.titleMedium!.copyWith(
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.normal,
                  ),
            ),
          );
        }).toList(),
        initialValue: initialValue,
        name: name,
        // enable: !hasFilter,
        optionsOrientation: OptionsOrientation.vertical,
      ),
    );
  }
}
