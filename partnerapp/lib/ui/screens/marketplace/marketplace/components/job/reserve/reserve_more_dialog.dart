import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/job/reserve/components/no_offer_template_card.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/market_place_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class ReserveMoreDialog extends StatelessWidget {
  const ReserveMoreDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      backgroundColor: Colors.white,
      titlePadding: EdgeInsets.zero,
      contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 35),
      insetPadding: const EdgeInsets.fromLTRB(20, 0, 20, 40),
      title: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Align(
          alignment: Alignment.bottomRight,
          child: IconButton(
            onPressed: () {
              final marketplaceVm = context.read<MarketPlaceV2ViewModel>();
              if (marketplaceVm.checkReserveResponse?.success ?? false) {
                marketplaceVm.cancelDialog = true;
              } else {
                Navigator.pop(context);
              }
            },
            icon: Stack(
              alignment: Alignment.center,
              children: [
                Container(
                  width: 23,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black.withValues(alpha: 0.4),
                      width: 2,
                    ),
                    shape: BoxShape.circle,
                  ),
                ),
                Icon(
                  Icons.close,
                  size: 18,
                  color: Colors.black.withValues(alpha: 0.4),
                ),
              ],
            ),
          ),
        ),
      ),
      content: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ///

            SvgPicture.asset('assets/images/warning.svg'),

            SmartGaps.gapH10,

            Text(
              tr('sold_out_reservations'),
              textAlign: TextAlign.center,
              style: context.pttTitleSmall
                  .copyWith(color: Colors.black, height: 1.5),
            ),

            SmartGaps.gapH10,

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 17),
              child: Text(
                tr('sold_out_reservations_desc'),
                textAlign: TextAlign.center,
                style: context.pttBodyLarge
                    .copyWith(color: Colors.black, fontWeight: FontWeight.w400),
              ),
            ),
            SmartGaps.gapH30,

            ...context.watch<ClientsViewModel>().partnerJobs.map(
                  (job) => NoOfferTemplateCard(job: job),
                ),

            const Padding(
              padding: EdgeInsets.only(top: 10, bottom: 20),
              child: Divider(
                color: Color(0xff707070),
                thickness: 0.3,
              ),
            ),

            CustomDesignTheme.flatButtonStyle(
              onPressed: context
                          .watch<MarketPlaceV2ViewModel>()
                          .checkReserveResponse
                          ?.success ??
                      false
                  ? () => context
                      .read<MarketPlaceV2ViewModel>()
                      .isReserveSuccess = RequestResponse.extra
                  : null,
              backgroundColor: context
                          .watch<MarketPlaceV2ViewModel>()
                          .checkReserveResponse
                          ?.success ??
                      false
                  ? PartnerAppColors.darkBlue
                  : PartnerAppColors.grey,
              child: SizedBox(
                height: 50,
                child: Center(
                  child: Text(
                    tr('buy_multiple_reservations'),
                    style: context.pttBodyLarge
                        .copyWith(color: Colors.white, fontSize: 14, height: 1),
                  ),
                ),
              ),
            )

            ///
          ],
        ),
      ),
    );
  }
}
