import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/job/marketplace_sort.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/labor_task_toggle_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/market_place_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/mester-mester/mester_partner_profile_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/packages/packages_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class ReservationLabel extends StatelessWidget {
  const ReservationLabel({super.key});

  @override
  Widget build(BuildContext context) {
    final laborTaskVm = context.read<LaborTaskToggleViewModel>();
    return Column(
      children: [
        Consumer<LaborTaskToggleViewModel>(
          builder: (context, vm, _) => Material(
            color: Colors.white,
            child: InkWell(
              onTap: context.watch<MarketPlaceV2ViewModel>().busy ||
                      context.watch<MesterPartnerProfileViewModel>().busy
                  ? null
                  : () {
                      //
                      vm.setTask(!vm.isTask);

                      if (vm.isTask) {
                        final marketplaceVm =
                            context.read<MarketPlaceV2ViewModel>();
                        marketplaceVm.setBusy(true);
                        if (marketplaceVm.fromNewsFeed) {
                          marketplaceVm.fromNewsFeed = false;
                        }
                      } else {
                        final mesterVm =
                            context.read<MesterPartnerProfileViewModel>();
                        mesterVm.setBusy(true);
                        if (mesterVm.fromNewsFeed) {
                          mesterVm.fromNewsFeed = false;
                        }
                      }

                      //
                    },
              child: Container(
                height: 48.0,
                width: double.infinity,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    border: Border.all(
                        color: Theme.of(context).colorScheme.onSurfaceVariant)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset(
                      vm.isTask
                          ? 'assets/images/see_job.svg'
                          : 'assets/images/see_task.svg',
                      height: 30,
                      width: 30,
                      colorFilter: ColorFilter.mode(
                        Theme.of(context).colorScheme.onSurfaceVariant,
                        BlendMode.srcIn,
                      ),
                    ),
                    SmartGaps.gapW8,
                    Text(
                      tr(vm.isTask ? 'see_labor' : 'see_tasks'),
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                          color:
                              Theme.of(context).colorScheme.onSurfaceVariant),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        SmartGaps.gapH14,
        Row(
          children: [
            Expanded(
              child: Consumer<PackagesViewModel>(
                builder: (context, vm, _) => Container(
                  height: 48.0,
                  width: double.infinity,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.onSurfaceVariant,
                    border: Border.all(
                        color: Theme.of(context).colorScheme.onSurfaceVariant,
                        width: 1.0),
                  ),
                  child: Text(
                    '${tr('number_of_reservations')}: ${vm.nummode} ${tr('out_of')} ${vm.reserveSize}',
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .titleSmall!
                        .copyWith(color: Colors.white),
                  ),
                ),
              ),
            ),
            if (laborTaskVm.isTask) SmartGaps.gapW10,
            if (laborTaskVm.isTask)
              InkWell(
                onTap: () => marketplaceSortDialog(context: context),
                child: const Icon(
                  FeatherIcons.filter,
                  size: 35,
                ),
              ),
          ],
        ),
        if (!laborTaskVm.isTask) SmartGaps.gapH10,
        if (!laborTaskVm.isTask)
          Row(
            children: [
              Expanded(
                child: expandedButtonWidget(
                  context,
                  color: Theme.of(context).colorScheme.onSurfaceVariant,
                  padding: const EdgeInsets.all(13),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        tr('filter'),
                        style: const TextStyle(
                            color: Colors.white, fontSize: 14.0),
                      ),
                      SmartGaps.gapW2,
                    ],
                  ),
                  onTap: () {},
                ),
              ),
            ],
          ),
      ],
    );
  }

  Widget expandedButtonWidget(
    BuildContext context, {
    required Widget child,
    required Function() onTap,
    Color color = PartnerAppColors.darkBlue,
    EdgeInsets padding = const EdgeInsets.symmetric(vertical: 8.0),
    elevation = 0.0,
  }) {
    return Material(
      elevation: elevation,
      color: color,
      child: InkWell(
          onTap: onTap,
          child: Padding(padding: padding, child: Center(child: child))),
    );
  }
}
