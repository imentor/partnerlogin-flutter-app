import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OnboardingDialog extends StatelessWidget {
  const OnboardingDialog({
    super.key,
    required this.url,
  });

  final String url;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      backgroundColor: Colors.white,
      titlePadding: EdgeInsets.zero,
      contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 30),
      content: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(
              'assets/images/check-mark.svg',
              width: 100,
              colorFilter: const ColorFilter.mode(
                  PartnerAppColors.green, BlendMode.srcIn),
            ),
            SmartGaps.gapH20,
            Text(
              tr('trial_period_activated'),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                    height: 1,
                  ),
            ),
            SmartGaps.gapH20,
            Text(
              tr('plan_is_active'),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    height: 1,
                  ),
            ),
            SmartGaps.gapH30,
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: CustomDesignTheme.flatButtonStyle(
                onPressed: () async {
                  changeDrawerRoute(Routes.inAppWebView,
                      arguments: Uri.parse(url));
                },
                height: 50,
                backgroundColor: PartnerAppColors.darkBlue,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4),
                ),
                child: Center(
                  child: Text(
                    tr('update_company_profile'),
                    style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                          fontSize: 14,
                          fontWeight: FontWeight.w700,
                          height: 1,
                          color: Colors.white,
                        ),
                  ),
                ),
              ),
            ),
            SmartGaps.gapH30,
            Text(
              tr('really_good_bidding'),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    height: 1,
                  ),
            ),
            SmartGaps.gapH10,
            Text(
              tr('the_best_greetings'),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    height: 1,
                  ),
            ),
            SmartGaps.gapH30,
            Container(
              decoration: const BoxDecoration(
                color: PartnerAppColors.darkBlue,
              ),
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    'assets/images/h.svg',
                    width: 40,
                  ),
                  SmartGaps.gapH5,
                  Text(
                    'Håndværker.dk',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          height: 1,
                          color: Colors.white,
                        ),
                  ),
                  SmartGaps.gapH20,
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      style:
                          Theme.of(context).textTheme.headlineLarge!.copyWith(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                height: 1,
                                color: Colors.white,
                              ),
                      children: [
                        TextSpan(
                          text: '${tr('in_case_of_questions')} ',
                        ),
                        TextSpan(
                          style: Theme.of(context)
                              .textTheme
                              .headlineLarge!
                              .copyWith(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                height: 1,
                                color: PartnerAppColors.accentBlue,
                              ),
                          text: 'info@haandvaerker.dk.',
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Navigator.pop(context, true);
                            },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
