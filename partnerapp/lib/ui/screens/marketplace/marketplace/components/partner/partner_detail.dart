import 'package:Haandvaerker.dk/model/mester_til_mester/mester_to_mester_job_model.dart';
import 'package:Haandvaerker.dk/ui/components/cache_image.dart';
import 'package:Haandvaerker.dk/ui/components/custom_button.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/package/freemium_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/package/package_popup_dialog.dart';
// import 'package:Haandvaerker.dk/ui/screens/customers/components/price_feedback_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/partner/partner_detail_grid.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/mester-mester/mester_partner_profile_viewmodel.dart';
// import 'package:Haandvaerker.dk/utils/global_keys.dart';
// import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
// import 'package:Haandvaerker.dk/viewmodel/book_partner_viewmodel.dart';
// import 'package:Haandvaerker.dk/viewmodel/open_jobs_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/packages/packages_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
// import 'package:Haandvaerker.dk/viewmodel/price_feedback_viewmodel.dart';
// import 'package:Haandvaerker.dk/viewmodel/reserve_partners_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
// import 'package:provider/provider.dart';

class PartnerDetail extends StatefulWidget {
  final MesterToMesterJobItem? partner;
  const PartnerDetail({super.key, this.partner});

  @override
  PartnerDetailState createState() => PartnerDetailState();
}

class PartnerDetailState extends State<PartnerDetail> {
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();
  bool _show = false;
  // bool _isAvailable = true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      // packagesVm = Provider.of<PackagesViewModel>(context, listen: false);
      // final openJobsVm = Provider.of<OpenJobsViewModel>(context, listen: false);
      // final response = await tryCatchWrapper(
      //     context: myGlobals.homeScaffoldKey!.currentContext,
      //     function: openJobsVm.checkmeeting(appid: widget.partner!.appid));
      // if (!mounted) return;
      // setState(() {
      //   _isAvailable = response?.available ?? true;
      // });
    });
  }

  Future<void> bookPartner() async {
    final packagesVm = context.read<PackagesViewModel>();
    if (packagesVm.packageHandle == PackageHandle.freemium &&
        packagesVm.reserveSize == '0') {
      await showFreemiumDialog(context);
    } else if (!packagesVm.mesterMesterReserve.status) {
      await showUnAvailableDialog(context, packagesVm.mesterMesterReserve,
          popTwice: false);
    } else {
      // if (Provider.of<OpenJobsViewModel>(context, listen: false)
      //         .openJobs
      //         .partnerinvoice !=
      //     0) {
      //   await showErrorDialog(context, tr('error'),
      //       tr('cant_book_payment_not_receive'), tr('close'));
      // } else {
      final selected = await showOkCancelAlertDialog(
        context: context,
        title: tr('confirm_reservation'),
        message: tr('confirm_meeting'),
        okLabel: tr('book'),
      );

      if (selected == OkCancelResult.ok && mounted) {
        showLoadingDialog(context, loadingKey);

        final mesterVm = context.read<MesterPartnerProfileViewModel>();
        final userVm = context.read<UserViewModel>();

        final reserve = await mesterVm.reserveMesterJob(
            partnerId: userVm.userModel?.user?.id.toString() ?? '',
            mesterId: widget.partner!.id!);

        if (reserve) {
          await mesterVm.getMesterJobs(getAll: true);
          Navigator.of(loadingKey.currentContext!).pop();
          if (mounted) {
            final success = await showSuccessAnimationDialog(
              context,
              true,
              tr('reserve_labor_success'),
            );
            if (success && mounted) {
              Navigator.pop(context);
            }
          }
        } else {
          Navigator.of(loadingKey.currentContext!).pop();
          if (mounted) {
            await showErrorDialog(
              context,
              tr('error'),
              tr('reserve_labor_failed'),
              tr('close'),
            );
          }
        }

        // dynamic tilbudid = widget.partner!.id;
        // String? short = widget.partner!.urlshort as String?;

        // final bookPartnerVM =
        //     Provider.of<BookPartnerViewModel>(context, listen: false);
        // final priceVm =
        //     Provider.of<PriceFeedbackViewModel>(context, listen: false);

        // await tryCatchWrapper(
        //     context: myGlobals.homeScaffoldKey!.currentContext,
        //     function: bookPartnerVM.performRequest(
        //         tilbudid: tilbudid, short: short));

        // var feedbackResponse = await tryCatchWrapper(
        //     context: myGlobals.homeScaffoldKey!.currentContext,
        //     function: priceVm
        //         .getFeedbackList(shortUrl: widget.partner!.urlshort)
        //         .then((value) => value!));

        // String? message = '';
        // bool isSuccessful = false;
        // bool hasCustomers = feedbackResponse?.result?.isNotEmpty ?? false;

        // if (mounted) {
        //   final reservePartnersVM =
        //       Provider.of<ReservePartnersViewModel>(context, listen: false);

        //   if (bookPartnerVM.getValue.success!) {
        //     print('bookPartnerVM success');
        //     if (bookPartnerVM.getValue.result!.app == 2) {
        //       isSuccessful = true;
        //       message = tr('success!');
        //       //add to reserve partner list
        //       reservePartnersVM.setValue(bookPartnerVM.getValue.result!.name,
        //           bookPartnerVM.getValue.result!.phone, widget.partner!.id);
        //     } else {
        //       isSuccessful = false;
        //       if (bookPartnerVM.getValue.result!.texterror != null) {
        //         message = bookPartnerVM.getValue.result!.texterror as String?;
        //       } else {
        //         message = tr('error');
        //       }
        //     }
        //   } else {
        //     print('bookPartnerVM failed');
        //     isSuccessful = false;
        //     message = tr('failed');
        //   }
        //   Navigator.of(loadingKey.currentContext!).pop();
        //   await showSuccessAnimationDialog(
        //           myGlobals.homeScaffoldKey!.currentContext!,
        //           isSuccessful,
        //           message)
        //       .then((value) async {
        //     if (isSuccessful && hasCustomers) {
        //       await showPriceFeedback(context,
        //           shorturl: widget.partner!.urlshort);
        //     }
        //     await packagesVm.getPackages();
        //     if (mounted) {
        //       Navigator.of(context).pop();
        //     }
        //   });
        // }
      }
      // }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Stack(children: [
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 8),
          child: ListView(
            children: [
              SmartGaps.gapH20,
              CacheImage(
                imageUrl: widget.partner!.images!.isEmpty
                    ? ''
                    : widget.partner!.images!.first,
                height: 100,
                width: double.infinity,
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  color: const Color(0xff00d8d6),
                  width: double.infinity,
                  height: 60,
                  child: Text(
                      '${widget.partner!.postalCode} ${widget.partner!.city ?? ''}',
                      style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 18))),
              SmartGaps.gapH5,
              Container(
                padding: const EdgeInsets.fromLTRB(15, 8, 15, 8),
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              widget.partner!.title ?? '',
                              overflow: TextOverflow.fade,
                              style: const TextStyle(
                                  fontSize: 26,
                                  fontWeight: FontWeight.w600,
                                  color: Color.fromRGBO(0, 54, 69, 1)),
                            ),
                          ),
                          // title for job item in modal
                          SmartGaps.gapW20,
                          // labor tag title
                          Text(
                            tr('labor'),
                            overflow: TextOverflow.fade,
                            style: const TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: Color.fromRGBO(0, 54, 69, 1)),
                          ),
                        ]),
                    _show
                        ? Container(
                            padding: const EdgeInsets.only(top: 15),
                            width: double.infinity,
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    tr('notes'),
                                    style: Theme.of(context)
                                        .textTheme
                                        .headlineSmall,
                                  ),
                                  SmartGaps.gapH10,
                                  Text(widget.partner!.description ?? '',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium)
                                ]))
                        : Container(),
                    ShowHideButton(
                      show: _show,
                      onPressed: () {
                        setState(() {
                          _show = !_show;
                        });
                      },
                    ),
                    SmartGaps.gapH30,
                    Container(
                      alignment: Alignment.center,
                      width: double.infinity,
                      // element containing important info about customer
                      child: Center(
                        child: Text(tr('info_about_manpower'),
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headlineMedium),
                      ),
                    ),
                    SmartGaps.gapH30,
                    PartnerDetailGrid(
                      partner: widget.partner,
                    ),
                    SmartGaps.gapH20,
                    // _isAvailable
                    //     ?
                    InkWell(
                      onTap: () async {
                        await bookPartner();
                      },
                      // reserve labor button
                      child: Container(
                        height: 70,
                        width: double.infinity,
                        color: const Color(0xff00DA58),
                        child: Center(
                            child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              tr('book'),
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineLarge!
                                  .copyWith(color: Colors.white),
                            ),
                            SmartGaps.gapW10,
                            const Icon(Icons.arrow_forward,
                                color: Colors.white),
                          ],
                        )),
                      ),
                    ),
                    // : Center(
                    //     child: Text(
                    //         tr('this_meeting_is_not_available'),
                    //         style: Theme.of(context)
                    //             .textTheme
                    //             .titleSmall!
                    //             .copyWith(color: Colors.red)),
                    //   ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Positioned(
          left: 10,
          top: 10,
          child: Opacity(
            opacity: 0.7,
            child: Container(
                padding: const EdgeInsets.all(5),
                decoration: const BoxDecoration(
                  color: Colors.black,
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      blurRadius: 2.0,
                      spreadRadius: 0.0,
                      offset: Offset(2.0, 2.0),
                    ),
                  ],
                ),
                child: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.close,
                      color: Colors.grey[200],
                    ))),
          ),
        )
      ])),
    );
  }
}
