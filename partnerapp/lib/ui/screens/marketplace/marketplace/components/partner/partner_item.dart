import 'package:Haandvaerker.dk/model/mester_til_mester/mester_to_mester_job_model.dart';
import 'package:Haandvaerker.dk/ui/components/cache_image.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/partner/partner_detail.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class PartnerItem extends StatelessWidget {
  const PartnerItem({
    super.key,
    required this.indexPartner,
  });

  final MesterToMesterJobItem indexPartner;

  @override
  Widget build(BuildContext context) {
    //

    final partnerIds = indexPartner.partners!.map((e) => e.id!).toList();

    return Container(
      margin: const EdgeInsets.only(top: 10),
      child: Card(
          elevation: 4,
          child: Column(
            children: <Widget>[
              AbsorbPointer(
                absorbing: partnerIds.contains(
                    context.read<UserViewModel>().userModel?.user?.id),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context, rootNavigator: true).push(
                      MaterialPageRoute<void>(
                        fullscreenDialog: true,
                        builder: (_) => PartnerDetail(partner: indexPartner),
                      ),
                    );
                  },
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // labor items layout
                        Expanded(
                          flex: 2,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: const EdgeInsets.only(
                                    top: 8, right: 0, bottom: 0, left: 10),
                                color: const Color(0xff5cd3d2),
                                width: double.infinity,
                                height: 50,
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        indexPartner.postalCode ?? '',
                                        overflow: TextOverflow.ellipsis,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyMedium!
                                            .copyWith(
                                                color: Colors.white,
                                                height: 1.2),
                                      ),
                                      Expanded(
                                        child: Text(
                                          indexPartner.city ?? '',
                                          overflow: TextOverflow.ellipsis,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyMedium!
                                              .copyWith(
                                                  color: Colors.white,
                                                  height: 1.2),
                                        ),
                                      )
                                    ]),
                              ),
                              CacheImage(
                                imageUrl: indexPartner.images!.isEmpty
                                    ? ''
                                    : indexPartner.images!.first,
                                height: 70,
                                width: double.infinity,
                              ),
                            ],
                          ),
                        ),

                        // title of labor item
                        Expanded(
                            flex: 3,
                            child: Container(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    partnerIds.contains(context
                                            .read<UserViewModel>()
                                            .userModel
                                            ?.user
                                            ?.id) //mark as reserved if appid is in the reserved list
                                        ? Text(tr('booked'),
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyMedium!
                                                .copyWith(color: Colors.red))
                                        : Container(),
                                    SmartGaps.gapH5,
                                    Container(
                                      height: 30,
                                      alignment: Alignment.centerLeft,
                                      child: Text(indexPartner.title ?? '',
                                          overflow: TextOverflow.ellipsis,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyMedium!
                                              .copyWith(
                                                  height: 1,
                                                  fontWeight: FontWeight.w700)),
                                    ),
                                    // labor item description
                                    SizedBox(
                                      height: 70,
                                      width: double.infinity,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Expanded(
                                              child: Text(
                                            indexPartner.description ?? '',
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 4,
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyMedium!
                                                .copyWith(fontSize: 13),
                                          )),
                                        ],
                                      ),
                                    ),
                                  ]),
                            ))
                      ]),
                ),
              ),
              // reserve section for labor
              partnerIds.contains(
                      context.read<UserViewModel>().userModel?.user?.id)
                  ? Container(
                      color: const Color(0xff00c34f),
                      padding:
                          const EdgeInsets.only(top: 8, bottom: 8, left: 10),
                      // color: Colors.lightGreenAccent,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '${tr('name')}: ',
                              overflow: TextOverflow.fade,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .copyWith(height: 1.2, color: Colors.white),
                            ),
                            Row(
                              children: <Widget>[
                                Text('${tr('mobile')}: ',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyMedium!
                                        .copyWith(
                                            height: 1.2, color: Colors.white)),
                                GestureDetector(
                                  onTap: () async {
                                    if (await canLaunchUrl(
                                        Uri.parse(Uri.encodeFull('tel:')))) {
                                      await launchUrl(
                                          Uri.parse(Uri.encodeFull('tel:')));
                                    } else {
                                      throw 'Could not launch tel:';
                                    }
                                  },
                                  child: Text(
                                    '',
                                    overflow: TextOverflow.fade,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyMedium!
                                        .copyWith(
                                            height: 1.2, color: Colors.white),
                                  ),
                                ),
                              ],
                            )
                          ]))
                  : Container()
            ],
          )),
    );
  }
}
