// extra info grid for labor items
import 'package:Haandvaerker.dk/model/mester_til_mester/mester_to_mester_job_model.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PartnerDetailGrid extends StatelessWidget {
  const PartnerDetailGrid({
    super.key,
    required this.partner,
  });
  final MesterToMesterJobItem? partner;

  @override
  Widget build(BuildContext context) {
    getSVGImage(String image) => SvgPicture.asset(image, width: 70, height: 70);

    getStartDate() {
      String? date = Formatter.formatDateStrings(
        type: DateFormatType.fullMonthDate,
        dateString: partner?.startDate,
      );
      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SmartGaps.gapH20,
          getSVGImage('assets/images/calendar_check.svg'),
          Flexible(
            child: Text('${tr('start_date')}: \n$date',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyMedium),
          )
        ],
      );
    }

    Widget getNumberofEmployees() {
      return Column(
        children: <Widget>[
          SmartGaps.gapH20,
          getSVGImage('assets/images/employees.svg'),
          Flexible(
            child: Text(
                '${tr('number_employed_men')}: ${partner!.numberOfStaff}',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyMedium),
          )
        ],
      );
    }

    getTimePrice() {
      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SmartGaps.gapH20,
          getSVGImage('assets/images/krona.svg'),
          Flexible(
            child: Text('${tr('hourly_rate')}: ${partner!.budget}',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyMedium),
          )
        ],
      );
    }

    getPartnerBids() {
      int partnerOffer = partner!.numberOfOffers ?? 0;

      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SmartGaps.gapH20,
          if (partnerOffer > 2)
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SvgPicture.asset('assets/images/partner.svg',
                    width: 40, height: 40),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SvgPicture.asset('assets/images/partner.svg',
                        width: 40, height: 40),
                    SvgPicture.asset('assets/images/partner.svg',
                        width: 40, height: 40),
                  ],
                )
              ],
            )
          else
            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: List.generate(
                    partnerOffer,
                    (_) => (partnerOffer == 1)
                        ? getSVGImage('assets/images/partner.svg')
                        : getSVGImage('assets/images/partner.svg'))),
          Flexible(
            child: Text('$partnerOffer ${tr('bidding_on_task')}',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyMedium),
          )
        ],
      );
    }

    List<Widget> gridList = [
      getStartDate(),
      getNumberofEmployees(),
      getTimePrice(),
      getPartnerBids()
    ];
    return SizedBox(
      width: double.infinity,
      child: GridView.count(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          crossAxisCount: 2,
          mainAxisSpacing: 20,
          children: List.generate(gridList.length, (index) {
            final f = gridList.elementAt(index);
            return Container(
              child: f,
            );
          })),
    );
  }
}
