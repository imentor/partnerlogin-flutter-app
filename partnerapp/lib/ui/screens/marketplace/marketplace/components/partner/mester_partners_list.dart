import 'package:Haandvaerker.dk/model/mester_partners_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/pagination/custom_pagination.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/marketplace_header.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/partner/partner_item.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/mester-mester/mester_partner_profile_viewmodel.dart';
// import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class MesterPartnersList extends StatefulWidget {
  const MesterPartnersList({super.key});

  @override
  MesterPartnersListState createState() => MesterPartnersListState();
}

class MesterPartnersListState extends State<MesterPartnersList>
    with TickerProviderStateMixin {
  double? filtersHeight = 0;
  String? searchKeyword;
  bool isFiltering = true;
  List<String> locationFilters = [];
  List<String> taskTypeFilters = [];
  List<String> selectedLocations = [];
  List<String> selectedTaskTypes = [];
  List<MesterPartnersModel> mesterJobs = [];
  List<MesterPartnersModel> filteredJobs = [];
  Iterable<MesterPartnersModel> filteredJobsByLocation = [];
  Iterable<MesterPartnersModel> filteredJobsByTaskType = [];
  late ScrollController scroll;
  final searchController = TextEditingController();

  @override
  void initState() {
    scroll = ScrollController();
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      //

      final mesterVm = context.read<MesterPartnerProfileViewModel>();
      mesterVm.type = 1;
      mesterVm.currentPage = 1;

      if (!mesterVm.busy) {
        mesterVm.setBusy(true);
      }

      if (!mesterVm.fromNewsFeed) {
        await tryCatchWrapper(
          context: myGlobals.homeScaffoldKey!.currentContext,
          function: mesterVm.getMesterJobs(getAll: true),
        );
      }
      mesterVm.setBusy(false);

      //
    });
  }

  @override
  void dispose() {
    scroll.dispose();
    super.dispose();
  }

  Widget expandedButtonWidget(
    BuildContext context, {
    required Widget child,
    required Function() onTap,
    Color color = PartnerAppColors.darkBlue,
    EdgeInsets padding = const EdgeInsets.symmetric(vertical: 8.0),
    elevation = 0.0,
  }) {
    return Material(
      elevation: elevation,
      color: color,
      child: InkWell(
          onTap: onTap,
          child: Padding(padding: padding, child: Center(child: child))),
    );
  }

  Widget filterFormWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SmartGaps.gapH14,
        typeTypeFiltersWidget(context),
        SmartGaps.gapH14,
        locationFiltersWidget(context),
      ],
    );
  }

  Widget locationFiltersWidget(BuildContext context) {
    return MultiSelectDialogField<String>(
      searchable: true,
      initialValue: selectedLocations,
      buttonIcon: const Icon(Icons.filter_list),
      listType: MultiSelectListType.LIST,
      selectedColor: Theme.of(context).colorScheme.onSurfaceVariant,
      decoration: BoxDecoration(color: Colors.grey.shade200),
      items: locationFilters.map((e) => MultiSelectItem(e, e)).toList(),
      title: Text(
        tr('location'),
        style: TextStyle(
            color: Theme.of(context).colorScheme.onSurfaceVariant,
            fontSize: 14.0),
      ),
      buttonText: Text(
        tr('location'),
        style: TextStyle(
            color: Theme.of(context).colorScheme.onSurfaceVariant,
            fontSize: 14.0),
      ),
      onConfirm: (values) {
        setState(() {
          selectedLocations = values;
          filteredJobsByLocation = mesterJobs
              .where((job) => selectedLocations.contains(job.postnumbercity2));
          filteredJobs = [];
          filteredJobs.addAll(filteredJobsByTaskType);
          filteredJobs.addAll(filteredJobsByLocation);
          filteredJobs.toSet().toList();
          if (filteredJobsByTaskType.isEmpty &&
              filteredJobsByLocation.isEmpty) {
            filteredJobs = mesterJobs;
          }
        });
      },
    );
  }

  Widget typeTypeFiltersWidget(BuildContext context) {
    return MultiSelectDialogField<String>(
      searchable: true,
      initialValue: selectedTaskTypes,
      buttonIcon: const Icon(Icons.filter_list),
      listType: MultiSelectListType.LIST,
      selectedColor: Theme.of(context).colorScheme.onSurfaceVariant,
      decoration: BoxDecoration(color: Colors.grey.shade200),
      items: taskTypeFilters.map((e) => MultiSelectItem(e, e)).toList(),
      title: Text(
        tr('task_type'),
        style: TextStyle(
            color: Theme.of(context).colorScheme.onSurfaceVariant,
            fontSize: 14.0),
      ),
      buttonText: Text(
        tr('task_type'),
        style: TextStyle(
            color: Theme.of(context).colorScheme.onSurfaceVariant,
            fontSize: 14.0),
      ),
      onConfirm: (values) {
        setState(() {
          selectedTaskTypes = values;
          filteredJobsByTaskType = mesterJobs
              .where((job) => selectedTaskTypes.contains(job.productname));
          filteredJobs = [];
          filteredJobs.addAll(filteredJobsByTaskType);
          filteredJobs.addAll(filteredJobsByLocation);
          filteredJobs.toSet().toList();
          if (filteredJobsByTaskType.isEmpty &&
              filteredJobsByLocation.isEmpty) {
            filteredJobs = mesterJobs;
          }
        });
      },
    );
  }

  // labor items setup
  @override
  Widget build(BuildContext context) {
    return Consumer<MesterPartnerProfileViewModel>(
      builder: (context, mesterVm, child) {
        return SingleChildScrollView(
          controller: scroll,
          physics: mesterVm.busy
              ? const NeverScrollableScrollPhysics()
              : const ClampingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                const MarketplaceHeader(),
                // Row(
                //   children: [
                //     Expanded(
                //       child: expandedButtonWidget(
                //         context,
                //         color: Theme.of(context).colorScheme.onSurfaceVariant,
                //         padding: const EdgeInsets.all(13),
                //         child: Row(
                //           mainAxisAlignment: MainAxisAlignment.center,
                //           children: [
                //             Text(
                //               tr('filter'),
                //               style: const TextStyle(
                //                   color: Colors.white, fontSize: 14.0),
                //             ),
                //             SmartGaps.gapW2,
                //             filtersHeight == 0
                //                 ? const Icon(Icons.expand_more,
                //                     color: Colors.white)
                //                 : const Icon(Icons.expand_less,
                //                     color: Colors.white),
                //           ],
                //         ),
                //         onTap: () => setState(() {
                //           if (mesterJobs.isNotEmpty) {
                //             filtersHeight = filtersHeight == 0 ? null : 0;
                //           }
                //         }),
                //       ),
                //     ),
                //   ],
                // ),
                AnimatedSize(
                  curve: Curves.easeIn,
                  duration: const Duration(milliseconds: 300),
                  child: SizedBox(
                      height: filtersHeight, child: filterFormWidget(context)),
                ),
                SmartGaps.gapH20,
                if (!mesterVm.busy && mesterVm.jobItems.isEmpty)
                  const Column(
                    children: [
                      SizedBox(height: 80),
                      Center(
                        child: EmptyListIndicator(),
                      ),
                    ],
                  )
                else
                  Skeletonizer(
                    enabled: mesterVm.busy,
                    child: ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: mesterVm.jobItems.length,
                      itemBuilder: (context, index) => PartnerItem(
                          indexPartner: mesterVm.jobItems.elementAt(index)),
                    ),
                  ),
                Opacity(
                  opacity: !mesterVm.busy ? 1 : 0,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 30, bottom: 10),
                    child: CustomNumberPaginator(
                      numberPages: mesterVm.lastPage,
                      height: 50,
                      initialPage: mesterVm.currentPage - 1,
                      buttonSelectedBackgroundColor:
                          PartnerAppColors.accentBlue,
                      buttonUnselectedForegroundColor: Colors.black,
                      onPageChange: (page) async {
                        //

                        if (mesterVm.fromNewsFeed) {
                          mesterVm.fromNewsFeed = false;
                        }

                        scroll.animateTo(0,
                            duration: const Duration(milliseconds: 800),
                            curve: Curves.easeOut);

                        mesterVm.currentPage = page + 1;
                        mesterVm.setBusy(true);
                        await tryCatchWrapper(
                          context: myGlobals.homeScaffoldKey!.currentContext,
                          function: mesterVm.getMesterJobs(getAll: true),
                        );

                        mesterVm.setBusy(false);

                        //
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
