import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/reservation_label.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class MarketplaceHeader extends StatelessWidget {
  const MarketplaceHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Text(tr('marketplace'),
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(height: 1)),
          ],
        ),
        SmartGaps.gapH6,
        Text(
          tr('marketplace_description'),
          style: Theme.of(context)
              .textTheme
              .titleMedium!
              .copyWith(height: 1.6, fontWeight: FontWeight.normal),
        ),
        SmartGaps.gapH20,
        const ReservationLabel(),
        SmartGaps.gapH12,
      ],
    );
  }
}
