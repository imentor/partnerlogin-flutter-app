import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/job/open_job_list.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/partner/mester_partners_list.dart';
import 'package:Haandvaerker.dk/ui/screens/newsfeed/latest_job_dialog.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/labor_task_toggle_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MarketPlaceScreen extends StatefulWidget {
  const MarketPlaceScreen({super.key, required this.isFromAppLink});
  final bool isFromAppLink;
  @override
  MarketPlaceScreenState createState() => MarketPlaceScreenState();
}

class MarketPlaceScreenState extends State<MarketPlaceScreen>
    with WidgetsBindingObserver {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      context.read<LaborTaskToggleViewModel>().setTask(true);
      final appDrawerVm = context.read<AppDrawerViewModel>();
      final newsFeedVm = context.read<NewsFeedViewModel>();

      const dynamicStoryPage = 'privatmarkedsplads';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      if (dynamicStoryEnable) {
        await Future.value(newsFeedVm.getDynamicStory(page: dynamicStoryPage))
            .whenComplete(() {
          if (!mounted) return;

          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      }

      if ((appDrawerVm.bitrixCompanyModel?.deadlineOffer ?? 0) == 1 &&
          newsFeedVm.latestJobFeedForPopup.isNotEmpty) {
        if (!mounted) return;

        latestJobDialog(context: context);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Consumer<LaborTaskToggleViewModel>(
        builder:
            (BuildContext context, LaborTaskToggleViewModel vm, Widget? child) {
          if (vm.isTask) {
            return MarketplaceJobSceen(
              isFromAppLink: widget.isFromAppLink,
            );
          } else {
            return const MesterPartnersList();
          }
        },
      ),
    );
  }
}
