import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/job_invitations_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AcceptDeclineInvitation extends StatelessWidget {
  const AcceptDeclineInvitation({
    super.key,
    required this.isAccept,
  });

  final bool isAccept;
  @override
  Widget build(BuildContext context) {
    final jobInvitationVm = context.watch<JobInvitationsViewModel>();
    return jobInvitationVm.isResponding == RequestResponse.loading
        ? AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            backgroundColor: Colors.white,
            titlePadding: EdgeInsets.zero,
            title: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 250,
              child: const Center(
                child: ConnectivityLoader(),
              ),
            ),
          )
        : jobInvitationVm.isResponding != RequestResponse.idle
            ? AlertDialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
                backgroundColor: Colors.white,
                titlePadding: EdgeInsets.zero,
                title: SizedBox(
                  height: 250,
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.check_circle_outline,
                        color: Theme.of(context).colorScheme.secondary,
                        size: 70,
                      ),
                      SmartGaps.gapH20,
                      Text(
                        tr('success!'),
                        style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                              fontSize: 24,
                              fontWeight: FontWeight.w700,
                            ),
                      ),
                      SmartGaps.gapH20,
                      CustomDesignTheme.flatButtonStyle(
                        backgroundColor:
                            Theme.of(context).colorScheme.secondary,
                        onPressed: () {
                          final clientsVm = context.read<ClientsViewModel>();
                          Navigator.pop(context);
                          changeDrawerReplaceRoute(Routes.yourClient);
                          if (clientsVm.fromNewsFeed) {
                            clientsVm.fromNewsFeed = false;
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          child: Text(
                            tr('close'),
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white,
                                  letterSpacing: 1.1,
                                ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            : AlertDialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
                backgroundColor: Colors.white,
                titlePadding: EdgeInsets.zero,
                title: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          const Spacer(),
                          IconButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            icon: Stack(
                              alignment: Alignment.center,
                              children: [
                                Container(
                                  width: 23,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color:
                                          Colors.black.withValues(alpha: 0.4),
                                      width: 2,
                                    ),
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                Icon(
                                  Icons.close,
                                  size: 18,
                                  color: Colors.black.withValues(alpha: 0.4),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Icon(Icons.info, color: Colors.yellow[700], size: 70),
                      SmartGaps.gapH10,
                      Text(
                        isAccept ? tr('do_accept_job') : tr('do_decline_job'),
                        style:
                            Theme.of(context).textTheme.headlineLarge!.copyWith(
                                  fontSize: 22,
                                  fontWeight: FontWeight.w700,
                                ),
                      ),
                      SmartGaps.gapH30,
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              child: CustomDesignTheme.flatButtonStyle(
                                height: 40,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  side: BorderSide(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .onTertiaryContainer),
                                ),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text(
                                  tr('no_thanks'),
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineLarge!
                                      .copyWith(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .onTertiaryContainer,
                                      ),
                                ),
                              ),
                            ),
                            SmartGaps.gapW20,
                            Expanded(
                              child: CustomDesignTheme.flatButtonStyle(
                                height: 40,
                                backgroundColor: PartnerAppColors.malachite,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                onPressed: () async {
                                  context
                                      .read<JobInvitationsViewModel>()
                                      .isResponding = RequestResponse.loading;
                                  await context
                                      .read<JobInvitationsViewModel>()
                                      .acceptDeclineInvitation(
                                          invitationId: context
                                              .read<JobInvitationsViewModel>()
                                              .currentInvitationId!,
                                          isAccept: isAccept ? 1 : 0);
                                },
                                child: Text(
                                  tr('yes_please'),
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineLarge!
                                      .copyWith(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white,
                                      ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SmartGaps.gapH30,
                    ],
                  ),
                ),
              );
  }
}
