import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/pagination/custom_pagination.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/components/expanded_section.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/job/job_item_card.dart';
import 'package:Haandvaerker.dk/utils/fake_data.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/job_invitations_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class JobInvitationsPage extends StatefulWidget {
  const JobInvitationsPage({super.key});

  @override
  State<JobInvitationsPage> createState() => _JobInvitationsPageState();
}

class _JobInvitationsPageState extends State<JobInvitationsPage> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback(
      (timeStamp) async {
        final jobInvitationsVm = context.read<JobInvitationsViewModel>();
        final newsFeedVm = context.read<NewsFeedViewModel>();
        final appDrawerVm = context.read<AppDrawerViewModel>();
        const dynamicStoryPage = 'pending';
        jobInvitationsVm.expandFilter = false;
        jobInvitationsVm.currentFilterStatus = 'pending';
        final dynamicStoryEnable =
            (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

        if (dynamicStoryEnable) {
          await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: Future.wait(
              [
                jobInvitationsVm.getInvitation(page: 1),
                newsFeedVm.getDynamicStory(page: dynamicStoryPage),
              ],
            ).whenComplete(() {
              if (!mounted) return;

              newsFeedVm.dynamicStoryFunction(
                  dynamicStoryPage: dynamicStoryPage,
                  dynamicStoryEnable: dynamicStoryEnable,
                  pageContext: context);
            }),
          );
        } else {
          await tryCatchWrapper(
              context: myGlobals.homeScaffoldKey!.currentContext,
              function: Future.wait([
                jobInvitationsVm.getInvitation(page: 1),
              ]));
        }
      },
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //

    final readJobsVm = context.read<JobInvitationsViewModel>();
    final watchJobsVm = context.watch<JobInvitationsViewModel>();

    //

    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //

          // HEADER
          const JobInvitationsHeader(),

          //FILTER
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: CustomDesignTheme.flatButtonStyle(
              backgroundColor: const Color(0xff133543),
              onPressed: () {
                if (watchJobsVm.invitationPagination.jobInvites != null) {
                  readJobsVm.expandFilter = !readJobsVm.expandFilter;
                }
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      tr("filter"),
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                    const Icon(
                      Icons.keyboard_arrow_down,
                      color: Colors.white,
                      size: 26,
                    )
                  ],
                ),
              ),
            ),
          ),
          if (watchJobsVm.invitationPagination.jobInvites != null)
            ExpandedSection(
              expand: watchJobsVm.expandFilter,
              child: Column(
                children: [
                  ...watchJobsVm.filterStatuses.map(
                    (e) => RadioListTile(
                      controlAffinity: ListTileControlAffinity.leading,
                      title: Text(
                        e.toUpperCase(),
                        style: Theme.of(context)
                            .textTheme
                            .bodyMedium!
                            .copyWith(height: 1.2),
                      ),
                      onChanged: (bool? value) {
                        if (value!) {
                          readJobsVm.currentFilterStatus = e;
                          readJobsVm.getInvitation(page: 1);
                          // readJobsVm.currentFileteredJobInvitation = readJobsVm
                          //     .jobInvitations
                          //     .where((element) =>
                          //         element.status!.toLowerCase() ==
                          //         e.toLowerCase())
                          //     .toList();
                        }
                      },
                      groupValue: watchJobsVm.currentFilterStatus == e,
                      value: true,
                    ),
                  ),
                ],
              ),
            ),

          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Consumer<JobInvitationsViewModel>(
                      builder: (context, jobInvitationsVm, child) {
                        final isLoading = jobInvitationsVm.busy ||
                            context.watch<TenderFolderViewModel>().busy;
                        if (jobInvitationsVm.invitationPagination.jobInvites ==
                                null &&
                            !jobInvitationsVm.busy) {
                          return const SizedBox(
                            height: 300,
                            child: Center(
                              child: EmptyListIndicator(),
                            ),
                          );
                        }

                        return Skeletonizer(
                          enabled: isLoading,
                          child: GridView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            gridDelegate:
                                const SliverGridDelegateWithFixedCrossAxisCount(
                              childAspectRatio: 0.65,
                              crossAxisCount: 2,
                              crossAxisSpacing: 13,
                            ),
                            itemCount: isLoading
                                ? 10
                                : jobInvitationsVm.invitationPagination
                                        .jobInvites?.length ??
                                    0,
                            itemBuilder: (context, index) {
                              if (isLoading) {
                                return JobItemCard(
                                    jobItem: fakeData.marketplaceItem);
                              } else {
                                final item = jobInvitationsVm
                                    .invitationPagination.jobInvites![index];

                                return JobItemCard(
                                  jobItem: item.project,
                                  invitationId: item.invitationId,
                                  status: item.status,
                                );
                              }
                            },
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          if (watchJobsVm.invitationPagination.lastPage != null &&
              watchJobsVm.invitationPagination.lastPage! > 1) ...[
            CustomNumberPaginator(
                numberPages: 1,
                height: 50,
                initialPage: 1,
                buttonSelectedBackgroundColor: PartnerAppColors.accentBlue,
                buttonUnselectedForegroundColor: Colors.black,
                onPageChange: (page) async {
                  readJobsVm.getInvitation(page: page);
                })
          ]
        ],
      ),
    );
  }
}

class JobInvitationsHeader extends StatelessWidget {
  const JobInvitationsHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //

          Text(
            tr('task_invitations'),
            style: Theme.of(context).textTheme.headlineLarge,
          ),

          if (context
                  .watch<JobInvitationsViewModel>()
                  .invitationPagination
                  .jobInvites !=
              null) ...[
            //

            SmartGaps.gapH20,

            Text(tr('task_invitations_desc'),
                style: Theme.of(context).textTheme.titleSmall),

            SmartGaps.gapH30,
          ],

          //
        ],
      ),
    );
  }
}
