import 'package:Haandvaerker.dk/viewmodel/marketplace/udbud_jobs_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SortFilter extends StatelessWidget {
  const SortFilter({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<UdbudJobsViewModel>(builder: (context, udbudJobsVM, _) {
      return Column(
        children: [
          RadioListTile(
            value: SortMode.city,
            groupValue: udbudJobsVM.sortMode,
            onChanged: udbudJobsVM.onSortModeChanged,
            title: Text(tr("sort_by_city")),
          ),
          RadioListTile(
            value: SortMode.createdDate,
            groupValue: udbudJobsVM.sortMode,
            onChanged: udbudJobsVM.onSortModeChanged,
            title: Text(tr("sort_by_created_date")),
          ),
          RadioListTile(
            value: SortMode.editDate,
            groupValue: udbudJobsVM.sortMode,
            onChanged: udbudJobsVM.onSortModeChanged,
            title: Text(tr("sort_by_edit_date")),
          ),
          RadioListTile(
            value: SortMode.deadlineDate,
            groupValue: udbudJobsVM.sortMode,
            onChanged: udbudJobsVM.onSortModeChanged,
            title: Text(tr("sort_by_date_deadline")),
          )
        ],
      );
    });
  }
}
