import 'package:Haandvaerker.dk/model/marketplace/udbud_response_model.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

String jamCalendarUrl = 'assets/images/jam-calendar.svg'; // used

class UdbudJobItem extends StatelessWidget {
  const UdbudJobItem({
    super.key,
    required this.udbudJobItem,
    required this.onTap,
  });

  final UdbudModel udbudJobItem;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: 10),
        child: Card(
            elevation: 4,
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: onTap,
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            flex: 2,
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.only(
                                        top: 8, right: 0, bottom: 0, left: 10),
                                    color: const Color(0xff5cd3d2),
                                    width: double.infinity,
                                    height: 50,
                                    child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            udbudJobItem.postalCode ?? '',
                                            overflow: TextOverflow.ellipsis,
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyMedium!
                                                .copyWith(
                                                    color: Colors.white,
                                                    height: 1.2),
                                          ),
                                          Expanded(
                                            child: Text(
                                              udbudJobItem.city ?? '',
                                              overflow: TextOverflow.ellipsis,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyMedium!
                                                  .copyWith(
                                                      color: Colors.white,
                                                      height: 1.2),
                                            ),
                                          )
                                        ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    child: Image.asset(
                                      "assets/images/marketplace/udbud_item_header.png",
                                      height: 90,
                                      width: double.infinity,
                                    ),
                                  ),
                                ])),
                        Expanded(
                            flex: 3,
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 10),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SmartGaps.gapH5,
                                    Container(
                                      width: double.infinity,
                                      alignment: Alignment.centerLeft,
                                      // job type
                                      child: Text(
                                        udbudJobItem.tenderTitle ?? '',
                                        maxLines: 3,
                                        overflow: TextOverflow.ellipsis,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyMedium!
                                            .copyWith(
                                                height: 1,
                                                fontWeight: FontWeight.w700),
                                      ),
                                    ),
                                    SizedBox(
                                      // height: 120,
                                      width: double.infinity,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          // job description
                                          Text(
                                            udbudJobItem
                                                    .procurementDescription ??
                                                '',
                                            overflow: TextOverflow.ellipsis,
                                            maxLines:
                                                udbudJobItem.tenderDeadline !=
                                                        null
                                                    ? 3
                                                    : 4,
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyMedium!
                                                .copyWith(fontSize: 13),
                                          ),
                                          SmartGaps.gapH10,
                                          if (udbudJobItem.tenderDeadline !=
                                              null)
                                            Row(
                                              children: <Widget>[
                                                SvgPicture.asset(
                                                  jamCalendarUrl,
                                                  width: 24.0,
                                                  height: 24.0,
                                                  colorFilter: ColorFilter.mode(
                                                    Theme.of(context)
                                                        .colorScheme
                                                        .onTertiaryContainer,
                                                    BlendMode.srcIn,
                                                  ),
                                                ),
                                                SmartGaps.gapW2,
                                                Text(
                                                  "${tr("deadline")}: ",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyMedium!
                                                      .copyWith(
                                                          fontSize: 13,
                                                          fontWeight:
                                                              FontWeight.w700),
                                                ),
                                              ],
                                            ),
                                          if (udbudJobItem.tenderDeadline !=
                                              null)
                                            Text(
                                              Formatter.formatDateStrings(
                                                  type: DateFormatType
                                                      .eventDateTime,
                                                  dateString: udbudJobItem
                                                      .tenderDeadline),
                                              overflow: TextOverflow.ellipsis,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyMedium!
                                                  .copyWith(
                                                      fontSize: 13,
                                                      fontWeight:
                                                          FontWeight.w700),
                                            )
                                        ],
                                      ),
                                    ),
                                  ]),
                            ))
                      ]),
                ),
              ],
            )));
  }
}
