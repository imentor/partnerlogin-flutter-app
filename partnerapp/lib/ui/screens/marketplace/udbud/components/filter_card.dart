import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class FilterCard extends StatelessWidget {
  const FilterCard({super.key, required this.onSort, required this.onFilter});
  final Function() onSort;
  final Function() onFilter;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
            child: CustomDesignTheme.flatButtonStyle(
                backgroundColor: const Color(0xff133543),
                onPressed: onSort,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      tr("sort"),
                      style: const TextStyle(color: Colors.white),
                    ),
                    const Icon(Icons.keyboard_arrow_down, color: Colors.white)
                  ],
                ))),
        SmartGaps.gapW10,
        Expanded(
          child: CustomDesignTheme.flatButtonStyle(
              backgroundColor: const Color(0xff133543),
              onPressed: onFilter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    tr("filter"),
                    style: const TextStyle(color: Colors.white),
                  ),
                  const Icon(Icons.keyboard_arrow_down, color: Colors.white)
                ],
              )),
        )
      ],
    );
  }
}
