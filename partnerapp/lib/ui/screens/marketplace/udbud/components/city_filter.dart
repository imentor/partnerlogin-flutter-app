import 'package:Haandvaerker.dk/viewmodel/marketplace/udbud_jobs_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CityFilter extends StatefulWidget {
  const CityFilter({super.key});

  @override
  CityFilterState createState() => CityFilterState();
}

class CityFilterState extends State<CityFilter> {
  bool? _selectAll = false;

  @override
  Widget build(BuildContext context) {
    return Consumer<UdbudJobsViewModel>(builder: (context, udbudJobsVM, _) {
      return Column(
        children: [
          CheckboxListTile(
              value: _selectAll,
              title: Text(tr("select_all")),
              onChanged: (val) {
                setState(() {
                  _selectAll = val;
                });
                udbudJobsVM.onSelectAllCity(val!);
              }),
          Expanded(
            child: ListView.builder(
                itemCount: udbudJobsVM.allUniqueCities.length,
                itemBuilder: (context, index) {
                  final city = udbudJobsVM.allUniqueCities.elementAt(index)!;
                  return CheckboxListTile(
                    value: udbudJobsVM.selectedCities.contains(city),
                    onChanged: (val) {
                      udbudJobsVM.onCityFilter(city);
                    },
                    title: Text(city),
                  );
                }),
          ),
        ],
      );
    });
  }
}
