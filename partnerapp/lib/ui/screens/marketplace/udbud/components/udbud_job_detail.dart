import 'package:Haandvaerker.dk/model/marketplace/udbud_response_model.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

const marketPlaceImageDir = "assets/images/marketplace";

class UdbudJobDetail extends StatelessWidget {
  const UdbudJobDetail({
    super.key,
    required this.udbudJobItem,
  });

  final UdbudModel udbudJobItem;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 8),
              child: ListView(children: [
                SmartGaps.gapH20,
                Image.asset("$marketPlaceImageDir/udbud_item_header.png",
                    height: 100, width: double.infinity),
                Container(
                    alignment: Alignment.centerLeft,
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    color: const Color(0xff00d8d6),
                    width: double.infinity,
                    height: 60,
                    child: Text(
                        "${udbudJobItem.postalCode ?? ""} ${udbudJobItem.city ?? ""}",
                        style: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 18))),
                SmartGaps.gapH5,
                Container(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SmartGaps.gapH20,
                        Row(
                          children: <Widget>[
                            const Icon(Icons.calendar_today),
                            SmartGaps.gapW10,
                            Text(tr('call_and_schedule'),
                                overflow: TextOverflow.ellipsis,
                                style:
                                    Theme.of(context).textTheme.headlineSmall)
                          ],
                        ),
                        SmartGaps.gapH20,
                        //Published
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                                flex: 2,
                                child: Text("${tr("published")}: ",
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleSmall)),
                            Expanded(
                                flex: 3,
                                child: Text(
                                    Formatter.formatDateStrings(
                                        type: DateFormatType.eventDateTime,
                                        dateString: udbudJobItem.dateCreated),
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleSmall)),
                          ],
                        ),
                        SmartGaps.gapH10,
                        //Deadline
                        if (udbudJobItem.tenderDeadline != null)
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                  flex: 2,
                                  child: Text('${tr("deadline")}: ',
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleSmall)),
                              Expanded(
                                  flex: 3,
                                  child: Text(
                                      Formatter.formatDateStrings(
                                          type: DateFormatType.eventDateTime,
                                          dateString:
                                              udbudJobItem.tenderDeadline),
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleSmall)),
                            ],
                          ),
                        //Location
                        SmartGaps.gapH10,
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                                flex: 2,
                                child: Text('${tr("location")}: ',
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleSmall)),
                            Expanded(
                                flex: 3,
                                child: Text(
                                    "${udbudJobItem.postalCode ?? ""} ${udbudJobItem.city ?? ""}",
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleSmall)),
                          ],
                        ),
                        //Title
                        SmartGaps.gapH10,
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                                flex: 2,
                                child: Text('${tr("title")}: ',
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleSmall)),
                            Expanded(
                                flex: 3,
                                child: Text("${udbudJobItem.tenderTitle}",
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleSmall)),
                          ],
                        ),
                        //Description
                        SmartGaps.gapH10,
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                                flex: 2,
                                child: Text('${tr("description")}: ',
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleSmall)),
                            Expanded(
                                flex: 3,
                                child: Text(
                                    "${udbudJobItem.procurementDescription}",
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleSmall)),
                          ],
                        ),
                        SmartGaps.gapH20,
                        // Bid Button
                        InkWell(
                          onTap: () async {
                            try {
                              final url =
                                  Uri.encodeFull(udbudJobItem.formUrlId!);
                              if (!await launchUrl(Uri.parse(url))) {
                                throw 'Could not launch $url';
                              }
                            } catch (e) {
                              // Scaffold.of(context)
                              //     .showSnackBar(SnackBar(content: Text("$e")));
                            }
                          },
                          child: Container(
                            height: 70,
                            width: double.infinity,
                            color: const Color(0xff00DA58),
                            child: Center(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "${tr("see_more_at")} udbud.dk",
                                  style: const TextStyle(
                                      fontSize: 24,
                                      color: Colors.white,
                                      height: 1.3,
                                      fontWeight: FontWeight.w700),
                                ),
                                const Icon(Icons.arrow_forward,
                                    color: Colors.white),
                              ],
                            )),
                          ),
                        ),
                        SmartGaps.gapH30,
                      ],
                    )),
              ]),
            ),
            // Close Button on top right
            Positioned(
              right: 10,
              top: 0,
              child: Opacity(
                opacity: 0.7,
                child: Container(
                    padding: const EdgeInsets.all(5),
                    decoration: const BoxDecoration(
                      color: Colors.black,
                      shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black45,
                          blurRadius: 2.0,
                          spreadRadius: 0.0,
                          offset: Offset(2.0, 2.0),
                        ),
                      ],
                    ),
                    child: InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.close,
                          color: Colors.grey[200],
                        ))),
              ),
            )
          ],
        ),
      ),
    );
  }
}
