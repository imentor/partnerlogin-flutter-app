import 'package:Haandvaerker.dk/model/marketplace/udbud_response_model.dart';
import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/udbud/components/udbud_job_detail.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/udbud/components/udbud_job_item.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/udbud_jobs_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class UdbudJobList extends StatefulWidget {
  const UdbudJobList({super.key});

  @override
  UdbudJobListState createState() => UdbudJobListState();
}

class UdbudJobListState extends State<UdbudJobList> {
  Widget _jobListView(List<UdbudModel> jobs) {
    if (jobs.isEmpty) {
      return Center(child: empty());
    } else {
      return ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: jobs.length,
          itemBuilder: (context, index) {
            final job = jobs.elementAt(index);
            return UdbudJobItem(
              udbudJobItem: job,
              onTap: () {
                Navigator.of(context, rootNavigator: true)
                    .push(MaterialPageRoute<void>(
                        builder: (BuildContext context) {
                          return UdbudJobDetail(
                            udbudJobItem: job,
                          );
                        },
                        fullscreenDialog: true));
              },
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UdbudJobsViewModel>(
      builder: (BuildContext context, UdbudJobsViewModel udbudJobsVM,
          Widget? child) {
        return udbudJobsVM.busy
            ? const Center(child: ConnectivityLoader())
            : _jobListView(udbudJobsVM.filteredJobs!);
      },
    );
  }
}
