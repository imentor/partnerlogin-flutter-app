import 'dart:async';

import 'package:Haandvaerker.dk/ui/components/custom_textfield.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/udbud/components/city_filter.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/udbud/components/filter_card.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/udbud/components/sort_filter.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/udbud/components/udbud_job_list.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/udbud_jobs_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class UdbudScreen extends StatefulWidget {
  const UdbudScreen({super.key});

  @override
  UdbudScreenState createState() => UdbudScreenState();
}

class UdbudScreenState extends State<UdbudScreen> {
  final TextEditingController _searchController = TextEditingController();
  Timer? _debounce;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      getUdbudJobs();
    });
  }

  Future<void> getUdbudJobs() async {
    context.read<UdbudJobsViewModel>().getUdbudJobs();
  }

  @override
  void dispose() {
    super.dispose();
    _searchController.dispose();
    _debounce?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Container(
        padding:
            const EdgeInsets.only(top: 20, right: 15, bottom: 20, left: 15),
        child: Consumer<UdbudJobsViewModel>(builder: (context, udbudJobsVM, _) {
          return RefreshIndicator(
            onRefresh: () async {},
            child: ListView(children: [
              Row(children: [
                Text(tr('udbud'),
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium!
                        .copyWith(height: 1)),
              ]),
              SmartGaps.gapH15,
              // description text
              Text(tr('udbud_description'),
                  style: Theme.of(context).textTheme.titleSmall),
              SmartGaps.gapH10,
              _buildSearchTextField(context, udbudJobsVM),
              SmartGaps.gapH5,
              if (udbudJobsVM.busy == false)
                FilterCard(
                  onSort: () {
                    showFilterDialog(context, const SortFilter());
                  },
                  onFilter: () {
                    showFilterDialog(context, const CityFilter());
                  },
                ),
              SmartGaps.gapH5,
              if (udbudJobsVM.filteredJobs!.isNotEmpty)
                Text(
                  "${udbudJobsVM.filteredJobs!.length} ${tr(udbudJobsVM.filteredJobs!.length > 1 ? "items_found" : "item_found")}",
                  style: Theme.of(context).textTheme.bodySmall,
                ),
              const UdbudJobList()
            ]),
          );
        }),
      ),
    );
  }

  Future<void> showFilterDialog(BuildContext context, Widget content) async {
    Dialog simpleDialog = Dialog(
      child: Stack(
        children: [
          Container(
            margin: const EdgeInsets.only(
                left: 20.0, right: 20.0, top: 20, bottom: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[SmartGaps.gapH20, Expanded(child: content)],
            ),
          ),
          Positioned(
            right: 5,
            top: 5,
            child: Opacity(
              opacity: 0.7,
              child: Container(
                padding: const EdgeInsets.all(5),
                decoration: const BoxDecoration(
                  color: Colors.black,
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      blurRadius: 2.0,
                      spreadRadius: 0.0,
                      offset: Offset(2.0, 2.0),
                    ),
                  ],
                ),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context, rootNavigator: true).pop();
                  },
                  child: Icon(
                    Icons.close,
                    color: Colors.grey[200],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );

    return await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => simpleDialog);
  }

  SearchTextField _buildSearchTextField(
      BuildContext context, UdbudJobsViewModel udbudJobsVM) {
    return SearchTextField(
      controller: _searchController,
      hintText: tr('search_city_title'),
      onChange: (text) {
        if (_debounce?.isActive ?? false) _debounce!.cancel();
        _debounce = Timer(const Duration(milliseconds: 200), () {
          udbudJobsVM.onSearchKeyChanged(_searchController.text.trim());
        });
      },
      onClear: () {
        if (_debounce?.isActive ?? false) _debounce!.cancel();
        _debounce = Timer(const Duration(milliseconds: 200), () {
          udbudJobsVM.onSearchKeyChanged(_searchController.text.trim());
        });
      },
      onSubmit: (String text) {},
    );
  }
}
