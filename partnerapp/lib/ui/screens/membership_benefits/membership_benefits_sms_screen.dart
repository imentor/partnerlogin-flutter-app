import 'dart:io';

import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/membership_benefits/contact_selector.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class MembershipBenefitsSmsScreen extends StatefulWidget {
  const MembershipBenefitsSmsScreen({super.key});

  @override
  MembershipBenefitsSmsScreenState createState() =>
      MembershipBenefitsSmsScreenState();
}

class MembershipBenefitsSmsScreenState
    extends State<MembershipBenefitsSmsScreen> {
  String? key;

  @override
  initState() {
    key = '';
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      // final apiResponse =
      //     await Provider.of<MembershipBenefitsViewModel>(context, listen: false)
      //         .getPartnerKey();

      // if (!mounted) return;
      // setState(() {
      //   key = apiResponse.key;
      // });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(bottom: 20, left: 20, right: 20),
          child: Column(
              children: [_DescriptionCard(), SmartGaps.gapH10, _MessageCard()]),
        ),
      ),
    );
  }
}

class _DescriptionCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _CardContainer(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(tr('recommend_us_to_a_friend'),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.titleMedium!.apply(
                  color: Theme.of(context).colorScheme.onSurfaceVariant)),
          SmartGaps.gapH10,
          Text(tr('both_parties_get_months_free'),
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium!
                  .apply(color: Colors.black))
        ],
      ),
    );
  }
}

class _MessageCard extends StatefulWidget {
  @override
  _MessageCardState createState() => _MessageCardState();
}

class _MessageCardState extends State<_MessageCard> {
  final TextEditingController _number = TextEditingController();
  final TextEditingController _body = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final userVm = context.read<UserViewModel>();

      final String referralLink =
          'https://www.haandvaerker.dk/business/referral/${userVm.userModel?.user?.id ?? 0}';
      _body.text = '${tr('membership_benefits_sms_body')}. $referralLink';
    });
  }

  @override
  Widget build(BuildContext context) {
    return _CardContainer(
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ContactSelector(
              onContactChanged: (value) {
                if (value != null) {
                  _number.text = value.phones.elementAt(0).number;
                } else {
                  _number.text = '';
                }
              },
            ),
            SmartGaps.gapH10,
            Text(tr('enter_friends_number'),
                style: Theme.of(context).textTheme.titleSmall),
            SmartGaps.gapH10,
            TextFormField(
                keyboardType: TextInputType.number,
                controller: _number,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    filled: true,
                    fillColor: Colors.grey[200],
                    hintStyle: Theme.of(context)
                        .textTheme
                        .bodyMedium!
                        .copyWith(fontWeight: FontWeight.w500),
                    hintText: tr('phone_number')),
                validator: (value) {
                  if (value!.isEmpty) {
                    return tr('please_enter_valid_mobile_number');
                  }
                  return null;
                }),
            SmartGaps.gapH10,
            TextFormField(
                maxLines: 10,
                controller: _body,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    filled: true,
                    fillColor: Colors.grey[200],
                    hintStyle: Theme.of(context)
                        .textTheme
                        .bodyMedium!
                        .copyWith(fontWeight: FontWeight.w500),
                    hintText: ''),
                validator: (value) {
                  if (value!.isEmpty) {
                    return tr('please_enter_some_text');
                  }
                  return null;
                }),
            SmartGaps.gapH10,
            ButtonTheme(
              minWidth: double.infinity,
              child: CustomDesignTheme.flatButtonStyle(
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    final phoneNumber = _number.text;
                    final bodyMessage = _body.text;
                    String uri = 'sms:$phoneNumber?body=$bodyMessage';
                    if (Platform.isIOS) {
                      uri = 'sms:$phoneNumber&body=$bodyMessage';
                    }
                    if (await canLaunchUrl(Uri.parse(Uri.encodeFull(uri)))) {
                      await launchUrl(Uri.parse(Uri.encodeFull(uri)));
                    }
                  }
                },
                padding: const EdgeInsets.symmetric(vertical: 15),
                backgroundColor: Theme.of(context).colorScheme.secondary,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(tr('send_sms'),
                        style: Theme.of(context)
                            .textTheme
                            .labelMedium!
                            .apply(color: Colors.white, heightFactor: 1)),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _CardContainer extends StatelessWidget {
  const _CardContainer({this.child});

  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.all(20),
        child: child,
      ),
    );
  }
}
