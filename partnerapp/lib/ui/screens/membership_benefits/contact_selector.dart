import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/extension/snackbar.dart';
import 'package:Haandvaerker.dk/utils/extensions.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fast_contacts/fast_contacts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:permission_handler/permission_handler.dart';

class ContactSelector extends StatefulWidget {
  const ContactSelector({super.key, required this.onContactChanged});

  final Function(Contact?) onContactChanged;

  @override
  ContactSelectorState createState() => ContactSelectorState();
}

class ContactSelectorState extends State<ContactSelector> {
  Contact? selectedItem;
  bool isGrantedContactPermission = false;

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await determinePermission();
    });
  }

  Future<bool> determinePermission() async {
    isGrantedContactPermission = await Permission.contacts.status.isGranted;
    if (!mounted) return false;
    setState(() {});
    return isGrantedContactPermission;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _getContacts(),
    );
  }

  Widget _getContacts() {
    return isGrantedContactPermission
        ? FutureBuilder(
            initialData: const [],
            future: refreshContacts(),
            builder: (context, snapshot) {
              if (snapshot.connectionState != ConnectionState.done) {
                return Center(child: loader());
              } else if (snapshot.hasData) {
                if (snapshot.data is! List<Contact>) {
                  context.showErrorSnackBar();
                  return Container();
                }

                var items = snapshot.data as List<Contact>;

                if (items.isEmpty) {
                  return Text(
                    tr('no_data'),
                    style: Theme.of(context).textTheme.headlineSmall,
                  );
                }

                return DropdownSearch<Contact>(
                  selectedItem: selectedItem,
                  items: (filter, loadProps) => items,
                  // showSearchBox: true,
                  // showClearButton: true,
                  // showSelectedItem: true,
                  compareFn: (Contact i, Contact s) {
                    return identical(i, s);
                  },
                  // searchBoxDecoration: InputDecoration(
                  //   floatingLabelBehavior: FloatingLabelBehavior.never,
                  //   border: InputBorder.none,
                  //   filled: true,
                  //   fillColor: Colors.grey[200],
                  //   labelText: tr('search_from_your_contacts'),
                  // ),
                  // label: tr('choose_from_your_contacts'),
                  filterFn: (item, filter) => getCriteria(item, filter),
                  onChanged: (Contact? data) {
                    widget.onContactChanged(data);
                  },
                  // dropdownBuilder: _customDropDownExample,
                  // popupItemBuilder: _customPopupItemBuilderExample2,
                );
              } else if (snapshot.hasError) {
                context.showErrorSnackBar();
                return const EmptyListIndicator();
              } else {
                return const EmptyListIndicator();
              }
            },
          )
        : Center(
            child: Text(tr('please_enable_contact_permission'),
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .titleSmall!
                    .copyWith(color: Colors.red)));
  }

  bool getCriteria(Contact item, String filter) {
    return item.displayName
            .displayEmptyIfNull()
            .toLowerCase()
            .contains(filter.toLowerCase()) ||
        item.structuredName!.familyName
            .displayEmptyIfNull()
            .toLowerCase()
            .contains(filter.toLowerCase()) ||
        item.structuredName!.displayName
            .displayEmptyIfNull()
            .toLowerCase()
            .contains(filter.toLowerCase()) ||
        item.structuredName!.middleName
            .displayEmptyIfNull()
            .toLowerCase()
            .contains(filter.toLowerCase()) ||
        item.phones
            .where((element) =>
                element.number.toLowerCase().contains(filter.toLowerCase()))
            .isNotEmpty;
  }

  // Widget _customDropDownExample(
  //     BuildContext context, Contact item, String itemDesignation) {
  //   return Container(
  //     child: (item == null)
  //         ? ListTile(
  //             contentPadding: const EdgeInsets.all(0),
  //             title: Text(tr('no_item_selected')),
  //           )
  //         : ListTile(
  //             contentPadding: const EdgeInsets.all(0),
  //             title: Text(
  //                 '${item.structuredName!.givenName} ${item.structuredName!.familyName}',
  //                 style: Theme.of(context).textTheme.headlineSmall),
  //             //subtitle: getSubtitle(item),
  //           ),
  //   );
  // }

  // Widget _customPopupItemBuilderExample2(
  //     BuildContext context, Contact item, bool isSelected) {
  //   return Container(
  //     margin: const EdgeInsets.only(bottom: 10),
  //     decoration: !isSelected
  //         ? null
  //         : BoxDecoration(
  //             border: Border.all(color: Theme.of(context).primaryColor),
  //             borderRadius: BorderRadius.circular(0),
  //             color: Colors.white,
  //           ),
  //     child: Padding(
  //       padding: const EdgeInsets.only(bottom: 8.0),
  //       child: ListTile(
  //         selected: isSelected,
  //         title: Padding(
  //           padding: const EdgeInsets.only(bottom: 8.0),
  //           child: Text(
  //               '${item.structuredName!.givenName} ${item.structuredName!.familyName}',
  //               style: Theme.of(context).textTheme.headlineSmall),
  //         ),
  //         subtitle: getSubtitle(item),
  //       ),
  //     ),
  //   );
  // }

  Widget getSubtitle(Contact item) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            // email
            SvgPicture.asset(
              SvgIcons.sendIconUrl,
              width: 24,
              height: 24,
              colorFilter: ColorFilter.mode(
                Theme.of(context).colorScheme.onSurfaceVariant,
                BlendMode.srcIn,
              ),
            ),
            SmartGaps.gapW12,
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(),
                for (var item in item.emails)
                  Flexible(
                    child: Text(item.address,
                        softWrap: true,
                        style: Theme.of(context).textTheme.titleSmall,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis),
                  ),
              ],
            ),
          ],
        ),
        SmartGaps.gapH5,
        Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SvgPicture.asset(
              SvgIcons.phoneIconUrl,
              width: 24,
              height: 24,
              colorFilter: ColorFilter.mode(
                Theme.of(context).colorScheme.onSurfaceVariant,
                BlendMode.srcIn,
              ),
            ),
            SmartGaps.gapW12,
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(),
                for (var item in item.phones)
                  Text(item.number,
                      style: Theme.of(context).textTheme.titleSmall),
              ],
            ),
          ],
        ),
      ],
    );
  }

  Future<List<Contact>> refreshContacts() async {
    // Load without thumbnails initially.
    var contacts = await FastContacts.getAllContacts();

    return contacts;
  }
}
