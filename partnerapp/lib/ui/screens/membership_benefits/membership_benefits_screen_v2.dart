import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/membership/membership_benefits_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class MembershipBenefitsScreenV2 extends StatefulWidget {
  const MembershipBenefitsScreenV2({super.key});

  @override
  MembershipBenefitsScreenV2State createState() =>
      MembershipBenefitsScreenV2State();
}

class MembershipBenefitsScreenV2State
    extends State<MembershipBenefitsScreenV2> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final newsFeedVm = context.read<NewsFeedViewModel>();
      final appDrawerVm = context.read<AppDrawerViewModel>();
      const dynamicStoryPage = 'medlemsfordele';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      if (dynamicStoryEnable) {
        await tryCatchWrapper(
          context: myGlobals.homeScaffoldKey!.currentContext,
          function: Future.wait([
            context.read<MembershipBenefitsViewModel>().getMembersV2(),
            newsFeedVm.getDynamicStory(page: dynamicStoryPage),
          ]),
        ).whenComplete(() {
          if (!mounted) return;

          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      } else {
        await tryCatchWrapper(
          context: myGlobals.homeScaffoldKey!.currentContext,
          function: Future.value(
              context.read<MembershipBenefitsViewModel>().getMembersV2()),
        );
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // TextButton(
            //     style: TextButton.styleFrom(
            //         backgroundColor: PartnerAppColors.malachite),
            //     onPressed: () async {
            //       changeDrawerRoute(Routes.membershipBenefitsSms);
            //     },
            //     child: Row(
            //       mainAxisSize: MainAxisSize.min,
            //       children: [
            //         Text(
            //           tr('recommend_and_get_months_free'),
            //           style: Theme.of(context)
            //               .textTheme
            //               .titleSmall!
            //               .copyWith(color: Colors.white),
            //         ),
            //         SmartGaps.gapW10,
            //         const Icon(
            //           Icons.arrow_forward,
            //           color: Colors.white,
            //         )
            //       ],
            //     )),
            // SmartGaps.gapH20,
            Text(tr('membership_benefits'),
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(height: 1)),
            SmartGaps.gapH12,
            Text(
              tr('membership_benefits_desc'),
              style:
                  Theme.of(context).textTheme.titleSmall!.copyWith(height: 1.6),
            ),
            SmartGaps.gapH20,
            Consumer<MembershipBenefitsViewModel>(builder: (_, vm, __) {
              if (vm.busy) {
                return const Center(
                  child: ConnectivityLoader(),
                );
              }
              return ListView.separated(
                  itemCount: vm.memberships.length,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  separatorBuilder: (context, index) {
                    return SmartGaps.gapH20;
                  },
                  itemBuilder: (context, index) {
                    final membership = vm.memberships[index];

                    return Card(
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: 100,
                              child: membership.propertyImageUrlValue!
                                      .contains('svg')
                                  ? SvgPicture.network(
                                      membership.propertyImageUrlValue!,
                                      fit: BoxFit.contain,
                                    )
                                  : CachedNetworkImage(
                                      imageUrl:
                                          membership.propertyImageUrlValue!,
                                      fit: BoxFit.contain,
                                    ),
                            ),
                            SmartGaps.gapH10,
                            HtmlWidget(
                              membership.propertyDescriptionValue!.text!,
                              textStyle: Theme.of(context).textTheme.titleSmall,
                            ),
                            // Text(
                            //   membership.propertyDescriptionValue!.text!,
                            //   style: Theme.of(context).textTheme.titleSmall,
                            // ),
                            if (membership.propertyCodeValue != null) ...[
                              SmartGaps.gapH10,
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  RichText(
                                      text: TextSpan(children: [
                                    TextSpan(
                                        text: '${tr('promotion_code')}: ',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleSmall),
                                    TextSpan(
                                        text: membership.propertyCodeValue,
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleSmall!
                                            .copyWith(
                                                color: PartnerAppColors.blue,
                                                fontWeight: FontWeight.bold))
                                  ])),
                                  TextButton(
                                      style: TextButton.styleFrom(
                                          backgroundColor:
                                              PartnerAppColors.blue),
                                      onPressed: () async {
                                        await Clipboard.setData(ClipboardData(
                                            text:
                                                membership.propertyCodeValue!));
                                      },
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text(
                                            tr('copy_code'),
                                            style: Theme.of(context)
                                                .textTheme
                                                .titleSmall!
                                                .copyWith(color: Colors.white),
                                          ),
                                          SmartGaps.gapW10,
                                          const Icon(
                                            Icons.copy_rounded,
                                            color: Colors.white,
                                          )
                                        ],
                                      ))
                                ],
                              ),
                              SmartGaps.gapH10,
                            ] else
                              SmartGaps.gapH10,
                            if (membership.propertyContactNameValue !=
                                null) ...[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(tr('contact'),
                                      style: Theme.of(context)
                                          .textTheme
                                          .headlineMedium!
                                          .copyWith(height: 1)),
                                  GestureDetector(
                                    onTap: () async {
                                      await launchUrl(Uri.parse(
                                          'tel:${membership.propertyContactPhoneValue}'));
                                    },
                                    child: Icon(ElementIcons.phone_outline,
                                        size: 20,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary),
                                  )
                                ],
                              ),
                              SmartGaps.gapH10,
                              Row(
                                children: [
                                  SizedBox(
                                    height: 100,
                                    width: 80,
                                    child: membership.propertyContactPhotoValue!
                                            .contains('svg')
                                        ? SvgPicture.network(
                                            membership
                                                    .propertyContactPhotoValue ??
                                                '',
                                            fit: BoxFit.fill,
                                          )
                                        : CachedNetworkImage(
                                            imageUrl: membership
                                                    .propertyContactPhotoValue ??
                                                '',
                                            fit: BoxFit.fill,
                                          ),
                                  ),
                                  SmartGaps.gapW10,
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        membership.propertyContactNameValue ??
                                            '',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleSmall,
                                      ),
                                      SmartGaps.gapH5,
                                      Text(
                                        'Tel: ${membership.propertyContactPhoneValue ?? ''}',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleSmall,
                                      ),
                                      SmartGaps.gapH5,
                                      Text(
                                        membership.propertyContactEmailValue ??
                                            '',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleSmall,
                                      )
                                    ],
                                  )
                                ],
                              )
                            ] else ...[
                              TextButton(
                                  style: TextButton.styleFrom(
                                      backgroundColor:
                                          PartnerAppColors.malachite),
                                  onPressed: () async {
                                    await launchUrl(Uri.parse(
                                        membership.propertyLinkValue ?? ''));
                                  },
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text(
                                        membership.propertyButtonValue ?? '',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleSmall!
                                            .copyWith(color: Colors.white),
                                      ),
                                      SmartGaps.gapW10,
                                      const Icon(
                                        Icons.arrow_forward,
                                        color: Colors.white,
                                      )
                                    ],
                                  ))
                            ]
                          ],
                        ),
                      ),
                    );
                  });
            })
          ],
        ),
      ),
    );
  }
}
