import 'package:Haandvaerker.dk/services/main/main_service.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({super.key});

  @override
  SettingsScreenState createState() => SettingsScreenState();
}

class SettingsScreenState extends State<SettingsScreen> {
  List<bool>? isSelected;
  final _languages = [Language.english, Language.danish];
  String? _selectedLanguage;

  @override
  void initState() {
    isSelected = [true, false];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SafeArea(
        top: true,
        bottom: true,
        child: Container(
          height: double.infinity,
          margin: const EdgeInsets.only(
              left: 20.0, right: 20.0, top: 20, bottom: 20),
          decoration: BoxDecoration(color: Colors.white, boxShadow: [
            BoxShadow(
                color: Colors.black.withValues(alpha: .1), blurRadius: 8.0)
          ]),
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Container(
              margin: const EdgeInsets.only(
                  left: 20.0, right: 20.0, top: 20, bottom: 10),
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          tr('settings'),
                          style: Theme.of(context).textTheme.headlineSmall,
                        ),
                        const Spacer(),
                      ],
                    ),
                    SmartGaps.gapH20,
                    Padding(
                      padding: const EdgeInsets.only(left: 17),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(ElementIcons.reading,
                              color: Theme.of(context).colorScheme.primary,
                              size: 18),
                          SmartGaps.gapW20,
                          DropdownButtonHideUnderline(
                            child: DropdownButton(
                              hint: Text(
                                tr('select_a_language'),
                                style: Theme.of(context)
                                    .textTheme
                                    .titleSmall!
                                    .copyWith(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                              ),
                              icon: Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, right: 8.0),
                                child: Icon(ElementIcons.arrow_down,
                                    size: 16,
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                              value: _selectedLanguage,
                              onChanged: (dynamic newValue) async {
                                final mainService = context.read<MainService>();
                                modalManager.showLoadingModal();
                                setState(() {
                                  _selectedLanguage = newValue;
                                });

                                if (_selectedLanguage == Language.danish) {
                                  await context
                                      .setLocale(const Locale('da', 'DK'));
                                } else {
                                  await context.setLocale(const Locale('en'));
                                }

                                // ignore: use_build_context_synchronously
                                mainService
                                    .setLanguage(language: newValue)
                                    .whenComplete(() {
                                  if (mounted) {
                                    modalManager.hideLoadingModal();
                                  }
                                });
                              },
                              items: _languages
                                  .map(
                                    (language) => DropdownMenuItem(
                                      value: language,
                                      child: Text(
                                        language,
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleSmall,
                                      ),
                                    ),
                                  )
                                  .toList(),
                            ),
                          )
                        ],
                      ),
                    )
                  ]),
            ),
          ),
        ),
      ),
    );
  }
}
