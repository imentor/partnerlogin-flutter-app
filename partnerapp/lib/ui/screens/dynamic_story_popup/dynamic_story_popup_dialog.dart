import 'package:Haandvaerker.dk/model/dynamic_story_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';
import 'package:story_view/story_view.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

Future<void> dynamicStoryPopupDialog({
  required BuildContext context,
}) {
  return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return const Dialog.fullscreen(
          child: DynamicStoryPopupDialog(),
        );
      });
}

class DynamicStoryPopupDialog extends StatefulWidget {
  const DynamicStoryPopupDialog({super.key});

  @override
  State<DynamicStoryPopupDialog> createState() =>
      _DynamicStoryPopupDialogState();
}

class _DynamicStoryPopupDialogState extends State<DynamicStoryPopupDialog> {
  late YoutubePlayerController videoController;
  late StoryController storyController;
  late bool popupMounted;

  @override
  void initState() {
    storyController = StoryController();
    videoController = YoutubePlayerController(
      initialVideoId: '',
    );
    popupMounted = false;
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (mounted) {
        final nfvm = context.read<NewsFeedViewModel>();
        nfvm.redirectUrl = nfvm.dynamicStory?.storyItems?[0].redirectUrl ?? '';
        popupMounted = true;
      }
    });
  }

  void onTap({required String redirectUrl}) async {
    await launchUrl(Uri.parse(redirectUrl)).whenComplete(() {
      if (!mounted) return;
      Navigator.of(context).pop();
    });
  }

  @override
  Widget build(BuildContext context) {
    final newsFeedVm = context.read<NewsFeedViewModel>();
    final storyList =
        newsFeedVm.dynamicStory?.storyItems ?? <StoryItemsModel>[];

    newsFeedVm.setStoryItems(
        storyList: storyList,
        videoController: videoController,
        storyController: storyController);

    return Stack(
      children: <Widget>[
        StoryView(
          storyItems: [...newsFeedVm.storyItems],
          controller: storyController,
          repeat: true,
          onStoryShow: (storyItem, index) {
            if (popupMounted) {
              newsFeedVm.redirectUrl =
                  storyList[newsFeedVm.storyItems.indexOf(storyItem)]
                          .redirectUrl ??
                      '';
            }
          },
        ),
        Positioned(
            top: 25.0,
            right: 15.0,
            child: InkWell(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Container(
                height: 30,
                width: 30,
                decoration: const BoxDecoration(
                    shape: BoxShape.circle, color: PartnerAppColors.grey1),
                child: const Center(
                  child: Icon(
                    FeatherIcons.x,
                    color: Colors.white,
                  ),
                ),
              ),
            )),
        Consumer<NewsFeedViewModel>(builder: (_, nfvm, __) {
          return nfvm.redirectUrl.isNotEmpty
              ? Align(
                  alignment: Alignment.bottomCenter,
                  child: InkWell(
                    onTap: () => onTap(redirectUrl: newsFeedVm.redirectUrl),
                    child: Container(
                      height: 50,
                      width: 150,
                      margin: const EdgeInsets.only(bottom: 10),
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          shape: BoxShape.rectangle,
                          color: PartnerAppColors.malachite),
                      child: Center(
                        child: Text(
                          tr('learn_more'),
                          style: Theme.of(context)
                              .textTheme
                              .displayLarge!
                              .copyWith(
                                fontSize: 18,
                                fontWeight: FontWeight.normal,
                                color: Colors.white,
                              ),
                        ),
                      ),
                    ),
                  ))
              : Container();
        }),
      ],
    );
  }

  @override
  void dispose() {
    videoController.dispose();
    storyController.dispose();
    super.dispose();
  }
}

class TextStoryItem extends StatelessWidget {
  const TextStoryItem(
      {super.key,
      required this.storyTitle,
      required this.storyDescription,
      required this.controllerId});
  final String storyTitle;
  final String storyDescription;
  final String controllerId;

  @override
  Widget build(BuildContext context) {
    return Container(
      key: ValueKey(controllerId),
      color: PartnerAppColors.darkBlue,
      child: Center(
        child: Text.rich(
          textAlign: TextAlign.start,
          TextSpan(
            children: [
              TextSpan(
                text: '$storyTitle\n\n',
                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
              ),
              TextSpan(
                text: storyDescription,
                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      color: Colors.white,
                    ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ImageStoryItem extends StatelessWidget {
  const ImageStoryItem(
      {super.key,
      required this.imageUrl,
      required this.storyController,
      required this.controllerId,
      this.storyTitle,
      this.storyDescription,
      this.redirectUrl});
  final String imageUrl;
  final StoryController storyController;
  final String controllerId;
  final String? storyTitle;
  final String? storyDescription;
  final String? redirectUrl;

  @override
  Widget build(BuildContext context) {
    return Container(
      key: ValueKey(controllerId),
      color: PartnerAppColors.darkBlue,
      child: Stack(
        children: <Widget>[
          Center(
            child: StoryImage(
              ImageLoader(imageUrl),
              controller: storyController,
              fit: BoxFit.contain,
            ),
          ),
          SafeArea(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: double.infinity,
                margin: const EdgeInsets.only(bottom: 24),
                padding:
                    const EdgeInsets.symmetric(horizontal: 24, vertical: 8),
                decoration: BoxDecoration(
                  color: PartnerAppColors.darkBlue.withValues(alpha: 0.5),
                ),
                child: Text.rich(
                  textAlign: TextAlign.start,
                  TextSpan(
                    children: [
                      TextSpan(
                        text: '${storyTitle ?? ''}\n\n',
                        style:
                            Theme.of(context).textTheme.titleMedium!.copyWith(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                      ),
                      TextSpan(
                        text: storyDescription ?? '',
                        style:
                            Theme.of(context).textTheme.titleMedium!.copyWith(
                                  color: Colors.white,
                                ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class YoutubeStoryVideoLoader extends StatefulWidget {
  const YoutubeStoryVideoLoader(
      {super.key,
      required this.videoController,
      required this.storyController,
      required this.controllerId,
      this.storyTitle,
      this.storyDescription});
  final YoutubePlayerController videoController;
  final StoryController storyController;
  final String controllerId;
  final String? storyTitle;
  final String? storyDescription;

  @override
  State<YoutubeStoryVideoLoader> createState() =>
      _YoutubeStoryVideoLoaderState();
}

class _YoutubeStoryVideoLoaderState extends State<YoutubeStoryVideoLoader> {
  @override
  Widget build(BuildContext context) {
    return Container(
      key: ValueKey(widget.controllerId),
      color: PartnerAppColors.darkBlue,
      child: Stack(
        children: <Widget>[
          Center(
            child: YoutubePlayer(
              controller: widget.videoController,
            ),
          ),
          SafeArea(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: double.infinity,
                margin: const EdgeInsets.only(bottom: 24),
                padding:
                    const EdgeInsets.symmetric(horizontal: 24, vertical: 8),
                decoration: BoxDecoration(
                  color: PartnerAppColors.darkBlue.withValues(alpha: 0.5),
                ),
                child: Text.rich(
                  textAlign: TextAlign.start,
                  TextSpan(
                    children: [
                      TextSpan(
                        text: '${widget.storyTitle ?? ''}\n\n',
                        style:
                            Theme.of(context).textTheme.titleMedium!.copyWith(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                      ),
                      TextSpan(
                        text: widget.storyDescription ?? '',
                        style:
                            Theme.of(context).textTheme.titleMedium!.copyWith(
                                  color: Colors.white,
                                ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
