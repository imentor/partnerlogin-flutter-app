import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/messages/components.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

class ResetEmailPassword extends StatefulWidget {
  const ResetEmailPassword({super.key});

  @override
  State<ResetEmailPassword> createState() => _ResetEmailPasswordState();
}

class _ResetEmailPasswordState extends State<ResetEmailPassword> {
  final formKey = GlobalKey<FormBuilderState>();

  void onPressedSendLink({required UserViewModel userVm}) async {
    if (formKey.currentState!.validate()) {
      final isResetLinkSent = await tryCatchWrapper(
          context: context,
          function: userVm.sendResetLink(
              email: formKey.currentState!.fields['email']!.value));

      userVm.setBusy(false);
      if (mounted && (isResetLinkSent ?? false)) {
        showSuccessAnimationDialog(
                context, true, tr('login_data_will_be_sent_to_email'))
            .whenComplete(() {
          if (!mounted) return;

          Navigator.of(context).pop();
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        elevation: 0,
        leadingWidth: 120,
        leading: Container(
          width: 80,
          margin: const EdgeInsets.only(left: 20, top: 15),
          child: TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Transform.translate(
                  offset: const Offset(0, 1),
                  child: Icon(Icons.arrow_back,
                      color: Theme.of(context).colorScheme.primary, size: 15),
                ),
                SmartGaps.gapW5,
                Text(tr('back')),
              ],
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: FormBuilder(
          key: formKey,
          child: Consumer<UserViewModel>(
            builder: (_, vm, __) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        child: Image.asset(
                          ImagePaths.logo,
                          height: 30.0,
                          width: 30.0,
                        ),
                      ),
                      SmartGaps.gapW10,
                      Text(
                        'Håndværker.dk',
                        style: Theme.of(context)
                            .textTheme
                            .headlineLarge!
                            .copyWith(fontSize: 25.0),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Text(tr('reset_password'),
                      style: Theme.of(context).textTheme.titleLarge!.copyWith(
                          color:
                              Theme.of(context).colorScheme.onSurfaceVariant)),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    tr('reset_password_description'),
                    style: Theme.of(context).textTheme.titleSmall!.copyWith(
                        color: Theme.of(context).colorScheme.onSurfaceVariant),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  CustomTextFieldFormBuilder(
                    name: 'email',
                    labelText: tr('email'),
                    validator: FormBuilderValidators.required(
                        errorText: tr('required')),
                    initialValue: vm.userModel?.user?.email,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  vm.busy
                      ? const Center(
                          child: SizedBox(
                              height: 25,
                              width: 25,
                              child: CircularProgressIndicator(strokeWidth: 2)),
                        )
                      : CustomButton(
                          onPressed: () => onPressedSendLink(userVm: vm),
                          text: tr('send_reset_link'),
                        ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
