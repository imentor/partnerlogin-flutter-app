import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/messages/components.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/reset_viewmodel_states.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/location/location_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

class ResetPassword extends StatefulWidget {
  const ResetPassword({super.key, required this.email});
  final String email;

  @override
  State<ResetPassword> createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  final formKey = GlobalKey<FormBuilderState>();

  void onPressedSaveChanges({required UserViewModel userVm}) async {
    final locationVm = context.read<LocationViewModel>();
    if (formKey.currentState!.validate()) {
      final isReset = await tryCatchWrapper(
          context: context,
          function: userVm.resetPassword(
              email: formKey.currentState!.fields['email']!.value,
              password: formKey.currentState!.fields['password']!.value));

      userVm.setBusy(false);
      if (mounted && (isReset ?? false)) {
        await tryCatchWrapper(
            context: context,
            function: Future.value([
              locationVm.cancelLocationListener(),
              userVm.logoutFromResetPassword()
            ]));

        if (!mounted) return;

        resetViewmodelStates(context: context);
        Sentry.configureScope((scope) => scope.setUser(null));

        showSuccessAnimationDialog(
                context, true, tr('your_password_was_changed_successfully'))
            .then((value) {
          if (!mounted) return;

          Navigator.pushNamedAndRemoveUntil(
              context, Routes.login, (r) => false);
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        elevation: 0,
        leadingWidth: 120,
        leading: Container(
          width: 80,
          margin: const EdgeInsets.only(left: 20, top: 15),
          child: TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Transform.translate(
                  offset: const Offset(0, 1),
                  child: Icon(Icons.arrow_back,
                      color: Theme.of(context).colorScheme.primary, size: 15),
                ),
                SmartGaps.gapW5,
                Text(tr('back')),
              ],
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: FormBuilder(
          key: formKey,
          child: Consumer<UserViewModel>(builder: (_, vm, __) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      child: Image.asset(
                        ImagePaths.logo,
                        height: 30.0,
                        width: 30.0,
                      ),
                    ),
                    SmartGaps.gapW10,
                    Text(
                      'Håndværker.dk',
                      style: Theme.of(context)
                          .textTheme
                          .headlineLarge!
                          .copyWith(fontSize: 25.0),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 30,
                ),
                Text(tr('update_your_password'),
                    style: Theme.of(context).textTheme.titleLarge!.copyWith(
                        color: Theme.of(context).colorScheme.onSurfaceVariant)),
                const SizedBox(
                  height: 20,
                ),
                CustomTextFieldFormBuilder(
                  name: 'email',
                  labelText: tr('email'),
                  enabled: false,
                  initialValue: widget.email,
                  validator:
                      FormBuilderValidators.required(errorText: tr('required')),
                ),
                CustomTextFieldFormBuilder(
                  name: 'password',
                  labelText: tr('new_password'),
                  maxLines: 1,
                  obscureText: true,
                  validator:
                      FormBuilderValidators.required(errorText: tr('required')),
                ),
                CustomTextFieldFormBuilder(
                  name: 'confirm_password',
                  labelText: tr('confirm_password'),
                  obscureText: true,
                  maxLines: 1,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(errorText: tr('required')),
                    (value) {
                      if (formKey.currentState!.fields['password']!.value !=
                          value) {
                        return tr('passwords_dont_match');
                      }
                      return null;
                    }
                  ]),
                ),
                const SizedBox(
                  height: 20,
                ),
                vm.busy
                    ? const Center(
                        child: SizedBox(
                            height: 25,
                            width: 25,
                            child: CircularProgressIndicator(strokeWidth: 2)),
                      )
                    : CustomButton(
                        onPressed: () => onPressedSaveChanges(userVm: vm),
                        text: tr('save_changes'),
                      ),
              ],
            );
          }),
        ),
      ),
    );
  }
}
