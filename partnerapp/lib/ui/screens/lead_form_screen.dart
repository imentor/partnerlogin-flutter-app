import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
// import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
// import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
// import 'package:Haandvaerker.dk/viewmodel/send_lead_info_viewmodel.dart';
import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
import 'package:validators/validators.dart';
// import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';

class LeadFormScreen extends StatefulWidget {
  const LeadFormScreen({super.key});
  @override
  LeadFormScreenState createState() => LeadFormScreenState();
}

class LeadFormScreenState extends State<LeadFormScreen> {
  @override
  Widget build(BuildContext context) {
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);

    return Scaffold(
      body: SafeArea(
          child: Container(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0),
        child: ListView(children: const [HeaderTexts(), LeadForm()]),
      )),
    );
  }
}

class HeaderTexts extends StatelessWidget {
  const HeaderTexts({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SmartGaps.gapH5,
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Text(
                tr('back'),
                textAlign: TextAlign.end,
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: Theme.of(context).colorScheme.primary,
                    fontWeight: FontWeight.w700),
              ),
            ),
            SmartGaps.gapH20,
            Text(tr('lead_form_header_text'),
                style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                    color: Theme.of(context).colorScheme.onSurfaceVariant)),
            SmartGaps.gapH20,
            Text(
              tr('lead_form_text1'),
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium!
                  .copyWith(color: Theme.of(context).colorScheme.primary),
            ),
            Icon(ElementIcons.right,
                color: Theme.of(context).colorScheme.primary, size: 16),
            SmartGaps.gapH10,
            Text(
              tr('lead_form_text2'),
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium!
                  .copyWith(color: Theme.of(context).colorScheme.primary),
            ),
            Icon(ElementIcons.right,
                color: Theme.of(context).colorScheme.primary, size: 16),
            SmartGaps.gapH10,
            Text(
              tr('lead_form_text3'),
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium!
                  .copyWith(color: Theme.of(context).colorScheme.primary),
            ),
            Icon(ElementIcons.right,
                color: Theme.of(context).colorScheme.primary, size: 16),
          ]),
    );
  }
}

class LeadForm extends StatefulWidget {
  const LeadForm({super.key});
  @override
  LeadFormState createState() => LeadFormState();
}

class LeadFormState extends State<LeadForm> {
  GlobalKey<FormState> leadFormKey = GlobalKey<FormState>();
  final nameController = TextEditingController();
  final mobileController = TextEditingController();
  final companyController = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
    mobileController.dispose();
    companyController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: leadFormKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SmartGaps.gapH20,
          Text(tr('enter_name'), style: Theme.of(context).textTheme.titleSmall),
          SmartGaps.gapH5,
          TextFormField(
            controller: nameController,
            style: Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(fontWeight: FontWeight.w600),
            decoration: InputDecoration(
                filled: true,
                fillColor: Colors.grey[300],
                border: InputBorder.none),
            validator: (value) {
              if (value!.isEmpty) {
                return tr('this_field_cannot_be_empty');
              }
              return null;
            },
          ),
          SmartGaps.gapH5,
          Text(tr('enter_phone'),
              style: Theme.of(context).textTheme.titleSmall),
          SmartGaps.gapH5,
          TextFormField(
            controller: mobileController,
            keyboardType: TextInputType.number,
            style: Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(fontWeight: FontWeight.w600),
            decoration: InputDecoration(
                filled: true,
                fillColor: Colors.grey[300],
                border: InputBorder.none),
            validator: (value) {
              if (value!.isEmpty) {
                return tr('this_field_cannot_be_empty');
              }
              if (isNumeric(value.trim()) == false) {
                return tr('value_must_be_numeric');
              }
              return null;
            },
          ),
          SmartGaps.gapH5,
          Text(tr('enter_company'),
              style: Theme.of(context).textTheme.titleSmall),
          SmartGaps.gapH5,
          TextFormField(
            controller: companyController,
            style: Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(fontWeight: FontWeight.w600),
            decoration: InputDecoration(
                filled: true,
                fillColor: Colors.grey[300],
                border: InputBorder.none),
            validator: (value) {
              if (value!.isEmpty) {
                return tr('this_field_cannot_be_empty');
              }
              return null;
            },
          ),
          SmartGaps.gapH30,
          Container(
            height: 60,
            width: double.infinity,
            decoration: BoxDecoration(
                border:
                    Border.all(color: Theme.of(context).colorScheme.primary)),
            child: CustomDesignTheme.flatButtonStyle(
              child: Text(
                tr('submit').toUpperCase(),
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(color: Theme.of(context).colorScheme.primary),
              ),
              onPressed: () async {
                // if (leadFormKey.currentState!.validate()) {
                //   final vm = Provider.of<SendLeadInfoViewModel>(context,
                //       listen: false);

                //   await tryCatchWrapper(
                //       context: context,
                //       function: vm.performRequest(
                //           company: companyController.text,
                //           name: nameController.text,
                //           mobile: mobileController.text));
                //   if (mounted) {
                //     await showSuccessAnimationDialog(
                //         context,
                //         vm.value!.success!,
                //         tr(vm.value!.success!
                //             ? 'lead_form_submit_message'
                //             : 'failed'));
                //   }
                //   //go to login screen, this assumes that the user will be given an account or cvr after assited by the marketing team

                //   if (vm.value!.success! && mounted) {
                //     Navigator.of(context).pushNamed(
                //       Routes.login,
                //     );
                //   }
                // }
              },
            ),
          ),
          SmartGaps.gapH30,
        ],
      ),
    );
  }
}
