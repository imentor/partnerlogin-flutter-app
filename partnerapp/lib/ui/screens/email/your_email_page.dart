import 'package:Haandvaerker.dk/model/email/email_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class YourEmailPage extends StatefulWidget {
  const YourEmailPage({super.key});

  @override
  State<YourEmailPage> createState() => _YourEmailPageState();
}

class _YourEmailPageState extends State<YourEmailPage> {
  final formKey = GlobalKey<FormBuilderState>();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final userVm = context.read<UserViewModel>();
      final newsFeedVm = context.read<NewsFeedViewModel>();
      final appDrawerVm = context.read<AppDrawerViewModel>();
      const dynamicStoryPage = 'ekstraemail';
      userVm.isGetEmail = true;
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      if (dynamicStoryEnable) {
        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: Future.wait([
              userVm.getPartnerEmails(),
              newsFeedVm.getDynamicStory(page: dynamicStoryPage),
            ])).whenComplete(() {
          if (!mounted) return;

          userVm.isGetEmail = false;
          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      } else {
        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: Future.wait([
              userVm.getPartnerEmails(),
            ])).whenComplete(() {
          userVm.isGetEmail = false;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: FormBuilder(
          key: formKey,
          child: Consumer<UserViewModel>(builder: (_, vm, __) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  tr('your_email'),
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                SmartGaps.gapH10,
                Text(
                  tr('your_email_description'),
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 16),
                ),
                SmartGaps.gapH10,
                CustomTextFieldFormBuilder(
                  name: 'email',
                  labelText: tr('enter_email'),
                  keyboardType: TextInputType.emailAddress,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(errorText: tr('required')),
                    FormBuilderValidators.email(
                        errorText: tr('enter_valid_email'))
                  ]),
                ),
                SmartGaps.gapH10,
                CustomDesignTheme.flatButtonStyle(
                  height: 50,
                  backgroundColor: PartnerAppColors.malachite,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  onPressed: () async {
                    if (formKey.currentState!.validate()) {
                      modalManager.showLoadingModal();

                      await vm
                          .addPartnerEmail(
                              email: formKey
                                      .currentState?.fields['email']?.value ??
                                  '')
                          .then((value) {
                        formKey.currentState!.patchValue({'email': ''});
                        modalManager.hideLoadingModal();
                      });
                    }
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        tr('save'),
                        style:
                            Theme.of(context).textTheme.displayLarge!.copyWith(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white,
                                ),
                      ),
                      SmartGaps.gapH10,
                      const Icon(FeatherIcons.arrowRight, color: Colors.white),
                    ],
                  ),
                ),
                SmartGaps.gapH30,
                Text(
                  tr('saved_email'),
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
                SmartGaps.gapH20,
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Text(
                        tr('email'),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.bold,
                                fontSize: 14),
                      ),
                    ),
                    SmartGaps.gapH10,
                    Expanded(
                      child: Text(
                        tr('date'),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.bold,
                                fontSize: 14),
                      ),
                    ),
                    SmartGaps.gapH10,
                    Expanded(child: Container())
                  ],
                ),
                SmartGaps.gapH20,
                Expanded(
                  child: Skeletonizer(
                    enabled: vm.isGetEmail,
                    child: ListView.separated(
                      separatorBuilder: (context, index) {
                        return SmartGaps.gapH10;
                      },
                      itemBuilder: (context, index) {
                        return vm.isGetEmail
                            ? const FakeEmailListRow()
                            : EmailListRow(
                                emailList: vm.partnerEmails,
                                vm: vm,
                                index: index);
                      },
                      itemCount: vm.isGetEmail ? 10 : vm.partnerEmails.length,
                      shrinkWrap: true,
                    ),
                  ),
                )
              ],
            );
          }),
        ),
      ),
    );
  }
}

class EmailListRow extends StatelessWidget {
  const EmailListRow(
      {super.key,
      required this.emailList,
      required this.vm,
      required this.index});
  final List<EmailModel> emailList;
  final UserViewModel vm;
  final int index;

  @override
  Widget build(BuildContext context) {
    final email = emailList[index];
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Text(
            email.email ?? '',
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.bold,
                fontSize: 14),
          ),
        ),
        SmartGaps.gapH10,
        Expanded(
          child: Text(
            Formatter.formatDateStrings(
                type: DateFormatType.standardDate,
                dateString: email.dateCreated),
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.bold,
                fontSize: 14),
          ),
        ),
        SmartGaps.gapH10,
        Expanded(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: () async {
                if (email.id != null) {
                  modalManager.showLoadingModal();

                  await vm.deletePartnerEmail(emailId: email.id!).then((value) {
                    modalManager.hideLoadingModal();
                  });
                }
              },
              child: const Icon(
                FeatherIcons.trash2,
                color: PartnerAppColors.red,
              ),
            ),
          ],
        ))
      ],
    );
  }
}

class FakeEmailListRow extends StatelessWidget {
  const FakeEmailListRow({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Text(
            'Fake Email',
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.bold,
                fontSize: 14),
          ),
        ),
        SmartGaps.gapH10,
        Expanded(
          child: Text(
            '00. Fake 0000',
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.bold,
                fontSize: 14),
          ),
        ),
        SmartGaps.gapH10,
        Expanded(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: () {},
              child: const Icon(
                FeatherIcons.trash2,
                color: PartnerAppColors.red,
              ),
            ),
          ],
        ))
      ],
    );
  }
}
