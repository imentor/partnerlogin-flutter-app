import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/extension/snackbar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/change_password_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:password_strength/password_strength.dart';
import 'package:provider/provider.dart';

class PasswordSpec {
  String title;
  bool status;
  PasswordSpec({required this.title, required this.status});
}

class ChangePasswordScreen extends StatefulWidget {
  const ChangePasswordScreen({super.key});

  @override
  ChangePasswordScreenState createState() => ChangePasswordScreenState();
}

class ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController newPasswordController;
  late TextEditingController repeatPasswordController;
  late FocusNode newPassFocus;
  late FocusNode repeatPassFocus;

  bool hidePassword = true;
  Color? passwordColor = Colors.grey[300];
  String passwordDesription = '';
  List<bool> colorMark = [false, false, false, false];

  List<PasswordSpec> passwordSpec = [
    PasswordSpec(title: 'longer_than_7_characters', status: false),
    PasswordSpec(title: 'contains_capital_letter', status: false),
    PasswordSpec(title: 'contains_lowercase_letter', status: false),
    PasswordSpec(title: 'contains_number', status: false),
    PasswordSpec(title: 'contains_special_character', status: false),
  ];

  @override
  void initState() {
    newPasswordController = TextEditingController();
    repeatPasswordController = TextEditingController();
    newPassFocus = FocusNode();
    repeatPassFocus = FocusNode();
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final appDrawerVm = context.read<AppDrawerViewModel>();
      final newsFeedVm = context.read<NewsFeedViewModel>();
      const dynamicStoryPage = 'password';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      if (dynamicStoryEnable) {
        await Future.value(newsFeedVm.getDynamicStory(page: dynamicStoryPage))
            .whenComplete(() {
          if (!mounted) return;

          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      }
    });
  }

  @override
  void dispose() {
    newPasswordController.dispose();
    repeatPasswordController.dispose();
    newPassFocus.dispose();
    repeatPassFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final changePasswordViewModel =
        Provider.of<ChangePasswordViewModel>(context, listen: true);

    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SingleChildScrollView(
        child: Container(
          margin:
              const EdgeInsets.only(top: 50, right: 20, bottom: 20, left: 20),
          child: Form(
              key: _formKey,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              child: Column(children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      tr('change_password'),
                      style: Theme.of(context).textTheme.headlineLarge,
                    ),
                  ],
                ),
                SmartGaps.gapH20,
                TextFormField(
                    controller: newPasswordController,
                    focusNode: newPassFocus,
                    onChanged: analyzeValue,
                    obscureText: hidePassword,
                    decoration: InputDecoration(
                        suffixIcon: IconButton(
                            icon: Icon(
                              hidePassword
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Colors.black,
                            ),
                            onPressed: () {
                              setState(() {
                                hidePassword = !hidePassword;
                              });
                            }),
                        border: InputBorder.none,
                        filled: true,
                        hintStyle: const TextStyle(fontSize: 18),
                        hintText: tr('new_password')),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return tr('please_enter_some_text');
                      }

                      if (estimatePasswordStrength(value) < 0.33) {
                        return tr('password_is_weak');
                      }

                      return null;
                    }),
                Container(
                  padding: const EdgeInsets.only(top: 5),
                  height: 12,
                  child: Row(
                    children: <Widget>[
                      for (int i = 0; i <= 3; i++)
                        Expanded(
                            child: Container(
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 3),
                                color: colorMark[i]
                                    ? passwordColor
                                    : Colors.grey[300]))
                    ],
                  ),
                ),
                SizedBox(
                  width: double.infinity,
                  child: Text(
                    passwordDesription,
                    textAlign: TextAlign.right,
                    style: TextStyle(color: passwordColor),
                  ),
                ),
                SmartGaps.gapH20,
                TextFormField(
                    controller: repeatPasswordController,
                    focusNode: repeatPassFocus,
                    obscureText: true,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        filled: true,
                        hintStyle: const TextStyle(fontSize: 18),
                        hintText: tr('repeat_password')),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return tr('please_enter_some_text');
                      }
                      if (value.trim() !=
                          newPasswordController.text.toString().trim()) {
                        return tr('passwords_dont_match');
                      }
                      return null;
                    }),
                SmartGaps.gapH30,
                for (PasswordSpec f in passwordSpec) passwordSpecDescription(f),
                SmartGaps.gapH30,
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: const Color(0xFF00DA58),
                  ),
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      final changePassVm =
                          context.read<ChangePasswordViewModel>();

                      changePassVm.setBusy(true);
                      try {
                        newPassFocus.unfocus();
                        repeatPassFocus.unfocus();
                        await changePassVm
                            .changePassword(
                                password: newPasswordController.text.trim())
                            .whenComplete(() async {
                          changePassVm.setBusy(false);
                          newPasswordController.clear();
                          repeatPasswordController.clear();
                          await showSuccessAnimationDialog(
                            myGlobals.homeScaffoldKey!.currentContext!,
                            true,
                            '',
                          ).then((value) {
                            if (value) {
                              backDrawerRoute();
                            }
                          });
                        });
                      } on ApiException catch (_) {
                        myGlobals.homeScaffoldKey!.currentContext!
                            .showErrorSnackBar();
                      }
                    }
                  },
                  child: SizedBox(
                    height: 70,
                    width: double.infinity,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          tr('save'),
                          style: const TextStyle(
                              fontSize: 24,
                              color: Colors.white,
                              fontWeight: FontWeight.w700),
                        ),
                        SmartGaps.gapW5,
                        const Icon(
                          Icons.arrow_forward,
                          size: 20,
                          color: Colors.white,
                        )
                      ],
                    ),
                  ),
                ),
                SmartGaps.gapH10,
                (changePasswordViewModel.busy) ? loader() : Container()
              ])),
        ),
      ),
    );
  }

  analyzeValue(String value) {
    String value0 = value.toString().trim();
    double strength = estimatePasswordStrength(value0);

    if (value0.isEmpty) {
      setState(() {
        colorMark = [false, false, false, false];
        passwordColor = Colors.grey[300];
        passwordDesription = '';
      });
    } else {
      if (strength < 0.13) {
        setState(() {
          colorMark = [true, false, false, false];
          passwordColor = Colors.red;
          passwordDesription = tr('very_weak');
        });
      } else if (strength < 0.33) {
        setState(() {
          colorMark = [true, true, false, false];
          passwordColor = Colors.orange;
          passwordDesription = tr('weak');
        });
      } else if (strength < 0.67) {
        setState(() {
          colorMark = [true, true, true, false];
          passwordColor = Colors.blue;
          passwordDesription = tr('strong');
        });
      } else {
        setState(() {
          colorMark = [true, true, true, true];
          passwordColor = Colors.green;
          passwordDesription = tr('very_strong');
        });
      }
    }

    //check if more than 7 characters
    if (value0.length > 7) {
      setState(() {
        passwordSpec.elementAt(0).status = true;
      });
    } else {
      setState(() {
        passwordSpec.elementAt(0).status = false;
      });
    }
    //check if has uppercase
    if (value0.contains(RegExp(r'[A-Z]', caseSensitive: true))) {
      setState(() {
        passwordSpec.elementAt(1).status = true;
      });
    } else {
      setState(() {
        passwordSpec.elementAt(1).status = false;
      });
    }
    //check if contains lowercase
    if (value0.contains(RegExp(r'[a-z]', caseSensitive: true))) {
      setState(() {
        passwordSpec.elementAt(2).status = true;
      });
    } else {
      setState(() {
        passwordSpec.elementAt(2).status = false;
      });
    }
    //check if has numbers
    if (value0.contains(RegExp(r'[0-9]'))) {
      setState(() {
        passwordSpec.elementAt(3).status = true;
      });
    } else {
      setState(() {
        passwordSpec.elementAt(3).status = false;
      });
    }

    // check for special characters
    if (value0.contains(RegExp(r'[^A-Za-z0-9]'))) {
      setState(() {
        passwordSpec.elementAt(4).status = true;
      });
    } else {
      setState(() {
        passwordSpec.elementAt(4).status = false;
      });
    }
  }

  Widget passwordSpecDescription(PasswordSpec p) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Row(
          children: <Widget>[
            p.status
                ? const Icon(
                    Icons.check,
                    color: Colors.green,
                  )
                : const Icon(Icons.clear, color: Colors.red),
            SmartGaps.gapW10,
            Text(
              tr(p.title),
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w300,
                  color: p.status ? Colors.grey : Colors.black),
            )
          ],
        ),
      );
}
