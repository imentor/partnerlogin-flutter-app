import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/photo_gallery.dart';
import 'package:Haandvaerker.dk/ui/screens/create_project/create_project_components.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/create_projects_viewmodel.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class CreateProjectStep2 extends StatefulWidget {
  const CreateProjectStep2({
    super.key,
    required this.onBack,
    required this.onNext,
  });

  final VoidCallback onBack;
  final Function(String, String, String, String, String, String) onNext;

  @override
  CreateProjectStep2State createState() => CreateProjectStep2State();
}

class CreateProjectStep2State extends State<CreateProjectStep2> {
  bool enableCropBeforeUpload = true;
  List<String>? newImages;
  final loadingKey = GlobalKey<NavigatorState>();
  final firstFormKey = GlobalKey<FormState>();
  late TextEditingController title;
  late TextEditingController description;
  late TextEditingController price;
  late TextEditingController time;
  late TextEditingController projectCreated;
  late TextEditingController zipController;
  final CurrencyTextInputFormatter _priceFormatter =
      CurrencyTextInputFormatter(NumberFormat.currency(
    locale: 'da',
    decimalDigits: 2,
    symbol: '',
  ));

  @override
  void initState() {
    title = TextEditingController();
    description = TextEditingController();
    price = TextEditingController();
    time = TextEditingController();
    projectCreated = TextEditingController();
    zipController = TextEditingController();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final createProjectsVm = context.read<CreateProjectsViewModel>();
      if (createProjectsVm.step2FormData.isNotEmpty) {
        final formData = createProjectsVm.step2FormData;
        title.text = formData['title'];
        description.text = formData['description'];
        price.text = formData['price'];
        time.text = formData['time'];
        projectCreated.text = formData['projectCreated'];
        zipController.text = formData['zipController'];
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    title.dispose();
    description.dispose();
    price.dispose();
    time.dispose();
    projectCreated.dispose();
    zipController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(tr('project_information'),
            style: Theme.of(context).textTheme.headlineLarge),
        SmartGaps.gapH20,
        Text(tr('create_project_desc_1'),
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.titleSmall),
        SmartGaps.gapH20,
        _getForm(),
        SmartGaps.gapH20,
        Consumer<CreateProjectsViewModel>(
          builder: (_, vm, __) {
            if (vm.projectImages.isNotEmpty) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(tr('pictures'),
                        style: Theme.of(context).textTheme.headlineSmall),
                    SmartGaps.gapH20,
                    SizedBox(
                      height: 150.0,
                      child: CustomHeroGallery(assetFiles: vm.projectImages),
                    )
                  ],
                ),
              );
            } else {
              return Container();
            }
          },
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: 150,
              height: 60,
              child: CustomDesignTheme.flatButtonStyle(
                backgroundColor: Theme.of(context).colorScheme.primary,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(tr('back'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(color: Colors.white, height: 1)),
                  ],
                ),
                onPressed: () {
                  final formData = {
                    'title': title.text.trim(),
                    'description': description.text.trim(),
                    'price': price.text.trim(),
                    'time': time.text.trim(),
                    'projectCreated': projectCreated.text.trim(),
                    'zipController': zipController.text.trim(),
                  };

                  context.read<CreateProjectsViewModel>().step2FormData =
                      formData;
                  widget.onBack();
                },
              ),
            ),
            SizedBox(
              width: 150,
              height: 60,
              child: CustomDesignTheme.flatButtonStyle(
                backgroundColor: Theme.of(context).colorScheme.secondary,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(tr('next'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(color: Colors.white, height: 1)),
                  ],
                ),
                onPressed: () {
                  if (firstFormKey.currentState!.validate()) {
                    widget.onNext(
                      title.text.trim(),
                      description.text.trim(),
                      zipController.text.trim(),
                      _priceFormatter.getUnformattedValue() != 0
                          ? _priceFormatter
                              .getUnformattedValue()
                              .toString()
                              .trim()
                          : price.text
                              .replaceAll('.', '')
                              .replaceAll(',', '.')
                              .trim(),
                      time.text.trim(),
                      projectCreated.text.trim(),
                    );
                    final formData = {
                      'title': title.text.trim(),
                      'description': description.text.trim(),
                      'price': price.text.trim(),
                      'time': time.text.trim(),
                      'projectCreated': projectCreated.text.trim(),
                      'zipController': zipController.text.trim(),
                    };

                    context.read<CreateProjectsViewModel>().step2FormData =
                        formData;
                  }
                },
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _getForm() {
    return Container(
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.grey.withValues(alpha: 0.25)),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withValues(alpha: .1),
              blurRadius: 8.0,
              spreadRadius: -1,
              offset: const Offset(0, 5),
            )
          ]),
      child: Form(
        key: firstFormKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Text(tr('job_type'),
                  style: Theme.of(context).textTheme.titleSmall),
            ),
            const JobTypeList(
              filter: '',
              isReadOnly: true,
            ),
            SmartGaps.gapH20,
            RichText(
              maxLines: 2,
              text: TextSpan(
                text: tr('name_of_project'),
                style: Theme.of(context).textTheme.titleSmall,
                children: const <TextSpan>[
                  TextSpan(
                    text: '*',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.red),
                  ),
                ],
              ),
            ),
            SmartGaps.gapH5,
            TextFormField(
                controller: title,
                style: Theme.of(context).textTheme.titleSmall,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withAlpha(100)),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withAlpha(100)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withAlpha(100)),
                    ),
                    filled: true,
                    fillColor: Colors.transparent,
                    hintStyle: Theme.of(context).textTheme.titleSmall!.copyWith(
                          color: Colors.black.withValues(alpha: 0.5),
                        ),
                    hintText: tr('name')),
                validator: (value) {
                  if (value!.isEmpty) {
                    return tr('please_enter_some_text');
                  }
                  return null;
                }),
            SmartGaps.gapH20,
            RichText(
              maxLines: 3,
              text: TextSpan(
                text: tr('describe_the_project'),
                style: Theme.of(context)
                    .textTheme
                    .titleSmall!
                    .copyWith(height: 1.2),
                children: const <TextSpan>[
                  TextSpan(
                    text: '*',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.red),
                  ),
                ],
              ),
            ),
            SmartGaps.gapH10,
            TextFormField(
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.multiline,
                maxLines: null,
                controller: description,
                style: Theme.of(context).textTheme.titleSmall,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withAlpha(100)),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withAlpha(100)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withAlpha(100)),
                    ),
                    hintStyle: Theme.of(context).textTheme.titleSmall!.copyWith(
                          color: Colors.black.withValues(alpha: 0.5),
                        ),
                    hintText: tr('description')),
                validator: (value) {
                  if (value!.isEmpty) {
                    return tr('please_enter_some_text');
                  }
                  return null;
                }),
            SmartGaps.gapH20,
            RichText(
              maxLines: 2,
              text: TextSpan(
                text: tr('date_of_the_project'),
                style:
                    Theme.of(context).textTheme.titleSmall!.copyWith(height: 1),
                children: const <TextSpan>[
                  TextSpan(
                    text: '*',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.red),
                  ),
                ],
              ),
            ),
            SmartGaps.gapH5,
            InkWell(
              onTap: () async {
                await showDatePicker(
                        context: context,
                        firstDate: DateTime(1900),
                        initialDate: projectCreated.text.isEmpty
                            ? DateTime.now()
                            : DateTime.parse(projectCreated.text),
                        lastDate: DateTime(2100))
                    .then((value) {
                  if (value != null) {
                    setState(() {
                      projectCreated.text = Formatter.formatDateTime(
                          type: DateFormatType.isoDate, date: value);
                    });
                  }
                });
              },
              child: TextFormField(
                  controller: projectCreated,
                  enabled: false,
                  style: Theme.of(context).textTheme.titleSmall,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Theme.of(context)
                                .colorScheme
                                .primary
                                .withAlpha(100)),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Theme.of(context)
                                .colorScheme
                                .primary
                                .withAlpha(100)),
                      ),
                      disabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Theme.of(context)
                                .colorScheme
                                .primary
                                .withAlpha(100)),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Theme.of(context)
                                .colorScheme
                                .primary
                                .withAlpha(100)),
                      ),
                      filled: true,
                      fillColor: Colors.transparent,
                      hintStyle:
                          Theme.of(context).textTheme.titleSmall!.copyWith(
                                color: Colors.black.withValues(alpha: 0.5),
                              ),
                      hintText: tr('name')),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return tr('please_enter_some_text');
                    }
                    return null;
                  }),
            ),
            SmartGaps.gapH20,
            RichText(
              maxLines: 2,
              text: TextSpan(
                text: '${tr('time_number_of_days')}:',
                style:
                    Theme.of(context).textTheme.titleSmall!.copyWith(height: 1),
                children: const <TextSpan>[
                  TextSpan(
                    text: '*',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.red),
                  ),
                ],
              ),
            ),
            SmartGaps.gapH5,
            TextFormField(
                controller: time,
                style: Theme.of(context).textTheme.titleSmall,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withAlpha(100)),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withAlpha(100)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withAlpha(100)),
                    ),
                    hintStyle: Theme.of(context).textTheme.titleSmall!.copyWith(
                          color: Colors.black.withValues(alpha: 0.5),
                        ),
                    hintText: tr('eg_4_6_weeks')),
                validator: (value) {
                  return null;
                }),
            SmartGaps.gapH20,
            RichText(
              maxLines: 2,
              text: TextSpan(
                text: '${tr('price')} (kr.)',
                style:
                    Theme.of(context).textTheme.titleSmall!.copyWith(height: 1),
                children: const <TextSpan>[
                  TextSpan(
                    text: '*',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.red),
                  ),
                ],
              ),
            ),
            SmartGaps.gapH5,
            TextFormField(
                controller: price,
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[_priceFormatter],
                style:
                    Theme.of(context).textTheme.titleSmall!.copyWith(height: 1),
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withAlpha(100)),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withAlpha(100)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withAlpha(100)),
                    ),
                    hintStyle: Theme.of(context).textTheme.titleSmall!.copyWith(
                          color: Colors.black.withValues(alpha: 0.5),
                        ),
                    hintText: tr('dkk_incl_vat')),
                validator: (value) {
                  return null;
                }),
            SmartGaps.gapH20,
            RichText(
              maxLines: 2,
              text: TextSpan(
                text: tr('zip_code'),
                style:
                    Theme.of(context).textTheme.titleSmall!.copyWith(height: 1),
                children: const <TextSpan>[
                  TextSpan(
                    text: '*',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.red),
                  ),
                ],
              ),
            ),
            SmartGaps.gapH5,
            TextFormField(
                controller: zipController,
                keyboardType: TextInputType.number,
                style:
                    Theme.of(context).textTheme.titleSmall!.copyWith(height: 1),
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withAlpha(100)),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withAlpha(100)),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withAlpha(100)),
                  ),
                  hintStyle: Theme.of(context).textTheme.titleSmall!.copyWith(
                        color: Colors.black.withValues(alpha: 0.5),
                      ),
                  hintText: '2300',
                ),
                validator: (value) {
                  return null;
                }),
            SmartGaps.gapH20,
          ],
        ),
      ),
    );
  }
}
