import 'package:Haandvaerker.dk/ui/components/copy_to_clipboard_snackbar.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/my_projects_screen.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CreateProjectDone extends StatelessWidget {
  const CreateProjectDone({super.key, this.projectId, this.projectUrl});

  final String? projectId;
  final String? projectUrl;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Container(
        padding: const EdgeInsets.only(top: 0, left: 20.0, right: 20.0),
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(20),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                          color: Colors.grey.withValues(alpha: 0.25)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withValues(alpha: .1),
                          blurRadius: 8.0,
                          spreadRadius: -1,
                          offset: const Offset(0, 5),
                        )
                      ]),
                  child: Column(children: [
                    // projectId != null
                    //     ? existingProjectName(context)
                    //     : Container(),
                    SmartGaps.gapH10,
                    Text(tr('success!'),
                        style: Theme.of(context).textTheme.headlineLarge),
                    SmartGaps.gapH5,
                    Text(
                        projectId == null
                            ? tr('your_project_has_been_added')
                            : tr('your_project_has_been_updated'),
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.bodyMedium),
                    SmartGaps.gapH20,
                    Text(tr('project_url'),
                        style: Theme.of(context).textTheme.headlineMedium),
                    TextFormField(
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                            color: Theme.of(context).colorScheme.primary),
                        readOnly: true,
                        onTap: () {
                          changeDrawerRoute(
                            Routes.inAppWebView,
                            arguments: Uri.parse(
                                'https://www.haandvaerker.dk$projectUrl?clearcache=true'),
                          );
                        },
                        initialValue: 'https://www.haandvaerker.dk$projectUrl',
                        maxLines: null,
                        minLines: 2,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            filled: false,
                            fillColor: Colors.grey[200],
                            hintStyle: Theme.of(context)
                                .textTheme
                                .titleSmall!
                                .copyWith(fontWeight: FontWeight.w700),
                            hintText: 'url'),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return tr('please_enter_some_text');
                          }
                          return null;
                        }),
                  ]),
                ),
                SmartGaps.gapH20,
                SizedBox(
                  width: double.infinity,
                  height: 70,
                  child: CustomDesignTheme.flatButtonStyle(
                      backgroundColor: Colors.blue,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          const FaIcon(FontAwesomeIcons.facebook,
                              color: Colors.white),
                          SmartGaps.gapW5,
                          Flexible(
                            child: Text(
                              tr('share_your_project_on_facebook'),
                              maxLines: 2,
                              overflow: TextOverflow.fade,
                              style: const TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                            ),
                          ),
                        ],
                      ),
                      onPressed: () async {
                        await openFb('$projectUrl');
                        /*var fburl =
                'https://www.facebook.com/sharer/sharer.php?u=$projectUrl';
            if (await canLaunch(Uri.encodeFull(fburl))) {
              await launch(Uri.encodeFull(fburl));
            }*/
                      }),
                ),
                SmartGaps.gapH10,
                SizedBox(
                  width: double.infinity,
                  height: 70,
                  child: CustomDesignTheme.flatButtonStyle(
                    backgroundColor: Theme.of(context).colorScheme.secondary,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Flexible(
                          child: Text(
                            tr('copy_url'),
                            maxLines: 2,
                            overflow: TextOverflow.fade,
                            style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 16),
                          ),
                        ),
                      ],
                    ),
                    onPressed: () async {
                      await copyToClipboard(
                        context: context,
                        copyString: projectUrl!,
                        displayStatus: tr('project_url_copy_success'),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
