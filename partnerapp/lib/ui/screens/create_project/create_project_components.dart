import 'package:Haandvaerker.dk/model/get_types_response_model.dart';
import 'package:Haandvaerker.dk/model/projects/manufacture_model.dart';
import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/create_projects_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class JobTypeList extends StatelessWidget {
  const JobTypeList({
    super.key,
    this.filter,
    this.isReadOnly = false,
    this.existingTaskTypes,
  });

  final String? filter;
  final List<String>? existingTaskTypes;
  final bool isReadOnly;

  @override
  Widget build(BuildContext context) {
    return Consumer<CreateProjectsViewModel>(
      builder: (context, vm, _) {
        if (vm.busy) return loader();

        if (vm.jobTypes.isEmpty) return const EmptyListIndicator();

        List<SubIndustries> taskTypes = [];

        if (isReadOnly) {
          taskTypes = vm.selectedJobTypes;
        } else {
          if (filter!.isEmpty) {
            taskTypes = vm.jobTypes.sublist(0, 20);
          } else {
            taskTypes = vm.jobTypes
                .where((e) => e.subIndustryName!
                    .toLowerCase()
                    .contains(filter!.toLowerCase()))
                .toList();
          }
        }

        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RichText(
              textAlign: TextAlign.justify,
              text: TextSpan(
                children: [
                  if (existingTaskTypes != null)
                    if (existingTaskTypes!.isNotEmpty)
                      ...existingTaskTypes!.map(
                        (e) => WidgetSpan(
                          child: _taskItemContainer(context, null, e),
                        ),
                      ),
                  ...taskTypes.map(
                    (e) => WidgetSpan(
                      child: _taskItemContainer(context, e, null),
                    ),
                  ),
                ],
              ),
            ),
            if (context.watch<CreateProjectsViewModel>().validateJobTypes)
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  tr('required'),
                  textAlign: TextAlign.start,
                  style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        color: Colors.red,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w200,
                      ),
                ),
              ),
          ],
        );
      },
    );
  }

  Widget _taskItemContainer(
      BuildContext context, SubIndustries? type, String? existingTaskType) {
    final vm = context.read<CreateProjectsViewModel>();
    final isSelected = vm.selectedJobTypes.contains(type);

    return InkWell(
      onTap: () {
        if (!isReadOnly) {
          vm.addRemoveJobTypes(type!);
        }

        //this.onSelect(task);
      },
      child: Container(
          padding: const EdgeInsets.only(top: 2, bottom: 2, right: 5, left: 5),
          margin: const EdgeInsets.all(2),
          decoration: BoxDecoration(
              border: Border.all(
                  color: isSelected && !isReadOnly
                      ? Theme.of(context).colorScheme.secondary
                      : Colors.grey.withValues(alpha: 0.3))),
          child: Text(
              existingTaskType ??
                  (context.locale.languageCode == 'da'
                      ? type!.subCategoryName!
                      : type!.subCategoryNameEn!),
              style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                  color: isSelected && !isReadOnly
                      ? Theme.of(context).colorScheme.secondary
                      : Colors.black))),
    );
  }
}

class ManufactureList extends StatelessWidget {
  const ManufactureList(
      {super.key, this.filter, this.chosenIndex, this.isReadOnly = false});

  final String? filter;
  final bool isReadOnly;
  final String? chosenIndex;

  @override
  Widget build(BuildContext context) {
    return Consumer<CreateProjectsViewModel>(
      builder: (context, vm, _) {
        if (vm.busy) return loader();

        if (vm.manufactures.isEmpty) {
          return const EmptyListIndicator();
        }

        List<ManufactureModel> manufactures = [];

        if (isReadOnly) {
          manufactures = vm.selectedManufactures;
        } else {
          manufactures = vm.manufactures;

          if (chosenIndex!.isEmpty && filter!.isEmpty) {
            manufactures = vm.manufactures.sublist(0, 20);
          }
          //filter by index
          if (chosenIndex!.isNotEmpty) {
            manufactures = manufactures
                .where((element) =>
                    element.name!.characters.first.toLowerCase() ==
                    chosenIndex!.toLowerCase())
                .toList();
          }

          //filter by search bar
          manufactures = manufactures
              .where(
                  (e) => e.name!.toLowerCase().contains(filter!.toLowerCase()))
              .toList();

          manufactures.sort((a, b) => a.name!.compareTo(b.name!));
        }

        return Container(
          width: double.maxFinite,
          padding: EdgeInsets.all(isReadOnly ? 0 : 20),
          decoration: isReadOnly
              ? const BoxDecoration()
              : BoxDecoration(
                  color: Colors.white,
                  border:
                      Border.all(color: Colors.grey.withValues(alpha: 0.25)),
                  boxShadow: [
                      BoxShadow(
                        color: Colors.black.withValues(alpha: .1),
                        blurRadius: 8.0,
                        spreadRadius: -1,
                        offset: const Offset(0, 5),
                      )
                    ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              RichText(
                textAlign: TextAlign.justify,
                text: TextSpan(
                  children: [
                    ...manufactures.map((e) =>
                        WidgetSpan(child: _taskItemContainer(context, e))),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _taskItemContainer(
      BuildContext context, ManufactureModel manufacture) {
    final vm = context.read<CreateProjectsViewModel>();
    final isSelected = vm.selectedManufactures.contains(manufacture);

    return InkWell(
      onTap: () {
        if (!isReadOnly) {
          vm.addRemoveManufactures(manufacture);
        }

        //this.onSelect(task);
      },
      child: Container(
          padding: const EdgeInsets.only(top: 2, bottom: 2, right: 5, left: 5),
          margin: const EdgeInsets.all(2),
          decoration: BoxDecoration(
              border: Border.all(
                  color: isSelected && !isReadOnly
                      ? Theme.of(context).colorScheme.secondary
                      : Colors.grey.withValues(alpha: 0.3))),
          child: Text(manufacture.name!,
              style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                  color: isSelected && !isReadOnly
                      ? Theme.of(context).colorScheme.secondary
                      : Colors.black))),
    );
  }
}
