import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/screens/create_project/create_project_components.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class CreateProjectStep3 extends StatefulWidget {
  const CreateProjectStep3(
      {super.key, required this.onBack, required this.onNext});

  final VoidCallback onBack;
  final VoidCallback onNext;

  @override
  CreateProjectStep3State createState() => CreateProjectStep3State();
}

class CreateProjectStep3State extends State<CreateProjectStep3> {
  bool enableCropBeforeUpload = true;
  List<String>? newImages;
  final loadingKey = GlobalKey<NavigatorState>();
  TextEditingController search = TextEditingController();
  String chosenIndex = '';

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(tr('manufacturers'),
            style: Theme.of(context).textTheme.headlineLarge),
        SmartGaps.gapH20,
        Text('Index',
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.titleSmall),
        SmartGaps.gapH20,
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(children: [
            ...generateIndex()
                .map((e) => WidgetSpan(child: _itemIndexContainer(e))),
          ]),
        ),
        SmartGaps.gapH20,
        Material(
          elevation: 10.0,
          shadowColor: Colors.white,
          child: TextField(
            textInputAction: TextInputAction.search,
            onSubmitted: (value) {},
            onChanged: (value) {
              setState(() {});
            },
            controller: search,
            decoration: InputDecoration(
              enabledBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.transparent),
                  borderRadius: BorderRadius.all(Radius.circular(0.0))),
              focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.white,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(0.0))),
              focusColor: Colors.white,
              hintText: tr('search'),
              prefixIcon: const Icon(
                Icons.search,
                color: Colors.grey,
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red),
                borderRadius: BorderRadius.all(Radius.circular(0.0)),
              ),
            ),
          ),
        ),
        SmartGaps.gapH20,
        ManufactureList(
          filter: search.text,
          chosenIndex: chosenIndex,
        ),
        SmartGaps.gapH20,
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: 150,
              height: 60,
              child: CustomDesignTheme.flatButtonStyle(
                backgroundColor: Theme.of(context).colorScheme.primary,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(tr('back'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(color: Colors.white, height: 1)),
                  ],
                ),
                onPressed: () {
                  widget.onBack();
                },
              ),
            ),
            SizedBox(
              width: 150,
              height: 60,
              child: CustomDesignTheme.flatButtonStyle(
                backgroundColor: Theme.of(context).colorScheme.secondary,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(tr('next'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(color: Colors.white, height: 1)),
                  ],
                ),
                onPressed: () {
                  widget.onNext();
                },
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _itemIndexContainer(String index) {
    final bool isChosen = index == chosenIndex;

    return InkWell(
      onTap: () {
        setState(() {
          if (isChosen) {
            chosenIndex = '';
          } else {
            chosenIndex = index;
          }
        });
      },
      child: Container(
          padding: const EdgeInsets.only(top: 2, bottom: 2, right: 5, left: 5),
          margin: const EdgeInsets.all(10),
          child: Text(index,
              style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                  color: isChosen
                      ? Theme.of(context).colorScheme.secondary
                      : Theme.of(context).colorScheme.primary))),
    );
  }

  List<String> generateIndex() {
    var aCode = 'A'.codeUnitAt(0);
    var zCode = 'Y'.codeUnitAt(0);
    List<String> alphabets = List<String>.generate(
      zCode - aCode + 1,
      (index) => String.fromCharCode(aCode + index),
    );

    alphabets.add('Ø');

    return alphabets;
  }
}
