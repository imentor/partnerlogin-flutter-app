import 'dart:io';

import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/get_image_with_custom_picker.dart';
import 'package:Haandvaerker.dk/ui/screens/create_project/create_project_components.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/create_projects_viewmodel.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:provider/provider.dart';
import 'package:show_up_animation/show_up_animation.dart';

class CreateProjectStep1 extends StatefulWidget {
  const CreateProjectStep1({super.key, required this.onNext});

  final VoidCallback onNext;

  @override
  CreateProjectStep1State createState() => CreateProjectStep1State();
}

class CreateProjectStep1State extends State<CreateProjectStep1> {
  bool enableCropBeforeUpload = true;
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();
  final searchController = TextEditingController();
  bool validate = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(tr('pictures'), style: Theme.of(context).textTheme.headlineLarge),
        SmartGaps.gapH20,
        Text(tr('start_uploading_photos'),
            style: Theme.of(context).textTheme.titleSmall),
        // if (!(widget.projectId == null || widget.projectId == ''))
        //   Consumer<CreateProjectsViewModel>(
        //     builder: (_, vm, __) {
        //       if (vm.project.project != null &&
        //           !vm.project.project!.images.isNullOrEmpty) {
        //         return Padding(
        //           padding: const EdgeInsets.only(top: 20),
        //           child: GridView.count(
        //             shrinkWrap: true,
        //             physics: const ClampingScrollPhysics(),
        //             primary: true,
        //             crossAxisCount: 2,
        //             childAspectRatio: 0.50,
        //             children: List.generate(vm.project.project!.images!.length,
        //                 (index) {
        //               final item = vm.project.project!.images!.elementAt(index);
        //               return photoCard(item, widget.projectId);
        //             }),
        //           ),
        //         );
        //       } else {
        //         return Container();
        //       }
        //     },
        //   ),
        SmartGaps.gapH20,
        DottedBorder(
          color: Theme.of(context).colorScheme.primary,
          dashPattern: const [2, 4, 2, 4],
          strokeWidth: 2,
          strokeCap: StrokeCap.round,
          borderType: BorderType.RRect,
          child: Container(
            padding: const EdgeInsets.all(10),
            height: 80,
            width: double.infinity,
            child: CustomDesignTheme.flatButtonStyle(
              onPressed: () async {
                final vm = context.read<CreateProjectsViewModel>();
                await getImage(context).then((image) async {
                  if (image != null) {
                    await ImageCropper().cropImage(
                      sourcePath: image.path,
                      aspectRatio: const CropAspectRatio(ratioX: 4, ratioY: 3),
                      uiSettings: [
                        AndroidUiSettings(
                            toolbarTitle: tr('crop_image'),
                            toolbarColor: Colors.black,
                            toolbarWidgetColor: Colors.white,
                            initAspectRatio: CropAspectRatioPreset.ratio4x3,
                            backgroundColor: Colors.white,
                            lockAspectRatio: false),
                        IOSUiSettings(
                          title: tr('crop_image'),
                        )
                      ],
                    ).then((crop) {
                      if (crop != null) {
                        final images = <File>[...vm.projectImages];
                        images.add(File(crop.path));
                        vm.projectImages = images;
                      }
                    });
                  }
                });
              },
              // uploaded image display
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    ImagePaths.picturePrimaryColor,
                    height: 50,
                  ),
                  SmartGaps.gapW5,
                  RichText(
                    maxLines: 2,
                    text: TextSpan(
                      text: '',
                      style: TextStyle(
                          color: Theme.of(context).colorScheme.primary),
                      children: <TextSpan>[
                        TextSpan(
                            text: tr('click_to_select_files'),
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Theme.of(context).colorScheme.primary,
                                fontSize: 14,
                                fontFamily: 'Gibson')),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Consumer<CreateProjectsViewModel>(builder: (vmContext, vm, _) {
          if (vm.projectImages.isEmpty && validate) {
            return ShowUpAnimation(
                //delayStart: Duration(seconds: 1),
                animationDuration: const Duration(milliseconds: 300),
                curve: Curves.ease,
                direction: Direction.vertical,
                offset: -0.5,
                child: Container(
                  width: double.infinity,
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    tr('required'),
                    textAlign: TextAlign.start,
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        color: Colors.red,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w200),
                  ),
                ));
          } else {
            return Container();
          }
        }),
        SmartGaps.gapH20,
        Consumer<CreateProjectsViewModel>(
          builder: (_, vm, __) {
            if (vm.projectImages.isNotEmpty) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(tr('pictures'),
                        style: Theme.of(context).textTheme.headlineSmall),
                    SmartGaps.gapH20,
                    (vm.projectImages.isNotEmpty)
                        ? Container(
                            height: 150.0,
                            alignment: Alignment.topLeft,
                            padding: const EdgeInsets.symmetric(vertical: 16),
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              physics: const ClampingScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: vm.projectImages.length,
                              itemBuilder: (BuildContext context, int index) {
                                final item = vm.projectImages.elementAt(index);
                                return Stack(
                                  children: [
                                    Align(
                                      alignment: Alignment.center,
                                      child: Image.file(item),
                                    ),
                                    Positioned(
                                      right: 10,
                                      top: 10,
                                      child: Opacity(
                                        opacity: 0.7,
                                        child: Container(
                                          padding: const EdgeInsets.all(5),
                                          decoration: const BoxDecoration(
                                            color: Colors.black,
                                            shape: BoxShape.circle,
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.black45,
                                                blurRadius: 2.0,
                                                spreadRadius: 0.0,
                                                offset: Offset(2.0, 2.0),
                                              ),
                                            ],
                                          ),
                                          child: InkWell(
                                            onTap: () {
                                              vm.removeNewImage(item);
                                            },
                                            child: Icon(
                                              Icons.close,
                                              color: Colors.grey[200],
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                );
                              },
                            ),
                          )
                        : Container(child: empty())
                  ],
                ),
              );
            } else {
              return Container();
            }
          },
        ),
        Material(
          elevation: 10.0,
          shadowColor: Colors.white,
          child: TextField(
            textInputAction: TextInputAction.search,
            onSubmitted: (value) {},
            onChanged: (value) {
              setState(() {});
            },
            controller: searchController,
            decoration: InputDecoration(
              enabledBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.transparent),
                  borderRadius: BorderRadius.all(Radius.circular(0.0))),
              focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.white,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(0.0))),
              focusColor: Colors.white,
              hintText: tr('search_task_type'),
              prefixIcon: const Icon(
                Icons.search,
                color: Colors.grey,
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red),
                borderRadius: BorderRadius.all(Radius.circular(0.0)),
              ),
            ),
          ),
        ),
        SmartGaps.gapH20,
        Align(
          alignment: Alignment.centerLeft,
          child: Text(tr('job_type'),
              style: Theme.of(context).textTheme.headlineSmall),
        ),
        Consumer<CreateProjectsViewModel>(builder: (vmContext, vm, _) {
          if (vm.selectedJobTypes.isEmpty && validate) {
            return ShowUpAnimation(
                //delayStart: Duration(seconds: 1),
                animationDuration: const Duration(milliseconds: 300),
                curve: Curves.ease,
                direction: Direction.vertical,
                offset: -0.5,
                child: Container(
                  width: double.infinity,
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    tr('required'),
                    textAlign: TextAlign.start,
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        color: Colors.red,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w200),
                  ),
                ));
          } else {
            return Container();
          }
        }),
        SmartGaps.gapH20,
        JobTypeList(
          filter: searchController.text,
        ),
        SmartGaps.gapH20,
        SizedBox(
          height: 60,
          width: double.infinity,
          child: CustomDesignTheme.flatButtonStyle(
            backgroundColor: Theme.of(context).colorScheme.secondary,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(tr('next'),
                    style: Theme.of(context)
                        .textTheme
                        .titleMedium!
                        .copyWith(color: Colors.white, height: 1)),
              ],
            ),
            onPressed: () {
              var viewModel = context.read<CreateProjectsViewModel>();

              if (!validate) {
                setState(() {
                  validate = true;
                });
              }

              // if (!(widget.projectId == null || widget.projectId == '')) {
              //   if (viewModel.project.project!.images!.isNotEmpty &&
              //       viewModel.selectedJobTypes.isNotEmpty) {
              //     widget.onNext();
              //   }
              // } else {
              if (viewModel.projectImages.isNotEmpty &&
                  viewModel.selectedJobTypes.isNotEmpty) {
                widget.onNext();
              }
              // }
            },
          ),
        ),
        SmartGaps.gapH40,
      ],
    );
  }
}
