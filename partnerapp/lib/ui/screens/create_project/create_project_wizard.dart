import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/steps_widget.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/create_project/create_project_step1.dart';
import 'package:Haandvaerker.dk/ui/screens/create_project/create_project_step2.dart';
import 'package:Haandvaerker.dk/ui/screens/create_project/create_project_step3.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/extension/snackbar.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/create_projects_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CreateProjectWizard extends StatefulWidget {
  const CreateProjectWizard({super.key});

  @override
  CreateProjectWizardState createState() => CreateProjectWizardState();
}

class CreateProjectWizardState extends State<CreateProjectWizard> {
  int currentPage = 0;
  int totalSteps = 3;
  final loadingKey = GlobalKey<NavigatorState>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final viewModel = context.read<CreateProjectsViewModel>();

      //INITIALIZING UPLOADED IMAGES TO EMPTY

      viewModel.setValuesToDefault();

      viewModel.setBusy(true);

      await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function: Future.wait([
          viewModel.getJobTypes(),
          viewModel.getAllManufactures(),
        ]),
      );

      viewModel.setBusy(false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Container(
        padding: const EdgeInsets.only(top: 0, left: 20.0, right: 20.0),
        child: SingleChildScrollView(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 100,
                  child: DynamicStepsWidget(
                    currentPage: currentPage,
                    steps: totalSteps,
                    titles: const ['', '', ''],
                  ),
                ),
                _steps(),
              ]),
        ),
      ),
    );
  }

  Widget _steps() {
    switch (currentPage) {
      case 0:
        return CreateProjectStep1(
          onNext: () {
            setState(() {
              currentPage = 1;
            });
          },
        );
      case 1:
        return CreateProjectStep2(
          onBack: () {
            setState(() {
              currentPage = 0;
            });
          },
          onNext: (
            title,
            description,
            post,
            price,
            time,
            projectCreated,
          ) {
            final vm = context.read<CreateProjectsViewModel>();

            setState(() {
              vm.title = title;
              vm.description = description;
              vm.post = post;
              vm.price = price;
              vm.time = time;
              vm.projectCreated = projectCreated;
              currentPage = 2;
            });
          },
        );
      case 2:
        return CreateProjectStep3(onBack: () {
          setState(() {
            currentPage = 1;
          });
        }, onNext: () async {
          await createProject();
        });
    }

    return Container();
  }

  Future<void> createProject() async {
    var viewModel = context.read<CreateProjectsViewModel>();

    showLoadingDialog(context, loadingKey);

    try {
      await viewModel.createProject().whenComplete(() {
        Navigator.of(loadingKey.currentContext!, rootNavigator: true).pop();
        showSuccessAnimationDialog(
            myGlobals.homeScaffoldKey!.currentContext!, true, '');

        changeDrawerReplaceRoute(Routes.projectDone, arguments: {
          'projectUrl': viewModel.projectURL,
        });
      });
    } on ApiException catch (_) {
      Navigator.of(loadingKey.currentContext!, rootNavigator: true).pop();
      myGlobals.homeScaffoldKey!.currentContext!.showErrorSnackBar();
    }
  }
}
