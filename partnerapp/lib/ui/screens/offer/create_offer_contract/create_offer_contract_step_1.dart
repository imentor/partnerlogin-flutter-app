import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/fast_track_offer/fast_track_offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:skeletonizer/skeletonizer.dart';

class CreateOfferContractStepOne extends StatefulWidget {
  const CreateOfferContractStepOne({super.key, required this.vm});

  final FastTrackOfferViewModel vm;

  @override
  CreateOfferContractStepOneState createState() =>
      CreateOfferContractStepOneState();
}

class CreateOfferContractStepOneState
    extends State<CreateOfferContractStepOne> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('what_type_contract'),
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              letterSpacing: 1.0,
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18),
        ),
        SmartGaps.gapH20,
        Skeletonizer(
          enabled: widget.vm.isLoading,
          child: Column(
            children: [
              InkWell(
                onTap: () {
                  widget.vm.updateCreateOfferContractForms(
                      key: 'contractType', value: 'Hovedentreprise');
                },
                child: Container(
                  padding: const EdgeInsets.all(20),
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: widget.vm
                                    .createOfferContractForm['contractType'] ==
                                'Hovedentreprise'
                            ? PartnerAppColors.blue
                            : PartnerAppColors.darkBlue.withValues(alpha: .4),
                      ),
                      borderRadius: BorderRadius.circular(5)),
                  child: Row(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SvgPicture.asset(SvgIcons.mainContractSolidLock,
                            colorFilter: ColorFilter.mode(
                              widget.vm.createOfferContractForm[
                                          'contractType'] ==
                                      'Hovedentreprise'
                                  ? PartnerAppColors.blue
                                  : PartnerAppColors.darkBlue,
                              BlendMode.srcIn,
                            )),
                        SmartGaps.gapW20,
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                tr('main_contractor'),
                                style: Theme.of(context)
                                    .textTheme
                                    .displayLarge!
                                    .copyWith(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      color: widget.vm.createOfferContractForm[
                                                  'contractType'] ==
                                              'Hovedentreprise'
                                          ? PartnerAppColors.blue
                                          : PartnerAppColors.darkBlue,
                                    ),
                              ),
                              SmartGaps.gapH10,
                              Text(
                                tr('hoved_description'),
                                style: Theme.of(context)
                                    .textTheme
                                    .displayLarge!
                                    .copyWith(
                                      fontSize: 14,
                                      fontWeight: FontWeight.normal,
                                      color: widget.vm.createOfferContractForm[
                                                  'contractType'] ==
                                              'Hovedentreprise'
                                          ? PartnerAppColors.blue
                                          : PartnerAppColors.darkBlue,
                                    ),
                              ),
                            ],
                          ),
                        )
                      ]),
                ),
              ),
              SmartGaps.gapH20,
              InkWell(
                onTap: () {
                  widget.vm.updateCreateOfferContractForms(
                      key: 'contractType', value: 'Fagentreprise');
                },
                child: Container(
                  padding: const EdgeInsets.all(20),
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: widget.vm
                                    .createOfferContractForm['contractType'] ==
                                'Fagentreprise'
                            ? PartnerAppColors.blue
                            : PartnerAppColors.darkBlue.withValues(alpha: .4),
                      ),
                      borderRadius: BorderRadius.circular(5)),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SvgPicture.asset(SvgIcons.mainContractSolidLock,
                          colorFilter: ColorFilter.mode(
                            widget.vm.createOfferContractForm['contractType'] ==
                                    'Fagentreprise'
                                ? PartnerAppColors.blue
                                : PartnerAppColors.darkBlue,
                            BlendMode.srcIn,
                          )),
                      SmartGaps.gapW20,
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              tr('prof_contractor'),
                              style: Theme.of(context)
                                  .textTheme
                                  .displayLarge!
                                  .copyWith(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: widget.vm.createOfferContractForm[
                                                'contractType'] ==
                                            'Fagentreprise'
                                        ? PartnerAppColors.blue
                                        : PartnerAppColors.darkBlue,
                                  ),
                            ),
                            SmartGaps.gapH10,
                            Text(
                              tr('fagen_description'),
                              style: Theme.of(context)
                                  .textTheme
                                  .displayLarge!
                                  .copyWith(
                                    fontSize: 14,
                                    fontWeight: FontWeight.normal,
                                    color: widget.vm.createOfferContractForm[
                                                'contractType'] ==
                                            'Fagentreprise'
                                        ? PartnerAppColors.blue
                                        : PartnerAppColors.darkBlue,
                                  ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
