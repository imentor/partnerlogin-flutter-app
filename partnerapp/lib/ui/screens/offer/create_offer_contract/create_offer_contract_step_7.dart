import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/fast_track_offer/fast_track_offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class CreateOfferContractStepSeven extends StatefulWidget {
  const CreateOfferContractStepSeven({
    super.key,
    required this.vm,
    required this.formKey,
  });
  final FastTrackOfferViewModel vm;
  final GlobalKey<FormBuilderState> formKey;

  @override
  State<CreateOfferContractStepSeven> createState() =>
      _CreateOfferContractStepSevenState();
}

class _CreateOfferContractStepSevenState
    extends State<CreateOfferContractStepSeven> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('time_schedule'),
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              letterSpacing: 1.0,
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18),
        ),
        SmartGaps.gapH10,
        Text(
          tr('time_schedule_description'),
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              letterSpacing: 1.0,
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.normal,
              fontSize: 16),
        ),
        SmartGaps.gapH10,
        CustomDatePickerFormBuilder(
          name: 'offer_start',
          labelText: tr('work_begin'),
          initialValue: DateTime.now(),
          initialDate: DateTime.now(),
          dateFormat: DateFormatType.europeanDate.formatter,
          validator: FormBuilderValidators.required(errorText: tr('required')),
          suffixIcon: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 5),
            child: Icon(Icons.event, color: PartnerAppColors.blue),
          ),
        ),
        CustomDatePickerFormBuilder(
          name: 'offer_end',
          labelText: tr('offer_end_description'),
          initialValue: DateTime.now(),
          initialDate: DateTime.now(),
          dateFormat: DateFormatType.europeanDate.formatter,
          validator: FormBuilderValidators.required(errorText: tr('required')),
          suffixIcon: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 5),
            child: Icon(Icons.event, color: PartnerAppColors.blue),
          ),
        ),
      ],
    );
  }
}
