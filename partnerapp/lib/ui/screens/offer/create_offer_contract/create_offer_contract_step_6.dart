import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/screens/search_address/search_address.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/fast_track_offer/fast_track_offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class CreateOfferContractStepSix extends StatefulWidget {
  const CreateOfferContractStepSix(
      {super.key, required this.vm, required this.formKey});

  final FastTrackOfferViewModel vm;
  final GlobalKey<FormBuilderState> formKey;

  @override
  State<CreateOfferContractStepSix> createState() =>
      _CreateOfferContractStepSixState();
}

class _CreateOfferContractStepSixState
    extends State<CreateOfferContractStepSix> {
  bool seeMorePdf = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomTextFieldFormBuilder(
          labelText: tr('project_day_estimate'),
          hintText: tr('days_hint_text'),
          name: 'projectEstimate',
          keyboardType: TextInputType.number,
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
        SmartGaps.gapH10,
        RichText(
            text: TextSpan(children: [
          TextSpan(
              text: '${tr('your_offer_is_valid')} ',
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                  letterSpacing: 1.0,
                  color: PartnerAppColors.darkBlue,
                  fontWeight: FontWeight.normal,
                  fontSize: 18)),
          TextSpan(
              text: widget.vm.createOfferContractForm['offerSignatureValid'],
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                  letterSpacing: 1.0,
                  color: PartnerAppColors.blue,
                  fontWeight: FontWeight.bold,
                  fontSize: 18)),
          TextSpan(
              text: ' ${tr('days_valid')}',
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                  letterSpacing: 1.0,
                  color: PartnerAppColors.darkBlue,
                  fontWeight: FontWeight.normal,
                  fontSize: 18))
        ])),
        SmartGaps.gapH10,
        CustomDropdownFormBuilder(
            items: [
              ...List.generate(
                  30,
                  (index) => DropdownMenuItem(
                      value: '${index + 1}', child: Text('${index + 1}')))
            ],
            name: 'offerSignatureValid',
            initialValue:
                widget.vm.createOfferContractForm['offerSignatureValid'],
            onChanged: (p0) {
              if (p0 != null) {
                widget.vm.updateCreateOfferContractForms(
                    key: 'offerSignatureValid', value: p0);
              }
            }),
        SmartGaps.gapH10,
        Text(
          tr('customers_info'),
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              letterSpacing: 1.0,
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18),
        ),
        CustomTextFieldFormBuilder(
          name: 'customerFirstName',
          labelText: tr('first_name'),
          keyboardType: TextInputType.name,
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
        CustomTextFieldFormBuilder(
          name: 'customerLastName',
          labelText: tr('last_name'),
          keyboardType: TextInputType.name,
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
        GestureDetector(
          onTap: () async {
            final address = await addAddress(context: context);

            if (address != null) {
              widget.formKey.currentState!
                  .patchValue({'address': address.address});

              if (!mounted) return;

              widget.vm.updateCreateOfferContractForms(
                  key: 'customerAddress', value: address);
            }
          },
          child: AbsorbPointer(
            absorbing: true,
            child: CustomTextFieldFormBuilder(
              name: 'address',
              labelText: tr('complete_address'),
              validator:
                  FormBuilderValidators.required(errorText: tr('required')),
            ),
          ),
        ),
        CustomTextFieldFormBuilder(
          name: 'customerEmail',
          labelText: tr('email'),
          keyboardType: TextInputType.emailAddress,
          validator:
              FormBuilderValidators.email(errorText: tr('enter_valid_email')),
        ),
        CustomTextFieldFormBuilder(
          name: 'customerPhone',
          labelText: tr('telephone'),
          keyboardType: TextInputType.number,
          validator: FormBuilderValidators.equalLength(8,
              errorText: tr('phone_number_length')),
          inputFormatters: [LengthLimitingTextInputFormatter(8)],
          prefixIcon: Container(
            padding: const EdgeInsets.all(10),
            child: Text('+45',
                style: Theme.of(context)
                    .textTheme
                    .headlineSmall!
                    .copyWith(fontWeight: FontWeight.normal)),
          ),
        ),
      ],
    );
  }
}
