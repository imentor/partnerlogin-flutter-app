import 'dart:developer';
import 'dart:io';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/fast_track_offer/fast_track_offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:file_selector/file_selector.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';

class CreateOfferContractStepTwo extends StatefulWidget {
  const CreateOfferContractStepTwo({super.key, required this.vm});

  final FastTrackOfferViewModel vm;

  @override
  CreateOfferContractStepTwoState createState() =>
      CreateOfferContractStepTwoState();
}

class CreateOfferContractStepTwoState
    extends State<CreateOfferContractStepTwo> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('upload_offer_here'),
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              letterSpacing: 1.0,
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18),
        ),
        SmartGaps.gapH20,
        Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
              border: Border.all(
                color: PartnerAppColors.darkBlue.withValues(alpha: .3),
              ),
              borderRadius: BorderRadius.circular(8)),
          child: Column(children: [
            const Icon(
              FeatherIcons.uploadCloud,
              color: PartnerAppColors.darkBlue,
              size: 30,
            ),
            SmartGaps.gapH10,
            Text(
              tr('upload_file'),
              style: Theme.of(context).textTheme.displayLarge!.copyWith(
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: PartnerAppColors.darkBlue,
                  ),
            ),
            SmartGaps.gapH10,
            TextButton(
                style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                        side: const BorderSide(
                            color: PartnerAppColors.darkBlue))),
                onPressed: () async {
                  if (Platform.isIOS) {
                    // File Picker - added this again but only for iOS since it has issues with file selector
                    FilePickerResult? fileResultList = await FilePicker.platform
                        .pickFiles(type: FileType.custom, allowedExtensions: [
                      "pdf",
                      "jpg",
                      "jpeg",
                      "png",
                      "docx",
                      "doc",
                      "xls",
                      "xlsx",
                      "heif",
                      "hevc"
                    ]);

                    if (fileResultList != null) {
                      widget.vm.addOrDeleteOfferContractFiles(
                          files: fileResultList.files);
                    }
                  } else if (Platform.isAndroid) {
                    // File Selector - this will now only be for android since it is not working on iOS
                    const XTypeGroup fileTypeGroupAndroid = XTypeGroup(
                      label: 'files',
                      extensions: [
                        'pdf',
                        'jpg',
                        'jpeg',
                        'png',
                        'docx',
                        'doc',
                        'xls',
                        'xlsx',
                        'heif',
                        'hevc'
                      ],
                    );

                    final XFile? selectedFile = await openFile(
                        acceptedTypeGroups: <XTypeGroup>[fileTypeGroupAndroid]);

                    if (selectedFile != null) {
                      widget.vm.addOrDeleteOfferContractFileSelector(
                          file: selectedFile);
                      log(selectedFile.mimeType.toString());
                    }
                  }
                },
                child: Text(tr('upload'),
                    style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                          fontWeight: FontWeight.normal,
                          color: PartnerAppColors.darkBlue,
                        )))
          ]),
        ),
        SmartGaps.gapH10,
        ListView.separated(
          separatorBuilder: ((context, index) {
            return SmartGaps.gapH10;
          }),
          itemBuilder: (context, index) {
            return Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: PartnerAppColors.spanishGrey.withValues(alpha: .3),
                  borderRadius: BorderRadius.circular(5)),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                        // File Selector / File Picker - updated this since iOS has issues with file selector while android has issues with file picker
                        child: Text(Platform.isIOS
                            ? widget.vm.createOfferContractFiles[index].name
                            : '${widget.vm.createOfferContractFileSelector[index].name.replaceAll(RegExp('[^A-Za-z0-9]'), '')}.${widget.vm.createOfferContractFileSelector[index].mimeType!.split('/').last}')),
                    SmartGaps.gapW10,
                    GestureDetector(
                      onTap: () {
                        // File Selector / File Picker - updated this since iOS has issues with file selector while android has issues with file picker
                        if (Platform.isIOS) {
                          widget.vm.addOrDeleteOfferContractFiles(index: index);
                        } else if (Platform.isAndroid) {
                          widget.vm.addOrDeleteOfferContractFileSelector(
                              index: index);
                        }
                      },
                      child: const Icon(
                        FeatherIcons.trash2,
                        color: PartnerAppColors.spanishGrey,
                      ),
                    )
                  ]),
            );
          },
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: Platform.isIOS
              ? widget.vm.createOfferContractFiles.length
              : widget.vm.createOfferContractFileSelector.length,
        )
      ],
    );
  }
}
