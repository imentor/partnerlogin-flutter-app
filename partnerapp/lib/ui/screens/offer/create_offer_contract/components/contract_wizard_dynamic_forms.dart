import 'package:Haandvaerker.dk/model/contract/contract_template_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/contract/contract_viewmodel.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:provider/provider.dart';

class ContractWizardDynamicForms extends StatefulWidget {
  const ContractWizardDynamicForms(
      {super.key, required this.contractFields, required this.step});

  final List<ContractTemplateField> contractFields;
  final TemplateSteps step;

  @override
  State<ContractWizardDynamicForms> createState() =>
      _ContractWizardDynamicFormsState();
}

class _ContractWizardDynamicFormsState
    extends State<ContractWizardDynamicForms> {
  @override
  Widget build(BuildContext context) {
    final contractVm = context.read<ContractViewModel>();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if ((widget.step.info ?? '') != 'Addendum') ...[
          Text(widget.step.info ?? '',
              style: Theme.of(context).textTheme.headlineLarge)
        ] else ...[
          Text(tr('additions_to_contract'),
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium!
                  .copyWith(fontWeight: FontWeight.normal))
        ],
        SmartGaps.gapH20,
        ...forms(contractVm: contractVm)
      ],
    );
  }

  List<Widget> forms({required ContractViewModel contractVm}) {
    final CurrencyTextInputFormatter inputFormatter =
        CurrencyTextInputFormatter(
      NumberFormat.currency(locale: 'da', decimalDigits: 2, symbol: 'kr'),
    );
    return widget.step.fields!.map((e) {
      switch (e!.field) {
        case 0:
          if (e.label == 'Addendum' || (e.field == 0 && e.key == 3)) {
            return const SizedBox.shrink();
          } else if (e.label == 'Pris') {
            return Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: Html(
                data: tr('fixed_price_incl_vat'),
                shrinkWrap: true,
              ),
            );
          } else {
            return Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              child: Html(
                data: e.label,
                shrinkWrap: true,
              ),
            );
          }
        case 1:
          return const SizedBox.shrink();
        case 2:
          if (e.fieldName == 'ADDENDUM') {
            return CustomHtmlEditor(contractVm: contractVm);
          } else if (e.fieldName == 'PRIS') {
            return CustomTextFieldFormBuilder(
              name: '${widget.step.key}:${e.key}:${e.fieldName}',
              inputFormatters: <TextInputFormatter>[inputFormatter],
              keyboardType: TextInputType.number,
              isRequired: e.mandatory == 1,
              initialValue: (e.defaultValue == 'null' || e.defaultValue == null
                  ? ''
                  : e.defaultValue),
              validator: (val) {
                if (e.mandatory == 1 && val!.isEmpty) {
                  return tr('required');
                } else {
                  final unformattedVal = val!
                      .replaceAll('.', '')
                      .replaceFirst(',', '.')
                      .replaceAll('kr', '')
                      .trim();
                  final unformattedDefaultVal =
                      (e.defaultValue ?? '1.000,00 kr')
                          .replaceAll('.', '')
                          .replaceFirst(',', '.')
                          .replaceAll('kr', '')
                          .trim();
                  final doubleVal = double.tryParse(unformattedVal);
                  if (doubleVal == null) {
                    return tr('required');
                  } else if (doubleVal < 1000) {
                    return tr('total_should_not_be_less_than_1000');
                  } else {
                    if (unformattedVal == unformattedDefaultVal) {
                      contractVm.editContractItemCount = 5;
                    } else {
                      contractVm.editContractItemCount = 6;
                    }
                    contractVm.prisUnformattedValue = unformattedVal;
                    return null;
                  }
                }
              },
              onChanged: (val) {
                final unformattedVal = val!
                    .replaceAll('.', '')
                    .replaceFirst(',', '.')
                    .replaceAll('kr', '')
                    .trim();
                final unformattedDefaultVal = (e.defaultValue ?? '1.000,00 kr')
                    .replaceAll('.', '')
                    .replaceFirst(',', '.')
                    .replaceAll('kr', '')
                    .trim();
                final doubleVal = double.tryParse(unformattedVal);
                if (doubleVal != null) {
                  if (unformattedVal == unformattedDefaultVal) {
                    contractVm.editContractItemCount = 5;
                  } else {
                    contractVm.editContractItemCount = 6;
                  }
                  contractVm.prisUnformattedValue = unformattedVal;
                }
              },
            );
          } else {
            return CustomTextFieldFormBuilder(
              name: '${widget.step.key}:${e.key}:${e.fieldName}',
              labelText: e.label,
              isRequired: e.mandatory == 1,
              initialValue: (e.defaultValue == 'null' || e.defaultValue == null
                  ? ''
                  : e.defaultValue),
              validator: (val) {
                if (e.mandatory == 1 && val!.isEmpty) {
                  return tr('required');
                }
                return null;
              },
              tooltip: e.help!.isNotEmpty
                  ? Tooltip(
                      message: e.help,
                      child: Icon(Icons.info,
                          color: Theme.of(context)
                              .colorScheme
                              .primary
                              .withValues(alpha: .4)),
                    )
                  : null,
            );
          }
        case 3:
          return CustomRadioGroupFormBuilder(
            name: '${widget.step.key}:${e.key}:${e.defaultType}:${e.fieldName}',
            options: [
              ...e.values!.map((radioValue) {
                return FormBuilderFieldOption(
                  value: radioValue,
                  child: Text(
                    radioValue,
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                          color: PartnerAppColors.darkBlue,
                          fontWeight: FontWeight.normal,
                        ),
                  ),
                );
              })
            ],
            labelText: e.label,
            isRequired: e.mandatory == 1,
            initialValue: e.defaultValue,
            validator: (val) {
              if (e.mandatory == 1 && val!.isEmpty) {
                return tr('required');
              }
              return null;
            },
            tooltip: e.help!.isNotEmpty
                ? Tooltip(
                    message: e.help,
                    child: Icon(Icons.info,
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withValues(alpha: .4)),
                  )
                : null,
          );
        case 4:
          return CustomCheckboxGroupFormBuilder(
            name: '${widget.step.key}:${e.key}:${e.defaultType}:${e.fieldName}',
            options: [
              ...e.values!.map((checkValue) {
                return FormBuilderFieldOption(
                  value: checkValue,
                  child: Text(
                    checkValue,
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                          color: PartnerAppColors.darkBlue,
                          fontWeight: FontWeight.normal,
                        ),
                  ),
                );
              })
            ],
            labelText: e.label,
            isRequired: e.mandatory == 1,
            initialValue: [e.defaultValue ?? ''],
            validator: (val) {
              if (e.mandatory == 1 && val!.isEmpty) {
                return tr('required');
              }
              return null;
            },
            tooltip: e.help!.isNotEmpty
                ? Tooltip(
                    message: e.help,
                    child: Icon(Icons.info,
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withValues(alpha: .4)),
                  )
                : null,
          );
        case 7:
          return CustomDropdownFormBuilder(
            name: '${widget.step.key}:${e.key}:${e.defaultType}:${e.fieldName}',
            items: [
              ...e.values!.map((e) => DropdownMenuItem(
                    value: e,
                    child: Text(e,
                        style: Theme.of(context)
                            .textTheme
                            .bodyMedium!
                            .copyWith(fontWeight: FontWeight.w500)),
                  )),
            ],
            labelText: e.label,
            isRequired: e.mandatory == 1,
            initialValue: e.defaultValue,
            validator: (val) {
              if (e.mandatory == 1 && val!.isEmpty) {
                return tr('required');
              }
              return null;
            },
            tooltip: e.help!.isNotEmpty
                ? Tooltip(
                    message: e.help,
                    child: Icon(Icons.info,
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withValues(alpha: .4)),
                  )
                : null,
          );
        case 8:
          return CustomTextFieldFormBuilder(
            name: '${widget.step.key}:${e.key}:${e.fieldName}',
            labelText: e.label,
            isRequired: e.mandatory == 1,
            initialValue: (e.defaultValue == 'null' || e.defaultValue == null
                ? ''
                : e.defaultValue),
            minLines: 5,
            validator: (val) {
              if (e.mandatory == 1 && val!.isEmpty) {
                return tr('required');
              }
              return null;
            },
            tooltip: e.help!.isNotEmpty
                ? Tooltip(
                    message: e.help,
                    child: Icon(Icons.info,
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withValues(alpha: .4)),
                  )
                : null,
          );
        case 10:
          DateTime? initialValue;

          if (e.defaultValue != null && (e.defaultValue ?? '').isNotEmpty) {
            initialValue = DateTime.parse(e.defaultValue!);
          }

          return CustomDatePickerFormBuilder(
            name: '${widget.step.key}:${e.key}:${e.fieldName}',
            labelText: e.label,
            isRequired: e.mandatory == 1,
            initialValue: initialValue,
            initialDate: DateTime.now(),
            dateFormat: DateFormatType.europeanDate.formatter,
            suffixIcon: const Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: Icon(Icons.event, color: PartnerAppColors.blue),
            ),
            validator: e.mandatory == 1
                ? FormBuilderValidators.required(errorText: tr('required'))
                : null,
          );
        default:
          return const SizedBox.shrink();
      }
    }).toList();
  }
}

class CustomHtmlEditor extends StatefulWidget {
  const CustomHtmlEditor({super.key, required this.contractVm});

  final ContractViewModel contractVm;

  @override
  State<CustomHtmlEditor> createState() => _CustomHtmlEditorState();
}

class _CustomHtmlEditorState extends State<CustomHtmlEditor> {
  final HtmlEditorController htmlController =
      HtmlEditorController(processNewLineAsBr: true);
  @override
  Widget build(BuildContext context) {
    return HtmlEditor(
      controller: htmlController,
      htmlEditorOptions: HtmlEditorOptions(
        hint: '${tr('message')}...',
        shouldEnsureVisible: true,
      ),
      htmlToolbarOptions: const HtmlToolbarOptions(
        toolbarPosition: ToolbarPosition.aboveEditor,
        dropdownBoxDecoration: BoxDecoration(),
        defaultToolbarButtons: [
          FontButtons(
              italic: false,
              clearAll: false,
              strikethrough: false,
              superscript: false,
              subscript: false),
          ListButtons(listStyles: false)
        ],
      ),
      callbacks: Callbacks(onInit: () {
        htmlController.setText(widget.contractVm.addendumText);
      }, onChangeContent: (String? value) {
        widget.contractVm.addendumText = value ?? '';
      }),
    );
  }
}
