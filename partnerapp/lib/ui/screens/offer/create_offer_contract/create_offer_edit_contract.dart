import 'package:Haandvaerker.dk/model/contract/contract_template_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer_contract/components/contract_wizard_dynamic_forms.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/fast_track_offer/fast_track_offer_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';

class CreateOfferEditContract extends StatefulWidget {
  const CreateOfferEditContract({super.key});

  @override
  State<CreateOfferEditContract> createState() =>
      _CreateOfferEditContractState();
}

class _CreateOfferEditContractState extends State<CreateOfferEditContract> {
  final formKey = GlobalKey<FormBuilderState>();

  void onPressed() async {
    if (formKey.currentState!.validate()) {
      final fastTrackVm = context.read<FastTrackOfferViewModel>();

      Map<String, dynamic> newMap = {};
      for (var element in formKey.currentState!.fields.entries) {
        newMap[element.key] = element.value.value;
      }

      modalManager.showLoadingModal();

      await tryCatchWrapper(
          context: context,
          function: fastTrackVm.updateContract(payload: newMap));

      if (!mounted) return;

      modalManager.hideLoadingModal();

      await showOkAlertDialog(context: context, message: tr('contract_updated'))
          .whenComplete(() {
        backDrawerRoute();

        changeDrawerRoute(Routes.contructionWalletContractPreview);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      bottomNavigationBar: Container(
        padding:
            const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
        color: Colors.white,
        child: TextButton(
          style: TextButton.styleFrom(
              fixedSize: Size(MediaQuery.of(context).size.width, 45),
              backgroundColor: PartnerAppColors.darkBlue,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              )),
          onPressed: () => onPressed(),
          child: Text(
            tr('next'),
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(color: Colors.white),
          ),
        ),
      ),
      body: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: Consumer<FastTrackOfferViewModel>(
            builder: (_, vm, __) {
              return FormBuilder(
                key: formKey,
                child: Column(
                  children: [..._contractForms(vm: vm)],
                ),
              );
            },
          )),
    );
  }

  List<Widget> _contractForms({required FastTrackOfferViewModel vm}) {
    final List<Widget> contractSections = [];

    final template =
        vm.createOfferContractForm['contractTemplate'] as ContractTemplateModel;

    for (final item in (template.steps as Map<String, dynamic>).values) {
      final template = TemplateSteps.fromJson(item as Map<String, dynamic>);

      contractSections.add(Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: ContractWizardDynamicForms(
            contractFields: (vm.contractTemplate?.fields ?? [])
                .where((field) => field != null)
                .cast<ContractTemplateField>()
                .toList(),
            step: template),
      ));
    }

    return contractSections;
  }
}
