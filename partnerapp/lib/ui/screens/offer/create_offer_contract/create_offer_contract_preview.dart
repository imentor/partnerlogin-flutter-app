import 'dart:io';

import 'package:Haandvaerker.dk/model/contract/contract_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/signature_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/fast_track_offer/fast_track_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class CreateOfferContractPreview extends StatefulWidget {
  const CreateOfferContractPreview({super.key});

  @override
  State<CreateOfferContractPreview> createState() =>
      _CreateOfferContractPreviewState();
}

class _CreateOfferContractPreviewState
    extends State<CreateOfferContractPreview> {
  bool isLoadingPdf = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Consumer<FastTrackOfferViewModel>(builder: (_, vm, __) {
        return Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                tr('contract'),
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              ),
              SmartGaps.gapH10,
              if (((vm.createOfferContractForm['contract'] as ContractModel)
                          .dateSignedByPartner ??
                      '')
                  .isEmpty) ...[
                GestureDetector(
                  onTap: () async {
                    changeDrawerRoute(Routes.contructionWalletEditContract);
                  },
                  child:
                      Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                    const Icon(
                      FeatherIcons.edit,
                      color: PartnerAppColors.blue,
                      size: 14,
                    ),
                    SmartGaps.gapW5,
                    Text(
                      tr('edit_contract'),
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(
                              letterSpacing: 1.0,
                              color: PartnerAppColors.blue,
                              fontWeight: FontWeight.normal,
                              fontSize: 14),
                    ),
                  ]),
                ),
                SmartGaps.gapH10,
              ],
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    color: PartnerAppColors.spanishGrey.withValues(alpha: .2),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  padding: const EdgeInsets.all(10),
                  child: isLoadingPdf
                      ? const Center(child: CircularProgressIndicator())
                      : SfPdfViewer.file(
                          File(vm
                              .createOfferContractForm['contractPdfFilePath']),
                        ),
                ),
              ),
              SmartGaps.gapH20,
              SizedBox(
                height: 45,
                width: MediaQuery.of(context).size.width,
                child: CustomDesignTheme.flatButtonStyle(
                  backgroundColor:
                      ((vm.createOfferContractForm['contract'] as ContractModel)
                                      .dateSignedByPartner ??
                                  '')
                              .isEmpty
                          ? PartnerAppColors.malachite
                          : PartnerAppColors.spanishGrey,
                  onPressed: () async {
                    if (((vm.createOfferContractForm['contract']
                                    as ContractModel)
                                .dateSignedByPartner ??
                            '')
                        .isEmpty) {
                      await signatureDialog(
                              context: context,
                              label: context
                                  .read<SharedDataViewmodel>()
                                  .termsLabel,
                              htmlTerms:
                                  context.read<SharedDataViewmodel>().termsHtml,
                              buttonLabel: 'submit')
                          .then((value) async {
                        if (value != null) {
                          setState(() {
                            isLoadingPdf = true;
                          });

                          await tryCatchWrapper(
                                  context:
                                      myGlobals.homeScaffoldKey!.currentContext,
                                  function: vm.signContract(signature: value))
                              .then((val) {
                            setState(() {
                              isLoadingPdf = false;
                            });
                          });
                        }
                      });
                    }
                  },
                  child: Text(
                    tr('sign'),
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
        );
      }),
    );
  }
}
