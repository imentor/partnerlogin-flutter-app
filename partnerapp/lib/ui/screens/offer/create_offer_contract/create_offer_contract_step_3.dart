import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/fast_track_offer/fast_track_offer_viewmodel.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class CreateOfferContractStepThree extends StatefulWidget {
  const CreateOfferContractStepThree({super.key, required this.vm});
  final FastTrackOfferViewModel vm;

  @override
  State<CreateOfferContractStepThree> createState() =>
      _CreateOfferContractStepThreeState();
}

class _CreateOfferContractStepThreeState
    extends State<CreateOfferContractStepThree> {
  double totalProjectPrice = 0;
  final CurrencyTextInputFormatter _totalProjectPriceFormatter =
      CurrencyTextInputFormatter(NumberFormat.currency(
    locale: 'da',
    decimalDigits: 2,
    symbol: '',
  ));

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if (widget.vm.createOfferContractForm.containsKey('projectPrice')) {
        setState(() {
          totalProjectPrice =
              double.parse(widget.vm.createOfferContractForm['projectPrice']);
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomTextFieldFormBuilder(
          name: 'projectName',
          labelText: tr('project_name'),
          validator: FormBuilderValidators.required(errorText: tr('required')),
        ),
        CustomTextFieldFormBuilder(
          validator: FormBuilderValidators.compose([
            (val) {
              if (val != null) {
                if (val.isEmpty) {
                  return tr('required');
                } else if (val == '0') {
                  return tr('price_cannot_be_lower_than_0');
                } else if ((totalProjectPrice + (totalProjectPrice * .25)) <
                    1000) {
                  return tr('total_should_not_be_less_than_1000');
                }
              } else {
                return null;
              }
              return null;
            }
          ]),
          name: 'projectPrice',
          labelText: tr('total_project_price'),
          initialValue: '0',
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[_totalProjectPriceFormatter],
          textAlign: TextAlign.end,
          suffixIcon: Container(
            padding:
                const EdgeInsets.only(left: 10, right: 10, bottom: 4, top: 12),
            child: Text(
              'kr.',
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    fontSize: 18,
                    fontWeight: FontWeight.normal,
                    color: PartnerAppColors.darkBlue,
                  ),
            ),
          ),
          onChanged: (p0) {
            if (p0 != null && p0.isNotEmpty) {
              setState(() {
                totalProjectPrice = _totalProjectPriceFormatter
                    .getUnformattedValue()
                    .toDouble();
              });
            } else {
              setState(() {
                totalProjectPrice = 0;
              });
            }
          },
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(tr('total_price_exl_vat'),
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: PartnerAppColors.darkBlue,
                    fontSize: 16,
                    fontWeight: FontWeight.normal)),
            Text(
                '${NumberFormat.decimalPattern('da').format(totalProjectPrice)} kr.',
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: PartnerAppColors.darkBlue,
                    fontSize: 16,
                    fontWeight: FontWeight.normal))
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(tr('vat_value'),
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: PartnerAppColors.darkBlue,
                    fontSize: 16,
                    fontWeight: FontWeight.normal)),
            Text(
                '${NumberFormat.decimalPattern('da').format(totalProjectPrice * .25)} kr.',
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: PartnerAppColors.darkBlue,
                    fontSize: 16,
                    fontWeight: FontWeight.normal))
          ],
        ),
        SmartGaps.gapH5,
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(tr('total_include_vat'),
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: PartnerAppColors.darkBlue,
                    fontSize: 16,
                    fontWeight: FontWeight.normal)),
            Text(
                '${NumberFormat.decimalPattern('da').format(totalProjectPrice + (totalProjectPrice * .25))} kr.',
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: PartnerAppColors.darkBlue,
                    fontSize: 16,
                    fontWeight: FontWeight.normal))
          ],
        ),
      ],
    );
  }
}
