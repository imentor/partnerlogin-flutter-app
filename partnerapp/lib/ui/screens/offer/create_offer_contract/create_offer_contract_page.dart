import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer_contract/create_offer_contract_step_1.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer_contract/create_offer_contract_step_2.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer_contract/create_offer_contract_step_3.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer_contract/create_offer_contract_step_4.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer_contract/create_offer_contract_step_5.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer_contract/create_offer_contract_step_6.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer_contract/create_offer_contract_step_7.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/fast_track_offer/fast_track_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/widgets_recommendations_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:provider/provider.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:timeline_tile/timeline_tile.dart';

class CreateOfferContractPage extends StatefulWidget {
  const CreateOfferContractPage({super.key});

  @override
  State<CreateOfferContractPage> createState() =>
      _CreateOfferContractPageState();
}

class _CreateOfferContractPageState extends State<CreateOfferContractPage> {
  final formKey = GlobalKey<FormBuilderState>();
  final ItemScrollController itemScrollController = ItemScrollController();

  int tempSteps = 0;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      modalManager
          .setContext(myGlobals.homeScaffoldKey?.currentContext ?? context);

      final vm = context.read<FastTrackOfferViewModel>();
      final newsFeedVm = context.read<NewsFeedViewModel>();
      final appDrawerVm = context.read<AppDrawerViewModel>();
      const dynamicStoryPage = 'fastcontract';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      if (dynamicStoryEnable) {
        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: Future.wait([
              vm.initCreateOfferWithContract(),
              newsFeedVm.getDynamicStory(page: dynamicStoryPage),
            ])).whenComplete(() {
          if (!mounted) return;

          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      } else {
        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: Future.value(vm.initCreateOfferWithContract()));
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      bottomNavigationBar: buttonNav(),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: FormBuilder(
          key: formKey,
          child: Consumer<FastTrackOfferViewModel>(
            builder: (_, vm, __) {
              if (itemScrollController.isAttached &&
                  (tempSteps == vm.createOfferContractStep)) {
                itemScrollController.scrollTo(
                    index: vm.createOfferContractStep,
                    duration: const Duration(seconds: 2),
                    curve: Curves.easeInOutCubic);
              }
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if ((vm.createOfferContractStep + 1) < 8) ...[
                    SizedBox(
                      height: 65,
                      width: MediaQuery.of(context).size.width,
                      child: ScrollablePositionedList.builder(
                          initialAlignment: 0,
                          scrollDirection: Axis.horizontal,
                          itemScrollController: itemScrollController,
                          shrinkWrap: true,
                          itemCount: 7,
                          itemBuilder: (context, index) {
                            final isCurrentPage =
                                index <= vm.createOfferContractStep;
                            return TimelineTile(
                              axis: TimelineAxis.horizontal,
                              alignment: TimelineAlign.center,
                              isFirst: index == 0,
                              isLast: index == 7 - 1,
                              indicatorStyle: IndicatorStyle(
                                width: 30.0,
                                height: 30.0,
                                indicator: DecoratedBox(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        color: PartnerAppColors.blue),
                                    color: isCurrentPage
                                        ? PartnerAppColors.blue
                                        : Colors.white,
                                  ),
                                  child: Center(
                                      child: Text('${index + 1}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodySmall!
                                              .copyWith(
                                                  color: isCurrentPage
                                                      ? Colors.white
                                                      : PartnerAppColors.blue,
                                                  fontWeight:
                                                      FontWeight.bold))),
                                ),
                              ),
                              beforeLineStyle: const LineStyle(
                                  thickness: 1.0,
                                  color: PartnerAppColors.spanishGrey),
                              endChild: Container(
                                margin: const EdgeInsets.only(top: 5),
                                constraints: const BoxConstraints(
                                  maxHeight: 30,
                                  minWidth: 50,
                                ),
                                child: Text('${tr('step')} ${index + 1}',
                                    style:
                                        Theme.of(context).textTheme.bodySmall,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.center),
                              ),
                            );
                          }),
                    ),
                    SmartGaps.gapH20,
                  ],
                  step(
                    step: vm.createOfferContractStep,
                    formKey: formKey,
                    vm: vm,
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Widget buttonNav() {
    return Consumer<FastTrackOfferViewModel>(builder: (_, vm, __) {
      return Container(
        padding:
            const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
        color: Colors.white,
        child: Row(children: [
          if (vm.createOfferContractStep != 0) ...[
            Expanded(
                child: TextButton(
                    style: TextButton.styleFrom(
                        fixedSize: Size(MediaQuery.of(context).size.width, 45),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                            side: const BorderSide(
                                color: PartnerAppColors.darkBlue))),
                    onPressed: () {
                      vm.updateCreateOfferContractStep(
                          step: vm.createOfferContractStep - 1);
                      tempSteps--;
                    },
                    child: Text(
                      tr('back'),
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(color: PartnerAppColors.darkBlue),
                    ))),
            SmartGaps.gapW20,
          ],
          Expanded(
              child: TextButton(
                  style: TextButton.styleFrom(
                      fixedSize: Size(MediaQuery.of(context).size.width, 45),
                      backgroundColor: PartnerAppColors.darkBlue,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      )),
                  onPressed: () async {
                    switch (vm.createOfferContractStep + 1) {
                      case 1:
                        if (vm.createOfferContractForm
                            .containsKey('contractType')) {
                          vm.updateCreateOfferContractStep(
                              step: vm.createOfferContractStep + 1);
                          tempSteps++;
                        }
                        break;
                      case 2:
                        // File Selector - added second condition since file_picker is returning null on android devices
                        if (vm.createOfferContractFiles.isNotEmpty ||
                            vm.createOfferContractFileSelector.isNotEmpty) {
                          vm.updateCreateOfferContractStep(
                              step: vm.createOfferContractStep + 1);
                          tempSteps++;
                        }
                        break;
                      case 3:
                        if (formKey.currentState!.validate()) {
                          formKey.currentState!.fields.forEach((key, value) {
                            vm.updateCreateOfferContractForms(
                                key: key,
                                value:
                                    '${value.value.replaceAll('.', '').replaceAll(',', '.')}');
                          });

                          vm.updateCreateOfferContractForms(
                              key: 'priceHasVat', value: false);
                          vm.updateCreateOfferContractStep(
                              step: vm.createOfferContractStep + 1);
                          tempSteps++;
                        }
                        break;
                      case 4:
                        if (vm.selectedIndustriesOfferWithContract.isNotEmpty) {
                          modalManager.showLoadingModal();

                          await vm.getListPrices().then((value) {
                            tempSteps++;
                            modalManager.hideLoadingModal();
                          });
                        }
                        break;
                      case 5:
                        double totalPriceWithVat = 0;

                        for (var industry in vm.industryDescriptions) {
                          for (var subIndutry
                              in industry['subIndustry'] as List) {
                            totalPriceWithVat += subIndutry['priceVat'];
                          }
                        }
                        if (totalPriceWithVat != vm.tempTotalPrice) {
                          totalPrisErrorPopupDialog(
                              context: context,
                              message: tr('total_pris_error_txt'));
                        } else {
                          vm.updateCreateOfferContractStep(
                              step: vm.createOfferContractStep + 1);
                          tempSteps++;
                        }
                        break;
                      case 6:
                        if (formKey.currentState!.validate()) {
                          formKey.currentState!.fields.forEach((key, value) {
                            vm.updateCreateOfferContractForms(
                                key: key, value: '${value.value}');
                          });

                          vm.updateCreateOfferContractStep(
                              step: vm.createOfferContractStep + 1);
                          tempSteps++;
                        }
                        break;
                      case 7:
                        if (formKey.currentState!.validate()) {
                          final supplierInfo = context.read<WidgetsViewModel>();
                          final clientVm = context.read<ClientsViewModel>();
                          final tenderFolderVm =
                              context.read<TenderFolderViewModel>();
                          final createOfferVm =
                              context.read<CreateOfferViewModel>();

                          modalManager.showLoadingModal();

                          await vm.createContactAddressProjectOffer(
                              contractPayload: {
                                "name": supplierInfo.company!.bCrmCompany
                                                ?.contactPerson !=
                                            null &&
                                        supplierInfo.company!.bCrmCompany!
                                            .contactPerson!
                                            .contains(' ')
                                    ? supplierInfo
                                        .company!.bCrmCompany!.contactPerson!
                                        .split(' ')[0]
                                    : supplierInfo.company!.bCrmCompany!.title!
                                        .split(' ')[0],
                                "lastname": supplierInfo.company!.bCrmCompany
                                                ?.contactPerson !=
                                            null &&
                                        supplierInfo.company!.bCrmCompany!
                                            .contactPerson!
                                            .contains(' ') &&
                                        supplierInfo.company!.bCrmCompany!
                                                .contactPerson!
                                                .split(' ')
                                                .length >
                                            1
                                    ? supplierInfo
                                        .company!.bCrmCompany!.contactPerson!
                                        .split(' ')[1]
                                    : supplierInfo.company!.bCrmCompany!.title!
                                        .split(' ')[1],
                                "address": supplierInfo
                                        .company!.bCrmCompany?.companyAddress ??
                                    '',
                                "zipcode": supplierInfo
                                        .company!.bCrmCompany?.zipCode
                                        .toString() ??
                                    '',
                                "email":
                                    supplierInfo.company!.bCrmCompany?.email ??
                                        '',
                                "mobile": supplierInfo
                                        .company!.bCrmCompany?.mobile
                                        .toString() ??
                                    '',
                                "city":
                                    supplierInfo.company!.bCrmCompany?.city ??
                                        '',
                                "cvr": supplierInfo.company!.bCrmCompany?.cvr ??
                                    '',
                                "companyName": supplierInfo
                                        .company!.bCrmCompany?.companyName ??
                                    '',
                                "ENDDATE": (formKey.currentState!
                                        .fields['offer_end']!.value as DateTime)
                                    .toString()
                                    .split(' ')
                                    .first,
                                "STARTDATE": (formKey
                                        .currentState!
                                        .fields['offer_start']!
                                        .value as DateTime)
                                    .toString()
                                    .split(' ')
                                    .first,
                              },
                              isAffiliate: (context
                                          .read<AppDrawerViewModel>()
                                          .supplierProfileModel
                                          ?.bCrmCompany
                                          ?.companyPackge ??
                                      '') ==
                                  "affiliatepartner").then((value) async {
                            // modalManager.hideLoadingModal();
                            await tryCatchWrapper(
                                    context: myGlobals
                                        .homeScaffoldKey!.currentContext,
                                    function: clientVm.getClients(
                                        tenderVm: tenderFolderVm,
                                        createOfferVm: createOfferVm))
                                .then((value) {
                              if (!mounted) return;

                              modalManager.hideLoadingModal();

                              backDrawerRoute();
                              changeDrawerRoute(Routes.yourClient,
                                  arguments: {'skipStory': true});
                              changeDrawerRoute(Routes.contractPreview);
                            });
                          });
                        }

                        break;
                      default:
                    }
                  },
                  child: Text(
                    tr('next'),
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium!
                        .copyWith(color: Colors.white),
                  ))),
        ]),
      );
    });
  }

  Future<bool?> priceHasVatDialog({
    required BuildContext context,
  }) {
    bool isConfirmYes = false;
    return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return StatefulBuilder(builder: (context, setState) {
          return AlertDialog(
            insetPadding: const EdgeInsets.all(20),
            contentPadding: const EdgeInsets.all(20),
            title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
              InkWell(
                onTap: () {
                  Navigator.of(dialogContext).pop();
                },
                child: const Icon(
                  FeatherIcons.x,
                  color: PartnerAppColors.spanishGrey,
                ),
              )
            ]),
            content: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      isConfirmYes
                          ? tr('are_you_sure_has_vat')
                          : tr('does_it_include_vat'),
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.displayLarge!.copyWith(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: PartnerAppColors.darkBlue,
                          ),
                    ),
                    SmartGaps.gapH20,
                    CustomDesignTheme.flatButtonStyle(
                      height: 50,
                      backgroundColor: PartnerAppColors.darkBlue,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                      onPressed: () {
                        if (!isConfirmYes) {
                          setState(() {
                            isConfirmYes = true;
                          });
                        } else {
                          Navigator.of(dialogContext).pop(true);
                        }
                      },
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              isConfirmYes ? tr('yes_im_sure') : tr('yes'),
                              style: Theme.of(context)
                                  .textTheme
                                  .displayLarge!
                                  .copyWith(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.white,
                                  ),
                            )
                          ]),
                    ),
                    SmartGaps.gapH10,
                    CustomDesignTheme.flatButtonStyle(
                      height: 50,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                          side: const BorderSide(
                              color: PartnerAppColors.darkBlue)),
                      onPressed: () async {
                        Navigator.of(dialogContext).pop(false);
                      },
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              isConfirmYes ? tr('include_vat') : tr('no'),
                              style: Theme.of(context)
                                  .textTheme
                                  .displayLarge!
                                  .copyWith(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400,
                                    color: PartnerAppColors.darkBlue,
                                  ),
                            )
                          ]),
                    )
                  ],
                ))),
          );
        });
      },
    );
  }

  Widget step(
      {required int step,
      required GlobalKey<FormBuilderState> formKey,
      required FastTrackOfferViewModel vm}) {
    switch (step + 1) {
      case 1:
        return CreateOfferContractStepOne(
          vm: vm,
        );
      case 2:
        return CreateOfferContractStepTwo(
          vm: vm,
        );
      case 3:
        return CreateOfferContractStepThree(
          vm: vm,
        );
      case 4:
        return CreateOfferContractStepFour(
          vm: vm,
        );
      case 5:
        return CreateOfferContractStepFive(
          vm: vm,
        );
      case 6:
        return CreateOfferContractStepSix(
          vm: vm,
          formKey: formKey,
        );
      case 7:
        return CreateOfferContractStepSeven(vm: vm, formKey: formKey);
      default:
        return CreateOfferContractStepOne(
          vm: vm,
        );
    }
  }
}

Future<void> totalPrisErrorPopupDialog(
    {required BuildContext context, required String message}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          GestureDetector(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(Icons.close),
          )
        ]),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: TotalPrisErrorPopup(message: message))),
      );
    },
  );
}

class TotalPrisErrorPopup extends StatelessWidget {
  const TotalPrisErrorPopup({super.key, required this.message});
  final String message;

  @override
  Widget build(BuildContext context) {
    final centeredMessageContent =
        '<div style="text-align= center;"> $message </div>';
    return Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 4),
          child: HtmlWidget(centeredMessageContent,
              textStyle: context.pttBodyLarge.copyWith(color: Colors.black))),
      SmartGaps.gapH20,
      CustomDesignTheme.flatButtonStyle(
          width: MediaQuery.of(context).size.width,
          height: 50,
          backgroundColor: PartnerAppColors.malachite,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('OK',
              style: Theme.of(context).textTheme.displayLarge!.copyWith(
                  fontSize: 18,
                  fontWeight: FontWeight.w400,
                  color: Colors.white)))
    ]);
  }
}
