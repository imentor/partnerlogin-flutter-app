import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class ProjectOfferDetail extends StatefulWidget {
  const ProjectOfferDetail({super.key});

  @override
  State<ProjectOfferDetail> createState() => _ProjectOfferDetailState();
}

class _ProjectOfferDetailState extends State<ProjectOfferDetail> {
  late bool isLoading;
  @override
  void initState() {
    isLoading = true;
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final projectVm = context.read<MyProjectsViewModel>();
      final offerVm = context.read<OfferViewModel>();

      if ((projectVm.activeProject.offers ?? []).isNotEmpty &&
          projectVm.activeProject.offers!.first.id != null) {
        // offerVm.setBusy(true);

        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: Future.wait([
              offerVm.getOfferById(
                  offerId: projectVm.activeProject.offers!.first.id!)
            ])).whenComplete(() => isLoading = false);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Consumer<OfferViewModel>(builder: (_, vm, __) {
        return Skeletonizer(
          enabled: isLoading,
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(tr('offer_detail'),
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium!
                        .copyWith(color: PartnerAppColors.darkBlue)),
                SmartGaps.gapH30,
                Flexible(
                  child: Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: PartnerAppColors.spanishGrey
                              .withValues(alpha: .3),
                          borderRadius: BorderRadius.circular(5)),
                      child: (vm.viewedOffer?.offerFile ?? '').isEmpty
                          ? null
                          : SfPdfViewer.network(vm.viewedOffer!.offerFile!)),
                )
              ],
            ),
          ),
        );
      }),
    );
  }
}
