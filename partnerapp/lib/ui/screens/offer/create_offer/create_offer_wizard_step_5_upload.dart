import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/string_utils.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class CreateOfferWizardStepFiveUpload extends StatefulWidget {
  const CreateOfferWizardStepFiveUpload({super.key, required this.offerVm});
  final OfferViewModel offerVm;

  @override
  State<CreateOfferWizardStepFiveUpload> createState() =>
      _CreateOfferWizardStepFiveUploadState();
}

class _CreateOfferWizardStepFiveUploadState
    extends State<CreateOfferWizardStepFiveUpload> {
  final CurrencyTextInputFormatter _subIndustryPriceFormatter =
      CurrencyTextInputFormatter(
          NumberFormat.currency(locale: 'da', decimalDigits: 2, symbol: ''));

  double totalPriceWithoutVat = 0;
  double totalPriceWithVat = 0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      for (final industry in widget.offerVm.industryDescriptions) {
        for (final subIndustry in industry['subIndustry'] as List) {
          totalPriceWithoutVat += subIndustry['price'];
          totalPriceWithVat += subIndustry['priceVat'];
        }
      }
      widget.offerVm.tempTotalPrice = totalPriceWithVat;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(tr('allocate_total_amount'),
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              letterSpacing: 1.0,
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18)),
      SmartGaps.gapH20,
      ListView.separated(
          itemCount: widget.offerVm.industryDescriptions.length,
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          separatorBuilder: (context, index) {
            return SmartGaps.gapH10;
          },
          itemBuilder: (context, index) {
            final industryMap = widget.offerVm.industryDescriptions[index];
            return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ...(industryMap['subIndustry'] as List).map((sub) {
                    final subIndustryIndex =
                        (industryMap['subIndustry'] as List).indexOf(sub);

                    final subIndustryMap = sub as Map<String, dynamic>;

                    return subIndustryList(
                        industryIndex: index,
                        subIndustryIndex: subIndustryIndex,
                        subIndustryMap: subIndustryMap,
                        offerVm: widget.offerVm);
                  })
                ]);
          }),
      SmartGaps.gapH20,
      const Divider(),
      SmartGaps.gapH20,
      Builder(builder: (context) {
        return Column(children: [
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                    child: Text(tr('total_price'),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                letterSpacing: 1.0,
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 16))),
                Flexible(
                    child: Text(
                        Formatter.curencyFormat(amount: totalPriceWithVat),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                letterSpacing: 1.0,
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 16)))
              ]),
          SmartGaps.gapH10,
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                    child: Text(tr('total_price_exl_vat'),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                letterSpacing: 1.0,
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 16))),
                Flexible(
                    child: Text(
                        Formatter.curencyFormat(amount: totalPriceWithoutVat),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                letterSpacing: 1.0,
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 16)))
              ]),
          SmartGaps.gapH10,
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                    child: Text(tr('total_price_with_vat_fee'),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                letterSpacing: 1.0,
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 16))),
                Flexible(
                    child: Text(
                        Formatter.curencyFormat(
                            amount: totalPriceWithVat +
                                widget.offerVm.createOfferWizardForms[
                                    'projectPriceFees']),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                letterSpacing: 1.0,
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 16)))
              ])
        ]);
      })
    ]);
  }

  Widget subIndustryList(
      {required int industryIndex,
      required int subIndustryIndex,
      required Map<String, dynamic> subIndustryMap,
      required OfferViewModel offerVm}) {
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            border: Border.all(
                color: PartnerAppColors.darkBlue.withValues(alpha: .3))),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Flexible(
                child: Text(
                    (subIndustryMap['subIndustryName'] as String)
                        .toCapitalizedFirst(),
                    style: Theme.of(context).textTheme.titleSmall!.copyWith(
                        fontSize: 20,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal))),
            SmartGaps.gapW10,
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              InkWell(
                  onTap: () {
                    updatePrice(
                        context: context,
                        industryIndex: industryIndex,
                        subIndustryIndex: subIndustryIndex,
                        offerVm: offerVm,
                        subIndustry:
                            (subIndustryMap['subIndustryName'] as String)
                                .toCapitalizedFirst(),
                        price: subIndustryMap['priceVat']);
                  },
                  child: Container(
                      padding: const EdgeInsets.all(10),
                      width: 150,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(3),
                          border: Border.all(
                              color: PartnerAppColors.darkBlue
                                  .withValues(alpha: .4))),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(NumberFormat.currency(
                                    locale: 'da',
                                    symbol: 'kr.',
                                    decimalDigits:
                                        subIndustryMap['priceVat'] == 0 ? 0 : 2)
                                .format(subIndustryMap['priceVat']))
                          ]))),
              SmartGaps.gapW10,
              InkWell(
                  onTap: () async {
                    modalManager.showLoadingModal();

                    await offerVm
                        .deleteIndustry(
                            industryIndex: industryIndex,
                            subIndustryIndex: subIndustryIndex)
                        .then((value) {
                      modalManager.hideLoadingModal();
                    });
                  },
                  child: const Icon(FeatherIcons.trash2,
                      color: PartnerAppColors.red, size: 30))
            ])
          ])
        ]));
  }

  Future<void> updatePrice(
      {required BuildContext context,
      required int industryIndex,
      required int subIndustryIndex,
      required String subIndustry,
      required double price,
      required OfferViewModel offerVm}) {
    final formKey = GlobalKey<FormBuilderState>();
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext dialogContext) {
          return AlertDialog(
              insetPadding: const EdgeInsets.all(20),
              contentPadding: const EdgeInsets.all(20),
              title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                GestureDetector(
                    onTap: () {
                      Navigator.of(dialogContext).pop();
                    },
                    child: const Icon(Icons.close, size: 18))
              ]),
              content: FormBuilder(
                  key: formKey,
                  child: SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: SingleChildScrollView(
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                            Text(subIndustry,
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium!
                                    .copyWith(
                                        color: PartnerAppColors.darkBlue,
                                        fontWeight: FontWeight.bold)),
                            CustomTextFieldFormBuilder(
                                validator: FormBuilderValidators.required(
                                    errorText: tr('required')),
                                initialValue: price.toStringAsFixed(2),
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  _subIndustryPriceFormatter
                                ],
                                name: 'subIndustryPrice',
                                suffixIcon: Container(
                                    padding: const EdgeInsets.all(10),
                                    child: Text('kr.',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headlineMedium!
                                            .copyWith(
                                                fontSize: 18,
                                                fontWeight: FontWeight.normal,
                                                color: PartnerAppColors
                                                    .darkBlue)))),
                            SmartGaps.gapH10,
                            TextButton(
                                style: TextButton.styleFrom(
                                    fixedSize: Size(
                                        MediaQuery.of(context).size.width, 45),
                                    backgroundColor: PartnerAppColors.darkBlue,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(5))),
                                onPressed: () {
                                  Navigator.of(dialogContext).pop();
                                  modalManager.showLoadingModal();

                                  offerVm
                                      .updatePrice(
                                          industryIndex: industryIndex,
                                          subIndustryIndex: subIndustryIndex,
                                          price: double.parse(
                                              _subIndustryPriceFormatter
                                                  .getUnformattedValue()
                                                  .toString()))
                                      .then((value) {
                                    modalManager.hideLoadingModal();
                                  });
                                },
                                child: Text(tr('update'),
                                    style: Theme.of(context)
                                        .textTheme
                                        .headlineMedium!
                                        .copyWith(
                                            color: Colors.white,
                                            fontWeight: FontWeight.normal)))
                          ])))));
        });
  }
}
