import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

Future<void> chooseOfferTypeWizardDialog({required BuildContext context}) {
  final offerVm = context.read<OfferViewModel>();
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
            insetPadding: const EdgeInsets.all(20),
            contentPadding: const EdgeInsets.all(20),
            title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
              GestureDetector(
                  onTap: () {
                    offerVm.offerType = '';
                    Navigator.of(dialogContext).pop();
                  },
                  child: const Icon(Icons.close))
            ]),
            content: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: const SingleChildScrollView(
                    child: ChooseOfferTypeWizardDialog())));
      });
}

class ChooseOfferTypeWizardDialog extends StatelessWidget {
  const ChooseOfferTypeWizardDialog({super.key});

  @override
  Widget build(BuildContext context) {
    final offerVm = context.read<OfferViewModel>();
    return Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
      Text(
        tr('how_will_you_offer'),
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.displayLarge!.copyWith(
            fontSize: 18,
            fontWeight: FontWeight.w400,
            color: PartnerAppColors.darkBlue),
      ),
      SmartGaps.gapH20,
      CustomDesignTheme.flatButtonStyle(
          height: 50,
          backgroundColor: PartnerAppColors.darkBlue,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          onPressed: () {
            offerVm.offerType = 'uploadOffer';
            Navigator.of(context).pop();
          },
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text(tr('upload_offer'),
                style: Theme.of(context).textTheme.displayLarge!.copyWith(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Colors.white))
          ])),
      SmartGaps.gapH10,
      CustomDesignTheme.flatButtonStyle(
          height: 50,
          backgroundColor: PartnerAppColors.darkBlue,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          onPressed: () {
            offerVm.offerType = 'createOffer';
            Navigator.of(context).pop();
          },
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text(tr('create_offer'),
                style: Theme.of(context).textTheme.displayLarge!.copyWith(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Colors.white))
          ]))
    ]);
  }
}
