import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/screens/search_address/search_address.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class CreateOfferWizardStepOne extends StatefulWidget {
  const CreateOfferWizardStepOne(
      {super.key, required this.formKey, required this.offerVm});
  final GlobalKey<FormBuilderState> formKey;
  final OfferViewModel offerVm;

  @override
  CreateOfferWizardStepOneState createState() =>
      CreateOfferWizardStepOneState();
}

class CreateOfferWizardStepOneState extends State<CreateOfferWizardStepOne> {
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      if (!widget.offerVm.isCreateOfferLoading) ...[
        GestureDetector(
            onTap: () async {
              final address = await addAddress(context: context);

              if (address != null) {
                widget.formKey.currentState!
                    .patchValue({'address': address.address});

                widget.offerVm.updateCreateOfferWizardForms(
                    key: 'address', value: address);
              }
            },
            child: AbsorbPointer(
                absorbing: true,
                child: CustomTextFieldFormBuilder(
                    name: 'address',
                    labelText: tr('complete_address'),
                    validator: FormBuilderValidators.required(
                        errorText: tr('required'))))),
        CustomTextFieldFormBuilder(
            name: 'homeOwnerEmail',
            labelText: tr('email'),
            keyboardType: TextInputType.emailAddress,
            validator:
                FormBuilderValidators.email(errorText: tr('enter_valid_email')),
            initialValue: widget.offerVm.createOfferWizardForms['contact']
                ?['EMAIL'])
      ],
      if (widget.offerVm.isCreateOfferLoading) ...[
        SmartGaps.gapH80,
        Center(
            child: CircularProgressIndicator(
                color: PartnerAppColors.blue.withValues(alpha: .3)))
      ]
    ]);
  }
}
