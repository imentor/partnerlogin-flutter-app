import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/string_utils.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class CreateOfferWizardStepThreeDescriptionEdit extends StatefulWidget {
  const CreateOfferWizardStepThreeDescriptionEdit(
      {super.key,
      required this.industryIndex,
      required this.subIndustryIndex,
      required this.industryMapLength,
      required this.subIndustryMapLength,
      required this.descriptionList,
      required this.subIndustryMap});
  final int industryIndex;
  final int subIndustryIndex;
  final int industryMapLength;
  final int subIndustryMapLength;
  final List<dynamic> descriptionList;
  final Map<String, dynamic> subIndustryMap;

  @override
  State<CreateOfferWizardStepThreeDescriptionEdit> createState() =>
      _CreateOfferWizardStepThreeDescriptionEditState();
}

class _CreateOfferWizardStepThreeDescriptionEditState
    extends State<CreateOfferWizardStepThreeDescriptionEdit> {
  final formKey = GlobalKey<FormBuilderState>();
  final ScrollController _scrollController = ScrollController();
  Map<int, bool> deletedDescriptions = {};
  Map<int, String> editedDescriptions = {};
  Map<int, String> templateDescriptions = {};
  Map<int, String> newDescriptions = {};
  List<dynamic> editableDescriptions = [];

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      setState(() {
        editableDescriptions = widget.descriptionList;
        deletedDescriptions.clear();
        editedDescriptions.clear();
        templateDescriptions.clear();
        newDescriptions.clear();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final subIndustryName = widget.subIndustryMap['subIndustryName'] as String;
    return Scaffold(
        appBar: const DrawerAppBar(),
        bottomNavigationBar:
            Consumer<OfferViewModel>(builder: (_, offerVm, __) {
          return Container(
              height: 124,
              padding: const EdgeInsets.only(
                  left: 20, right: 20, bottom: 10, top: 10),
              color: Colors.white,
              child: Column(children: [
                CustomDesignTheme.flatButtonStyle(
                    backgroundColor: PartnerAppColors.malachite,
                    padding:
                        const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
                    onPressed: () async {
                      if (editedDescriptions.isNotEmpty) {
                        editedDescriptions.forEach((key, value) {
                          if (value.isNotEmpty) {
                            offerVm.updateIndustryDescriptions(
                                isDeleteIndustry: false,
                                isDelete: false,
                                isAdd: false,
                                industryIndex: widget.industryIndex,
                                subIndustryIndex: widget.subIndustryIndex,
                                descriptionIndex: key,
                                description: value);
                          }
                        });
                      }
                      if (deletedDescriptions.isNotEmpty) {
                        if (deletedDescriptions.length > 1) {
                          final sortedKeys = deletedDescriptions.keys.toList()
                            ..sort((a, b) => b.compareTo(a));
                          final sortedDeletedDescriptions = {
                            for (var key in sortedKeys)
                              key: deletedDescriptions[key]!
                          };
                          sortedDeletedDescriptions.forEach((key, value) {
                            if (value) {
                              offerVm.updateIndustryDescriptions(
                                  isDeleteIndustry: false,
                                  isDelete: true,
                                  isAdd: false,
                                  industryIndex: widget.industryIndex,
                                  subIndustryIndex: widget.subIndustryIndex,
                                  descriptionIndex: key,
                                  description: '');
                            }
                          });
                        } else {
                          deletedDescriptions.forEach((key, value) {
                            if (value) {
                              offerVm.updateIndustryDescriptions(
                                  isDeleteIndustry: false,
                                  isDelete: true,
                                  isAdd: false,
                                  industryIndex: widget.industryIndex,
                                  subIndustryIndex: widget.subIndustryIndex,
                                  descriptionIndex: key,
                                  description: '');
                            }
                          });
                        }
                      }
                      backDrawerRoute();
                    },
                    child: Center(
                        child: Text(tr('change_text'),
                            style: const TextStyle(
                                color: Colors.white, fontSize: 16)))),
                CustomDesignTheme.flatButtonStyle(
                    padding:
                        const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
                    onPressed: () async {
                      if (newDescriptions.isNotEmpty) {
                        if (newDescriptions.keys.first !=
                            newDescriptions.keys.last) {
                          setState(() {
                            editableDescriptions.removeRange(
                                newDescriptions.keys.first - 1,
                                newDescriptions.keys.last);
                          });
                        } else {
                          setState(() {
                            editableDescriptions
                                .removeAt(newDescriptions.keys.first - 1);
                          });
                        }
                      }
                      backDrawerRoute();
                    },
                    child: Center(
                        child: Text(tr('close_without_saving'),
                            style: const TextStyle(
                                color: PartnerAppColors.accentBlue,
                                fontSize: 16))))
              ]));
        }),
        body: Consumer<OfferViewModel>(builder: (_, offerVm, __) {
          return SingleChildScrollView(
              controller: _scrollController,
              padding: const EdgeInsets.all(20),
              child: FormBuilder(
                  key: formKey,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(tr('adjust_description'),
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium!
                                .copyWith(
                                    letterSpacing: 1.0,
                                    color: PartnerAppColors.darkBlue,
                                    fontWeight: FontWeight.bold)),
                        SmartGaps.gapH10,
                        Text(tr('adjust_description_text'),
                            style: Theme.of(context)
                                .textTheme
                                .headlineSmall!
                                .copyWith(
                                    letterSpacing: 1.0,
                                    color: PartnerAppColors.spanishGrey,
                                    fontWeight: FontWeight.normal)),
                        SmartGaps.gapH10,
                        const Divider(thickness: 2),
                        SmartGaps.gapH10,
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                  '${widget.industryIndex + 1}.${widget.subIndustryIndex + 1} ${subIndustryName.toCapitalizedFirst()}',
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleSmall!
                                      .copyWith(
                                          fontSize: 18,
                                          color: PartnerAppColors.darkBlue,
                                          fontWeight: FontWeight.normal)),
                              InkWell(
                                  onTap: () async {
                                    await showCustomDialog(
                                        context: context,
                                        contentText: tr('restore_description'),
                                        continueFunction: () {
                                          templateDescriptions
                                              .forEach((key, value) {
                                            setState(() {
                                              formKey.currentState?.patchValue({
                                                'description_$key': value
                                                    .replaceFirst('-', '')
                                                    .replaceFirst(' ', '')
                                              });
                                              editableDescriptions[key] = value;
                                            });
                                          });
                                        });
                                  },
                                  child: Row(children: [
                                    const Icon(Icons.refresh_sharp,
                                        size: 20, color: PartnerAppColors.blue),
                                    Text(tr('restore_text'),
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleSmall!
                                            .copyWith(
                                                fontSize: 16,
                                                color: PartnerAppColors.blue,
                                                fontWeight: FontWeight.normal))
                                  ]))
                            ]),
                        SmartGaps.gapH10,
                        ListView.separated(
                            itemCount: widget.descriptionList.length + 1,
                            shrinkWrap: true,
                            padding: EdgeInsets.zero,
                            physics: const NeverScrollableScrollPhysics(),
                            separatorBuilder: (context, descriptionIndex) {
                              return Container();
                            },
                            itemBuilder: (context, descriptionIndex) {
                              return industryDescriptionEditList(
                                  industryIndex: widget.industryIndex,
                                  subIndustryIndex: widget.subIndustryIndex,
                                  descriptionIndex: descriptionIndex,
                                  offerVm: offerVm);
                            }),
                        SmartGaps.gapH5,
                        InkWell(
                            onTap: () async {
                              setState(() {
                                editableDescriptions.add('');
                                newDescriptions.putIfAbsent(
                                    editableDescriptions.length, () => '');
                              });
                            },
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  const Icon(FeatherIcons.plus,
                                      size: 20, color: PartnerAppColors.blue),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 3.0),
                                    child: Text(tr('add_description'),
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleSmall!
                                            .copyWith(
                                                fontSize: 16,
                                                color: PartnerAppColors.blue,
                                                fontWeight: FontWeight.normal)),
                                  )
                                ])),
                        SmartGaps.gapH10
                      ])));
        }));
  }

  Widget industryDescriptionEditList(
      {required int industryIndex,
      required int subIndustryIndex,
      required int descriptionIndex,
      required OfferViewModel offerVm}) {
    if (descriptionIndex < editableDescriptions.length) {
      String description = editableDescriptions[descriptionIndex];
      deletedDescriptions.putIfAbsent(descriptionIndex, () => false);
      if (templateDescriptions.containsKey(descriptionIndex)) {
        templateDescriptions.update(descriptionIndex, (value) => description);
      } else {
        templateDescriptions.putIfAbsent(descriptionIndex, () => description);
      }

      return Visibility(
        visible: !deletedDescriptions[descriptionIndex]!,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 1),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Expanded(
                  child: CustomTextFieldFormBuilder(
                      key: Key('description_$descriptionIndex'),
                      validator: FormBuilderValidators.required(
                          errorText: tr('required')),
                      name: 'description_$descriptionIndex',
                      initialValue: description
                          .replaceFirst('-', '')
                          .replaceFirst(' ', ''),
                      onChanged: (newValue) async {
                        if (newValue != null && newValue.isNotEmpty) {
                          setState(() {
                            if (editedDescriptions
                                .containsKey(descriptionIndex)) {
                              editedDescriptions.update(
                                  descriptionIndex, (value) => '- $newValue');
                            } else {
                              editedDescriptions.putIfAbsent(
                                  descriptionIndex, () => '- $newValue');
                            }
                            if (newDescriptions.isNotEmpty) {
                              if (newDescriptions
                                  .containsKey(descriptionIndex + 1)) {
                                newDescriptions.update(descriptionIndex + 1,
                                    (value) => '- $newValue');
                              }
                            }
                          });
                        }
                      },
                      prefixTextDecoration: Text(
                          '${industryIndex + 1}.${subIndustryIndex + 1}.${descriptionIndex + 1} - ',
                          style: GoogleFonts.notoSans(
                              textStyle: TextStyle(
                                  fontSize: 15,
                                  height: 1.81,
                                  color: Colors.black.withValues(alpha: 0.6),
                                  fontWeight: FontWeight.w400))))),
              SmartGaps.gapW5,
              InkWell(
                  child: const Icon(FeatherIcons.trash2,
                      color: PartnerAppColors.spanishGrey, size: 28),
                  onTap: () async {
                    setState(() {
                      if (deletedDescriptions.containsKey(descriptionIndex)) {
                        deletedDescriptions.update(
                            descriptionIndex, (value) => true);
                      } else {
                        deletedDescriptions.putIfAbsent(
                            descriptionIndex, () => true);
                      }
                    });
                  })
            ])
          ]),
        ),
      );
    } else {
      return const Column();
    }
  }
}
