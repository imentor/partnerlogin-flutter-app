import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/components/signature_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer/choose_offer_type_wizard_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer/create_offer_wizard_step_1.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer/create_offer_wizard_step_2.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer/create_offer_wizard_step_2_upload.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer/create_offer_wizard_step_3_upload.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer/create_offer_wizard_step_3_v2.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer/create_offer_wizard_step_4.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer/create_offer_wizard_step_4_upload.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer/create_offer_wizard_step_5.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer/create_offer_wizard_step_5_upload.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer/create_offer_wizard_step_6.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer/create_offer_wizard_step_6_upload.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer/create_offer_wizard_step_7.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer/create_offer_wizard_step_7_upload.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:provider/provider.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:timeline_tile/timeline_tile.dart';

class CreateOfferWizardPage extends StatefulWidget {
  const CreateOfferWizardPage({super.key});

  @override
  CreateOfferWizardPageState createState() => CreateOfferWizardPageState();
}

class CreateOfferWizardPageState extends State<CreateOfferWizardPage> {
  final formKey = GlobalKey<FormBuilderState>();
  final ItemScrollController itemScrollController = ItemScrollController();
  late ScrollController scrollController;

  int currentStep = 0;

  Future<void> onPressedBackButton({required OfferViewModel offerVm}) async {
    switch (offerVm.createOfferStep + 1) {
      case 2:
        switch (offerVm.stepTwoSubStep + 1) {
          case 1:
            offerVm.updateCreateOfferSteps(
                createOfferStep: offerVm.createOfferStep - 1);
            break;
          case 2:
            if (offerVm.offerType == 'createOffer') {
              if (offerVm.createOfferProductsStep == 0) {
                formKey.currentState!.reset();
                formKey.currentState!.fields.forEach((key, value) {
                  formKey.currentState!.fields[key]?.didChange(null);
                });
                offerVm.updateCreateOfferSteps(
                    stepTwoSubStep: offerVm.stepTwoSubStep - 1);
              } else {
                offerVm.updateCreateOfferSteps(
                    createOfferProductsStep:
                        offerVm.createOfferProductsStep - 1);
              }
            } else {
              offerVm.updateCreateOfferSteps(
                  createOfferStep: offerVm.createOfferStep - 1);
            }
            break;
          case 3:
            if (offerVm.offerType == 'createOffer') {
              formKey.currentState!.reset();
              formKey.currentState!.fields.forEach((key, value) {
                formKey.currentState!.fields[key]?.didChange(null);
              });
              offerVm.updateCreateOfferSteps(
                  stepTwoSubStep: offerVm.stepTwoSubStep - 1);
            } else {
              offerVm.updateCreateOfferSteps(
                  createOfferStep: offerVm.createOfferStep - 1);
            }
            break;
        }
        break;
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
        offerVm.updateCreateOfferSteps(
            createOfferStep: offerVm.createOfferStep - 1);
        break;
      case 8:
        if (offerVm.offerType == 'uploadOffer') {
          offerVm.updateCreateOfferSteps(
              createOfferStep: offerVm.createOfferStep - 1);
        }
        break;
      default:
    }
  }

  Future<void> onPressedNextButton(
      {required OfferViewModel offerVm,
      required AppDrawerViewModel appDrawerVm,
      required ClientsViewModel clientVm,
      required TenderFolderViewModel tenderFolderVm,
      required CreateOfferViewModel createOfferVm,
      required UserViewModel userVm}) async {
    final isAffiliate = (appDrawerVm.bitrixCompanyModel?.companyPackge ?? '') ==
        'affiliatepartner';

    switch (offerVm.createOfferStep + 1) {
      case 1:
        if (offerVm.isCreateOfferLoading) return;
        if (formKey.currentState!.validate()) {
          offerVm.updateCreateOfferWizardForms(key: 'contact', value: {
            "EMAIL": formKey.currentState!.fields['homeOwnerEmail']!.value
          });

          await chooseOfferTypeWizardDialog(context: context).then((val) async {
            if (!mounted) return;
            if (offerVm.offerType.isNotEmpty) {
              modalManager.showLoadingModal();
              await tryCatchWrapper(
                      context: myGlobals.homeScaffoldKey!.currentContext,
                      function: offerVm.createContactAndAddress(
                          isAffiliate: isAffiliate))
                  .whenComplete(() {
                formKey.currentState!.fields['address']?.reset();
                modalManager.hideLoadingModal();
              });
            } else {
              return;
            }
          });
        }
        break;
      case 2:
        if (offerVm.offerType == 'createOffer') {
          switch (offerVm.stepTwoSubStep + 1) {
            case 1:
              if (offerVm.selectedIndustries.isNotEmpty) {
                modalManager.showLoadingModal();

                await tryCatchWrapper(
                  context: myGlobals.homeScaffoldKey!.currentContext,
                  function: offerVm.getWizardProductQuestions(),
                ).whenComplete(() {
                  offerVm.updateSelectedIndustryLength(
                      length: offerVm.selectedIndustries.length);
                  modalManager.hideLoadingModal();
                });
              }
              break;
            case 2:
              if (offerVm.createOfferProductsStep ==
                  offerVm.wizardQuestions.length - 1) {
                modalManager.showLoadingModal();
                await tryCatchWrapper(
                        context: myGlobals.homeScaffoldKey!.currentContext,
                        function: offerVm.stepTwoSubStepUpdate())
                    .whenComplete(() {
                  modalManager.hideLoadingModal();
                  scrollController.animateTo(0.0,
                      duration: const Duration(milliseconds: 800),
                      curve: Curves.easeOutQuart);
                });
              } else {
                formKey.currentState!.fields.forEach((key, value) {
                  if (key.contains('checkbox')) {
                    if (value.value is List) {
                      for (final val in value.value as List) {
                        offerVm.updateWizardProductAnswers(
                            key: '$key$val', value: '$val');
                      }
                    }
                  } else {
                    offerVm.updateWizardProductAnswers(
                        key: key, value: '${value.value}');
                  }
                });

                offerVm.updateCreateOfferSteps(
                    createOfferProductsStep:
                        offerVm.createOfferProductsStep + 1);
              }
              break;
            case 3:
              if (offerVm.selectedIndustryLength !=
                  offerVm.selectedIndustries.length) {
                modalManager.showLoadingModal();
                await tryCatchWrapper(
                        context: context,
                        function: offerVm.getWizardProductQuestions())
                    .whenComplete(() {
                  offerVm.updateSelectedIndustryLength(
                      length: offerVm.selectedIndustries.length);
                  modalManager.hideLoadingModal();
                });
              } else {
                modalManager.showLoadingModal();
                await tryCatchWrapper(
                        context: context,
                        function: Future.value(offerVm.submitWizardAnswers()))
                    .whenComplete(() {
                  modalManager.hideLoadingModal();
                  offerVm.updateCreateOfferSteps(
                      createOfferStep: offerVm.createOfferStep + 1);
                });
              }
              break;
            default:
              break;
          }
        } else {
          if (offerVm.selectedOfferFiles.isNotEmpty ||
              offerVm.uploadOfferFiles.isNotEmpty) {
            modalManager.showLoadingModal();
            await tryCatchWrapper(
                    context: context, function: offerVm.mergeFilesFastOffer())
                .whenComplete(() async {
              if (!mounted) return;
              modalManager.hideLoadingModal();
              offerVm.updateCreateOfferSteps(
                  createOfferStep: offerVm.createOfferStep + 1);
            });
          }
        }
        break;
      case 3:
        if (offerVm.offerType == 'createOffer') {
          for (var industry in offerVm.industryDescriptions) {
            for (var subIndutry in industry['subIndustry'] as List) {
              if (subIndutry['price'] == 0) {
                setState(() {
                  offerVm.isValidateError[subIndutry['subIndustryName']] = true;
                });
              }
            }
          }

          modalManager.showLoadingModal();
          if (await offerVm.checkIndustryPrices()) {
            if (formKey.currentState!.validate()) {
              formKey.currentState!.fields.forEach((key, value) {
                offerVm.updateCreateOfferWizardForms(
                    key: key, value: '${value.value}');
              });

              double totalPriceWithoutVat = 0;

              for (var industry in offerVm.industryDescriptions) {
                for (var subIndutry in industry['subIndustry'] as List) {
                  if (subIndutry['price'] != 0) {
                    setState(() {
                      offerVm.isValidateError[subIndutry['subIndustryName']] =
                          false;
                    });
                  }
                  totalPriceWithoutVat += subIndutry['price'];
                }
              }
              if (!mounted) return;
              if ((totalPriceWithoutVat * 1.25) < 1000) {
                modalManager.hideLoadingModal();
                await totalPrisErrorPopupDialog(
                    context: context,
                    message: tr('total_should_not_be_less_than_1000'));
              } else {
                modalManager.hideLoadingModal();
                offerVm.updateCreateOfferSteps(
                    createOfferStep: offerVm.createOfferStep + 1);
              }
            } else {
              modalManager.hideLoadingModal();
            }
          } else {
            modalManager.hideLoadingModal();
          }
        } else {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.fields.forEach((key, value) {
              offerVm.updateCreateOfferWizardForms(
                  key: key,
                  value:
                      '${value.value.replaceAll('.', '').replaceAll(',', '.')}');
            });
            offerVm.updateCreateOfferWizardForms(
                key: 'priceHasVat', value: false);
            offerVm.updateCreateOfferSteps(
                createOfferStep: offerVm.createOfferStep + 1);
          }
        }
        break;
      case 4:
        if (offerVm.offerType == 'uploadOffer') {
          if (offerVm.selectedIndustries.isNotEmpty) {
            modalManager.showLoadingModal();

            await tryCatchWrapper(
              context: myGlobals.homeScaffoldKey!.currentContext,
              function: offerVm.getListPrices(),
            ).whenComplete(() {
              offerVm.updateSelectedIndustryLength(
                  length: offerVm.selectedIndustries.length);
              modalManager.hideLoadingModal();
            });
          }
        } else {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.fields.forEach((key, value) {
              offerVm.updateCreateOfferWizardForms(
                  key: key, value: '${value.value}');
            });

            offerVm.updateCreateOfferSteps(
                createOfferStep: offerVm.createOfferStep + 1);
          }
        }
        break;
      case 5:
        if (offerVm.offerType == 'uploadOffer') {
          double totalPriceWithVat = 0;

          for (var industry in offerVm.industryDescriptions) {
            for (var subIndutry in industry['subIndustry'] as List) {
              totalPriceWithVat += subIndutry['priceVat'];
            }
          }
          if (totalPriceWithVat != offerVm.tempTotalPrice) {
            totalPrisErrorPopupDialog(
                context: context, message: tr('total_pris_error_txt'));
          } else {
            offerVm.updateCreateOfferSteps(
                createOfferStep: offerVm.createOfferStep + 1);
          }
        } else {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.fields.forEach((key, value) {
              offerVm.updateCreateOfferWizardForms(
                  key: key, value: '${value.value}');
            });

            offerVm.updateCreateOfferSteps(
                createOfferStep: offerVm.createOfferStep + 1);
          }
        }
        break;
      case 6:
        if (formKey.currentState!.validate()) {
          formKey.currentState!.fields.forEach((key, value) {
            offerVm.updateCreateOfferWizardForms(
                key: key, value: '${value.value}');
          });

          offerVm.updateCreateOfferSteps(
              createOfferStep: offerVm.createOfferStep + 1);
        }
        break;
      case 7:
        if (offerVm.offerType == 'createOffer') {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.fields.forEach((key, value) {
              offerVm.updateCreateOfferWizardForms(
                  key: key, value: '${value.value}');
            });

            final signature = await signatureDialog(
                context: context,
                label: context.read<SharedDataViewmodel>().termsLabel,
                htmlTerms: context.read<SharedDataViewmodel>().termsHtml,
                buttonLabel: 'done_send_offer');

            if (signature != null) {
              modalManager.showLoadingModal();

              await tryCatchWrapper(
                      context: myGlobals.homeScaffoldKey!.currentContext,
                      function: offerVm.createOffer(
                          signature: signature,
                          userVm: userVm,
                          isAffiliate: (appDrawerVm.supplierProfileModel
                                      ?.bCrmCompany?.companyPackge ??
                                  '') ==
                              'affiliatepartner'))
                  .then((value) async {
                if (value != null && value) {
                  await tryCatchWrapper(
                          context: myGlobals.homeScaffoldKey!.currentContext,
                          function: clientVm.getClients(
                              tenderVm: tenderFolderVm,
                              createOfferVm: createOfferVm))
                      .whenComplete(() async {
                    modalManager.hideLoadingModal();
                    await successDialog(
                            context: myGlobals.homeScaffoldKey!.currentContext!,
                            successMessage: 'project_has_been_created')
                        .whenComplete(() {
                      modalManager.hideLoadingModal();
                      backDrawerRoute();
                      changeDrawerRoute(Routes.yourClient);
                    });
                  });
                }
              }).whenComplete(() {
                modalManager.hideLoadingModal();
                offerVm.resetCreateOfferWizards();
              });
            }
          }
        } else {
          offerVm.updateCreateOfferSteps(
              createOfferStep: offerVm.createOfferStep + 1);
        }
        break;
      case 8:
        if (offerVm.offerType == 'uploadOffer') {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.fields.forEach((key, value) {
              offerVm.updateCreateOfferWizardForms(
                  key: key, value: '${value.value}');
            });

            final signature = await signatureDialog(
                context: context,
                label: context.read<SharedDataViewmodel>().termsLabel,
                htmlTerms: context.read<SharedDataViewmodel>().termsHtml,
                buttonLabel: 'done_send_offer');

            if (signature != null) {
              modalManager.showLoadingModal();

              await tryCatchWrapper(
                      context: myGlobals.homeScaffoldKey!.currentContext,
                      function: offerVm.createUploadFastOffer(
                          signature: signature,
                          userVm: userVm,
                          isAffiliate: (appDrawerVm.supplierProfileModel
                                      ?.bCrmCompany?.companyPackge ??
                                  '') ==
                              'affiliatepartner'))
                  .then((value) async {
                if (value != null && value) {
                  await tryCatchWrapper(
                          context: myGlobals.homeScaffoldKey!.currentContext,
                          function: clientVm.getClients(
                              tenderVm: tenderFolderVm,
                              createOfferVm: createOfferVm))
                      .whenComplete(() async {
                    modalManager.hideLoadingModal();
                    await successDialog(
                            context: myGlobals.homeScaffoldKey!.currentContext!,
                            successMessage: 'project_has_been_created')
                        .whenComplete(() {
                      modalManager.hideLoadingModal();
                      backDrawerRoute();
                      changeDrawerRoute(Routes.yourClient);
                    });
                  });
                }
              }).whenComplete(() {
                modalManager.hideLoadingModal();
                offerVm.resetCreateOfferWizards();
              });
            }
          }
        }
        break;
      default:
    }
  }

  @override
  void initState() {
    scrollController = ScrollController();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final offerVm = context.read<OfferViewModel>();
      final sharedDataVm = context.read<SharedDataViewmodel>();
      final newsFeedVm = context.read<NewsFeedViewModel>();
      final appDrawerVm = context.read<AppDrawerViewModel>();
      const dynamicStoryPage = 'standardtilbudproduct_createproject';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;
      modalManager
          .setContext(myGlobals.homeScaffoldKey?.currentContext ?? context);
      offerVm.isCreateOfferLoading = true;
      offerVm.seeMore = false;

      if (dynamicStoryEnable) {
        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey?.currentContext ?? context,
            function: Future.wait([
              offerVm.initCreateOffer(),
              newsFeedVm.getDynamicStory(page: dynamicStoryPage),
            ])).whenComplete(() {
          if (!mounted) return;

          offerVm.filteredIndustries = sharedDataVm.industryList;
          offerVm.industryList = sharedDataVm.industryList;
          offerVm.isCreateOfferLoading = false;
          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
          // if (offerVm.existingCreateOfferWizardList.isNotEmpty) {
          //   createOfferExistedProjectsDialog(context: context);
          // }
        });
      } else {
        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey?.currentContext ?? context,
            function: Future.wait([
              offerVm.initCreateOffer(),
            ])).whenComplete(() {
          if (!mounted) return;

          offerVm.filteredIndustries = sharedDataVm.industryList;
          offerVm.industryList = sharedDataVm.industryList;
          offerVm.isCreateOfferLoading = false;
          // if (offerVm.existingCreateOfferWizardList.isNotEmpty) {
          //   createOfferExistedProjectsDialog(context: context);
          // }
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: const DrawerAppBar(),
        bottomNavigationBar: Consumer<OfferViewModel>(builder: (_, vm, __) {
          return Container(
              padding: const EdgeInsets.only(
                  left: 20, right: 20, bottom: 20, top: 10),
              color: Colors.white,
              child: Row(children: [
                if (vm.createOfferStep != 0) ...[
                  Expanded(
                      child: TextButton(
                          style: TextButton.styleFrom(
                              fixedSize:
                                  Size(MediaQuery.of(context).size.width, 45),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  side: const BorderSide(
                                      color: PartnerAppColors.darkBlue))),
                          onPressed: () async {
                            onPressedBackButton(offerVm: vm);
                          },
                          child: Text(
                            tr('back'),
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium!
                                .copyWith(color: PartnerAppColors.darkBlue),
                          ))),
                  SmartGaps.gapW20
                ],
                Expanded(
                    child: TextButton(
                        style: TextButton.styleFrom(
                            fixedSize:
                                Size(MediaQuery.of(context).size.width, 45),
                            backgroundColor: vm.createOfferStep + 1 == 7
                                ? PartnerAppColors.malachite
                                : PartnerAppColors.darkBlue,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5))),
                        onPressed: () async {
                          final appDrawerVm =
                              context.read<AppDrawerViewModel>();
                          final clientVm = context.read<ClientsViewModel>();
                          final userVm = context.read<UserViewModel>();
                          final tenderFolderVm =
                              context.read<TenderFolderViewModel>();
                          final createOfferVm =
                              context.read<CreateOfferViewModel>();

                          onPressedNextButton(
                              offerVm: vm,
                              appDrawerVm: appDrawerVm,
                              clientVm: clientVm,
                              tenderFolderVm: tenderFolderVm,
                              createOfferVm: createOfferVm,
                              userVm: userVm);
                        },
                        child: Text(
                            vm.offerType == 'createOffer'
                                ? vm.createOfferStep + 1 == 7
                                    ? tr('done')
                                    : tr('next')
                                : vm.createOfferStep + 1 == 8
                                    ? tr('done')
                                    : tr('next'),
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium!
                                .copyWith(color: Colors.white))))
              ]));
        }),
        body: Consumer<OfferViewModel>(builder: (_, vm, __) {
          if (itemScrollController.isAttached) {
            itemScrollController.scrollTo(
                index: vm.createOfferStep,
                duration: const Duration(seconds: 2),
                curve: Curves.easeInOutCubic);
          }
          return SingleChildScrollView(
              physics: vm.seeMore
                  ? const NeverScrollableScrollPhysics()
                  : const AlwaysScrollableScrollPhysics(),
              controller: scrollController,
              padding: const EdgeInsets.all(20),
              child: FormBuilder(
                  key: formKey,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                            height: 65,
                            width: MediaQuery.of(context).size.width,
                            child: ScrollablePositionedList.builder(
                                initialAlignment: 0,
                                scrollDirection: Axis.horizontal,
                                itemScrollController: itemScrollController,
                                shrinkWrap: true,
                                itemCount:
                                    vm.offerType == 'uploadOffer' ? 8 : 7,
                                itemBuilder: (context, index) {
                                  final isCurrentPage =
                                      index <= vm.createOfferStep;
                                  return TimelineTile(
                                    axis: TimelineAxis.horizontal,
                                    alignment: TimelineAlign.center,
                                    isFirst: index == 0,
                                    isLast: vm.offerType == 'uploadOffer'
                                        ? index == 8 - 1
                                        : index == 7 - 1,
                                    indicatorStyle: IndicatorStyle(
                                        width: 30.0,
                                        height: 30.0,
                                        indicator: DecoratedBox(
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                border: Border.all(
                                                    color:
                                                        PartnerAppColors.blue),
                                                color: isCurrentPage
                                                    ? PartnerAppColors.blue
                                                    : Colors.white),
                                            child: Center(
                                                child: Text('${index + 1}',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodySmall!
                                                        .copyWith(
                                                            color: isCurrentPage
                                                                ? Colors.white
                                                                : PartnerAppColors
                                                                    .blue,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold))))),
                                    beforeLineStyle: const LineStyle(
                                        thickness: 1.0,
                                        color: PartnerAppColors.spanishGrey),
                                    endChild: Container(
                                      margin: const EdgeInsets.only(top: 5),
                                      constraints: const BoxConstraints(
                                          maxHeight: 30, minWidth: 50),
                                      child: Text('${tr('step')} ${index + 1}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodySmall,
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.center),
                                    ),
                                  );
                                })),
                        SmartGaps.gapH20,
                        step(step: vm.createOfferStep, formKey: formKey, vm: vm)
                      ])));
        }));
  }

  Widget step(
      {required int step,
      required GlobalKey<FormBuilderState> formKey,
      required OfferViewModel vm}) {
    switch (step + 1) {
      case 1:
        return CreateOfferWizardStepOne(formKey: formKey, offerVm: vm);
      case 2:
        if (vm.offerType == 'createOffer') {
          return const CreateOfferWizardStepTwo();
        } else {
          return CreateOfferWizardStepTwoUpload(offerVm: vm);
        }
      case 3:
        if (vm.offerType == 'createOffer') {
          return CreateOfferWizardStepThreeV2(offerVm: vm);
        } else {
          return CreateOfferWizardStepThreeUpload(offerVm: vm);
        }
      case 4:
        if (vm.offerType == 'createOffer') {
          return CreateOfferWizardStepFour(offerVm: vm);
        } else {
          return CreateOfferWizardStepFourUpload(offerVm: vm);
        }
      case 5:
        if (vm.offerType == 'createOffer') {
          return const CreateOfferWizardStepFive();
        } else {
          return CreateOfferWizardStepFiveUpload(offerVm: vm);
        }
      case 6:
        if (vm.offerType == 'createOffer') {
          return const CreateOfferWizardStepSix();
        } else {
          return CreateOfferWizardStepSixUpload(offerVm: vm);
        }
      case 7:
        if (vm.offerType == 'createOffer') {
          return CreateOfferWizardStepSeven(formKey: formKey, offerVm: vm);
        } else {
          return CreateOfferWizardStepSevenUpload(
              scrollController: scrollController, offerVm: vm);
        }
      case 8:
        return CreateOfferWizardStepSeven(formKey: formKey, offerVm: vm);
      default:
        return CreateOfferWizardStepOne(formKey: formKey, offerVm: vm);
    }
  }
}

Future<void> totalPrisErrorPopupDialog(
    {required BuildContext context, required String message}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
            insetPadding: const EdgeInsets.all(20),
            contentPadding: const EdgeInsets.all(20),
            title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
              GestureDetector(
                  onTap: () {
                    Navigator.of(dialogContext).pop();
                  },
                  child: const Icon(Icons.close))
            ]),
            content: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                    child: TotalPrisErrorPopup(message: message))));
      });
}

class TotalPrisErrorPopup extends StatelessWidget {
  const TotalPrisErrorPopup({super.key, required this.message});
  final String message;

  @override
  Widget build(BuildContext context) {
    final centeredMessageContent =
        '<div style="text-align= center;"> $message </div>';
    return Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 4),
          child: HtmlWidget(centeredMessageContent,
              textStyle: context.pttBodyLarge.copyWith(color: Colors.black))),
      SmartGaps.gapH20,
      CustomDesignTheme.flatButtonStyle(
          width: MediaQuery.of(context).size.width,
          height: 50,
          backgroundColor: PartnerAppColors.malachite,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('OK',
              style: Theme.of(context).textTheme.displayLarge!.copyWith(
                  fontSize: 18,
                  fontWeight: FontWeight.w400,
                  color: Colors.white)))
    ]);
  }
}
