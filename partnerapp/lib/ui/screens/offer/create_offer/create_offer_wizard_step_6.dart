import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/string_utils.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';

class CreateOfferWizardStepSix extends StatefulWidget {
  const CreateOfferWizardStepSix({super.key});

  @override
  CreateOfferWizardStepSixState createState() =>
      CreateOfferWizardStepSixState();
}

class CreateOfferWizardStepSixState extends State<CreateOfferWizardStepSix> {
  @override
  Widget build(BuildContext context) {
    return Consumer<OfferViewModel>(builder: (_, vm, __) {
      double totalPriceWithoutVat = 0;
      double vat = 0;

      for (var industry in vm.industryDescriptions) {
        for (var subIndutry in industry['subIndustry'] as List) {
          totalPriceWithoutVat += subIndutry['price'];
        }
      }

      vat = totalPriceWithoutVat * .25;

      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            tr('summary'),
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                letterSpacing: 1.0,
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.bold,
                fontSize: 18),
          ),
          SmartGaps.gapH20,
          Text(
            tr('project_name'),
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                letterSpacing: 1.0,
                color: PartnerAppColors.spanishGrey,
                fontWeight: FontWeight.w500,
                fontSize: 16),
          ),
          SmartGaps.gapH10,
          Text(
            vm.createOfferWizardForms['name'] ?? '',
            style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                letterSpacing: 1.0,
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.normal,
                fontSize: 18),
          ),
          SmartGaps.gapH20,
          Text(
            tr('project_overview'),
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                letterSpacing: 1.0,
                color: PartnerAppColors.spanishGrey,
                fontWeight: FontWeight.w500,
                fontSize: 16),
          ),
          SmartGaps.gapH10,
          Text(
            vm.createOfferWizardForms['description'] ?? '',
            style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                letterSpacing: 1.0,
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.normal,
                fontSize: 18),
          ),
          SmartGaps.gapH20,
          ListView.builder(
            itemCount: vm.industryDescriptions.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              final industryMap = vm.industryDescriptions[index];
              return Card(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '${index + 1}. ${(industryMap['productName'] as String).toCapitalizedFirst()}',
                        style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            color: PartnerAppColors.darkBlue,
                            fontWeight: FontWeight.bold),
                      ),
                      ...(industryMap['subIndustry'] as List).map((sub) {
                        final subIndustryIndex =
                            (industryMap['subIndustry'] as List).indexOf(sub);

                        final subIndustryMap = sub as Map<String, dynamic>;

                        return subIndustryList(
                            industryIndex: index,
                            subIndustryIndex: subIndustryIndex,
                            subIndustryMap: subIndustryMap);
                      })
                    ],
                  ),
                ),
              );
            },
          ),
          SmartGaps.gapH20,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(
                  tr('unit_price_ex_vat'),
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 18),
                ),
              ),
              Text(
                Formatter.curencyFormat(amount: totalPriceWithoutVat),
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.normal,
                    fontSize: 18),
              )
            ],
          ),
          SmartGaps.gapH10,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(
                  tr('vat_value'),
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 18),
                ),
              ),
              Text(
                Formatter.curencyFormat(amount: vat),
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.normal,
                    fontSize: 18),
              )
            ],
          ),
          SmartGaps.gapH10,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(
                  tr('total_price_incl_vat'),
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                      fontSize: 18),
                ),
              ),
              Text(
                Formatter.curencyFormat(amount: totalPriceWithoutVat + vat),
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.normal,
                    fontSize: 18),
              )
            ],
          ),
          SmartGaps.gapH20,
          const Divider(),
          SmartGaps.gapH20,
          Text(tr('project_day_estimate'),
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                  letterSpacing: 1.0,
                  color: PartnerAppColors.darkBlue,
                  fontWeight: FontWeight.w500,
                  fontSize: 18)),
          SmartGaps.gapH10,
          RichText(
              text: TextSpan(children: [
            const WidgetSpan(
                alignment: PlaceholderAlignment.middle,
                child: Icon(
                  FeatherIcons.calendar,
                  color: PartnerAppColors.blue,
                )),
            TextSpan(
                text:
                    ' ${vm.createOfferWizardForms['projectEstimate']} ${tr('days').toCapitalizedFirst()}',
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.w500,
                    fontSize: 16))
          ])),
          SmartGaps.gapH20,
          Text(tr('date_to_start_project'),
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                  letterSpacing: 1.0,
                  color: PartnerAppColors.darkBlue,
                  fontWeight: FontWeight.w500,
                  fontSize: 18)),
          SmartGaps.gapH10,
          RichText(
              text: TextSpan(children: [
            const WidgetSpan(
                alignment: PlaceholderAlignment.middle,
                child: Icon(
                  FeatherIcons.calendar,
                  color: PartnerAppColors.blue,
                )),
            TextSpan(
                text:
                    ' ${Formatter.formatDateStrings(type: DateFormatType.europeanDate, dateString: vm.createOfferWizardForms['projectStartDate'] as String)}',
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.w500,
                    fontSize: 16))
          ])),
          if (vm.createOfferWizardForms['offerCondition'] != 'null') ...[
            SmartGaps.gapH20,
            const Divider(),
            SmartGaps.gapH20,
            Text(tr('standard_text'),
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.w500,
                    fontSize: 18)),
            SmartGaps.gapH10,
            Text(vm.createOfferWizardForms['offerCondition'],
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.w500,
                    fontSize: 16))
          ]
        ],
      );
    });
  }

  Widget subIndustryList(
      {required int industryIndex,
      required int subIndustryIndex,
      required Map<String, dynamic> subIndustryMap}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
              '${industryIndex + 1}.${subIndustryIndex + 1}. ${(subIndustryMap['subIndustryName'] as String).toCapitalizedFirst()}',
              style: Theme.of(context).textTheme.titleSmall!.copyWith(
                  fontSize: 14,
                  color: PartnerAppColors.blue,
                  fontWeight: FontWeight.bold)),
          SmartGaps.gapH5,
          const Divider(),
          SmartGaps.gapH5,
          ListView.separated(
              itemCount: (subIndustryMap['descriptions'] as List).length,
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              physics: const NeverScrollableScrollPhysics(),
              separatorBuilder: (context, descriptionIndex) {
                return const Divider();
              },
              itemBuilder: (context, descriptionIndex) {
                return industryDescriptionList(
                    industryIndex: industryIndex,
                    subIndustryIndex: subIndustryIndex,
                    descriptionIndex: descriptionIndex,
                    subIndustryMap: subIndustryMap);
              })
        ],
      ),
    );
  }

  Widget industryDescriptionList(
      {required int industryIndex,
      required int subIndustryIndex,
      required int descriptionIndex,
      required Map<String, dynamic> subIndustryMap}) {
    final description =
        (subIndustryMap['descriptions'] as List)[descriptionIndex];
    return Text(
        '${industryIndex + 1}.${subIndustryIndex + 1}.${descriptionIndex + 1}. $description',
        style: Theme.of(context).textTheme.titleSmall!.copyWith(
            fontSize: 14,
            color: PartnerAppColors.darkBlue,
            fontWeight: FontWeight.normal));
  }
}
