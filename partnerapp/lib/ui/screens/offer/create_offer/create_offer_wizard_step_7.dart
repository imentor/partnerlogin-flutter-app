import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_contacts/flutter_contacts.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class CreateOfferWizardStepSeven extends StatefulWidget {
  const CreateOfferWizardStepSeven(
      {super.key, required this.formKey, required this.offerVm});
  final GlobalKey<FormBuilderState> formKey;
  final OfferViewModel offerVm;

  @override
  CreateOfferWizardStepSevenState createState() =>
      CreateOfferWizardStepSevenState();
}

class CreateOfferWizardStepSevenState
    extends State<CreateOfferWizardStepSeven> {
  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(tr('customer_info'),
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              letterSpacing: 1.0,
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18)),
      SmartGaps.gapH20,
      if (widget.offerVm.createOfferWizardForms['contact']['NAME'] ==
          'John Doe') ...[
        TextButton(
            style: TextButton.styleFrom(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                    side: const BorderSide(color: PartnerAppColors.darkBlue))),
            onPressed: () async {
              if (await FlutterContacts.requestPermission()) {
                final contact = await FlutterContacts.openExternalPick();

                if (contact != null) {
                  widget.formKey.currentState!.patchValue({
                    'homeOwnerName': contact.displayName,
                    'homeOwnerPhone': contact.phones.first.number
                        .replaceAll('-', '')
                        .replaceAll('(', '')
                        .replaceAll(')', '')
                        .replaceAll(' ', '')
                        .substring(0, 8)
                  });
                }
              }
            },
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              const Icon(
                FeatherIcons.phone,
                color: PartnerAppColors.darkBlue,
              ),
              SmartGaps.gapW10,
              Text(tr('select_phone_contacts'),
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium!
                      .copyWith(color: PartnerAppColors.darkBlue))
            ])),
        SmartGaps.gapH20
      ],
      CustomTextFieldFormBuilder(
        name: 'homeOwnerName',
        labelText: tr('full_name'),
        initialValue: widget.offerVm.createOfferWizardForms['contact']
                    ['NAME'] !=
                'John Doe'
            ? widget.offerVm.createOfferWizardForms['contact']['NAME']
            : '',
        readOnly: widget.offerVm.createOfferWizardForms['contact']['NAME'] !=
            'John Doe',
        validator: FormBuilderValidators.required(errorText: tr('required')),
      ),
      CustomTextFieldFormBuilder(
          name: 'homeOwnerPhone',
          labelText: tr('phone_number'),
          initialValue: widget.offerVm.createOfferWizardForms['contact']
                      ['NAME'] !=
                  'John Doe'
              ? widget.offerVm.createOfferWizardForms['contact']['MOBILE']
              : '',
          inputFormatters: [LengthLimitingTextInputFormatter(8)],
          keyboardType: TextInputType.number,
          readOnly: widget.offerVm.createOfferWizardForms['contact']['NAME'] !=
              'John Doe',
          validator: FormBuilderValidators.equalLength(8,
              errorText: tr('phone_number_length')),
          prefixIcon: Container(
              padding: const EdgeInsets.all(10),
              child: Text('+45',
                  style: Theme.of(context)
                      .textTheme
                      .headlineSmall!
                      .copyWith(fontWeight: FontWeight.normal))))
    ]);
  }
}
