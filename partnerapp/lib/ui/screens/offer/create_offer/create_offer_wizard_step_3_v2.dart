import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/string_utils.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class CreateOfferWizardStepThreeV2 extends StatefulWidget {
  const CreateOfferWizardStepThreeV2({super.key, required this.offerVm});
  final OfferViewModel offerVm;

  @override
  State<CreateOfferWizardStepThreeV2> createState() =>
      _CreateOfferWizardStepThreeV2State();
}

class _CreateOfferWizardStepThreeV2State
    extends State<CreateOfferWizardStepThreeV2> {
  final CurrencyTextInputFormatter _subIndustryPriceFormatter =
      CurrencyTextInputFormatter(
          NumberFormat.currency(locale: 'da', decimalDigits: 2, symbol: ''));

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(tr('project_title_v2'),
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              letterSpacing: 1.0,
              color: PartnerAppColors.spanishGrey,
              fontWeight: FontWeight.bold,
              fontSize: 16)),
      CustomTextFieldFormBuilder(
          name: 'name',
          maxLines: 5,
          initialValue: widget.offerVm.createOfferWizardForms['name']),
      Text(tr('project_overview'),
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              letterSpacing: 1.0,
              color: PartnerAppColors.spanishGrey,
              fontWeight: FontWeight.bold,
              fontSize: 16)),
      CustomTextFieldFormBuilder(
          name: 'description',
          maxLines: 5,
          initialValue: widget.offerVm.createOfferWizardForms['description']),
      SmartGaps.gapH20,
      Text(tr('offer_list'),
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              letterSpacing: 1.0,
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.bold)),
      SmartGaps.gapH10,
      ListView.builder(
          itemCount: widget.offerVm.industryDescriptions.length,
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            final industryMap = widget.offerVm.industryDescriptions[index];
            return Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                          '${index + 1}. ${(industryMap['productName'] as String).toUpperCase()}',
                          style: Theme.of(context)
                              .textTheme
                              .titleSmall!
                              .copyWith(
                                  fontSize: 17,
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.bold)),
                      ...(industryMap['subIndustry'] as List).map((sub) {
                        final subIndustryIndex =
                            (industryMap['subIndustry'] as List).indexOf(sub);
                        final subIndustryMap = sub as Map<String, dynamic>;
                        final descriptions =
                            subIndustryMap['descriptions'] as List;
                        subIndustryMap['descriptions'] = descriptions;

                        return subIndustryList(
                            industryIndex: index,
                            subIndustryIndex: subIndustryIndex,
                            subIndustryMap: subIndustryMap,
                            offerVm: widget.offerVm,
                            industryMapLength:
                                widget.offerVm.industryDescriptions.length,
                            subIndustryMapLength:
                                (industryMap['subIndustry'] as List).length);
                      }),
                    ]));
          }),
      const Divider(thickness: 2),
      SmartGaps.gapH10,
      Builder(builder: (context) {
        double vat = 0;
        double totalPriceWithoutVat = widget.offerVm.industryDescriptions
            .expand((industry) => industry['subIndustry'] as List)
            .fold(0, (sum, subIndustry) => sum + subIndustry['price']);
        vat = totalPriceWithoutVat * .25;

        return Column(children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Flexible(
                child: Text(tr('unit_price_ex_vat'),
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        letterSpacing: 1.0,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                        fontSize: 18))),
            Text(Formatter.curencyFormat(amount: totalPriceWithoutVat),
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.bold,
                    fontSize: 18))
          ]),
          SmartGaps.gapH10,
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Flexible(
                child: Text(tr('vat_value'),
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        letterSpacing: 1.0,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                        fontSize: 18))),
            Text(Formatter.curencyFormat(amount: vat),
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.bold,
                    fontSize: 18))
          ]),
          SmartGaps.gapH10,
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Flexible(
                child: Text(tr('total_price_incl_vat'),
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        letterSpacing: 1.0,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                        fontSize: 18))),
            Text(Formatter.curencyFormat(amount: totalPriceWithoutVat + vat),
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.bold,
                    fontSize: 18))
          ])
        ]);
      })
    ]);
  }

  Widget subIndustryList(
      {required int industryIndex,
      required int subIndustryIndex,
      required int industryMapLength,
      required int subIndustryMapLength,
      required OfferViewModel offerVm,
      required Map<String, dynamic> subIndustryMap}) {
    final subIndustryName = subIndustryMap['subIndustryName'] as String;
    offerVm.showDescription.putIfAbsent(subIndustryIndex, () => false);
    offerVm.isValidateError.putIfAbsent(subIndustryName, () => false);

    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Expanded(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                        '${industryIndex + 1}.${subIndustryIndex + 1} ${subIndustryName.toCapitalizedFirst()}',
                        style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            fontSize: 16,
                            color: PartnerAppColors.darkBlue,
                            fontWeight: FontWeight.normal)),
                    SmartGaps.gapH5,
                    InkWell(
                        onTap: () {
                          offerVm.showDescription
                              .update(subIndustryIndex, (value) => !value);
                          setState(() {});
                        },
                        child: Text(
                            (offerVm.showDescription[subIndustryIndex] == false)
                                ? tr('see_and_correct_description')
                                : tr('hide'),
                            style: Theme.of(context)
                                .textTheme
                                .titleSmall!
                                .copyWith(
                                    fontSize: 14,
                                    color: PartnerAppColors.blue,
                                    fontWeight: FontWeight.normal)))
                  ]),
            ),
            SmartGaps.gapW10,
            Column(children: [
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Column(children: [
                  InkWell(
                      onTap: () {
                        updatePrice(
                            context: context,
                            industryIndex: industryIndex,
                            subIndustryIndex: subIndustryIndex,
                            price: subIndustryMap['price'],
                            offerVm: offerVm,
                            subIndustry: subIndustryName.toCapitalizedFirst());
                      },
                      child: Container(
                          padding: const EdgeInsets.all(10),
                          width: 150,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(3),
                              border: Border.all(
                                  color: offerVm.isValidateError[
                                              subIndustryName] ==
                                          true
                                      ? PartnerAppColors.red
                                      : PartnerAppColors.darkBlue
                                          .withValues(alpha: .4))),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(NumberFormat.currency(
                                        locale: 'da',
                                        symbol: 'kr.',
                                        decimalDigits:
                                            subIndustryMap['price'] == 0
                                                ? 0
                                                : 2)
                                    .format(subIndustryMap['price']))
                              ])))
                ]),
                SmartGaps.gapW5,
                InkWell(
                    child: const Icon(FeatherIcons.trash2,
                        color: PartnerAppColors.spanishGrey, size: 28),
                    onTap: () {
                      offerVm.updateIndustryDescriptions(
                          isDeleteIndustry: true,
                          isDelete: false,
                          isAdd: false,
                          industryIndex: industryIndex,
                          subIndustryIndex: subIndustryIndex,
                          descriptionIndex: 0,
                          description: '');
                    })
              ]),
              Visibility(
                  visible: offerVm.isValidateError[subIndustryName] == true,
                  child: Column(children: [
                    SmartGaps.gapH5,
                    Text(tr('price_cannot_be_lower_than_0'),
                        style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            fontSize: 12,
                            color: PartnerAppColors.red,
                            fontWeight: FontWeight.normal))
                  ]))
            ])
          ]),
          if (offerVm.showDescription[subIndustryIndex] == true) ...[
            SmartGaps.gapH15,
            InkWell(
                onTap: () {
                  changeDrawerRoute(Routes.createOfferWizardDescriptionEdit,
                      arguments: {
                        'industryIndex': industryIndex,
                        'subIndustryIndex': subIndustryIndex,
                        'industryMapLength': industryMapLength,
                        'subIndustryMapLength': subIndustryMapLength,
                        'descriptionList':
                            subIndustryMap['descriptions'] as List,
                        'subIndustryMap': subIndustryMap
                      });
                },
                child: Row(children: [
                  const Icon(FeatherIcons.edit,
                      color: PartnerAppColors.blue, size: 16),
                  SmartGaps.gapW5,
                  Text(tr('edit_description'),
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                          fontSize: 14,
                          color: PartnerAppColors.blue,
                          fontWeight: FontWeight.normal))
                ])),
            SmartGaps.gapH15,
            ListView.separated(
                itemCount: (subIndustryMap['descriptions'] as List).length + 1,
                shrinkWrap: true,
                padding: EdgeInsets.zero,
                physics: const NeverScrollableScrollPhysics(),
                separatorBuilder: (context, descriptionIndex) {
                  return const Divider();
                },
                itemBuilder: (context, descriptionIndex) {
                  return industryDescriptionList(
                      industryIndex: industryIndex,
                      subIndustryIndex: subIndustryIndex,
                      descriptionIndex: descriptionIndex,
                      subIndustryMap: subIndustryMap);
                })
          ],
          if (industryIndex + 1 <= industryMapLength) ...[
            SmartGaps.gapH15,
            if (subIndustryIndex + 1 < subIndustryMapLength) ...[
              const Divider()
            ] else if ((industryIndex + 1 < industryMapLength) &&
                (subIndustryIndex + 1 == subIndustryMapLength)) ...[
              const Divider()
            ]
          ]
        ]));
  }

  Widget industryDescriptionList(
      {required int industryIndex,
      required int subIndustryIndex,
      required int descriptionIndex,
      required Map<String, dynamic> subIndustryMap}) {
    final subIndustryMapList = subIndustryMap['descriptions'] as List;
    if (descriptionIndex < subIndustryMapList.length) {
      final description = subIndustryMapList[descriptionIndex];

      return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text(
            '${industryIndex + 1}.${subIndustryIndex + 1}.${descriptionIndex + 1} $description',
            style: Theme.of(context).textTheme.titleSmall!.copyWith(
                fontSize: 14,
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.normal)),
        SmartGaps.gapH10,
      ]);
    } else {
      return const Column();
    }
  }

  Future<void> updatePrice(
      {required BuildContext context,
      required int industryIndex,
      required int subIndustryIndex,
      required String subIndustry,
      required double price,
      required OfferViewModel offerVm}) {
    final formKey = GlobalKey<FormBuilderState>();
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext dialogContext) {
          return AlertDialog(
              insetPadding: const EdgeInsets.all(20),
              contentPadding: const EdgeInsets.all(20),
              title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                GestureDetector(
                    onTap: () {
                      Navigator.of(dialogContext).pop();
                    },
                    child: const Icon(Icons.close, size: 18))
              ]),
              content: FormBuilder(
                  key: formKey,
                  child: SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: SingleChildScrollView(
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                            Text(subIndustry,
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium!
                                    .copyWith(
                                        color: PartnerAppColors.blue,
                                        fontWeight: FontWeight.bold)),
                            CustomTextFieldFormBuilder(
                                validator: FormBuilderValidators.required(
                                    errorText: tr('required')),
                                initialValue: price.toStringAsFixed(2),
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  _subIndustryPriceFormatter
                                ],
                                name: 'subIndustryPrice',
                                suffixIcon: Container(
                                    padding: const EdgeInsets.fromLTRB(
                                        12, 12, 12, 12),
                                    child: Text('kr.',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headlineMedium!
                                            .copyWith(
                                                fontSize: 18,
                                                fontWeight: FontWeight.normal,
                                                color: PartnerAppColors
                                                    .darkBlue)))),
                            SmartGaps.gapH10,
                            TextButton(
                                style: TextButton.styleFrom(
                                    fixedSize: Size(
                                        MediaQuery.of(context).size.width, 45),
                                    backgroundColor: PartnerAppColors.darkBlue,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(5))),
                                onPressed: () {
                                  offerVm.updateIndustryPrice(
                                      industryIndex: industryIndex,
                                      subIndustryIndex: subIndustryIndex,
                                      price: _subIndustryPriceFormatter
                                          .getUnformattedValue()
                                          .toString());
                                  Navigator.of(dialogContext).pop();
                                },
                                child: Text(tr('update'),
                                    style: Theme.of(context)
                                        .textTheme
                                        .headlineMedium!
                                        .copyWith(
                                            color: Colors.white,
                                            fontWeight: FontWeight.normal)))
                          ])))));
        });
  }
}
