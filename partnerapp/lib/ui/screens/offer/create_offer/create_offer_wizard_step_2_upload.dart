import 'dart:developer';
import 'dart:io';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:file_selector/file_selector.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';

class CreateOfferWizardStepTwoUpload extends StatefulWidget {
  const CreateOfferWizardStepTwoUpload({super.key, required this.offerVm});
  final OfferViewModel offerVm;

  @override
  State<CreateOfferWizardStepTwoUpload> createState() =>
      _CreateOfferWizardStepTwoUploadState();
}

class _CreateOfferWizardStepTwoUploadState
    extends State<CreateOfferWizardStepTwoUpload> {
  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
      Align(
        alignment: Alignment.centerLeft,
        child: Text(tr('upload_offer_here'),
            style: Theme.of(context).textTheme.displayMedium!.copyWith(
                fontSize: 19,
                fontWeight: FontWeight.w500,
                color: Colors.black)),
      ),
      SmartGaps.gapH20,
      Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
              border: Border.all(
                  color: PartnerAppColors.darkBlue.withValues(alpha: .3)),
              borderRadius: BorderRadius.circular(8)),
          child: Column(children: [
            const Icon(FeatherIcons.uploadCloud,
                color: PartnerAppColors.darkBlue, size: 30),
            SmartGaps.gapH10,
            Text(tr('upload_file'),
                style: Theme.of(context).textTheme.displayLarge!.copyWith(
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: PartnerAppColors.darkBlue)),
            SmartGaps.gapH10,
            TextButton(
                style: TextButton.styleFrom(
                    fixedSize: const Size(100, 50),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                        side: const BorderSide(
                            color: PartnerAppColors.darkBlue))),
                onPressed: widget.offerVm.isUploadOfferLoading
                    ? null
                    : () async {
                        if (Platform.isIOS) {
                          var fileResultList =
                              await FilePicker.platform.pickFiles(
                            type: FileType.any,
                            allowMultiple: true,
                          );

                          if (fileResultList != null) {
                            widget.offerVm.addOrDeleteOfferFiles(
                                files: fileResultList.files);
                          }
                        } else if (Platform.isAndroid) {
                          const XTypeGroup fileTypeGroupAndroid = XTypeGroup(
                              label: 'files',
                              extensions: [
                                'pdf',
                                'jpg',
                                'jpeg',
                                'png',
                                'docx',
                                'doc',
                                'xls',
                                'xlsx',
                                'heif',
                                'hevc'
                              ]);

                          final List<XFile> selectedFiles = await openFiles(
                            acceptedTypeGroups: <XTypeGroup>[
                              fileTypeGroupAndroid
                            ],
                          );

                          if (selectedFiles.isNotEmpty) {
                            widget.offerVm.addOrDeleteSelectedOfferFiles(
                                files: selectedFiles);
                            for (var i = 0; i < selectedFiles.length; i++) {
                              log('Selected file: ${selectedFiles[i].name.replaceAll(RegExp('[^A-Za-z0-9]'), '')}.${selectedFiles[i].mimeType!.split('/').last}');
                            }
                          }
                        }
                      },
                child: Text(tr('upload'),
                    style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                          fontWeight: FontWeight.normal,
                          color: PartnerAppColors.darkBlue,
                        )))
          ])),
      SmartGaps.gapH20,
      if (!widget.offerVm.isUploadOfferLoading) ...[
        ListView.separated(
            separatorBuilder: ((context, index) {
              return SmartGaps.gapH10;
            }),
            itemBuilder: (context, index) {
              return Container(
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: PartnerAppColors.spanishGrey.withValues(alpha: .3),
                      borderRadius: BorderRadius.circular(5)),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                            child: Text(Platform.isIOS
                                ? widget.offerVm.uploadOfferFiles[index].name
                                : '${widget.offerVm.selectedOfferFiles[index].name.replaceAll(RegExp('[^A-Za-z0-9]'), '')}.${widget.offerVm.selectedOfferFiles[index].mimeType!.split('/').last}')),
                        SmartGaps.gapH10,
                        GestureDetector(
                            onTap: () {
                              if (Platform.isIOS) {
                                widget.offerVm
                                    .addOrDeleteOfferFiles(index: index);
                              } else if (Platform.isAndroid) {
                                widget.offerVm.addOrDeleteSelectedOfferFiles(
                                    index: index);
                              }
                            },
                            child: const Icon(
                              FeatherIcons.trash2,
                              color: PartnerAppColors.spanishGrey,
                            ))
                      ]));
            },
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: Platform.isIOS
                ? widget.offerVm.uploadOfferFiles.length
                : widget.offerVm.selectedOfferFiles.length)
      ],
      if (widget.offerVm.isUploadOfferLoading) ...[
        const Column(
          children: [
            SmartGaps.gapH30,
            Center(
              child: CircularProgressIndicator(),
            ),
          ],
        )
      ]
    ]);
  }
}
