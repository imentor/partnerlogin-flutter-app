import 'dart:async';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';

class CreateOfferWizardStepFourUpload extends StatefulWidget {
  const CreateOfferWizardStepFourUpload({super.key, required this.offerVm});
  final OfferViewModel offerVm;

  @override
  State<CreateOfferWizardStepFourUpload> createState() =>
      _CreateOfferWizardStepFourUploadState();
}

class _CreateOfferWizardStepFourUploadState
    extends State<CreateOfferWizardStepFourUpload> {
  Timer? _debounce;

  @override
  Widget build(BuildContext context) {
    final filteredIndustries = widget.offerVm.filteredIndustries
        .where((element) => element.industry != '0')
        .toList();
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(tr('choose_type'),
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              letterSpacing: 1.0,
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18)),
      SmartGaps.gapH20,
      CustomTextFieldFormBuilder(
          name: 'industry_search',
          suffixIcon: const Icon(FeatherIcons.search),
          onChanged: (p0) {
            if (_debounce?.isActive ?? false) {
              _debounce?.cancel();
            }
            _debounce = Timer(const Duration(milliseconds: 1500), () {
              if (p0 != null) {
                widget.offerVm.searchIndustry(query: p0);
              }
            });
          }),
      SmartGaps.gapH10,
      GridView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: filteredIndustries.length,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, mainAxisExtent: 180, crossAxisSpacing: 2),
          itemBuilder: (context, index) {
            final industry = filteredIndustries.elementAt(index);
            return InkWell(
                onTap: () {
                  widget.offerVm.updateSelectedIndustryList(industry: industry);
                },
                child: Card(
                    elevation: 1.5,
                    shape: widget.offerVm.selectedIndustries.contains(industry)
                        ? Border.all(color: PartnerAppColors.blue)
                        : Border.all(
                            color: PartnerAppColors.darkBlue
                                .withValues(alpha: .3)),
                    child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                  height: 50,
                                  width: 50,
                                  child: CachedNetworkImage(
                                      imageUrl:
                                          'https://newcrm.kundematch.dk/upload/products/${industry.mpic}',
                                      fit: BoxFit.fill)),
                              SmartGaps.gapH20,
                              FittedBox(
                                  child: Text(industry.producttypeDa ?? 'N/A',
                                      textAlign: TextAlign.center,
                                      style: Theme.of(context)
                                          .textTheme
                                          .displaySmall!
                                          .copyWith(
                                              fontSize: 14,
                                              color:
                                                  PartnerAppColors.darkBlue))),
                              SmartGaps.gapH5,
                              Text(industry.generalDescription ?? 'N/A',
                                  textAlign: TextAlign.center,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: Theme.of(context)
                                      .textTheme
                                      .displaySmall!
                                      .copyWith(
                                          fontSize: 12,
                                          color: PartnerAppColors.spanishGrey))
                            ]))));
          })
    ]);
  }
}
