import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/string_utils.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

class CreateOfferWizardStepThree extends StatefulWidget {
  const CreateOfferWizardStepThree({super.key, required this.offerVm});
  final OfferViewModel offerVm;

  @override
  CreateOfferWizardStepThreeState createState() =>
      CreateOfferWizardStepThreeState();
}

class CreateOfferWizardStepThreeState
    extends State<CreateOfferWizardStepThree> {
  final CurrencyTextInputFormatter _subIndustryPriceFormatter =
      CurrencyTextInputFormatter(
          NumberFormat.currency(locale: 'da', decimalDigits: 2, symbol: ''));
  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Text(tr('project_title_v2'),
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                letterSpacing: 1.0,
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.normal,
                fontSize: 18)),
        RichText(
            text: TextSpan(children: [
          const WidgetSpan(
              alignment: PlaceholderAlignment.middle,
              child: Icon(FeatherIcons.edit,
                  color: PartnerAppColors.blue, size: 16)),
          TextSpan(
              text: ' ${tr('edit')}',
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                  letterSpacing: 1.0,
                  color: PartnerAppColors.blue,
                  fontWeight: FontWeight.normal,
                  fontSize: 16))
        ]))
      ]),
      CustomTextFieldFormBuilder(
          name: 'name',
          maxLines: 5,
          initialValue: widget.offerVm.createOfferWizardForms['name']),
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Text(tr('project_overview'),
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                letterSpacing: 1.0,
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.normal,
                fontSize: 18)),
        RichText(
            text: TextSpan(children: [
          const WidgetSpan(
              alignment: PlaceholderAlignment.middle,
              child: Icon(FeatherIcons.edit,
                  color: PartnerAppColors.blue, size: 16)),
          TextSpan(
              text: ' ${tr('edit')}',
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                  letterSpacing: 1.0,
                  color: PartnerAppColors.blue,
                  fontWeight: FontWeight.normal,
                  fontSize: 16))
        ]))
      ]),
      CustomTextFieldFormBuilder(
          name: 'description',
          maxLines: 5,
          initialValue: widget.offerVm.createOfferWizardForms['description']),
      SmartGaps.gapH20,
      Text(tr('offer_list'),
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              letterSpacing: 1.0,
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.bold)),
      SmartGaps.gapH10,
      ListView.builder(
          itemCount: widget.offerVm.industryDescriptions.length,
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            final industryMap = widget.offerVm.industryDescriptions[index];
            return Card(
                child: Padding(
                    padding: const EdgeInsets.all(20),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                              '${index + 1}. ${(industryMap['productName'] as String).toCapitalizedFirst()}',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                      color: PartnerAppColors.darkBlue,
                                      fontWeight: FontWeight.bold)),
                          ...(industryMap['subIndustry'] as List).map((sub) {
                            final subIndustryIndex =
                                (industryMap['subIndustry'] as List)
                                    .indexOf(sub);

                            final subIndustryMap = sub as Map<String, dynamic>;

                            return subIndustryList(
                                industryIndex: index,
                                subIndustryIndex: subIndustryIndex,
                                subIndustryMap: subIndustryMap);
                          })
                        ])));
          }),
      SmartGaps.gapH10,
      const Divider(),
      SmartGaps.gapH10,
      Builder(builder: (context) {
        double vat = 0;

        double totalPriceWithoutVat = widget.offerVm.industryDescriptions
            .expand((industry) => industry['subIndustry'] as List)
            .fold(0, (sum, subIndustry) => sum + subIndustry['price']);

        vat = totalPriceWithoutVat * .25;

        return Column(children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Flexible(
                child: Text(tr('unit_price_ex_vat'),
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        letterSpacing: 1.0,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                        fontSize: 18))),
            Text(Formatter.curencyFormat(amount: totalPriceWithoutVat),
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.bold,
                    fontSize: 18))
          ]),
          SmartGaps.gapH10,
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Flexible(
                child: Text(tr('vat_value'),
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        letterSpacing: 1.0,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                        fontSize: 18))),
            Text(Formatter.curencyFormat(amount: vat),
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.bold,
                    fontSize: 18))
          ]),
          SmartGaps.gapH10,
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Flexible(
                child: Text(tr('total_price_incl_vat'),
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        letterSpacing: 1.0,
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                        fontSize: 18))),
            Text(Formatter.curencyFormat(amount: totalPriceWithoutVat + vat),
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.bold,
                    fontSize: 18))
          ])
        ]);
      })
    ]);
  }

  Widget subIndustryList(
      {required int industryIndex,
      required int subIndustryIndex,
      required Map<String, dynamic> subIndustryMap}) {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 20),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Flexible(
              child: Text(
                  '${industryIndex + 1}.${subIndustryIndex + 1}. ${(subIndustryMap['subIndustryName'] as String).toCapitalizedFirst()}',
                  style: Theme.of(context).textTheme.titleSmall!.copyWith(
                      fontSize: 14,
                      color: PartnerAppColors.blue,
                      fontWeight: FontWeight.bold)),
            ),
            SmartGaps.gapW10,
            Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
              InkWell(
                  onTap: () {
                    updatePrice(
                        context: context,
                        industryIndex: industryIndex,
                        subIndustryIndex: subIndustryIndex,
                        subIndustry:
                            (subIndustryMap['subIndustryName'] as String)
                                .toCapitalizedFirst(),
                        price: subIndustryMap['price']);
                  },
                  child: Container(
                      padding: const EdgeInsets.all(10),
                      width: 150,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(3),
                          border: Border.all(
                              color: PartnerAppColors.darkBlue
                                  .withValues(alpha: .4))),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(NumberFormat.currency(
                                    locale: 'da',
                                    symbol: 'kr.',
                                    decimalDigits:
                                        subIndustryMap['price'] == 0 ? 0 : 2)
                                .format(subIndustryMap['price']))
                          ]))),
              if (subIndustryMap['errorMessage'] != null) ...[
                Text(tr(subIndustryMap['errorMessage']),
                    style: Theme.of(context)
                        .textTheme
                        .titleSmall!
                        .copyWith(fontSize: 14, color: PartnerAppColors.red))
              ]
            ])
          ]),
          SmartGaps.gapH5,
          const Divider(),
          SmartGaps.gapH5,
          ListView.separated(
              itemCount: (subIndustryMap['descriptions'] as List).length + 1,
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              physics: const NeverScrollableScrollPhysics(),
              separatorBuilder: (context, descriptionIndex) {
                return const Divider();
              },
              itemBuilder: (context, descriptionIndex) {
                return industryDescriptionList(
                    industryIndex: industryIndex,
                    subIndustryIndex: subIndustryIndex,
                    descriptionIndex: descriptionIndex,
                    subIndustryMap: subIndustryMap);
              })
        ]));
  }

  Widget industryDescriptionList(
      {required int industryIndex,
      required int subIndustryIndex,
      required int descriptionIndex,
      required Map<String, dynamic> subIndustryMap}) {
    if (descriptionIndex < (subIndustryMap['descriptions'] as List).length) {
      final description =
          (subIndustryMap['descriptions'] as List)[descriptionIndex];
      return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text(
            '${industryIndex + 1}.${subIndustryIndex + 1}.${descriptionIndex + 1}. $description',
            style: Theme.of(context).textTheme.titleSmall!.copyWith(
                fontSize: 14,
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.normal)),
        SmartGaps.gapH10,
        Row(children: [
          Expanded(
              child: TextButton(
                  style: TextButton.styleFrom(
                      backgroundColor: PartnerAppColors.darkBlue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                  onPressed: () {
                    editDescription(
                        context: context,
                        isAdd: false,
                        industryIndex: industryIndex,
                        subIndustryIndex: subIndustryIndex,
                        descriptionIndex: descriptionIndex,
                        description: description as String);
                  },
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Icon(FeatherIcons.edit,
                            size: 16, color: Colors.white),
                        SmartGaps.gapW5,
                        Text(tr('edit'),
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium!
                                .copyWith(
                                    color: Colors.white,
                                    fontWeight: FontWeight.normal))
                      ]))),
          SmartGaps.gapW10,
          Expanded(
              child: TextButton(
                  style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          side: const BorderSide(color: PartnerAppColors.red),
                          borderRadius: BorderRadius.circular(5))),
                  onPressed: () {
                    context.read<OfferViewModel>().updateIndustryDescriptions(
                        isDeleteIndustry: false,
                        isDelete: true,
                        isAdd: false,
                        industryIndex: industryIndex,
                        subIndustryIndex: subIndustryIndex,
                        descriptionIndex: descriptionIndex,
                        description: description);
                  },
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Icon(
                          FeatherIcons.trash2,
                          size: 16,
                          color: PartnerAppColors.red,
                        ),
                        SmartGaps.gapW5,
                        Text(tr('delete'),
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium!
                                .copyWith(
                                    color: PartnerAppColors.red,
                                    fontWeight: FontWeight.normal))
                      ])))
        ])
      ]);
    } else {
      return Column(children: [
        TextButton(
            style: TextButton.styleFrom(
                backgroundColor: PartnerAppColors.darkBlue,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5))),
            onPressed: () {
              editDescription(
                  context: context,
                  isAdd: true,
                  industryIndex: industryIndex,
                  subIndustryIndex: subIndustryIndex,
                  descriptionIndex: descriptionIndex + 1,
                  description: '');
            },
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              const Icon(FeatherIcons.plusCircle,
                  size: 16, color: Colors.white),
              SmartGaps.gapW5,
              Text(tr('add_description'),
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      color: Colors.white, fontWeight: FontWeight.normal))
            ])),
        SmartGaps.gapH10,
        TextButton(
            style: TextButton.styleFrom(
                shape: RoundedRectangleBorder(
                    side: const BorderSide(color: PartnerAppColors.red),
                    borderRadius: BorderRadius.circular(5))),
            onPressed: () {
              context.read<OfferViewModel>().updateIndustryDescriptions(
                  isDeleteIndustry: true,
                  isDelete: false,
                  isAdd: false,
                  industryIndex: industryIndex,
                  subIndustryIndex: subIndustryIndex,
                  descriptionIndex: descriptionIndex,
                  description: '');
            },
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              const Icon(
                FeatherIcons.trash2,
                size: 17,
                color: PartnerAppColors.red,
              ),
              SmartGaps.gapW5,
              Text('${tr('delete')} ${subIndustryMap['subIndustryName']}',
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      color: PartnerAppColors.red,
                      fontWeight: FontWeight.normal))
            ]))
      ]);
    }
  }

  Future<void> updatePrice(
      {required BuildContext context,
      required int industryIndex,
      required int subIndustryIndex,
      required String subIndustry,
      required double price}) {
    final formKey = GlobalKey<FormBuilderState>();
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext dialogContext) {
          return AlertDialog(
              insetPadding: const EdgeInsets.all(20),
              contentPadding: const EdgeInsets.all(20),
              title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                GestureDetector(
                    onTap: () {
                      Navigator.of(dialogContext).pop();
                    },
                    child: const Icon(Icons.close, size: 18))
              ]),
              content: FormBuilder(
                  key: formKey,
                  child: SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: SingleChildScrollView(
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                            Text(subIndustry,
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium!
                                    .copyWith(
                                        color: PartnerAppColors.blue,
                                        fontWeight: FontWeight.bold)),
                            CustomTextFieldFormBuilder(
                              validator: FormBuilderValidators.required(
                                  errorText: tr('required')),
                              initialValue: price.toStringAsFixed(2),
                              keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[
                                _subIndustryPriceFormatter
                              ],
                              name: 'subIndustryPrice',
                              suffixIcon: Container(
                                padding:
                                    const EdgeInsets.fromLTRB(12, 12, 12, 12),
                                child: Text('kr.',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headlineMedium!
                                        .copyWith(
                                            fontSize: 18,
                                            fontWeight: FontWeight.normal,
                                            color: PartnerAppColors.darkBlue)),
                              ),
                            ),
                            SmartGaps.gapH10,
                            TextButton(
                                style: TextButton.styleFrom(
                                    fixedSize: Size(
                                        MediaQuery.of(context).size.width, 45),
                                    backgroundColor: PartnerAppColors.darkBlue,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(5))),
                                onPressed: () {
                                  context
                                      .read<OfferViewModel>()
                                      .updateIndustryPrice(
                                          industryIndex: industryIndex,
                                          subIndustryIndex: subIndustryIndex,
                                          price: _subIndustryPriceFormatter
                                              .getUnformattedValue()
                                              .toString());
                                  Navigator.of(dialogContext).pop();
                                },
                                child: Text(
                                  tr('update'),
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineMedium!
                                      .copyWith(
                                          color: Colors.white,
                                          fontWeight: FontWeight.normal),
                                ))
                          ])))));
        });
  }

  Future<void> editDescription(
      {required bool isAdd,
      required BuildContext context,
      required int industryIndex,
      required int subIndustryIndex,
      required int descriptionIndex,
      required String description}) {
    final formKey = GlobalKey<FormBuilderState>();
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext dialogContext) {
          return AlertDialog(
              insetPadding: const EdgeInsets.all(20),
              contentPadding: const EdgeInsets.all(20),
              title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                GestureDetector(
                    onTap: () {
                      Navigator.of(dialogContext).pop();
                    },
                    child: const Icon(Icons.close, size: 18))
              ]),
              content: FormBuilder(
                  key: formKey,
                  child: SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: SingleChildScrollView(
                          child: Column(children: [
                        CustomTextFieldFormBuilder(
                            validator: FormBuilderValidators.required(
                                errorText: tr('required')),
                            initialValue: description,
                            name: 'description'),
                        SmartGaps.gapH10,
                        TextButton(
                            style: TextButton.styleFrom(
                                fixedSize:
                                    Size(MediaQuery.of(context).size.width, 45),
                                backgroundColor: PartnerAppColors.darkBlue,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5))),
                            onPressed: () {
                              context
                                  .read<OfferViewModel>()
                                  .updateIndustryDescriptions(
                                      isDeleteIndustry: false,
                                      isDelete: false,
                                      isAdd: isAdd,
                                      industryIndex: industryIndex,
                                      subIndustryIndex: subIndustryIndex,
                                      descriptionIndex: descriptionIndex,
                                      description: formKey
                                          .currentState!
                                          .fields['description']!
                                          .value as String);
                              Navigator.of(dialogContext).pop();
                            },
                            child: Text(isAdd ? tr('add') : tr('update'),
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineMedium!
                                    .copyWith(
                                        color: Colors.white,
                                        fontWeight: FontWeight.normal)))
                      ])))));
        });
  }
}
