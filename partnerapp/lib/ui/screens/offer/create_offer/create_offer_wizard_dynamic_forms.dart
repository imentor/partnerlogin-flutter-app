import 'package:Haandvaerker.dk/model/wizard/wizard_response_new_version_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

class CreateOfferWizardDynamicForms extends StatefulWidget {
  final ProductWizardModelV2 productWizard;
  final String label;
  final int currentStep;
  final int totalStep;

  const CreateOfferWizardDynamicForms(
      {super.key,
      required this.label,
      required this.productWizard,
      required this.currentStep,
      required this.totalStep});

  @override
  CreateOfferWizardDynamicFormsState createState() =>
      CreateOfferWizardDynamicFormsState();
}

class CreateOfferWizardDynamicFormsState
    extends State<CreateOfferWizardDynamicForms> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(widget.label,
            style: Theme.of(context).textTheme.titleMedium!.copyWith(
                color: PartnerAppColors.darkBlue, fontWeight: FontWeight.bold)),
        SmartGaps.gapH10,
        Text('${tr('step')} ${widget.currentStep} / ${widget.totalStep}',
            style: Theme.of(context)
                .textTheme
                .headlineSmall!
                .copyWith(color: PartnerAppColors.spanishGrey)),
        SmartGaps.gapH10,
        ...formsV2()
      ],
    );
  }

  List<Widget> formsV2() {
    final vm = Provider.of<OfferViewModel>(context, listen: true);

    if (widget.productWizard.productFields == null) {
      return [const SizedBox.shrink()];
    }

    return widget.productWizard.productFields!.map((producField) {
      return buildFormField(producField: producField, vm: vm);
    }).toList();
  }

  Widget buildFormField(
      {required ProductWizardFieldV2 producField, required OfferViewModel vm}) {
    switch (producField.productField) {
      case 1:
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Text(
            producField.productName ?? '',
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                letterSpacing: 1.0,
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.normal,
                fontSize: 18),
          ),
        );
      case 2:
        return buildTextFieldFormBuilder(
            key: 'text_2_${widget.productWizard.productId}_${producField.id}',
            producField: producField,
            vm: vm);
      case 3:
        return buildRadioGroupFormBuilder(
            key: 'radio_3_${widget.productWizard.productId}_${producField.id}',
            producField: producField,
            vm: vm);
      case 4:
        return buildCheckboxGroupFormBuilder(
            key: 'checkbox_4_${widget.productWizard.productId}_',
            producField: producField,
            vm: vm);
      default:
        return const SizedBox.shrink();
    }
  }

  Widget buildTextFieldFormBuilder(
      {required String key,
      required ProductWizardFieldV2 producField,
      required OfferViewModel vm}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: CustomTextFieldFormBuilder(
        labelText: producField.productName,
        isRequired: producField.mandatory == 1,
        validator: producField.mandatory == 1
            ? FormBuilderValidators.required(errorText: tr('required'))
            : null,
        name: key,
        initialValue: vm.wizardProductAnswers[key],
        keyboardType: producField.onlyDescription == 1
            ? TextInputType.text
            : TextInputType.number,
      ),
    );
  }

  Widget buildRadioGroupFormBuilder(
      {required String key,
      required ProductWizardFieldV2 producField,
      required OfferViewModel vm}) {
    List<FormBuilderFieldOption<String>> options =
        producField.values?.map((childValue) {
              return FormBuilderFieldOption(
                value: childValue.id!,
                child: Text(
                  childValue.productName!,
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                      ),
                ),
              );
            }).toList() ??
            [];

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomRadioGroupFormBuilder(
            labelText: producField.productName,
            isRequired: producField.mandatory == 1,
            validator: producField.mandatory == 1
                ? FormBuilderValidators.required(errorText: tr('required'))
                : null,
            options: options,
            name: key,
            initialValue: vm.wizardProductAnswers[key],
            onChanged: (p0) {
              if (p0 != null) {
                vm.updateWizardProductAnswers(key: key, value: p0);
              }
            },
          ),
          if (producField.children != null && producField.children!.isNotEmpty)
            ...producField.children!.map((child) {
              return vm.wizardProductAnswers
                      .containsValue(child.parentproductid.toString())
                  ? buildFormField(producField: child, vm: vm)
                  : const SizedBox.shrink();
            })
        ],
      ),
    );
  }

  Widget buildCheckboxGroupFormBuilder(
      {required String key,
      required ProductWizardFieldV2 producField,
      required OfferViewModel vm}) {
    List<FormBuilderFieldOption<String>> options =
        producField.values?.map((childValue) {
              return FormBuilderFieldOption(
                value: childValue.id!,
                child: Text(
                  childValue.productName!,
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                      ),
                ),
              );
            }).toList() ??
            [];

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomCheckboxGroupFormBuilder(
            labelText: producField.productName,
            isRequired: producField.mandatory == 1,
            optionsOrientation: OptionsOrientation.vertical,
            validator: producField.mandatory == 1
                ? FormBuilderValidators.required(errorText: tr('required'))
                : null,
            options: options,
            name: key,
            initialValue: [vm.wizardProductAnswers[key] ?? ''],
            onChanged: (p0) {
              if (p0 != null) {
                vm.updateWizardProductAnswers(
                    key: key,
                    value: '',
                    options: [
                      ...options.map((optionValue) => optionValue.value)
                    ],
                    selectedChecks: p0);
              }
            },
          ),
          if (producField.children != null && producField.children!.isNotEmpty)
            ...producField.children!.map((child) {
              return vm.wizardProductAnswers
                      .containsValue(child.parentproductid.toString())
                  ? buildFormField(producField: child, vm: vm)
                  : const SizedBox.shrink();
            })
        ],
      ),
    );
  }
}

class CreateOfferWizardDynamicFormsV2 extends StatefulWidget {
  final ProductWizardModelV2 productWizard;
  final String label;
  final int currentStep;
  final int totalStep;

  const CreateOfferWizardDynamicFormsV2(
      {super.key,
      required this.label,
      required this.productWizard,
      required this.currentStep,
      required this.totalStep});

  @override
  CreateOfferWizardDynamicFormsV2State createState() =>
      CreateOfferWizardDynamicFormsV2State();
}

class CreateOfferWizardDynamicFormsV2State
    extends State<CreateOfferWizardDynamicFormsV2> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(widget.label,
            style: Theme.of(context).textTheme.titleMedium!.copyWith(
                color: PartnerAppColors.darkBlue, fontWeight: FontWeight.bold)),
        SmartGaps.gapH10,
        Text('${tr('step')} ${widget.currentStep} / ${widget.totalStep}',
            style: Theme.of(context)
                .textTheme
                .headlineSmall!
                .copyWith(color: PartnerAppColors.spanishGrey)),
        SmartGaps.gapH10,
        ...formsV2()
      ],
    );
  }

  List<Widget> formsV2() {
    final vm = Provider.of<OfferViewModel>(context, listen: true);

    if (widget.productWizard.productFields == null) {
      return [const SizedBox.shrink()];
    }

    return widget.productWizard.productFields!.map((producField) {
      return buildFormField(producField: producField, vm: vm);
    }).toList();
  }

  Widget buildFormField(
      {required ProductWizardFieldV2 producField, required OfferViewModel vm}) {
    switch (producField.productField) {
      case 1:
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Text(
            producField.productName ?? '',
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                letterSpacing: 1.0,
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.normal,
                fontSize: 18),
          ),
        );
      case 2:
        return buildTextFieldFormBuilder(
            key: 'text_2_${widget.productWizard.productId}_${producField.id}',
            producField: producField,
            vm: vm);
      case 3:
        return buildRadioGroupFormBuilder(
            key: 'radio_3_${widget.productWizard.productId}_${producField.id}',
            producField: producField,
            vm: vm);
      case 4:
        return buildCheckboxGroupFormBuilder(
            key: 'checkbox_4_${widget.productWizard.productId}_',
            producField: producField,
            vm: vm);
      default:
        return const SizedBox.shrink();
    }
  }

  Widget buildTextFieldFormBuilder(
      {required String key,
      required ProductWizardFieldV2 producField,
      required OfferViewModel vm}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: CustomTextFieldFormBuilder(
        labelText: producField.productName,
        isRequired: producField.mandatory == 1,
        validator: producField.mandatory == 1
            ? FormBuilderValidators.required(errorText: tr('required'))
            : null,
        name: key,
        initialValue: vm.wizardProductAnswers[key],
        keyboardType: producField.onlyDescription == 1
            ? TextInputType.text
            : TextInputType.number,
      ),
    );
  }

  Widget buildRadioGroupFormBuilder(
      {required String key,
      required ProductWizardFieldV2 producField,
      required OfferViewModel vm}) {
    List<FormBuilderFieldOption<String>> options =
        producField.values?.map((childValue) {
              return FormBuilderFieldOption(
                value: childValue.id!,
                child: Text(
                  childValue.productName!,
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                      ),
                ),
              );
            }).toList() ??
            [];

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomRadioGroupFormBuilder(
            labelText: producField.productName,
            isRequired: producField.mandatory == 1,
            validator: producField.mandatory == 1
                ? FormBuilderValidators.required(errorText: tr('required'))
                : null,
            options: options,
            name: key,
            initialValue: vm.wizardProductAnswers[key],
            onChanged: (p0) {
              if (p0 != null) {
                vm.updateWizardProductAnswers(key: key, value: p0);
              }
            },
          ),
          if (producField.children != null && producField.children!.isNotEmpty)
            ...producField.children!.map((child) {
              return vm.wizardProductAnswers
                      .containsValue(child.parentproductid.toString())
                  ? buildFormField(producField: child, vm: vm)
                  : const SizedBox.shrink();
            })
        ],
      ),
    );
  }

  Widget buildCheckboxGroupFormBuilder(
      {required String key,
      required ProductWizardFieldV2 producField,
      required OfferViewModel vm}) {
    List<FormBuilderFieldOption<String>> options =
        producField.values?.map((childValue) {
              return FormBuilderFieldOption(
                value: childValue.id!,
                child: Text(
                  childValue.productName!,
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        color: PartnerAppColors.darkBlue,
                        fontWeight: FontWeight.normal,
                      ),
                ),
              );
            }).toList() ??
            [];

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomCheckboxGroupFormBuilder(
            labelText: producField.productName,
            isRequired: producField.mandatory == 1,
            optionsOrientation: OptionsOrientation.vertical,
            validator: producField.mandatory == 1
                ? FormBuilderValidators.required(errorText: tr('required'))
                : null,
            options: options,
            name: key,
            initialValue: [vm.wizardProductAnswers[key] ?? ''],
            onChanged: (p0) {
              if (p0 != null) {
                vm.updateWizardProductAnswers(
                    key: key,
                    value: '',
                    options: [
                      ...options.map((optionValue) => optionValue.value)
                    ],
                    selectedChecks: p0);
              }
            },
          ),
          if (producField.children != null && producField.children!.isNotEmpty)
            ...producField.children!.map((child) {
              return vm.wizardProductAnswers
                      .containsValue(child.parentproductid.toString())
                  ? buildFormField(producField: child, vm: vm)
                  : const SizedBox.shrink();
            })
        ],
      ),
    );
  }
}
