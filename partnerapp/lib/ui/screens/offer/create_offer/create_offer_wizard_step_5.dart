import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class CreateOfferWizardStepFive extends StatefulWidget {
  const CreateOfferWizardStepFive({super.key});

  @override
  CreateOfferWizardStepFiveState createState() =>
      CreateOfferWizardStepFiveState();
}

class CreateOfferWizardStepFiveState extends State<CreateOfferWizardStepFive> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('add_reservations_to_offer'),
          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
              letterSpacing: 1.0,
              color: PartnerAppColors.darkBlue,
              fontWeight: FontWeight.bold,
              fontSize: 18),
        ),
        SmartGaps.gapH10,
        CustomRadioGroupFormBuilder(
          options: [
            FormBuilderFieldOption(
              value: 'yes',
              child: Text(
                tr('save_as_default_text'),
                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.normal,
                    ),
              ),
            )
          ],
          name: 'saveConditionDefault',
        ),
        CustomTextFieldFormBuilder(
          name: 'offerCondition',
          hintText: tr('add_conditions_to_offer'),
          maxLines: 10,
        )
      ],
    );
  }
}
