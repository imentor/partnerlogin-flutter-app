import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/components/signature_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/messages/components.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/iterable_utils.dart';
import 'package:Haandvaerker.dk/utils/string_utils.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app_links/app_links_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class SignatureOffer extends StatefulWidget {
  const SignatureOffer({super.key});

  @override
  State<SignatureOffer> createState() => _SignatureOfferState();
}

class _SignatureOfferState extends State<SignatureOffer> {
  bool seeMore = false;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final appLinksVm = context.read<AppLinksViewModel>();
      final offerVm = context.read<OfferViewModel>();

      if (appLinksVm.offerId.isEmpty) {
        backDrawerRoute();
      } else {
        await tryCatchWrapper(
                context: myGlobals.homeScaffoldKey!.currentContext!,
                function: offerVm.getViewedSignatureOffer(
                    offerId: int.parse(appLinksVm.offerId)))
            .whenComplete(() async {});
      }

      appLinksVm.resetLoginAppLink();
    });
    super.initState();
  }

  void onPressed() async {
    final offerVm = context.read<OfferViewModel>();
    final clientVm = context.read<ClientsViewModel>();
    final tenderFolderVm = context.read<TenderFolderViewModel>();
    final createOfferVm = context.read<CreateOfferViewModel>();
    final newsfeedVm = context.read<NewsFeedViewModel>();
    final termsVm = context.read<SharedDataViewmodel>();

    final signature = await signatureDialog(
        context: context,
        label: termsVm.termsLabel,
        htmlTerms: termsVm.termsHtml);

    if (mounted && signature != null) {
      modalManager.showLoadingModal();

      await tryCatchWrapper(
              context: myGlobals.homeScaffoldKey!.currentContext,
              function: offerVm.addSignatureToOffer(signature: signature))
          .then((signatureResponse) async {
        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: Future.wait([
              clientVm.getClients(
                  tenderVm: tenderFolderVm, createOfferVm: createOfferVm),
              newsfeedVm.getLatestJobFeed(),
            ]));

        if (!mounted) return;

        modalManager.hideLoadingModal();
        showOkAlertDialog(
                context: context, message: tr('offer_has_been_signed'))
            .whenComplete(() {
          backDrawerRoute();
          changeDrawerRoute(Routes.yourClient);
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      bottomNavigationBar: Container(
          padding:
              const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
          color: Colors.white,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 60,
                child: CustomButton(
                  onPressed: () async {},
                  text: tr('sign'),
                ),
              ),
            ],
          )),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: Consumer<OfferViewModel>(
          builder: (_, vm, __) {
            return Skeletonizer(
              enabled: vm.busy,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (vm.viewedSignatureOffer?.offerType == 'normal') ...[
                    ...vm.viewedSignatureOfferListPrice
                        .groupBy((value) => value.industryName ?? '')
                        .entries
                        .map((map) {
                      final index = vm.viewedSignatureOfferListPrice
                          .groupBy((value) => value.industryName ?? '')
                          .keys
                          .toList()
                          .indexOf(map.key);

                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: const EdgeInsets.only(bottom: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${index + 1}. ${(map.key)}",
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineMedium!
                                      .copyWith(
                                          fontWeight: FontWeight.bold,
                                          color: PartnerAppColors.blue),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                ...map.value.map((listPrice) {
                                  final typeIndex =
                                      map.value.indexOf(listPrice);

                                  return Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.all(20),
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: PartnerAppColors.darkBlue
                                                    .withValues(alpha: .4)),
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text(
                                                  "${index + 1}.${typeIndex + 1} ${(listPrice.jobIndustryName ?? '')}",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headlineMedium!
                                                      .copyWith(
                                                          fontWeight:
                                                              FontWeight.bold),
                                                ),
                                                const SizedBox(
                                                  width: 10,
                                                ),
                                                Text(
                                                  Formatter.curencyFormat(
                                                      amount: num.parse(
                                                          listPrice.total ??
                                                              '0')),
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headlineMedium!
                                                      .copyWith(
                                                          fontWeight:
                                                              FontWeight.bold),
                                                ),
                                              ],
                                            ),
                                            const SizedBox(
                                              height: 5,
                                            ),
                                            if (listPrice.series != null ||
                                                listPrice.series is List) ...[
                                              ...(listPrice.series as List)
                                                  .map((description) {
                                                final descriptionIndex =
                                                    (listPrice.series as List)
                                                        .indexOf(description);

                                                return Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(
                                                      vertical: 5,
                                                      horizontal: 20),
                                                  child: Text(
                                                      "${index + 1}.${typeIndex + 1}.${descriptionIndex + 1}  $description",
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .titleSmall!
                                                          .copyWith(
                                                              fontSize: 14,
                                                              color:
                                                                  PartnerAppColors
                                                                      .darkBlue,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal)),
                                                );
                                              })
                                            ] else ...[
                                              ...(listPrice.seriesOriginal
                                                      as List)
                                                  .map((description) {
                                                final descriptionIndex =
                                                    (listPrice.seriesOriginal
                                                            as List)
                                                        .indexOf(description);

                                                return Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(vertical: 5),
                                                  child: Text(
                                                      "1.1.${descriptionIndex + 1}  $description",
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .titleSmall!
                                                          .copyWith(
                                                              fontSize: 14,
                                                              color:
                                                                  PartnerAppColors
                                                                      .darkBlue,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal)),
                                                );
                                              })
                                            ],
                                          ],
                                        ),
                                      ),
                                      if ((listPrice.note ?? '')
                                          .isNotEmpty) ...[
                                        const SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          tr('craftsman_standard_text'),
                                          style: Theme.of(context)
                                              .textTheme
                                              .headlineMedium!
                                              .copyWith(
                                                fontWeight: FontWeight.bold,
                                              ),
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Container(
                                          width: double.infinity,
                                          padding: const EdgeInsets.all(20),
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: PartnerAppColors
                                                      .darkBlue
                                                      .withValues(alpha: .4)),
                                              borderRadius:
                                                  BorderRadius.circular(5)),
                                          child: Text(listPrice.note ?? '',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .titleSmall!
                                                  .copyWith(
                                                      fontSize: 14,
                                                      color: PartnerAppColors
                                                          .darkBlue,
                                                      fontWeight:
                                                          FontWeight.normal)),
                                        )
                                      ],
                                    ],
                                  );
                                })
                              ],
                            ),
                          )
                        ],
                      );
                    })
                  ] else ...[
                    if ((vm.viewedSignatureOffer?.offerFile ?? '').isNotEmpty)
                      Container(
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: PartnerAppColors.spanishGrey
                                  .withValues(alpha: .3),
                              borderRadius: BorderRadius.circular(5)),
                          height: seeMore
                              ? (MediaQuery.of(context).size.height / 2)
                              : 300,
                          child: Column(
                            children: [
                              Expanded(
                                  child: SfPdfViewer.network(
                                      vm.viewedSignatureOffer!.offerFile!)),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    seeMore = !seeMore;
                                  });
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Center(
                                    child: Text(
                                        seeMore ? tr('hide') : tr('see_more'),
                                        style: Theme.of(context)
                                            .textTheme
                                            .headlineMedium!
                                            .copyWith(
                                                letterSpacing: 1.0,
                                                color: PartnerAppColors.blue,
                                                fontWeight: FontWeight.normal,
                                                fontSize: 18)),
                                  ),
                                ),
                              )
                            ],
                          ))
                  ],
                  SmartGaps.gapH10,
                  const Divider(),
                  SmartGaps.gapH10,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Text(
                          tr('unit_price_ex_vat'),
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(
                                  letterSpacing: 1.0,
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 18),
                        ),
                      ),
                      Text(
                        Formatter.curencyFormat(
                          amount: vm.viewedSignatureOffer?.subtotalPrice ?? 0,
                        ),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                letterSpacing: 1.0,
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 18),
                      )
                    ],
                  ),
                  SmartGaps.gapH10,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Text(
                          tr('vat_value'),
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(
                                  letterSpacing: 1.0,
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 18),
                        ),
                      ),
                      Text(
                        Formatter.curencyFormat(
                            amount: vm.viewedSignatureOffer?.vat ?? 0),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                letterSpacing: 1.0,
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 18),
                      )
                    ],
                  ),
                  SmartGaps.gapH10,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Text(
                          tr('total_price_incl_vat'),
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(
                                  letterSpacing: 1.0,
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 18),
                        ),
                      ),
                      Text(
                        Formatter.curencyFormat(
                            amount: vm.viewedSignatureOffer?.totalVat ?? 0),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                letterSpacing: 1.0,
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.normal,
                                fontSize: 18),
                      )
                    ],
                  ),
                  SmartGaps.gapH10,
                  const Divider(),
                  SmartGaps.gapH20,
                  Text(tr('project_day_estimate'),
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(
                              letterSpacing: 1.0,
                              color: PartnerAppColors.darkBlue,
                              fontWeight: FontWeight.w500,
                              fontSize: 18)),
                  SmartGaps.gapH10,
                  RichText(
                      text: TextSpan(children: [
                    const WidgetSpan(
                        alignment: PlaceholderAlignment.middle,
                        child: Icon(
                          FeatherIcons.calendar,
                          color: PartnerAppColors.blue,
                        )),
                    TextSpan(
                        text:
                            ' ${tr('days').toCapitalizedFirst()} ${vm.viewedSignatureOfferListPrice.isNotEmpty ? vm.viewedSignatureOfferListPrice.first.weeks ?? '' : ''}',
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                letterSpacing: 1.0,
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.w500,
                                fontSize: 16))
                  ])),
                  SmartGaps.gapH20,
                  Text(tr('date_to_start_project'),
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(
                              letterSpacing: 1.0,
                              color: PartnerAppColors.darkBlue,
                              fontWeight: FontWeight.w500,
                              fontSize: 18)),
                  SmartGaps.gapH10,
                  RichText(
                      text: TextSpan(children: [
                    const WidgetSpan(
                        alignment: PlaceholderAlignment.middle,
                        child: Icon(
                          FeatherIcons.calendar,
                          color: PartnerAppColors.blue,
                        )),
                    TextSpan(
                        text:
                            ' ${vm.viewedSignatureOfferListPrice.isNotEmpty ? Formatter.formatDateStrings(type: DateFormatType.standardDate, dateString: vm.viewedSignatureOfferListPrice.first.startDate) : ''}',
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                letterSpacing: 1.0,
                                color: PartnerAppColors.darkBlue,
                                fontWeight: FontWeight.w500,
                                fontSize: 16))
                  ])),
                  SmartGaps.gapH20,
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: tr('your_offer_is_valid'),
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(
                                  letterSpacing: 1.0,
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 18),
                        ),
                        TextSpan(
                          text: ' ${vm.viewedSignatureOffer?.expiry ?? 0} ',
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(
                                  letterSpacing: 1.0,
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                        ),
                        TextSpan(
                          text: tr('days_valid'),
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(
                                  letterSpacing: 1.0,
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 18),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
