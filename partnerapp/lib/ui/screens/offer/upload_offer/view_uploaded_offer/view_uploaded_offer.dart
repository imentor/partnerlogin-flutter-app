import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

Future<void> viewUploadedOffer(
    {required BuildContext context,
    required int offerId,
    String? pdfUrl,
    String? subtotalPrice,
    String? totalPrice}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          ),
        ]),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: ViewUploadedOffer(
              offerId: offerId,
              pdfUrl: pdfUrl,
              subTotalPrice: subtotalPrice,
              totalPrice: totalPrice,
            ))),
      );
    },
  );
}

Future<void> viewPdf({required BuildContext context, required String url}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                Flexible(
                    child: SfPdfViewer.network(
                  url,
                ))
              ],
            )),
      );
    },
  );
}

class ViewUploadedOffer extends StatefulWidget {
  const ViewUploadedOffer(
      {super.key,
      required this.offerId,
      this.pdfUrl,
      this.subTotalPrice,
      this.totalPrice});

  final int offerId;
  final String? pdfUrl;
  final String? subTotalPrice;
  final String? totalPrice;

  @override
  ViewUploadedOfferState createState() => ViewUploadedOfferState();
}

class ViewUploadedOfferState extends State<ViewUploadedOffer> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final vm = context.read<OfferViewModel>();
      await tryCatchWrapper(
          context: myGlobals.homeScaffoldKey!.currentContext,
          function: vm.getOfferById(offerId: widget.offerId));
    });
    super.initState();
  }

  @override
  void dispose() {
    // APPLIC.cancelToken.cancel('cancel requests');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<OfferViewModel>(builder: (_, vm, __) {
      if (vm.busy) return const Center(child: ConnectivityLoader());

      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(tr('offer_list'),
              style: Theme.of(context).textTheme.headlineMedium),
          SmartGaps.gapH30,
          if (vm.viewedOffer == null) ...[
            Align(
              alignment: Alignment.center,
              child: Text(tr('error'),
                  style: Theme.of(context).textTheme.headlineMedium),
            )
          ] else ...[
            ListTile(
              contentPadding: EdgeInsets.zero,
              leading: SvgPicture.asset(
                SvgIcons.pdfFileFormat,
                width: 40,
                height: 40,
              ),
              title: Text(
                vm.viewedOffer?.title ?? '',
                maxLines: 2,
              ),
              trailing: PopupMenuButton(
                itemBuilder: (BuildContext context) {
                  return [
                    PopupMenuItem(
                      value: 1,
                      child: Row(
                        children: [
                          const Icon(FeatherIcons.eye),
                          SmartGaps.gapW10,
                          Text(tr('view_file'))
                        ],
                      ),
                    ),
                    PopupMenuItem(
                      value: 2,
                      child: Row(
                        children: [
                          const Icon(FeatherIcons.download),
                          SmartGaps.gapW10,
                          Text(tr('download_file'))
                        ],
                      ),
                    )
                  ];
                },
                child: const Icon(
                  FeatherIcons.moreVertical,
                  color: PartnerAppColors.darkBlue,
                ),
                onSelected: (value) async {
                  switch (value) {
                    case 1:
                      viewPdf(
                          context: context,
                          url: widget.pdfUrl ??
                              (vm.viewedOffer?.offerFile ?? ''));
                      // showUrlViewer(context, vm.viewedOffer?.offerFile ?? '',
                      //     isPdf: true, pdfUrl: vm.viewedOffer?.offerFile ?? '');
                      break;
                    case 2:
                      Navigator.of(context).pop();
                      modalManager.showLoadingModal(
                          message: 'Downloading file...');

                      await tryCatchWrapper(
                              context:
                                  myGlobals.homeScaffoldKey!.currentContext,
                              function: vm.downloadFile(
                                  name:
                                      'Offer: ${vm.viewedOffer?.projectId ?? 0}',
                                  url: widget.pdfUrl ??
                                      (vm.viewedOffer?.offerFile ?? '')))
                          .then((value) async {
                        modalManager.hideLoadingModal();
                        if (value != null) {
                          await showOkAlertDialog(
                              context:
                                  myGlobals.homeScaffoldKey!.currentContext!,
                              message: value
                                  ? tr('file_downloaded')
                                  : tr('error_occurred'),
                              okLabel: 'OK');
                        }
                      });
                      break;
                    default:
                  }
                },
              ),
            ),
            SmartGaps.gapH10,
            const Divider(
              color: PartnerAppColors.darkBlue,
            ),
            SmartGaps.gapH10,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(tr('total_price_exl_vat'),
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        color: PartnerAppColors.darkBlue,
                        fontSize: 16,
                        fontWeight: FontWeight.normal)),
                Text(
                    (widget.subTotalPrice != null &&
                            widget.subTotalPrice!.isNotEmpty)
                        ? '${NumberFormat.decimalPattern('da').format(widget.subTotalPrice!.contains('.') ? double.parse(widget.subTotalPrice!) : int.parse(widget.subTotalPrice!))} kr.'
                        : '${NumberFormat.decimalPattern('da').format((vm.viewedOffer?.subtotalPrice ?? 0))} kr.',
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        color: PartnerAppColors.darkBlue,
                        fontSize: 16,
                        fontWeight: FontWeight.normal))
              ],
            ),
            SmartGaps.gapH10,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(tr('total_price_incl_vat'),
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        color: PartnerAppColors.darkBlue,
                        fontSize: 16,
                        fontWeight: FontWeight.normal)),
                Text(
                    (widget.totalPrice != null && widget.totalPrice!.isNotEmpty)
                        ? '${NumberFormat.decimalPattern('da').format(widget.totalPrice!.contains('.') ? double.parse(widget.totalPrice!) : int.parse(widget.totalPrice!))} kr.'
                        : '${NumberFormat.decimalPattern('da').format((vm.viewedOffer?.totalVat ?? 0))} kr.',
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        color: PartnerAppColors.darkBlue,
                        fontSize: 16,
                        fontWeight: FontWeight.normal))
              ],
            ),
          ]
        ],
      );
    });
  }
}
