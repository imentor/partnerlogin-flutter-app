import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/components/signature_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/upload_offer/create_upload_offer/upload_offer_step_1.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/upload_offer/create_upload_offer/upload_offer_step_2.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/upload_offer/create_upload_offer/upload_offer_step_3.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/upload_offer/create_upload_offer/upload_offer_step_4.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart' hide TextDirection;
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';
import 'package:timeline_tile/timeline_tile.dart';

Future<void> chooseOfferDialog(
    {required BuildContext context,
    required VoidCallback uploadOffer,
    required VoidCallback createOffer}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  tr('how_will_you_offer'),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.displayLarge!.copyWith(
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                        color: PartnerAppColors.darkBlue,
                      ),
                ),
                SmartGaps.gapH20,
                CustomDesignTheme.flatButtonStyle(
                  height: 50,
                  backgroundColor: PartnerAppColors.darkBlue,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  onPressed: () {
                    Navigator.of(dialogContext).pop();
                    uploadOffer();
                  },
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          tr('upload_offer'),
                          style: Theme.of(context)
                              .textTheme
                              .displayLarge!
                              .copyWith(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                                color: Colors.white,
                              ),
                        )
                      ]),
                ),
                SmartGaps.gapH10,
                CustomDesignTheme.flatButtonStyle(
                  height: 50,
                  backgroundColor: PartnerAppColors.darkBlue,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  onPressed: () async {
                    Navigator.of(dialogContext).pop();
                    createOffer();
                  },
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          tr('create_offer'),
                          style: Theme.of(context)
                              .textTheme
                              .displayLarge!
                              .copyWith(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                                color: Colors.white,
                              ),
                        )
                      ]),
                )
              ],
            ))),
      );
    },
  );
}

Future<bool?> priceHasVatDialog({
  required BuildContext context,
}) {
  bool isConfirmYes = false;
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return StatefulBuilder(builder: (context, setState) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: const EdgeInsets.all(20),
          title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            InkWell(
              onTap: () {
                Navigator.of(dialogContext).pop();
              },
              child: const Icon(
                FeatherIcons.x,
                color: PartnerAppColors.spanishGrey,
              ),
            ),
          ]),
          content: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: SingleChildScrollView(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    isConfirmYes
                        ? tr('are_you_sure_has_vat')
                        : tr('does_it_include_vat'),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.displayLarge!.copyWith(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: PartnerAppColors.darkBlue,
                        ),
                  ),
                  SmartGaps.gapH20,
                  CustomDesignTheme.flatButtonStyle(
                    height: 50,
                    backgroundColor: PartnerAppColors.darkBlue,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    onPressed: () {
                      if (!isConfirmYes) {
                        setState(() {
                          isConfirmYes = true;
                        });
                      } else {
                        Navigator.of(dialogContext).pop(true);
                      }
                    },
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            isConfirmYes ? tr('yes_im_sure') : tr('yes'),
                            style: Theme.of(context)
                                .textTheme
                                .displayLarge!
                                .copyWith(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white,
                                ),
                          )
                        ]),
                  ),
                  SmartGaps.gapH10,
                  CustomDesignTheme.flatButtonStyle(
                    height: 50,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                        side:
                            const BorderSide(color: PartnerAppColors.darkBlue)),
                    onPressed: () async {
                      Navigator.of(dialogContext).pop(false);
                    },
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            isConfirmYes ? tr('include_vat') : tr('no'),
                            style: Theme.of(context)
                                .textTheme
                                .displayLarge!
                                .copyWith(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400,
                                  color: PartnerAppColors.darkBlue,
                                ),
                          )
                        ]),
                  )
                ],
              ))),
        );
      });
    },
  );
}

class UploadOfferPage extends StatefulWidget {
  const UploadOfferPage({super.key, this.job});

  final PartnerJobModel? job;

  @override
  UploadOfferPageState createState() => UploadOfferPageState();
}

class UploadOfferPageState extends State<UploadOfferPage> {
  final formKey = GlobalKey<FormBuilderState>();

  final scrollController = ScrollController();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final offerVm = context.read<OfferViewModel>();
      final createOfferVm = context.read<CreateOfferViewModel>();

      int projectParentId = widget.job?.projectParentId != null
          ? int.parse(widget.job?.projectParentId)
          : (widget.job?.projectId ?? 0);

      createOfferVm.activeJob = widget.job;

      modalManager.setContext(myGlobals.homeScaffoldKey!.currentContext!);
      offerVm.isUploadOfferLoading = true;

      await tryCatchWrapper(
              context: myGlobals.homeScaffoldKey!.currentContext,
              function: Future.value(offerVm.initUploadOffer(
                  contactId: widget.job?.customerId ?? 0,
                  projectId: widget.job?.contractType == 'Fagentreprise'
                      ? (int.parse(
                          (widget.job?.parentProject?.id ?? '0') as String))
                      : widget.job?.projectId ?? '0')))
          .whenComplete(() async {
        if (offerVm.isUploadOfferUpdate) {
          await Future.wait([
            createOfferVm.getListPriceMinbolig(
              projectId: projectParentId,
              contactId: widget.job?.customerId ?? 0,
            ),
            offerVm.getOfferByIdV2(offerId: widget.job?.offerId)
          ]);
        }
        offerVm.isUploadOfferLoading = false;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      bottomNavigationBar: Consumer<OfferViewModel>(builder: (_, vm, __) {
        return Container(
          padding:
              const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
          color: Colors.white,
          child: Row(children: [
            if (vm.uploadOfferStep != 0) ...[
              Expanded(
                  child: TextButton(
                      style: TextButton.styleFrom(
                          fixedSize:
                              Size(MediaQuery.of(context).size.width, 45),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                              side: const BorderSide(
                                  color: PartnerAppColors.darkBlue))),
                      onPressed: () {
                        vm.updateUploadOfferSteps(
                            uploadOfferStep: vm.uploadOfferStep - 1);
                      },
                      child: Text(
                        tr('back'),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(color: PartnerAppColors.darkBlue),
                      ))),
              SmartGaps.gapW8,
            ],
            Expanded(
                child: TextButton(
                    style: TextButton.styleFrom(
                        fixedSize: Size(MediaQuery.of(context).size.width, 45),
                        backgroundColor: PartnerAppColors.darkBlue,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                        )),
                    onPressed: () async {
                      switch (vm.uploadOfferStep + 1) {
                        case 1:
                          if (vm.uploadOfferFiles.isNotEmpty) {
                            modalManager.showLoadingModal();
                            await tryCatchWrapper(
                                    context: myGlobals
                                        .homeScaffoldKey!.currentContext,
                                    function: vm.mergeFiles())
                                .whenComplete(
                                    () => modalManager.hideLoadingModal());
                          } else if (vm.selectedOfferFiles.isNotEmpty) {
                            modalManager.showLoadingModal();
                            await tryCatchWrapper(
                                    context: myGlobals
                                        .homeScaffoldKey!.currentContext,
                                    function: vm.mergeFiles())
                                .whenComplete(
                                    () => modalManager.hideLoadingModal());
                          }
                          break;
                        case 2:
                          if (formKey.currentState!.validate()) {
                            formKey.currentState!.fields.forEach((key, value) {
                              vm.updateUploadOfferForms(
                                  key: key,
                                  value: double.parse(value.value
                                      .replaceAll('.', '')
                                      .replaceAll(',', '.')));
                            });
                            vm.updateUploadOfferForms(
                                key: 'priceHasVat', value: false);

                            vm.updateUploadOfferSteps(
                                uploadOfferStep: vm.uploadOfferStep + 1);
                          }
                          break;
                        case 3:
                          if (formKey.currentState!.validate()) {
                            formKey.currentState!.fields.forEach((key, value) {
                              vm.updateUploadOfferForms(
                                  key: key, value: '${value.value}');
                            });

                            vm.updateUploadOfferSteps(
                                uploadOfferStep: vm.uploadOfferStep + 1);
                          }
                          break;
                        case 4:
                          if (vm.csUserId.isNotEmpty) {
                            showOkCancelAlertDialog(
                                    context: context,
                                    message: tr('send_offer_by_cs'),
                                    okLabel: tr('yes'),
                                    cancelLabel: tr('no'))
                                .then((value) {
                              if (value == OkCancelResult.ok) {
                                sendOffer(vm: vm);
                              }
                            });
                          } else {
                            sendOffer(vm: vm);
                          }
                          break;
                        default:
                      }
                    },
                    child: Text(
                      vm.uploadOfferStep + 1 == 4 ? tr('done') : tr('next'),
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(color: Colors.white),
                    ))),
          ]),
        );
      }),
      body: Consumer<OfferViewModel>(builder: (_, vm, __) {
        return SingleChildScrollView(
          physics: vm.seeMore
              ? const NeverScrollableScrollPhysics()
              : const AlwaysScrollableScrollPhysics(),
          controller: scrollController,
          padding: const EdgeInsets.all(20),
          child: FormBuilder(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 65,
                  width: MediaQuery.of(context).size.width,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: 4,
                      itemBuilder: (context, index) {
                        final isCurrentPage = index <= vm.uploadOfferStep;
                        return TimelineTile(
                          axis: TimelineAxis.horizontal,
                          alignment: TimelineAlign.center,
                          isFirst: index == 0,
                          isLast: index == 4 - 1,
                          indicatorStyle: IndicatorStyle(
                            width: 30.0,
                            height: 30.0,
                            indicator: DecoratedBox(
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border:
                                    Border.all(color: PartnerAppColors.blue),
                                color: isCurrentPage
                                    ? PartnerAppColors.blue
                                    : Colors.white,
                              ),
                              child: Center(
                                  child: Text('${index + 1}',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodySmall!
                                          .copyWith(
                                              color: isCurrentPage
                                                  ? Colors.white
                                                  : PartnerAppColors.blue,
                                              fontWeight: FontWeight.bold))),
                            ),
                          ),
                          beforeLineStyle: const LineStyle(
                              thickness: 1.0,
                              color: PartnerAppColors.spanishGrey),
                          endChild: Container(
                            margin: const EdgeInsets.only(top: 5),
                            constraints: const BoxConstraints(
                              maxHeight: 30,
                              minWidth: 50,
                            ),
                            child: Text('${tr('step')} ${index + 1}',
                                style: Theme.of(context).textTheme.bodySmall,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.center),
                          ),
                        );
                      }),
                ),
                SmartGaps.gapH20,
                step(step: vm.uploadOfferStep, vm: vm)
              ],
            ),
          ),
        );
      }),
    );
  }

  void sendOffer({required OfferViewModel vm}) async {
    final clientVm = context.read<ClientsViewModel>();
    final tenderFolderVm = context.read<TenderFolderViewModel>();
    final createOfferVm = context.read<CreateOfferViewModel>();
    final newsfeedVm = context.read<NewsFeedViewModel>();

    final signature = await signatureDialog(
        context: context,
        label: context.read<SharedDataViewmodel>().termsLabel,
        htmlTerms: context.read<SharedDataViewmodel>().termsHtml,
        buttonLabel: 'done_send_offer');

    if (signature != null) {
      if (!mounted) return;
      modalManager.showLoadingModal();
      await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function: vm.createUploadOffer(
            job: widget.job!,
            signature: signature,
            jobsList: widget.job?.jobsList),
      ).whenComplete(
        () async {
          await tryCatchWrapper(
              context: myGlobals.homeScaffoldKey!.currentContext,
              function: Future.wait([
                clientVm.getClients(
                    tenderVm: tenderFolderVm, createOfferVm: createOfferVm),
                newsfeedVm.getLatestJobFeed(),
              ])).whenComplete(() async {
            modalManager.hideLoadingModal();

            if (vm.isOfferUploaded) {
              await successDialog(
                      context: myGlobals.homeScaffoldKey!.currentContext!,
                      successMessage: 'successfully_created_offer')
                  .whenComplete(() async {
                backDrawerRoute();
              });
            } else if (!vm.isOfferUploaded &&
                vm.isUploadOfferUpdate &&
                (vm.offerVersionList?.offerSeen ?? true) == true) {
              await failedDialog(
                      context: myGlobals.homeScaffoldKey!.currentContext!,
                      failedMessage: 'failed_v2')
                  .whenComplete(() async => backDrawerRoute());
            }
          });
        },
      );
    }
  }

  Widget step({required int step, required OfferViewModel vm}) {
    switch (step + 1) {
      case 1:
        return UploadOfferStepOne(
          offerVm: vm,
        );
      case 2:
        return UploadOfferStepTwo(
          offerVm: vm,
        );
      case 3:
        return UploadOfferStepThree(
          offerVm: vm,
        );
      case 4:
        return UploadOfferStepFour(
          offerVm: vm,
          scrollController: scrollController,
        );
      default:
        return UploadOfferStepOne(
          offerVm: vm,
        );
    }
  }
}
