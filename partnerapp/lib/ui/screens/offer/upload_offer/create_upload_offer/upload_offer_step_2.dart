import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class UploadOfferStepTwo extends StatefulWidget {
  const UploadOfferStepTwo({super.key, required this.offerVm});
  final OfferViewModel offerVm;

  @override
  UploadOfferStepTwoState createState() => UploadOfferStepTwoState();
}

class UploadOfferStepTwoState extends State<UploadOfferStepTwo> {
  double uploadOfferPrice = 0;
  final CurrencyTextInputFormatter _uploadOfferPriceFormatter =
      CurrencyTextInputFormatter(NumberFormat.currency(
    locale: 'da',
    decimalDigits: 2,
    symbol: '',
  ));

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if (widget.offerVm.uploadOfferForms.containsKey('uploadOfferPrice')) {
        setState(() {
          uploadOfferPrice =
              widget.offerVm.uploadOfferForms['uploadOfferPrice'];
        });
      }

      if (widget.offerVm.isUploadOfferUpdate) {
        final subtotalPrice =
            widget.offerVm.offerVersionList?.subTotalPrice.toString() ?? '';
        if (subtotalPrice.isNotEmpty) {
          if (uploadOfferPrice == 0) {
            setState(() {
              uploadOfferPrice = double.parse(subtotalPrice);
            });
          }
        }
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomTextFieldFormBuilder(
            validator: FormBuilderValidators.compose([
              (val) {
                if (val != null) {
                  if (val.isEmpty) {
                    return tr('required');
                  } else if (val == '0') {
                    return tr('price_cannot_be_lower_than_0');
                  } else if ((uploadOfferPrice + (uploadOfferPrice * .25)) <
                      1000) {
                    return tr('total_should_not_be_less_than_1000');
                  }
                }

                return null;
              }
            ]),
            name: 'uploadOfferPrice',
            labelText: tr('total_offer_amount_without_vat'),
            initialValue: widget.offerVm.isUploadOfferUpdate
                ? (widget.offerVm.offerVersionList?.subTotalPrice != null
                    ? _uploadOfferPriceFormatter.formatString(
                        '${(widget.offerVm.offerVersionList?.subTotalPrice ?? 0)}.00')
                    : '')
                : '',
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[_uploadOfferPriceFormatter],
            textAlign: TextAlign.end,
            onChanged: (p0) {
              if (p0 != null && p0.isNotEmpty) {
                setState(() {
                  uploadOfferPrice = _uploadOfferPriceFormatter
                      .getUnformattedValue()
                      .toDouble();
                });
              } else {
                setState(() {
                  uploadOfferPrice = 0;
                });
              }
            },
            suffixIcon: Container(
              padding: const EdgeInsets.fromLTRB(12, 12, 12, 12),
              child: Text('kr.',
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      fontSize: 18,
                      fontWeight: FontWeight.normal,
                      color: PartnerAppColors.darkBlue)),
            )),
        SmartGaps.gapH20,
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(tr('vat_value'),
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: PartnerAppColors.darkBlue,
                    fontSize: 16,
                    fontWeight: FontWeight.normal)),
            Text(
                '${NumberFormat.decimalPattern('da').format(uploadOfferPrice * .25)} kr.',
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: PartnerAppColors.darkBlue,
                    fontSize: 16,
                    fontWeight: FontWeight.normal))
          ],
        ),
        SmartGaps.gapH5,
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(tr('total_with_vat'),
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: PartnerAppColors.darkBlue,
                    fontSize: 16,
                    fontWeight: FontWeight.normal)),
            Text(
                '${NumberFormat.decimalPattern('da').format(uploadOfferPrice + (uploadOfferPrice * .25))} kr.',
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: PartnerAppColors.darkBlue,
                    fontSize: 16,
                    fontWeight: FontWeight.normal))
          ],
        ),
      ],
    );
  }
}
