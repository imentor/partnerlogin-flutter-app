import 'dart:io';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class UploadOfferStepFour extends StatefulWidget {
  const UploadOfferStepFour(
      {super.key, required this.offerVm, required this.scrollController});
  final OfferViewModel offerVm;
  final ScrollController scrollController;

  @override
  UploadOfferStepFourState createState() => UploadOfferStepFourState();
}

class UploadOfferStepFourState extends State<UploadOfferStepFour> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(tr('summary'), style: Theme.of(context).textTheme.headlineMedium),
        SmartGaps.gapH30,
        SafeArea(
          child: Container(
              width: MediaQuery.of(context).size.width,
              height: widget.offerVm.seeMore
                  ? MediaQuery.of(context).size.height / 2
                  : MediaQuery.of(context).size.height / 4,
              padding: const EdgeInsets.all(10),
              margin: const EdgeInsets.only(bottom: 30),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: PartnerAppColors.spanishGrey.withValues(alpha: .3)),
              child: Column(
                children: [
                  Flexible(
                    child: SfPdfViewer.file(
                      File(widget.offerVm.uploadOfferForms['fileToViewPath']),
                      pageLayoutMode: PdfPageLayoutMode.continuous,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      if (!widget.offerVm.seeMore) {
                        widget.scrollController.animateTo(
                          0.0,
                          duration: const Duration(milliseconds: 500),
                          curve: Curves.easeInOut,
                        );
                      }
                      widget.offerVm.seeMore = !widget.offerVm.seeMore;
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Center(
                        child: Text(
                          widget.offerVm.seeMore ? tr('hide') : tr('see_more'),
                          style: GoogleFonts.notoSans(
                            textStyle: const TextStyle(
                                color: PartnerAppColors.darkBlue,
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                height: 1.4),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )),
        ),
        Builder(builder: (context) {
          double uploadOfferPriceWithoutVat = 0;
          double vatValue = 0;
          double uploadOfferPriceWithVat = 0;

          if (widget.offerVm.uploadOfferForms['priceHasVat']) {
            uploadOfferPriceWithoutVat =
                widget.offerVm.uploadOfferForms['uploadOfferPrice'] -
                    (widget.offerVm.uploadOfferForms['uploadOfferPrice'] * .25);

            vatValue =
                widget.offerVm.uploadOfferForms['uploadOfferPrice'] * .25;

            uploadOfferPriceWithVat =
                widget.offerVm.uploadOfferForms['uploadOfferPrice'];
          } else {
            uploadOfferPriceWithoutVat =
                widget.offerVm.uploadOfferForms['uploadOfferPrice'];

            vatValue =
                widget.offerVm.uploadOfferForms['uploadOfferPrice'] * .25;

            uploadOfferPriceWithVat = uploadOfferPriceWithoutVat + vatValue;
          }

          return Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(tr('unit_price_ex_vat'),
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          color: PartnerAppColors.darkBlue,
                          fontSize: 16,
                          fontWeight: FontWeight.normal)),
                  Text(
                      '${NumberFormat.decimalPattern('da').format(uploadOfferPriceWithoutVat)} kr.',
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          color: PartnerAppColors.darkBlue,
                          fontSize: 16,
                          fontWeight: FontWeight.normal))
                ],
              ),
              SmartGaps.gapH10,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(tr('vat_value'),
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          color: PartnerAppColors.darkBlue,
                          fontSize: 16,
                          fontWeight: FontWeight.normal)),
                  Text(
                      '${NumberFormat.decimalPattern('da').format(vatValue)} kr.',
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          color: PartnerAppColors.darkBlue,
                          fontSize: 16,
                          fontWeight: FontWeight.normal))
                ],
              ),
              SmartGaps.gapH5,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(tr('total_with_vat'),
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          color: PartnerAppColors.darkBlue,
                          fontSize: 16,
                          fontWeight: FontWeight.normal)),
                  Text(
                      '${NumberFormat.decimalPattern('da').format(uploadOfferPriceWithVat)} kr.',
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          color: PartnerAppColors.darkBlue,
                          fontSize: 16,
                          fontWeight: FontWeight.normal))
                ],
              ),
            ],
          );
        }),
        SmartGaps.gapH10,
        const Divider(color: PartnerAppColors.spanishGrey),
        SmartGaps.gapH10,
        Text(tr('project_day_estimate'),
            style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                color: PartnerAppColors.darkBlue,
                fontSize: 16,
                fontWeight: FontWeight.normal)),
        SmartGaps.gapH5,
        RichText(
            text: TextSpan(children: [
          const WidgetSpan(
              child: Icon(Icons.event, color: PartnerAppColors.blue),
              alignment: PlaceholderAlignment.middle),
          const WidgetSpan(
              child: SizedBox(
            width: 10,
          )),
          TextSpan(
              text:
                  '${widget.offerVm.uploadOfferForms['uploadOfferProjectEstimate']} ${tr('days')}',
              style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                  color: PartnerAppColors.darkBlue,
                  fontSize: 16,
                  fontWeight: FontWeight.bold)),
        ])),
        SmartGaps.gapH20,
        Text(tr('date_to_start_project'),
            style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                color: PartnerAppColors.darkBlue,
                fontSize: 16,
                fontWeight: FontWeight.normal)),
        SmartGaps.gapH5,
        RichText(
            text: TextSpan(children: [
          const WidgetSpan(
              child: Icon(Icons.event, color: PartnerAppColors.blue),
              alignment: PlaceholderAlignment.middle),
          const WidgetSpan(
              child: SizedBox(
            width: 10,
          )),
          TextSpan(
              text: Formatter.formatDateStrings(
                  type: DateFormatType.standardDate,
                  dateString: (widget.offerVm
                          .uploadOfferForms['uploadOfferProjectStartDate']
                      as String)),
              style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                  color: PartnerAppColors.darkBlue,
                  fontSize: 16,
                  fontWeight: FontWeight.bold)),
        ])),
        SmartGaps.gapH20,
        RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: '${tr('your_offer_is_valid')} ',
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.normal,
                    fontSize: 18),
              ),
              TextSpan(
                text:
                    '${widget.offerVm.uploadOfferForms['uploadOfferSignatureValid']} ${tr('days')}',
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              ),
              TextSpan(
                text: ' ${tr('days_valid')}',
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    letterSpacing: 1.0,
                    color: PartnerAppColors.darkBlue,
                    fontWeight: FontWeight.normal,
                    fontSize: 18),
              )
            ],
          ),
        ),
      ],
    );
  }
}
