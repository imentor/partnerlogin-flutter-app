import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/full_url_viewer.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

class UpdateUploadedOfferPage extends StatefulWidget {
  const UpdateUploadedOfferPage({super.key, this.job});

  final PartnerJobModel? job;

  @override
  UpdateUploadedOfferPageState createState() => UpdateUploadedOfferPageState();
}

class UpdateUploadedOfferPageState extends State<UpdateUploadedOfferPage> {
  double uploadOfferPrice = 0;
  final CurrencyTextInputFormatter _uploadOfferPriceFormatter =
      CurrencyTextInputFormatter(NumberFormat.currency(
    locale: 'da',
    decimalDigits: 2,
    symbol: '',
  ));

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final vm = context.read<OfferViewModel>();
      await tryCatchWrapper(
              context: myGlobals.homeScaffoldKey!.currentContext,
              function:
                  Future.value(vm.getOfferById(offerId: widget.job!.offerId!)))
          .whenComplete(() {
        setState(() {
          uploadOfferPrice = vm.viewedOffer?.subtotalPrice?.toDouble() ?? 0.0;
        });
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    APPLIC.cancelToken.cancel('cancel requests');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      bottomNavigationBar: Consumer<OfferViewModel>(builder: (_, vm, __) {
        return Container(
          padding:
              const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
          color: Colors.white,
          child: TextButton(
              style: TextButton.styleFrom(
                  fixedSize: Size(MediaQuery.of(context).size.width, 45),
                  backgroundColor:
                      (vm.viewedOffer?.subtotalPrice?.toDouble() ?? 0) !=
                              uploadOfferPrice
                          ? PartnerAppColors.darkBlue
                          : PartnerAppColors.spanishGrey,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  )),
              onPressed: () async {
                if ((vm.viewedOffer?.subtotalPrice?.toDouble() ?? 0) !=
                    uploadOfferPrice) {
                  modalManager.showLoadingModal();

                  await tryCatchWrapper(
                          context: myGlobals.homeScaffoldKey!.currentContext,
                          function:
                              vm.updateOfferPrice(price: uploadOfferPrice))
                      .whenComplete(() => modalManager.hideLoadingModal());
                }
              },
              child: Text(
                tr('save'),
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium!
                    .copyWith(color: Colors.white),
              )),
        );
      }),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: FormBuilder(
          child: Consumer<OfferViewModel>(builder: (_, vm, __) {
            if (vm.busy) return const Center(child: ConnectivityLoader());

            return Column(
              children: [
                if (vm.viewedOffer == null) ...[
                  Align(
                    alignment: Alignment.center,
                    child: Text(tr('error'),
                        style: Theme.of(context).textTheme.headlineMedium),
                  )
                ] else ...[
                  ListTile(
                    contentPadding: EdgeInsets.zero,
                    leading: SvgPicture.asset(
                      SvgIcons.pdfFileFormat,
                      width: 40,
                      height: 40,
                    ),
                    trailing: PopupMenuButton(
                      itemBuilder: (BuildContext context) {
                        return [
                          PopupMenuItem(
                            value: 1,
                            child: Row(
                              children: [
                                const Icon(FeatherIcons.eye),
                                SmartGaps.gapW10,
                                Text(tr('view_file'))
                              ],
                            ),
                          ),
                          PopupMenuItem(
                            value: 2,
                            child: Row(
                              children: [
                                const Icon(FeatherIcons.download),
                                SmartGaps.gapW10,
                                Text(tr('download_file'))
                              ],
                            ),
                          )
                        ];
                      },
                      child: const Icon(
                        FeatherIcons.moreVertical,
                        color: PartnerAppColors.darkBlue,
                      ),
                      onSelected: (value) async {
                        switch (value) {
                          case 1:
                            showUrlViewer(
                                context, vm.viewedOffer?.offerFile ?? '',
                                isPdf: true,
                                pdfUrl: vm.viewedOffer?.offerFile ?? '');
                            break;
                          case 2:
                            Navigator.of(context).pop();
                            modalManager.showLoadingModal(
                                message: 'Downloading file...');

                            await tryCatchWrapper(
                                    context: myGlobals
                                        .homeScaffoldKey!.currentContext,
                                    function: vm.downloadFile(
                                        name:
                                            'Offer: ${vm.viewedOffer?.projectId ?? 0}',
                                        url: vm.viewedOffer?.offerFile ?? ''))
                                .then((value) async {
                              modalManager.hideLoadingModal();
                              if (value != null) {
                                await showOkAlertDialog(
                                    context: myGlobals
                                        .homeScaffoldKey!.currentContext!,
                                    message: value
                                        ? tr('file_downloaded')
                                        : tr('error_occurred'),
                                    okLabel: 'OK');
                              }
                            });
                            break;
                          default:
                        }
                      },
                    ),
                  ),
                  // SmartGaps.gapH20,
                  // TextButton(
                  //     style: TextButton.styleFrom(
                  //         fixedSize:
                  //             Size(MediaQuery.of(context).size.width, 45),
                  //         shape: RoundedRectangleBorder(
                  //             borderRadius: BorderRadius.circular(5),
                  //             side: const BorderSide(
                  //                 color: PartnerAppColors.darkBlue))),
                  //     onPressed: () {
                  //       addOfferFileDialog(
                  //           context: context,
                  //           tenderId: widget.job?.parentProject
                  //                   ?.projectTenderFolderId ??
                  //               0);
                  //     },
                  //     child: Text(
                  //       tr('add_more'),
                  //       style: Theme.of(context)
                  //           .textTheme
                  //           .headlineMedium!
                  //           .copyWith(color: PartnerAppColors.darkBlue),
                  //     )),
                  SmartGaps.gapH30,
                  CustomTextFieldFormBuilder(
                      validator: FormBuilderValidators.required(
                          errorText: tr('required')),
                      name: 'uploadOfferPrice',
                      labelText: tr('enterprise_sum'),
                      initialValue:
                          vm.viewedOffer?.subtotalPrice.toString() ?? '0',
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        _uploadOfferPriceFormatter
                      ],
                      textAlign: TextAlign.end,
                      onChanged: (p0) {
                        if (p0 != null && p0.isNotEmpty) {
                          setState(() {
                            uploadOfferPrice = _uploadOfferPriceFormatter
                                .getUnformattedValue()
                                .toDouble();
                          });
                        } else {
                          setState(() {
                            uploadOfferPrice = 0;
                          });
                        }
                      },
                      suffixIcon: Container(
                        padding: const EdgeInsets.fromLTRB(12, 12, 12, 12),
                        child: Text('kr.',
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium!
                                .copyWith(
                                    fontSize: 18,
                                    fontWeight: FontWeight.normal,
                                    color: PartnerAppColors.darkBlue)),
                      )),
                  SmartGaps.gapH20,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(tr('vat_value'),
                          style: Theme.of(context)
                              .textTheme
                              .bodyMedium!
                              .copyWith(
                                  color: PartnerAppColors.darkBlue,
                                  fontSize: 16,
                                  fontWeight: FontWeight.normal)),
                      Text(
                          '${NumberFormat.decimalPattern('da').format(uploadOfferPrice * .25)} kr.',
                          style: Theme.of(context)
                              .textTheme
                              .bodyMedium!
                              .copyWith(
                                  color: PartnerAppColors.darkBlue,
                                  fontSize: 16,
                                  fontWeight: FontWeight.normal))
                    ],
                  ),
                  SmartGaps.gapH5,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(tr('total_with_vat'),
                          style: Theme.of(context)
                              .textTheme
                              .bodyMedium!
                              .copyWith(
                                  color: PartnerAppColors.darkBlue,
                                  fontSize: 16,
                                  fontWeight: FontWeight.normal)),
                      Text(
                          '${NumberFormat.decimalPattern('da').format(uploadOfferPrice + (uploadOfferPrice * .25))} kr.',
                          style: Theme.of(context)
                              .textTheme
                              .bodyMedium!
                              .copyWith(
                                  color: PartnerAppColors.darkBlue,
                                  fontSize: 16,
                                  fontWeight: FontWeight.normal))
                    ],
                  ),
                ]
              ],
            );
          }),
        ),
      ),
    );
  }
}
