import 'package:Haandvaerker.dk/model/messages/message_v2_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/cache_image.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/ui/components/number_pagination.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/messages/message_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:timeago_flutter/timeago_flutter.dart';

class MessageScreenV2 extends StatefulWidget {
  const MessageScreenV2({
    super.key,
    this.projectId,
  });

  final int? projectId;

  @override
  State<MessageScreenV2> createState() => _MessageScreenV2State();
}

class _MessageScreenV2State extends State<MessageScreenV2>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  Future<void> getMessages() async {
    final messageVm = context.read<MessageV2ViewModel>();

    if (widget.projectId != null) {
      await tryCatchWrapper(
          context: myGlobals.homeScaffoldKey!.currentContext,
          function: messageVm.getBothMessage(
              page: 1, isLoadMore: false, projectId: widget.projectId));
    } else {
      if (messageVm.filteredInbox.isEmpty || messageVm.filteredSent.isEmpty) {
        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: messageVm.getBothMessage(page: 1, isLoadMore: false));
      }
    }
  }

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this, initialIndex: 0);

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final appDrawerVm = context.read<AppDrawerViewModel>();
      final newsFeedVm = context.read<NewsFeedViewModel>();
      const dynamicStoryPage = 'indbakke';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      getMessages();
      if (dynamicStoryEnable) {
        await Future.value(newsFeedVm.getDynamicStory(page: dynamicStoryPage))
            .whenComplete(() {
          if (!mounted) return;

          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Colors.white,
        icon: const Icon(Icons.message_outlined, color: PartnerAppColors.blue),
        onPressed: () {
          changeDrawerRoute(Routes.createProjectMessage);
        },
        label: Text(
          tr('write'),
          style: const TextStyle(color: PartnerAppColors.blue),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Consumer<MessageV2ViewModel>(builder: (_, vm, __) {
          return Column(
            children: [
              TabBar(
                indicatorColor: PartnerAppColors.blue,
                controller: _tabController,
                tabs: [
                  Tab(
                    child: Text(
                      tr('inbox'),
                      style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                          ),
                    ),
                  ),
                  Tab(
                    child: Text(
                      tr('sent'),
                      style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                          ),
                    ),
                  )
                ],
              ),
              SmartGaps.gapH20,
              CustomTextFieldFormBuilder(
                name: 'search',
                suffixIcon: const Icon(Icons.search),
                onChanged: (p0) {
                  vm.searchMessage(searchKey: (p0 ?? ''));
                },
              ),
              Expanded(
                  child: TabBarView(
                controller: _tabController,
                children: [_inbox(vm), _sent(vm)],
              ))
            ],
          );
        }),
      ),
    );
  }

  Widget _inbox(MessageV2ViewModel vm) {
    if (vm.busy) {
      return SizedBox(
        child: Skeletonizer(
          enabled: true,
          child: SingleChildScrollView(
            child: Column(
              children: [
                SmartGaps.gapH12,
                ...List.generate(
                    10,
                    (index) => messageCard(
                        message: MessageV2Items(),
                        vm: vm,
                        index: index,
                        isFakeData: true))
              ],
            ),
          ),
        ),
      );
    }

    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
              itemCount: vm.filteredInbox.length,
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return messageCard(
                    message: vm.filteredInbox[index], vm: vm, index: index);
              }),
          NumberPagination(
            currentPage: vm.inbox?.currentPage ?? 0,
            totalPages: vm.inbox?.lastPage ?? 0,
            onPageChanged: (p0) async {
              if (widget.projectId != null) {
                await vm.getBothMessage(
                    page: p0,
                    isLoadMore: true,
                    projectId: widget.projectId,
                    from: 'inbox');
              } else {
                await vm.getBothMessage(
                    page: p0, isLoadMore: true, from: 'inbox');
              }
            },
          ),
          SmartGaps.gapH40,
        ],
      ),
    );
  }

  Widget _sent(MessageV2ViewModel vm) {
    if (vm.busy) {
      return SizedBox(
        child: Skeletonizer(
          enabled: true,
          child: SingleChildScrollView(
            child: Column(
              children: [
                SmartGaps.gapH12,
                ...List.generate(
                    10,
                    (index) => messageCard(
                        message: MessageV2Items(),
                        vm: vm,
                        index: index,
                        isFakeData: true))
              ],
            ),
          ),
        ),
      );
    }

    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
              itemCount: vm.filteredSent.length,
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return messageCard(
                    message: vm.filteredSent[index], vm: vm, index: index);
              }),
          NumberPagination(
            currentPage: vm.sent?.currentPage ?? 0,
            totalPages: vm.sent?.lastPage ?? 0,
            onPageChanged: (p0) async {
              if (widget.projectId != null) {
                await vm.getBothMessage(
                    page: p0,
                    isLoadMore: true,
                    projectId: widget.projectId,
                    from: 'sent');
              } else {
                await vm.getBothMessage(
                    page: p0, isLoadMore: true, from: 'sent');
              }
            },
          ),
          SmartGaps.gapH40,
        ],
      ),
    );
  }

  Widget messageCard(
      {required MessageV2Items message,
      required MessageV2ViewModel vm,
      required int index,
      bool isFakeData = false}) {
    if (isFakeData) {
      return Card(
        elevation: 2,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const CircleAvatar(
                radius: 28.0,
                child: ClipOval(),
              ),
              SmartGaps.gapW10,
              Column(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Fake message name',
                        style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                              color: Theme.of(context).colorScheme.primary,
                              fontWeight: FontWeight.bold,
                              height: 1,
                            ),
                      ),
                      SmartGaps.gapH10,
                      Text(
                        'Fake message title',
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                              color: message.seen == 0 &&
                                      (message.isFromPartner ?? false) == true
                                  ? Colors.grey
                                  : Colors.grey,
                              fontWeight: FontWeight.normal,
                              height: 1,
                            ),
                      ),
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      );
    } else {
      return InkWell(
        onTap: () {
          changeDrawerRoute(Routes.messageView, arguments: message);
        },
        child: Card(
          elevation: 2,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(
                  radius: 28.0,
                  child: ClipOval(
                    child: (message.partner?.avatar ?? '').isNotEmpty ||
                            (message.partner?.avatar ?? '').isNotEmpty
                        ? CacheImage(
                            imageUrl: (message.isFromPartner ?? false)
                                ? (message.partner?.avatar ?? '')
                                : (message.contact?.avatar ?? ''),
                          )
                        : null,
                  ),
                ),
                SmartGaps.gapW10,
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              (message.isFromPartner ?? false)
                                  ? message.partner?.name?.trim() ?? ''
                                  : message.contact?.name?.trim() ?? '',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyLarge!
                                  .copyWith(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                    fontWeight: FontWeight.bold,
                                    height: 1,
                                  ),
                            ),
                            SmartGaps.gapH10,
                            Text(
                              (message.title ?? '').trim(),
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyLarge!
                                  .copyWith(
                                    color: message.seen == 0 &&
                                            (message.isFromPartner ?? false)
                                        ? Colors.grey
                                        : Colors.grey,
                                    fontWeight: FontWeight.normal,
                                    height: 1,
                                  ),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Timeago(
                            date: (message.sendDate != null &&
                                    (message.sendDate ?? '').isNotEmpty)
                                ? DateTime.parse(message.sendDate ?? '')
                                : DateTime.now(),
                            locale: context.locale.languageCode == 'da'
                                ? 'da'
                                : 'en',
                            builder: (context, value) => Text(
                              value,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .copyWith(
                                      fontWeight: FontWeight.normal,
                                      color: Colors.grey,
                                      fontSize: 12.0),
                            ),
                          ),
                          SmartGaps.gapH10,
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      '${tr('thread')}: ${message.threadCount} ',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyMedium!
                                      .copyWith(
                                          fontWeight: FontWeight.normal,
                                          color: Colors.grey,
                                          fontSize: 12.0),
                                ),
                                WidgetSpan(
                                  alignment: PlaceholderAlignment.middle,
                                  child: Icon(
                                    message.seen == 0
                                        ? Icons.mark_email_unread
                                        : Icons.mark_email_read,
                                    color: message.seen == 0
                                        ? PartnerAppColors.blue
                                        : Colors.grey,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
  }
}
