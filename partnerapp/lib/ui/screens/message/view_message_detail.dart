import 'package:Haandvaerker.dk/model/messages/message_v2_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/cache_image.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/messages/components.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/messages/message_v2_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:timeago_flutter/timeago_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';

Future<bool?> deleteMessageDialog({required BuildContext context}) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          InkWell(
            onTap: () {
              Navigator.of(dialogContext).pop();
            },
            child: const Icon(
              FeatherIcons.x,
              color: PartnerAppColors.spanishGrey,
            ),
          )
        ]),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(children: [
              const Icon(
                FeatherIcons.trash2,
                color: PartnerAppColors.red,
                size: 30,
              ),
              SmartGaps.gapH30,
              Text(
                tr('delete_message_confirmation'),
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                      fontSize: 24,
                      fontWeight: FontWeight.w700,
                    ),
              ),
              SmartGaps.gapH30,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  CustomDesignTheme.flatButtonStyle(
                    height: 50,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                        side:
                            const BorderSide(color: PartnerAppColors.darkBlue)),
                    onPressed: () {
                      Navigator.of(dialogContext).pop(false);
                    },
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            tr('no'),
                            style: Theme.of(context)
                                .textTheme
                                .displayLarge!
                                .copyWith(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400,
                                  color: PartnerAppColors.darkBlue,
                                ),
                          )
                        ]),
                  ),
                  CustomDesignTheme.flatButtonStyle(
                    height: 50,
                    backgroundColor: PartnerAppColors.darkBlue,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    onPressed: () {
                      Navigator.of(dialogContext).pop(true);
                    },
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            tr('yes'),
                            style: Theme.of(context)
                                .textTheme
                                .displayLarge!
                                .copyWith(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white,
                                ),
                          )
                        ]),
                  ),
                ],
              )
            ]),
          ),
        ),
      );
    },
  );
}

class ViewMessageDetail extends StatefulWidget {
  const ViewMessageDetail({super.key, required this.message});
  final MessageV2Items message;

  @override
  State<ViewMessageDetail> createState() => _ViewMessageDetailState();
}

class _ViewMessageDetailState extends State<ViewMessageDetail> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final messageVm = context.read<MessageV2ViewModel>();

      await tryCatchWrapper(
          context: context,
          function: messageVm.getMessageThreads(message: widget.message));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DrawerAppBar(actions: [
        InkWell(
          onTap: () async {
            final vm = context.read<MessageV2ViewModel>();

            await deleteMessageDialog(context: context).then((value) async {
              if ((value ?? false)) {
                modalManager.showLoadingModal();

                await tryCatchWrapper(
                        context: myGlobals.homeScaffoldKey!.currentContext!,
                        function:
                            vm.deleteMessage(messageId: widget.message.id ?? 0))
                    .then((value) async {
                  await vm.getBothMessage(page: 1, isLoadMore: false);

                  if ((value ?? false)) {
                    backDrawerRoute();
                  }
                }).whenComplete(() => modalManager.hideLoadingModal());
              }
            });
          },
          child: const Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Icon(FeatherIcons.trash2)),
        )
      ]),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Text(
                    widget.message.title ?? '',
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          color: PartnerAppColors.blue,
                          fontSize: 20,
                          height: 1,
                        ),
                  ),
                ),
                if (widget.message.project != null) ...[
                  SmartGaps.gapW10,
                  Flexible(
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: PartnerAppColors.blue,
                      ),
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        widget.message.project?.name ?? '',
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context)
                            .textTheme
                            .titleSmall!
                            .copyWith(fontSize: 12, color: Colors.white),
                      ),
                    ),
                  ),
                ]
              ],
            ),
            SmartGaps.gapH10,
            Consumer<MessageV2ViewModel>(builder: (_, vm, __) {
              if (vm.busy) {
                return Skeletonizer(
                    enabled: true,
                    child: Column(
                      children: [
                        ListTile(
                          contentPadding: EdgeInsets.zero,
                          leading: const CircleAvatar(
                            radius: 24.0,
                            child: ClipOval(),
                          ),
                          title: Text(
                            'Title message',
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium!
                                .copyWith(
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.bold,
                                ),
                          ),
                          subtitle: Text(
                            'Subtitle message',
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium!
                                .copyWith(
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.bold,
                                ),
                          ),
                          trailing: Text(
                            'Timeago',
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium!
                                .copyWith(
                                  color: PartnerAppColors.darkBlue,
                                  fontWeight: FontWeight.bold,
                                ),
                          ),
                        )
                      ],
                    ));
              }

              return Column(
                children: [
                  ...vm.currentMessageThread.map((message) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: ExpansionWidget(
                        initiallyExpanded: vm.currentMessageThread.isNotEmpty
                            ? message.id == vm.currentMessageThread.last.id
                            : false,
                        titleBuilder: (animationValue, easeInValue, isExpanded,
                            toggleFunction) {
                          return ListTile(
                            onTap: () => toggleFunction(animated: true),
                            contentPadding: EdgeInsets.zero,
                            leading: CircleAvatar(
                              radius: 24.0,
                              child: ClipOval(
                                child: CacheImage(
                                    imageUrl: (message.isFromPartner ?? false)
                                        ? message.partner?.avatar ?? ''
                                        : message.contact?.avatar ?? ''),
                              ),
                            ),
                            title: Row(
                              children: [
                                if ((message.isFromPartner ?? false))
                                  Expanded(
                                    child: Text(
                                      message.partner?.name ?? '',
                                      maxLines: !isExpanded ? 2 : null,
                                      overflow: !isExpanded
                                          ? TextOverflow.ellipsis
                                          : null,
                                      style: Theme.of(context)
                                          .textTheme
                                          .headlineMedium!
                                          .copyWith(
                                            color: PartnerAppColors.darkBlue,
                                            fontWeight: FontWeight.bold,
                                          ),
                                    ),
                                  ),
                                if (!(message.isFromPartner ?? true))
                                  Expanded(
                                    child: Text(
                                      message.contact?.name ?? '',
                                      maxLines: !isExpanded ? 2 : null,
                                      overflow: !isExpanded
                                          ? TextOverflow.ellipsis
                                          : null,
                                      style: Theme.of(context)
                                          .textTheme
                                          .headlineMedium!
                                          .copyWith(
                                            color: PartnerAppColors.darkBlue,
                                            fontWeight: FontWeight.bold,
                                          ),
                                    ),
                                  ),
                                SmartGaps.gapW10,
                                if (!isExpanded)
                                  Expanded(
                                    child: Text(
                                      Bidi.stripHtmlIfNeeded(
                                          message.message ?? ''),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: Theme.of(context)
                                          .textTheme
                                          .headlineSmall!
                                          .copyWith(
                                            color: PartnerAppColors.darkBlue,
                                            fontWeight: FontWeight.normal,
                                          ),
                                    ),
                                  )
                              ],
                            ),
                            trailing: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  Formatter.formatDateStrings(
                                      type: DateFormatType.standardDate,
                                      dateString: message.sendDate),
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyMedium!
                                      .copyWith(
                                          color: PartnerAppColors.darkBlue,
                                          fontSize: 14.0),
                                ),
                                Timeago(
                                  date: DateTime.parse(message.sendDate ?? ''),
                                  locale: context.locale.languageCode == 'da'
                                      ? 'da_short'
                                      : 'en_short',
                                  builder: (context, value) => Text(
                                    value,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyMedium!
                                        .copyWith(
                                            color: PartnerAppColors.darkBlue,
                                            fontSize: 14.0),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        content: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SmartGaps.gapH20,
                            if ((message.message ?? '')
                                .contains('<!doctype html>'))
                              SizedBox(
                                width: MediaQuery.of(context).size.width,
                                height: MediaQuery.of(context).size.height / 2,
                                child: InAppWebView(
                                  initialSettings: InAppWebViewSettings(
                                    javaScriptEnabled: true,
                                    useShouldOverrideUrlLoading: true,
                                    javaScriptCanOpenWindowsAutomatically: true,
                                    cacheEnabled: false,
                                    cacheMode: CacheMode.LOAD_NO_CACHE,
                                    allowsBackForwardNavigationGestures: true,
                                    isInspectable: true,
                                    supportZoom: false,
                                  ),
                                  onCreateWindow: _onCreateWindow,
                                  onWebViewCreated: _onWebViewCreated,
                                  initialData: InAppWebViewInitialData(
                                    data: message.message ?? '',
                                  ),
                                  shouldOverrideUrlLoading:
                                      _shouldOverrideUrlLoading,
                                ),
                              )
                            else
                              Html(
                                onLinkTap: (url, attributes, element) =>
                                    launchUrlString(
                                  url!,
                                ),
                                shrinkWrap: true,
                                data: message.message ?? '',
                                style: {
                                  '*': Style(
                                    color: PartnerAppColors.darkBlue,
                                    lineHeight: LineHeight.number(1.3),
                                  )
                                },
                              ),
                            if ((message.files ?? []).isNotEmpty) ...[
                              SmartGaps.gapH20,
                              Text(
                                '${tr('attachments')} :',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyLarge!
                                    .copyWith(fontSize: 15),
                              ),
                              SmartGaps.gapH10,
                              SizedBox(
                                height: 50,
                                child: SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                      children: [
                                        ...(message.files ?? [])
                                            .map((messageFiles) {
                                          return InkWell(
                                            onTap: () async {
                                              await launchUrl(
                                                  Uri.parse(messageFiles),
                                                  mode: LaunchMode
                                                      .externalApplication);
                                            },
                                            child: Container(
                                              height: 50,
                                              width: 50,
                                              decoration: BoxDecoration(
                                                  border: Border.all(
                                                      color: PartnerAppColors
                                                          .blue),
                                                  borderRadius:
                                                      BorderRadius.circular(5)),
                                              child: const Icon(
                                                FeatherIcons.paperclip,
                                                color: PartnerAppColors.blue,
                                              ),
                                            ),
                                          );
                                        })
                                      ],
                                    )),
                              )
                            ],
                            if (message.actions != null) ...[
                              SmartGaps.gapH20,
                              Wrap(
                                spacing: 20.0,
                                runSpacing: 20.0,
                                alignment: WrapAlignment.center,
                                children: [
                                  ...(message.actions?.items ?? [])
                                      .where((element) =>
                                          !(element.isDone ?? true))
                                      .map(
                                        (e) => SizedBox(
                                          width: 200.0,
                                          height: 80,
                                          child: CustomButton(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 16.0,
                                                horizontal: 20.0),
                                            text: tr(e.action ?? ''),
                                            fontWeight: FontWeight.normal,
                                            textSize: 18.0,
                                            onPressed: () async {
                                              actionButton(
                                                  messageV2Vm: vm,
                                                  action: e,
                                                  message: message);
                                              // backInnerRoute(context);
                                              // messageAction(
                                              //     messageAction: e,
                                              //     message: message);
                                            },
                                          ),
                                        ),
                                      ),
                                ],
                              )
                            ]
                          ],
                        ),
                        expandedAlignment: Alignment.centerLeft,
                      ),
                    );
                  }),
                  SmartGaps.gapH10,
                  Row(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 20),
                          child: CustomBorderButton(
                            text: tr('reply'),
                            fontWeight: FontWeight.normal,
                            textSize: 18.0,
                            textColor: PartnerAppColors.blue,
                            backgroundColor: Colors.transparent,
                            icon: SvgPicture.asset(SvgIcons.replyArrowIcon,
                                colorFilter: const ColorFilter.mode(
                                    PartnerAppColors.blue, BlendMode.srcIn),
                                height: 16.0),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10.0, vertical: 18.0),
                            onPressed: () {
                              changeDrawerRoute(
                                Routes.createProjectMessage,
                                arguments: {
                                  'partnerId': widget.message.partner?.id ?? 0,
                                  'subject':
                                      'Re: ${vm.currentMessageThread.isNotEmpty ? (vm.currentMessageThread.last.title ?? '') : ''}',
                                  'message': '',
                                  'threadId': widget.message.threadId ?? 0,
                                  'customerId': widget.message.contact?.id ?? 0,
                                },
                              );
                            },
                          ),
                        ),
                      ),
                      Expanded(
                        child: CustomBorderButton(
                          text: tr('forward'),
                          fontWeight: FontWeight.normal,
                          textSize: 18.0,
                          textColor: PartnerAppColors.blue,
                          backgroundColor: Colors.transparent,
                          icon: SvgPicture.asset(SvgIcons.forwardArrowIcon,
                              colorFilter: const ColorFilter.mode(
                                  PartnerAppColors.blue, BlendMode.srcIn),
                              height: 16.0),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 18.0),
                          onPressed: () {
                            changeDrawerRoute(
                              Routes.createProjectMessage,
                              arguments: {
                                'partnerId': null,
                                'subject':
                                    'Fwd: ${vm.currentMessageThread.isNotEmpty ? (vm.currentMessageThread.last.title ?? '') : ''}',
                                'message':
                                    ' <br><br>Fwd: <br>${vm.currentMessageThread.isNotEmpty ? (vm.currentMessageThread.first.message ?? '') : ''}',
                              },
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              );
            }),
          ],
        ),
      ),
    );
  }

  void _onWebViewCreated(InAppWebViewController controller) {}

  Future<bool?> _onCreateWindow(
    InAppWebViewController controller,
    CreateWindowAction createWindowAction,
  ) async {
    return false;
  }

  Future<NavigationActionPolicy?> _shouldOverrideUrlLoading(
    InAppWebViewController controller,
    NavigationAction navigationAction,
  ) async {
    return NavigationActionPolicy.ALLOW;
  }

  actionButton(
      {required MessageV2ViewModel messageV2Vm,
      required MessageV2ActionItem action,
      required MessageV2Items message}) async {
    if (action.type == 'link') {
      switch (action.action) {
        case 'view_deficiency':
          if (message.projectId != null) {
            Navigator.of(context).pop();
            changeDrawerRoute(
              Routes.projectDashboard,
              arguments: {
                'projectId': message.projectId!,
                'pageRedirect': 'deficiency'
              },
            );
          }
          break;
        case 'edit_contract':
          if (message.projectId != null) {
            Navigator.of(context).pop();
            changeDrawerRoute(
              Routes.projectDashboard,
              arguments: {
                'projectId': message.projectId!,
                'pageRedirect': 'contract'
              },
            );
          }
          break;
        case 'rate_partner':
        case 'contract_document_url':
          if ((action.data ?? '').isNotEmpty) {
            Navigator.of(context).pop();
            changeDrawerRoute(Routes.inAppWebView,
                arguments: Uri.parse(action.data!));
          }
          break;
        default:
          break;
      }
    } else if (action.type == 'api') {
      if ((action.data ?? '').isNotEmpty) {
        modalManager.showLoadingModal();

        await messageV2Vm
            .postMessageAction(messageActionUrl: action.data)
            .then((value) async {
          if (!mounted) return;

          modalManager.hideLoadingModal();
          Navigator.of(context).pop();
        });
      }
    }
  }
}
