import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/integration/integration_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class IntegrationScreen extends StatefulWidget {
  const IntegrationScreen({super.key});

  @override
  IntegrationScreenState createState() => IntegrationScreenState();
}

class IntegrationScreenState extends State<IntegrationScreen> {
  late TextEditingController _apiKeyController;
  late FocusNode _apiKeyFocus;
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    _apiKeyController = TextEditingController();
    _apiKeyFocus = FocusNode();
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      final vm = context.read<IntegrationViewModel>();
      final newsFeedVm = context.read<NewsFeedViewModel>();
      final appDrawerVm = context.read<AppDrawerViewModel>();
      const dynamicStoryPage = 'ordrestyring';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      if (dynamicStoryEnable) {
        await tryCatchWrapper(
          context: myGlobals.homeScaffoldKey!.currentContext,
          function: Future.wait([
            vm.getOrderManagement(),
            newsFeedVm.getDynamicStory(page: dynamicStoryPage),
          ]),
        ).whenComplete(() {
          if (!mounted) return;

          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      } else {
        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: Future.value(vm.getOrderManagement()));
      }
    });
  }

  @override
  void dispose() {
    _apiKeyController.dispose();
    _apiKeyFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Container(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(tr('integrations'),
                  //tr('membership_benefits'),
                  style: Theme.of(context).textTheme.headlineLarge),
              SmartGaps.gapH20,
              Row(
                children: [
                  Image.asset(ImagePaths.ordrestyringLogo,
                      height: 100, width: 100),
                  SmartGaps.gapW10,
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Ordrestyring.dk',
                          //tr('membership_benefits'),
                          style: Theme.of(context)
                              .textTheme
                              .labelMedium!
                              .apply(color: Colors.black),
                        ),
                        Text(
                          tr('ordrestyring_desc'),
                          style: Theme.of(context)
                              .textTheme
                              .bodyMedium!
                              .copyWith(
                                  fontWeight: FontWeight.normal,
                                  color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SmartGaps.gapH20,
              Consumer<IntegrationViewModel>(
                builder: (context, vm, widget) {
                  if (vm.busy) {
                    return Center(child: loader());
                  }

                  if (vm.currentOrderManagement != null) {
                    return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('API',
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(color: Colors.black)),
                              Text(vm.currentOrderManagement!.apiKey!,
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleSmall!
                                      .copyWith(
                                          color: Colors.black,
                                          fontWeight: FontWeight.normal)),
                            ],
                          ),
                          Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(tr('date_modified'),
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleMedium!
                                        .copyWith(color: Colors.black)),
                                Text(vm.currentOrderManagement!.dateCreated!,
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleSmall!
                                        .copyWith(
                                            color: Colors.black,
                                            fontWeight: FontWeight.normal)),
                              ]),
                        ]);
                  }

                  return Container();
                },
              ),
              SmartGaps.gapH20,
              Text(
                'API Key:',
                //tr('membership_benefits'),
                style: Theme.of(context)
                    .textTheme
                    .titleMedium!
                    .apply(color: Colors.black),
              ),
              SmartGaps.gapH5,
              _getApiForm(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _getApiForm() {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFormField(
              controller: _apiKeyController,
              focusNode: _apiKeyFocus,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  filled: true,
                  fillColor: Colors.grey[200],
                  hintStyle: Theme.of(context)
                      .textTheme
                      .bodyMedium!
                      .copyWith(fontWeight: FontWeight.w500),
                  hintText: ''),
              validator: (value) {
                if (value!.isEmpty) {
                  return tr('please_enter_some_text');
                }
                return null;
              }),
          SmartGaps.gapH20,
          SizedBox(
            width: 150,
            height: 50,
            child: CustomDesignTheme.flatButtonStyle(
              disabledBackgroundColor: Colors.grey,
              backgroundColor: Theme.of(context).colorScheme.secondary,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0),
              ),
              onPressed: () async {
                if (_formKey.currentState!.validate()) {
                  final vm = context.read<IntegrationViewModel>();

                  _apiKeyFocus.unfocus();

                  showLoadingDialog(context, loadingKey);

                  await vm
                      .addOrderManagement(apiKey: _apiKeyController.text.trim())
                      .whenComplete(() async {
                    if (!mounted) return;

                    Navigator.of(loadingKey.currentContext!).pop();
                    await showSuccessAnimationDialog(
                      context,
                      true,
                      tr('api_key_update_success'),
                    );
                  }).catchError((_) async {
                    if (!mounted) return;

                    Navigator.of(loadingKey.currentContext!).pop();
                    await showSuccessAnimationDialog(
                      context,
                      false,
                      tr('api_key_update_failed'),
                    );
                  });

                  _apiKeyController.clear();
                }
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(tr('send'),
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium!
                          .copyWith(color: Colors.white, height: 1)),
                  SmartGaps.gapW10,
                  const Icon(
                    ElementIcons.right,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
