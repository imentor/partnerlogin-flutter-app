import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CreateProjectsScreen extends StatefulWidget {
  const CreateProjectsScreen({super.key});

  @override
  CreateProjectScreenState createState() => CreateProjectScreenState();
}

class CreateProjectScreenState extends State<CreateProjectsScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final appDrawerVm = context.read<AppDrawerViewModel>();
      final newsFeedVm = context.read<NewsFeedViewModel>();
      const dynamicStoryPage = 'createproject';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      if (dynamicStoryEnable) {
        await Future.value(newsFeedVm.getDynamicStory(page: dynamicStoryPage))
            .whenComplete(() {
          if (!mounted) return;

          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(top: 50, left: 20.0, right: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              getDescription(),
              SizedBox(
                height: 70,
                width: double.infinity,
                child: CustomDesignTheme.flatButtonStyle(
                  backgroundColor: Theme.of(context).colorScheme.secondary,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        tr('get_started'),
                        style: Theme.of(context)
                            .textTheme
                            .headlineLarge!
                            .copyWith(color: Colors.white, height: 1),
                      ),
                      SmartGaps.gapW10,
                      const Icon(
                        Icons.arrow_drop_down,
                        color: Colors.white,
                      )
                    ],
                  ),
                  onPressed: () {
                    changeDrawerRoute(Routes.projectWizard);
                  },
                ),
              ),
              SmartGaps.gapH20,
            ],
          ),
        ),
      ),
    );
  }

  Widget getDescription() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          tr('create_project_and_showcase'),
          style: Theme.of(context)
              .textTheme
              .headlineLarge!
              .copyWith(height: 1.2, letterSpacing: -0.5),
        ),
        SmartGaps.gapH20,
        Text(tr('create_project_desc_1'),
            style: Theme.of(context).textTheme.titleSmall),
        SmartGaps.gapH20,
        Text(tr('heres_how_you_do_it'),
            style: Theme.of(context).textTheme.headlineMedium),
        SmartGaps.gapH10,
        ListView(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          children: <Widget>[
            ListTile(
              dense: true,
              contentPadding: const EdgeInsets.only(left: 0),
              leading: Text('1',
                  textAlign: TextAlign.right,
                  style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                      height: 1, color: Theme.of(context).colorScheme.primary)),
              title: Text(tr('upload_the_pictures_of_your_project'),
                  style: Theme.of(context)
                      .textTheme
                      .titleSmall!
                      .copyWith(height: 1.2)),
            ),
            SmartGaps.gapH10,
            ListTile(
              dense: true,
              contentPadding: const EdgeInsets.only(left: 0.0, right: 0.0),
              leading: Text('2',
                  style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                      height: 1, color: Theme.of(context).colorScheme.primary)),
              title: Text(tr('create_project_step_2_ins'),
                  style: Theme.of(context)
                      .textTheme
                      .titleSmall!
                      .copyWith(height: 1.2)),
            ),
            SmartGaps.gapH10,
            ListTile(
              dense: true,
              contentPadding: const EdgeInsets.only(left: 0.0, right: 0.0),
              leading: Text('3',
                  style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                      height: 1, color: Theme.of(context).colorScheme.primary)),
              title: Text(tr('create_project_step_3_ins'),
                  style: Theme.of(context)
                      .textTheme
                      .titleSmall!
                      .copyWith(height: 1.2)),
            )
          ],
        ),
        SmartGaps.gapH30,
      ],
    );
  }
}
