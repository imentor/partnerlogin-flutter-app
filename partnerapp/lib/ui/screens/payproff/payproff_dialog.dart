import 'package:Haandvaerker.dk/model/payproff/post_payproff_account_model.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/payproff/payproff_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

Future<bool?> showPayproffDialog(BuildContext context,
    {required PostPayproffAccountModel account}) async {
  Dialog simpleDialog = Dialog(
    insetPadding:
        const EdgeInsets.only(top: 40, bottom: 40, left: 10, right: 10),
    child: SingleChildScrollView(
      physics: const AlwaysScrollableScrollPhysics(),
      child: Container(
        margin:
            const EdgeInsets.only(left: 20.0, right: 20.0, top: 10, bottom: 30),
        child: PEPSection(account: account),
      ),
    ),
  );

  return await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) => simpleDialog);
}

class PEPSection extends StatefulWidget {
  const PEPSection({super.key, required this.account});

  final PostPayproffAccountModel account;

  @override
  PEPSectionState createState() => PEPSectionState();
}

class PEPSectionState extends State<PEPSection> {
  bool? pepOption = false;
  final loadingKey = GlobalKey<NavigatorState>();
  late PostPayproffAccountModel account;

  @override
  void initState() {
    account = widget.account;
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(tr('politically_exposed_person'),
                style: Theme.of(context).textTheme.headlineSmall),
            Container(
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                        color: Theme.of(context)
                            .colorScheme
                            .primary
                            .withAlpha(100))),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Icon(Icons.close,
                      color:
                          Theme.of(context).colorScheme.primary.withAlpha(100),
                      size: 15),
                ))
          ],
        ),
        SmartGaps.gapH20,
        Text(tr('what_is_a_pep'),
            style: Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(fontWeight: FontWeight.normal)),
        SmartGaps.gapH20,
        Text(tr('i_agree_and_under_oath'),
            style: Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(fontWeight: FontWeight.normal)),
        SmartGaps.gapH20,
        Row(
          children: [
            Radio(
              groupValue: pepOption,
              onChanged: (dynamic value) {
                setState(() {
                  pepOption = value;
                });
              },
              value: true,
            ),
            Text(tr('yes'), style: Theme.of(context).textTheme.headlineSmall),
            SmartGaps.gapW10,
            Radio(
              groupValue: pepOption,
              onChanged: (dynamic value) {
                setState(() {
                  pepOption = value;
                });
              },
              value: false,
            ),
            Text(tr('no'), style: Theme.of(context).textTheme.headlineSmall),
          ],
        ),
        SmartGaps.gapH20,
        Text(tr('i_have_not_been_exposed_to_pep'),
            style: Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(fontWeight: FontWeight.normal)),
        SmartGaps.gapH20,
        Container(
          height: 60,
          width: double.infinity,
          decoration: BoxDecoration(
              border: Border.all(
                  color: Theme.of(context).colorScheme.primary.withAlpha(100))),
          child: CustomDesignTheme.flatButtonStyle(
            backgroundColor: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(tr('back'),
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        color: Theme.of(context).colorScheme.primary,
                        height: 1)),
              ],
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        SmartGaps.gapH20,
        SizedBox(
          height: 60,
          width: double.infinity,
          child: CustomDesignTheme.flatButtonStyle(
            backgroundColor: Theme.of(context).colorScheme.secondary,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(tr('send'),
                    style: Theme.of(context)
                        .textTheme
                        .titleMedium!
                        .copyWith(color: Colors.white, height: 1)),
              ],
            ),
            onPressed: () async {
              final vm = context.read<PayproffViewModel>();
              account.isPep = pepOption;

              showLoadingDialog(context, loadingKey);

              await tryCatchWrapper(
                      context: myGlobals.homeScaffoldKey!.currentContext,
                      function: vm.postPayproffAccount(account: account))
                  .then((value) async {
                Navigator.of(loadingKey.currentContext!).pop();
                showSuccessAnimationDialog(
                        myGlobals.homeScaffoldKey!.currentContext!,
                        value!.success!,
                        value.message)
                    .then((value) {
                  final payproffUrl = vm.payproff!.payproffUrl;
                  if (payproffUrl.isEmpty) {
                    changeDrawerRoute(Routes.newsFeed);
                  } else {
                    payproffVerifyDialog(
                        context: myGlobals.homeScaffoldKey!.currentContext!,
                        verify: () {
                          launchUrl(Uri.parse(payproffUrl));
                          vm.updatePayproffStatus();
                        });
                  }
                });
                //Navigator.of(context).pop();
              }).catchError((onError) {
                Navigator.of(loadingKey.currentContext!).pop();
                showErrorDialog(myGlobals.homeScaffoldKey!.currentContext!,
                    tr('error'), tr('error_unknown_content'), tr('ok'));
              });
            },
          ),
        ),
      ],
    );
  }
}
