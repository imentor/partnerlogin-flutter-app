import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/payproff/payproff_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class PayproffTermsScreen extends StatefulWidget {
  const PayproffTermsScreen({super.key});

  @override
  PayproffTermsScreenState createState() => PayproffTermsScreenState();
}

class PayproffTermsScreenState extends State<PayproffTermsScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      var payproffVm = context.read<PayproffViewModel>();
      await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function: payproffVm.checkPayproffAcount(),
      );

      final accountExists = payproffVm.payproff?.exists ?? false;
      final accountVerified = payproffVm.payproff?.payproffVerified ?? false;
      final hasIban = (payproffVm.payproffWallet?.iban ?? '').isNotEmpty;
      final payproffUrl = payproffVm.payproff?.payproffUrl ?? '';

      if (accountExists) {
        if (!hasIban) {
          payproffVm.isShowBanner = true;
          payproffVm.isPreIbanForm = true;
          payproffVm.ibanController = TextEditingController();
          changeDrawerReplaceRoute(Routes.payproffIbanScreen);
        } else {
          if (!accountVerified && mounted) {
            Navigator.pop(context);
            payproffVerifyDialog(
              context: context,
              verify: () async {
                launchUrl(Uri.parse(payproffUrl));
                await payproffVm.updatePayproffStatus();
              },
            );
          } else {
            changeDrawerReplaceRoute(Routes.payproffVerifiedScreen);
          }
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SvgPicture.asset(
                  'assets/images/payproff_logo.svg',
                ),
                Text(
                  'Godkendt af Finanstilsynet',
                  style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                      color: Colors.grey),
                ),
                SmartGaps.gapH20,
                Consumer<PayproffViewModel>(
                  builder: (context, vm, _) {
                    if (vm.busy) {
                      return Center(
                          child: Column(
                        children: [
                          loader(),
                          Text(
                            tr('loading_please_wait'),
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 20,
                                ),
                          ),
                        ],
                      ));
                    } else {
                      // if (vm.accountExists) {
                      //   return Center(
                      //     child: Column(
                      //       children: [
                      //         Icon(
                      //           Icons.check_circle_outline,
                      //           size: 100,
                      //           color: Theme.of(context).colorScheme.secondary,
                      //         ),
                      //         SmartGaps.gapH20,
                      //         Text(
                      //           tr('payproff_verified'),
                      //           style: Theme.of(context)
                      //               .textTheme
                      //               .bodyMedium!
                      //               .copyWith(
                      //                 fontWeight: FontWeight.w500,
                      //                 fontSize: 20,
                      //               ),
                      //         ),
                      //       ],
                      //     ),
                      //   );
                      // } else {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          RichText(
                              text: TextSpan(children: [
                            const WidgetSpan(
                              child: Icon(Icons.info_outline,
                                  size: 17, color: Colors.grey),
                              alignment: PlaceholderAlignment.middle,
                            ),
                            TextSpan(
                              text: ' Information',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .copyWith(
                                      fontWeight: FontWeight.normal,
                                      fontSize: 15,
                                      color: Colors.grey),
                            ),
                          ])),
                          SmartGaps.gapH10,
                          Text(
                            'For at sikre en tryg oplevelse i hele forløbet, har vi hos Håndværker.dk indgået et samarbejde med PayProff, om en deponeringskonto til opbevaring af din kundes penge, indtil projektet er fuldført.',
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                    color: Colors.grey),
                          ),
                          SmartGaps.gapH20,
                          Text(
                            'PayProff er godkendt som betalingstjeneste hos Finanstilsynet.',
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                    color: Colors.grey),
                          ),
                          RichText(
                              text: TextSpan(children: [
                            TextSpan(
                              text: 'Du kan se deres licens',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .copyWith(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18,
                                      color: Colors.grey),
                            ),
                            TextSpan(
                              text: ' her',
                              recognizer: TapGestureRecognizer()
                                ..onTap = () async => await launchUrl(Uri.parse(
                                    'https://virksomhedsregister.finanstilsynet.dk/virksomhed-under-tilsyn.html?v=30F336FD-6CE2-EC11-A2DB-005056907186')),
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .copyWith(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                            ),
                            TextSpan(
                              text:
                                  ', og læse mere om de godkendelser, de har fået.',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .copyWith(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18,
                                      color: Colors.grey),
                            ),
                          ])),
                          SmartGaps.gapH20,
                          Text(
                            'PayProff er en betalingsløsning skabt til at bringe sikkerhed ind i online pengehandel. Din kunde overfører entreprisesummen til kontoen, hvor de opbevares mens opgaven står på. Når opgaven er udført, kan du anmode om en udbetaling fra kunden, der via sin PayProff profil skal godkende udbetalingen.',
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                    color: Colors.grey),
                          ),
                          SmartGaps.gapH20,
                          Text(
                            'Det er med til at sikre dig, at der er en betaling klar, når projektet afsluttes.',
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                    color: Colors.grey),
                          ),
                          SmartGaps.gapH20,
                          Card(
                            elevation: 0,
                            clipBehavior: Clip.antiAlias,
                            margin: EdgeInsets.zero,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                                side: BorderSide(
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onSurfaceVariant)),
                            child: ExpansionTile(
                              title: Text(
                                'Hvorfor har du brug for mine bankoplysninger?',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyMedium!
                                    .copyWith(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18,
                                    ),
                              ),
                              childrenPadding: const EdgeInsets.only(
                                  left: 20, right: 20, bottom: 10, top: 10),
                              children: [
                                Text(
                                  'Vi har brug for dine bankoplysninger for at sende dig pengene eller for at refundere dig, hvis det er nødvendigt. Det betyder også, at du ikke behøver at dele dine bankoplysninger med din kontraktpartner.',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyMedium!
                                      .copyWith(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16,
                                          color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                          SmartGaps.gapH20,
                          Card(
                            elevation: 0,
                            clipBehavior: Clip.antiAlias,
                            margin: EdgeInsets.zero,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                                side: BorderSide(
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onSurfaceVariant)),
                            child: ExpansionTile(
                              title: Text(
                                'Hvordan overfører jeg pengene til PayProff?',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyMedium!
                                    .copyWith(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18,
                                    ),
                              ),
                              childrenPadding: const EdgeInsets.only(
                                  left: 20, right: 20, bottom: 10, top: 10),
                              children: [
                                Text(
                                  'De tilgængelige betalingsmetoder er: bankoverførsel, snart også i VISA, Mastercard, Dankort og Mobilepay. Ved bankoverførsler får du alle de bankoplysninger og referencenummer, du skal bruge input, når du foretager overførslen. Bemærk venligst, at hvis du ikke indtaster det korrekte referencenummer Der kan opstå forsinkelser, da vores system ikke vil være i stand til at afstemme din betaling med det samme.',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyMedium!
                                      .copyWith(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16,
                                          color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                          SmartGaps.gapH20,
                          Card(
                            elevation: 0,
                            clipBehavior: Clip.antiAlias,
                            margin: EdgeInsets.zero,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                                side: BorderSide(
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onSurfaceVariant)),
                            child: ExpansionTile(
                              title: Text(
                                'Hvorfor bliver jeg stillet ekstra sikkerhedsspørgsmål?',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyMedium!
                                    .copyWith(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18,
                                    ),
                              ),
                              childrenPadding: const EdgeInsets.only(
                                  left: 20, right: 20, bottom: 10, top: 10),
                              children: [
                                Text(
                                  'For at overholde lovmæssige krav skal vi muligvis stille dig et par ekstra spørgsmål. Dette er normal procedure for at sikre, at vi forstår, hvordan du vil bruge vores tjenester, og det hjælper vi bekæmper økonomisk kriminalitet.',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyMedium!
                                      .copyWith(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16,
                                          color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                          SmartGaps.gapH20,
                          Card(
                            elevation: 0,
                            clipBehavior: Clip.antiAlias,
                            margin: EdgeInsets.zero,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                                side: BorderSide(
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onSurfaceVariant)),
                            child: ExpansionTile(
                              title: Text(
                                'Hvorfor skal PayProff bekræfte min identitet? Hvad er Kend din kunde (KYC)?',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyMedium!
                                    .copyWith(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18,
                                    ),
                              ),
                              childrenPadding: const EdgeInsets.only(
                                  left: 20, right: 20, bottom: 10, top: 10),
                              children: [
                                Text(
                                  'PayProff er forpligtet til at levere et sikkert og sikkert miljø. Desuden har vi en juridisk forpligtelse til ved, hvem vores kunder er. Det betyder, at hvis du er privatperson, skal vi udføre en identitetsbekræftelse med NemID i Danmark. Flere muligheder om identitetsbekræftelse både for private enkeltpersoner og virksomheder i og uden for Danmark stilles til rådighed i 3. kvartal 2021.',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyMedium!
                                      .copyWith(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16,
                                          color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                          SmartGaps.gapH30,
                          SizedBox(
                            height: 60,
                            width: double.infinity,
                            child: CustomDesignTheme.flatButtonStyle(
                              shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              backgroundColor:
                                  Theme.of(context).colorScheme.secondary,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(tr('next').toUpperCase(),
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleMedium!
                                          .copyWith(
                                              color: Colors.white, height: 1)),
                                ],
                              ),
                              onPressed: () {
                                changeDrawerRoute(Routes.payproffAccount);
                              },
                            ),
                          ),
                          SmartGaps.gapH50,
                        ],
                      );
                      // }
                    }
                  },
                )
              ],
            ),
          )),
    );
  }
}
