import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/screens/payproff/payproff_iban_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/payproff/payproff_request_dialog.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/payproff/payproff_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

Future<void> showPayproffIbanDialog({required BuildContext context}) async {
  return await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return const PayproffIbanFormDialog();
      });
}

class PayproffIbanFormDialog extends StatelessWidget {
  const PayproffIbanFormDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      insetPadding: const EdgeInsets.all(20),
      backgroundColor: Colors.white,
      titlePadding: EdgeInsets.zero,
      contentPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
      content: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ///

            Text(
              tr('bank_verification'),
              style: context.pttTitleLarge.copyWith(
                fontWeight: FontWeight.bold,
              ),
            ),
            SmartGaps.gapH20,
            SvgPicture.asset(
              'assets/images/payproff_logo.svg',
            ),

            SmartGaps.gapH20,
            Text(
              'KYC (Kend Din Kunde) er en sikkerhedsprocedure, som vi skal følge for at overholde krav fra den danske stat og Finanstilsynet. Det hjælper os med at bekræfte, hvem du er, så vi sikrer, at betalingen når den rette modtager og forhindrer svindel.\n\nPayProff, som vi bruger til at håndtere betalinger, gennemgår dine oplysninger for at sikre, at alt foregår lovligt og sikkert. De tjekker blandt andet, at dit navn, adresse og billede-ID stemmer overens med oplysningerne, så du trygt kan modtage dine betalinger.',
              style: Theme.of(context).textTheme.titleSmall!.copyWith(
                    color: PartnerAppColors.spanishGrey,
                  ),
            ),
            const SizedBox(
              height: 20,
            ),
            Text('Hvorfor er det vigtigt?',
                style: Theme.of(context)
                    .textTheme
                    .titleMedium!
                    .copyWith(fontWeight: FontWeight.bold)),
            const SizedBox(
              height: 10,
            ),
            Text(
              'Når du har gennemført KYC-processen én gang, behøver du ikke gøre det igen. Det betyder, at fremtidige betalinger kan håndteres hurtigt og uden komplikationer.',
              style: Theme.of(context).textTheme.titleSmall!.copyWith(
                    color: PartnerAppColors.spanishGrey,
                  ),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              'KYC sikrer, at:',
              style: Theme.of(context).textTheme.titleSmall!.copyWith(
                    color: PartnerAppColors.spanishGrey,
                  ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '• Dine betalinger når frem til dig.',
                    style: Theme.of(context).textTheme.titleSmall!.copyWith(
                          color: PartnerAppColors.spanishGrey,
                        ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    '• Du er beskyttet mod svindel.',
                    style: Theme.of(context).textTheme.titleSmall!.copyWith(
                          color: PartnerAppColors.spanishGrey,
                        ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    '• Vi overholder gældende lovgivning.',
                    style: Theme.of(context).textTheme.titleSmall!.copyWith(
                          color: PartnerAppColors.spanishGrey,
                        ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              'Vi forstår, at det kan føles som en ekstra opgave, men det er kun nødvendigt én gang og sikrer, at alt forløber problemfrit fremover.',
              style: Theme.of(context).textTheme.titleSmall!.copyWith(
                    color: PartnerAppColors.spanishGrey,
                  ),
            ),
            const SizedBox(
              height: 20,
            ),
            Text('Sådan gør du:',
                style: Theme.of(context)
                    .textTheme
                    .titleMedium!
                    .copyWith(fontWeight: FontWeight.bold)),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '1. Klik på knappen.',
                    style: Theme.of(context).textTheme.titleSmall!.copyWith(
                          color: PartnerAppColors.spanishGrey,
                        ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    '2. Indtast dit IBAN-nummer og upload dit billede-ID.',
                    style: Theme.of(context).textTheme.titleSmall!.copyWith(
                          color: PartnerAppColors.spanishGrey,
                        ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    '3. PayProff tjekker og godkender dine oplysninger inden for 48 timer.',
                    style: Theme.of(context).textTheme.titleSmall!.copyWith(
                          color: PartnerAppColors.spanishGrey,
                        ),
                  ),
                ],
              ),
            ),

            const IbanNumberTextField(),

            SmartGaps.gapH20,

            CustomDesignTheme.flatButtonStyle(
              onPressed: () async {
                ///

                final payproffVm = context.read<PayproffViewModel>();
                if (!payproffVm.isIbanFieldEmpty) {
                  if (!payproffVm.isPayproffVerified &&
                      DateTime.now()
                              .difference(payproffVm.statusUpdated!)
                              .inHours <
                          48) {
                    Navigator.pop(context);
                    await showPayproffRequestDialog(context: context);
                  } else {
                    await payproffVm.submitIban(context: context);
                  }
                }

                ///
              },
              height: 50,
              width: MediaQuery.of(context).size.width,
              backgroundColor: PartnerAppColors.green,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ///
                  Text(
                    tr('authenticate_with_mitID'),
                    style: context.pttTitleLarge.copyWith(
                      fontSize: 19,
                      color: Colors.white,
                    ),
                  ),
                  const Icon(
                    Icons.arrow_forward_outlined,
                    size: 20,
                    color: Colors.white,
                  ),

                  ///
                ],
              ),
            ),

            ///
          ],
        ),
      ),
    );
  }
}
