import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

Future<void> showPayproffRequestDialog({required BuildContext context}) async {
  ///

  return await showDialog(
    context: context,
    builder: (context) => const PayproffRequestDialog(),
  );

  ///
}

class PayproffRequestDialog extends StatelessWidget {
  const PayproffRequestDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      insetPadding: const EdgeInsets.all(20),
      backgroundColor: Colors.white,
      titlePadding: EdgeInsets.zero,
      contentPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
      content: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ///

            Text(
              tr('payproff_48_hour_request'),
              style: context.pttTitleSmall.copyWith(
                fontWeight: FontWeight.w500,
                height: 2,
              ),
            ),

            ///
          ],
        ),
      ),
    );
  }
}
