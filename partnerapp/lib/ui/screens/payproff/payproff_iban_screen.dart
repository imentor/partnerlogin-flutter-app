import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_textfield.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/payproff/payproff_request_dialog.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/extension/colors.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/payproff/payproff_viewmodel.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class PayproffIbanScreen extends StatefulWidget {
  const PayproffIbanScreen({super.key});

  @override
  State<PayproffIbanScreen> createState() => _PayproffIbanScreenState();
}

class _PayproffIbanScreenState extends State<PayproffIbanScreen> {
  // final loadingKey = GlobalKey<NavigatorState>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      final payproffVm = context.read<PayproffViewModel>();

      modalManager.showLoadingModal();
      await tryCatchWrapper(
              context: context,
              function: Future.value(payproffVm.checkPayproffAcount()))
          .whenComplete(() {
        TextEditingController ibanController = TextEditingController(
            text: (payproffVm.payproffWallet?.iban ?? ''));
        payproffVm.ibanController = ibanController;
        modalManager.hideLoadingModal();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<PayproffViewModel>(builder: (_, payproffVm, __) {
      bool isShowBanner = payproffVm.isShowBanner;

      return Scaffold(
          //
          appBar: isShowBanner
              ? AppBar(
                  elevation: 2,
                  toolbarHeight: 50,
                  leading: const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Icon(
                      Icons.check_circle_outline_rounded,
                      color: PartnerAppColors.green,
                      size: 30,
                    ),
                  ),
                  leadingWidth: 40,
                  title: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      //

                      Expanded(
                        child: RichText(
                          textAlign: TextAlign.start,
                          text: TextSpan(
                            style: GoogleFonts.notoSans(
                              textStyle: const TextStyle(
                                  fontSize: 12,
                                  color: Colors.black,
                                  height: 1.4,
                                  fontWeight: FontWeight.w400),
                            ),
                            children: [
                              TextSpan(
                                text: '${tr('success')}! ',
                                style: const TextStyle(
                                    fontWeight: FontWeight.w600),
                              ),
                              TextSpan(
                                text: tr('enterprise_contract_now_signed'),
                              )
                            ],
                          ),
                        ),
                      ),

                      GestureDetector(
                        onTap: () => context
                            .read<PayproffViewModel>()
                            .isShowBanner = false,
                        child: const Icon(
                          Icons.close,
                          size: 20,
                          color: Colors.black,
                        ),
                      ),

                      SmartGaps.gapW20,

                      //
                    ],
                  ),
                )
              : const DrawerAppBar(),
          body: Theme(
            data: Theme.of(context).copyWith(
              colorScheme: context.colorScheme.copyWith(
                secondary: PartnerAppColors.accentBlue.withValues(alpha: 0.3),
              ),
            ),
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      tr('bank_verification'),
                      style: context.pttTitleLarge.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    SvgPicture.asset(
                      SvgIcons.payproffIcon,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    if ((payproffVm.payproff?.payproffVerified ?? false)) ...[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        padding: const EdgeInsets.all(30),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: PartnerAppColors.darkBlue
                                    .withValues(alpha: .3))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(
                              Icons.check_circle,
                              color: PartnerAppColors.malachite,
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Text(
                              'Verificeret',
                              style: Theme.of(context).textTheme.titleMedium,
                            )
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Du er nu verificeret af PayProff. Hvis dit IBAN-nummer ændrer sig, kan du opdatere det ved at indtaste dit nye IBAN-nummer og klikke på “Opdater IBAN-nummer”. Bemærk, at ændringerne vil blive valideret af PayProff inden for 3-5 hverdage.',
                        style: Theme.of(context).textTheme.titleSmall!.copyWith(
                              color: PartnerAppColors.spanishGrey,
                            ),
                      ),
                    ] else ...[
                      Text(
                        'KYC (Kend Din Kunde) er en sikkerhedsprocedure, som vi skal følge for at overholde krav fra den danske stat og Finanstilsynet. Det hjælper os med at bekræfte, hvem du er, så vi sikrer, at betalingen når den rette modtager og forhindrer svindel.\n\nPayProff, som vi bruger til at håndtere betalinger, gennemgår dine oplysninger for at sikre, at alt foregår lovligt og sikkert. De tjekker blandt andet, at dit navn, adresse og billede-ID stemmer overens med oplysningerne, så du trygt kan modtage dine betalinger.',
                        style: Theme.of(context).textTheme.titleSmall!.copyWith(
                              color: PartnerAppColors.spanishGrey,
                            ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Text('Hvorfor er det vigtigt?',
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium!
                              .copyWith(fontWeight: FontWeight.bold)),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Når du har gennemført KYC-processen én gang, behøver du ikke gøre det igen. Det betyder, at fremtidige betalinger kan håndteres hurtigt og uden komplikationer.',
                        style: Theme.of(context).textTheme.titleSmall!.copyWith(
                              color: PartnerAppColors.spanishGrey,
                            ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Text(
                        'KYC sikrer, at:',
                        style: Theme.of(context).textTheme.titleSmall!.copyWith(
                              color: PartnerAppColors.spanishGrey,
                            ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '• Dine betalinger når frem til dig.',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                    color: PartnerAppColors.spanishGrey,
                                  ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              '• Du er beskyttet mod svindel.',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                    color: PartnerAppColors.spanishGrey,
                                  ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              '• Vi overholder gældende lovgivning.',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                    color: PartnerAppColors.spanishGrey,
                                  ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Vi forstår, at det kan føles som en ekstra opgave, men det er kun nødvendigt én gang og sikrer, at alt forløber problemfrit fremover.',
                        style: Theme.of(context).textTheme.titleSmall!.copyWith(
                              color: PartnerAppColors.spanishGrey,
                            ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Text('Sådan gør du:',
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium!
                              .copyWith(fontWeight: FontWeight.bold)),
                      Container(
                        margin: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '1. Klik på knappen.',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                    color: PartnerAppColors.spanishGrey,
                                  ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              '2. Indtast dit IBAN-nummer og upload dit billede-ID.',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                    color: PartnerAppColors.spanishGrey,
                                  ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              '3. PayProff tjekker og godkender dine oplysninger inden for 48 timer.',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                    color: PartnerAppColors.spanishGrey,
                                  ),
                            ),
                          ],
                        ),
                      ),
                    ],
                    const IbanNumberTextField(),
                    SmartGaps.gapH20,
                    Align(
                      alignment: Alignment.centerRight,
                      child: CustomDesignTheme.flatButtonStyle(
                        height: 50,
                        backgroundColor: !payproffVm.payproff!.exists
                            ? const Color(0xff3664AF)
                            : PartnerAppColors.malachite,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Text(
                          (payproffVm.payproff?.payproffVerified ?? false)
                              ? 'Opdater IBAN-nummer'
                              : !payproffVm.payproff!.exists
                                  ? tr('authenticate_with_mitID')
                                  : tr('add_iban_number'),
                          style: context.pttTitleLarge.copyWith(
                            fontSize: 19,
                            color: Colors.white,
                          ),
                        ),
                        onPressed: () async {
                          if (payproffVm.payproff?.payproffVerified ?? false) {
                            if (!payproffVm.isPayproffVerified &&
                                payproffVm.statusUpdated != null &&
                                DateTime.now()
                                        .difference(payproffVm.statusUpdated!)
                                        .inHours <
                                    48) {
                              await showPayproffRequestDialog(context: context);
                            } else {
                              if (payproffVm.payproff?.payproffUrl.isNotEmpty ==
                                  true) {
                                modalManager.showLoadingModal();
                                await payproffVm
                                    .updatePayproffStatus()
                                    .whenComplete(
                                        () => modalManager.hideLoadingModal());
                                await launchUrl(Uri.parse(
                                    payproffVm.payproff!.payproffUrl));
                              } else {
                                await payproffVm
                                    .payproffVerify()
                                    .whenComplete(() async {
                                  await payproffVm
                                      .updatePayproffStatus()
                                      .whenComplete(() =>
                                          modalManager.hideLoadingModal());
                                  await launchUrl(
                                      Uri.parse(payproffVm.payproffUrl));
                                });
                              }
                            }
                          } else {
                            context
                                .read<PayproffViewModel>()
                                .submitIban(context: context);
                          }
                        },
                      ),
                    ),
                    // SmartGaps.gapH20,

                    // SmartGaps.gapH20,
                    // SvgPicture.asset(
                    //   'assets/images/payproff_logo.svg',
                    // ),
                    // SmartGaps.gapH20,
                    // Text(
                    //   tr('bank_verification_message'),
                    //   style: context.pttTitleMedium,
                    // ),
                    // SmartGaps.gapH20,
                    // Text(
                    //   tr('how_to_find_your_iban'),
                    //   textAlign: TextAlign.left,
                    //   style: context.pttTitleSmall.copyWith(
                    //     fontWeight: FontWeight.w400,
                    //     height: 2,
                    //   ),
                    // ),
                    // ...[
                    //   SmartGaps.gapH10,
                    //   FindIbanBullet(text: tr('login_to_your_online_bank')),
                    //   SmartGaps.gapH10,
                    //   FindIbanBullet(text: tr('go_to_account_details')),
                    //   SmartGaps.gapH10,
                    //   FindIbanBullet(text: tr('find_and_note_iban')),
                    //   SmartGaps.gapH20,
                    // ],

                    // const IbanNumberTextField(),
                    // SmartGaps.gapH20,
                    // Align(
                    //   alignment: Alignment.centerRight,
                    //   child: CustomDesignTheme.flatButtonStyle(
                    //     height: 50,
                    //     backgroundColor: !payproffVm.payproff!.exists
                    //         ? const Color(0xff3664AF)
                    //         : PartnerAppColors.malachite,
                    //     shape: RoundedRectangleBorder(
                    //       borderRadius: BorderRadius.circular(5),
                    //     ),
                    //     child: Text(
                    //       !payproffVm.payproff!.exists
                    //           ? tr('authenticate_with_mitID')
                    //           : tr('add_iban_number'),
                    //       style: context.pttTitleLarge.copyWith(
                    //         fontSize: 19,
                    //         color: Colors.white,
                    //       ),
                    //     ),
                    //     onPressed: () async {
                    //       if (!(payproffVm.payproff?.payproffVerified ?? true)) {
                    //         if (!payproffVm.isPayproffVerified &&
                    //             payproffVm.statusUpdated != null &&
                    //             DateTime.now()
                    //                     .difference(payproffVm.statusUpdated!)
                    //                     .inHours <
                    //                 48) {
                    //           await showPayproffRequestDialog(context: context);
                    //         } else {
                    //           if (payproffVm.payproff?.payproffUrl.isNotEmpty ==
                    //               true) {
                    //             modalManager.showLoadingModal();
                    //             await payproffVm
                    //                 .updatePayproffStatus()
                    //                 .whenComplete(
                    //                     () => modalManager.hideLoadingModal());
                    //             await launchUrl(
                    //                 Uri.parse(payproffVm.payproff!.payproffUrl));
                    //           } else {
                    //             await payproffVm
                    //                 .payproffVerify()
                    //                 .whenComplete(() async {
                    //               await payproffVm
                    //                   .updatePayproffStatus()
                    //                   .whenComplete(
                    //                       () => modalManager.hideLoadingModal());
                    //               await launchUrl(
                    //                   Uri.parse(payproffVm.payproffUrl));
                    //             });
                    //           }
                    //         }
                    //       } else {
                    //         context
                    //             .read<PayproffViewModel>()
                    //             .submitIban(context: context);
                    //       }
                    //     },
                    //   ),
                    // ),
                    // SmartGaps.gapH40,

                    ///
                  ],
                ),
              ),
            ),
          )
          //
          );
    });

    //
  }
}

class FindIbanBullet extends StatelessWidget {
  const FindIbanBullet({super.key, required this.text});

  final String text;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 15),
          child: Icon(
            Icons.circle,
            size: 10,
            color: Colors.black,
          ),
        ),
        SmartGaps.gapW10,
        Expanded(
          child: Text(text,
              textAlign: TextAlign.start,
              style: context.pttTitleSmall.copyWith(
                fontWeight: FontWeight.w400,
                color: PartnerAppColors.grey1,
                height: 2,
              )),
        ),
      ],
    );
  }
}

class IbanNumberTextField extends StatelessWidget {
  const IbanNumberTextField({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<PayproffViewModel>(
      builder: (context, payproffVm, _) {
        return Form(
          key: payproffVm.ibanFormKey,
          child: CustomTextField(
            labelText: '',
            controller: payproffVm.ibanController ?? TextEditingController(),
            titleWidget: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(
                style: GoogleFonts.notoSans(
                  textStyle: context.pttBodySmall.copyWith(
                    color: Colors.black,
                    fontSize: 14,
                    height: 1.35,
                  ),
                ),
                children: [
                  const TextSpan(
                    text: '* ',
                    style: TextStyle(color: Colors.red),
                  ),
                  TextSpan(
                    text: tr('iban_number'),
                  )
                ],
              ),
            ),
            validation: (val) {
              if (val!.isEmpty) {
                return tr('iban_number_empty_validator');
              }

              return null;
            },
            onChanged: (val) {
              payproffVm.isBanFieldEmpty = val!.isEmpty;
            },
            inputStyle: GoogleFonts.notoSans(
              textStyle: const TextStyle(
                fontSize: 14,
                color: Colors.black,
                fontWeight: FontWeight.w400,
              ),
            ),
            maxLength: 19,
            decoration: InputDecoration(
              prefixIcon: CountryCodePicker(
                onChanged: (value) {
                  payproffVm.selectedCountry = value.code ?? 'DK';
                },
                initialSelection: 'DK',
                favorite: const ['DK'],
                showCountryOnly: true,
                showOnlyCountryWhenClosed: false,
                builder: (p0) {
                  if (p0 == null) return const SizedBox.shrink();

                  return Container(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: Row(mainAxisSize: MainAxisSize.min, children: [
                      SizedBox(
                        height: 30,
                        width: 30,
                        child: Image.asset(
                          p0.flagUri!,
                          package: 'country_code_picker',
                        ),
                      ),
                      SmartGaps.gapW10,
                      Text(p0.code!),
                      const Icon(FeatherIcons.chevronDown),
                    ]),
                  );
                },
              ),
              suffixIcon: payproffVm.isIbanFieldEmpty
                  ? null
                  : GestureDetector(
                      onTap: () {
                        payproffVm.ibanController!.clear();
                        payproffVm.isBanFieldEmpty = true;
                      },
                      child: const Icon(
                        Icons.close,
                        color: Colors.black,
                        size: 20,
                      ),
                    ),
              hintText: 'XXXXXXXXXXXXXXXXXX',
              counterText: '',
              hintStyle: GoogleFonts.notoSans(
                textStyle: TextStyle(
                  fontSize: 14,
                  color: const Color(0xff707070).withValues(alpha: 0.5),
                  fontWeight: FontWeight.w400,
                ),
              ),
              filled: false,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: const BorderSide(
                  color: Color(0xff707070),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: const BorderSide(
                  color: PartnerAppColors.accentBlue,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: const BorderSide(
                  color: Color(0xff707070),
                ),
              ),
              errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: const BorderSide(
                  color: PartnerAppColors.red,
                ),
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: const BorderSide(
                  color: PartnerAppColors.red,
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
