import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/payproff/payproff_viewmodel.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

Future<void> payproffIbanVerifyDialog(BuildContext context) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        insetPadding: const EdgeInsets.all(20),
        contentPadding: const EdgeInsets.all(20),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: const PayproffIbanVerify(),
        ),
      );
    },
  );
}

class PayproffIbanVerify extends StatefulWidget {
  const PayproffIbanVerify({super.key});

  @override
  PayproffIbanVerifyState createState() => PayproffIbanVerifyState();
}

class PayproffIbanVerifyState extends State<PayproffIbanVerify> {
  final formKey = GlobalKey<FormBuilderState>();
  String countryCode = "DK";
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: FormBuilder(
        key: formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (isLoading)
              const Center(child: ConnectivityLoader())
            else ...[
              Text(
                tr('iban_form_header'),
                textAlign: TextAlign.start,
                style: GoogleFonts.notoSans(
                  textStyle: const TextStyle(
                    fontSize: 16,
                    color: PartnerAppColors.darkBlue,
                    height: 1.67,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              SmartGaps.gapH20,
              RichText(
                textAlign: TextAlign.start,
                text: TextSpan(
                  style: GoogleFonts.notoSans(
                    textStyle: const TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                        height: 1.35,
                        fontWeight: FontWeight.w600),
                  ),
                  children: [
                    const TextSpan(
                      text: '* ',
                      style: TextStyle(color: Colors.red),
                    ),
                    TextSpan(
                      text: tr('iban_number'),
                    )
                  ],
                ),
              ),
              SmartGaps.gapH5,
              FormBuilderTextField(
                name: "iban",
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  prefixIcon: CountryCodePicker(
                    onChanged: (value) {
                      setState(() {
                        countryCode = value.code ?? '';
                      });
                    },
                    initialSelection: 'DK',
                    favorite: const ['DK'],
                    showCountryOnly: true,
                    showOnlyCountryWhenClosed: false,
                    builder: (p0) {
                      if (p0 == null) return const SizedBox.shrink();

                      return Container(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: Row(mainAxisSize: MainAxisSize.min, children: [
                          SizedBox(
                            height: 30,
                            width: 30,
                            child: Image.asset(
                              p0.flagUri!,
                              package: 'country_code_picker',
                            ),
                          ),
                          SmartGaps.gapW10,
                          Text(p0.code!),
                          const Icon(FeatherIcons.chevronDown),
                        ]),
                      );
                    },
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: const BorderSide(
                      color: Color(0xff707070),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: const BorderSide(
                      color: PartnerAppColors.accentBlue,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: const BorderSide(
                      color: Color(0xff707070),
                    ),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: const BorderSide(
                      color: PartnerAppColors.red,
                    ),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: const BorderSide(
                      color: PartnerAppColors.red,
                    ),
                  ),
                ),
                validator: FormBuilderValidators.required(
                    errorText: tr('this_field_cannot_be_empty')),
              ),
              SmartGaps.gapH30,
              Align(
                alignment: Alignment.centerRight,
                child: CustomDesignTheme.flatButtonStyle(
                  onPressed: () async {
                    final payproffVm = context.read<PayproffViewModel>();

                    final navigator = Navigator.of(context);

                    if (formKey.currentState!.validate()) {
                      try {
                        setState(() {
                          isLoading = true;
                        });

                        // final isIbanValid = await payproffVm.validateIban(
                        //     iban: ibanFormat.getUnmaskedText());

                        // if (isIbanValid) {
                        await tryCatchWrapper(
                                context:
                                    myGlobals.homeScaffoldKey!.currentContext,
                                function: payproffVm.payproffUpdate(
                                    iban:
                                        '$countryCode${(formKey.currentState!.fields['iban']!.value as String).replaceAll(' ', '')}'))
                            .then((value) async {
                          setState(() {
                            isLoading = false;
                          });
                          if ((value ?? false)) {
                            navigator.pop();

                            await payproffVm.updatePayproffStatus();
                            await launchUrl(
                                Uri.parse(payproffVm.payproff!.payproffUrl));
                          }
                        });
                        // } else {
                        //   if (mounted) {
                        //     Navigator.of(context).pop();
                        //     await showOkAlertDialog(
                        //       context: context,
                        //       title: tr('error'),
                        //       message: tr('iban_number_invalid'),
                        //       okLabel: tr('ok'),
                        //     );
                        //   }
                        // }
                      } catch (e) {
                        setState(() {
                          isLoading = false;
                        });
                      }
                    }
                  },
                  height: 50,
                  backgroundColor: PartnerAppColors.green,
                  child: Text(
                    tr('authenticate_with_mitID'),
                    style: GoogleFonts.notoSans(
                      textStyle: const TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        height: 1.38,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ]
          ],
        ),
      ),
    );
  }
}
