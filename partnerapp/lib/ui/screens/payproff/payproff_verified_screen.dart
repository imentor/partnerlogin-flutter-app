import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/contract/contract_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class PayproffVerifiedScreen extends StatelessWidget {
  const PayproffVerifiedScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //
      appBar: const DrawerAppBar(
          // onBack: () {
          //   final projectsVm = context.read<MyProjectsViewModel>();
          //   projectsVm.setBusy(true);
          //   changeDrawerReplaceRoute(Routes.projectDashboard,
          //       arguments: projectsVm.currentProjectId);
          // },
          ),

      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            //

            Text(
              tr('to_view_status_manage_project'),
              textAlign: TextAlign.start,
              style: GoogleFonts.notoSans(
                textStyle: const TextStyle(
                  fontSize: 14,
                  color: Color(0xff707070),
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),

            SmartGaps.gapH10,

            CustomDesignTheme.flatButtonStyle(
              onPressed: () {
                context.read<ContractViewModel>().contractDetailsToDefault();
                final activeProject =
                    context.read<MyProjectsViewModel>().activeProject;

                if ((activeProject.id ?? 0) > 0) {
                  Navigator.pop(context);

                  changeDrawerRoute(Routes.projectDashboard,
                      arguments: {'projectId': activeProject.id});
                }
              },
              height: 50,
              width: MediaQuery.of(context).size.width,
              backgroundColor: PartnerAppColors.green,
              child: Text(
                tr('manage_project'),
                style: GoogleFonts.notoSans(
                  textStyle: const TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                    height: 1.38,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),

            //
          ],
        ),
      ),

      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              //

              SmartGaps.gapH140,

              const Icon(
                Icons.check_circle,
                size: 70,
                color: PartnerAppColors.green,
              ),

              SmartGaps.gapH30,

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  '${tr('success')}! ${tr('enterprise_contract_now_signed')}',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.notoSans(
                    textStyle: const TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      height: 1.75,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),

              SmartGaps.gapH13,

              Text(
                tr('customer_has_been_notified'),
                textAlign: TextAlign.center,
                style: GoogleFonts.notoSans(
                  textStyle: const TextStyle(
                    fontSize: 16,
                    color: Color(0xff707070),
                    height: 1.75,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),

              //
            ],
          ),
        ),
      ),
    );
  }
}
