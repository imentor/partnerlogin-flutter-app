import 'package:Haandvaerker.dk/model/partner/bitrix_company_model.dart';
import 'package:Haandvaerker.dk/model/payproff/post_payproff_account_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_textfield.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/payproff/payproff_dialog.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/payproff/payproff_viewmodel.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class PayproffAccountScreen extends StatefulWidget {
  const PayproffAccountScreen({super.key});

  @override
  PayproffAccountScreenState createState() => PayproffAccountScreenState();
}

class PayproffAccountScreenState extends State<PayproffAccountScreen> {
  final firstName = TextEditingController();
  final lastName = TextEditingController();
  final middleName = TextEditingController();
  final birthday = TextEditingController();
  final address = TextEditingController();
  final zipcode = TextEditingController();
  final town = TextEditingController();
  final state = TextEditingController();
  final email = TextEditingController();
  final phoneNumber = TextEditingController();
  final countryCode = TextEditingController();
  final cprNumber = TextEditingController();
  final iban = TextEditingController();
  final formKey = GlobalKey<FormState>();

  MaskTextInputFormatter cprFormatter = MaskTextInputFormatter();

  final loadingKey = GlobalKey<NavigatorState>();

  String countryCodeSelector = 'DK';

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      final payproffVm = context.read<PayproffViewModel>();

      payproffVm.setBusy(true);
      await tryCatchWrapper(
          context: myGlobals.homeScaffoldKey!.currentContext,
          function: Future.wait([
            payproffVm.getSupplierInfo(),
            payproffVm.checkPayproffAcount()
          ]));

      await payproffVm.initView();
      payproffVm.setBusy(false);
      BitrixCompanyModel? company = payproffVm.company;

      email.text = (company?.email ?? '');
      zipcode.text = (company?.zipCode ?? '');
      address.text = (company?.companyAddress ?? '');
      phoneNumber.text = (company?.mobile ?? '');
      town.text = (company?.city ?? '');
      state.text = (company?.city ?? '');
      countryCode.text = '+45';

      cprFormatter = MaskTextInputFormatter(
          mask: '###### - ####', filter: {"#": RegExp('[0-9]')});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: Form(
          key: formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SvgPicture.asset(
                'assets/images/payproff_logo.svg',
              ),
              Text(
                'Godkendt af Finanstilsynet',
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    fontWeight: FontWeight.w500,
                    fontSize: 12,
                    color: Colors.grey),
              ),
              SmartGaps.gapH20,
              _forms(),
            ],
          ),
        ),
      ),
      // body: Container(
      //   padding: const EdgeInsets.only(top: 20, left: 20.0, right: 20.0),
      //   child: SingleChildScrollView(
      //     child: Column(
      //         crossAxisAlignment: CrossAxisAlignment.start,
      //         children: <Widget>[
      //           Text(tr('payproff_account'),
      //               style: Theme.of(context).textTheme.headlineLarge),
      //           SmartGaps.gapH20,
      //           Text(tr('verify_payproff'),
      //               style: Theme.of(context).textTheme.titleSmall),
      //           SmartGaps.gapH20,
      //           _getForm(),
      //         ]),
      //   ),
      // ),
    );
  }

  Widget _forms() {
    return Consumer<PayproffViewModel>(builder: (_, vm, __) {
      final bool accountExists = vm.payproff?.exists ?? false;
      final bool verified = vm.payproff?.payproffVerified ?? false;

      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: tr('bank_verification'),
                  style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                      letterSpacing: 1.0,
                      color: PartnerAppColors.darkBlue,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                WidgetSpan(
                    alignment: PlaceholderAlignment.middle,
                    child: Container(
                      margin: const EdgeInsets.symmetric(horizontal: 10),
                      height: 25,
                      width: 25,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: verified
                              ? PartnerAppColors.malachite
                              : PartnerAppColors.darkBlue),
                      child: const Icon(FeatherIcons.check,
                          color: Colors.white, size: 18),
                    ))
              ],
            ),
          ),
          SmartGaps.gapH20,
          Text(
            tr('validate_payproff_account'),
            style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                letterSpacing: 1.0,
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.bold,
                fontSize: 20),
          ),
          SmartGaps.gapH10,
          SizedBox(
            height: 40,
            width: double.infinity,
            child: TextButton(
              style: TextButton.styleFrom(
                  backgroundColor: verified
                      ? PartnerAppColors.darkBlue.withValues(alpha: .4)
                      : PartnerAppColors.darkBlue),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(tr('validate_account'),
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium!
                          .copyWith(color: Colors.white, height: 1)),
                ],
              ),
              onPressed: () {
                if (!verified) {
                  launchUrl(Uri.parse(vm.payproff?.payproffUrl ?? ''));
                }
              },
            ),
          ),
          SmartGaps.gapH20,
          if (verified) ...[
            Text(
              tr('payproff_done'),
              style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                  color: PartnerAppColors.darkBlue,
                  fontWeight: FontWeight.normal),
            ),
            SmartGaps.gapH40,
          ] else
            SmartGaps.gapH40,
          Text(
            tr('update_iban'),
            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                color: PartnerAppColors.darkBlue,
                fontWeight: FontWeight.normal),
          ),
          SmartGaps.gapH20,
          Row(
            children: [
              Expanded(
                child: CustomTextFieldNew(
                  controller: firstName,
                  labelText: tr('first_name'),
                  labelFontSize: 16,
                  labelFontWeight: FontWeight.normal,
                  enabled: !accountExists,
                ),
              ),
              SmartGaps.gapW10,
              Expanded(
                child: CustomTextFieldNew(
                  controller: lastName,
                  labelText: tr('last_name'),
                  labelFontSize: 16,
                  labelFontWeight: FontWeight.normal,
                  enabled: !accountExists,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: CustomTextFieldNew(
                  controller: middleName,
                  labelText: tr('middle_name'),
                  labelFontSize: 16,
                  labelFontWeight: FontWeight.normal,
                  enabled: !accountExists,
                ),
              ),
              SmartGaps.gapW10,
              Expanded(
                child: GestureDetector(
                  onTap: () async {
                    if (!accountExists) {
                      final date = await showDatePicker(
                          initialEntryMode: DatePickerEntryMode.calendar,
                          context: context,
                          firstDate: DateTime(1900),
                          initialDate: DateTime.now(),
                          lastDate: DateTime(2100));

                      if (date != null) {
                        setState(() {
                          birthday.text = Formatter.formatDateTime(
                              type: DateFormatType.fullMonthYearDate,
                              date: date);
                        });
                      }
                    }
                  },
                  child: CustomTextFieldNew(
                    controller: birthday,
                    enabled: false,
                    labelText: tr('birthday'),
                    labelFontSize: 16,
                    labelFontWeight: FontWeight.normal,
                    suffixWidget: Icon(
                      Icons.calendar_today_outlined,
                      size: 30,
                      color: Theme.of(context)
                          .colorScheme
                          .primary
                          .withValues(alpha: .4),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: CustomTextFieldNew(
                  controller: address,
                  labelText: tr('address'),
                  labelFontSize: 16,
                  labelFontWeight: FontWeight.normal,
                  enabled: !accountExists,
                ),
              ),
              SmartGaps.gapW10,
              Expanded(
                child: CustomTextFieldNew(
                  controller: zipcode,
                  labelText: tr('zip_code'),
                  labelFontSize: 16,
                  labelFontWeight: FontWeight.normal,
                  enabled: !accountExists,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: CustomTextFieldNew(
                  controller: town,
                  labelText: tr('town'),
                  labelFontSize: 16,
                  labelFontWeight: FontWeight.normal,
                  enabled: !accountExists,
                ),
              ),
              SmartGaps.gapW10,
              Expanded(
                child: CustomTextFieldNew(
                  controller: state,
                  labelText: tr('state'),
                  labelFontSize: 16,
                  labelFontWeight: FontWeight.normal,
                  enabled: !accountExists,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: CustomTextFieldNew(
                  controller: email,
                  labelText: tr('email'),
                  labelFontSize: 16,
                  labelFontWeight: FontWeight.normal,
                  enabled: !accountExists,
                  keyboardType: TextInputType.emailAddress,
                ),
              ),
              SmartGaps.gapW10,
              Expanded(
                child: CustomTextFieldNew(
                  controller: TextEditingController(),
                  enabled: false,
                  labelText: tr('country'),
                  labelFontSize: 16,
                  labelFontWeight: FontWeight.normal,
                  validation: (val) {
                    return null;
                  },
                  prefixWidget: Padding(
                    padding: const EdgeInsets.only(left: 12),
                    child: Wrap(
                      alignment: WrapAlignment.center,
                      children: [
                        SvgPicture.asset(
                          'assets/images/denmark_flag.svg',
                          height: 25,
                          width: 30,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, top: 4),
                          child: Text(
                            'Denmark',
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge!
                                .copyWith(
                                    letterSpacing: 1.0,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 15),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          CustomTextFieldNew(
            controller: phoneNumber,
            labelText: tr('phone_number'),
            labelFontSize: 16,
            labelFontWeight: FontWeight.normal,
            enabled: !accountExists,
            prefixWidget: Padding(
              padding: const EdgeInsets.only(left: 12),
              child: Wrap(
                alignment: WrapAlignment.center,
                children: [
                  SvgPicture.asset(
                    'assets/images/denmark_flag.svg',
                    height: 25,
                    width: 30,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10, top: 4),
                    child: Text(
                      '+45',
                      style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                          letterSpacing: 1.0,
                          fontWeight: FontWeight.bold,
                          fontSize: 15),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 3),
                    child: Icon(
                      Icons.arrow_drop_down,
                      color: Theme.of(context).colorScheme.onSurfaceVariant,
                    ),
                  ),
                ],
              ),
            ),
          ),
          CustomTextFieldNew(
              controller: cprNumber,
              labelText: tr('social_security_number'),
              enabled: !accountExists,
              labelFontSize: 16,
              labelFontWeight: FontWeight.normal,
              inputFormatters: [cprFormatter]),
          SmartGaps.gapH10,
          Row(
            children: [
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(tr('language'),
                      style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                          letterSpacing: 1.0,
                          fontWeight: FontWeight.normal,
                          fontSize: 16)),
                  SmartGaps.gapH7,
                  InputDecorator(
                    decoration: InputDecoration(
                        contentPadding:
                            const EdgeInsets.only(left: 12, right: 12),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Theme.of(context)
                                  .colorScheme
                                  .onSurfaceVariant
                                  .withValues(alpha: 0.4)),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(3)),
                        )),
                    child: DropdownButtonFormField(
                      items: vm.country!
                          .map((language, value) {
                            return MapEntry(
                                value,
                                DropdownMenuItem<String>(
                                  value: language,
                                  child: Text(value,
                                      overflow: TextOverflow.ellipsis,
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium!
                                          .copyWith(
                                              fontWeight: FontWeight.w500)),
                                ));
                          })
                          .values
                          .toList(),
                      onChanged: !accountExists
                          ? (dynamic val) {
                              setState(() {
                                vm.selectedCountry = val as String;
                              });
                            }
                          : null,
                      decoration: const InputDecoration.collapsed(
                        hintText: '',
                      ),
                      // isDense: false,
                      isExpanded: true,

                      value: vm.selectedCountry,
                    ),
                  ),
                ],
              )),
              SmartGaps.gapW10,
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(tr('means_of_payment'),
                        style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                            letterSpacing: 1.0,
                            fontWeight: FontWeight.normal,
                            fontSize: 16)),
                    SmartGaps.gapH7,
                    InputDecorator(
                      decoration: InputDecoration(
                          contentPadding:
                              const EdgeInsets.only(left: 12, right: 12),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context)
                                    .colorScheme
                                    .onSurfaceVariant
                                    .withValues(alpha: 0.4)),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(3)),
                          )),
                      child: DropdownButtonFormField(
                        items: [
                          DropdownMenuItem(
                            value: 'DKK',
                            child: Text('DKK',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyMedium!
                                    .copyWith(fontWeight: FontWeight.w500)),
                          ),
                          // DropdownMenuItem(
                          //   value: 'EUR',
                          //   child: Text('EUR',
                          //       style: Theme.of(context)
                          //           .textTheme
                          //           .bodyMedium!
                          //           .copyWith(fontWeight: FontWeight.w500)),
                          // ),
                        ],

                        onChanged: !accountExists
                            ? (dynamic val) {
                                setState(() {
                                  vm.selectedCurrency = val as String?;
                                });
                              }
                            : null,
                        decoration: const InputDecoration.collapsed(
                          hintText: '',
                        ),
                        // isDense: false,
                        isExpanded: false,
                        value: vm.selectedCurrency,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
          SmartGaps.gapH10,
          CustomTextFieldNew(
            controller: iban,
            labelText:
                'Iban (Last updated: ${vm.payproff != null ? Formatter.formatDateStrings(type: DateFormatType.fullMonthYearDate, dateString: vm.payproff?.statusUpdated) : ''})',
            labelFontSize: 16,
            labelFontWeight: FontWeight.normal,
            prefixWidget: CountryCodePicker(
              onChanged: (value) {
                setState(() {
                  countryCodeSelector = value.code ?? '';
                });
              },
              initialSelection: 'DK',
              favorite: const ['DK'],
              showCountryOnly: true,
              showOnlyCountryWhenClosed: false,
              builder: (p0) {
                if (p0 == null) return const SizedBox.shrink();

                return Container(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Row(mainAxisSize: MainAxisSize.min, children: [
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: Image.asset(
                        p0.flagUri!,
                        package: 'country_code_picker',
                      ),
                    ),
                    SmartGaps.gapW10,
                    Text(p0.code!),
                    const Icon(FeatherIcons.chevronDown),
                  ]),
                );
              },
            ),
          ),
          SmartGaps.gapH30,
          SizedBox(
            height: 60,
            width: double.infinity,
            child: CustomDesignTheme.flatButtonStyle(
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5))),
              backgroundColor: Theme.of(context).colorScheme.secondary,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(tr('submit').toUpperCase(),
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium!
                          .copyWith(color: Colors.white, height: 1)),
                ],
              ),
              onPressed: () async {
                if (!accountExists) {
                  if (formKey.currentState!.validate()) {
                    final vm = context.read<PayproffViewModel>();

                    final account = PostPayproffAccountModel(
                        address: address.text,
                        birthDate: birthday.text.isEmpty
                            ? DateTime.now().toString()
                            : birthday.text,
                        city: town.text,
                        companyName: vm.company!.title,
                        country: vm.selectedCountry,
                        currency: vm.selectedCurrency,
                        cvr: vm.company!.cvr,
                        displayLanguage: vm.selectedLanguage,
                        email: email.text,
                        firstName: firstName.text,
                        lastName: lastName.text,
                        middleName: middleName.text,
                        phoneNumber: int.parse(phoneNumber.text),
                        phoneNumberAreaCode: countryCode.text,
                        socialSecurityNumber:
                            int.parse(cprFormatter.getUnmaskedText()),
                        state: town.text,
                        zipCode: zipcode.text,
                        iban:
                            '$countryCodeSelector${iban.text.replaceAll(' ', '')}');

                    showPayproffDialog(context, account: account);
                  }
                } else {
                  showLoadingDialog(context, loadingKey);

                  await tryCatchWrapper(
                          context: myGlobals.homeScaffoldKey!.currentContext,
                          function: vm.updateWallet(
                              iban:
                                  '$countryCodeSelector${iban.text.replaceAll(' ', '')}'))
                      .then((value) {
                    Navigator.of(loadingKey.currentContext!).pop();
                    if (value!) {
                      showSuccessAnimationDialog(
                          myGlobals.homeScaffoldKey!.currentContext!,
                          value,
                          tr('iban_updated'));
                    }
                  });
                }
              },
            ),
          ),
          SmartGaps.gapH50,
        ],
      );
    });
  }

  // Widget _getForm() {
  //   final _inputDecoration = InputDecoration(
  //       contentPadding: const EdgeInsets.only(left: 10, right: 10),
  //       border: OutlineInputBorder(
  //         borderSide: BorderSide(color: Theme.of(context).colorScheme.primary.withAlpha(100)),
  //       ),
  //       enabledBorder: OutlineInputBorder(
  //         borderSide: BorderSide(color: Theme.of(context).colorScheme.primary.withAlpha(100)),
  //       ),
  //       focusedBorder: OutlineInputBorder(
  //         borderSide: BorderSide(color: Theme.of(context).colorScheme.primary.withAlpha(100)),
  //       ),
  //       hintStyle: Theme.of(context)
  //           .textTheme
  //           .bodyMedium
  //           .copyWith(color: Colors.grey[350]),
  //       hintText: '');

  //   return Form(
  //     key: formKey,
  //     child: Column(
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       children: [
  //         _getLabel(tr('first_name'), isRequired: true),
  //         SizedBox(height: 10),
  //         TextFormField(
  //             textInputAction: TextInputAction.done,
  //             keyboardType: TextInputType.name,
  //             maxLines: null,
  //             controller: firstName,
  //             style: Theme.of(context).textTheme.titleSmall,
  //             decoration: _inputDecoration.copyWith(
  //                 hintText: tr('first_name')),
  //             validator: (value) {
  //               if (value.isEmpty) {
  //                 return tr('please_enter_some_text');
  //               }
  //               return null;
  //             }),
  //         SmartGaps.gapH20,
  //         _getLabel(tr('last_name'), isRequired: true),
  //         SizedBox(height: 10),
  //         TextFormField(
  //             textInputAction: TextInputAction.done,
  //             keyboardType: TextInputType.name,
  //             maxLines: null,
  //             controller: lastName,
  //             style: Theme.of(context).textTheme.titleSmall,
  //             decoration: _inputDecoration.copyWith(
  //                 hintText: tr('last_name')),
  //             validator: (value) {
  //               if (value.isEmpty) {
  //                 return tr('please_enter_some_text');
  //               }
  //               return null;
  //             }),
  //         SmartGaps.gapH20,
  //         _getLabel(tr('middle_name')),
  //         SizedBox(height: 10),
  //         TextFormField(
  //             textInputAction: TextInputAction.done,
  //             keyboardType: TextInputType.name,
  //             maxLines: null,
  //             controller: middleName,
  //             style: Theme.of(context).textTheme.titleSmall,
  //             decoration: _inputDecoration.copyWith(
  //                 hintText: tr('middle_name')),
  //             validator: (value) {
  //               if (value.isEmpty) {
  //                 return tr('please_enter_some_text');
  //               }
  //               return null;
  //             }),
  //         SmartGaps.gapH20,
  //         _getLabel(tr('birthday'), isRequired: true),
  //         SizedBox(height: 10),
  //         DateTimeField(
  //           decoration: InputDecoration(
  //               border: OutlineInputBorder(
  //                 borderSide:
  //                     BorderSide(color: Theme.of(context).colorScheme.primary.withAlpha(100)),
  //               ),
  //               enabledBorder: OutlineInputBorder(
  //                 borderSide:
  //                     BorderSide(color: Theme.of(context).colorScheme.primary.withAlpha(100)),
  //               ),
  //               focusedBorder: OutlineInputBorder(
  //                 borderSide:
  //                     BorderSide(color: Theme.of(context).colorScheme.primary.withAlpha(100)),
  //               ),
  //               filled: true,
  //               fillColor: Colors.transparent),
  //           format: Formatter.dateFormat(),
  //           onShowPicker: (context, currentValue) async {
  //             final date = await showDatePicker(
  //                 context: context,
  //                 firstDate: DateTime(1900),
  //                 initialDate: currentValue ?? DateTime.now(),
  //                 lastDate: DateTime(2100));
  //             if (date != null) {
  //               return date;
  //             } else {
  //               return currentValue;
  //             }
  //           },
  //           autovalidate: false,
  //           validator: (date) =>
  //               date == null ? tr('invalid_date') : null,
  //           initialValue: DateTime.now(),
  //           onChanged: (date) => setState(() {
  //             birthday.text = date.toString();
  //           }),
  //           onSaved: (date) => setState(() {
  //             birthday.text = date.toString();
  //           }),
  //           resetIcon: Icon(
  //             ElementIcons.delete,
  //             size: 18,
  //             color: Theme.of(context).colorScheme.onSurfaceVariant,
  //           ),
  //         ),
  //         SmartGaps.gapH20,
  //         _getLabel(tr('address'), isRequired: true),
  //         SizedBox(height: 10),
  //         TextFormField(
  //             textInputAction: TextInputAction.done,
  //             keyboardType: TextInputType.name,
  //             maxLines: null,
  //             controller: address,
  //             style: Theme.of(context).textTheme.titleSmall,
  //             decoration: _inputDecoration.copyWith(
  //                 hintText: tr('address')),
  //             validator: (value) {
  //               if (value.isEmpty) {
  //                 return tr('please_enter_some_text');
  //               }
  //               return null;
  //             }),
  //         SmartGaps.gapH20,
  //         _getLabel('Zipcode', isRequired: true),
  //         SizedBox(height: 10),
  //         TextFormField(
  //             textInputAction: TextInputAction.done,
  //             keyboardType: TextInputType.name,
  //             maxLines: null,
  //             controller: zipcode,
  //             style: Theme.of(context).textTheme.titleSmall,
  //             decoration: _inputDecoration.copyWith(hintText: 'Zipcode'),
  //             validator: (value) {
  //               if (value.isEmpty) {
  //                 return tr('please_enter_some_text');
  //               }
  //               return null;
  //             }),
  //         SmartGaps.gapH20,
  //         _getLabel(tr('town'), isRequired: true),
  //         SizedBox(height: 10),
  //         TextFormField(
  //             textInputAction: TextInputAction.done,
  //             keyboardType: TextInputType.name,
  //             maxLines: null,
  //             controller: town,
  //             style: Theme.of(context).textTheme.titleSmall,
  //             decoration:
  //                 _inputDecoration.copyWith(hintText: tr('town')),
  //             validator: (value) {
  //               if (value.isEmpty) {
  //                 return tr('please_enter_some_text');
  //               }
  //               return null;
  //             }),
  //         SmartGaps.gapH20,
  //         _getLabel(tr('state')),
  //         SizedBox(height: 10),
  //         TextFormField(
  //             textInputAction: TextInputAction.done,
  //             keyboardType: TextInputType.name,
  //             maxLines: null,
  //             controller: state,
  //             style: Theme.of(context).textTheme.titleSmall,
  //             decoration: _inputDecoration.copyWith(
  //                 hintText: tr('state')),
  //             validator: (value) {
  //               if (value.isEmpty) {
  //                 return tr('please_enter_some_text');
  //               }
  //               return null;
  //             }),
  //         SmartGaps.gapH20,
  //         _getLabel(tr('country'), isRequired: true),
  //         SizedBox(height: 10),
  //         CountryDropdown(),
  //         SmartGaps.gapH20,
  //         _getLabel(tr('email'), isRequired: true),
  //         SizedBox(height: 10),
  //         TextFormField(
  //             textInputAction: TextInputAction.done,
  //             keyboardType: TextInputType.emailAddress,
  //             maxLines: null,
  //             controller: email,
  //             style: Theme.of(context).textTheme.titleSmall,
  //             decoration: _inputDecoration.copyWith(
  //                 hintText: tr('email')),
  //             validator: (value) {
  //               if (value.isEmpty) {
  //                 return tr('please_enter_some_text');
  //               }
  //               return null;
  //             }),
  //         SmartGaps.gapH20,
  //         _getLabel(tr('phone_number'), isRequired: true),
  //         SizedBox(height: 10),
  //         TextFormField(
  //             textInputAction: TextInputAction.done,
  //             keyboardType: TextInputType.emailAddress,
  //             maxLines: null,
  //             controller: phoneNumber,
  //             style: Theme.of(context).textTheme.titleSmall,
  //             decoration: _inputDecoration.copyWith(
  //                 hintText: tr('phone_number')),
  //             validator: (value) {
  //               if (value.isEmpty) {
  //                 return tr('please_enter_some_text');
  //               }
  //               return null;
  //             }),
  //         SmartGaps.gapH20,
  //         _getLabel(tr('telephone_country_code'),
  //             isRequired: true),
  //         SizedBox(height: 10),
  //         TextFormField(
  //             textInputAction: TextInputAction.done,
  //             keyboardType: TextInputType.emailAddress,
  //             maxLines: null,
  //             controller: countryCode,
  //             style: Theme.of(context).textTheme.titleSmall,
  //             decoration: _inputDecoration.copyWith(
  //                 hintText: tr('telephone_country_code')),
  //             validator: (value) {
  //               if (value.isEmpty) {
  //                 return tr('please_enter_some_text');
  //               }
  //               return null;
  //             }),
  //         SmartGaps.gapH20,
  //         _getLabel(tr('social_security_number'),
  //             isRequired: true),
  //         SizedBox(height: 10),
  //         TextFormField(
  //             textInputAction: TextInputAction.done,
  //             keyboardType: TextInputType.emailAddress,
  //             maxLines: null,
  //             controller: cprNumber,
  //             style: Theme.of(context).textTheme.titleSmall,
  //             decoration: _inputDecoration.copyWith(
  //                 hintText: tr('social_security_number')),
  //             validator: (value) {
  //               if (value.isEmpty) {
  //                 return tr('please_enter_some_text');
  //               }
  //               return null;
  //             }),
  //         SmartGaps.gapH20,
  //         _getLabel(tr('language')),
  //         SizedBox(height: 10),
  //         PayproffLanguageDropdown(),
  //         SmartGaps.gapH20,
  //         _getLabel(tr('means_of_payment')),
  //         SizedBox(height: 10),
  //         PaymentDropdown(),
  //         SmartGaps.gapH20,
  //         Container(
  //           height: 60,
  //           width: double.infinity,
  //           decoration: BoxDecoration(
  //               border: Border.all(color: Theme.of(context).colorScheme.primary.withAlpha(100))),
  //           child: CustomDesignTheme.flatButtonStyle(
  //             color: Colors.white,
  //             child: Row(
  //               mainAxisAlignment: MainAxisAlignment.center,
  //               children: <Widget>[
  //                 Text(tr('dicontinue'),
  //                     style: Theme.of(context)
  //                         .textTheme
  //                         .headlineSmall
  //                         .copyWith(color: Theme.of(context).colorScheme.primary, height: 1)),
  //               ],
  //             ),
  //             onPressed: () {},
  //           ),
  //         ),
  //         SmartGaps.gapH20,
  //         Container(
  //           height: 60,
  //           width: double.infinity,
  //           child: CustomDesignTheme.flatButtonStyle(
  //             color: Theme.of(context).colorScheme.secondary,
  //             child: Row(
  //               mainAxisAlignment: MainAxisAlignment.center,
  //               children: <Widget>[
  //                 Text(tr('next'),
  //                     style: Theme.of(context)
  //                         .textTheme
  //                         .headlineSmall
  //                         .copyWith(color: Colors.white, height: 1)),
  //               ],
  //             ),
  //             onPressed: () {
  //               if (formKey.currentState.validate()) {
  //                 final vm =
  //                     Provider.of<PayproffViewModel>(context, listen: false);

  //                 final account = PostPayproffAccountModel(
  //                   address: address.text,
  //                   birthDate: birthday.text.isEmpty
  //                       ? DateTime.now().toString()
  //                       : birthday.text,
  //                   city: town.text,
  //                   companyName: vm.company.title,
  //                   country: vm.selectedCountry,
  //                   currency: vm.selectedCurrency,
  //                   cvr: vm.company.cvr,
  //                   displayLanguage: vm.selectedLanguage,
  //                   email: email.text,
  //                   firstName: firstName.text,
  //                   lastName: lastName.text,
  //                   middleName: middleName.text,
  //                   phoneNumber: int.parse(phoneNumber.text),
  //                   phoneNumberAreaCode: countryCode.text,
  //                   socialSecurityNumber: int.parse(cprNumber.text),
  //                   state: town.text,
  //                   zipCode: zipcode.text,
  //                 );

  //                 showPayproffDialog(context, account: account);
  //               }
  //             },
  //           ),
  //         ),
  //         SmartGaps.gapH20,
  //       ],
  //     ),
  //   );
  // }

  // Widget _getLabel(String label, {bool isRequired = false}) {
  //   return RichText(
  //     maxLines: 3,
  //     text: TextSpan(
  //       text: isRequired ? '* ' : '',
  //       style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
  //       children: <TextSpan>[
  //         TextSpan(
  //           text: label,
  //           style: Theme.of(context)
  //               .textTheme
  //               .titleSmall
  //               .copyWith(height: 1.2, color: Colors.grey),
  //         ),
  //       ],
  //     ),
  //   );
  // }
}
