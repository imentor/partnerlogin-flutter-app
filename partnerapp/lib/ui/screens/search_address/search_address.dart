import 'dart:async';

import 'package:Haandvaerker.dk/model/autocomplete/autocomplete_model.dart';
import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

Future<Address?> addAddress(
    {required BuildContext context, String? initialAddress}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: const EdgeInsets.all(20),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              GestureDetector(
                  onTap: () {
                    Navigator.of(dialogContext).pop();
                  },
                  child: const Icon(Icons.close, size: 18))
            ],
          ),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
                child: SearchAddress(
              initialAddress: initialAddress,
            )),
          ),
        );
      });
}

class SearchAddress extends StatefulWidget {
  const SearchAddress({super.key, this.initialAddress});

  final String? initialAddress;

  @override
  SearchAddressState createState() => SearchAddressState();
}

class SearchAddressState extends State<SearchAddress> {
  static const String autocompleteUrl = 'https://dawa.aws.dk/autocomplete/?q=';
  static const int debounceDurationMilliseconds = 1500;

  List<AutoCompleteAddress> list = [];
  Timer? _debounce;
  bool isLoading = false;

  @override
  void initState() {
    getInitialAddress();
    super.initState();
  }

  Future<void> getInitialAddress() async {
    setState(() {
      isLoading = true;
    });

    try {
      final request = await Dio().get(
          '$autocompleteUrl${widget.initialAddress}&startfra=adresse&fuzzy=true');

      if ((request.statusCode ?? 400) == 200) {
        setState(() {
          isLoading = false;
          list = AutoCompleteAddress.fromCollection(request.data);
        });
      } else {
        setState(() {
          isLoading = false;
          list = [];
        });
      }
    } catch (e) {
      setState(() {
        isLoading = false;
        list = [];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomTextFieldFormBuilder(
          name: 'search_address',
          labelText: tr('address'),
          initialValue: widget.initialAddress,
          onChanged: (p0) async {
            if (p0 != null && p0.isNotEmpty) {
              if (_debounce?.isActive ?? false) {
                _debounce?.cancel();
              }
              _debounce = Timer(
                const Duration(milliseconds: debounceDurationMilliseconds),
                () async {
                  setState(() {
                    isLoading = true;
                  });

                  try {
                    final request = await Dio()
                        .get('$autocompleteUrl$p0&startfra=adresse&fuzzy=true');

                    if ((request.statusCode ?? 400) == 200) {
                      setState(() {
                        isLoading = false;
                        list = AutoCompleteAddress.fromCollection(request.data);
                      });
                    } else {
                      setState(() {
                        isLoading = false;
                        list = [];
                      });
                    }
                  } catch (e) {
                    // Handle network request error
                    setState(() {
                      isLoading = false;
                      list = [];
                    });
                  }
                },
              );
            } else {
              setState(() {
                isLoading = false;
                list = [];
              });
            }
          },
        ),
        if (isLoading) ...[
          SmartGaps.gapH10,
          CircularProgressIndicator(
            color: PartnerAppColors.blue.withValues(alpha: .3),
          )
        ] else ...[
          if (list.isNotEmpty) ...[
            Container(
              height: 150,
              decoration: BoxDecoration(
                border: Border(
                    left: BorderSide(
                        color: PartnerAppColors.blue.withValues(alpha: .3)),
                    right: BorderSide(
                        color: PartnerAppColors.blue.withValues(alpha: .3)),
                    bottom: BorderSide(
                        color: PartnerAppColors.blue.withValues(alpha: .3)),
                    top: const BorderSide(color: Colors.white)),
              ),
              child: ListView.separated(
                  itemCount: list.length,
                  padding: const EdgeInsets.all(10),
                  shrinkWrap: true,
                  separatorBuilder: (context, index) {
                    return const Divider();
                  },
                  itemBuilder: (context, index) {
                    return ListTile(
                      onTap: () async {
                        Navigator.of(context).pop(Address(
                            addressId: list[index].data!.id,
                            adgangsAddressId:
                                list[index].data!.adgangsadresseid!,
                            address: list[index].tekst!,
                            zip: list[index].data!.postnr!,
                            latitude: '${list[index].data!.y!}',
                            longitude: '${list[index].data!.x!}'));
                      },
                      contentPadding: EdgeInsets.zero,
                      title: Text(list[index].tekst ?? 'N/A'),
                    );
                  }),
            )
          ]
        ],
      ],
    );
  }
}
