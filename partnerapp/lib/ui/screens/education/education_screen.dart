import 'dart:developer';
import 'dart:io';

import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/recommendation/industry_category_dropdown.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/education/education_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/recommendation_read_answer_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:collection/collection.dart' show IterableExtension;
import 'package:dotted_border/dotted_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:show_up_animation/show_up_animation.dart';

class EducationScreen extends StatefulWidget {
  const EducationScreen({super.key});

  @override
  EducationScreenState createState() => EducationScreenState();
}

class EducationScreenState extends State<EducationScreen> {
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  late TextEditingController _nameController;
  late FocusNode _nameFocus;

  int? _chosenIndustry;
  File? _file;
  bool initiallyLoaded = true;

  @override
  void initState() {
    _nameController = TextEditingController();
    _nameFocus = FocusNode();
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(
      (_) async {
        final vm = context.read<EducationViewModel>();
        final newsFeedVm = context.read<NewsFeedViewModel>();
        final appDrawerVm = context.read<AppDrawerViewModel>();
        const dynamicStoryPage = 'uddanelseBevis';
        final dynamicStoryEnable =
            (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

        if (dynamicStoryEnable) {
          await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: Future.wait([
              vm.getCertificates(),
              vm.getIndustries(),
              newsFeedVm.getDynamicStory(page: dynamicStoryPage),
            ]),
          ).whenComplete(() {
            if (!mounted) return;

            newsFeedVm.dynamicStoryFunction(
                dynamicStoryPage: dynamicStoryPage,
                dynamicStoryEnable: dynamicStoryEnable,
                pageContext: context);
          });
        } else {
          await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: Future.wait([
              vm.getCertificates(),
              vm.getIndustries(),
            ]),
          );
        }
      },
    );
  }

  @override
  void dispose() {
    _nameController.dispose();
    _nameFocus.dispose();
    super.dispose();
  }

  void onDelete({dynamic value}) async {
    final vm = context.read<EducationViewModel>();

    final result = await showOkCancelAlertDialog(
        title: tr('delete'),
        context: context,
        message: tr('are_you_sure'),
        okLabel: tr('delete'),
        cancelLabel: tr('cancel'));

    if (result == OkCancelResult.ok && mounted) {
      showLoadingDialog(context, loadingKey);

      final deleteResponse = await tryCatchWrapper(
          context: context,
          function: vm.deleteCertificate(certificateId: value));

      if (!mounted) return;

      if ((deleteResponse ?? false)) {
        await tryCatchWrapper(context: context, function: vm.getCertificates());
        if (!mounted) return;

        Navigator.of(loadingKey.currentContext!).pop();
        await showSuccessAnimationDialog(
          context,
          deleteResponse!,
          tr('delete_certificate_success'),
        );
      } else {
        Navigator.of(loadingKey.currentContext!).pop();
        await showSuccessAnimationDialog(
          context,
          deleteResponse!,
          tr('delete_certificate_failed'),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(tr('education_proof'),
                  style: Theme.of(context).textTheme.headlineLarge),
              SmartGaps.gapH10,
              Text(tr('education_desc'),
                  style: Theme.of(context).textTheme.bodyMedium),
              SmartGaps.gapH20,
              _EducationList(
                onDelete: (value) => onDelete(value: value),
              ),
              SmartGaps.gapH20,
              _getForm(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _getForm() {
    return Form(
        key: formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextFormField(
                controller: _nameController,
                focusNode: _nameFocus,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    filled: true,
                    fillColor: Colors.grey[200],
                    hintStyle: Theme.of(context)
                        .textTheme
                        .bodyMedium!
                        .copyWith(fontWeight: FontWeight.w500),
                    hintText: tr('name')),
                validator: (value) {
                  if (value!.isEmpty) {
                    return tr('please_enter_some_text');
                  }
                  return null;
                }),
            IndustryCategoryDropdown(
              showLabel: false,
              type: CustomDropDownType.simple,
              initialValue: _chosenIndustry,
              onIndustryChanged: (industryId) {
                setState(() {
                  _chosenIndustry = industryId;
                });
              },
            ),
            if (_chosenIndustry == null && !initiallyLoaded)
              ShowUpAnimation(
                  //delayStart: Duration(seconds: 1),
                  animationDuration: const Duration(milliseconds: 300),
                  curve: Curves.ease,
                  direction: Direction.vertical,
                  offset: -0.5,
                  child: Container(
                    width: double.infinity,
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Text(
                      tr('please_choose_a_valid_industry'),
                      textAlign: TextAlign.start,
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          color: Colors.red,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w200),
                    ),
                  )),
            _file != null
                ? ListTile(
                    title: Text(Formatter.getBaseName(_file!),
                        style: Theme.of(context).textTheme.bodyMedium),
                    trailing: IconButton(
                        icon:
                            const Icon(ElementIcons.delete, color: Colors.red),
                        onPressed: () {
                          setState(() {
                            _file = null;
                          });
                        }),
                  )
                : DottedBorder(
                    color: Theme.of(context).colorScheme.primary,
                    dashPattern: const [2, 4, 2, 4],
                    strokeWidth: 2,
                    strokeCap: StrokeCap.round,
                    borderType: BorderType.RRect,
                    child: Container(
                      padding: const EdgeInsets.all(10),
                      height: 80,
                      width: double.infinity,
                      child: CustomDesignTheme.flatButtonStyle(
                        onPressed: () async {
                          await vaelgFil();
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Image.asset(ImagePaths.picturePrimaryColor,
                                height: 50, width: 50),
                            SmartGaps.gapW10,
                            Flexible(
                              child: RichText(
                                maxLines: 2,
                                text: TextSpan(
                                  text: '',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: tr('attach_diploma'),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            fontSize: 14,
                                            fontFamily: 'Gibson')),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
            if (_file == null && !initiallyLoaded)
              ShowUpAnimation(
                  //delayStart: Duration(seconds: 1),
                  animationDuration: const Duration(milliseconds: 300),
                  curve: Curves.ease,
                  direction: Direction.vertical,
                  offset: -0.5,
                  child: Container(
                    width: double.infinity,
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Text(
                      tr('education_proof_is_required'),
                      textAlign: TextAlign.start,
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          color: Colors.red,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w200),
                    ),
                  )),
            SmartGaps.gapH20,
            SizedBox(
              width: double.maxFinite,
              height: 60,
              child: CustomDesignTheme.flatButtonStyle(
                disabledBackgroundColor: Colors.grey,
                backgroundColor: Theme.of(context).colorScheme.secondary,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(0),
                ),
                onPressed: () async {
                  if (initiallyLoaded) {
                    setState(() {
                      initiallyLoaded = false;
                    });
                  }
                  if (formKey.currentState!.validate() &&
                      _chosenIndustry != null &&
                      _file != null) {
                    final vm = context.read<EducationViewModel>();
                    final industry = context
                        .read<RecommendationReadAnswerViewModel>()
                        .industries
                        .singleWhereOrNull(
                            (e) => e.brancheId == _chosenIndustry)!
                        .brancheId;

                    showLoadingDialog(context, loadingKey);
                    final addResponse = await vm.addCertificate(
                      name: _nameController.text.trim(),
                      industryId: industry!,
                      branch: vm.industries
                              .firstWhere(
                                (element) => element.brancheId == industry,
                                orElse: () => FullIndustry.defaults(),
                              )
                              .brancheDa ??
                          '',
                      file: _file!,
                    );

                    if (addResponse) {
                      _nameFocus.unfocus();
                      await Future.delayed(const Duration(milliseconds: 500));
                      Navigator.of(loadingKey.currentContext!).pop();
                      await vm.getCertificates();
                      clearData();
                      if (mounted) {
                        await showSuccessAnimationDialog(
                          context,
                          addResponse,
                          tr('add_certificate_success'),
                        );
                      }
                    } else {
                      Navigator.of(loadingKey.currentContext!).pop();
                      if (mounted) {
                        await showSuccessAnimationDialog(
                          context,
                          addResponse,
                          tr('add_certificate_failed'),
                        );
                      }
                    }
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(tr('send'),
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(color: Colors.white, height: 1)),
                    SmartGaps.gapW10,
                    const Icon(
                      ElementIcons.right,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }

  clearData() {
    setState(() {
      _chosenIndustry = null;
      _nameController.clear();
      _file = null;
      initiallyLoaded = true;
    });
  }

  Future<File?> vaelgFil() async {
    FocusScope.of(context).requestFocus(FocusNode());

    final model = context.read<EducationViewModel>();
    var status = await Permission.storage.status;

    if (!status.isGranted) {
      await requestPermission(Permission.storage);
    }

    if (mounted) {
      var filePath = await _openFileExplorer(context, model);

      for (var item in filePath) {
        final tempFile = await _readFileByte(item!);
        setState(() {
          _file = tempFile;
        });

        // final _snackBar =
        if (mounted) {
          SnackBar(
            content: Text(tr('successfully_uploaded_file'),
                style: const TextStyle(color: Colors.white)),
          );
        }
      }
    }

    return _file;
  }

  Future<File> _readFileByte(String filePath) async {
    Uri myUri = Uri.parse(filePath);
    File file = File.fromUri(myUri);
    return file;
  }

  Future<List<String?>> _openFileExplorer(
      BuildContext context, EducationViewModel model) async {
    try {
      var paths = await FilePicker.platform.pickFiles(
        allowMultiple: true,
        type: FileType.custom,
        allowedExtensions:
            model.userExtensions.map((e) => e.toString()).toList(),
      );

      return paths?.paths ?? [];
    } on PlatformException catch (e) {
      log("Unsupported operation$e");
      rethrow;
    }
  }

  Future<void> requestPermission(Permission permission) async {
    // final status =
    await permission.request();
  }
}

class _EducationList extends StatelessWidget {
  const _EducationList({this.onDelete});

  final Function(dynamic)? onDelete;

  @override
  Widget build(BuildContext context) {
    return Consumer<EducationViewModel>(
      builder: (_, vm, widget) {
        if (vm.busy) return Center(child: loader());

        return Column(
          children: [
            ListTile(
              title: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 1,
                    child: Text(tr('name'),
                        style: Theme.of(context).textTheme.headlineSmall),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(tr('education'),
                        style: Theme.of(context).textTheme.headlineSmall),
                  ),
                ],
              ),
              trailing: const IconButton(
                icon: Icon(ElementIcons.delete, color: Colors.transparent),
                onPressed: null,
              ),
            ),
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: vm.currentEducationProofs.length,
              itemBuilder: (context, index) {
                final item = vm.currentEducationProofs.elementAt(index);
                return ListTile(
                  title: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Text(
                          '${item.description}',
                          style: Theme.of(context).textTheme.titleSmall,
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(item.branche!,
                            style: Theme.of(context).textTheme.titleSmall),
                      ),
                    ],
                  ),
                  trailing: IconButton(
                    icon: const Icon(ElementIcons.delete, color: Colors.red),
                    onPressed: () {
                      onDelete!(item.id);
                    },
                  ),
                );
              },
            ),
          ],
        );
      },
    );
  }
}
