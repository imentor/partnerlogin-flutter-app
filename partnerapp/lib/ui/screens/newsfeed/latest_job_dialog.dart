import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

Future<int?> latestJobDialog(
    {required BuildContext context, bool fromCustomer = false}) {
  return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: const EdgeInsets.all(20),
          title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            GestureDetector(
                onTap: () {
                  Navigator.of(dialogContext).pop();
                },
                child: const Icon(Icons.close))
          ]),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: LatestJobDialog(fromCustomer: fromCustomer),
            ),
          ),
        );
      });
}

class LatestJobDialog extends StatelessWidget {
  const LatestJobDialog({super.key, this.fromCustomer = false});
  final bool fromCustomer;

  @override
  Widget build(BuildContext context) {
    return Consumer<NewsFeedViewModel>(builder: (_, newsfeedVm, __) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.center,
            child: Text(
              'Kunder venter på tilbud',
              style: Theme.of(context)
                  .textTheme
                  .titleMedium!
                  .copyWith(fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          const Divider(
            color: PartnerAppColors.darkBlue,
          ),
          const SizedBox(
            height: 10,
          ),
          RichText(
            text: TextSpan(
                text: 'Hej ',
                style: Theme.of(context)
                    .textTheme
                    .titleSmall!
                    .copyWith(color: PartnerAppColors.spanishGrey),
                children: [
                  TextSpan(
                    text:
                        '${context.read<UserViewModel>().userModel?.user?.name ?? ''}, ',
                    style: Theme.of(context)
                        .textTheme
                        .titleSmall!
                        .copyWith(fontWeight: FontWeight.bold),
                  ),
                  TextSpan(
                    text:
                        '\nDu har kunder, der venter på deres tilbud. Her er listen over kunder, der i øjeblikket venter på svar:',
                    style: Theme.of(context)
                        .textTheme
                        .titleSmall!
                        .copyWith(color: PartnerAppColors.spanishGrey),
                  ),
                ]),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                border: Border.all(
                    color: PartnerAppColors.darkBlue.withValues(alpha: .3)),
                borderRadius: BorderRadius.circular(5)),
            child: ListView.separated(
              separatorBuilder: (context, index) {
                return Container(
                  color: const Color(0xffF9F9F9),
                  child: const Divider(
                    color: PartnerAppColors.darkBlue,
                  ),
                );
              },
              itemBuilder: (context, index) {
                final job = newsfeedVm.latestJobFeedForPopup[index];

                Color color = PartnerAppColors.darkBlue;
                String offerDueDate = '';

                if ((job.colorCode ?? '').isNotEmpty) {
                  color = Color(int.parse(
                      '0xff${(job.colorCode ?? '').replaceAll(RegExp('[^A-Za-z0-9]'), '')}'));
                }
                if ((job.deadlineOffer ?? '').isNotEmpty) {
                  offerDueDate = (job.deadlineOffer ?? '')
                      .replaceAll(RegExp('[^A-Za-z0-9]'), '.');
                }

                return Container(
                  padding: const EdgeInsets.all(20),
                  color: const Color(0xffF9F9F9),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        job.contactName ?? '',
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        job.projectName ?? '',
                        style: Theme.of(context).textTheme.titleSmall,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      RichText(
                        text: TextSpan(
                          text: tr('due_date'),
                          style:
                              Theme.of(context).textTheme.titleSmall!.copyWith(
                                    color: PartnerAppColors.spanishGrey,
                                  ),
                          children: [
                            TextSpan(
                              text: ' ${tr('offer_due_date')} $offerDueDate',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                    color: color,
                                  ),
                            )
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextButton(
                          style: TextButton.styleFrom(
                              backgroundColor: PartnerAppColors.malachite,
                              fixedSize:
                                  Size(MediaQuery.of(context).size.width, 55)),
                          onPressed: () {
                            if (fromCustomer) {
                              Navigator.of(context).pop(job.jobId);
                            } else {
                              Navigator.of(context).pop();
                              changeDrawerRoute(Routes.yourClient, arguments: {
                                'jobId': job.jobId,
                              });
                            }
                          },
                          child: Text(
                            'Afgiv tilbud nu',
                            style: Theme.of(context)
                                .textTheme
                                .titleSmall!
                                .copyWith(
                                  color: Colors.white,
                                ),
                          ))
                    ],
                  ),
                );
              },
              itemCount: (newsfeedVm.latestJobFeedForPopup.length == 1)
                  ? 1
                  : newsfeedVm.latestJobFeedForPopup.length.clamp(1, 2),
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            'Sørg for rettidige svar for at opretholde kundetilfredshed',
            style: Theme.of(context)
                .textTheme
                .titleSmall!
                .copyWith(color: PartnerAppColors.spanishGrey),
          ),
        ],
      );
    });
  }
}
