import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/launcher.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';

class MembersBenefitsVideo extends StatefulWidget {
  const MembersBenefitsVideo({super.key});

  @override
  State<MembersBenefitsVideo> createState() => _MembersBenefitsVideoState();
}

class _MembersBenefitsVideoState extends State<MembersBenefitsVideo> {
  late VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller =
        VideoPlayerController.asset(VideoPaths.membershipBenefitsBanner)
          ..initialize().then((_) {
            setState(() {});
          })
          ..play()
          ..setLooping(false)
          ..setVolume(0);

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      final newsfeedVm = context.read<NewsFeedViewModel>();

      _controller.addListener(() async {
        if (_controller.value.position >= _controller.value.duration) {
          if (newsfeedVm.membershipBannerWatchCount < 1) {
            newsfeedVm.videoBannerTracking(type: 'view');
          }

          newsfeedVm.membershipBannerWatchCount += 1;
          _controller.seekTo(Duration.zero);
          _controller.play();
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Medlemsfordele',
            style: Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(fontWeight: FontWeight.normal),
          ),
          const SizedBox(
            height: 10,
          ),
          InkWell(
            onTap: () {
              context
                  .read<NewsFeedViewModel>()
                  .videoBannerTracking(type: 'click');
              Launcher()
                  .launchUrlLocal('https://ricogruppen.no/da/kampanjedanmark/');
            },
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 120,
              child: VideoPlayer(_controller),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
