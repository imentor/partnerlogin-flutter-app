import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/screens/newsfeed/components/onboarding_video_popup_dialog.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:timeline_tile/timeline_tile.dart';

class NewsFeedOnboarding extends StatefulWidget {
  const NewsFeedOnboarding({super.key});

  @override
  State<NewsFeedOnboarding> createState() => _NewsFeedOnboardingState();
}

class _NewsFeedOnboardingState extends State<NewsFeedOnboarding> {
  @override
  Widget build(BuildContext context) {
    final newsfeedVm = context.read<NewsFeedViewModel>();
    final isDk = context.locale.languageCode == 'da';

    return Card(
      margin: const EdgeInsets.symmetric(horizontal: 20),
      child: Skeletonizer(
        enabled: newsfeedVm.busy,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(tr('welcome_onboard'),
                      style: Theme.of(context).textTheme.titleLarge!.copyWith(
                          color:
                              Theme.of(context).colorScheme.onSurfaceVariant)),
                  SmartGaps.gapH10,
                  Text(
                    tr('startup_checklist'),
                    style: Theme.of(context).textTheme.titleSmall!.copyWith(
                        color: Theme.of(context).colorScheme.onSurfaceVariant),
                  ),
                  SmartGaps.gapH20,
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Flexible(
                        child: Center(
                          child: LinearPercentIndicator(
                            percent:
                                (newsfeedVm.onboardingData?.percentage ?? 0) /
                                    100,
                            animation: true,
                            lineHeight: 10,
                            padding: EdgeInsets.zero,
                            barRadius: const Radius.circular(8),
                            backgroundColor: PartnerAppColors.spanishGrey
                                .withValues(alpha: .4),
                            progressColor: PartnerAppColors.malachite,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            bottom: newsfeedVm.busy ? 6 : 0,
                            top: newsfeedVm.busy ? 0 : 1),
                        child: Text(
                            '${newsfeedVm.onboardingData?.percentage ?? 0}%',
                            style: Theme.of(context)
                                .textTheme
                                .titleSmall!
                                .copyWith(
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onSurfaceVariant)),
                      ),
                    ],
                  ),
                  SmartGaps.gapH20,
                  SmartGaps.gapH5,
                  ListView.builder(
                      itemCount:
                          (newsfeedVm.onboardingData?.steps ?? []).length,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        final onboardingStep =
                            (newsfeedVm.onboardingData?.steps ?? [])[index];

                        return SizedBox(
                          height: 80,
                          width: double.infinity,
                          child: TimelineTile(
                            alignment: TimelineAlign.start,
                            isFirst: index == 0,
                            isLast: ((newsfeedVm.onboardingData?.steps ?? [])
                                        .length -
                                    1) ==
                                index,
                            indicatorStyle: IndicatorStyle(
                              width: 40,
                              height: 40,
                              indicatorXY: 0,
                              indicator: DecoratedBox(
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    color: (onboardingStep.done ?? false)
                                        ? PartnerAppColors.malachite
                                        : PartnerAppColors.spanishGrey,
                                  ),
                                  color: (onboardingStep.done ?? false)
                                      ? PartnerAppColors.malachite
                                      : Colors.white,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: 8.5, right: 8, left: 8, top: 7.5),
                                  child: newsfeedVm.isUnlockedList[index]
                                      ? ((onboardingStep.done ?? false)
                                          ? const Icon(
                                              FeatherIcons.check,
                                              color: Colors.white,
                                            )
                                          : Container())
                                      : SvgPicture.asset(
                                          'assets/images/12_Lock.svg',
                                          width: 15,
                                          height: 15,
                                          colorFilter: const ColorFilter.mode(
                                            PartnerAppColors.spanishGrey,
                                            BlendMode.srcIn,
                                          ),
                                        ),
                                ),
                              ),
                            ),
                            beforeLineStyle: const LineStyle(
                              thickness: 1.0,
                              color: PartnerAppColors.spanishGrey,
                            ),
                            endChild: InkWell(
                              onTap: () {
                                final videoUrl = onboardingStep.videoUrl;
                                final duration = onboardingStep.duration ?? 12;

                                if (newsfeedVm.isUnlockedList[index] == true) {
                                  if (videoUrl != null && videoUrl.isNotEmpty) {
                                    showOnboardingVideoPopupDialog(
                                        context: context,
                                        videoUrl: videoUrl,
                                        duration: duration,
                                        stepIndex: index);
                                  }
                                }
                              },
                              child: Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          SmartGaps.gapH5,
                                          Text(
                                            isDk
                                                ? (onboardingStep.titleDk ?? '')
                                                : (onboardingStep.titleEn ??
                                                    ''),
                                            style: Theme.of(context)
                                                .textTheme
                                                .titleSmall!
                                                .copyWith(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .onSurfaceVariant,
                                                    fontWeight:
                                                        FontWeight.bold),
                                          ),
                                          Text(
                                            isDk
                                                ? (onboardingStep
                                                        .descriptionDk ??
                                                    '')
                                                : (onboardingStep
                                                        .descriptionEn ??
                                                    ''),
                                            style: Theme.of(context)
                                                .textTheme
                                                .titleSmall!
                                                .copyWith(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .onSurfaceVariant),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Container(
                                          margin:
                                              const EdgeInsets.only(left: 10),
                                          padding: const EdgeInsets.only(
                                              bottom: 30.0),
                                          child: const Icon(
                                            Icons.arrow_forward,
                                            color: PartnerAppColors.spanishGrey,
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      })
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
