import 'package:Haandvaerker.dk/model/onboarding_data_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';
import 'package:story_view/story_view.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

Future<void> showOnboardingVideoPopupDialog({
  required BuildContext context,
  required String videoUrl,
  required int duration,
  required int stepIndex,
}) async {
  return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return Dialog.fullscreen(
          child: OnboardingVideoPopupDialog(
            videoUrl: videoUrl,
            duration: duration,
            stepIndex: stepIndex,
          ),
        );
      });
}

class OnboardingVideoPopupDialog extends StatefulWidget {
  const OnboardingVideoPopupDialog(
      {super.key,
      required this.stepIndex,
      required this.videoUrl,
      required this.duration});
  final String videoUrl;
  final int duration;
  final int stepIndex;

  @override
  State<OnboardingVideoPopupDialog> createState() =>
      _OnboardingVideoPopupDialogState();
}

class _OnboardingVideoPopupDialogState
    extends State<OnboardingVideoPopupDialog> {
  late StoryController storyController;
  late YoutubePlayerController videoController;
  late List<OnboardingStepsModel> onboardingStepsList;
  late List<StoryItem> onboardingVideoItems;

  @override
  void initState() {
    storyController = StoryController();
    videoController = YoutubePlayerController(initialVideoId: '');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final newsFeedVm = context.read<NewsFeedViewModel>();

    return Stack(
      children: <Widget>[
        StoryView(
          storyItems: [
            newsFeedVm.setOnboardingVideoList(
              videoController: videoController,
              storyController: storyController,
              videoUrl: widget.videoUrl,
              duration: widget.duration,
              stepIndex: widget.stepIndex,
            ),
          ],
          controller: storyController,
          repeat: true,
        ),
        Positioned(
          top: 25.0,
          right: 15.0,
          child: InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Container(
              height: 30,
              width: 30,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: PartnerAppColors.grey1,
              ),
              child: const Center(
                child: Icon(
                  FeatherIcons.x,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
