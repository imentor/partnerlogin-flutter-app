import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class NewsFeedCard extends StatelessWidget {
  const NewsFeedCard({
    super.key,
    required this.count,
    this.subText,
    required this.mainText,
    this.callbackText,
    this.onPressedCallback,
  });

  final int count;
  final String? subText;
  final String mainText;
  final String? callbackText;
  final VoidCallback? onPressedCallback;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
        side: const BorderSide(
          color: Colors.black12,
          width: 1.0,
        ),
      ),
      child: SizedBox(
        height: 120,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            //

            Text(
              count.toString(),
              style: GoogleFonts.notoSans(
                textStyle: const TextStyle(
                  color: PartnerAppColors.darkBlue,
                  fontSize: 24,
                  fontWeight: FontWeight.w600,
                  height: 1.5,
                ),
              ),
            ),

            SmartGaps.gapH10,

            Text(
              subText ?? '',
              style: GoogleFonts.notoSans(
                textStyle: const TextStyle(
                  color: Color(0xff707070),
                  fontSize: 10,
                  fontWeight: FontWeight.w400,
                  height: 1.4,
                ),
              ),
            ),

            Text(
              mainText,
              style: GoogleFonts.notoSans(
                textStyle: const TextStyle(
                  color: Colors.black,
                  fontSize: 10,
                  fontWeight: FontWeight.w600,
                  height: 1.4,
                ),
              ),
            ),

            SmartGaps.gapH5,

            if (callbackText != null)
              GestureDetector(
                onTap: onPressedCallback,
                child: Text(
                  callbackText!,
                  style: GoogleFonts.notoSans(
                    textStyle: const TextStyle(
                      color: Color(0xff0044CC),
                      fontSize: 10,
                      fontWeight: FontWeight.w400,
                      height: 1.4,
                    ),
                  ),
                ),
              ),
            //
          ],
        ),
      ),
    );
  }
}
