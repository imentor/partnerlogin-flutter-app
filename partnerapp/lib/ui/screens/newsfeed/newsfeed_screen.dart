import 'dart:async';

import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/newsfeed/components/newsfeed_card.dart';
import 'package:Haandvaerker.dk/ui/screens/newsfeed/components/newsfeed_onboarding.dart';
import 'package:Haandvaerker.dk/ui/screens/newsfeed/members_benefits_video.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/kanikke_settings_response_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/widgets_recommendations_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';

class NewsfeedScreen extends StatefulWidget {
  const NewsfeedScreen({super.key});
  @override
  NewsfeedScreenState createState() => NewsfeedScreenState();
}

class NewsfeedScreenState extends State<NewsfeedScreen> {
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final newsFeedVm = context.read<NewsFeedViewModel>();
      final appDrawerVm = context.read<AppDrawerViewModel>();
      const dynamicStoryPage = 'newsfeed';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      if (newsFeedVm.afterInitNewsFeed) {
        if (dynamicStoryEnable) {
          await Future.value(newsFeedVm.getDynamicStory(page: dynamicStoryPage))
              .whenComplete(() {
            if (!mounted) return;

            newsFeedVm.dynamicStoryFunction(
                dynamicStoryPage: dynamicStoryPage,
                dynamicStoryEnable: dynamicStoryEnable,
                pageContext: context);
          });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final kanIkkeVm = context.read<KanIkkeSettingsResponseViewModel>();

    return Consumer<NewsFeedViewModel>(builder: (_, vm, __) {
      return Scaffold(
        floatingActionButton: FloatingActionButton.extended(
          backgroundColor: Colors.white,
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
            side: BorderSide(
              color: PartnerAppColors.darkBlue.withValues(alpha: .3),
            ),
          ),
          icon: SvgPicture.asset(
            SvgIcons.chatIconUrl,
            height: 25.0,
            width: 25.0,
            colorFilter: ColorFilter.mode(
              Theme.of(context).colorScheme.onSurfaceVariant,
              BlendMode.srcIn,
            ),
          ),
          onPressed: () {
            final supplierInfo = context.read<WidgetsViewModel>();

            if (supplierInfo.company != null &&
                (supplierInfo.company?.bCrmCompany?.companyName ?? '')
                    .isNotEmpty &&
                (supplierInfo.company?.bCrmCompany?.id ?? 0) > 0) {
              final parsedUri = Uri.parse(
                  '${Links.bitrixChatWidget}name=${supplierInfo.company?.bCrmCompany?.companyName ?? ''}&pid=${supplierInfo.company?.bCrmCompany?.id ?? 0}');

              changeDrawerRoute(Routes.inAppWebView, arguments: parsedUri);
            }
          },
          label: Text(
            tr('chat'),
            style: Theme.of(context).textTheme.titleMedium,
          ),
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            vm.setBusy(true);
            await Future.wait([
              kanIkkeVm.getKanIkkeSettings(),
              vm.getNewsAndLatestJobFeedInitial(),
              vm.getOnboardingData(),
            ]).whenComplete(() => vm.setBusy(false));
          },
          child: ListView(
            children: [
              SmartGaps.gapH20,
              // InkWell(
              //   onTap: () {
              //     throw Exception();
              //   },
              //   child: Padding(
              //     padding: const EdgeInsets.symmetric(horizontal: 20),
              //     child: Text(
              //         '${tr('welcome')}, ${context.read<UserViewModel>().userModel?.user?.name ?? ''}'),
              //   ),
              // ),
              // SmartGaps.gapH20,
              const MembersBenefitsVideo(),
              SmartGaps.gapH20,
              if ((context
                          .read<AppDrawerViewModel>()
                          .supplierProfileModel
                          ?.bCrmCompany
                          ?.onboarding ??
                      0) ==
                  1)
                if ((vm.onboardingData?.percentage ?? 0) >= 0) ...[
                  const NewsFeedOnboarding(),
                  SmartGaps.gapH20,
                ],
              if ((context
                          .read<AppDrawerViewModel>()
                          .supplierProfileModel
                          ?.bCrmCompany
                          ?.companyPackge ??
                      '') !=
                  'affiliatepartner')
                Card(
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Skeleton.shade(
                              child: SvgPicture.asset(
                                'assets/images/bar-chart.svg',
                                width: 24.0,
                                height: 24.0,
                              ),
                            ),
                            SmartGaps.gapW10,
                            Text(
                              tr('statistics'),
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineMedium!
                                  .copyWith(height: 0),
                            ),
                          ],
                        ),
                        SmartGaps.gapH20,
                        Text(
                          tr('how_is_your_profile'),
                          style: Theme.of(context)
                              .textTheme
                              .titleSmall!
                              .copyWith(
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onSurfaceVariant,
                                  fontWeight: FontWeight.w600),
                        ),
                        SmartGaps.gapH20,
                        if (vm.busy) ...[
                          Skeletonizer(
                            enabled: vm.busy,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Flexible(
                                      flex: 2,
                                      child: Text(
                                        vm.newsFeed?.logs?.profileViews?.total
                                                .toString() ??
                                            '0',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headlineLarge!
                                            .copyWith(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                      ),
                                    ),
                                    SmartGaps.gapW10,
                                    Flexible(
                                      flex: 5,
                                      child: Skeleton.keep(
                                        child: Text(tr('profile_views'),
                                            overflow: TextOverflow.fade,
                                            style: Theme.of(context)
                                                .textTheme
                                                .titleMedium!
                                                .copyWith(height: 1)),
                                      ),
                                    )
                                  ],
                                ),
                                SmartGaps.gapH5,
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Flexible(
                                      flex: 2,
                                      child: Text(
                                        vm.totalClicks.toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .headlineLarge!
                                            .copyWith(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                      ),
                                    ),
                                    SmartGaps.gapW10,
                                    Flexible(
                                      flex: 5,
                                      child: Skeleton.keep(
                                        child: Text(tr('project_views'),
                                            overflow: TextOverflow.fade,
                                            style: Theme.of(context)
                                                .textTheme
                                                .titleMedium!
                                                .copyWith(height: 1)),
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ] else
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              if ((vm.newsFeed?.logs?.profileViews?.total
                                              .toString() ??
                                          '0') !=
                                      '0' &&
                                  !vm.busy) ...[
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Flexible(
                                      flex: 2,
                                      child: Text(
                                        vm.newsFeed?.logs?.profileViews?.total
                                                .toString() ??
                                            '0',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headlineLarge!
                                            .copyWith(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                      ),
                                    ),
                                    SmartGaps.gapW10,
                                    Flexible(
                                      flex: 5,
                                      child: Skeleton.keep(
                                        child: Text(tr('profile_views'),
                                            overflow: TextOverflow.fade,
                                            style: Theme.of(context)
                                                .textTheme
                                                .titleMedium!
                                                .copyWith(height: 1)),
                                      ),
                                    )
                                  ],
                                ),
                              ],
                              if ((vm.totalClicks.toString()) != '0' &&
                                  !vm.busy) ...[
                                SmartGaps.gapH5,
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Flexible(
                                      flex: 2,
                                      child: Text(
                                        vm.totalClicks.toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .headlineLarge!
                                            .copyWith(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                      ),
                                    ),
                                    SmartGaps.gapW10,
                                    Flexible(
                                      flex: 5,
                                      child: Skeleton.keep(
                                        child: Text(tr('project_views'),
                                            overflow: TextOverflow.fade,
                                            style: Theme.of(context)
                                                .textTheme
                                                .titleMedium!
                                                .copyWith(height: 1)),
                                      ),
                                    )
                                  ],
                                ),
                              ]
                            ],
                          ),
                        SmartGaps.gapH10,
                        Text(
                          '(Last 3 Months)',
                          style: GoogleFonts.notoSans(
                            textStyle: const TextStyle(
                              color: Color(0xff707070),
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                        SmartGaps.gapH20,
                        InkWell(
                          onTap: () async {
                            final haaid = context
                                .read<UserViewModel>()
                                .userModel
                                ?.user
                                ?.id;
                            final url = Uri.tryParse(
                                'https://www.haandvaerker.dk/profil/firma/x/$haaid');

                            if (url != null) {
                              changeDrawerRoute(Routes.inAppWebView,
                                  arguments: url);
                            }
                          },
                          child: Text(
                            tr('view_your_profile_here'),
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              // Offer Due Date - displays the latest job feed
              SmartGaps.gapH20,
              if ((context
                          .watch<AppDrawerViewModel>()
                          .supplierProfileModel
                          ?.bCrmCompany
                          ?.deadlineOffer ??
                      0) ==
                  1)
                if (vm.latestJobFeed != null &&
                    vm.latestJobFeed!.isNotEmpty) ...[
                  const SizedBox(height: 20),
                  Card(
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ...vm.latestJobFeed!.map(
                          (latestJob) => NewsFeedLatestJobItem(
                            projectName: latestJob.projectName ?? '',
                            offerDueDate: latestJob.deadlineOffer ?? '',
                            offerDueDateColor: latestJob.colorCode ?? '',
                            jobId: latestJob.jobId ?? 0,
                            jobIndex: vm.latestJobFeed?.indexOf(latestJob) ?? 0,
                            totalJobs: vm.latestJobFeed!.length,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
            ],
          ),
        ),
        // //!HIDDEN FOR NOW
        // body: Skeletonizer(
        //   enabled: context.watch<NewsFeedViewModel>().busy ||
        //       context.watch<NewsFeedViewModel>().newsFeed == null,
        //   child: Container(
        //     padding: const EdgeInsets.fromLTRB(20, 10, 20, 0),
        //     child: RefreshIndicator(
        //       onRefresh: () async {
        //         final newsFeedVm = context.read<NewsFeedViewModel>();
        //         newsFeedVm.setBusy(true);
        //         await newsFeedVm.getNewsFeed();
        //         newsFeedVm.setBusy(false);
        //       },
        //       child: const SingleChildScrollView(
        //         child: Column(
        //           children: [
        //             //
        //             // SizedBox(height: 20),
        //             //HIDDEN FOR NOW
        //             //THIS SHOULD BE DYNAMIC
        //             // Image.asset(
        //             //   'assets/images/airtoxPromo.png',
        //             //   height: 145,
        //             //   fit: BoxFit.fitWidth,
        //             //   width: MediaQuery.of(context).size.width,
        //             // ),
        //             NewsFeedCardTiles(),
        //             ChartCard(),
        //             TotalProjectsCard(),
        //             TotalRatingCard(),
        //             RatingBreakdownCard(),
        //             //
        //           ],
        //         ),
        //       ),
        //     ),
        //   ),
        // ),
      );
    });
  }
}

class NewsFeedCardTiles extends StatelessWidget {
  const NewsFeedCardTiles({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<NewsFeedViewModel>(
      builder: (context, newsFeedVM, _) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Column(
            children: [
              //

              Row(
                children: [
                  //

                  // totalMarketplace
                  Expanded(
                    child: NewsFeedCard(
                      count: newsFeedVM.newsFeed?.totalMarketPlace ?? 0,
                      subText: tr('total_number_of'),
                      mainText: tr('marketplace'),
                      callbackText: tr('go_to_marketplace'),
                      onPressedCallback: () =>
                          changeDrawerRoute(Routes.marketPlaceScreen),
                    ),
                  ),

                  SmartGaps.gapW10,

                  // totalCustomers
                  Expanded(
                    child: NewsFeedCard(
                      count: newsFeedVM.newsFeed?.totalJobs ?? 0,
                      subText: tr('total_number_of'),
                      mainText: tr('my_customers'),
                      callbackText: tr('go_to_my_customers'),
                      onPressedCallback: () =>
                          changeDrawerRoute(Routes.yourClient),
                    ),
                  ),

                  SmartGaps.gapW10,

                  // activeContract
                  Expanded(
                    child: NewsFeedCard(
                      count: newsFeedVM.newsFeed?.totalContract ?? 0,
                      mainText: tr('active_contract'),
                    ),
                  ),

                  //
                ],
              ),

              SmartGaps.gapH10,

              Row(
                children: [
                  //

                  // totalProjects
                  Expanded(
                    child: NewsFeedCard(
                      count: newsFeedVM.displayProjects.isNotEmpty
                          ? newsFeedVM.displayProjects.length
                          : 0,
                      subText: tr('total_number_of'),
                      mainText: tr('projects'),
                    ),
                  ),

                  SmartGaps.gapW10,

                  // totalReviews
                  Expanded(
                    child: NewsFeedCard(
                      count: newsFeedVM.newsFeed?.totalReviews ?? 0,
                      subText: tr('all_time'),
                      mainText: tr('recommendations'),
                    ),
                  ),

                  SmartGaps.gapW10,

                  // totalViews
                  Expanded(
                    child: NewsFeedCard(
                      count:
                          newsFeedVM.newsFeed?.logs?.profileViews?.total ?? 0,
                      subText: tr('all_time'),
                      mainText: tr('profile_views'),
                    ),
                  ),

                  //
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}

// Offer Due Date - Widget for Latest Job Item(s)
class NewsFeedLatestJobItem extends StatefulWidget {
  const NewsFeedLatestJobItem({
    super.key,
    required this.projectName,
    required this.offerDueDate,
    required this.offerDueDateColor,
    required this.jobId,
    required this.jobIndex,
    required this.totalJobs,
  });

  final String projectName;
  final String offerDueDate;
  final String offerDueDateColor;
  final int jobId;
  final int jobIndex;
  final int totalJobs;

  @override
  NewsFeedLatestJobItemState createState() => NewsFeedLatestJobItemState();
}

class NewsFeedLatestJobItemState extends State<NewsFeedLatestJobItem> {
  @override
  Widget build(BuildContext context) {
    Color color = PartnerAppColors.darkBlue;
    String offerDueDate = '';
    if (widget.offerDueDateColor.isNotEmpty) {
      color = Color(int.parse(
          '0xff${widget.offerDueDateColor.replaceAll(RegExp('[^A-Za-z0-9]'), '')}'));
    }
    if (widget.offerDueDate.isNotEmpty) {
      offerDueDate =
          widget.offerDueDate.replaceAll(RegExp('[^A-Za-z0-9]'), '.');
    }
    return InkWell(
      onTap: () {
        changeDrawerRoute(Routes.yourClient, arguments: {
          'jobId': widget.jobId,
        });
      },
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.projectName,
                  style: Theme.of(context)
                      .textTheme
                      .titleMedium!
                      .copyWith(height: 0),
                ),
                SmartGaps.gapH20,
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      tr('due_date'),
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            color:
                                Theme.of(context).colorScheme.onSurfaceVariant,
                          ),
                    ),
                    Text(
                      '${tr('offer_due_date')} $offerDueDate',
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            color: color,
                          ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          if (widget.jobIndex != widget.totalJobs - 1)
            Divider(
              color: Theme.of(context).colorScheme.onSurfaceVariant,
              thickness: 0.15,
            ),
          if (widget.jobIndex == widget.totalJobs - 1) SmartGaps.gapH10,
        ],
      ),
    );
  }
}
