import 'dart:io';

import 'package:Haandvaerker.dk/ui/components/custom_formbuilder_imagepicker.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/package/package_popup_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/extension/snackbar.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/mester-mester/mester_partner_profile_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/packages/packages_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

class M2mLaborFormScreen extends StatefulWidget {
  const M2mLaborFormScreen({super.key});
  @override
  M2mLaborFormScreenState createState() => M2mLaborFormScreenState();
}

class M2mLaborFormScreenState extends State<M2mLaborFormScreen> {
  final GlobalKey<FormBuilderState> _fbKeyLabor = GlobalKey<FormBuilderState>();
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();

  List<File> files = <File>[];

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await context.read<MesterPartnerProfileViewModel>().getIndustryData();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: context.watch<MesterPartnerProfileViewModel>().busy
          ? Center(
              child: loader(),
            )
          : SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //

                    SmartGaps.gapH20,

                    Text(
                      tr('post_vacancies'),
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),

                    SmartGaps.gapH10,

                    FormBuilder(
                      key: _fbKeyLabor,
                      initialValue: const {'unemployed_men': '1', 'offer': '3'},
                      autovalidateMode: AutovalidateMode.disabled,
                      child: Consumer<MesterPartnerProfileViewModel>(
                        builder: (context, vm, _) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              //

                              SmartGaps.gapH10,

                              Text(
                                tr('selectSubjectGroup'),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              SmartGaps.gapH5,

                              FormBuilderDropdown(
                                name: "subject_group",
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    filled: true,
                                    fillColor: Colors.grey[200]),
                                validator: FormBuilderValidators.required(
                                    errorText: context
                                        .tr('this_field_cannot_be_empty')),
                                items: vm.industryData
                                    .map(
                                      (data) => DropdownMenuItem(
                                        value: data.branchId.toString(),
                                        child: Text(
                                          context.locale.languageCode == 'da'
                                              ? data.branche!
                                              : data.branchEn!,
                                        ),
                                      ),
                                    )
                                    .toList(),
                              ),

                              SmartGaps.gapH30,

                              Text(
                                tr('numberUnemployedMen'),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              SmartGaps.gapH5,

                              FormBuilderTextField(
                                name: "unemployed_men",
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    filled: true,
                                    fillColor: Colors.grey[200]),
                                validator: FormBuilderValidators.numeric(
                                    errorText: tr('value_must_be_numeric')),
                              ),

                              SmartGaps.gapH30,

                              Text(tr('numberOfLaborOffers'),
                                  style:
                                      Theme.of(context).textTheme.bodyMedium),

                              SmartGaps.gapH5,

                              FormBuilderDropdown(
                                name: "offer",
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.grey[200],
                                  border: InputBorder.none,
                                ),
                                // validators: [],
                                items: [3, 2, 1]
                                    .map(
                                      (f) => DropdownMenuItem(
                                        value: f.toString(),
                                        child: Text(
                                          '${f.toString()} ${tr('offer')}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyMedium,
                                        ),
                                      ),
                                    )
                                    .toList(),
                              ),

                              SmartGaps.gapH30,

                              Text(
                                tr('zipCode'),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              SmartGaps.gapH5,

                              FormBuilderTextField(
                                name: "zipcode",
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.grey[200],
                                  border: InputBorder.none,
                                ),
                                validator: FormBuilderValidators.compose(
                                  [
                                    FormBuilderValidators.required(
                                        errorText: context
                                            .tr('this_field_cannot_be_empty')),
                                    FormBuilderValidators.numeric(
                                        errorText: context
                                            .tr('value_must_be_numeric')),
                                  ],
                                ),
                              ),

                              SmartGaps.gapH30,

                              Text(
                                tr('startingDate'),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              FormBuilderDateTimePicker(
                                initialDate: DateTime.now(),
                                firstDate: DateTime.now()
                                    .subtract(const Duration(days: 1)),
                                lastDate: DateTime(2100),
                                name: "starting_date",
                                inputType: InputType.date,
                                format: DateFormatType.isoDate.formatter,
                                decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.grey[200],
                                    border: InputBorder.none),
                                validator: FormBuilderValidators.required(
                                    errorText: context
                                        .tr('this_field_cannot_be_empty')),
                              ),

                              SmartGaps.gapH30,

                              Text(
                                tr('hourlyRate'),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              SmartGaps.gapH5,

                              FormBuilderTextField(
                                name: "hourly_rate",
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.grey[200],
                                    border: InputBorder.none),
                                validator: FormBuilderValidators.compose(
                                  [
                                    FormBuilderValidators.required(
                                        errorText: context
                                            .tr('this_field_cannot_be_empty')),
                                    FormBuilderValidators.numeric(
                                        errorText: context
                                            .tr('value_must_be_numeric')),
                                  ],
                                ),
                              ),

                              SmartGaps.gapH30,

                              Text(
                                tr('shortDescription'),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              SmartGaps.gapH5,

                              FormBuilderTextField(
                                name: "write_about_you",
                                decoration: InputDecoration(
                                    isDense: false,
                                    contentPadding: const EdgeInsets.fromLTRB(
                                        20, 10, 20, 35),
                                    filled: true,
                                    fillColor: Colors.grey[200],
                                    border: InputBorder.none),
                                validator: FormBuilderValidators.required(
                                    errorText: context
                                        .tr('this_field_cannot_be_empty')),
                              ),

                              SmartGaps.gapH30,

                              Text(
                                tr('you_can_upload_images'),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              SmartGaps.gapH10,

                              CustomFormBuilderImagePicker(
                                customButtonMargin: const EdgeInsets.all(1),
                                useOriginalImagePicker: false,
                                attribute: "images",
                                decoration: const InputDecoration(
                                  border: InputBorder.none,
                                ),
                                validators: [
                                  FormBuilderValidators.required(
                                      errorText:
                                          tr('this_field_cannot_be_empty')),
                                  (val) {
                                    if (val != null) {
                                      for (File f in val) {
                                        var byteSize = f.lengthSync();
                                        String fileExtension =
                                            f.path.substring(f.path.length - 4);
                                        if (byteSize > 17194304) {
                                          return tr(
                                              'added_image_more_than_4000kb');
                                        } else if (fileExtension != '.jpg' &&
                                            fileExtension != 'jpeg' &&
                                            fileExtension != '.gif' &&
                                            fileExtension != '.png') {
                                          return context
                                              .tr('image_not_JPG_GIF_PNG');
                                        } else {
                                          return null;
                                        }
                                      }
                                    }
                                    return null;
                                  },
                                ],
                              ),

                              //
                            ],
                            //
                          );
                        },
                      ),
                    ),

                    SmartGaps.gapH20,

                    Row(
                      children: [
                        //

                        Expanded(
                          child: InkWell(
                            onTap: () {
                              _fbKeyLabor.currentState!.reset();
                            },
                            child: Container(
                              height: 50,
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    tr('clear_fields'),
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),

                        SmartGaps.gapW20,

                        Expanded(
                          child: InkWell(
                            onTap: () async {
                              await submitLaborForm();
                            },
                            child: Container(
                              height: 50,
                              decoration: BoxDecoration(
                                  color:
                                      Theme.of(context).colorScheme.secondary,
                                  border: Border.all(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    tr('continue'),
                                    style: const TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),

                        //
                      ],
                    ),

                    SmartGaps.gapH20,

                    //
                  ],
                ),
              ),
            ),
    );
  }

  Future<void> submitLaborForm() async {
    final packagesVm = context.read<PackagesViewModel>();

    showLoadingDialog(context, loadingKey);

    await packagesVm.getPackages();

    if (!packagesVm.mesterMesterPost.status) {
      Navigator.of(loadingKey.currentContext!).pop();
      if (mounted) {
        await showUnAvailableDialog(context, packagesVm.mesterMesterPost,
            popTwice: false);
      }
    } else {
      //
      if (_fbKeyLabor.currentState!.saveAndValidate() && mounted) {
        final vm = context.read<MesterPartnerProfileViewModel>();

        final payload = {
          'type': 'arbedjskraft',
          'industryId': int.parse(
              (_fbKeyLabor.currentState!.value['subject_group'] ?? '0')),
          'subIndustryId': 10,
          'numberOfStaff': int.parse(
              (_fbKeyLabor.currentState!.value['unemployed_men'] ?? '0')),
          'numberOfOffers':
              int.parse((_fbKeyLabor.currentState!.value['offer'] ?? '0')),
          'postalCode': (_fbKeyLabor.currentState!.value['zipcode'] ?? ''),
          'startDate': _fbKeyLabor.currentState!.value['starting_date'] != null
              ? _fbKeyLabor.currentState!.value['starting_date']
                  .toString()
                  .split(' ')
                  .first
              : '',
          'duration': '',
          'budget': num.parse(
              (_fbKeyLabor.currentState!.value['hourly_rate'] ?? '0')),
          'description':
              _fbKeyLabor.currentState!.value['write_about_you'] ?? '',
          'files': [],
        };

        //UPLOAD ONLY ONE FOR NOW
        //API IS NOT SET TO ACCEPT MUTLITPLE UPLOADS YET
        List<dynamic> valueImages =
            _fbKeyLabor.currentState!.value['images'] ?? [];
        List<File> imagesToUpload = valueImages.whereType<File>().toList();

        if (imagesToUpload.isNotEmpty) {
          payload['images'] = await MultipartFile.fromFile(
              imagesToUpload.first.path,
              filename: imagesToUpload.first.path.split("/").last);
        }

        try {
          await vm.submitLaborOrTask(payload: payload).whenComplete(
            () async {
              if (!mounted) return;

              Navigator.of(loadingKey.currentContext!).pop();
              final success =
                  await showSuccessAnimationDialog(context, true, '');

              if (success && mounted) {
                backDrawerRoute();
              }
            },
          );
        } catch (_) {
          Navigator.of(loadingKey.currentContext!).pop();
          if (mounted) {
            context.showErrorSnackBar();
          }
        }
      }
      //
    }
  }

  void clearData() {
    final fields = _fbKeyLabor.currentState!.fields;
    fields['subject_group']?.reset();
    fields['unemployed_men']?.didChange('1');
    fields['offer']?.didChange('3');
    fields['zipcode']?.didChange('');
    fields['starting_date']?.reset();
    fields['hourly_rate']?.didChange('');
    fields['write_about_you']?.didChange('');
    fields['images']?.didChange([]);
  }
}
