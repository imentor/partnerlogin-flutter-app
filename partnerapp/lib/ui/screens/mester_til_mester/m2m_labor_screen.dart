import 'package:Haandvaerker.dk/ui/components/custom_button.dart';
import 'package:Haandvaerker.dk/ui/components/empty.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/package/package_popup_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/photo_gallery.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:Haandvaerker.dk/utils/extension/snackbar.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/launcher.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/labor_task_toggle_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/mester-mester/mester_partner_profile_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/packages/packages_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class M2mLaborScreen extends StatefulWidget {
  const M2mLaborScreen({super.key});

  @override
  M2mLaborScreenState createState() => M2mLaborScreenState();
}

class M2mLaborScreenState extends State<M2mLaborScreen> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final m2mVm = context.read<MesterPartnerProfileViewModel>();
      m2mVm.type = 1;
      m2mVm.setBusy(true);
      await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function: m2mVm.getMesterJobs(),
      );
      m2mVm.setBusy(false);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Container(
        padding: const EdgeInsets.only(top: 50, left: 20.0, right: 20.0),
        child: const SingleChildScrollView(
          child: Column(
            children: <Widget>[
              StaticHeaders(),
              DualButtons(),
            ],
          ),
        ),
      ),
    );
  }
}

class StaticHeaders extends StatefulWidget {
  const StaticHeaders({super.key});

  @override
  StaticHeadersState createState() => StaticHeadersState();
}

class StaticHeadersState extends State<StaticHeaders> {
  bool _showHeaders = false;

  @override
  Widget build(BuildContext context) {
    List<String> list = [
      'lending_excess_labor',
      'optimize_the_economy',
      'retain_the_company_workforce'
    ];

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(tr('too_few_tasks'),
            style: Theme.of(context)
                .textTheme
                .headlineLarge!
                .copyWith(letterSpacing: -0.5)),
        SmartGaps.gapH10,
        Text('- ${tr('make_your_labor_available')}',
            style: Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(fontWeight: FontWeight.w600)),
        SmartGaps.gapH30,
        for (String f in list)
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Icon(ElementIcons.success,
                    size: 18, color: Theme.of(context).colorScheme.primary),
                Text('  ${tr(f)}',
                    style: Theme.of(context)
                        .textTheme
                        .titleSmall!
                        .copyWith(height: 1))
              ],
            ),
          ),
        !_showHeaders
            ? Container()
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SmartGaps.gapH10,
                  Text(tr('many_companies_find'),
                      style: Theme.of(context).textTheme.bodyMedium),
                  SmartGaps.gapH10,
                  Text(tr('by_lending_labor'),
                      style: Theme.of(context).textTheme.bodyMedium),
                  SmartGaps.gapH10,
                  Text(tr('when_the_task_come'),
                      style: Theme.of(context).textTheme.bodyMedium),
                  SmartGaps.gapH10,
                ],
              ),
        SmartGaps.gapH20,
        ShowHideButton(
          show: _showHeaders,
          onPressed: () {
            setState(() {
              _showHeaders = !_showHeaders;
            });
          },
        ),
        SmartGaps.gapH20,
      ],
    );
  }
}

class DualButtons extends StatefulWidget {
  const DualButtons({super.key});

  @override
  DualButtonsState createState() => DualButtonsState();
}

class DualButtonsState extends State<DualButtons> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
                child: InkWell(
              splashColor: Theme.of(context).colorScheme.primary,
              onTap: context.watch<MesterPartnerProfileViewModel>().busy
                  ? null
                  : () {
                      final packagesVm = context.read<PackagesViewModel>();
                      if (!packagesVm.mesterMesterPost.status) {
                        showUnAvailableDialog(
                            context, packagesVm.mesterMesterPost);
                      } else {
                        changeDrawerRoute(Routes.m2mLaborFormScreen);
                      }
                    },
              child: Container(
                height: 60,
                padding: const EdgeInsets.symmetric(horizontal: 15),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Theme.of(context).colorScheme.primary,
                    width: 2,
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Flexible(
                      child: Text(tr('post_vacancy'),
                          overflow: TextOverflow.fade,
                          textAlign: TextAlign.center,
                          style: Theme.of(context)
                              .textTheme
                              .bodyMedium!
                              .copyWith(
                                  color: Theme.of(context).colorScheme.primary,
                                  height: 1,
                                  fontSize: 13)),
                    ),
                    SmartGaps.gapW10,
                    Icon(ElementIcons.right,
                        color: Theme.of(context).colorScheme.primary)
                  ],
                ),
              ),
            )),
            SmartGaps.gapW10,
            Expanded(
              child: InkWell(
                splashColor: Theme.of(context).colorScheme.primary,
                onTap: context.watch<MesterPartnerProfileViewModel>().busy
                    ? null
                    : () {
                        final toggleVM =
                            context.read<LaborTaskToggleViewModel>();
                        toggleVM.setTask(false);
                        changeDrawerRoute(Routes.marketPlaceScreen);
                      },
                child: Container(
                  height: 60,
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Theme.of(context).colorScheme.primary,
                      width: 2,
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Flexible(
                        child: Text(tr('see_available_labor'),
                            overflow: TextOverflow.fade,
                            textAlign: TextAlign.center,
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                    height: 1,
                                    fontSize: 13)),
                      ),
                      SmartGaps.gapW10,
                      Icon(ElementIcons.right,
                          color: Theme.of(context).colorScheme.primary),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
        SmartGaps.gapH30,
        const LaborList()
      ],
    );
  }
}

class LaborList extends StatefulWidget {
  const LaborList({super.key});

  @override
  LaborListState createState() => LaborListState();
}

class LaborListState extends State<LaborList> {
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();
  int activeIndex = -1;

  Future<void> attemptDeleteLabor(id) async {
    final selected = await showOkCancelAlertDialog(
        context: context,
        title: tr('delete_labor'),
        message: tr('are_you_sure'),
        okLabel: tr('delete'));

    if (selected == OkCancelResult.ok && mounted) {
      showLoadingDialog(context, loadingKey);

      final mesterParterProfileVM =
          context.read<MesterPartnerProfileViewModel>();
      try {
        await mesterParterProfileVM
            .deleteLaborOrTask(mesterId: id)
            .then((value) {
          Navigator.of(loadingKey.currentContext!, rootNavigator: true).pop();
          if (value) {
            showSuccessAnimationDialog(
              myGlobals.homeScaffoldKey!.currentContext!,
              true,
              tr('delete_labor_success'),
            );
          }
        });
      } catch (_) {
        Navigator.of(loadingKey.currentContext!, rootNavigator: true).pop();
        myGlobals.homeScaffoldKey!.currentContext!.showErrorSnackBar();
      }
    } else {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    // MesterPartnerProfileViewModel
    return Consumer<MesterPartnerProfileViewModel>(
      builder:
          (BuildContext context, MesterPartnerProfileViewModel vm, Widget? _) {
        return vm.busy
            ? const Center(child: ConnectivityLoader())
            : (vm.jobItems.isEmpty)
                ? Container()
                : ListView.builder(
                    primary: false,
                    shrinkWrap: true,
                    itemCount: vm.jobItems.length,
                    itemBuilder: (context, index) {
                      final item = vm.jobItems.elementAt(index);

                      return Dismissible(
                        key: UniqueKey(),
                        background: Container(
                            alignment: Alignment.center,
                            child: const Icon(ElementIcons.delete,
                                size: 40, color: Colors.red)),
                        onDismissed: (_) async {
                          await attemptDeleteLabor(item.id);
                        },
                        child: Card(
                            child: Container(
                                padding: const EdgeInsets.all(5),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: IconButton(
                                        icon: const Icon(ElementIcons.delete,
                                            size: 40, color: Colors.red),
                                        onPressed: () async {
                                          await attemptDeleteLabor(item.id);
                                        },
                                      ),
                                    ),
                                    SmartGaps.gapH20,
                                    Row(
                                      children: [
                                        SvgPicture.asset(
                                            'assets/images/partner.svg',
                                            colorFilter: ColorFilter.mode(
                                              Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              BlendMode.srcIn,
                                            ),
                                            width: 50,
                                            height: 50),
                                        SmartGaps.gapW5,
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text('Labor',
                                                  overflow: TextOverflow.fade,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headlineSmall),
                                              Text(
                                                  '${tr('start_date')}: ${Formatter.formatDateStrings(type: DateFormatType.standardDate, dateString: item.startDate)}',
                                                  overflow: TextOverflow.fade,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headlineSmall)
                                            ],
                                          ),
                                        ),
                                        SmartGaps.gapW5,
                                        IconButton(
                                          icon: Icon(
                                            activeIndex != index
                                                ? Icons.expand_more
                                                : Icons.expand_less,
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            size: 35,
                                          ),
                                          onPressed: () {
                                            setState(
                                              () {
                                                if (activeIndex == index) {
                                                  activeIndex = -1;
                                                } else {
                                                  activeIndex = index;
                                                }
                                              },
                                            );
                                          },
                                        )
                                      ],
                                    ),
                                    SmartGaps.gapH10,
                                    Text(
                                      item.title!,
                                      overflow: TextOverflow.fade,
                                      style: Theme.of(context)
                                          .textTheme
                                          .headlineSmall,
                                    ),
                                    SmartGaps.gapH5,
                                    Container(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .onSurfaceVariant,
                                        width: double.infinity,
                                        alignment: Alignment.center,
                                        padding: const EdgeInsets.all(5),
                                        child: Text(
                                            '${item.partners!.length} ${tr('out_of')} ${item.numberOfOffers.toString()} ${tr('offer')}s',
                                            overflow: TextOverflow.fade,
                                            style: Theme.of(context)
                                                .textTheme
                                                .titleMedium!
                                                .apply(color: Colors.white))),
                                    SmartGaps.gapH5,
                                    //Table
                                    if (item.partners!.isNotEmpty)
                                      SingleChildScrollView(
                                        physics: const BouncingScrollPhysics(),
                                        scrollDirection: Axis.horizontal,
                                        child: DataTable(
                                          columns: <DataColumn>[
                                            DataColumn(
                                              label: Text(
                                                tr('business'),
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .titleSmall,
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                tr('name'),
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .titleSmall,
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                tr('mobile'),
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .titleSmall,
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                tr('email'),
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .titleSmall,
                                              ),
                                            ),
                                          ],
                                          rows: item.partners!
                                              .map(
                                                (pidl) => DataRow(
                                                  cells: [
                                                    DataCell(
                                                      Text(
                                                        pidl.company as String,
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .titleSmall,
                                                      ),
                                                    ),
                                                    DataCell(
                                                      Text(
                                                        pidl.name as String,
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .titleSmall,
                                                      ),
                                                    ),
                                                    DataCell(
                                                      InkWell(
                                                        onTap: () {
                                                          Launcher()
                                                              .launchPhoneCall(
                                                                  pidl.mobile);
                                                        },
                                                        child: Text(
                                                          pidl.mobile as String,
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .titleSmall!
                                                                  .apply(
                                                                    color: Theme.of(
                                                                            context)
                                                                        .colorScheme
                                                                        .primary,
                                                                  ),
                                                        ),
                                                      ),
                                                    ),
                                                    DataCell(
                                                      InkWell(
                                                        onTap: () {
                                                          Launcher()
                                                              .launchEmail(
                                                                  pidl.email);
                                                        },
                                                        child: Text(
                                                          pidl.email as String,
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .titleSmall!
                                                                  .apply(
                                                                    color: Theme.of(
                                                                            context)
                                                                        .colorScheme
                                                                        .primary,
                                                                  ),
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              )
                                              .toList(),
                                        ),
                                      ),
                                    SmartGaps.gapH10,
                                    // //hidden info
                                    if (activeIndex == index)
                                      Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                // expected_staffing
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Text(
                                                        tr('expected_staffing'),
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .headlineSmall),
                                                    Text(
                                                        '${item.numberOfStaff.toString()} ${tr('men')}',
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .bodyMedium),
                                                  ],
                                                ),
                                              ],
                                            ),
                                            SmartGaps.gapH10,
                                            Text(
                                              tr('expected_budget'),
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headlineSmall,
                                            ),
                                            Text(
                                              item.budget.toString(),
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyMedium,
                                            ),
                                            SmartGaps.gapH10,
                                            Text(
                                              tr('description'),
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headlineSmall,
                                            ),
                                            Text(
                                              item.description as String,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyMedium,
                                            ),
                                            SmartGaps.gapH10,
                                            Text(
                                              tr('pictures'),
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headlineSmall,
                                            ),
                                            (item.images != null &&
                                                    item.images!.isNotEmpty)
                                                ? SizedBox(
                                                    height: 150.0,
                                                    child: CustomHeroGallery(
                                                        images: item.images),
                                                  )
                                                : Center(child: empty())
                                          ])
                                  ],
                                ))),
                      );
                    });
      },
    );
  }
}
