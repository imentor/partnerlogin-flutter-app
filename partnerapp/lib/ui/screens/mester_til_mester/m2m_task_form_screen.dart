import 'dart:io';

import 'package:Haandvaerker.dk/model/get_types_response_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/custom_formbuilder_imagepicker.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/package/package_popup_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/caster.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/extension/snackbar.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/mester-mester/mester_partner_profile_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/packages/packages_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class M2mTaskFormScreen extends StatefulWidget {
  const M2mTaskFormScreen({super.key});
  @override
  M2mTaskFormScreenState createState() => M2mTaskFormScreenState();
}

class M2mTaskFormScreenState extends State<M2mTaskFormScreen> {
  final GlobalKey<FormBuilderState> _fbKeyTask = GlobalKey<FormBuilderState>();
  static List<Map<String, dynamic>> durationList = [
    {'value': '1t', 'label': '1 timer'},
    {'value': '2t', 'label': '2 timer'},
    {'value': '3t', 'label': '3 timer'},
    {'value': '4t', 'label': '4 timer'},
    {'value': '5t', 'label': '5 timer'},
    {'value': '6t', 'label': '6 timer'},
    {'value': '1d', 'label': '1 dag'},
    {'value': '2d', 'label': '2 dage'},
    {'value': '3d', 'label': '3 dage'},
    {'value': '4d', 'label': '4 dage'},
    {'value': '5d', 'label': '5 dage'},
    {'value': '6d', 'label': '6 dage'},
    {'value': '7d', 'label': '7 dage'},
    {'value': '8d', 'label': '8 dage'},
    {'value': '9d', 'label': '9 dage'},
    {'value': '10d', 'label': '10 dage'},
    {'value': '11d', 'label': '11 dage'},
    {'value': '12d', 'label': '12 dage'},
    {'value': '13d', 'label': '13 dage'},
    {'value': '14d', 'label': '14 dage'},
    {'value': '15d', 'label': '15 dage'},
    {'value': '16d', 'label': 'Over 15 dage'}
  ];
  Data? _selectedSubjectGroup;
  String? _selectedSuProduct;
  String? _selectedDuration = durationList.elementAt(0)['value'] as String?;
  List<File> files = <File>[];
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await context.read<MesterPartnerProfileViewModel>().getIndustryData();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: context.watch<MesterPartnerProfileViewModel>().busy
          ? Center(
              child: loader(),
            )
          : SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //

                    Consumer<MesterPartnerProfileViewModel>(
                      builder: (context, vm, _) {
                        return FormBuilder(
                          key: _fbKeyTask,
                          initialValue: const {
                            'unemployed_men': '1',
                            'offer': '3'
                          },
                          autovalidateMode: AutovalidateMode.disabled,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              //

                              SmartGaps.gapH10,

                              Text(
                                tr('select_the_task_type'),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              SmartGaps.gapH5,

                              FormBuilderDropdown(
                                  name: "subject_group",
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      filled: true,
                                      fillColor: Colors.grey[200]),
                                  validator: FormBuilderValidators.required(
                                      errorText: context
                                          .tr('this_field_cannot_be_empty')),
                                  onChanged: (val) {
                                    setState(() {
                                      if (val == null) {
                                        _selectedSubjectGroup = null;
                                      } else {
                                        _selectedSubjectGroup =
                                            Caster.cast<Data>(val);

                                        _selectedSuProduct = null;
                                      }
                                    });
                                  },
                                  items: vm.industryData
                                      .map((data) => DropdownMenuItem(
                                          value: data,
                                          child: Text(
                                            context.locale.languageCode == 'da'
                                                ? data.branche!
                                                : data.branchEn!,
                                          )))
                                      .toList()),

                              /**
                     * Sub category here
                     * implemented normal dropdown instead for formdropdown,
                     * value is inside _selectedSuProduct
                     */

                              if (_selectedSubjectGroup != null)
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    //

                                    SmartGaps.gapH30,

                                    Text(
                                      tr('more_info'),
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium,
                                    ),

                                    Container(
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                          color: Colors.grey[200]),
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 10, horizontal: 0),
                                      child: ButtonTheme(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 10, horizontal: 0),
                                        alignedDropdown: true,
                                        child: DropdownButtonFormField<dynamic>(
                                          onTap: () {
                                            FocusManager.instance.primaryFocus!
                                                .unfocus();
                                          },
                                          decoration: InputDecoration.collapsed(
                                              hintText: '',
                                              filled: true,
                                              fillColor: Colors.grey[200]),
                                          autovalidateMode: AutovalidateMode
                                              .onUserInteraction,
                                          isExpanded: true,
                                          value: _selectedSuProduct,
                                          elevation: 24,
                                          iconSize: 24,
                                          icon: const Padding(
                                            padding: EdgeInsets.only(right: 10),
                                            child: Icon(Icons.arrow_drop_down),
                                          ),
                                          onChanged: (val) {
                                            setState(() {
                                              _selectedSuProduct =
                                                  val as String;
                                            });
                                          },
                                          items: _selectedSubjectGroup
                                              ?.subIndustries
                                              ?.map(
                                            (SubIndustries value) {
                                              return DropdownMenuItem<String>(
                                                value: value.subcategoryId
                                                    .toString(),
                                                child: Text(
                                                    context.locale
                                                                .languageCode ==
                                                            'da'
                                                        ? value.subCategoryName!
                                                        : value
                                                            .subCategoryNameEn!,
                                                    overflow:
                                                        TextOverflow.ellipsis),
                                              );
                                            },
                                          ).toList(),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),

                              SmartGaps.gapH30,

                              Text(
                                tr('expected_staffing'),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              SmartGaps.gapH5,

                              FormBuilderTextField(
                                name: "unemployed_men",
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    filled: true,
                                    fillColor: Colors.grey[200]),
                                validator: FormBuilderValidators.numeric(
                                  errorText: tr('value_must_be_numeric'),
                                ),
                              ),

                              SmartGaps.gapH30,

                              Text(
                                tr('number_of_offers_on_the_job'),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              SmartGaps.gapH5,

                              FormBuilderDropdown(
                                name: "offer",
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    filled: true,
                                    fillColor: Colors.grey[200]),
                                // validators: [],
                                items: [3, 2, 1]
                                    .map(
                                      (f) => DropdownMenuItem(
                                        value: f.toString(),
                                        child: Text(
                                          '${f.toString()} ${tr('offer')}',
                                          style: GoogleFonts.openSans(
                                              textStyle: const TextStyle(
                                                  fontSize: 16)),
                                        ),
                                      ),
                                    )
                                    .toList(),
                              ),

                              SmartGaps.gapH30,

                              Text(
                                tr('zip_code_for_the_assignment'),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              SmartGaps.gapH5,

                              FormBuilderTextField(
                                name: "zipcode",
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    filled: true,
                                    fillColor: Colors.grey[200]),
                                validator: FormBuilderValidators.compose(
                                  [
                                    FormBuilderValidators.required(
                                        errorText: context
                                            .tr('this_field_cannot_be_empty')),
                                    FormBuilderValidators.numeric(
                                        errorText: context
                                            .tr('value_must_be_numeric')),
                                  ],
                                ),
                              ),

                              SmartGaps.gapH30,

                              Text(
                                tr('startingDate'),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              SmartGaps.gapH5,

                              FormBuilderDateTimePicker(
                                initialDate: DateTime.now(),
                                firstDate: DateTime.now()
                                    .subtract(const Duration(days: 1)),
                                lastDate: DateTime(2100),
                                name: "starting_date",
                                inputType: InputType.date,
                                format:
                                    DateFormatType.descriptiveDate.formatter,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    filled: true,
                                    fillColor: Colors.grey[200]),
                                validator: FormBuilderValidators.required(
                                  errorText: tr('this_field_cannot_be_empty'),
                                ),
                              ),

                              SmartGaps.gapH30,

                              Text(
                                tr('duration'),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              SmartGaps.gapH5,

                              FormBuilderDropdown(
                                initialValue: _selectedDuration,
                                name: "duration",
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    filled: true,
                                    fillColor: Colors.grey[200]),
                                validator: FormBuilderValidators.required(
                                    errorText: context
                                        .tr('this_field_cannot_be_empty')),
                                onChanged: (val) {
                                  setState(() {
                                    _selectedDuration = val.toString();
                                  });
                                },
                                items: durationList
                                    .map(
                                      (f) => DropdownMenuItem(
                                        value: f['value'],
                                        child: Text(
                                          f['label'] as String,
                                        ),
                                      ),
                                    )
                                    .toList(),
                              ),

                              SmartGaps.gapH30,

                              Text(
                                tr('expected_budget'),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              SmartGaps.gapH5,

                              FormBuilderTextField(
                                name: "budget",
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    filled: true,
                                    fillColor: Colors.grey[200]),
                                validator: FormBuilderValidators.compose(
                                  [
                                    FormBuilderValidators.required(
                                        errorText: context
                                            .tr('this_field_cannot_be_empty')),
                                    FormBuilderValidators.numeric(
                                        errorText: context
                                            .tr('value_must_be_numeric')),
                                  ],
                                ),
                              ),

                              SmartGaps.gapH30,

                              Text(
                                tr('description'),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              SmartGaps.gapH5,

                              FormBuilderTextField(
                                name: "write_about_you",
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  filled: true,
                                  fillColor: Colors.grey[200],
                                ),
                                validator: FormBuilderValidators.required(
                                    errorText: context
                                        .tr('this_field_cannot_be_empty')),
                              ),

                              SmartGaps.gapH30,

                              Text(
                                tr('you_can_upload_images'),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              SmartGaps.gapH5,

                              CustomFormBuilderImagePicker(
                                customButtonMargin: const EdgeInsets.all(1),
                                useOriginalImagePicker: false,
                                attribute: "images",
                                decoration: const InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'Tis is the hint text'),
                                validators: [
                                  (val) {
                                    if (val != null) {
                                      if ((val as List).isEmpty) return null;

                                      for (File f in val) {
                                        var byteSize = f.lengthSync();
                                        String fileExtension =
                                            f.path.substring(f.path.length - 4);
                                        if (byteSize > 17194304) {
                                          return tr(
                                              'added_image_more_than_4000kb');
                                        } else if (fileExtension != '.jpg' &&
                                            fileExtension != 'jpeg' &&
                                            fileExtension != '.gif' &&
                                            fileExtension != '.png') {
                                          return context
                                              .tr('image_not_JPG_GIF_PNG');
                                        } else {
                                          return null;
                                        }
                                      }
                                    }
                                    return null;
                                  },
                                ],
                              ),

                              SmartGaps.gapH30,

                              Text(
                                tr('you_can_upload_files'),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              SmartGaps.gapH5,

                              Container(
                                margin: const EdgeInsets.all(10),
                                child: DottedBorder(
                                  color: Theme.of(context).colorScheme.primary,
                                  strokeWidth: 1,
                                  child: Container(
                                    padding: const EdgeInsets.all(10),
                                    height: 80,
                                    width: double.infinity,
                                    child: CustomDesignTheme.flatButtonStyle(
                                      onPressed: () async {
                                        //FOR NOW WE JUST ALLOW MULTPILE FILE PICKING
                                        //API WILL NOT ACCEPT MULTIPLE FILES YET.
                                        //IT WILL ONLY UPLOAD THE FIRST FILE IN THE LIST.

                                        FilePickerResult? result =
                                            await FilePicker.platform.pickFiles(
                                          type: FileType.custom,
                                          allowMultiple: true,
                                          allowedExtensions: [
                                            'pdf',
                                            'doc',
                                            'docx'
                                          ],
                                        );
                                        if (result != null) {
                                          List<File> tempFiles = result.paths
                                              .map((path) => File(path!))
                                              .toList();

                                          final copy = <File>[...files];
                                          copy.addAll(tempFiles);
                                          setState(() {
                                            files = copy;
                                          });
                                        }

                                        //
                                      },
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Image.asset(
                                            ImagePaths.files,
                                          ),
                                          SmartGaps.gapW5,
                                          RichText(
                                            maxLines: 2,
                                            text: TextSpan(
                                              text: '',
                                              style: TextStyle(
                                                  color: Theme.of(context)
                                                      .colorScheme
                                                      .primary),
                                              children: <TextSpan>[
                                                TextSpan(
                                                    text: tr(
                                                        'click_to_select_files'),
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Theme.of(context)
                                                            .colorScheme
                                                            .primary)),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),

                              SmartGaps.gapH5,
                              SizedBox(
                                width: MediaQuery.of(context).size.width,
                                child: Column(
                                  children: files.map<Widget>((item) {
                                    return Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        const Icon(
                                          Icons.file_present_rounded,
                                          color: PartnerAppColors.blue,
                                          size: 30,
                                        ),
                                        SmartGaps.gapW5,
                                        Expanded(
                                          child:
                                              Text(item.path.split('/').last),
                                        ),
                                        IconButton(
                                          onPressed: () {
                                            final copy = <File>[...files];
                                            copy.remove(item);
                                            setState(() {
                                              files = copy;
                                            });
                                          },
                                          color: PartnerAppColors.red,
                                          iconSize: 30,
                                          icon: const Icon(Icons.delete),
                                        ),
                                      ],
                                    );
                                  }).toList(),
                                ),
                              ),

                              //
                            ],
                          ),
                        );
                      },
                    ),

                    SmartGaps.gapH20,

                    Row(
                      children: [
                        //

                        Expanded(
                          child: InkWell(
                            onTap: () {
                              clearData();
                            },
                            child: Container(
                              height: 50,
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    tr('clear_fields'),
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),

                        SmartGaps.gapW20,

                        Expanded(
                          child: InkWell(
                            onTap: () async {
                              await submitLaborForm();
                            },
                            child: Container(
                              height: 50,
                              decoration: BoxDecoration(
                                  color:
                                      Theme.of(context).colorScheme.secondary,
                                  border: Border.all(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    tr('continue'),
                                    style: const TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )

                        //
                      ],
                    ),
                    SmartGaps.gapH20,
                  ],
                ),
              ),
            ),
    );
  }

  Future<void> submitLaborForm() async {
    final packagesVm = context.read<PackagesViewModel>();

    showLoadingDialog(context, loadingKey);

    await packagesVm.getPackages();

    if (!packagesVm.mesterMesterPost.status) {
      Navigator.of(loadingKey.currentContext!).pop();
      if (mounted) {
        await showUnAvailableDialog(context, packagesVm.mesterMesterPost,
            popTwice: false);
      }
    } else {
      //
      if (_fbKeyTask.currentState!.saveAndValidate() && mounted) {
        final vm = context.read<MesterPartnerProfileViewModel>();

        final payload = {
          'type': 'opgave',
          'industryId':
              (_fbKeyTask.currentState!.value['subject_group'] as Data)
                  .industryId!,
          'subIndustryId': int.parse(_selectedSuProduct!),
          'numberOfStaff':
              int.parse(_fbKeyTask.currentState!.value['unemployed_men']),
          'numberOfOffers': int.parse(_fbKeyTask.currentState!.value['offer']),
          'postalCode': _fbKeyTask.currentState!.value['zipcode'].toString(),
          'startDate': _fbKeyTask.currentState!.value['starting_date']
              .toString()
              .split(' ')
              .first,
          'duration': _fbKeyTask.currentState!.value['duration'].toString(),
          'budget': num.parse(_fbKeyTask.currentState!.value['budget']),
          'description':
              _fbKeyTask.currentState!.value['write_about_you'].toString(),
        };

        //UPLOAD ONLY ONE FOR NOW
        //API IS NOT SET TO ACCEPT MUTLITPLE UPLOADS YET
        final imagesToUpload = _fbKeyTask.currentState!.value['images'];

        if (imagesToUpload.isNotEmpty) {
          payload['images'] = await MultipartFile.fromFile(
              imagesToUpload.first.path,
              filename: imagesToUpload.first.path.split("/").last);
        }

        if (files.isNotEmpty) {
          payload['files'] = await MultipartFile.fromFile(files.first.path,
              filename: files.first.path.split("/").last);
        }

        try {
          await vm.submitLaborOrTask(payload: payload).whenComplete(
            () async {
              if (!mounted) return;

              Navigator.of(loadingKey.currentContext!).pop();
              final success =
                  await showSuccessAnimationDialog(context, true, '');

              if (success && mounted) {
                backDrawerRoute();
              }
            },
          );
        } catch (_) {
          Navigator.of(loadingKey.currentContext!).pop();
          if (mounted) {
            context.showErrorSnackBar();
          }
        }
      }
      //
    }
  }

  clearData() {
    final fields = _fbKeyTask.currentState!.fields;
    fields['subject_group']?.didChange(null);
    fields['unemployed_men']?.didChange('1');
    fields['offer']?.didChange('3');
    fields['zipcode']?.didChange('');
    fields['starting_date']?.reset();
    fields['duration']?.didChange('1t');
    fields['write_about_you']?.didChange('');
    fields['images']?.didChange([]);
    fields['budget']?.didChange('');
    setState(() {
      _selectedSubjectGroup = null;
    });
  }
}
