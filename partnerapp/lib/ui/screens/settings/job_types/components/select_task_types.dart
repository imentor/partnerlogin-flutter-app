part of '../job_types_setting.dart';

class SelectTaskTypes extends StatelessWidget {
  const SelectTaskTypes({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr('select_task_types'),
          style: GoogleFonts.notoSans(
            textStyle: const TextStyle(
              fontSize: 20,
              height: 1.35,
              color: Colors.black,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        SmartGaps.gapH20,
        ...context
            .select<JobTypesViewModel, List<Data?>?>(
                (model) => model.industryList)!
            .map((e) {
          return TaskTypeTemplate(
            layer: TaskTypeLayers.industry,
            industry: e,
          );
        }),
      ],
    );
  }
}
