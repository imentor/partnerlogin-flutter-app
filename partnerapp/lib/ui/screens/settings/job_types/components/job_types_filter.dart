part of '../job_types_setting.dart';

class JobTypesFilter extends StatelessWidget {
  const JobTypesFilter({
    super.key,
    required this.filterType,
  });

  final String filterType;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          filterType == 'region'
              ? tr('select_region')
              : tr('select_business_type'),
          style: GoogleFonts.notoSans(
            textStyle: const TextStyle(
              fontSize: 20,
              height: 1.35,
              color: Colors.black,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        SmartGaps.gapH20,
        if (filterType == 'region')
          ...context
              .select<JobTypesViewModel, List<RegionModel>>(
                  (model) => model.allRegions)
              .map(
                (e) => JobTypeFilterBoxTemplate(
                  label: e.name!.split("Region").last.trim(),
                  isSelected: context
                      .select<JobTypesViewModel, List<int>>(
                          (model) => model.selectedRegions)
                      .contains(e.id),
                  regionId: e.id,
                  filterType: filterType,
                ),
              )
        else
          ...['main_contractor', 'prof_contractor'].map((e) {
            final index = ['main_contractor', 'prof_contractor'].indexOf(e);
            return JobTypeFilterBoxTemplate(
              label: tr(e),
              isSelected: context.select<JobTypesViewModel, bool>(
                  (model) => model.enterpriseBoxValues[index]),
              filterType: filterType,
              index: index,
            );
          }),
      ],
    );
  }
}
