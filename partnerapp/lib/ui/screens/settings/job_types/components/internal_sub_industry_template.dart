part of '../job_types_setting.dart';

class InternalSubIndustryTemplate extends StatefulWidget {
  const InternalSubIndustryTemplate({
    super.key,
    required this.subCategories,
    required this.industryId,
    required this.topSubCategory,
    this.onTaskSelected,
  });
  final List<SubIndustries?>? subCategories;
  final SubIndustries topSubCategory;
  final int? industryId;
  final Function? onTaskSelected;

  @override
  State<InternalSubIndustryTemplate> createState() =>
      _InternalSubIndustryTemplateState();
}

class _InternalSubIndustryTemplateState
    extends State<InternalSubIndustryTemplate> {
  bool? checkAll;

  @override
  void initState() {
    checkAll = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}
