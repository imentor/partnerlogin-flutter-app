part of '../job_types_setting.dart';

class SubmitJobTypesButton extends StatelessWidget {
  const SubmitJobTypesButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 10),
      height: 60,
      child: CustomDesignTheme.flatButtonStyle(
        backgroundColor: context.select<JobTypesViewModel, bool>((model) =>
                const ListEquality().equals(model.taskTemplate, model.tasks))
            ? Colors.grey
            : Theme.of(context).colorScheme.secondary,
        onPressed: context.select<JobTypesViewModel, bool>((model) =>
                const ListEquality().equals(model.taskTemplate, model.tasks))
            ? null
            : () async {
                final jobTypesVm = context.read<JobTypesViewModel>();
                final currentContext =
                    myGlobals.homeScaffoldKey?.currentContext ?? context;
                final response = await showOkCancelAlertDialog(
                    context: context,
                    title: tr('confirmation'),
                    message: tr('confirm_selected_task_types'),
                    okLabel: tr('confirm'),
                    cancelLabel: tr('cancel'));

                if (response == OkCancelResult.ok && context.mounted) {
                  modalManager.showLoadingModal();
                  await tryCatchWrapper(
                          context: currentContext,
                          function: jobTypesVm.newproducttask())
                      .then((model) async {
                    if ((model?.success ?? false) && context.mounted) {
                      jobTypesVm.taskTemplate = <int>[...jobTypesVm.tasks];
                      modalManager.hideLoadingModal();
                      await showSuccessAnimationDialog(context,
                          (model?.success ?? false), (model?.message ?? ''));
                    }
                  });
                }
              },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(tr('save_task_types'),
                style: const TextStyle(color: Colors.white)),
            SmartGaps.gapW5,
            const Icon(Icons.arrow_forward, color: Colors.white),
          ],
        ),
      ),
    );
  }
}
