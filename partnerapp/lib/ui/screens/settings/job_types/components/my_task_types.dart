part of '../job_types_setting.dart';

class MyTaskTypes extends StatelessWidget {
  const MyTaskTypes({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        //

        //HEADER
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              tr('my_task_types'),
              style: GoogleFonts.notoSans(
                textStyle: const TextStyle(
                  fontSize: 20,
                  height: 1.35,
                  color: Colors.black,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                final jobTypesVm = context.read<JobTypesViewModel>();
                jobTypesVm.tasks = [];
                jobTypesVm.industryTasks = [];
              },
              child: Text(
                tr('clear_filter'),
                style: GoogleFonts.notoSans(
                  textStyle: const TextStyle(
                    fontSize: 14,
                    height: 1.35,
                    color: PartnerAppColors.accentBlue,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ),
          ],
        ),
        SmartGaps.gapH30,

        //MAIN LIST OF SELECTED TASK TYPES
        Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 15),
          decoration: BoxDecoration(
            border: Border.all(
              color: PartnerAppColors.darkBlue,
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: Wrap(
            children: List.generate(
              context.select<JobTypesViewModel, int>(
                  (model) => model.tasks.length),
              (index) {
                final taskType = context.select<JobTypesViewModel, TaskTypes>(
                    (model) => model.getTaskTypesCRM(index: index));
                return Container(
                  decoration: BoxDecoration(
                    color: PartnerAppColors.darkBlue,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 11, vertical: 7),
                  margin: const EdgeInsets.only(right: 5, bottom: 5),
                  child: Text(
                    taskType.name != null && taskType.name != ''
                        ? taskType.name!
                        : taskType.subcategoryName ?? '',
                    style: GoogleFonts.notoSans(
                      textStyle: const TextStyle(
                        fontSize: 12,
                        height: 1.35,
                        color: Colors.white,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ),

        //
      ],
    );
  }
}
