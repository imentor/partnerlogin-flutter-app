part of '../job_types_setting.dart';

class TaskTypeTemplate extends StatefulWidget {
  const TaskTypeTemplate({
    super.key,
    this.industry,
    required this.layer,
    this.subCategories,
    this.topSubCategory,
    this.industryId,
    this.onTaskSelected,
    this.taskName,
    this.taskSubIndustryId,
  });

  //LAYER IDENTIFIER
  final TaskTypeLayers layer;

  //THIS DATA IS USED
  //FOR INDUSTRY LAYER
  final Data? industry;

  //DATA USED FOR
  //SUB INDUSTRY LAYER
  final List<SubIndustries?>? subCategories;
  final SubIndustries? topSubCategory;
  final int? industryId;

  final Function? onTaskSelected;

  //DATA USED FOR
  //TASK TYPE LAYER
  final String? taskName;
  final int? taskSubIndustryId;

  @override
  State<TaskTypeTemplate> createState() => _TaskTypeTemplateState();
}

class _TaskTypeTemplateState extends State<TaskTypeTemplate> {
  bool? checkAll;

  @override
  void initState() {
    checkAll = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.layer == TaskTypeLayers.taskType) {
      return CheckboxListTile(
          controlAffinity: ListTileControlAffinity.leading,
          contentPadding: const EdgeInsets.only(left: 40),
          title: Text(
            widget.taskName!,
            style: GoogleFonts.notoSans(
              textStyle: const TextStyle(
                fontSize: 18,
                height: 1.33,
                color: Colors.black,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
          activeColor: PartnerAppColors.darkBlue,
          value: context.select<JobTypesViewModel, bool>(
              (model) => model.tasks.contains(widget.taskSubIndustryId)),
          onChanged: (value) {
            //

            final jobTypesVm = context.read<JobTypesViewModel>();
            final tasks = <int>[...jobTypesVm.tasks];
            final industryTasks = <int>[...jobTypesVm.industryTasks];

            //

            final listOfTopSubs =
                jobTypesVm.taskMap[widget.industryId]!.keys.toList().map((e) {
              if (tasks.contains(e)) {
                return e;
              }
            }).toList();
            listOfTopSubs.removeWhere((element) => element == null);

            final listOfChildren = jobTypesVm.taskMap[widget.industryId]![
                    widget.topSubCategory!.subcategoryId]!
                .map((e) {
              if (jobTypesVm.tasks.contains(e)) {
                return e;
              }
            }).toList();

            listOfChildren.removeWhere((element) => element == null);

            if (value!) {
              //ADDING ONE CHILD
              tasks.add(widget.taskSubIndustryId!);

              if (listOfChildren.isEmpty) {
                tasks.add(widget.topSubCategory!.subcategoryId!);
              }

              if (listOfTopSubs.isEmpty) {
                industryTasks.add(widget.industryId!);
                jobTypesVm.industryTasks = industryTasks;
              }

              //
            } else {
              //REMOVING ONE CHILD
              tasks.removeWhere(
                  (element) => element == widget.taskSubIndustryId);

              listOfChildren.removeWhere(
                  (element) => element == widget.taskSubIndustryId);

              //CHECKING WHETHER THERE IS NONE LEFT
              if (listOfChildren.isEmpty) {
                tasks.removeWhere((element) =>
                    element == widget.topSubCategory!.subcategoryId);
                listOfTopSubs.removeWhere((element) =>
                    element == widget.topSubCategory!.subcategoryId!);
              }

              //CHECK IF THERE'S
              if (listOfTopSubs.isEmpty) {
                industryTasks
                    .removeWhere((element) => element == widget.industryId!);
                jobTypesVm.industryTasks = industryTasks;
              }
              //
            }

            jobTypesVm.tasks = tasks;
          });
    } else {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Theme(
          data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
          child: ExpansionTile(
            tilePadding: EdgeInsets.only(
                right: 10,
                left: widget.layer == TaskTypeLayers.subIndustry ? 20 : 0),
            backgroundColor: const Color(0xffF5F5F5),
            collapsedBackgroundColor: const Color(0xffF5F5F5),
            trailing: widget.layer == TaskTypeLayers.subIndustry
                ? widget.subCategories!.isEmpty
                    ? const SizedBox.shrink()
                    : const Icon(Icons.expand_more, color: Colors.black)
                : const Icon(Icons.expand_more, color: Colors.black),
            title: CheckboxListTile(
              activeColor: PartnerAppColors.darkBlue,
              contentPadding: EdgeInsets.zero,
              controlAffinity: ListTileControlAffinity.leading,
              title: Text(
                widget.layer == TaskTypeLayers.industry
                    ? widget.industry!.industryName!
                    : context.locale.languageCode == 'da'
                        ? widget.topSubCategory!.subCategoryName!
                        : widget.topSubCategory!.subCategoryNameEn!,
                style: GoogleFonts.notoSans(
                  textStyle: const TextStyle(
                    fontSize: 18,
                    height: 1.33,
                    color: Colors.black,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              onChanged: (value) {
                final jobTypesVm = context.read<JobTypesViewModel>();
                if (widget.layer == TaskTypeLayers.industry) {
                  final tasks = <int>[...jobTypesVm.tasks];
                  final industryTasks = <int>[...jobTypesVm.industryTasks];
                  List<int> selectedTasks = [];
                  for (final int id
                      in jobTypesVm.taskMap[widget.industry!.branchId!]!.keys) {
                    selectedTasks.addAll([
                      id,
                      ...jobTypesVm.taskMap[widget.industry!.branchId!]![id]!
                    ]);
                  }

                  if (value!) {
                    tasks.addAll(selectedTasks);
                    industryTasks.add(widget.industry!.branchId!);
                  } else {
                    tasks.removeWhere(
                        (element) => selectedTasks.contains(element));
                    industryTasks.removeWhere(
                        (element) => element == widget.industry!.branchId);
                  }

                  jobTypesVm.tasks = tasks;
                  industryTasks.sort((a, b) => a.compareTo(b));
                  jobTypesVm.industryTasks = industryTasks;
                } else {
                  final list = widget.subCategories!
                      .map((e) => e!.subcategoryId!)
                      .toList();
                  final tasks = <int>[...jobTypesVm.tasks];
                  //

                  final listOfTopSubs = jobTypesVm
                      .taskMap[widget.industryId]!.keys
                      .toList()
                      .map((e) {
                    if (tasks.contains(e)) {
                      return e;
                    }
                  }).toList();

                  listOfTopSubs.removeWhere((element) => element == null);

                  //
                  if (value!) {
                    tasks.addAll(
                        [widget.topSubCategory!.subcategoryId!, ...list]);

                    //LETS CHECK IF THERE ARE NO MORE TOP SUBS FOR THIS INDUSTRY
                    //WE UPDATE THE CHECKBOX FOR THE INDUSTRY IF THERE'S ATLEAST
                    //ONE TOP SUB CHECKED
                    if (listOfTopSubs.isEmpty) {
                      final industryTasks = <int>[...jobTypesVm.industryTasks];
                      industryTasks.add(widget.industryId!);
                      jobTypesVm.industryTasks = industryTasks;
                    }
                  } else {
                    //
                    //NORMAL REMOVE
                    tasks.removeWhere(
                      (element) => [
                        widget.topSubCategory!.subcategoryId!,
                        ...list
                      ].contains(element),
                    );

                    listOfTopSubs.removeWhere((element) =>
                        element == widget.topSubCategory!.subcategoryId!);

                    log("TOP SUBS: $listOfTopSubs");
                    //LETS CHECK IF THERE ARE NO MORE TOP SUBS FOR THIS INDUSTRY
                    //WE UPDATE THE CHECKBOX FOR THE INDUSTRY IF THERE'S NO MORE
                    //TOP SUBS CHECKED
                    if (listOfTopSubs.isEmpty) {
                      log("REMOVED");
                      final industryTasks = <int>[...jobTypesVm.industryTasks];
                      industryTasks.removeWhere(
                          (element) => widget.industryId == element);

                      jobTypesVm.industryTasks = industryTasks;
                    }

                    //
                  }

                  jobTypesVm.tasks = tasks;
                }
              },
              value: widget.layer == TaskTypeLayers.industry
                  ? context.select<JobTypesViewModel, bool>((model) =>
                      model.industryTasks.contains(widget.industry!.branchId))
                  : context.select<JobTypesViewModel, bool>((model) => model
                      .tasks
                      .contains(widget.topSubCategory!.subcategoryId)),
            ),
            children: [
              if (widget.layer == TaskTypeLayers.industry)
                ...context
                    .select<JobTypesViewModel, List<SubIndustries?>>((model) =>
                        model.getTopSubIndustries(
                            subinds: widget.industry!.subIndustries!))
                    .map((e) {
                  return TaskTypeTemplate(
                    layer: TaskTypeLayers.subIndustry,
                    topSubCategory: e,
                    subCategories:
                        context.select<JobTypesViewModel, List<SubIndustries?>>(
                            (model) => model.getInternalSubIndustries(
                                parentId: e!.subcategoryId,
                                subinds: widget.industry!.subIndustries!)),
                    industryId: widget.industry!.branchId!,
                  );
                })
              else
                ...widget.subCategories!.map((e) {
                  return TaskTypeTemplate(
                    layer: TaskTypeLayers.taskType,
                    taskName: context.locale.languageCode == 'da'
                        ? e!.subCategoryName!
                        : e!.subCategoryNameEn!,
                    taskSubIndustryId: e.subcategoryId,
                    topSubCategory: widget.topSubCategory,
                    industryId: widget.industryId,
                  );
                })
            ],
          ),
        ),
      );
    }
  }
}
