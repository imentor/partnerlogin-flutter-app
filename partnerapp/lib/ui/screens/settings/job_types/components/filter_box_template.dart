part of '../job_types_setting.dart';

class JobTypeFilterBoxTemplate extends StatelessWidget {
  const JobTypeFilterBoxTemplate({
    super.key,
    required this.label,
    required this.isSelected,
    this.regionId,
    this.index,
    required this.filterType,
  });

  final String label;
  final bool isSelected;
  final int? regionId;
  final int? index;
  final String filterType;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: CheckboxListTile(
        title: Text(
          label,
          style: GoogleFonts.notoSans(
            textStyle: const TextStyle(
              fontSize: 18,
              height: 1.33,
              color: Colors.black,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        tileColor: const Color(0xffF5F5F5),
        dense: true,
        contentPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 7),
        controlAffinity: ListTileControlAffinity.trailing,
        value: isSelected,
        activeColor:
            WidgetStateColor.resolveWith((states) => PartnerAppColors.blue),
        onChanged: filterType != 'region'
            ? context.select<JobTypesViewModel, bool>(
                    (model) => model.isEnterpriseUpdating)
                ? null
                : (val) async {
                    final jobTypesVm = context.read<JobTypesViewModel>();
                    final otherIndex = index == 0 ? index! + 1 : index! - 1;
                    final boxValues = <bool>[...jobTypesVm.enterpriseBoxValues];
                    int fieldValue = 0;
                    if (boxValues[otherIndex]) {
                      jobTypesVm.isEnterpriseUpdating = true;
                      boxValues[index!] = val!;

                      log('VALUES: $boxValues');
                      if (boxValues.any((element) => !element)) {
                        if (boxValues.first) {
                          fieldValue = 1;
                        } else {
                          fieldValue = 2;
                        }
                      } else {
                        fieldValue = 3;
                      }

                      jobTypesVm.enterpriseBoxValues = boxValues;

                      await jobTypesVm
                          .updateSupplierInfo(
                              fields: FormData.fromMap(
                        {'UF_CRM_COMPANY_ENTERPRISE': fieldValue},
                      ))
                          .then((value) async {
                        if (!context.mounted) return;

                        jobTypesVm.isEnterpriseUpdating = false;
                        if (value.success!) {
                          await showSuccessAnimationDialog(
                            myGlobals.homeScaffoldKey!.currentContext!,
                            true,
                            tr('success!'),
                          );
                        } else {
                          await showErrorDialog(
                            context,
                            tr('error'),
                            value.message!,
                            tr('back'),
                          );
                        }
                      });
                    }
                  }
            : context.select<JobTypesViewModel, bool>(
                        (model) => model.isRegionUpdating) ||
                    label ==
                        context.select<JobTypesViewModel, String>(
                          (model) => model.company!.areaText!
                              .split('Region')
                              .last
                              .trim(),
                        )
                ? null
                : (val) async {
                    final jobTypesVm = context.read<JobTypesViewModel>();
                    final regions = <int>[...jobTypesVm.selectedRegions];

                    if (val!) {
                      regions.add(regionId!);
                    } else {
                      regions.removeWhere((element) => element == regionId);
                    }
                    jobTypesVm.selectedRegions = regions;

                    await tryCatchWrapper(
                      context: myGlobals.homeScaffoldKey!.currentContext,
                      function: jobTypesVm.editRegions(),
                    ).then((value) async {
                      if (!context.mounted) return;

                      jobTypesVm.isRegionUpdating = false;
                      if (value!.success!) {
                        await showSuccessAnimationDialog(
                          myGlobals.homeScaffoldKey!.currentContext!,
                          true,
                          tr('success!'),
                        );
                      } else {
                        await showErrorDialog(
                          context,
                          tr('error'),
                          value.message!,
                          tr('back'),
                        );
                      }
                    });
                  },
      ),
    );
  }
}
