library job_types_setting;

import 'dart:developer';

import 'package:Haandvaerker.dk/model/get_types_response_model.dart';
import 'package:Haandvaerker.dk/model/settings/regions_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/job_types_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:collection/collection.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

part 'components/filter_box_template.dart';
part 'components/internal_sub_industry_template.dart';
part 'components/job_types_filter.dart';
part 'components/my_task_types.dart';
part 'components/select_task_types.dart';
part 'components/submit_job_types_button.dart';
part 'components/task_types_template.dart';

enum TaskTypeLayers {
  industry,
  subIndustry,
  taskType,
}

class JobTypesSetting extends StatefulWidget {
  const JobTypesSetting({super.key});

  @override
  State<JobTypesSetting> createState() => _JobTypesSettingState();
}

class _JobTypesSettingState extends State<JobTypesSetting> {
  //

  Future<void> initializeData() async {
    final jobTypesVm = context.read<JobTypesViewModel>();
    jobTypesVm.setBusy(true);
    await Future.wait(
      [
        jobTypesVm.getAllRegions(),
        jobTypesVm.getRegions(),
        jobTypesVm.getSupplierProfileById(),
      ],
    );
    await jobTypesVm.getPartnerProfile();

    jobTypesVm.setBusy(false);
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final appDrawerVm = context.read<AppDrawerViewModel>();
      final newsFeedVm = context.read<NewsFeedViewModel>();
      const dynamicStoryPage = 'opgavetyper';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function: initializeData(),
      );
      if (dynamicStoryEnable) {
        await Future.value(newsFeedVm.getDynamicStory(page: dynamicStoryPage))
            .whenComplete(() {
          if (!mounted) return;

          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //

              //HEADER
              SmartGaps.gapH30,
              Text(
                tr('job_types'),
                style: GoogleFonts.notoSans(
                  textStyle: const TextStyle(
                    fontSize: 20,
                    height: 1.35,
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              SmartGaps.gapH18,
              Text(
                tr('job_type_description'),
                textAlign: TextAlign.left,
                style: GoogleFonts.notoSans(
                  textStyle: const TextStyle(
                    fontSize: 12,
                    height: 2.08,
                    letterSpacing: 0.96,
                    color: Color(0xff707070),
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),

              if (context
                      .select<JobTypesViewModel, bool>((model) => model.busy) ||
                  context.watch<JobTypesViewModel>().company == null)
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 100),
                  child: Center(child: ConnectivityLoader()),
                )
              else ...[
                SmartGaps.gapH30,

                //REGION FILTER
                const JobTypesFilter(filterType: 'region'),

                SmartGaps.gapH30,

                //ENTERPRISE FILTER
                const JobTypesFilter(filterType: 'enterprise'),

                SmartGaps.gapH30,

                //MY TASK TYPES
                if (context.select<JobTypesViewModel, bool>(
                    (model) => model.tasks.isNotEmpty))
                  const MyTaskTypes(),

                SmartGaps.gapH30,

                //SELECT TASK TYPES
                const SelectTaskTypes(),

                //
                const SubmitJobTypesButton(),
              ],

              //
            ],
          ),
        ),
      ),
    );
  }
}
