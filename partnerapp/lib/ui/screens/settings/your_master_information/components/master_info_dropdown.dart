import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';

class MasterInfoDropdown extends StatelessWidget {
  const MasterInfoDropdown({
    super.key,
    required this.items,
    required this.onChanged,
    required this.width,
    this.initialValue,
    this.validator,
  });

  final List<DropdownMenuItem> items;
  final Function(dynamic) onChanged;
  final double width;
  final String? initialValue;
  final String? Function(dynamic)? validator;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: DropdownButtonFormField2(
        value: initialValue,
        items: items,
        onChanged: onChanged,
        isExpanded: true,
        validator: validator,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.all(10),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: const BorderSide(
              color: Colors.black,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: const BorderSide(
              color: Colors.black,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: const BorderSide(
              color: Colors.black,
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: const BorderSide(
              color: Colors.red,
            ),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: const BorderSide(
              color: Colors.red,
            ),
          ),
        ),
        dropdownStyleData: DropdownStyleData(
          maxHeight: 300,
          offset: const Offset(0, 10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
          ),
        ),
      ),
    );
  }
}
