library your_master_info;

import 'dart:developer';

import 'package:Haandvaerker.dk/model/settings/master_info_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/settings/your_master_information/components/master_info_dropdown.dart';
import 'package:Haandvaerker.dk/utils/extensions.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/your_master_information_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:show_up_animation/show_up_animation.dart';

part 'components/master_info_text_box.dart';
part 'master_info_dynamic_form.dart';

class YourMasterInformation extends StatefulWidget {
  const YourMasterInformation({super.key});

  @override
  State<YourMasterInformation> createState() => _YourMasterInformationState();
}

class _YourMasterInformationState extends State<YourMasterInformation> {
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();

  final _formKey = GlobalKey<FormState>();

  void onPressedSave() async {
    final masterInfoVm = context.read<YourMasterInfoViewModel>();

    Map<String, dynamic> tempData = <String, dynamic>{
      ...masterInfoVm.masterInfoData
    };

    if (_formKey.currentState!.validate()) {
      for (final info in masterInfoVm.masterInfo) {
        if (info.labelType == 'CHECKBOX') {
          if (!List<dynamic>.from(
                  masterInfoVm.masterInfoData[info.name!]['value'])
              .contains(1)) {
            tempData[info.name!]['valid'] = false;
            masterInfoVm.validate = true;
            Scrollable.ensureVisible(
              (masterInfoVm.masterInfoData[info.name!]['key'] as GlobalKey)
                  .currentContext!,
              alignment: 0.5,
            );
          } else {
            tempData[info.name!]['valid'] = true;
          }
          masterInfoVm.masterInfoData = tempData;
        }
      }

      if (!masterInfoVm.masterInfoData.values
          .any((element) => element['valid'] == false)) {
        showLoadingDialog(context, loadingKey);

        try {
          await masterInfoVm.saveMasterInfo().then((response) async {
            if (response && mounted) {
              Navigator.of(loadingKey.currentContext!, rootNavigator: true)
                  .pop();
              await showSuccessAnimationDialog(
                  context, true, tr('master_info_update_success'));
              await masterInfoVm.getMasterInfo();
            }
          });
        } catch (_) {
          Navigator.of(loadingKey.currentContext!, rootNavigator: true).pop();

          if (!mounted) return;

          await showOkAlertDialog(
            context: context,
            title: tr('error'),
            message: tr('master_info_update_error'),
            okLabel: tr('ok'),
          );
        }
      }
    }
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final newsFeedVm = context.read<NewsFeedViewModel>();
      final appDrawerVm = context.read<AppDrawerViewModel>();
      const dynamicStoryPage = 'stamoplysninger';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      if (dynamicStoryEnable) {
        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: Future.wait([
              context.read<YourMasterInfoViewModel>().getMasterInfo(),
              newsFeedVm.getDynamicStory(page: dynamicStoryPage),
            ])).whenComplete(() {
          if (!mounted) return;

          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      } else {
        await tryCatchWrapper(
            context: myGlobals.homeScaffoldKey!.currentContext,
            function: Future.value(
                context.read<YourMasterInfoViewModel>().getMasterInfo()));
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Consumer<YourMasterInfoViewModel>(
        builder: (context, masterInfoVm, _) {
          //

          return SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //

                    SmartGaps.gapH10,

                    Text(
                      tr('your_master_information'),
                      style: Theme.of(context).textTheme.headlineLarge,
                    ),

                    if (masterInfoVm.busy ||
                        masterInfoVm.masterInfoData.isEmpty ||
                        masterInfoVm.masterInfo.isEmpty)
                      const SizedBox(
                        height: 250,
                        child: Center(
                          child: ConnectivityLoader(),
                        ),
                      )
                    //
                    else ...[
                      SmartGaps.gapH30,
                      ...masterInfoVm.masterInfo.map(
                        (info) {
                          return Padding(
                            padding: const EdgeInsets.only(bottom: 20),
                            child: MasterInfoDyanmicForm(info: info),
                          );
                        },
                      ),

                      CustomDesignTheme.flatButtonStyle(
                        backgroundColor: PartnerAppColors.green,
                        fixedSize: Size(MediaQuery.of(context).size.width, 50),
                        child: Text(
                          tr('save'),
                          style: const TextStyle(color: Colors.white),
                        ),
                        onPressed: () => onPressedSave(),
                      ),

                      SmartGaps.gapH30,
                      //
                    ],
                  ],
                ),
              ),
            ),
          );

          //
        },
      ),
    );
  }
}
