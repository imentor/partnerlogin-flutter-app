part of 'your_master_information.dart';

class MasterInfoDyanmicForm extends StatefulWidget {
  const MasterInfoDyanmicForm({super.key, required this.info});

  final MasterInfoGeneralModel info;

  @override
  State<MasterInfoDyanmicForm> createState() => _MasterInfoDyanmicFormState();
}

class _MasterInfoDyanmicFormState extends State<MasterInfoDyanmicForm> {
  late TextEditingController _textBoxController;
  late TextEditingController _firstSubController;
  late TextEditingController _secondSubController;

  @override
  void initState() {
    _textBoxController = TextEditingController();
    _firstSubController = TextEditingController();
    _secondSubController = TextEditingController();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final masterInfoData =
          context.read<YourMasterInfoViewModel>().masterInfoData;

      if (widget.info.labelType == 'TEXTBOX') {
        if (widget.info.subLabelDA.isNotEmpty) {
          _firstSubController.text =
              masterInfoData[widget.info.name]['value'][0] == null
                  ? ''
                  : masterInfoData[widget.info.name]['value'][0].toString();

          _secondSubController.text =
              masterInfoData[widget.info.name]['value'][1] == null
                  ? ''
                  : masterInfoData[widget.info.name]['value'][1].toString();
        } else {
          _textBoxController.text =
              masterInfoData[widget.info.name]['value'] ?? '';
        }
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _textBoxController.dispose();
    _firstSubController.dispose();
    _secondSubController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final masterInfoData =
        context.watch<YourMasterInfoViewModel>().masterInfoData;
    switch (widget.info.labelType) {
      //

      case 'TEXTBOX':
        final subLabel = context.locale.languageCode == 'da'
            ? widget.info.subLabelDA
            : widget.info.subLabelEN;
        if (subLabel.isNotEmpty) {
          //THIS IS A SPECIAL CASE FOR NOW\
          return Column(
            key: masterInfoData[widget.info.name]['key'],
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                context.locale.languageCode == 'da'
                    ? widget.info.labelDA
                    : widget.info.labelEN,
              ),
              SmartGaps.gapH15,
              Wrap(
                crossAxisAlignment: WrapCrossAlignment.center,
                children: [
                  Text(subLabel.split('||').first),
                  MasterInfoTextBox(
                    size: const Size(100, 50),
                    controller: _firstSubController,
                    onChanged: (val) {
                      Map<String, dynamic> temp = {...masterInfoData};
                      if (val.isNotEmpty) {
                        temp[widget.info.name!]['value'][0] = int.parse(val);
                        temp[widget.info.name!]['valid'] = true;
                      } else {
                        temp[widget.info.name!]['valid'] = false;
                      }

                      context.read<YourMasterInfoViewModel>().masterInfoData =
                          temp;
                    },
                    inputFormatters: [
                      FilteringTextInputFormatter.deny(
                        RegExp('[\\.|\\,|\\-]'),
                      ),
                    ],
                    inputType: TextInputType.number,
                    validator: (val) {
                      if (val.isNullOrEmpty) {
                        Scrollable.ensureVisible(
                            (masterInfoData[widget.info.name]['key']
                                    as GlobalKey)
                                .currentContext!,
                            alignment: 0.5);
                        return tr('required');
                      }
                      return null;
                    },
                  ),
                  Text(subLabel.split('||')[1]),
                  MasterInfoTextBox(
                    size: const Size(100, 50),
                    controller: _secondSubController,
                    onChanged: (val) {
                      Map<String, dynamic> temp = {...masterInfoData};
                      if (val.isNotEmpty) {
                        temp[widget.info.name!]['value'][1] = int.parse(val);
                        temp[widget.info.name!]['valid'] = true;
                      } else {
                        temp[widget.info.name!]['valid'] = false;
                      }

                      context.read<YourMasterInfoViewModel>().masterInfoData =
                          temp;
                    },
                    inputFormatters: [
                      FilteringTextInputFormatter.deny(
                        RegExp('[\\.|\\,|\\-]'),
                      ),
                    ],
                    inputType: TextInputType.number,
                    validator: (val) {
                      if (val.isNullOrEmpty) {
                        Scrollable.ensureVisible(
                            (masterInfoData[widget.info.name]['key']
                                    as GlobalKey)
                                .currentContext!,
                            alignment: 0.5);
                        return tr('required');
                      }
                      return null;
                    },
                  ),
                  Text(subLabel.split('||').last),
                ],
              ),
            ],
          );
        } else {
          return Column(
            key: masterInfoData[widget.info.name]['key'],
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                context.locale.languageCode == 'da'
                    ? widget.info.labelDA
                    : widget.info.labelEN,
              ),
              SmartGaps.gapH15,
              MasterInfoTextBox(
                size: Size(MediaQuery.of(context).size.width, 50),
                controller: _textBoxController,
                onChanged: (val) {
                  Map<String, dynamic> temp = {...masterInfoData};
                  if (val.isNotEmpty) {
                    temp[widget.info.name!]['value'] = val;
                    temp[widget.info.name!]['valid'] = true;
                  } else {
                    temp[widget.info.name!]['valid'] = false;
                  }

                  context.read<YourMasterInfoViewModel>().masterInfoData = temp;
                },
                validator: (val) {
                  if (val.isNullOrEmpty) {
                    Scrollable.ensureVisible(
                        (masterInfoData[widget.info.name]['key'] as GlobalKey)
                            .currentContext!,
                        alignment: 0.5);
                    return tr('required');
                  }
                  return null;
                },
              ),
            ],
          );
        }

      case 'SELECTBOX':
        if (widget.info.name == 'UF_STARTWORKMONTH') {
          final choices1 = context.locale.languageCode == 'da'
              ? widget.info.choicesDA
              : widget.info.choicesEN;
          final choices2 = context.locale.languageCode == 'da'
              ? widget.info.choicesDA2
              : widget.info.choicesEN2;

          final items1 = choices1
              .map(
                (e) => DropdownMenuItem(
                  value: e,
                  child: Text(
                    e,
                    textAlign: TextAlign.right,
                  ),
                ),
              )
              .toList();

          final items2 = choices2
              .map(
                (e) => DropdownMenuItem(
                  value: e,
                  child: Text(
                    e,
                    textAlign: TextAlign.right,
                  ),
                ),
              )
              .toList();

          final label = context.locale.languageCode == 'da'
              ? widget.info.labelDA
              : widget.info.labelEN;

          final firstLabel = label.split('||').first;
          final secondLabel = label.split('||')[1];
          final lastLabel = label.split('||').last;
          final firstInitial = masterInfoData[widget.info.name]['value'][0] ==
                  null
              ? null
              : List<dynamic>.from(masterInfoData[widget.info.name]['value'])
                  .first
                  .toString();

          final secondInitial = masterInfoData[widget.info.name]['value'][1] ==
                  null
              ? null
              : List<dynamic>.from(masterInfoData[widget.info.name]['value'])
                  .last
                  .toString();

          return Wrap(
            key: masterInfoData[widget.info.name]['key'],
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              Text(firstLabel),
              MasterInfoDropdown(
                initialValue: firstInitial,
                width: 100,
                items: items1,
                onChanged: (val) {
                  final name = widget.info.name!;
                  Map<String, dynamic> temp = {...masterInfoData};
                  temp[name]['value'][0] = int.parse(val);
                  temp[name]['valid'] = true;
                  context.read<YourMasterInfoViewModel>().masterInfoData = temp;
                },
                validator: (val) {
                  if (val == null) {
                    Scrollable.ensureVisible(
                        (masterInfoData[widget.info.name]['key'] as GlobalKey)
                            .currentContext!,
                        alignment: 0.5);
                    return tr('required');
                  }
                  return null;
                },
              ),
              Text(secondLabel),
              MasterInfoDropdown(
                initialValue: secondInitial,
                width: 100,
                items: items2,
                onChanged: (val) {
                  final name = widget.info.name!;
                  Map<String, dynamic> temp = {...masterInfoData};
                  temp[name]['value'][1] = int.parse(val);
                  temp[name]['valid'] = true;
                  context.read<YourMasterInfoViewModel>().masterInfoData = temp;
                },
                validator: (val) {
                  if (val == null) {
                    Scrollable.ensureVisible(
                        (masterInfoData[widget.info.name]['key'] as GlobalKey)
                            .currentContext!,
                        alignment: 0.5);
                    return tr('required');
                  }
                  return null;
                },
              ),
              Text(lastLabel),
            ],
          );
        } else {
          final choices = context.locale.languageCode == 'da'
              ? widget.info.choicesDA
              : widget.info.choicesEN;

          final items = choices
              .map(
                (e) => DropdownMenuItem(
                  value: e,
                  child: Text(
                    e,
                    textAlign: TextAlign.right,
                  ),
                ),
              )
              .toList();

          return Row(
            key: masterInfoData[widget.info.name]['key'],
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  context.locale.languageCode == 'da'
                      ? widget.info.labelDA
                      : widget.info.labelEN,
                ),
              ),
              SmartGaps.gapW20,
              MasterInfoDropdown(
                initialValue: masterInfoData[widget.info.name]['value'],
                width: 100,
                items: items,
                onChanged: (val) {
                  final name = widget.info.name!;
                  Map<String, dynamic> temp = {...masterInfoData};
                  temp[name]['value'] = val;
                  temp[name]['valid'] = true;
                  context.read<YourMasterInfoViewModel>().masterInfoData = temp;
                },
                validator: (val) {
                  if (val == null) {
                    Scrollable.ensureVisible(
                        (masterInfoData[widget.info.name]['key'] as GlobalKey)
                            .currentContext!,
                        alignment: 0.5);
                    return tr('required');
                  }
                  return null;
                },
              ),
            ],
          );
        }

      case 'CHECKBOX':
        final isDA = context.locale.languageCode == 'da';
        final label = isDA ? widget.info.labelDA : widget.info.labelEN;
        final choices = isDA ? widget.info.choicesDA : widget.info.choicesEN;

        return Column(
          key: masterInfoData[widget.info.name]['key'],
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(label),
            SmartGaps.gapH10,
            ...choices.map((e) {
              final index = choices.indexOf(e);
              final name = widget.info.name!;
              if (name != 'UF_CRM_COMPANY_PHONE_SELECT' &&
                  name != 'UF_CRM_COMPANY_HOUR_SELECT') {
                return CheckboxListTile(
                  contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                  value: masterInfoData[name]['value'] is List
                      ? masterInfoData[name]['value'][index] == 1
                      : masterInfoData[name]['value'] == e,
                  onChanged: (val) {
                    Map<String, dynamic> temp = <String, dynamic>{
                      ...masterInfoData
                    };
                    final name = widget.info.name!;

                    if (temp[name]['value'] is List) {
                      temp[name]['value'][index] = val == false ? 0 : 1;
                    } else {
                      temp[name]['value'] = val;
                    }

                    context.read<YourMasterInfoViewModel>().masterInfoData =
                        temp;
                  },
                  title: Text(e),
                  controlAffinity: ListTileControlAffinity.leading,
                );
              } else {
                return RadioListTile(
                  contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                  value: masterInfoData[name]['value'][index] == 1,
                  groupValue: true,
                  onChanged: (val) {
                    log('$val');
                    Map<String, dynamic> temp = <String, dynamic>{
                      ...masterInfoData
                    };
                    final name = widget.info.name!;

                    for (int i = 0; i < widget.info.choicesDA.length; i++) {
                      if (i == index) {
                        temp[name]['value'][i] = 1;
                      } else {
                        temp[name]['value'][i] = 0;
                      }
                    }

                    context.read<YourMasterInfoViewModel>().masterInfoData =
                        temp;
                  },
                  title: Text(e),
                  controlAffinity: ListTileControlAffinity.leading,
                );
              }
            }),
            if (!List<dynamic>.from(masterInfoData[widget.info.name]['value'])
                    .contains(1) &&
                context.watch<YourMasterInfoViewModel>().validate)
              ShowUpAnimation(
                  //delayStart: Duration(seconds: 1),
                  animationDuration: const Duration(milliseconds: 300),
                  curve: Curves.ease,
                  direction: Direction.vertical,
                  offset: -0.5,
                  child: Container(
                    width: double.infinity,
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Text(
                      tr('required'),
                      textAlign: TextAlign.start,
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          color: Colors.red,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w200),
                    ),
                  )),
          ],
        );

      default:
        return const SizedBox.shrink();

      //
    }
  }
}
