import 'package:Haandvaerker.dk/ui/components/custom_button.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/your_profile_page_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:provider/provider.dart';

class HtmlEditorScreen extends StatefulWidget {
  const HtmlEditorScreen({super.key});

  @override
  State<HtmlEditorScreen> createState() => _HtmlEditorScreenState();
}

class _HtmlEditorScreenState extends State<HtmlEditorScreen> {
  final messageController = HtmlEditorController();

  void onPressedSaveChanges() async {
    final profileVm = context.read<YourProfilePageViewModel>();
    final body = await messageController.getText();

    if (mounted && body.isNotEmpty) {
      modalManager.showLoadingModal();

      await tryCatchWrapper(
          context: context,
          function: profileVm
              .updateSupplierInfo(payload: {'profileText': body})).then(
        (value) async {
          modalManager.hideLoadingModal();

          if (mounted && (value ?? false)) {
            profileVm.profileText = body;
            await showSuccessAnimationDialog(context, true, '');
          }
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            HtmlEditor(
              controller: messageController,
              htmlEditorOptions: HtmlEditorOptions(
                  hint: '${tr('message')}...',
                  initialText:
                      context.read<YourProfilePageViewModel>().profileText,
                  shouldEnsureVisible: true,
                  webInitialScripts: null),
              htmlToolbarOptions: const HtmlToolbarOptions(
                toolbarType: ToolbarType.nativeExpandable,
                toolbarPosition: ToolbarPosition.aboveEditor,
                dropdownBoxDecoration: BoxDecoration(),
                defaultToolbarButtons: [
                  FontButtons(bold: true, italic: true, underline: true),
                  ParagraphButtons(),
                  ListButtons()
                ],
              ),
            ),
            SmartGaps.gapH20,
            SubmitButton(
              height: 60,
              text: tr('save_changes'),
              onPressed: () => onPressedSaveChanges(),
            ),
          ],
        ),
      ),
    );
  }
}
