import 'dart:io';

import 'package:Haandvaerker.dk/model/partner/supplier_info_file_model.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/cache_image.dart';
import 'package:Haandvaerker.dk/ui/components/custom_button.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/package/package_popup_dialog.dart';
import 'package:Haandvaerker.dk/ui/components/photo_editor_wrapper.dart';
import 'package:Haandvaerker.dk/ui/components/single_image_selector.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:Haandvaerker.dk/utils/extension/snackbar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/packages/packages_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/widgets_recommendations_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/result_images_view_model.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/result_videos_view_model.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/your_profile_page_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher_string.dart';

class YourProfilePageScreen extends StatefulWidget {
  const YourProfilePageScreen({super.key});
  @override
  YourProfilePageScreenState createState() => YourProfilePageScreenState();
}

class YourProfilePageScreenState extends State<YourProfilePageScreen> {
  Future<void> getPartnerHaa() async {
    final widgetsVM = context.read<WidgetsViewModel>();
    final resultImagesVM = context.read<ResultImagesViewModel>();
    final videosVM = context.read<ResultVideosViewModel>();

    resultImagesVM.value = <SupplierInfoFileModel>[];
    widgetsVM.setBusy(true);
    resultImagesVM.setBusy(true);
    await tryCatchWrapper(
        context: myGlobals.homeScaffoldKey!.currentContext,
        function: Future.value(widgetsVM.getSupplierInfo()));

    resultImagesVM.value = widgetsVM.company?.gallery ?? [];
    resultImagesVM.defaultProfileImage =
        widgetsVM.company!.getPrimaryIndustryV2!.pictureURL!;

    if (mounted) {
      videosVM.value = widgetsVM.company!.videos ?? [];
      context.read<YourProfilePageViewModel>().profileText =
          widgetsVM.company!.bCrmCompany!.companyProfileText ?? '';
    }

    resultImagesVM.setBusy(false);
    widgetsVM.setBusy(false);
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final appDrawerVm = context.read<AppDrawerViewModel>();
      final newsFeedVm = context.read<NewsFeedViewModel>();
      const dynamicStoryPage = 'profilside';
      final dynamicStoryEnable =
          (appDrawerVm.bitrixCompanyModel?.dynamicPopup ?? 0) == 1;

      getPartnerHaa();
      if (dynamicStoryEnable) {
        await Future.value(newsFeedVm.getDynamicStory(page: dynamicStoryPage))
            .whenComplete(() {
          if (!mounted) return;

          newsFeedVm.dynamicStoryFunction(
              dynamicStoryPage: dynamicStoryPage,
              dynamicStoryEnable: dynamicStoryEnable,
              pageContext: context);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Container(
        padding: const EdgeInsets.only(top: 50, left: 20.0, right: 20.0),
        child: Consumer<WidgetsViewModel>(builder: (context, widgetVm, _) {
          if (widgetVm.busy) return Center(child: loader());

          return ListView(
            children: const <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TopContent(),
                  SizedBox(height: 30),
                  InformationContent(),
                  ProfileTextContent(),
                  SizedBox(height: 25),
                  ProfilePicturesContent(),
                  SizedBox(height: 5),
                  ProfileVideoContent(),
                  SizedBox(height: 25),
                ],
              )
            ],
          );
        }),
      ),
    );
  }
}

/*
 * INFORMATION CONTENT 
 */
class InformationContent extends StatefulWidget {
  const InformationContent({
    super.key,
  });
  @override
  InformationContentState createState() => InformationContentState();
}

class InformationContentState extends State<InformationContent> {
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();
  final GlobalKey<FormBuilderState> _fbKeyInformationContent =
      GlobalKey<FormBuilderState>();
  bool _information = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          tr('information'),
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        SmartGaps.gapH5,
        Text(tr('these_are_the_information'),
            style: Theme.of(context).textTheme.bodyMedium),
        SmartGaps.gapH20,
        InkWell(
          onTap: () {
            setState(() {
              _information = !_information;
            });
          },
          splashColor: Theme.of(context).colorScheme.secondary,
          child: Container(
            padding:
                const EdgeInsets.only(top: 16, right: 30, bottom: 20, left: 30),
            decoration: BoxDecoration(
                border: Border.all(
                    color: Theme.of(context).colorScheme.secondary, width: 2)),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(tr('edit'),
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                        letterSpacing: 0,
                        height: 1.618,
                        color: Theme.of(context).colorScheme.secondary
                        // color: Color.fromRGBO(0, 54, 69, 1),
                        )
                    // style: GoogleFonts.openSans(
                    //   textStyle: TextStyle(
                    //     fontSize: 14,
                    //     fontWeight: FontWeight.bold,
                    //     color: Theme.of(context).colorScheme.secondary
                    //   )
                    // )
                    ),
                SmartGaps.gapW20,
                Icon(_information ? Icons.expand_less : Icons.expand_more,
                    color: Theme.of(context).colorScheme.secondary),
              ],
            ),
          ),
        ),
        AnimatedCrossFade(
          duration: const Duration(milliseconds: 500),
          firstChild: _informationForm(),
          secondChild: Container(),
          crossFadeState: _information
              ? CrossFadeState.showFirst
              : CrossFadeState.showSecond,
        )
      ],
    );
  }

  Widget _informationForm() {
    // WidgetsViewModel
    return Consumer<WidgetsViewModel>(
      builder: (BuildContext context, WidgetsViewModel vm, Widget? _) {
        return vm.busy || vm.company == null
            ? Center(child: loader())
            : FormBuilder(
                key: _fbKeyInformationContent,
                initialValue: {
                  'contact_person':
                      vm.company?.bCrmCompany?.contactPerson ?? '',
                  'company_name': vm.company?.bCrmCompany?.title ?? '',
                  'address': vm.company?.bCrmCompany?.companyAddress ?? '',
                  'zip_code': vm.company?.bCrmCompany?.zipCode ?? '',
                  'telephone': vm.company?.bCrmCompany?.companyTelephone ?? '',
                  'mobile_number': vm.company?.bCrmCompany?.mobile ?? '',
                  'email': vm.company?.bCrmCompany?.email ?? '',
                  'cvr': vm.company?.bCrmCompany?.cvr ?? '',
                  'number_of_employees': vm
                          .company?.bCrmCompany?.companyNumberOfEmployees
                          .toString() ??
                      '',
                  'established':
                      vm.company?.bCrmCompany?.companyFoundingYear.toString() ??
                          '',
                  'homepage': vm.company?.bCrmCompany?.companyWeb ?? '',
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                child: Column(
                  children: <Widget>[
                    SmartGaps.gapH10,
                    FormBuilderTextField(
                      name: 'contact_person',
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: const Color(0xffDAE1E3),
                          border: InputBorder.none,
                          labelStyle: GoogleFonts.openSans(
                              textStyle: const TextStyle(
                                  color: Colors.blueGrey, fontSize: 18)),
                          labelText: tr('contact_person')),
                      validator: FormBuilderValidators.required(
                          errorText: tr('this_field_cannot_be_empty')),
                    ),
                    SmartGaps.gapH10,
                    FormBuilderTextField(
                      name: 'company_name',
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: const Color(0xffDAE1E3),
                          border: InputBorder.none,
                          labelStyle: GoogleFonts.openSans(
                              textStyle: const TextStyle(
                                  color: Colors.blueGrey, fontSize: 18)),
                          labelText: tr('company_name')),
                      validator: FormBuilderValidators.required(
                          errorText: tr('this_field_cannot_be_empty')),
                    ),
                    SmartGaps.gapH10,
                    FormBuilderTextField(
                      name: 'address',
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: const Color(0xffDAE1E3),
                          border: InputBorder.none,
                          labelStyle: GoogleFonts.openSans(
                              textStyle: const TextStyle(
                                  color: Colors.blueGrey, fontSize: 18)),
                          labelText: tr('address')),
                      validator: FormBuilderValidators.required(
                          errorText: tr('this_field_cannot_be_empty')),
                    ),
                    SmartGaps.gapH10,
                    FormBuilderTextField(
                        name: 'zip_code',
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: const Color(0xffDAE1E3),
                            border: InputBorder.none,
                            labelStyle: GoogleFonts.openSans(
                                textStyle: const TextStyle(
                                    color: Colors.blueGrey, fontSize: 18)),
                            labelText: tr('zip_code')),
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.required(
                              errorText: tr('this_field_cannot_be_empty')),
                          FormBuilderValidators.numeric(
                              errorText: tr('value_must_be_numeric')),
                        ])),
                    SmartGaps.gapH10,
                    FormBuilderTextField(
                      name: 'telephone',
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: const Color(0xffDAE1E3),
                          border: InputBorder.none,
                          labelStyle: GoogleFonts.openSans(
                              textStyle: const TextStyle(
                                  color: Colors.blueGrey, fontSize: 18)),
                          labelText: tr('telephone')),
                      validator: FormBuilderValidators.compose(
                        [
                          (val) {
                            if (val != null) {
                              if (val.length != 8) {
                                return tr('must_contain_8_digits');
                              }
                            }
                            return null;
                          }
                        ],
                      ),
                    ),
                    SmartGaps.gapH10,
                    FormBuilderTextField(
                      name: 'mobile_number',
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: const Color(0xffDAE1E3),
                          border: InputBorder.none,
                          labelStyle: GoogleFonts.openSans(
                              textStyle: const TextStyle(
                                  color: Colors.blueGrey, fontSize: 18)),
                          labelText: tr('mobile_number')),
                      validator: FormBuilderValidators.compose(
                        [
                          (val) {
                            if (val != null) {
                              if (val.length != 8) {
                                return tr('must_contain_8_digits');
                              }
                            }
                            return null;
                          }
                        ],
                      ),
                    ),
                    SmartGaps.gapH10,
                    FormBuilderTextField(
                      name: 'email',
                      readOnly: true,
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: const Color(0xffDAE1E3),
                          border: InputBorder.none,
                          labelStyle: GoogleFonts.openSans(
                              textStyle: const TextStyle(
                                  color: Colors.blueGrey, fontSize: 18)),
                          labelText: tr('email')),
                      validator: FormBuilderValidators.compose(
                        [
                          FormBuilderValidators.required(
                              errorText: tr('this_field_cannot_be_empty')),
                          FormBuilderValidators.email(
                              errorText: tr('enter_valid_email'))
                        ],
                      ),
                    ),
                    SmartGaps.gapH10,
                    FormBuilderTextField(
                      name: 'cvr',
                      readOnly: true,
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: const Color(0xffDAE1E3),
                          border: InputBorder.none,
                          labelStyle: GoogleFonts.openSans(
                              textStyle: const TextStyle(
                                  color: Colors.blueGrey, fontSize: 18)),
                          labelText: tr('cvr')),
                      validator: FormBuilderValidators.numeric(
                          errorText: tr('value_must_be_numeric')),
                    ),
                    SmartGaps.gapH10,
                    FormBuilderTextField(
                      name: 'number_of_employees',
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: const Color(0xffDAE1E3),
                          border: InputBorder.none,
                          labelStyle: GoogleFonts.openSans(
                              textStyle: const TextStyle(
                                  color: Colors.blueGrey, fontSize: 18)),
                          labelText: tr('number_of_employees')),
                      validator: FormBuilderValidators.compose(
                        [
                          FormBuilderValidators.required(
                              errorText: tr('this_field_cannot_be_empty')),
                          FormBuilderValidators.numeric(
                              errorText: tr('value_must_be_numeric')),
                        ],
                      ),
                    ),
                    SmartGaps.gapH10,
                    FormBuilderTextField(
                      name: 'established',
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: const Color(0xffDAE1E3),
                          border: InputBorder.none,
                          labelStyle: GoogleFonts.openSans(
                              textStyle: const TextStyle(
                                  color: Colors.blueGrey, fontSize: 18)),
                          labelText: tr('established')),
                      validator: FormBuilderValidators.compose(
                        [
                          FormBuilderValidators.required(
                              errorText: tr('this_field_cannot_be_empty')),
                          FormBuilderValidators.numeric(
                              errorText: tr('value_must_be_numeric')),
                        ],
                      ),
                    ),
                    SmartGaps.gapH10,
                    FormBuilderTextField(
                      name: 'homepage',
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: const Color(0xffDAE1E3),
                          border: InputBorder.none,
                          labelStyle: GoogleFonts.openSans(
                              textStyle: const TextStyle(
                                  color: Colors.blueGrey, fontSize: 18)),
                          labelText: tr('homepage')),
                      validator: FormBuilderValidators.compose(
                        [
                          FormBuilderValidators.url(
                              errorText: tr('enter_valid_web_address'))
                        ],
                      ),
                    ),
                    SmartGaps.gapH15,
                    SubmitButton(
                      text: tr('save_changes'),
                      onPressed: () async {
                        if (_fbKeyInformationContent.currentState!
                            .saveAndValidate()) {
                          final yourProfilePageVM =
                              context.read<YourProfilePageViewModel>();

                          showLoadingDialog(context, loadingKey);
                          final widgetsVm = context.read<WidgetsViewModel>();
                          final oldMap = {
                            'contactPerson':
                                widgetsVm.company?.bCrmCompany?.contactPerson ??
                                    '',
                            'address': widgetsVm
                                    .company?.bCrmCompany?.companyAddress ??
                                '',
                            'foundingYear': widgetsVm.company?.bCrmCompany
                                    ?.companyFoundingYear ??
                                0,
                            'numberOfEmployees': widgetsVm.company?.bCrmCompany
                                    ?.companyNumberOfEmployees ??
                                0,
                            'telephone': widgetsVm
                                    .company?.bCrmCompany?.companyTelephone ??
                                '',
                            'phone':
                                widgetsVm.company?.bCrmCompany?.mobile ?? '',
                            'title':
                                widgetsVm.company?.bCrmCompany?.title ?? '',
                            'zipCode':
                                widgetsVm.company?.bCrmCompany?.zipCode ?? '',
                            'web': widgetsVm.company?.bCrmCompany?.companyWeb,
                          };

                          final newMap = {
                            'contactPerson': _fbKeyInformationContent
                                    .currentState!.value['contact_person'] ??
                                '',
                            'address': _fbKeyInformationContent
                                    .currentState!.value['address'] ??
                                '',
                            'foundingYear': int.parse(_fbKeyInformationContent
                                    .currentState!.value['established'] ??
                                ''),
                            'numberOfEmployees': int.parse(
                                _fbKeyInformationContent.currentState!
                                        .value['number_of_employees'] ??
                                    ''),
                            'telephone': _fbKeyInformationContent
                                    .currentState!.value['telephone'] ??
                                '',
                            'phone': _fbKeyInformationContent
                                    .currentState!.value['mobile_number'] ??
                                '',
                            'title': _fbKeyInformationContent
                                    .currentState!.value['company_name'] ??
                                '',
                            'zipCode': _fbKeyInformationContent
                                    .currentState!.value['zip_code'] ??
                                '',
                            'web': _fbKeyInformationContent
                                    .currentState!.value['homepage'] ??
                                '',
                          };

                          await tryCatchWrapper(
                            context: myGlobals.homeScaffoldKey!.currentContext,
                            function: yourProfilePageVM.updateSupplierInfo(
                              payload:
                                  yourProfilePageVM.getChanges(oldMap, newMap),
                            ),
                          ).then((value) async {
                            if (value!) {
                              widgetsVm.setBusy(true);
                              await widgetsVm.getSupplierInfo();
                              widgetsVm.setBusy(false);
                            }
                            Navigator.of(loadingKey.currentContext!).pop();
                            showSuccessAnimationDialog(
                                myGlobals.homeScaffoldKey!.currentContext!,
                                value,
                                '');
                          });
                        }
                      },
                    ),
                    SmartGaps.gapH10,
                    Consumer<YourProfilePageViewModel>(
                      builder: (BuildContext context,
                          YourProfilePageViewModel yourProfilePageVM,
                          Widget? _) {
                        return yourProfilePageVM.busy ? loader() : Container();
                      },
                    ),
                    SmartGaps.gapH10,
                  ],
                ),
              );
      },
    );
  }
}

/*
 * TOP CONTENT 
 */

class TopContent extends StatelessWidget {
  const TopContent({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(tr('edit_in_your_profile_page_at_handicraft.dk'),
            style: Theme.of(context).textTheme.headlineLarge),
        SmartGaps.gapH20,
        Text(
          tr('your_profile_page_at_Håndværker.dk_is_your_professional'),
          style: Theme.of(context).textTheme.titleSmall,
        ),
        SmartGaps.gapH24,
        Consumer<WidgetsViewModel>(
          builder: (BuildContext context, widgetsVM, Widget? child) {
            return InkWell(
              onTap: () async {
                final profileUrl =
                    "https://www.haandvaerker.dk/${widgetsVM.company!.bCrmCompany!.canonical!}";
                if (await canLaunchUrlString(profileUrl)) {
                  changeDrawerRoute(Routes.inAppWebView,
                      arguments: Uri.parse(profileUrl));
                } else {
                  if (context.mounted) {
                    context.showErrorSnackBar(
                        message: 'Could not launch $profileUrl');
                  }
                }
              },
              child: child,
            );
          },
          child: Container(
              padding: const EdgeInsets.only(
                  top: 20, right: 30, bottom: 26, left: 30),
              color: Theme.of(context).colorScheme.onSurfaceVariant,
              child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(tr('see_my_profile_page'),
                        style: const TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          letterSpacing: 0,
                          height: 1.618,
                          color: Colors.white,
                        )),
                    SmartGaps.gapW10,
                    const Icon(ElementIcons.right,
                        color: Colors.white, size: 32),
                  ])),
        ),
        SmartGaps.gapH20,
        Text(tr('would_you_choose_yourself'),
            style: Theme.of(context).textTheme.titleSmall),
        SmartGaps.gapH20,
        Text(tr('if_you_update_your_profile'),
            style: Theme.of(context).textTheme.titleSmall),
      ],
    );
  }
}

/*
 * PROFILE TEXT CONTENT 
 */
class ProfileTextContent extends StatefulWidget {
  const ProfileTextContent({
    super.key,
  });

  @override
  ProfileTextContentState createState() => ProfileTextContentState();
}

class ProfileTextContentState extends State<ProfileTextContent> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SmartGaps.gapH30,
        Text(tr('profile_text'),
            style: Theme.of(context).textTheme.headlineMedium),
        SmartGaps.gapH10,
        Text(tr('profile_text_is_written_by_copywriter'),
            style: Theme.of(context).textTheme.bodyMedium),
        SmartGaps.gapH24,
        InkWell(
          onTap: () async {
            changeDrawerRoute(Routes.htmlEditor);
          },
          splashColor: Theme.of(context).colorScheme.secondary,
          child: Container(
            padding:
                const EdgeInsets.only(top: 16, right: 30, bottom: 20, left: 30),
            decoration: BoxDecoration(
                border: Border.all(
                    color: Theme.of(context).colorScheme.secondary, width: 2)),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(tr('edit'),
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                        letterSpacing: 0,
                        height: 1.618,
                        color: Theme.of(context).colorScheme.secondary
                        // color: Color.fromRGBO(0, 54, 69, 1),
                        )),
                SmartGaps.gapW20,
                Icon(Icons.arrow_forward_rounded,
                    color: Theme.of(context).colorScheme.secondary),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

/*
 * PROFILE PICTURES CONTENT 
 */

class ProfilePicturesContent extends StatefulWidget {
  const ProfilePicturesContent({
    super.key,
  });

  @override
  ProfilePicturesContentState createState() => ProfilePicturesContentState();
}

class ProfilePicturesContentState extends State<ProfilePicturesContent> {
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();
  bool _profilePictures = false;
  File? _selectedImage;
  bool hasFiles = false;

  Future<void> attemptDelete({required int pictureId}) async {
    //remove image
    final vm = context.read<ResultImagesViewModel>();
    final selected = await showOkCancelAlertDialog(
        context: context,
        title: tr('confirm_delete'),
        message: tr('are_you_sure_you_want_to_delete_this_picture'),
        okLabel: tr('delete'));
    if (selected == OkCancelResult.ok && mounted) {
      showLoadingDialog(context, loadingKey);
      try {
        await vm.deleteProfilePicture(pictureId: pictureId);
        if (mounted) {
          Navigator.of(loadingKey.currentContext!).pop();
          showSuccessAnimationDialog(context, true, '');
        }
      } on ApiException catch (e) {
        Navigator.of(loadingKey.currentContext!).pop();
        showSuccessAnimationDialog(
            myGlobals.homeScaffoldKey!.currentContext!, false, e.message);
      }
    } else {
      setState(() {});
    }
  }

  void onPressedEdit(
      {required SupplierInfoFileModel itemImage,
      required ResultImagesViewModel resultImagesVm,
      required int index}) async {
    final editedImage = await Editor.standard(
      context: context,
      isImageUrl: true,
      loadingkey: loadingKey,
      imageUrl: itemImage.fullURL,
    );

    if (editedImage != null && mounted) {
      showLoadingDialog(context, loadingKey);

      try {
        await resultImagesVm.editProfileImage(
          index: index,
          picture: editedImage,
          pictureId: itemImage.id!,
        );

        if (!mounted) return;

        Navigator.of(loadingKey.currentContext!).pop();
        showSuccessAnimationDialog(context, true, '');
      } on ApiException catch (_) {
        Navigator.of(loadingKey.currentContext!).pop();
        showSuccessAnimationDialog(
            myGlobals.homeScaffoldKey!.currentContext!, false, '');
      }
    }
  }

  void onImageChange(File? file) async {
    if (file != null) {
      final croppedImage = await ImageCropper().cropImage(
        sourcePath: file.path,
        aspectRatio: const CropAspectRatio(ratioX: 4, ratioY: 3),
        uiSettings: [
          AndroidUiSettings(
              toolbarTitle: tr('crop_image'),
              toolbarColor: Colors.black,
              toolbarWidgetColor: Colors.white,
              initAspectRatio: CropAspectRatioPreset.ratio4x3,
              backgroundColor: Colors.white,
              lockAspectRatio: false),
          IOSUiSettings(
            title: tr('crop_image'),
          )
        ],
      );

      if (mounted && croppedImage != null) {
        await Editor.photo(
                context: context,
                isImageUrl: false,
                loadingkey: loadingKey,
                file: File(croppedImage.path))
            .then((edited) {
          if (edited != null) {
            setState(() {
              _selectedImage = edited;
            });
          }
        });
      }
    }
  }

  void onPressedUpload({required ResultImagesViewModel resultImageVm}) async {
    final userVm = context.read<UserViewModel>();

    showLoadingDialog(context, loadingKey);
    try {
      await resultImageVm.uploadProfilePicture(files: [_selectedImage!]);

      if (!mounted) return;

      Navigator.of(loadingKey.currentContext!).pop();
      showSuccessAnimationDialog(context, true,
          '${tr('hey')} ${userVm.userModel?.user?.name ?? ''}\n\n${tr('photo_available_24_hours')}');
      setState(() {
        _selectedImage = null;
      });
    } on ApiException catch (e) {
      Navigator.of(loadingKey.currentContext!).pop();
      showSuccessAnimationDialog(
          myGlobals.homeScaffoldKey!.currentContext!, false, e.message);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          tr('profile_pictures'),
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        SmartGaps.gapH10,
        Text(tr('first_image_in_list'),
            style: Theme.of(context).textTheme.bodyMedium),
        SmartGaps.gapH24,
        InkWell(
          onTap: () {
            setState(() {
              _profilePictures = !_profilePictures;
            });
          },
          splashColor: Theme.of(context).colorScheme.secondary,
          child: Container(
            padding:
                const EdgeInsets.only(top: 16, right: 30, bottom: 20, left: 30),
            decoration: BoxDecoration(
                border: Border.all(
                    color: Theme.of(context).colorScheme.secondary, width: 2)),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(tr('edit'),
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                        letterSpacing: 0,
                        height: 1.618,
                        color: Theme.of(context).colorScheme.secondary)),
                SmartGaps.gapW20,
                Icon(_profilePictures ? Icons.expand_less : Icons.expand_more,
                    color: Theme.of(context).colorScheme.secondary),
              ],
            ),
          ),
        ),
        SmartGaps.gapH10,
        AnimatedCrossFade(
          duration: const Duration(milliseconds: 500),
          firstChild: profileImages(),
          secondChild: Container(),
          crossFadeState: _profilePictures
              ? CrossFadeState.showFirst
              : CrossFadeState.showSecond,
        )
      ],
    );
  }

  Widget profileImages() {
    return Consumer<ResultImagesViewModel>(
      builder: (BuildContext context, vm, Widget? child) {
        return (vm.busy)
            ? Center(child: loader())
            : Column(
                children: <Widget>[
                  if (vm.value.isNotEmpty)
                    ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: vm.value.length,
                        itemBuilder: (context, index) {
                          final itemImage = vm.value.elementAt(index);

                          return Dismissible(
                            key: UniqueKey(),
                            background: Container(
                                alignment: Alignment.center,
                                child: const Icon(ElementIcons.delete,
                                    size: 40, color: Colors.red)),
                            onDismissed: (_) async {
                              await attemptDelete(pictureId: itemImage.id!);
                            },
                            child: Card(
                              child: SizedBox(
                                  height: 220,
                                  width: double.infinity,
                                  child: Column(
                                    children: <Widget>[
                                      Expanded(
                                        // flex: 1,
                                        child: CacheImage(
                                            fit: BoxFit.contain,
                                            imageUrl: itemImage.fullURL,
                                            height: 100,
                                            width: 100
                                            // width: double.infinity
                                            ),
                                      ),
                                      Expanded(
                                          // flex: 3,
                                          child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Expanded(
                                              child: Row(
                                            mainAxisSize: MainAxisSize.max,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Expanded(
                                                  child: IconButton(
                                                icon: const Icon(
                                                    FontAwesomeIcons.angleLeft),
                                                onPressed: index == 0
                                                    ? null
                                                    : () async {
                                                        await vm.shiftPhoto(
                                                            forward: false,
                                                            photoId:
                                                                itemImage.id!);
                                                      },
                                              )),
                                              Expanded(
                                                  child: IconButton(
                                                icon: const Icon(
                                                    FontAwesomeIcons
                                                        .anglesLeft),
                                                onPressed: index == 0
                                                    ? null
                                                    : () async {
                                                        await vm.shiftPhoto(
                                                          forward: false,
                                                          photoId:
                                                              itemImage.id!,
                                                          pushToEnd: true,
                                                        );
                                                      },
                                              )),
                                              Expanded(
                                                  child: IconButton(
                                                icon: const Icon(
                                                    FontAwesomeIcons
                                                        .anglesRight),
                                                onPressed:
                                                    index == vm.value.length - 1
                                                        ? null
                                                        : () async {
                                                            await vm.shiftPhoto(
                                                              forward: true,
                                                              photoId:
                                                                  itemImage.id!,
                                                              pushToEnd: true,
                                                            );
                                                          },
                                              )),
                                              Expanded(
                                                child: IconButton(
                                                  icon: const Icon(
                                                      FontAwesomeIcons
                                                          .angleRight),
                                                  onPressed: index ==
                                                          vm.value.length - 1
                                                      ? null
                                                      : () async {
                                                          await vm.shiftPhoto(
                                                            forward: true,
                                                            photoId:
                                                                itemImage.id!,
                                                          );
                                                        },
                                                ),
                                              )
                                            ],
                                          )),
                                          Expanded(
                                              child: Row(
                                            children: <Widget>[
                                              Expanded(
                                                  child: IconButton(
                                                icon: const Icon(
                                                    FontAwesomeIcons.pen),
                                                onPressed: () => onPressedEdit(
                                                    resultImagesVm: vm,
                                                    itemImage: itemImage,
                                                    index: index),
                                              )),
                                              Expanded(
                                                  child: IconButton(
                                                icon: const Icon(
                                                    ElementIcons.delete,
                                                    size: 24,
                                                    color: Colors.red),
                                                onPressed: () async {
                                                  await attemptDelete(
                                                      pictureId: itemImage.id!);
                                                },
                                              )),
                                            ],
                                          )),
                                        ],
                                      ))
                                    ],
                                  )),
                            ),
                          );
                        }),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SmartGaps.gapH20,
                      SizedBox(
                        height: 200,
                        width: double.infinity,
                        child: Row(
                          children: <Widget>[
                            if (vm.value.isNotEmpty)
                              Expanded(
                                child: CacheImage(
                                  fit: BoxFit.cover,
                                  imageUrl: vm.value.elementAt(0).fullURL,
                                ),
                              )
                            else
                              Expanded(
                                child: CacheImage(
                                  fit: BoxFit.cover,
                                  imageUrl: vm.defaultProfileImage,
                                ),
                              ),
                            Expanded(
                              child: GridView.count(
                                  childAspectRatio: 0.9,
                                  crossAxisCount: 2,
                                  shrinkWrap: false,
                                  primary: false,
                                  physics: const NeverScrollableScrollPhysics(),
                                  children: List.generate(
                                    4,
                                    (index) {
                                      if (vm.value.isNotEmpty) {
                                        final images = <SupplierInfoFileModel>[
                                          ...vm.value
                                        ];

                                        images.removeAt(0);

                                        final length = <SupplierInfoFileModel>[
                                          ...images
                                        ].length;

                                        if (length < 4) {
                                          for (int i = 0; i < 4 - length; i++) {
                                            images.add(vm.value.first);
                                          }
                                        }

                                        final itemImage =
                                            images.elementAt(index);

                                        return CacheImage(
                                            fit: BoxFit.cover,
                                            height: double.infinity,
                                            width: double.infinity,
                                            imageUrl: itemImage.fullURL);
                                      } else {
                                        return CacheImage(
                                          fit: BoxFit.cover,
                                          height: double.infinity,
                                          width: double.infinity,
                                          imageUrl: vm.defaultProfileImage,
                                        );
                                      }
                                    },
                                  )),
                            )
                          ],
                        ),
                      ),
                      SmartGaps.gapH20,
                      Text(
                        tr('you_can_upload_files'),
                      ),
                      SmartGaps.gapH10,
                      SingleImageSelector(
                        height: 150,
                        width: 150,
                        initialFile: _selectedImage,
                        disable: false,
                        onImageChange: (File? myFile) => onImageChange(myFile),
                      ),
                    ],
                  ),
                  Consumer<WidgetsViewModel>(
                    builder: (BuildContext context, WidgetsViewModel widgetsVM,
                        Widget? child) {
                      return child!;
                    },
                    child: Column(
                      children: <Widget>[
                        SmartGaps.gapH10,
                        AbsorbPointer(
                          absorbing: (_selectedImage == null),
                          child: SubmitButton(
                            text: tr('upload'),
                            backgroundColor: _selectedImage != null
                                ? Theme.of(context).colorScheme.secondary
                                : Colors.grey,
                            onPressed: () => onPressedUpload(resultImageVm: vm),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SmartGaps.gapH10,
                  // vm.isUploadingImage ? loader() : Container()
                ],
              );
      },
    );
  }
}

/*
 * PROFILE VIDEO CONTENT 
 */
class ProfileVideoContent extends StatefulWidget {
  const ProfileVideoContent({
    super.key,
  });

  @override
  ProfileVideoContentState createState() => ProfileVideoContentState();
}

class ProfileVideoContentState extends State<ProfileVideoContent> {
  bool _profileVideo = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SmartGaps.gapH20,
        Text(tr('profile_video'),
            style: Theme.of(context).textTheme.headlineMedium),
        SmartGaps.gapH10,
        Text(tr('upload_video_to_your_profile'),
            style: Theme.of(context).textTheme.bodyMedium),
        SmartGaps.gapH24,
        InkWell(
          onTap: () {
            setState(() {
              _profileVideo = !_profileVideo;
            });
          },
          splashColor: Theme.of(context).colorScheme.secondary,
          child: Container(
            padding:
                const EdgeInsets.only(top: 16, right: 30, bottom: 20, left: 30),
            decoration: BoxDecoration(
                border: Border.all(
                    color: Theme.of(context).colorScheme.secondary, width: 2)),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(tr('edit'),
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                        letterSpacing: 0,
                        height: 1.618,
                        color: Theme.of(context).colorScheme.secondary)),
                SmartGaps.gapW20,
                Icon(_profileVideo ? Icons.expand_less : Icons.expand_more,
                    color: Theme.of(context).colorScheme.secondary),
              ],
            ),
          ),
        ),
        SmartGaps.gapH10,
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            AnimatedCrossFade(
              duration: const Duration(milliseconds: 500),
              firstChild: const ProfileVideo(),
              secondChild: Container(),
              crossFadeState: _profileVideo
                  ? CrossFadeState.showFirst
                  : CrossFadeState.showSecond,
            ),
          ],
        ),
        SmartGaps.gapH50,
      ],
    );
  }
}

class ProfileVideo extends StatefulWidget {
  const ProfileVideo({super.key});
  @override
  ProfileVideoState createState() => ProfileVideoState();
}

class ProfileVideoState extends State<ProfileVideo> {
  GlobalKey<NavigatorState> loadingKey = GlobalKey<NavigatorState>();

  File? videoFile;

  void onTapDelete({required SupplierInfoFileModel video}) async {
    final resultVideosVM = context.read<ResultVideosViewModel>();

    final result = await showOkCancelAlertDialog(
        context: context,
        title: tr('confirm_delete'),
        message: tr('are_you_sure_you_want_to_delete_this_video'),
        okLabel: tr('delete'));

    if (result == OkCancelResult.ok && mounted) {
      showLoadingDialog(context, loadingKey);
      try {
        await resultVideosVM.deleteProfileVideo(videoId: video.id!);
        if (!mounted) return;

        Navigator.of(loadingKey.currentContext!).pop();
        showSuccessAnimationDialog(context, true, '');
      } on ApiException catch (e) {
        Navigator.of(loadingKey.currentContext!).pop();
        showSuccessAnimationDialog(
            myGlobals.homeScaffoldKey!.currentContext!, false, e.message);
      }
    }
  }

  void onPressedUpload() async {
    final packagesVm = context.read<PackagesViewModel>();

    if (packagesVm.accessVideoProfile.status == false) {
      showUnAvailableDialog(context, packagesVm.accessVideoProfile);
    } else {
      final resultVideosVM = context.read<ResultVideosViewModel>();

      showLoadingDialog(context, loadingKey);
      try {
        await resultVideosVM.uploadProfileVideo(video: videoFile!);

        if (!mounted) return;

        Navigator.of(loadingKey.currentContext!).pop();
        showSuccessAnimationDialog(context, true, '');
        setState(() {
          videoFile = null;
        });
      } on ApiException catch (e) {
        Navigator.of(loadingKey.currentContext!).pop();
        showSuccessAnimationDialog(
            myGlobals.homeScaffoldKey!.currentContext!, false, e.message);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ResultVideosViewModel>(
      builder:
          (BuildContext context, ResultVideosViewModel videoVm, Widget? _) {
        if (videoVm.busy) {
          return loader();
        }
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //

            if (videoVm.value.isNotEmpty) ...[
              //

              Text(
                tr('your_profile_video'),
                style: GoogleFonts.openSans(
                  textStyle: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),

              SmartGaps.gapH5,

              for (var video in videoVm.value)
                if (video.fullURL != null)
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        //

                        Expanded(
                          child: GestureDetector(
                            onTap: () => changeDrawerRoute(
                              Routes.inAppWebView,
                              arguments: Uri.parse(video.fullURL!),
                            ),
                            child: Text(
                              video.fullURL!,
                              style: GoogleFonts.openSans(
                                textStyle: const TextStyle(
                                  fontSize: 12,
                                  color: PartnerAppColors.accentBlue,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ),
                        ),

                        GestureDetector(
                          onTap: () => onTapDelete(video: video),
                          child: const Icon(
                            ElementIcons.delete,
                            color: PartnerAppColors.red,
                          ),
                        ),
                      ],
                    ),
                  ),
            ],
            SmartGaps.gapH20,
            VideoDottedBorder(
              onVideoChange: (File? f) {
                final fileSize = f!.lengthSync();

                if (fileSize > 68157440) {
                  context.showCustomSnackBar(SnackBar(
                      content: Row(
                    children: <Widget>[
                      const Icon(Icons.info, color: Colors.red),
                      Text(
                        tr('added_image_more_than_65mb'),
                        style: const TextStyle(color: Colors.white),
                      )
                    ],
                  )));
                } else {
                  setState(() {
                    videoFile = f;
                  });
                }
              },
            ),
            if (videoFile != null)
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SmartGaps.gapH20,
                  ListTile(
                      title: Text(
                        videoFile!.path.split('/').last,
                      ),
                      trailing: IconButton(
                          icon: const Icon(ElementIcons.delete,
                              color: Colors.red),
                          onPressed: () {
                            setState(() {
                              videoFile = null;
                            });
                          })),
                  SmartGaps.gapH20,
                  SubmitButton(
                      onPressed: () => onPressedUpload(), text: tr('upload'))
                ],
              ),
            SmartGaps.gapH10,
            Consumer<ResultVideosViewModel>(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(tr('uploading'),
                      style: GoogleFonts.openSans(
                          textStyle: const TextStyle(fontSize: 16))),
                  SmartGaps.gapW20,
                  loader(),
                ],
              ),
              builder: (context, resultVideosVM, child) {
                return resultVideosVM.isBusy || resultVideosVM.isUploadingImage
                    ? child!
                    : Container();
              },
            ),
          ],
        );
      },
    );
  }
}

Future<void> redirect(String url) async {
  await launchUrlString(url);
}

class VideoDottedBorder extends StatefulWidget {
  const VideoDottedBorder({super.key, required this.onVideoChange});

  final Function(File?) onVideoChange;

  @override
  VideoDottedBorderState createState() => VideoDottedBorderState();
}

class VideoDottedBorderState extends State<VideoDottedBorder> {
  File? _selectedVideo;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        DottedBorder(
          color: Theme.of(context).colorScheme.primary,
          dashPattern: const [2, 4, 2, 4],
          strokeWidth: 2,
          strokeCap: StrokeCap.round,
          borderType: BorderType.RRect,
          child: Container(
            padding: const EdgeInsets.all(10),
            height: 80,
            width: double.infinity,
            child: CustomDesignTheme.flatButtonStyle(
              onPressed: () async {
                var fileresult =
                    await FilePicker.platform.pickFiles(type: FileType.video);

                if (fileresult != null && fileresult.paths.isNotEmpty) {
                  List<File> files =
                      fileresult.paths.map((e) => File(e!)).toList();

                  setState(() {
                    _selectedVideo = files.first;
                  });
                  widget.onVideoChange(_selectedVideo);
                }
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Image.asset(ImagePaths.picturePrimaryColor,
                      height: 50, width: 50),
                  SmartGaps.gapW10,
                  Flexible(
                    child: RichText(
                      maxLines: 2,
                      text: TextSpan(
                        text: '',
                        style: TextStyle(
                            color: Theme.of(context).colorScheme.primary),
                        children: <TextSpan>[
                          TextSpan(
                              text: tr('click_to_select_files'),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Theme.of(context).colorScheme.primary,
                                  fontSize: 14,
                                  fontFamily: 'Gibson')),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        SmartGaps.gapH10,
        Center(
          child: Text(
            tr('profile_video_upload_validation_desc'),
            style: TextStyle(
              color: Theme.of(context).colorScheme.primary,
              fontSize: 12,
              fontWeight: FontWeight.w600,
            ),
            maxLines: 2,
          ),
        ),
      ],
    );
  }
}
