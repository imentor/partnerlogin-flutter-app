import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/components/signature_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/contract/components/contract_send_message.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/utils/extension/text_themes.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/contract/contract_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/fast_track_offer/fast_track_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/payproff/payproff_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:url_launcher/url_launcher.dart';

class ContractPreview extends StatefulWidget {
  const ContractPreview({super.key, this.isFrom});
  final String? isFrom;

  @override
  ContractPreviewState createState() => ContractPreviewState();
}

class ContractPreviewState extends State<ContractPreview> {
  int? contractId;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final viewModel = context.read<ContractViewModel>();
      final projectViewModel = context.read<MyProjectsViewModel>();
      final fastTrackOfferVm = context.read<FastTrackOfferViewModel>();
      final activeProject = projectViewModel.activeProject;

      if (widget.isFrom == 'editOffer') {
        ScaffoldMessenger.of(myGlobals.innerScreenScaffoldKey!.currentContext!)
            .hideCurrentMaterialBanner();
        ScaffoldMessenger.of(myGlobals.innerScreenScaffoldKey!.currentContext!)
            .showMaterialBanner(
          MaterialBanner(
            leading: const Icon(Icons.check_circle,
                color: PartnerAppColors.malachite),
            content: Text(tr('you_have_accepted_changes_offer')),
            contentTextStyle: const TextStyle(color: Colors.black),
            elevation: 2,
            actions: [
              InkWell(
                  onTap: () {
                    ScaffoldMessenger.of(
                            myGlobals.innerScreenScaffoldKey!.currentContext!)
                        .hideCurrentMaterialBanner();
                  },
                  child: const Icon(FeatherIcons.x))
            ],
          ),
        );
      }

      if ((activeProject.isVersion ?? false)) {
        final acceptedOffers = [
          ...(activeProject.offers ?? []).where((e) => e.status == 'accepted')
        ];
        if (acceptedOffers.isNotEmpty) {
          contractId = acceptedOffers.first.contractId != null
              ? int.parse(acceptedOffers.first.contractId!)
              : null;
        }
      } else {
        if (activeProject.contractId != null) {
          contractId = activeProject.contractId;
        } else {
          contractId = fastTrackOfferVm.contractProjectId?.contractId;
        }
      }

      if (contractId != null) {
        modalManager.showLoadingModal();
        await tryCatchWrapper(
                context: context,
                function: viewModel.getContractById(contractId: contractId!))
            .whenComplete(() {
          viewModel.setBusy(false);
          modalManager.hideLoadingModal();
        });
      } else {
        showOkAlertDialog(
                context: context,
                title: tr('contract'),
                message: 'Fejl ved hentning af kontraktoplysninger!')
            .whenComplete(() {
          backDrawerRoute();
        });
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void onPressed({required ContractViewModel vm}) async {
    final payproffVm = context.read<PayproffViewModel>();
    final termsVm = context.read<SharedDataViewmodel>();

    if (vm.contract != null && vm.contract?.id != null) {
      final signature = await signatureDialog(
          context: context,
          label: termsVm.termsLabel,
          htmlTerms: termsVm.termsHtml,
          buttonLabel: 'confirm');

      if (signature != null) {
        modalManager.showLoadingModal();

        final contractSent = await vm.sendContractSignature(
            contractId: vm.contract!.id!, signaturePng: signature);

        modalManager.hideLoadingModal();

        if ((contractSent ?? false)) {
          final payproffExists = payproffVm.payproff?.exists ?? false;
          final payproffVerified =
              payproffVm.payproff?.payproffVerified ?? false;

          if (!payproffExists) {
            changeDrawerRoute(Routes.payproffTerms);
          } else {
            if ((payproffVm.payproffWallet?.iban ?? '').isEmpty) {
              payproffVm.isShowBanner = true;
              payproffVm.isPreIbanForm = true;
              payproffVm.ibanController = TextEditingController();
              changeDrawerReplaceRoute(Routes.payproffIbanScreen);
            } else {
              if (!mounted) return;

              if (!payproffVerified) {
                payproffVerifyDialog(
                  context: context,
                  verify: () async {
                    launchUrl(Uri.parse(payproffVm.payproff!.payproffUrl));
                    await payproffVm.updatePayproffStatus();
                  },
                );
              } else {
                successDialog(context: context).then((_) {
                  // Navigator.pop(context);
                  changeDrawerReplaceRoute(Routes.payproffVerifiedScreen);
                });
              }
            }
          }
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      body: Consumer<ContractViewModel>(
        builder: (context, vm, _) {
          // if (vm.busy) return Center(child: loader());

          // if (vm.contractHtmlFile!.isEmpty) {
          //   return GestureDetector(
          //     onTap: () {},
          //     child: Center(
          //       child: Column(
          //         mainAxisAlignment: MainAxisAlignment.center,
          //         children: [
          //           Text(
          //             " Contract unavailable",
          //             style: Theme.of(context).textTheme.titleMedium!.copyWith(
          //                 fontSize: 25,
          //                 color: Colors.white,
          //                 fontWeight: FontWeight.bold),
          //           ),
          //           SmartGaps.gapH10,
          //           RichText(
          //             text: TextSpan(
          //               children: [
          //                 TextSpan(
          //                   text: "Press to refresh ",
          //                   style: Theme.of(context)
          //                       .textTheme
          //                       .titleMedium!
          //                       .copyWith(
          //                           fontSize: 16,
          //                           color: Colors.white,
          //                           fontWeight: FontWeight.bold),
          //                 ),
          //                 const WidgetSpan(
          //                   // alignment: PlaceholderAlignment.middle,
          //                   child: Icon(
          //                     FeatherIcons.refreshCcw,
          //                     size: 18,
          //                   ),
          //                 ),
          //               ],
          //             ),
          //           ),
          //         ],
          //       ),
          //     ),
          //   );
          // }

          final isContractSigned =
              (((vm.contract?.dateSignedByPartner ?? '').isNotEmpty &&
                      (vm.contract?.dateSignedByContact ?? '').isEmpty) ||
                  ((vm.contract?.dateSignedByContact ?? '').isNotEmpty &&
                      (vm.contract?.dateSignedByPartner ?? '').isEmpty));
          final isBothSigned =
              (vm.contract?.dateSignedByPartner ?? '').isNotEmpty &&
                  (vm.contract?.dateSignedByContact ?? '').isNotEmpty;

          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //
                if (widget.isFrom == 'editOffer') ...[
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    tr('enterprise_contract'),
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    tr('contract_standard'),
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                          fontWeight: FontWeight.normal,
                        ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
                if (!isBothSigned)
                  Align(
                    alignment: Alignment.centerRight,
                    child: SizedBox(
                      height: 45,
                      child: CustomDesignTheme.flatButtonStyle(
                        backgroundColor: Colors.transparent,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                        onPressed: () {
                          if (isContractSigned) {
                            createNewContractVersionDialog(
                                context:
                                    myGlobals.homeScaffoldKey?.currentContext ??
                                        context);
                          } else {
                            backDrawerRoute();
                            changeDrawerRoute(Routes.editContract,
                                arguments: true);
                          }
                        },
                        child: Row(mainAxisSize: MainAxisSize.min, children: [
                          const Padding(
                            padding: EdgeInsets.only(bottom: 3.0),
                            child: Icon(
                              FeatherIcons.edit,
                              color: PartnerAppColors.blue,
                            ),
                          ),
                          SmartGaps.gapW10,
                          Text(
                            tr('edit_contract'),
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium!
                                .copyWith(
                                    fontSize: 16,
                                    color: PartnerAppColors.blue,
                                    fontWeight: FontWeight.bold),
                          ),
                        ]),
                      ),
                    ),
                  ),
                SmartGaps.gapH20,

                Expanded(
                  child: Skeletonizer(
                    enabled: vm.busy,
                    child: Container(
                        decoration: BoxDecoration(
                          color: PartnerAppColors.spanishGrey
                              .withValues(alpha: .2),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        padding: const EdgeInsets.all(10),
                        child: SfPdfViewer.network(
                            '${applic.bitrixApiUrl}/partner/contract/GetCraftmanContractPDFDownloadT?CONTRACT_ID=${contractId ?? 0}&FORMAT=PDF',
                            headers:
                                context.read<SharedDataViewmodel>().headers)),
                  ),
                ),

                if ((vm.contract?.dateSignedByPartner ?? '').isEmpty) ...[
                  SmartGaps.gapH20,
                  CustomDesignTheme.flatButtonStyle(
                    fixedSize: Size(MediaQuery.of(context).size.width, 45),
                    backgroundColor:
                        (vm.contract?.dateSignedByPartner ?? '').isEmpty
                            ? Theme.of(context).colorScheme.secondary
                            : Colors.grey,
                    onPressed: () => onPressed(vm: vm),
                    child: Row(mainAxisSize: MainAxisSize.min, children: [
                      Text(
                        tr('sign'),
                        textAlign: TextAlign.center,
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(
                                fontSize: 16,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                      ),
                      const Icon(
                        Icons.arrow_forward,
                        color: Colors.white,
                      )
                    ]),
                  ),
                ],

                //
              ],
            ),
          );
        },
      ),
    );
  }

  Future<void> contractSendMessage({
    required BuildContext context,
  }) {
    return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return const AlertDialog(
          insetPadding: EdgeInsets.all(20),
          contentPadding: EdgeInsets.all(20),
          content: ContractSendMessage(),
        );
      },
    );
  }

  Future<void> createNewContractVersionDialog({
    required BuildContext context,
  }) {
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext dialogContext) {
          return AlertDialog(
            insetPadding: const EdgeInsets.all(20),
            contentPadding: const EdgeInsets.all(20),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                    onTap: () {
                      Navigator.of(dialogContext).pop();
                    },
                    child: const Icon(
                      FeatherIcons.x,
                      color: PartnerAppColors.spanishGrey,
                    )),
              ],
            ),
            content: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: const SingleChildScrollView(
                  child: CreateNewContractVersionDialog()),
            ),
          );
        });
  }
}

class CreateNewContractVersionDialog extends StatelessWidget {
  const CreateNewContractVersionDialog({super.key});

  @override
  Widget build(BuildContext context) {
    final contractVm = context.read<ContractViewModel>();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Center(
          child: Text(
            tr('create_new_contract_message'),
            textAlign: TextAlign.center,
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(fontWeight: FontWeight.normal),
          ),
        ),
        SmartGaps.gapH30,
        Row(
          children: [
            Expanded(
              child: CustomDesignTheme.flatButtonStyle(
                height: 50,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                    side: BorderSide(
                        color: Theme.of(context).colorScheme.primary)),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(tr('none'),
                    style: context.pttTitleLarge.copyWith(
                      fontSize: 15,
                      color: Theme.of(context).colorScheme.primary,
                    )),
              ),
            ),
            SmartGaps.gapW10,
            Expanded(
              child: CustomDesignTheme.flatButtonStyle(
                height: 50,
                backgroundColor: PartnerAppColors.malachite,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                    side: const BorderSide(
                      color: PartnerAppColors.malachite,
                    )),
                onPressed: () {
                  contractVm.isCreateNewContractVersion = true;
                  backDrawerRoute();
                  Navigator.pop(context);
                  changeDrawerRoute(Routes.contractWizard, arguments: false);
                },
                child: Text(tr('yes'),
                    style: context.pttTitleLarge.copyWith(
                      fontSize: 15,
                      color: Colors.white,
                    )),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
