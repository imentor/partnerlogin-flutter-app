import 'package:Haandvaerker.dk/model/contract/contract_template_model.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer_contract/components/contract_wizard_dynamic_forms.dart';
import 'package:Haandvaerker.dk/viewmodel/contract/contract_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EditContractStepFour extends StatefulWidget {
  const EditContractStepFour({super.key, required this.contractVm});
  final ContractViewModel contractVm;

  @override
  State<EditContractStepFour> createState() => _EditContractStepFourState();
}

class _EditContractStepFourState extends State<EditContractStepFour> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [...dynamicForm(contractVm: widget.contractVm)],
    );
  }

  List<Widget> dynamicForm({required ContractViewModel contractVm}) {
    final CurrencyTextInputFormatter inputFormatter =
        CurrencyTextInputFormatter(
      NumberFormat.currency(locale: 'da', decimalDigits: 2, symbol: 'kr'),
    );
    List<Widget> sections = [];

    if (contractVm.contractTemplateModel != null) {
      final createOfferVm = context.read<CreateOfferViewModel>();
      final steps = contractVm.contractTemplateModel!.steps;
      if (steps != null && steps is Map<String, dynamic>) {
        for (final item in steps.values) {
          if (item['label'] == 'Pris') {
            if (item['fields'].last['default_value'].isEmpty ||
                item['fields'].last['default_value'] == null) {
              item['fields'].last['default_value'] = inputFormatter.formatString(
                  '${(contractVm.contract?.vatPrice ?? (createOfferVm.offerVersionList?.totalVat ?? 1000.00)).toString().split('.').first}.00');
            }
            final template =
                TemplateSteps.fromJson(item as Map<String, dynamic>);
            sections.add(Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: ContractWizardDynamicForms(
                    step: template,
                    contractFields:
                        (contractVm.contractTemplateModel?.fields ?? [])
                            .where((field) => field != null)
                            .cast<ContractTemplateField>()
                            .toList())));
          }
        }
      }
    }

    return sections;
  }
}
