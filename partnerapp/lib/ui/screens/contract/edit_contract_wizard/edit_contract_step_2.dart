import 'package:Haandvaerker.dk/model/contract/contract_template_model.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer_contract/components/contract_wizard_dynamic_forms.dart';
import 'package:Haandvaerker.dk/viewmodel/contract/contract_viewmodel.dart';
import 'package:flutter/material.dart';

class EditContractStepTwo extends StatefulWidget {
  const EditContractStepTwo({super.key, required this.contractVm});
  final ContractViewModel contractVm;

  @override
  State<EditContractStepTwo> createState() => _EditContractStepTwoState();
}

class _EditContractStepTwoState extends State<EditContractStepTwo> {
  @override
  Widget build(BuildContext context) {
    return Column(children: [...dynamicForm(contractVm: widget.contractVm)]);
  }

  List<Widget> dynamicForm({required ContractViewModel contractVm}) {
    List<Widget> sections = [];

    if (contractVm.contractTemplateModel != null) {
      final steps = contractVm.contractTemplateModel!.steps;
      if (steps != null && steps is Map<String, dynamic>) {
        for (final item in steps.values) {
          if (item['label'] == 'Aftalen') {
            final template =
                TemplateSteps.fromJson(item as Map<String, dynamic>);
            sections.add(Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: ContractWizardDynamicForms(
                    step: template,
                    contractFields:
                        (contractVm.contractTemplateModel?.fields ?? [])
                            .where((field) => field != null)
                            .cast<ContractTemplateField>()
                            .toList())));
          }
        }
      }
    }

    return sections;
  }
}
