import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/contract/edit_contract_wizard/edit_contract_step_1.dart';
import 'package:Haandvaerker.dk/ui/screens/contract/edit_contract_wizard/edit_contract_step_2.dart';
import 'package:Haandvaerker.dk/ui/screens/contract/edit_contract_wizard/edit_contract_step_3.dart';
import 'package:Haandvaerker.dk/ui/screens/contract/edit_contract_wizard/edit_contract_step_4.dart';
import 'package:Haandvaerker.dk/ui/screens/contract/edit_contract_wizard/edit_contract_step_5.dart';
import 'package:Haandvaerker.dk/ui/screens/contract/edit_contract_wizard/edit_contract_step_6.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/extension/snackbar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/contract/contract_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/fast_track_offer/fast_track_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/widgets_recommendations_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:timeline_tile/timeline_tile.dart';

class EditContractWizardPage extends StatefulWidget {
  const EditContractWizardPage({super.key});

  @override
  State<EditContractWizardPage> createState() => _EditContractWizardPageState();
}

class _EditContractWizardPageState extends State<EditContractWizardPage> {
  final formKey = GlobalKey<FormBuilderState>();
  final ItemScrollController itemScrollController = ItemScrollController();
  final itemCount5 = [
    'Dine Info',
    'Aftalen',
    'Tidsplan',
    'Pris',
    'Garantiordning'
  ];
  final itemCount6 = [
    'Dine Info',
    'Aftalen',
    'Tidsplan',
    'Pris',
    'Addendum',
    'Garantiordning'
  ];

  int tempSteps = 0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final contractVm = context.read<ContractViewModel>();
      final projectVm = context.read<MyProjectsViewModel>();
      final tenderVm = context.read<TenderFolderViewModel>();
      final createOfferVm = context.read<CreateOfferViewModel>();
      final fastTrackOfferVm = context.read<FastTrackOfferViewModel>();

      final offerList = projectVm.activeProject.offers ??
          (fastTrackOfferVm.currentProject?.offers ?? <Offer>[]);
      final offerId = offerList.isNotEmpty ? offerList.first.id ?? 0 : 0;
      final acceptedOfferVersionId =
          createOfferVm.offerVersionList?.acceptedVersionId ?? 0;
      final isHovedentreprise =
          projectVm.activeProject.contractType == 'Hovedentreprise';

      contractVm.isContractDataLoading = true;

      await tryCatchWrapper(
              context: myGlobals.homeScaffoldKey!.currentContext,
              function:
                  Future.value(createOfferVm.getOfferById(offerId: offerId)))
          .whenComplete(() async {
        await tryCatchWrapper(
            context: myGlobals.innerScreenScaffoldKey!.currentContext,
            function: Future.value(contractVm.initContractWizard(
              acceptedOfferVersionId: acceptedOfferVersionId,
              parentProjectId: isHovedentreprise
                  ? projectVm.activeProject.id!
                  : projectVm.activeProject.parentId!,
              contactId: projectVm.activeProject.homeOwner!.id!,
              projectId: projectVm.activeProject.id!,
              offerId: projectVm.activeProject.offers!.first.id!,
              wholeEnterprise: isHovedentreprise ? '1' : '0',
              companyId: projectVm.activeProject.partner!.id!,
              wizardsTexts: tenderVm.wizardTexts,
              industryList: projectVm.activeProject.industryInfoNew!.industry,
              homeownerId: projectVm.activeProject.homeOwner!.id!,
              jobList: projectVm.activeProject.jobList,
              isContractUpdate: true,
              contractId: projectVm.activeProject.contractId,
            ))).whenComplete(() {
          contractVm.isContractDataLoading = false;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> newMap = {};
    return Scaffold(
      appBar: const DrawerAppBar(),
      bottomNavigationBar: buttonNav(newMap: newMap),
      body: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: FormBuilder(
            key: formKey,
            child: Consumer<ContractViewModel>(builder: (_, contractVm, __) {
              if (itemScrollController.isAttached &&
                  (tempSteps == contractVm.editContractStep)) {
                itemScrollController.scrollTo(
                    index: contractVm.editContractStep,
                    duration: const Duration(seconds: 2),
                    curve: Curves.easeInOutCubic);
              }
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('UDKAST KONTRAKT',
                      style: Theme.of(context).textTheme.headlineLarge),
                  SmartGaps.gapH20,
                  if ((contractVm.editContractStep + 1) < 8) ...[
                    SizedBox(
                        height: 65,
                        width: MediaQuery.of(context).size.width,
                        child: ScrollablePositionedList.builder(
                          initialAlignment: 0,
                          scrollDirection: Axis.horizontal,
                          itemScrollController: itemScrollController,
                          shrinkWrap: true,
                          itemCount: contractVm.editContractItemCount,
                          itemBuilder: (context, index) {
                            final isCurrentPage =
                                index <= contractVm.editContractStep;

                            return TimelineTile(
                              axis: TimelineAxis.horizontal,
                              alignment: TimelineAlign.center,
                              isFirst: index == 0,
                              isLast:
                                  index == contractVm.editContractItemCount - 1,
                              indicatorStyle: IndicatorStyle(
                                width: 30,
                                height: 30,
                                indicator: DecoratedBox(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        color: PartnerAppColors.blue),
                                    color: isCurrentPage
                                        ? PartnerAppColors.blue
                                        : Colors.white,
                                  ),
                                  child: Center(
                                      child: Text('${index + 1}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodySmall!
                                              .copyWith(
                                                  color: isCurrentPage
                                                      ? Colors.white
                                                      : PartnerAppColors.blue,
                                                  fontWeight:
                                                      FontWeight.bold))),
                                ),
                              ),
                              beforeLineStyle: const LineStyle(
                                  thickness: 1.0,
                                  color: PartnerAppColors.spanishGrey),
                              endChild: Container(
                                  margin: const EdgeInsets.only(top: 5),
                                  constraints: const BoxConstraints(
                                      maxHeight: 30, minWidth: 50),
                                  child: Text(
                                      contractVm.editContractItemCount == 5
                                          ? itemCount5[index]
                                          : itemCount6[index],
                                      style:
                                          Theme.of(context).textTheme.bodySmall,
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.center)),
                            );
                          },
                        )),
                    SmartGaps.gapH20
                  ],
                  if (contractVm.isContractDataLoading) ...[
                    SmartGaps.gapH50,
                    const Center(child: CircularProgressIndicator())
                  ] else ...[
                    step(
                        step: contractVm.editContractStep,
                        formKey: formKey,
                        contractVm: contractVm)
                  ],
                ],
              );
            }),
          )),
    );
  }

  Widget buttonNav({required Map<String, dynamic> newMap}) {
    return Consumer<ContractViewModel>(builder: (_, contractVm, __) {
      return Container(
          padding:
              const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
          color: Colors.white,
          child: Row(children: [
            if (contractVm.editContractStep != 0) ...[
              Expanded(
                  child: TextButton(
                      style: TextButton.styleFrom(
                          fixedSize:
                              Size(MediaQuery.of(context).size.width, 45),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                              side: const BorderSide(
                                  color: PartnerAppColors.darkBlue))),
                      child: Text(
                        tr('back'),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(color: PartnerAppColors.darkBlue),
                      ),
                      onPressed: () {
                        if (!contractVm.isContractDataLoading) {
                          contractVm.editContractPreviousStep();
                          tempSteps--;
                        }
                      })),
              SmartGaps.gapW10
            ],
            Expanded(
              child: TextButton(
                style: TextButton.styleFrom(
                    fixedSize: Size(MediaQuery.of(context).size.width, 45),
                    backgroundColor: PartnerAppColors.malachite,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    )),
                child: Text(
                  contractVm.editContractItemCount ==
                          (contractVm.editContractStep + 1)
                      ? tr('update_contract')
                      : tr('next'),
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium!
                      .copyWith(color: Colors.white),
                ),
                onPressed: () async {
                  if (!contractVm.isContractDataLoading) {
                    switch (contractVm.editContractStep + 1) {
                      case 2:
                        if (formKey.currentState!.validate()) {
                          for (var element
                              in formKey.currentState!.fields.entries) {
                            newMap.addAll(
                                {element.key: '${element.value.value}'});
                          }
                          contractVm.editContractNextStep();
                          tempSteps++;
                        }
                        break;
                      case 3:
                        if (formKey.currentState!.validate()) {
                          for (var element
                              in formKey.currentState!.fields.entries) {
                            newMap.addAll(
                                {element.key: '${element.value.value}'});
                          }
                          contractVm.editContractNextStep();
                          tempSteps++;
                        }
                        break;
                      case 4:
                        if (formKey.currentState!.validate()) {
                          for (var element
                              in formKey.currentState!.fields.entries) {
                            newMap.addAll(
                                {element.key: '${element.value.value}'});
                          }
                          contractVm.editContractNextStep();
                          tempSteps++;
                        }
                        break;
                      case 5:
                        final supplierInfo = context.read<WidgetsViewModel>();
                        final projectVm = context.read<MyProjectsViewModel>();
                        final projectId = projectVm.activeProject.id!;
                        final companyName =
                            supplierInfo.company!.bCrmCompany?.companyName ??
                                '';
                        final currentContext =
                            myGlobals.homeScaffoldKey?.currentContext ??
                                context;

                        if (contractVm.editContractItemCount != 5) {
                          if (contractVm.addendumText.isNotEmpty) {
                            for (var element
                                in formKey.currentState!.fields.entries) {
                              newMap.addAll(
                                  {element.key: contractVm.addendumText});
                            }
                            contractVm.editContractNextStep();
                            tempSteps++;
                          } else {
                            context.showErrorSnackBar(message: tr('required'));
                          }
                        } else {
                          if (formKey.currentState!.validate()) {
                            modalManager.showLoadingModal();
                            for (var element
                                in formKey.currentState!.fields.entries) {
                              newMap.addAll(
                                  {element.key: '${element.value.value}'});
                            }

                            await tryCatchWrapper(
                                context: currentContext,
                                function:
                                    Future.value(contractVm.submitContractV2(
                                  newForm: newMap,
                                  isContractUpdate: true,
                                  cvr: supplierInfo.company!.bCrmCompany?.cvr ??
                                      '',
                                  companyName: companyName,
                                  companyId:
                                      projectVm.activeProject.partner!.id!,
                                  contractId:
                                      projectVm.activeProject.contractId,
                                  projectId: projectId,
                                ))).then((value) {
                              if (!mounted) return;

                              if ((value ?? false)) {
                                projectVm
                                    .getProjectId(projectid: projectId)
                                    .then((value) async {
                                  if (!mounted) return;

                                  modalManager.hideLoadingModal();
                                  successDialog(context: context, exit: () {})
                                      .whenComplete(() {
                                    backDrawerRoute();
                                    changeDrawerRoute(Routes.contractPreview);
                                    contractVm.contractDetailsToDefault();
                                  });
                                });
                              }
                            }).timeout(const Duration(minutes: 1),
                                onTimeout: () {
                              modalManager.hideLoadingModal();
                            });
                          }
                        }
                        break;
                      case 6:
                        final supplierInfo = context.read<WidgetsViewModel>();
                        final projectVm = context.read<MyProjectsViewModel>();
                        final projectId = projectVm.activeProject.id!;
                        final companyName =
                            supplierInfo.company!.bCrmCompany?.companyName ??
                                '';
                        final currentContext =
                            myGlobals.homeScaffoldKey?.currentContext ??
                                context;

                        if (formKey.currentState!.validate()) {
                          modalManager.showLoadingModal();
                          for (var element
                              in formKey.currentState!.fields.entries) {
                            newMap.addAll(
                                {element.key: '${element.value.value}'});
                          }

                          await tryCatchWrapper(
                              context: currentContext,
                              function:
                                  Future.value(contractVm.submitContractV2(
                                newForm: newMap,
                                isContractUpdate: true,
                                cvr: supplierInfo.company!.bCrmCompany?.cvr ??
                                    '',
                                companyName: companyName,
                                companyId: projectVm.activeProject.partner!.id!,
                                contractId: projectVm.activeProject.contractId,
                                projectId: projectId,
                              ))).then((value) {
                            if (!mounted) return;

                            if ((value ?? false)) {
                              projectVm
                                  .getProjectId(projectid: projectId)
                                  .then((value) async {
                                if (!mounted) return;

                                modalManager.hideLoadingModal();
                                successDialog(context: context, exit: () {})
                                    .whenComplete(() {
                                  backDrawerRoute();
                                  changeDrawerRoute(Routes.contractPreview);
                                  contractVm.contractDetailsToDefault();
                                });
                              });
                            }
                          }).timeout(const Duration(minutes: 1), onTimeout: () {
                            modalManager.hideLoadingModal();
                          });
                        }
                        break;
                      default:
                        if (formKey.currentState!.validate()) {
                          for (var element
                              in formKey.currentState!.fields.entries) {
                            newMap.addAll(
                                {element.key: '${element.value.value}'});
                          }
                          contractVm.editContractNextStep();
                          tempSteps++;
                        }
                        break;
                    }
                  }
                },
              ),
            )
          ]));
    });
  }

  Widget step(
      {required int step,
      required GlobalKey<FormBuilderState> formKey,
      required ContractViewModel contractVm}) {
    switch (step + 1) {
      case 2:
        return EditContractStepTwo(contractVm: contractVm);
      case 3:
        return EditContractStepThree(contractVm: contractVm);
      case 4:
        return EditContractStepFour(contractVm: contractVm);
      case 5:
        return EditContractStepFive(contractVm: contractVm);
      case 6:
        return EditContractStepSix(contractVm: contractVm);
      default:
        return const EditContractStepOne();
    }
  }
}
