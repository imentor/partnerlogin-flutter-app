import 'package:Haandvaerker.dk/ui/screens/contract/components/info_form_v2.dart';
import 'package:flutter/material.dart';

class EditContractStepOne extends StatefulWidget {
  const EditContractStepOne({super.key});

  @override
  State<EditContractStepOne> createState() => _EditContractStepOneState();
}

class _EditContractStepOneState extends State<EditContractStepOne> {
  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: InfoFormV2(isContractUpdate: true),
    );
  }
}
