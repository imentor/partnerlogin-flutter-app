import 'package:Haandvaerker.dk/model/contract/contract_template_model.dart';
import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/contract/components/info_form_v2.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer_contract/components/contract_wizard_dynamic_forms.dart';
import 'package:Haandvaerker.dk/utils/extension/snackbar.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/contract/contract_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/widgets_recommendations_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';

class ContractWizardV2 extends StatefulWidget {
  const ContractWizardV2({super.key, this.isContractUpdate = false});
  final bool isContractUpdate;

  @override
  State<ContractWizardV2> createState() => _ContractWizardV2State();
}

class _ContractWizardV2State extends State<ContractWizardV2> {
  final formKey = GlobalKey<FormBuilderState>();

  int tempPris = 0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final contractVm = context.read<ContractViewModel>();
      final projectViewModel = context.read<MyProjectsViewModel>();
      final tenderViewModel = context.read<TenderFolderViewModel>();
      final createOfferVm = context.read<CreateOfferViewModel>();

      final offerList = projectViewModel.activeProject.offers ?? <Offer>[];
      final offerId = offerList.isNotEmpty ? offerList.first.id ?? 0 : 0;
      final acceptedOfferVersionId =
          createOfferVm.offerVersionList?.acceptedVersionId ?? 0;
      final isHovedentreprise =
          projectViewModel.activeProject.contractType == 'Hovedentreprise';

      contractVm.isContractDataLoading = true;

      await tryCatchWrapper(
              context: myGlobals.homeScaffoldKey!.currentContext,
              function:
                  Future.value(createOfferVm.getOfferById(offerId: offerId)))
          .whenComplete(() async {
        await tryCatchWrapper(
            context: myGlobals.innerScreenScaffoldKey!.currentContext,
            function: Future.value(contractVm.initContractWizard(
              acceptedOfferVersionId: acceptedOfferVersionId,
              parentProjectId: isHovedentreprise
                  ? projectViewModel.activeProject.id!
                  : projectViewModel.activeProject.parentId!,
              contactId: projectViewModel.activeProject.homeOwner!.id!,
              projectId: projectViewModel.activeProject.id!,
              offerId: projectViewModel.activeProject.offers!.first.id!,
              wholeEnterprise: isHovedentreprise ? '1' : '0',
              companyId: projectViewModel.activeProject.partner!.id!,
              wizardsTexts: tenderViewModel.wizardTexts,
              industryList:
                  projectViewModel.activeProject.industryInfoNew!.industry,
              homeownerId: projectViewModel.activeProject.homeOwner!.id!,
              jobList: projectViewModel.activeProject.jobList,
              isContractUpdate: widget.isContractUpdate,
              contractId: projectViewModel.activeProject.contractId,
            ))).whenComplete(() async {
          if (contractVm.contractTemplateModel != null) {
            final steps = contractVm.contractTemplateModel!.steps;
            if (steps != null && steps is Map<String, dynamic>) {
              for (final item in steps.values) {
                if (item['label'] == 'Pris') {
                  final vatPrice = (contractVm.contract?.vatPrice ??
                          (createOfferVm.offerVersionList?.totalVat ?? 1000.00))
                      .toString()
                      .split('.')
                      .first;
                  tempPris = int.tryParse(vatPrice) ?? 1000;
                  contractVm.prisUnformattedValue = vatPrice;
                }
              }
            }
          }
          contractVm.isContractDataLoading = false;
        });
      });
    });
  }

  void onPressed() async {
    final projectViewModel = context.read<MyProjectsViewModel>();
    final contractVm = context.read<ContractViewModel>();
    final supplierInfo = context.read<WidgetsViewModel>();
    final projectVm = context.read<MyProjectsViewModel>();

    final steps = contractVm.contractTemplateModel!.steps;

    Map<String, dynamic> newMap = {};

    if (formKey.currentState!.validate()) {
      if (tempPris !=
          double.parse((contractVm.prisUnformattedValue.isEmpty
              ? '10000'
              : contractVm.prisUnformattedValue))) {
        if (contractVm.addendumText.isEmpty) {
          context.showErrorSnackBar(
              message: '${tr('required')}: ${tr('additions_to_contract')}');
          return null;
        } else {
          newMap.addAll({'ADDENDUM': contractVm.addendumText});
        }
      }

      modalManager.showLoadingModal();

      for (var element in formKey.currentState!.fields.entries) {
        newMap.addAll({element.key: '${element.value.value}'});
      }

      if (steps != null && steps is Map<String, dynamic>) {
        for (final item in steps.values) {
          final template = TemplateSteps.fromJson(item as Map<String, dynamic>);
          if (item['label'] == 'Aftalen') {
            final step = template;
            List<Map<String, dynamic>> tempMap = step.fields!.map((e) {
              return {
                'name': '${step.key}:${e!.key}:${e.fieldName}',
                'defaultValue': e.defaultValue,
              };
            }).toList();
            for (var element in tempMap) {
              if (element['defaultValue'] != null) {
                newMap.addAll({element['name']: element['defaultValue']});
              }
            }
          }
        }
      }

      final contractSubmitted = await tryCatchWrapper(
          context: context,
          function: contractVm.submitContractV2(
            newForm: newMap,
            isContractUpdate: widget.isContractUpdate,
            cvr: supplierInfo.company!.bCrmCompany?.cvr ?? '',
            companyName: supplierInfo.company!.bCrmCompany?.companyName ?? '',
            companyId: projectVm.activeProject.partner!.id!,
            contractId: projectVm.activeProject.contractId,
            projectId: projectViewModel.activeProject.id!,
          )).timeout(const Duration(minutes: 1), onTimeout: () {
        modalManager.hideLoadingModal();
        return null;
      });

      if ((contractSubmitted ?? false)) {
        await projectVm.getProjectId(projectid: projectVm.activeProject.id!);
        if (!mounted) return;

        modalManager.hideLoadingModal();
        successDialog(context: context, exit: () {}).whenComplete(() {
          backDrawerRoute();
          changeDrawerRoute(Routes.contractPreview);
          contractVm.contractDetailsToDefault();
        });
      } else {
        modalManager.hideLoadingModal();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      bottomNavigationBar: Container(
        padding:
            const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
        color: Colors.white,
        child: SizedBox(
          height: 45,
          width: MediaQuery.of(context).size.width,
          child: CustomDesignTheme.flatButtonStyle(
            backgroundColor: PartnerAppColors.malachite,
            onPressed: () => onPressed(),
            child: Text(
              widget.isContractUpdate ? tr('update_contract') : tr('submit'),
              style: Theme.of(context).textTheme.titleMedium!.copyWith(
                  fontSize: 16,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: FormBuilder(
            key: formKey,
            child: Consumer<ContractViewModel>(builder: (_, vm, __) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('UDKAST KONTRAKT',
                      style: Theme.of(context).textTheme.headlineLarge),
                  SmartGaps.gapH20,
                  if (!vm.isContractDataLoading) ...[
                    ...fullForms(vm: vm),
                    SmartGaps.gapH5,
                    const ConsumerDataForm(),
                    if (tempPris !=
                        double.parse((vm.prisUnformattedValue.isEmpty
                            ? '10000'
                            : vm.prisUnformattedValue))) ...[
                      SmartGaps.gapH20,
                      addendumForm(vm: vm),
                    ]
                  ],
                  if (vm.isContractDataLoading) ...[
                    SmartGaps.gapH50,
                    const Center(child: CircularProgressIndicator())
                  ],
                ],
              );
            }),
          ),
        ),
      ),
    );
  }

  List<Widget> fullForms({required ContractViewModel vm}) {
    final createOfferVm = context.read<CreateOfferViewModel>();
    final CurrencyTextInputFormatter inputFormatter =
        CurrencyTextInputFormatter(
      NumberFormat.currency(locale: 'da', decimalDigits: 2, symbol: 'kr'),
    );
    List<Widget> sections = [];

    sections.add(Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: InfoFormV2(isContractUpdate: widget.isContractUpdate),
    ));

    if (vm.contractTemplateModel != null) {
      final steps = vm.contractTemplateModel!.steps;
      if (steps != null && steps is Map<String, dynamic>) {
        for (final item in steps.values) {
          if (item['label'] == 'Pris') {
            if (item['fields'].last['default_value'].isEmpty ||
                item['fields'].last['default_value'] == null) {
              item['fields'].last['default_value'] = inputFormatter.formatString(
                  '${(vm.contract?.vatPrice ?? (createOfferVm.offerVersionList?.totalVat ?? 1000.00)).toString().split('.').first}.00');
            }
          }
          final template = TemplateSteps.fromJson(item as Map<String, dynamic>);
          if (item['label'] == 'Tidsplan' || item['label'] == 'Pris') {
            sections.add(Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: ContractWizardDynamicForms(
                  contractFields: (vm.contractTemplateModel?.fields ?? [])
                      .where((field) => field != null)
                      .cast<ContractTemplateField>()
                      .toList(),
                  step: template),
            ));
          }
        }
      }
    }

    return sections;
  }

  Widget addendumForm({required ContractViewModel vm}) {
    if (vm.contractTemplateModel != null) {
      final steps = vm.contractTemplateModel!.steps;
      if (steps != null && steps is Map<String, dynamic>) {
        for (final item in steps.values) {
          final template = TemplateSteps.fromJson(item as Map<String, dynamic>);
          if (item['label'] == 'Addendum') {
            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: ContractWizardDynamicForms(
                  contractFields: (vm.contractTemplateModel?.fields ?? [])
                      .where((field) => field != null)
                      .cast<ContractTemplateField>()
                      .toList(),
                  step: template),
            );
          }
        }
      }
    }

    return const SizedBox.shrink();
  }
}
