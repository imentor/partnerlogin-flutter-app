import 'package:Haandvaerker.dk/ui/components/custom_form_builder_fields.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/contract/contract_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/widgets_recommendations_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

class InfoFormV2 extends StatefulWidget {
  const InfoFormV2({super.key, required this.isContractUpdate});
  final bool isContractUpdate;

  @override
  State<InfoFormV2> createState() => _InfoFormV2State();
}

class _InfoFormV2State extends State<InfoFormV2> {
  @override
  Widget build(BuildContext context) {
    final supplierInfo = context.read<WidgetsViewModel>();
    final contractVm = context.read<ContractViewModel>();

    final bcrmCompanyName =
        supplierInfo.company!.bCrmCompany?.title?.split(' ')[0] ?? '';
    final bcrmCompanyLastName =
        supplierInfo.company!.bCrmCompany!.title!.split(' ').length > 1
            ? supplierInfo.company!.bCrmCompany?.title?.split(' ')[1] ?? ''
            : '';
    final bcrmCompanyEmail = supplierInfo.company!.bCrmCompany?.email ?? '';
    final bcrmCompanyPhone =
        supplierInfo.company!.bCrmCompany?.mobile.toString() ?? '';

    final companyName = contractVm.contract?.companyName ?? '';
    final companyLastName = contractVm.contract?.companyLastName ?? '';
    final companyEmail = contractVm.contract?.companyEmail ?? '';
    final companyPhone = contractVm.contract?.companyPhone ?? '';

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(tr('contracting_party'),
            style: Theme.of(context).textTheme.headlineLarge),
        SmartGaps.gapH20,
        CustomTextFieldFormBuilder(
          name: 'partnerFirstName',
          labelText: tr('first_name'),
          keyboardType: TextInputType.name,
          validator: FormBuilderValidators.required(errorText: tr('required')),
          initialValue: !widget.isContractUpdate
              ? (!contractVm.isCreateNewContractVersion
                  ? bcrmCompanyName
                  : companyName)
              : companyName,
        ),
        CustomTextFieldFormBuilder(
          name: 'partnerLastName',
          labelText: tr('last_name'),
          keyboardType: TextInputType.name,
          validator: FormBuilderValidators.required(errorText: tr('required')),
          initialValue: !widget.isContractUpdate
              ? (!contractVm.isCreateNewContractVersion
                  ? bcrmCompanyLastName
                  : companyLastName)
              : companyLastName,
        ),
        CustomTextFieldFormBuilder(
            name: 'partnerAddress',
            labelText: tr('address'),
            validator:
                FormBuilderValidators.required(errorText: tr('required')),
            initialValue:
                supplierInfo.company!.bCrmCompany?.companyAddress ?? ''),
        CustomTextFieldFormBuilder(
          name: 'partnerZipCode',
          labelText: tr('zip_code'),
          keyboardType: TextInputType.number,
          validator: FormBuilderValidators.required(errorText: tr('required')),
          initialValue:
              supplierInfo.company!.bCrmCompany?.zipCode.toString() ?? '',
        ),
        CustomTextFieldFormBuilder(
          name: 'partnerTown',
          labelText: tr('city'),
          validator: FormBuilderValidators.required(errorText: tr('required')),
          initialValue: supplierInfo.company!.bCrmCompany?.city ?? '',
        ),
        CustomTextFieldFormBuilder(
          name: 'partnerEmail',
          labelText: tr('email'),
          keyboardType: TextInputType.emailAddress,
          validator: FormBuilderValidators.required(errorText: tr('required')),
          initialValue: !widget.isContractUpdate
              ? (!contractVm.isCreateNewContractVersion
                  ? bcrmCompanyEmail
                  : companyEmail)
              : companyEmail,
        ),
        CustomTextFieldFormBuilder(
          name: 'partnerPhoneNumber',
          labelText: tr('telephone'),
          keyboardType: TextInputType.number,
          inputFormatters: [LengthLimitingTextInputFormatter(8)],
          validator: FormBuilderValidators.required(errorText: tr('required')),
          initialValue: !widget.isContractUpdate
              ? (!contractVm.isCreateNewContractVersion
                  ? bcrmCompanyPhone
                  : companyPhone)
              : companyPhone,
        ),
        SmartGaps.gapH10,
        if (widget.isContractUpdate) ...[const ConsumerDataForm()],
      ],
    );
  }
}

class ConsumerDataForm extends StatelessWidget {
  const ConsumerDataForm({super.key});

  @override
  Widget build(BuildContext context) {
    final projectViewModel = context.read<MyProjectsViewModel>();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(tr('consumer'),
            style: Theme.of(context)
                .textTheme
                .bodyLarge!
                .copyWith(fontWeight: FontWeight.bold)),
        SmartGaps.gapH20,
        Text(projectViewModel.activeProject.homeOwner?.name ?? '',
            style: Theme.of(context)
                .textTheme
                .bodyLarge!
                .copyWith(fontWeight: FontWeight.normal)),
        SmartGaps.gapH5,
        Text(projectViewModel.activeProject.homeOwner?.email ?? '',
            style: Theme.of(context)
                .textTheme
                .bodyLarge!
                .copyWith(fontWeight: FontWeight.normal)),
        SmartGaps.gapH5,
        Text(projectViewModel.activeProject.homeOwner?.mobile ?? '',
            style: Theme.of(context)
                .textTheme
                .bodyLarge!
                .copyWith(fontWeight: FontWeight.normal)),
      ],
    );
  }
}
