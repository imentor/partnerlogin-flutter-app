import 'dart:io' as io;

import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/contract/contract_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class ContractSendMessage extends StatefulWidget {
  const ContractSendMessage({super.key});

  @override
  ContractSendMessageState createState() => ContractSendMessageState();
}

class ContractSendMessageState extends State<ContractSendMessage> {
  final formKey = GlobalKey<FormBuilderState>();
  final List<io.File> _uploadedFiles = [];
  final controller = HtmlEditorController();

  bool isSent = false;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: SingleChildScrollView(
        child: FormBuilder(
          key: formKey,
          child: Consumer<ContractViewModel>(
            builder: (context, vm, _) {
              if (vm.busy) {
                return SizedBox(height: 300, child: Center(child: loader()));
              }

              if (isSent) {
                return Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: const Icon(Icons.close),
                        )
                      ],
                    ),
                    Icon(
                      FeatherIcons.checkCircle,
                      color: Theme.of(context).colorScheme.secondary,
                      size: 45,
                    ),
                    SmartGaps.gapH20,
                    Text(tr('message_sent_successfully'),
                        style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                            fontSize: 18,
                            color: Theme.of(context)
                                .colorScheme
                                .onSurfaceVariant)),
                  ],
                );
              }

              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(tr('new_message'),
                          style: Theme.of(context)
                              .textTheme
                              .bodyLarge!
                              .copyWith(
                                  fontSize: 18,
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onSurfaceVariant)),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: const Icon(Icons.close),
                      )
                    ],
                  ),
                  const Divider(),
                  SmartGaps.gapH20,
                  Text(vm.contract!.project!.homeOwner!.name!,
                      style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                          fontSize: 18,
                          color:
                              Theme.of(context).colorScheme.onSurfaceVariant)),
                  SmartGaps.gapH10,
                  FormBuilderTextField(
                    name: 'title',
                    initialValue: 'Review Pdf',
                    validator: FormBuilderValidators.required(
                        errorText: tr('this_field_cannot_be_empty')),
                    decoration: InputDecoration(
                        hintStyle: const TextStyle(color: Colors.grey),
                        hintText: tr('input_title'),
                        errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context).colorScheme.error)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context)
                                    .colorScheme
                                    .onSurfaceVariant
                                    .withValues(alpha: .4))),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context)
                                    .colorScheme
                                    .onSurfaceVariant
                                    .withValues(alpha: .4)))),
                  ),
                  SmartGaps.gapH10,
                  SizedBox(
                    height: 300,
                    child: HtmlEditor(
                      controller: controller,
                      htmlEditorOptions: HtmlEditorOptions(
                        hint: '${tr('message')}...',
                        shouldEnsureVisible: true,
                      ),
                    ),
                  ),
                  SmartGaps.gapH20,
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: () async {
                          final keySelected = await showModalActionSheet(
                            context: context,
                            actions: [
                              SheetAction(
                                label: tr('camera'),
                                icon: Icons.camera_alt,
                                key: 'camera',
                              ),
                              SheetAction(
                                label: tr('gallery'),
                                icon: Icons.add_photo_alternate,
                                key: 'gallery',
                              ),
                            ],
                          );

                          final picker = ImagePicker();

                          XFile? file = await picker.pickImage(
                              source: keySelected == 'camera'
                                  ? ImageSource.camera
                                  : ImageSource.gallery);

                          if (file != null) {
                            setState(() {
                              _uploadedFiles.add(io.File(file.path));
                            });
                          }
                        },
                        child: DottedBorder(
                          color: Theme.of(context).colorScheme.primary,
                          dashPattern: const [2, 4, 2, 4],
                          strokeWidth: 2,
                          strokeCap: StrokeCap.round,
                          borderType: BorderType.RRect,
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            padding: const EdgeInsets.all(20),
                            child: Column(
                              children: [
                                Icon(
                                  FeatherIcons.uploadCloud,
                                  color: Theme.of(context).colorScheme.primary,
                                  size: 30,
                                ),
                                RichText(
                                  text: TextSpan(
                                    children: [
                                      TextSpan(
                                          text: '${tr('drop_file_here')} ',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyLarge!
                                              .copyWith(
                                                  fontSize: 18,
                                                  color: Theme.of(context)
                                                      .colorScheme
                                                      .onSurfaceVariant)),
                                      TextSpan(
                                          text: tr('or_click_to_upload'),
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyLarge!
                                              .copyWith(
                                                  fontSize: 18,
                                                  color: Theme.of(context)
                                                      .colorScheme
                                                      .primary))
                                    ],
                                  ),
                                ),
                                Text(tr('file_less_than_1GB'),
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyLarge!
                                        .copyWith(
                                            fontSize: 18,
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary))
                              ],
                            ),
                          ),
                        ),
                      ),
                      SmartGaps.gapH10,
                      ..._uploadedFiles.map(
                        (file) => Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text(
                                  file.path
                                      .split(io.Platform.pathSeparator)
                                      .last,
                                  overflow: TextOverflow.ellipsis,
                                  style: const TextStyle(fontSize: 16),
                                ),
                              ),
                              GestureDetector(
                                onTap: () => setState(() {
                                  _uploadedFiles
                                      .removeWhere((e) => e.uri == file.uri);
                                }),
                                child: const Icon(Icons.delete_outline,
                                    size: 20, color: Colors.red),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  SmartGaps.gapH20,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      CustomDesignTheme.flatButtonStyle(
                        backgroundColor:
                            Theme.of(context).colorScheme.secondary,
                        onPressed: () async {
                          final txt = await controller.getText();
                          if (formKey.currentState!.saveAndValidate()) {
                            final isMessageSent = await vm.contractSendMessage(
                                contactId: vm.contract!.project!.homeOwner!.id!,
                                title: formKey.currentState!.value['title'],
                                message: Bidi.stripHtmlIfNeeded(txt),
                                files: _uploadedFiles);

                            setState(() {
                              isSent = isMessageSent!;
                            });
                          }
                        },
                        child: Text(
                          tr('send'),
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium!
                              .copyWith(
                                  fontSize: 16,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  )
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
