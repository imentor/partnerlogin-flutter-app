import 'package:Haandvaerker.dk/model/deficiency/deficiency_model.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/deficiency/deficiency_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

class CommentDeficiency extends StatefulWidget {
  final DeficiencyModel? deficiency;
  const CommentDeficiency({super.key, this.deficiency});
  @override
  CommentDeficiencyState createState() => CommentDeficiencyState();
}

class CommentDeficiencyState extends State<CommentDeficiency> {
  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: SingleChildScrollView(
        child: FormBuilder(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(widget.deficiency!.title!,
                  style: Theme.of(context)
                      .textTheme
                      .bodyLarge!
                      .copyWith(fontSize: 20, fontWeight: FontWeight.w600)),
              SmartGaps.gapH10,
              SizedBox(
                height: 75,
                child: ListView.separated(
                  separatorBuilder: (context, index) {
                    return SmartGaps.gapW10;
                  },
                  itemBuilder: (context, index) {
                    return Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Theme.of(context)
                                  .colorScheme
                                  .onSurfaceVariant
                                  .withValues(alpha: .4)),
                          borderRadius: BorderRadius.circular(8)),
                      height: 75,
                      width: 75,
                      child: CachedNetworkImage(
                        imageUrl: widget.deficiency!.images![index]!.imageLink!,
                        width: 75,
                        height: 75,
                        fit: BoxFit.cover,
                      ),
                    );
                  },
                  itemCount: widget.deficiency!.images!.length,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                ),
              ),
              SmartGaps.gapH10,
              Text(widget.deficiency!.description!,
                  style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                        fontSize: 18,
                      )),
              SmartGaps.gapH20,
              const Divider(),
              SmartGaps.gapH20,
              ...widget.deficiency!.comments!.map((e) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                              text: e!.createdBy!.name,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyLarge!
                                  .copyWith(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600)),
                          TextSpan(
                              text:
                                  '  ${Formatter.formatDateStrings(type: DateFormatType.fullMonthYearDate, dateString: e.dateCreated)}',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyLarge!
                                  .copyWith(
                                      fontSize: 14,
                                      fontWeight: FontWeight.normal,
                                      color: Colors.grey)),
                        ],
                      ),
                    ),
                    SmartGaps.gapH20,
                    Text(e.comment!,
                        style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                              fontSize: 14,
                            )),
                    SmartGaps.gapH20,
                  ],
                );
              }),
              const Divider(),
              FormBuilderTextField(
                name: 'comment',
                minLines: 5,
                maxLines: 5,
                validator: FormBuilderValidators.required(
                    errorText: tr('this_field_cannot_be_empty')),
                decoration: InputDecoration(
                    hintText: tr('add_comment'),
                    errorBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Theme.of(context).colorScheme.error)),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Theme.of(context)
                                .colorScheme
                                .onSurfaceVariant
                                .withValues(alpha: .4))),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Theme.of(context)
                                .colorScheme
                                .onSurfaceVariant
                                .withValues(alpha: .4)))),
              ),
              SmartGaps.gapH20,
              Align(
                alignment: Alignment.centerRight,
                child: CustomDesignTheme.flatButtonStyle(
                    onPressed: () {
                      final deficiencyVm = context.read<DeficiencyViewmodel>();
                      final projectsVm = context.read<MyProjectsViewModel>();

                      if (_formKey.currentState!.saveAndValidate()) {
                        deficiencyVm.addDeficiencyComment(
                            projectId: projectsVm.activeProject.id!,
                            deficiencyId: widget.deficiency!.id!,
                            comment: _formKey.currentState!.value['comment']);

                        Navigator.of(context).pop();
                      }
                    },
                    backgroundColor: Theme.of(context).colorScheme.primary,
                    child: Text(
                      tr('add_comment').toUpperCase(),
                      style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: Colors.white),
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    if (widget.deficiency?.images != null) {
      for (var element in widget.deficiency!.images!) {
        if (element != null && element.imageLink != null) {
          CachedNetworkImageProvider(element.imageLink!).evict();
        }
      }
    }
  }
}
