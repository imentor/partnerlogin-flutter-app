import 'dart:io';
import 'dart:math';

import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/deficiency/deficiency_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

class AddDeficiency extends StatefulWidget {
  final Map<String, dynamic>? tempForm;
  const AddDeficiency({super.key, this.tempForm});
  @override
  AddDeficiencyState createState() => AddDeficiencyState();
}

class AddDeficiencyState extends State<AddDeficiency> {
  final _formKey = GlobalKey<FormBuilderState>();
  final List<File> _uploadedFiles = [];

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if (widget.tempForm!.containsKey('images')) {
        for (final a in widget.tempForm!['images'] as List) {
          Random random = Random();
          final file = await _fileFromImageUrl(
              id: random.nextInt(100).toString(), url: a);

          setState(() {
            _uploadedFiles.add(file);
          });
        }
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: SingleChildScrollView(
        child: FormBuilder(
          key: _formKey,
          child: Consumer<DeficiencyViewmodel>(builder: (context, vm, _) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(tr('title'),
                    style: Theme.of(context)
                        .textTheme
                        .bodyLarge!
                        .copyWith(fontSize: 16, color: Colors.grey)),
                SmartGaps.gapH10,
                FormBuilderTextField(
                  name: 'title',
                  initialValue: widget.tempForm!.containsKey('title')
                      ? widget.tempForm!['title']
                      : '',
                  validator: FormBuilderValidators.required(
                      errorText: tr('this_field_cannot_be_empty')),
                  decoration: InputDecoration(
                      hintStyle: const TextStyle(color: Colors.grey),
                      hintText: tr('input_title'),
                      errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Theme.of(context).colorScheme.error)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Theme.of(context)
                                  .colorScheme
                                  .onSurfaceVariant
                                  .withValues(alpha: .4))),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Theme.of(context)
                                  .colorScheme
                                  .onSurfaceVariant
                                  .withValues(alpha: .4)))),
                ),
                SmartGaps.gapH20,
                Text(tr('industry_type'),
                    style: Theme.of(context)
                        .textTheme
                        .bodyLarge!
                        .copyWith(fontSize: 16, color: Colors.grey)),
                SmartGaps.gapH10,
                FormBuilderDropdown(
                  name: 'industry',
                  initialValue: widget.tempForm!.containsKey('industry')
                      ? widget.tempForm!['industry']
                      : null,
                  items: [
                    ...vm.fullIndustries.map((e) => DropdownMenuItem(
                          value: e.brancheDa,
                          child: Text(e.brancheDa!),
                        ))
                  ],
                  isExpanded: false,
                  validator: FormBuilderValidators.required(
                      errorText: tr('this_field_cannot_be_empty')),
                  decoration: InputDecoration(
                      hintText: tr('select_industry'),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Theme.of(context)
                                  .colorScheme
                                  .onSurfaceVariant
                                  .withValues(alpha: .4)))),
                ),
                SmartGaps.gapH20,
                Text(tr('description'),
                    style: Theme.of(context)
                        .textTheme
                        .bodyLarge!
                        .copyWith(fontSize: 16, color: Colors.grey)),
                SmartGaps.gapH10,
                FormBuilderTextField(
                  name: 'description',
                  minLines: 5,
                  maxLines: 5,
                  initialValue: widget.tempForm!.containsKey('description')
                      ? widget.tempForm!['description']
                      : '',
                  validator: FormBuilderValidators.required(
                      errorText: tr('this_field_cannot_be_empty')),
                  decoration: InputDecoration(
                      hintStyle: const TextStyle(color: Colors.grey),
                      hintText: tr('input_description'),
                      errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Theme.of(context).colorScheme.error)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Theme.of(context)
                                  .colorScheme
                                  .onSurfaceVariant
                                  .withValues(alpha: .4))),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Theme.of(context)
                                  .colorScheme
                                  .onSurfaceVariant
                                  .withValues(alpha: .4)))),
                ),
                SmartGaps.gapH20,
                CustomDesignTheme.flatButtonStyle(
                  shape: RoundedRectangleBorder(
                      side: BorderSide(
                          color: Theme.of(context)
                              .colorScheme
                              .onSurfaceVariant
                              .withValues(alpha: .4))),
                  onPressed: () async {
                    final keySelected = await showModalActionSheet(
                      context: context,
                      actions: [
                        SheetAction(
                          label: tr('camera'),
                          icon: Icons.camera_alt,
                          key: 'camera',
                        ),
                        SheetAction(
                          label: tr('gallery'),
                          icon: Icons.add_photo_alternate,
                          key: 'gallery',
                        ),
                      ],
                    );

                    final picker = ImagePicker();

                    XFile? file = await picker.pickImage(
                        source: keySelected == 'camera'
                            ? ImageSource.camera
                            : ImageSource.gallery);

                    if (file != null) {
                      setState(() {
                        _uploadedFiles.add(File(file.path));
                      });
                    }
                  },
                  child: Text(
                    tr('upload_image'),
                    style: Theme.of(context)
                        .textTheme
                        .bodyLarge!
                        .copyWith(fontSize: 16, color: Colors.grey),
                  ),
                ),
                SizedBox(
                  height: 100,
                  child: ListView.separated(
                    separatorBuilder: (context, index) {
                      return SmartGaps.gapW10;
                    },
                    itemBuilder: (context, index) {
                      return Stack(
                        children: [
                          Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                  width: 3,
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onSurfaceVariant
                                      .withValues(alpha: .4),
                                ),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              height: 100,
                              width: 60,
                              child: Image.file(
                                _uploadedFiles[index],
                                fit: BoxFit.cover,
                              )),
                          Positioned(
                            top: 0,
                            right: 0,
                            child: GestureDetector(
                              onTap: () => setState(() {
                                _uploadedFiles.removeWhere(
                                    (e) => e.uri == _uploadedFiles[index].uri);
                              }),
                              child: const Padding(
                                padding: EdgeInsets.all(5),
                                child: Icon(
                                  Icons.cancel_outlined,
                                  size: 14,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      );
                    },
                    itemCount: _uploadedFiles.length,
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                  ),
                ),
                // ..._uploadedFiles.map(
                //   (file) => Padding(
                //     padding: const EdgeInsets.symmetric(vertical: 5),
                //     child: Row(
                //       children: [
                //         Expanded(
                //           child: Text(
                //             file.path.split(Platform.pathSeparator).last,
                //             overflow: TextOverflow.ellipsis,
                //             style: const TextStyle(fontSize: 16),
                //           ),
                //         ),
                //         GestureDetector(
                //           onTap: () => setState(() {
                //             _uploadedFiles
                //                 .removeWhere((e) => e.uri == file.uri);
                //           }),
                //           child: const Icon(Icons.delete_outline,
                //               size: 20, color: Colors.red),
                //         ),
                //       ],
                //     ),
                //   ),
                // ),
                SmartGaps.gapH30,
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  child: CustomDesignTheme.flatButtonStyle(
                      backgroundColor:
                          Theme.of(context).colorScheme.onSurfaceVariant,
                      onPressed: () async {
                        final vm = context.read<DeficiencyViewmodel>();

                        if (_formKey.currentState!.saveAndValidate()) {
                          final projectViewModel =
                              context.read<MyProjectsViewModel>();

                          Map<String, dynamic> form = {
                            'title': _formKey.currentState!.value['title'],
                            'industry':
                                _formKey.currentState!.value['industry'],
                            'description':
                                _formKey.currentState!.value['description'],
                            'endDate': '2023-05-01',
                          };

                          if (widget.tempForm!.isEmpty) {
                            vm.addDeficiency(
                                projectId: projectViewModel.activeProject.id!,
                                form: form,
                                images: _uploadedFiles);
                          } else {
                            vm.updateDeficiency(
                                projectId: projectViewModel.activeProject.id!,
                                deficiencyId: widget.tempForm!['id'] as int,
                                form: form,
                                images: _uploadedFiles);
                          }
                          Navigator.of(context).pop();
                        }
                      },
                      child: Text(
                        widget.tempForm!.isEmpty
                            ? tr('add_list')
                            : tr('update').toUpperCase(),
                        style: Theme.of(context)
                            .textTheme
                            .bodyLarge!
                            .copyWith(fontSize: 20, color: Colors.white),
                      )),
                )
              ],
            );
          }),
        ),
      ),
    );
  }

  Future<File> _fileFromImageUrl({String? id, required String url}) async {
    final response = await http.get(Uri.parse(url));

    final documentDirectory = await getApplicationDocumentsDirectory();

    final file = File(join(documentDirectory.path, '$id.png'));

    file.writeAsBytesSync(response.bodyBytes);

    return file;
  }
}
