import 'package:Haandvaerker.dk/model/deficiency/deficiency_model.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/custom_design.dart';
import 'package:Haandvaerker.dk/ui/components/loader.dart';
import 'package:Haandvaerker.dk/ui/screens/deficiency/components/add_deficiency.dart';
import 'package:Haandvaerker.dk/ui/screens/deficiency/components/comment_deficiency.dart';
import 'package:Haandvaerker.dk/ui/screens/home/components/app_bar.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:Haandvaerker.dk/viewmodel/deficiency/deficiency_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:provider/provider.dart';

class DeficiencyPage extends StatefulWidget {
  const DeficiencyPage({super.key});

  @override
  DeficiencyPageState createState() => DeficiencyPageState();
}

class DeficiencyPageState extends State<DeficiencyPage> {
  bool hey = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final viewModel = context.read<DeficiencyViewmodel>();
      final projectViewModel = context.read<MyProjectsViewModel>();

      await viewModel.getDeficiencyList(
          projectId: projectViewModel.activeProject.id!);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DrawerAppBar(),
      floatingActionButton: Consumer<DeficiencyViewmodel>(
        builder: (context, vm, _) {
          if (vm.preApproved!) return const SizedBox.shrink();
          return InkWell(
            onTap: () {
              addDeficiency(form: {});
            },
            child: Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Theme.of(context).colorScheme.primary),
              child: const Center(
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                  size: 40,
                ),
              ),
            ),
          );
        },
      ),
      bottomNavigationBar: Consumer<DeficiencyViewmodel>(
        builder: (context, vm, _) {
          String buttonName = '';
          if (vm.lastUpdatedBy == 'contact') {
            buttonName = tr('approve_list');
          } else {
            buttonName = tr('submit_for_approval');
          }

          if (vm.preApproved!) {
            return const SizedBox.shrink();
          } else {
            return Container(
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: SizedBox(
                height: 50,
                child: CustomDesignTheme.flatButtonStyle(
                  backgroundColor: Theme.of(context).colorScheme.secondary,
                  onPressed: () {
                    final projectViewModel =
                        context.read<MyProjectsViewModel>();
                    final deficiencyVm = context.read<DeficiencyViewmodel>();
                    if (!vm.preApproved!) {
                      deficiencyVm.preApproveDeficiency(
                          projectId: projectViewModel.activeProject.id!,
                          approve: true);
                    }
                  },
                  child: Text(
                    buttonName.toUpperCase(),
                    style: Theme.of(context)
                        .textTheme
                        .bodyLarge!
                        .copyWith(fontSize: 20, color: Colors.white),
                  ),
                ),
              ),
            );
          }
        },
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Text(tr('deficiency_list'),
                      style: Theme.of(context).textTheme.headlineLarge),
                ),
                SmartGaps.gapW10,
                Consumer<DeficiencyViewmodel>(builder: (_, vm, __) {
                  return InkWell(
                    onTap: () {
                      if (!vm.preApproved!) {
                        showDatePicker(
                                builder: (context, child) {
                                  return Theme(
                                    data: Theme.of(context).copyWith(
                                      colorScheme: ColorScheme.light(
                                        primary: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                        onSurface: Theme.of(context)
                                            .colorScheme
                                            .onSurfaceVariant,
                                      ),
                                    ),
                                    child: child!,
                                  );
                                },
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime.now(),
                                //in the initial code the lastDate was set to DateTime.now()
                                //this limits the user to only select dates in the past.
                                //please do change it back if it should be that way.
                                lastDate: DateTime(3000))
                            .then((value) {
                          if (value != null) {
                            standardDialog(
                                title: tr('update_end_date'),
                                description:
                                    '${tr('update_end_date_desc_1')} ${Formatter.formatDateTime(type: DateFormatType.fullMonthYearDate, date: value)} ${tr('update_end_date_desc_2')}',
                                ok: () {
                                  final projectViewModel =
                                      context.read<MyProjectsViewModel>();

                                  vm.updateDeficiencyEndDate(
                                      projectId:
                                          projectViewModel.activeProject.id!,
                                      endDate: Formatter.formatDateTime(
                                          type: DateFormatType.isoDate,
                                          date: value));
                                });
                          }
                        });
                      }
                    },
                    child: Container(
                      padding: const EdgeInsets.all(10),
                      margin: const EdgeInsets.only(left: 10),
                      decoration:
                          CustomDesignTheme.coldBlueBorderShadows.copyWith(
                        border: Border.all(
                            color: Colors.grey.withValues(alpha: 0.25)),
                      ),
                      child: Row(children: [
                        Text('${tr('end_date')}: ',
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge!
                                .copyWith(color: Colors.grey, fontSize: 14)),
                        const Icon(
                          FeatherIcons.calendar,
                          size: 16,
                          color: PartnerAppColors.blue,
                        ),
                        SmartGaps.gapW5,
                        Text(vm.endDate,
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge!
                                .copyWith(color: Colors.grey, fontSize: 12)),
                      ]),
                    ),
                  );
                }),
              ],
            ),
            SmartGaps.gapH20,
            Consumer<DeficiencyViewmodel>(
              builder: (context, vm, _) {
                if (vm.busy) {
                  return Center(child: loader());
                } else {
                  return Column(
                    children: [
                      ...vm.deficiencyList.map(
                        (e) {
                          return Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: e.status == 'done'
                                      ? Theme.of(context)
                                          .colorScheme
                                          .secondary
                                          .withValues(alpha: .2)
                                      : Colors.white,
                                  border: Border.all(
                                      color: e.status == 'done'
                                          ? Theme.of(context)
                                              .colorScheme
                                              .secondary
                                              .withValues(alpha: .2)
                                          : Theme.of(context)
                                              .colorScheme
                                              .onSurfaceVariant
                                              .withValues(alpha: .3)),
                                ),
                                padding: const EdgeInsets.all(20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(e.title!,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyLarge!
                                                      .copyWith(
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.w600)),
                                              SmartGaps.gapH10,
                                              SizedBox(
                                                height: 50,
                                                child: ListView.separated(
                                                  separatorBuilder:
                                                      (context, index) {
                                                    return SmartGaps.gapW10;
                                                  },
                                                  itemBuilder:
                                                      (context, index) {
                                                    return Container(
                                                      decoration: BoxDecoration(
                                                          border: Border.all(
                                                              color: Theme.of(
                                                                      context)
                                                                  .colorScheme
                                                                  .onSurfaceVariant
                                                                  .withValues(
                                                                      alpha:
                                                                          .4)),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(8)),
                                                      height: 50,
                                                      width: 50,
                                                      child: CachedNetworkImage(
                                                        imageUrl: e
                                                            .images![index]!
                                                            .imageLink!,
                                                        width: 50,
                                                        height: 50,
                                                        fit: BoxFit.cover,
                                                      ),
                                                    );
                                                  },
                                                  itemCount: e.images!.length,
                                                  shrinkWrap: true,
                                                  scrollDirection:
                                                      Axis.horizontal,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        SmartGaps.gapW10,
                                        if (true)
                                          Column(
                                            children: [
                                              Text(tr('mark_as_done'),
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyLarge!
                                                      .copyWith(
                                                          fontSize: 12,
                                                          color: Colors.grey)),
                                              Transform.scale(
                                                  scale: 1.4,
                                                  child: Theme(
                                                    data: ThemeData(),
                                                    child: Checkbox(
                                                        activeColor:
                                                            Theme.of(context)
                                                                .colorScheme
                                                                .secondary,
                                                        value:
                                                            e.status == 'done',
                                                        onChanged: (val) {
                                                          if (val!) {
                                                            standardDialog(
                                                                title: tr(
                                                                    'mark_as_done'),
                                                                description:
                                                                    '${tr('mark_as_done_desc_1')}: ${e.title} ${tr('mark_as_done_desc_2')}',
                                                                ok: () {
                                                                  final projectViewModel =
                                                                      context.read<
                                                                          MyProjectsViewModel>();

                                                                  vm.markAsDoneDeficiency(
                                                                      projectId: projectViewModel
                                                                          .activeProject
                                                                          .id!,
                                                                      deficiencyId:
                                                                          e.id);
                                                                });
                                                          }
                                                        }),
                                                  ))
                                            ],
                                          )
                                      ],
                                    ),
                                    SmartGaps.gapH10,
                                    Text(e.description!,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyLarge!
                                            .copyWith(
                                              fontSize: 16,
                                            )),
                                    SmartGaps.gapH10,
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          padding: const EdgeInsets.all(10),
                                          decoration: BoxDecoration(
                                              color: Colors.grey
                                                  .withValues(alpha: .4),
                                              borderRadius:
                                                  BorderRadius.circular(8)),
                                          child: Text(e.industry!,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyLarge!
                                                  .copyWith(
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w600)),
                                        ),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            GestureDetector(
                                              onTap: () {
                                                viewComment(e);
                                              },
                                              child: Icon(
                                                Icons.mode_comment_outlined,
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .onSurfaceVariant,
                                                size: 22,
                                              ),
                                            ),
                                            if (!vm.preApproved!) ...[
                                              SmartGaps.gapW15,
                                              GestureDetector(
                                                onTap: () {
                                                  addDeficiency(form: {
                                                    'id': e.id,
                                                    'title': e.title,
                                                    'industry': e.industry,
                                                    'description':
                                                        e.description,
                                                    'images': [
                                                      ...e.images!.map(
                                                          (e) => e!.imageLink)
                                                    ]
                                                  });
                                                },
                                                child: Icon(
                                                  FeatherIcons.edit,
                                                  color: Theme.of(context)
                                                      .colorScheme
                                                      .onSurfaceVariant,
                                                  size: 22,
                                                ),
                                              ),
                                              SmartGaps.gapW15,
                                              GestureDetector(
                                                onTap: () {
                                                  standardDialog(
                                                    title: context
                                                        .tr('confirm_delete'),
                                                    description:
                                                        '${tr('confirm_delete_desc_1')}: ${e.title}',
                                                    ok: () {
                                                      final projectViewModel =
                                                          context.read<
                                                              MyProjectsViewModel>();

                                                      vm.deleteDeficiency(
                                                          projectId:
                                                              projectViewModel
                                                                  .activeProject
                                                                  .id!,
                                                          deficiencyId: e.id!);
                                                    },
                                                  );
                                                },
                                                child: Icon(
                                                  FeatherIcons.trash2,
                                                  color: Theme.of(context)
                                                      .colorScheme
                                                      .onSurfaceVariant,
                                                  size: 22,
                                                ),
                                              ),
                                            ]
                                          ],
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              SmartGaps.gapH10,
                            ],
                          );
                        },
                      ),
                    ],
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  Future<void> addDeficiency({Map<String, dynamic>? form}) {
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext dialogContext) {
          return AlertDialog(
            insetPadding: const EdgeInsets.all(20),
            contentPadding: const EdgeInsets.all(20),
            title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
              GestureDetector(
                onTap: () {
                  Navigator.of(dialogContext).pop();
                },
                child: const Icon(
                  Icons.cancel_outlined,
                  color: Colors.grey,
                  size: 30,
                ),
              )
            ]),
            content: AddDeficiency(
              tempForm: form,
            ),
          );
        });
  }

  Future<void> viewComment(DeficiencyModel deficiency) {
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext dialogContext) {
          return AlertDialog(
              insetPadding: const EdgeInsets.all(20),
              contentPadding: const EdgeInsets.all(20),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(dialogContext).pop();
                    },
                    child: const Icon(
                      Icons.cancel_outlined,
                      color: Colors.grey,
                      size: 30,
                    ),
                  )
                ],
              ),
              content: CommentDeficiency(
                deficiency: deficiency,
              ));
        });
  }

  Future<void> standardDialog({
    String? title,
    String? description,
    VoidCallback? ok,
  }) {
    return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          insetPadding: const EdgeInsets.all(20),
          contentPadding: const EdgeInsets.all(20),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(title!,
                  style: Theme.of(context)
                      .textTheme
                      .bodyLarge!
                      .copyWith(fontSize: 18, fontWeight: FontWeight.w600)),
              GestureDetector(
                onTap: () {
                  Navigator.of(dialogContext).pop();
                },
                child: const Icon(
                  Icons.cancel_outlined,
                  color: Colors.grey,
                  size: 30,
                ),
              )
            ],
          ),
          content: SizedBox(
            width: MediaQuery.of(dialogContext).size.width,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Text(description!,
                      style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                          fontSize: 16,
                          fontWeight: FontWeight.normal,
                          color:
                              Theme.of(context).colorScheme.onSurfaceVariant)),
                  SmartGaps.gapH20,
                  SizedBox(
                    height: 50,
                    width: MediaQuery.of(context).size.width,
                    child: CustomDesignTheme.flatButtonStyle(
                      backgroundColor: Theme.of(context).colorScheme.secondary,
                      child: Text(
                        'OK',
                        style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            color: Colors.white),
                      ),
                      onPressed: () {
                        Navigator.of(dialogContext).pop();
                        ok!();
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
