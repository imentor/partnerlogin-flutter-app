import 'dart:developer';

import 'package:Haandvaerker.dk/services/analytics/analytics_service.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/viewmodel/packages/packages_viewmodel.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DrawerRouteObserver extends RouteObserver<Route<dynamic>> {
  void _sendScreenView(Route<dynamic> route) {
    var screenName = route.settings.name;
    if (screenName == '/') {
      APPLIC.currentDrawerRoute = Routes.newsFeed;
    } else {
      APPLIC.currentDrawerRoute = screenName;
    }

    final currentContext = myGlobals.homeScaffoldKey!.currentContext!;
    Provider.of<AnalyticsService>(currentContext, listen: false)
        .logScreenActivity(route: APPLIC.currentDrawerRoute);
  }

  @override
  void didPush(Route<dynamic> route, Route<dynamic>? previousRoute) {
    super.didPush(route, previousRoute);
    try {
      _sendScreenView(route);
    } catch (e) {
      if (kDebugMode) {
        log('Probably should do something about this error. Oh well. $e');
      }
    }
  }

  @override
  void didReplace({Route<dynamic>? newRoute, Route<dynamic>? oldRoute}) {
    super.didReplace(newRoute: newRoute, oldRoute: oldRoute);
    if (newRoute!.settings.name == '/' ||
        newRoute.settings.name == Routes.newsFeed) {
      _refreshPackages();
    }
    try {
      _sendScreenView(newRoute);
    } catch (e) {
      if (kDebugMode) {
        log('Probably should do something about this error. Oh well.');
      }
    }
  }

  @override
  void didPop(Route<dynamic> route, Route<dynamic>? previousRoute) {
    super.didPop(route, previousRoute);
    if (previousRoute!.settings.name == '/' ||
        previousRoute.settings.name == Routes.newsFeed) {
      _refreshPackages();
    }
    try {
      _sendScreenView(previousRoute);
    } catch (e) {
      if (kDebugMode) {
        log('Probably should do something about this error. Oh well.');
      }
    }
  }

  /// Refresh the packages for the current user
  _refreshPackages() {
    final currentContext = myGlobals.homeScaffoldKey!.currentContext!;
    Provider.of<PackagesViewModel>(currentContext, listen: false).getPackages();
  }
}
