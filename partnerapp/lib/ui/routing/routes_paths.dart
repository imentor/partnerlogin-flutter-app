class Routes {
  static const String intro = '/intro';
  static const String leadForm = '/leadform';
  static const String cvr = '/cvr';
  static const String registerChooseJobTypes = 'registerChooseJobTypes';
  static const String verifyPhoneNumber = '/verifyPhoneNumber';
  static const String userRegistration = '/userRegistration';
  static const String login = '/login';
  static const String home = '/home';
  static const String forgotPassword = '/forgotpassword';
  static const String allTaskScreen = '/alltaskscreen';
  static const String messageScreen = '/messages';
  static const String marketPlaceScreen = '/marketPlace';
  static const String udbudScreen = '/udbud';
  static const String m2mLaborScreen = '/m2m/labors';
  static const String m2mLaborFormScreen = '/m2m/labors/form';
  static const String m2mTaskScreen = '/m2m/tasks';
  static const String m2mTaskFormScreen = '/m2m/tasks/form';
  static const String clientOffer = '/your_client/tilbud';
  static const String m2mOptions = '/m2m_options';
  static const String contactUs = '/contact_us';
  static const String settings = '/settings';
  static const String yourClient = '/your_client';
  static const String tilbud = '/your_client/tilbud';
  static const String faq = '/faq';
  static const String changePassword = '/settings/change_password';
  static const String newsFeed = '/newsfeed';
  static const String recommendations = '/recommendations';
  static const String createRecommendation = '/recommendations/create';
  static const String getRecommendations = '/recommendations/get';
  static const String widgetRecommendations = '/recommendations/widgets';
  static const String readAndAnswerRecommendations =
      '/recommendations/read_and_answer';
  static const String sentRecommendations = '/recommendations/sent';
  static const String autoPilotRecommendations = '/recommendations/auto_pilot';
  static const String myProjects = '/projects/mine';
  static const String createProject = '/projects/create';
  static const String holidays = '/settings/holidays';
  static const String creditCards = '/settings/credit_cards';
  static const String supplierService = '/settings/supplierService';
  static const String invoice = '/settings/invoice';
  static const String subscription = '/settings/subscription';
  static const String yourMasterInformation =
      '/settings/your_master_information';
  static const String notifications = '/settings/notifications';
  static const String freeWebsite = '/free_website';
  static const String yourProfilePage = '/settings/your_profile_page';
  static const String addPage = '/free_website/add_page';
  static const String freeWebsiteSettings = '/free_website/settings';
  static const String freeWebsiteThemes = '/free_website/themes';
  static const String permissions = '/free_website/permissions';
  static const String pageOrder = '/free_website/page_order';
  static const String jobTypes = '/settings/job_types';
  static const String chooseJobTypes = '/settings/job_types/choose';
  static const String emailManage = '/email/manage';
  static const String emailCreate = '/email/create';
  static const String emailLogin = '/email/login';
  static const String allMailbox = '/email/mailbox/all';
  static const String emailMailbox = '/email/mailbox';
  static const String emailRead = '/email/read';
  static const String emailWrite = '/email/write';
  static const String logout = '/logout';
  static const String camera = '/camera';
  static const String makeAnOffer = '/make_an_offer';
  static const String allOffers = '/view_offers';
  static const String offerHistory = '/offerHistory';
  static const String partners = '/partners';
  static const String admin = '/admin';
  static const String badges = '/badges';
  static const String badgesPressRelease = '/badges/pressRelease';
  static const String membershipBenefits = '/membership_benefits';
  static const String membershipBenefitsSms = '/membership_benefits/sms';
  static const String phoneNumber = 'settings/phone_numbers';
  static const String loadingScreen = '/loading';
  static const String homeDomainEmail = '/email/domain_home';
  static const String changePasswordDomainEmail =
      '/email/domain_change_password';
  static const String webShopHome = '/webshop';
  static const String webShopProducts = '/webshop/products';
  static const String webshopProductDetail = '/webshop/products/detail';
  static const String webshopCart = '/webshop/products/cart';
  static const String webshopAdsHome = '/webshop/products/ads';
  static const String webshopFacebook = '/webshop/products/facebook';
  static const String webshopFacebookCreatePost =
      '/webshop/products/facebook/createPost';
  static const String webshopGoogle = '/webshop/products/google';
  static const String webshopOrderHistory = '/webshop/products/orders/history';
  static const String webshopOrderDetails = '/webshop/products/orders/details';
  // webshopWishlist
  static const String webshopWishlist = '/webshop/wishlist';
  static const String webSocketScreen = '/websocket';
  static const String webshopTermsAndConditions = '/webshop/termsAndConditions';
  static const String webshopBoostSpecificRecommendation =
      '/webshop/boost_specific_recommendation';
  static const String klima = '/klima';
  static const String klimaPlanClassification = '/klima/plan/classifications';
  static const String klimaPlan = '/klima/plan';
  static const String klimaValues = '/klima/values';
  static const String klimaValuesFields = '/klima/values/fields';
  static const String klimaSurvey = '/klima/survey';
  static const String klimaChecklistSteps = '/klima/survey/steps';
  static const String klimaSurveyIndustry = '/klima/survey/industry';
  static const String klimaDocumentation = '/klima/documentation';
  static const String klimaReport = '/klima/report';
  static const String integration = '/integration';
  static const String feedbackCustomerList =
      '/marketplace/feedback/customerList';
  static const String feedbackPriceSlider = '/marketplace/feedback/priceSlider';
  static const String feedbackNps = '/marketplace/feedback/nps';
  static const String appleSubscription = '/settings/subscription/apple';
  static const String education = '/settings/education';
  static const String aboutus = '/aboutus';
  static const String jobDetails = '/jobDetails';
  static const String taskDetails = '/taskDetails';
  static const String approvedProjects = '/projects/approved';
  static const String projectDashboard = '/projects/dashboard';
  static const String projectViewOffer = '/projects/viewOffer';
  static const String projectTimeline = '/projects/timeline';
  static const String projectMarketplace = '/projects/marketplace';
  static const String customerMinBolig = '/customer/minbolig';
  static const String paymentManagement = '/projects/paymentManagement';
  static const String contractWizard = '/projects/contractWizard';
  static const String contractPreview = '/projects/contractPreview';
  static const String previewContract = '/projects/contract';
  static const String editContract = '/projects/contractEdit';
  static const String projectWizard = '/projects/create/wizard';
  static const String projectDone = '/projects/create/wizard/done';
  static const String viewPaymentDetail = '/projects/paymentManagement/view';
  static const String projectCalendar = '/projects/calendar';
  static const String projectRate = '/projects/rate';
  static const String viewPaymentFiles =
      '/projects/paymentManagement/view/files';
  static const String projectWallet = '/projects/wallet';
  static const String projectWalletTransaction = '/projects/wallet/transaction';
  static const String projectWalletTransactionDetail =
      '/projects/wallet/transaction/detail';
  static const String projectWalletTransactionRequest =
      '/projects/wallet/transaction/request';
  static const String projectWalletTransactionDecline =
      '/projects/wallet/transaction/decline';
  static const String projectWalletExtraWork = '/projects/wallet/extraWork';
  static const String projectWalletExtraWorkDetail =
      '/projects/wallet/extraWork/detail';
  static const String projectWalletExtraWorkRequest =
      '/projects/wallet/extraWork/request';

  static const String projectInviteSubContractor =
      '/projects/invite/subContractor';
  static const String projectInviteManufacturer =
      '/projects/invite/Manufacturer';

  static const String payproffTerms = '/settings/payproff/terms';
  static const String payproffAccount = '/settings/payproff/account';
  static const String projectMessages = '/projects/messages';
  static const String projectMessageThread = '/projects/messages/thread';
  static const String createProjectMessage = '/projects/messages/create';
  static const String projectEdit = '/projects/edit';
  static const String projectDeficiency = '/projects/deficiency';
  static const String deactivateAccount = '/deactivateAccount';

  //NEWLY ADDED
  static const String contructionWallet = '/contructionWallet';
  static const String contructionWalletContractPreview =
      '/contructionWallet/contractPreview';
  static const String contructionWalletEditContract =
      '/contructionWallet/editContract';
  static const String tenderFolder = '/tenderFolder';
  static const String viewTenderFolder = '/viewTenderFolder';
  static const String jobDescriptions = '/jobDescriptions';
  static const String createOffer = '/createOffer';
  static const String createOfferEditDescription =
      '/createOffer/editDescription';
  static const String createOfferAiCalculation = '/createOffer/aiCalculation';
  static const String createOfferWizard = '/createOfferWizard';
  static const String createOfferWizardDescriptionEdit =
      '/createOfferWizardDescriptionEdit';
  static const String uploadOffer = '/uploadOffer';
  static const String updateUploadedOffer = '/updateUploadedOffer';
  static const String marketplaceV2 = 'marketplaceV2';
  static const String viewJobItem = '/viewJobItem';
  static const String jobInvitations = '/jobInvitations';
  static const String inAppWebView = '/inAppWebView';
  static const String messageScreenV2 = '/messageScreenV2';
  static const String messageView = '/messageView';
  static const String paymentMediaFiles = '/paymentMediaFiles';
  static const String paymentUploadMediaFiles = '/paymentUploadMediaFiles';
  static const String projectExtraWork = 'projectExtraWork';
  static const String htmlEditor = '/htmlEditor';
  static const String projectViewsScreen = '/projectViewsScreen';
  static const String payproffIbanScreen = '/payproffIbanScreen';
  static const String payproffVerifiedScreen = '/paproffVerifiedScreen';
  static const String digitalPackage = '/digitalPackage';
  static const String photoDocumentation = '/photoDocumentation';
  static const String viewDocumentation = '/viewDocumentation';
  static const String addDocumentation = '/addDocumentation';
  static const String priceCalculator = '/priceCalculator';
  static const String yourEmail = '/yourEmail';
  static const String notification = '/notification';
  static const String cancelContract = '/cancelContract';
  static const String bankVerification = '/bankVerification';
  static const String editOfferLanding = '/editOfferLanding';
  static const String editOffer = '/editOfferLanding/edit';
  static const String signatureOffer = '/signatureOffer';
  static const String subcontractorAcceptInvite =
      '/subcontractorAcceptInviteScreen';
  static const String subcontractorInviteContract =
      '/subcontractorInviteContract';
  static const String inviteCustomer = '/inviteCustomer';
  static const String inviteCustomerEditDescription =
      '/inviteCustomer/editDescriptions';
  static const String inviteCustomerAiCalculation =
      '/inviteCustomer/aiCalculation';
}
