import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';

void changeDrawerRoute(String? newRouteName, {arguments}) {
  myGlobals.innerScreenScaffoldKey!.currentState!.openEndDrawer();

  var isCurrent = APPLIC.currentDrawerRoute == newRouteName;

  if (!isCurrent) {
    myGlobals.drawerRouteKey!.currentState!
        .pushNamed(newRouteName!, arguments: arguments);
  }
}

void changeDrawerReplaceRoute(String newRouteName, {arguments}) {
  myGlobals.innerScreenScaffoldKey!.currentState!.openEndDrawer();

  var isCurrent = APPLIC.currentDrawerRoute == newRouteName;

  if (!isCurrent) {
    myGlobals.drawerRouteKey!.currentState!
        .pushReplacementNamed(newRouteName, arguments: arguments);
  }
}

void backDrawerRoute() {
  // final appDrawerVm = context.read<AppDrawerViewModel>();
  myGlobals.innerScreenScaffoldKey!.currentState!.openEndDrawer();

  if (myGlobals.drawerRouteKey!.currentState!.canPop()) {
    myGlobals.drawerRouteKey!.currentState!.pop();
    // appDrawerVm.update();
  }
}
