import 'dart:io';

import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/model/messages/message_v2_response_model.dart';
import 'package:Haandvaerker.dk/model/projects/extra_work_response_model.dart';
import 'package:Haandvaerker.dk/ui/components/app_web_view.dart';
import 'package:Haandvaerker.dk/ui/components/custom_page_route.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/about_us_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/app_loading_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/cancel_contract/cancel_contract_page.dart';
import 'package:Haandvaerker.dk/ui/screens/change_password_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/contract/contract_preview.dart';
import 'package:Haandvaerker.dk/ui/screens/contract/contract_wizard_v2.dart';
import 'package:Haandvaerker.dk/ui/screens/contract/edit_contract_wizard/edit_contract_wizard_page.dart';
import 'package:Haandvaerker.dk/ui/screens/create_project/create_project_done.dart';
import 'package:Haandvaerker.dk/ui/screens/create_project/create_project_wizard.dart';
import 'package:Haandvaerker.dk/ui/screens/create_projects_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/create_offer_wizard/create_offer_page_wrapper.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/create_offer_wizard/create_offer_wizard_ai_calculation_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/create_offer_wizard/create_offer_wizard_edit_descriptions_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/job_descriptions.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/tender_folder.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/tender_folder/view_tender_folder.dart';
import 'package:Haandvaerker.dk/ui/screens/customers/your_customer_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/deficiency/deficiency_page.dart';
import 'package:Haandvaerker.dk/ui/screens/digital_package/digital_package_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/domain_email/change_password_domain_email_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/edit_offer/edit_offer.dart';
import 'package:Haandvaerker.dk/ui/screens/edit_offer/landing_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/education/education_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/email/your_email_page.dart';
import 'package:Haandvaerker.dk/ui/screens/faq_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/forgot_password_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/holidays_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/home/home_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/integration/integration_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/invite_customer_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/offer_types/create_offer/step_3/create_offer_ai_calculation_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/invite_customer/offer_types/create_offer/step_3/create_offer_edit_descriptions_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/lead_form_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/login/intro_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/login/login_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/invitations/job_invitations.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/job/job_view.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/marketplace_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/udbud/udbud_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/membership_benefits/membership_benefits_screen_v2.dart';
import 'package:Haandvaerker.dk/ui/screens/membership_benefits/membership_benefits_sms_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/message/message_screen_v2.dart';
import 'package:Haandvaerker.dk/ui/screens/message/view_message_detail.dart';
import 'package:Haandvaerker.dk/ui/screens/mester_til_mester/m2m_labor_form_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/mester_til_mester/m2m_labor_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/mester_til_mester/m2m_task_form_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/mester_til_mester/m2m_task_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/newsfeed/newsfeed_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/notification/notification_page.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer/create_offer_wizard_page.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer/create_offer_wizard_step_3_description_edit.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer_contract/create_offer_contract_page.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer_contract/create_offer_contract_preview.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/create_offer_contract/create_offer_edit_contract.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/project_offer_detail.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/signature_offer/signature_offer.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/upload_offer/create_upload_offer/upload_offer_page.dart';
import 'package:Haandvaerker.dk/ui/screens/offer/upload_offer/update_uploaded_offer.dart/update_uploaded_offer_page.dart';
import 'package:Haandvaerker.dk/ui/screens/payproff/payproff_account_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/payproff/payproff_iban_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/payproff/payproff_terms_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/payproff/payproff_verified_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/phone_numbers_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/price_calculation/price_calculator_page.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/approved_projects_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/calendar/calendar_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/edit_projects_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/accept_invitation/subcontrator/subcontractor_accept_invite_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/accept_invitation/subcontrator/subcontractor_invite_contract_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/manufacturer/invitation_manufacturer_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/invitation_manufacturer_subcontractor/subcontractor/invitation_subcontractor_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/messages/create_project_message.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/my_projects_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/photo_documentation/photo_documentation_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/project_dashboard.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/rate/rate_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/extra_work/extra_work_detail_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/extra_work/extra_work_request_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/extra_work/wallet_extra_work_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/transaction/transaction_decline/wallet_transaction_decline_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/transaction/transaction_request/wallet_transaction_request_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/transaction/wallet_transaction_detail_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/transaction/wallet_transaction_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/wallet/wallet_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/recommendations/create_recommendations.dart';
import 'package:Haandvaerker.dk/ui/screens/recommendations/get_recommendations_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/recommendations/recommendation_auto_pilot.dart';
import 'package:Haandvaerker.dk/ui/screens/recommendations/recommendation_read_answer_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/recommendations/recommendation_widgets_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/recommendations/recommendations_sent/recommendations_sent.dart';
import 'package:Haandvaerker.dk/ui/screens/settings/job_types/job_types_setting.dart';
import 'package:Haandvaerker.dk/ui/screens/settings/your_master_information/your_master_information.dart';
import 'package:Haandvaerker.dk/ui/screens/settings/your_profile_page/components/html_editor.dart';
import 'package:Haandvaerker.dk/ui/screens/settings/your_profile_page/your_profile_page_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/settings_screen.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class RouteGenerator {
  static Route<dynamic> generateMainRoute(RouteSettings settings) {
    switch (settings.name) {
      case Routes.intro:
        return MaterialPageRoute(builder: (_) => const IntroScreen());

      case Routes.leadForm:
        return MaterialPageRoute(builder: (_) => const LeadFormScreen());

      case Routes.login:
        return MaterialPageRoute(builder: (_) => const LoginScreen());

      case Routes.home:
        final isFromAppLink = (settings.arguments as bool? ?? false);
        return CustomMaterialPageRoute(
          builder: (_) => HomeScreen(
            isFromAppLink: isFromAppLink,
          ),
        );

      case Routes.forgotPassword:
        return MaterialPageRoute(builder: (_) => const ForgotPasswordScreen());

      case Routes.loadingScreen:
        return MaterialPageRoute(builder: (_) => const AppSplashScreen());

      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> generateDrawerRoute(RouteSettings settings) {
    APPLIC.currentDrawerRoute = settings.name;

    switch (settings.name) {
      case Routes.settings:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const SettingsScreen());

      case Routes.yourClient:
        final Map<String, dynamic>? args =
            settings.arguments as Map<String, dynamic>?;
        return PageRouteBuilder(
            settings: settings,
            pageBuilder: (BuildContext context, Animation<double> animation,
                    Animation<double> secondaryAnimation) =>
                YourCustomerScreen(
                  isFromAppLink: (args?['isFromAppLink'] as bool?) ?? false,
                  jobId: args?['jobId'] as int?,
                  skipStory: (args?['skipStory'] as bool?) ?? false,
                ));

      case Routes.contactUs:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const ContactScreen());

      case Routes.faq:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const FAQScreen());

      case Routes.changePassword:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const ChangePasswordScreen());

      case Routes.newsFeed:
        return PageRouteBuilder(
          settings: settings,
          pageBuilder: (BuildContext context, Animation<double> animation,
                  Animation<double> secondaryAnimation) =>
              const NewsfeedScreen(),
        );

      case Routes.projectRate:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const RateScreen());

      case Routes.createRecommendation:
        Map<String, dynamic>? recommendationArgs =
            settings.arguments as Map<String, dynamic>?;
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => CreateRecommendationScreen(
                id: recommendationArgs!['cid'].toString()));

      case Routes.getRecommendations:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => const GetRecommendationsScreen());

      case Routes.readAndAnswerRecommendations:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => const RecommendationReadAnswerScreen());

      case Routes.widgetRecommendations:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => const RecommendationWidgetsScreen());

      case Routes.autoPilotRecommendations:
        final recommendationArgs = settings.arguments as String?;
        return MaterialPageRoute(
          settings: settings,
          builder: (_) =>
              RecommendationAutoPilotScreen(fromScreen: recommendationArgs),
        );

      case Routes.sentRecommendations:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => const RecommendationsSentScreen());

      case Routes.createOfferWizard:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const CreateOfferWizardPage());

      case Routes.createOfferWizardDescriptionEdit:
        final data = settings.arguments as Map<String, dynamic>;
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => CreateOfferWizardStepThreeDescriptionEdit(
                  industryIndex: data['industryIndex'],
                  subIndustryIndex: data['subIndustryIndex'],
                  industryMapLength: data['industryMapLength'],
                  subIndustryMapLength: data['subIndustryMapLength'],
                  descriptionList: data['descriptionList'],
                  subIndustryMap: data['subIndustryMap'],
                ));

      case Routes.uploadOffer:
        final job = settings.arguments as PartnerJobModel?;
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => UploadOfferPage(
                  job: job,
                ));

      case Routes.updateUploadedOffer:
        final job = settings.arguments as PartnerJobModel?;
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => UpdateUploadedOfferPage(
                  job: job,
                ));

      case Routes.marketPlaceScreen:
        final isFromAppLink = (settings.arguments as bool?) ?? false;
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => MarketPlaceScreen(
                  isFromAppLink: isFromAppLink,
                ));

      case Routes.udbudScreen:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const UdbudScreen());

      case Routes.m2mLaborScreen:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const M2mLaborScreen());

      case Routes.m2mLaborFormScreen:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const M2mLaborFormScreen());

      case Routes.m2mTaskScreen:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const M2mTaskScreen());

      case Routes.m2mTaskFormScreen:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const M2mTaskFormScreen());

      case Routes.myProjects:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const MyProjectsScreen());

      case Routes.createProject:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => const CreateProjectsScreen(),
        );

      case Routes.holidays:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const HolidayScreen());

      case Routes.yourMasterInformation:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const YourMasterInformation());

      case Routes.yourProfilePage:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const YourProfilePageScreen());

      case Routes.jobTypes:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const JobTypesSetting());

      case Routes.membershipBenefits:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => const MembershipBenefitsScreenV2());

      case Routes.membershipBenefitsSms:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => const MembershipBenefitsSmsScreen());

      case Routes.phoneNumber:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const PhoneNumbersScreen());

      case Routes.changePasswordDomainEmail:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => const ChangePasswordDomainEmailScreen());

      case Routes.integration:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const IntegrationScreen());
      case Routes.education:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const EducationScreen());
      case Routes.aboutus:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const TermsAndPolicies());
      case Routes.approvedProjects:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const ApprovedProjectsScreen());
      case Routes.projectDashboard:
        String? pageRedirect;

        final args = settings.arguments! as Map<String, dynamic>;
        if (args.containsKey('pageRedirect')) {
          pageRedirect = args['pageRedirect'];
        }

        return PageRouteBuilder(
          settings: settings,
          pageBuilder: (BuildContext context, Animation<double> animation,
                  Animation<double> secondaryAnimation) =>
              ProjectDashboard(
            projectId: args['projectId'],
            pageRedirect: pageRedirect,
          ),
        );
      case Routes.projectViewOffer:
        return PageRouteBuilder(
          settings: settings,
          pageBuilder: (BuildContext context, Animation<double> animation,
                  Animation<double> secondaryAnimation) =>
              const ProjectOfferDetail(),
        );

      case Routes.contractPreview:
        final Map<String, dynamic>? args =
            settings.arguments as Map<String, dynamic>?;

        String? isFrom;

        if (args != null) {
          if (args.containsKey('isFrom')) {
            isFrom = args['isFrom'];
          }
        }
        return PageRouteBuilder(
          settings: settings,
          pageBuilder: (BuildContext context, Animation<double> animation,
                  Animation<double> secondaryAnimation) =>
              ContractPreview(
            isFrom: isFrom,
          ),
        );
      case Routes.projectWizard:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const CreateProjectWizard());
      case Routes.projectDone:
        final Map<String, dynamic>? args =
            settings.arguments as Map<String, dynamic>?;
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => CreateProjectDone(
                  projectUrl: args!['projectUrl'],
                  projectId: args['projectId'],
                ));
      case Routes.projectEdit:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const EditProjectsScreen());
      case Routes.payproffAccount:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const PayproffAccountScreen());
      case Routes.payproffTerms:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const PayproffTermsScreen());

      case Routes.createProjectMessage:
        Map<String, dynamic>? args =
            settings.arguments as Map<String, dynamic>?;

        return PageRouteBuilder(
          settings: settings,
          pageBuilder: (BuildContext context, Animation<double> animation,
                  Animation<double> secondaryAnimation) =>
              args == null
                  ? const CreateProjectMessage()
                  : CreateProjectMessage(
                      threadId:
                          args.containsKey('threadId') ? args['threadId'] : 0,
                      partnerId:
                          args.containsKey('partnerId') ? args['partnerId'] : 0,
                      subject:
                          args.containsKey('subject') ? args['subject'] : '',
                      message:
                          args.containsKey('message') ? args['message'] : '',
                      customerId: args.containsKey('customerId')
                          ? args['customerId']
                          : 0,
                    ),
        );

      // NEWLY ADDED
      case Routes.tenderFolder:
        final job = settings.arguments as PartnerJobModel?;
        return MaterialPageRoute(
            settings: settings, builder: (_) => TenderFolder(job: job));

      case Routes.viewTenderFolder:
        final data = settings.arguments as Map<String, dynamic>?;
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => ViewFolder(
            files: data?['files'],
            projectParentId: data?['projectParentId'],
            parentFile: data?['parentFile'],
            jobProjectName: data?['jobProjectName'],
          ),
        );

      case Routes.jobDescriptions:
        final job = settings.arguments as PartnerJobModel?;
        return MaterialPageRoute(
            settings: settings, builder: (_) => JobDescriptions(job: job));

      case Routes.createOffer:
        final data = settings.arguments as Map<String, dynamic>;
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => CreateOfferPageWrapper(job: data['job']));
      // CreateOfferPage(job: data['job'], isUpdate: data['isUpdate']));

      case Routes.createOfferEditDescription:
        final map = settings.arguments as Map;
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => CreateOfferWizardEditDescriptionsScreen(
                  title: map['title'],
                  descriptions: map['descriptions'],
                  enterpriseId: map['enterpriseId'],
                  jobIndustryId: map['jobIndustryId'],
                ));

      case Routes.createOfferAiCalculation:
        final map = settings.arguments as Map;

        return MaterialPageRoute(
            settings: settings,
            builder: (_) => CreateOfferWizardAiCalculationScreen(
                  title: map['title'],
                  enterpriseId: map['enterpriseId'],
                  jobIndustryId: map['jobIndustryId'],
                  descriptions: map['descriptions'],
                ));

      case Routes.viewJobItem:
        final data = settings.arguments as Map<String, dynamic>?;
        return PageRouteBuilder(
          settings: settings,
          pageBuilder: (BuildContext context, Animation<double> animation,
                  Animation<double> secondaryAnimation) =>
              ViewJobItem(
            jobItem: data!['jobItem'],
            fromMarketPlace: data['fromMarketPlace'],
          ),
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            const begin = Offset(1.0, 0.0);
            const end = Offset.zero;
            const curve = Curves.easeInOut;

            var tween =
                Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

            var offsetAnimation = animation.drive(tween);

            return SlideTransition(position: offsetAnimation, child: child);
          },
        );

      case Routes.contractWizard:
        final isContractUpdate = settings.arguments as bool?;
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => ContractWizardV2(
                  isContractUpdate: (isContractUpdate ?? false),
                ));
      case Routes.editContract:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const EditContractWizardPage());
      case Routes.projectDeficiency:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const DeficiencyPage());

      case Routes.jobInvitations:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const JobInvitationsPage());

      case Routes.inAppWebView:
        final url = settings.arguments as Uri?;
        return PageRouteBuilder(
          settings: settings,
          pageBuilder: (BuildContext context, Animation<double> animation,
                  Animation<double> secondaryAnimation) =>
              AppWebView(url: url),
        );

      case Routes.messageView:
        final message = settings.arguments as MessageV2Items;
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => ViewMessageDetail(
            message: message,
            // item: data['item'],
            // threadId: data['threadId'],
            // messageId: data['messageId'],
          ),
        );

      case Routes.messageScreenV2:
        final projectId = settings.arguments as int?;
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => MessageScreenV2(
            projectId: projectId,
          ),
        );

      case Routes.htmlEditor:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => const HtmlEditorScreen(),
        );

      case Routes.payproffIbanScreen:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => const PayproffIbanScreen(),
        );

      case Routes.payproffVerifiedScreen:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => const PayproffVerifiedScreen(),
        );

      case Routes.digitalPackage:
        return PageRouteBuilder(
          settings: settings,
          pageBuilder: (BuildContext context, Animation<double> animation,
                  Animation<double> secondaryAnimation) =>
              const DigitalPackageScreen(),
        );

      case Routes.photoDocumentation:
        final projectId = settings.arguments as int;
        return PageRouteBuilder(
          settings: settings,
          pageBuilder: (BuildContext context, Animation<double> animation,
                  Animation<double> secondaryAnimation) =>
              PhotoDocumentationScreen(projectId: projectId),
        );

      case Routes.viewDocumentation:
        return PageRouteBuilder(
          settings: settings,
          pageBuilder: (BuildContext context, Animation<double> animation,
                  Animation<double> secondaryAnimation) =>
              const ViewDocumentation(),
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            const begin = 0.0;
            const end = 1.0;
            const curve = Curves.easeInOut;

            var tween =
                Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

            var offsetAnimation = animation.drive(tween);

            return FadeTransition(opacity: offsetAnimation, child: child);
          },
        );

      case Routes.addDocumentation:
        final image = settings.arguments as File;
        return PageRouteBuilder(
          settings: settings,
          pageBuilder: (BuildContext context, Animation<double> animation,
                  Animation<double> secondaryAnimation) =>
              AddDocumentation(image: image),
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            const begin = Offset(1.0, 0.0);
            const end = Offset.zero;
            const curve = Curves.easeInOut;

            var tween =
                Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

            var offsetAnimation = animation.drive(tween);

            return SlideTransition(position: offsetAnimation, child: child);
          },
        );

      case Routes.priceCalculator:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const PriceCalculatorPage());

      case Routes.contructionWallet:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => const CreateOfferContractPage(),
        );

      case Routes.contructionWalletContractPreview:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => const CreateOfferContractPreview());

      case Routes.contructionWalletEditContract:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => const CreateOfferEditContract());

      case Routes.yourEmail:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const YourEmailPage());

      case Routes.notification:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const NotificationPage());

      case Routes.cancelContract:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const CancelContractPage());

      case Routes.projectCalendar:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const CalendarScreen());

      case Routes.editOfferLanding:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const LandingScreen());

      case Routes.editOffer:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const EditOffer());

      case Routes.signatureOffer:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const SignatureOffer());

      case Routes.projectWallet:
        final isFromMain = settings.arguments as bool?;

        return MaterialPageRoute(
            settings: settings,
            builder: (_) => WalletScreen(
                  isFromMain: (isFromMain ?? false),
                ));

      case Routes.projectWalletTransaction:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => const WalletTransactionScreen());

      case Routes.projectWalletTransactionDetail:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => WalletTransactionDetailScreen(
                  title: settings.arguments as String,
                ));

      case Routes.projectWalletTransactionRequest:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => WalletTransactionRequestScreen(
                  title: settings.arguments as String,
                ));

      case Routes.projectWalletTransactionDecline:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => const WalletTransactionDeclineScreen());

      case Routes.projectWalletExtraWork:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => const WalletExtraWorkScreen(),
        );

      case Routes.projectWalletExtraWorkDetail:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => ExtraWorkDetailScreen(
                  extraWork: settings.arguments as ExtraWorkItems,
                ));

      case Routes.projectWalletExtraWorkRequest:
        ExtraWorkItems? extraWork;

        if (settings.arguments != null &&
            settings.arguments is ExtraWorkItems) {
          extraWork = settings.arguments as ExtraWorkItems;
        }
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => ExtraWorkRequestScreen(
            extraWork: extraWork,
          ),
        );

      case Routes.projectInviteManufacturer:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => const InvitationManufacturerScreen(),
        );

      case Routes.projectInviteSubContractor:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => const InvitationSubcontractorScreen(),
        );

      case Routes.subcontractorAcceptInvite:
        final map = settings.arguments as Map?;

        return MaterialPageRoute(
          settings: settings,
          builder: (_) => SubcontractorAcceptInviteScreen(
            subContractId: map?['subContractId'],
            projectId: map?['projectId'],
          ),
        );

      case Routes.subcontractorInviteContract:
        final map = settings.arguments as Map?;

        return MaterialPageRoute(
          settings: settings,
          builder: (_) => SubcontractorInviteContractScreen(
            subContractId: map?['subContractId'],
            projectId: map?['projectId'],
            from: map?['from'],
          ),
        );
      case Routes.inviteCustomer:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const InviteCustomerScreen());

      case Routes.inviteCustomerEditDescription:
        List<dynamic> descriptions =
            (settings.arguments as Map)['descriptions'];
        List<dynamic> originalDescriptions =
            (settings.arguments as Map)['originalDescriptions'];
        String title = (settings.arguments as Map)['title'];
        int industryIndex = (settings.arguments as Map)['industryIndex'];
        int subIndustryIndex = (settings.arguments as Map)['subIndustryIndex'];

        return MaterialPageRoute(
            settings: settings,
            builder: (_) => CreateOfferEditDescriptionsScreen(
                industryIndex: industryIndex,
                subIndustryIndex: subIndustryIndex,
                descriptions: descriptions,
                originalDescriptions: originalDescriptions,
                title: title));

      case Routes.inviteCustomerAiCalculation:
        int industryIndex = (settings.arguments as Map)['industryIndex'];
        int subIndustryIndex = (settings.arguments as Map)['subIndustryIndex'];
        List<dynamic> descriptions =
            (settings.arguments as Map)['descriptions'];
        String title = (settings.arguments as Map)['title'];
        String productTypeId = (settings.arguments as Map)['productTypeId'];
        String subIndustryId = (settings.arguments as Map)['subIndustryId'];
        GlobalKey<FormBuilderState> formKey =
            (settings.arguments as Map)['formKey'];

        return MaterialPageRoute(
            builder: (_) => CreateOfferAiCalculationScreen(
                  title: title,
                  productTypeId: productTypeId,
                  subIndustryId: subIndustryId,
                  descriptions: descriptions,
                  industryIndex: industryIndex,
                  subIndustryIndex: subIndustryIndex,
                  formKey: formKey,
                ));

      default:
        return MaterialPageRoute(
            settings: settings, builder: (_) => const NewsfeedScreen());
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(title: const Text('Error')),
        body: const Center(child: Text('ERROR')),
      );
    });
  }

  /// [routingMap] is used ONLY for analytics to get the screen name from a given route
  static const routingMap = {
    Routes.intro: 'IntroScreen',
    Routes.leadForm: 'LeadFormScreen',
    Routes.userRegistration: 'RegistrationScreen',
    Routes.login: 'LoginScreen',
    Routes.home: 'HomeScreen',
    Routes.forgotPassword: 'ForgotPasswordScreen',
    Routes.addPage: 'AddPage',
    Routes.freeWebsiteSettings: 'FreeWebsiteSettings',
    Routes.permissions: 'FreeWebsitePermissions',
    Routes.pageOrder: 'FreeWebsitePageOrder',
    Routes.chooseJobTypes: 'ChooseJobTypesScreen',
    Routes.freeWebsiteThemes: 'FreeWebsiteThemes',
    Routes.makeAnOffer: 'MakeAnOfferScreen',
    Routes.loadingScreen: 'AppSplashScreen',
    Routes.messageScreen: 'MessageScreen',
    Routes.settings: 'SettingsScreen',
    Routes.yourClient: 'YourCustomerScreen',
    Routes.contactUs: 'FAQScreen',
    Routes.changePassword: 'ChangePasswordScreen',
    Routes.newsFeed: 'NewsfeedScreen',
    Routes.getRecommendations: 'GetRecommendationsScreen',
    Routes.createRecommendation: 'CreateRecommendationsScreen',
    Routes.widgetRecommendations: 'RecommendationWidgetsScreen',
    Routes.autoPilotRecommendations: 'RecommendationAutoPilotScreen',
    Routes.readAndAnswerRecommendations: 'RecommendationsReadAnswerScreen',
    Routes.tilbud: 'OfferScreen',
    Routes.allOffers: 'AllOffersScreen',
    Routes.offerHistory: 'OfferHistoryScreen',
    Routes.marketPlaceScreen: 'MartketPlaceScreen',
    Routes.udbudScreen: 'UdbudScreen',
    Routes.m2mLaborScreen: 'M2mLaborScreen',
    Routes.m2mLaborFormScreen: 'M2mLaborFormScreen',
    Routes.m2mTaskScreen: 'M2mTaskScreen',
    Routes.m2mTaskFormScreen: 'M2mTaskFormScreen',
    Routes.myProjects: 'MyProjectsScreen',
    Routes.createProject: 'CreateProjectsScreen',
    Routes.holidays: 'HolidayScreen',
    Routes.creditCards: 'CreditCardScreen',
    Routes.supplierService: 'SupplierService',
    Routes.invoice: 'InvoiceScreen',
    Routes.subscription: 'SubscriptionScreen',
    Routes.yourMasterInformation: 'YourMasterInformationScreen',
    Routes.yourProfilePage: 'YourProfilePageScreen',
    Routes.freeWebsite: 'FreeWebsiteScreen',
    Routes.jobTypes: 'JobTypesScreen',
    Routes.partners: 'PartnersScreen',
    Routes.emailLogin: 'EmailLoginScreen',
    Routes.allMailbox: 'AllMailboxScreen',
    Routes.emailMailbox: 'EmailBoxScreen',
    Routes.emailRead: 'ReadMailScreen',
    Routes.emailWrite: 'IntroScreen',
    Routes.admin: 'AdminScreen',
    Routes.badges: 'BadgesScreen',
    Routes.membershipBenefits: 'MembershipBenefitsScreen',
    Routes.phoneNumber: 'PhoneNumbersScreen',
    Routes.homeDomainEmail: 'HomeDomainEmailScreen',
    Routes.changePasswordDomainEmail: 'ChangePasswordDomainEmailScreen',
    Routes.webShopHome: 'WebshopHomeScreen',
    Routes.webShopProducts: 'WebshopProductListScreen',
    Routes.webshopProductDetail: 'WebshopProductDetailScreen',
    Routes.webshopCart: 'WebshopCartScreen',
    Routes.webshopFacebook: 'WebshopAdsMainScreen',
    Routes.webshopFacebookCreatePost: 'WebshopFacebookCreatePostAd',
    Routes.webshopOrderHistory: 'WebshopOrderHistoryScreen',
    Routes.webshopOrderDetails: 'WebshopOrderDetailScreen',
    Routes.webshopWishlist: 'WebshopWishListScreen',
    Routes.webSocketScreen: 'WebSocketScreen',
  };
}
