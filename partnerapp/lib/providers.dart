import 'package:Haandvaerker.dk/services/analytics/analytics_service.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/badge/badge_service.dart';
import 'package:Haandvaerker.dk/services/booking_wizard/booking_wizard_service.dart';
import 'package:Haandvaerker.dk/services/calendar/calendar_service.dart';
import 'package:Haandvaerker.dk/services/client/client_service.dart';
import 'package:Haandvaerker.dk/services/contract/contract_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/deficiency/deficiency_service.dart';
import 'package:Haandvaerker.dk/services/education/education_service.dart';
import 'package:Haandvaerker.dk/services/facebook/facebook_service.dart';
import 'package:Haandvaerker.dk/services/file/file_service.dart';
import 'package:Haandvaerker.dk/services/helper/helper_service.dart';
import 'package:Haandvaerker.dk/services/integration/integration_service.dart';
import 'package:Haandvaerker.dk/services/invitation/invitation_service.dart';
import 'package:Haandvaerker.dk/services/location/location_service.dart';
import 'package:Haandvaerker.dk/services/main/main_service.dart';
import 'package:Haandvaerker.dk/services/manufacturer_sub_contractor/manufacturer_sub_contractor_service.dart';
import 'package:Haandvaerker.dk/services/marketplace/job_invitation/job_invitation_service.dart';
import 'package:Haandvaerker.dk/services/marketplace/marketplace/marketplace_service.dart';
import 'package:Haandvaerker.dk/services/membership/membership_service.dart';
import 'package:Haandvaerker.dk/services/messages/message_service.dart';
import 'package:Haandvaerker.dk/services/mester-mester/mester_mester_service.dart';
import 'package:Haandvaerker.dk/services/mitid/mitid_service.dart';
import 'package:Haandvaerker.dk/services/offer/offer_service.dart';
import 'package:Haandvaerker.dk/services/packages/packages_service.dart';
import 'package:Haandvaerker.dk/services/payproff/payproff_service.dart';
import 'package:Haandvaerker.dk/services/photo_doc/photo_doc_service.dart';
import 'package:Haandvaerker.dk/services/projects/extra_work/extra_work_service.dart';
import 'package:Haandvaerker.dk/services/projects/projects_service.dart';
import 'package:Haandvaerker.dk/services/push_notification/push_notification_service.dart';
import 'package:Haandvaerker.dk/services/question/questions_service.dart';
import 'package:Haandvaerker.dk/services/recommendation/recommendation_service.dart';
import 'package:Haandvaerker.dk/services/request_payment/request_payment_service.dart';
import 'package:Haandvaerker.dk/services/settings/settings_service.dart';
import 'package:Haandvaerker.dk/services/webshop/webshop_service.dart';
import 'package:Haandvaerker.dk/services/wizard/wizard_service.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_bottom_navigation_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/app/connectivity_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/app_links/app_links_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/badges/badges_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/calendar/calendar_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/contract/contract_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/deficiency/deficiency_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/edit_offer/edit_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/education/education_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/facebook/facebook_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/integration/integration_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/invite_customer/invite_customer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/location/location_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/job_invitations_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/labor_task_toggle_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/market_place_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/marketplace/udbud_jobs_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/membership/membership_benefits_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/messages/message_v2_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/mester-mester/mester_partner_profile_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/newsfeed/newsfeed_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/fast_track_offer/fast_track_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/packages/packages_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/change_password_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/kanikke_settings_response_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/payproff/payproff_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/price_calculation/price_calculation_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/create_projects_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/invitation_subcontractor_manufacturer/invitation_subcontractor_manufacturer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/photo_documentation_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/project_messages_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/wallet/wallet_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/recommendation_autopilot_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/recommendation_read_answer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/widgets_recommendations_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/request_payment/payment_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/faq_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/holidays_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/job_types_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/phone_numbers_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/result_images_view_model.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/result_videos_view_model.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/subscription_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/your_master_information_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/your_profile_page_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/shared_data/shared_data_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/question_answer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/see_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/version_checker/version_checker_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

// ignore: always_use_package_imports

List<SingleChildWidget> providers(
    {required Dio dio,
    required FlutterSecureStorage storage,
    required SharedPreferences sharedPreferences}) {
  return [
    Provider.value(value: FirebaseAnalytics.instance),
    Provider.value(value: dio),
    Provider(
      create: (context) => ApiService(
        client: http.Client(),
        dio: dio,
      ),
    ),
    Provider(
      create: (context) => SecuredStorageService(
          storage: storage, sharedPreferences: sharedPreferences),
    ),
    Provider(
      create: (context) => FirebaseAnalyticsObserver(
        analytics: context.read<FirebaseAnalytics>(),
      ),
    ),
    Provider(
      create: (context) => AnalyticsService(
          apiService: context.read<ApiService>(),
          analytics: context.read<FirebaseAnalytics>(),
          observer: context.read<FirebaseAnalyticsObserver>(),
          storage: context.read<SecuredStorageService>()),
    ),
    Provider(
      create: (context) => MainService(
          apiService: context.read<ApiService>(),
          storage: context.read<SecuredStorageService>()),
    ),
    Provider(
      create: (context) => HelperService(
          apiService: context.read<ApiService>(),
          storage: context.read<SecuredStorageService>()),
    ),
    Provider(
      create: (context) => PushNotificationService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => PackagesService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => IntegrationService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => BadgeService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => MarketPlaceService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => LocationService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => ClientService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => ProjectsService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => EducationService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => RecommendationService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => SettingsService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => WebshopService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => MesterMesterService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => FacebookService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => MembershipService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => InvitationService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => RequestPaymentService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => ContractService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => FilesService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => QuestionsService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => WizardService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => OfferService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => DeficiencyService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => JobInvitationService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => ExtraWorkService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => MessageService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => PayproffService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => BookingWizardService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => PhotoDocService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => MitidService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => CalendarService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
    Provider(
      create: (context) => ManufacturerSubContractorService(
        apiService: context.read<ApiService>(),
        storage: context.read<SecuredStorageService>(),
      ),
    ),
  ];
}

List<SingleChildWidget> changeNotifiers() {
  return [
    ChangeNotifierProvider(
      create: (context) => UserViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => AppLinksViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => PackagesViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => ChangePasswordViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => ClientsViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => NewsFeedViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => MesterPartnerProfileViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => WidgetsViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => RecommendationAutoPilotViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => RecommendationReadAnswerViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => MyProjectsViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => CreateProjectsViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => HolidaysViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => SubscriptionViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => YourProfilePageViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => ResultImagesViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => JobTypesViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => ResultVideosViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => LaborTaskToggleViewModel(),
    ),
    ChangeNotifierProvider(
      create: (context) => FaqViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => AppDrawerViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => OfferViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => PhoneNumbersViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => BadgesViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => ConnectivityViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => AppBottomNavigationViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => FacebookViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => MembershipBenefitsViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => IntegrationViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => EducationViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => UdbudJobsViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => PaymentViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => PayproffViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => ProjectMessagesViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => TenderFolderViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => QuestionAnswerViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => CreateOfferViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => MarketPlaceV2ViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => ContractViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => SeeOfferViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => DeficiencyViewmodel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => JobInvitationsViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => MessageV2ViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => LocationViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => YourMasterInfoViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => PhotoDocumentationViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => PriceCalculationViewmodel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => FastTrackOfferViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => KanIkkeSettingsResponseViewModel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => CalendarViewmodel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => VersionCheckerViewmodel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => EditOfferViewmodel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => SharedDataViewmodel(context: context),
    ),
    ChangeNotifierProvider(
      create: (context) => WalletViewmodel(context: context),
    ),
    ChangeNotifierProvider(
      create: (contex) =>
          InvitationSubcontractorManufacturerViewmodel(context: contex),
    ),
    ChangeNotifierProvider(
      create: (context) => InviteCustomerViewmodel(context: context),
    ),
  ];
}

List<SingleChildWidget> listOfProviders(
    {required Dio dio,
    required FlutterSecureStorage storage,
    required SharedPreferences sharedPreferences}) {
  return [
    ...providers(
        dio: dio, storage: storage, sharedPreferences: sharedPreferences),
    ...changeNotifiers()
  ];
}
