import 'package:json_annotation/json_annotation.dart';

part 'partner_jobs_model.g.dart';

@JsonSerializable()
class PaginatedPartnerJobsModel {
  @JsonKey(name: 'current_page')
  int? currentPage;
  @JsonKey(name: 'data')
  List<PartnerJobModel>? data;
  @JsonKey(name: 'last_page')
  int? lastPage;
  @JsonKey(name: 'per_page')
  int? perPage;
  @JsonKey(name: 'total')
  int? total;
  @JsonKey(name: 'from')
  final int? from;

  PaginatedPartnerJobsModel({
    this.currentPage,
    this.data,
    this.lastPage,
    this.perPage,
    this.total,
    this.from,
  });

  factory PaginatedPartnerJobsModel.fromJson(Map<String, dynamic> json) =>
      _$PaginatedPartnerJobsModelFromJson(json);

  Map<String, dynamic> toJson() => _$PaginatedPartnerJobsModelToJson(this);

  static List<PaginatedPartnerJobsModel> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => PaginatedPartnerJobsModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class PartnerJobModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'JOBS_LIST')
  dynamic jobsList;
  @JsonKey(name: 'OFFER_ID')
  dynamic offerId;
  @JsonKey(name: 'PARTNER_ID')
  int? partnerId;
  @JsonKey(name: 'PARTNER_NAME')
  String? partnerName;
  @JsonKey(name: 'PARENT_PROJECT')
  ParentProject? parentProject;
  @JsonKey(name: 'CALL_DATE')
  dynamic callDate;
  @JsonKey(name: 'COMPANY_FAKE_PHONE')
  dynamic companyFakePhone;
  @JsonKey(name: 'CONTACT_FAKE_PHONE')
  dynamic contactFakePhone;
  @JsonKey(name: 'CUSTOMER_ID')
  int? customerId;
  @JsonKey(name: 'CUSTOMER_NAME')
  String? customerName;
  @JsonKey(name: 'CUSTOMER_MOBILE')
  String? customerMobile;
  @JsonKey(name: 'CUSTOMER_EMAIL')
  String? customerEmail;
  @JsonKey(name: 'CREATED_FROM')
  String? createdFrom;
  @JsonKey(name: 'PROJECT_ID')
  dynamic projectId;
  @JsonKey(name: 'PROJECT_NAME')
  String? projectName;
  @JsonKey(name: 'PROJECT_STATUS')
  String? projectStatus;
  @JsonKey(name: 'PROJECT_PARENT_ID')
  dynamic projectParentId;
  @JsonKey(name: 'PROJECT_CITY')
  final String? projectCity;
  @JsonKey(name: 'PROJECT_ZIP')
  final String? projectZip;
  @JsonKey(name: 'PROJECT_ADDRESS')
  String? projectAddress;
  @JsonKey(name: 'PROJECT_DESCRIPTION')
  String? projectDescription;
  @JsonKey(name: 'PROJECT_TYPE')
  String? projectType;
  @JsonKey(name: 'CONTRACT_TYPE')
  String? contractType;
  @JsonKey(name: 'PROJECT_OFFER_TYPE')
  String? projectOfferType;
  @JsonKey(name: 'PROJECT_TENDER_FOLDER_ID')
  int? projectTenderFolderId;
  @JsonKey(name: 'OFFER_STATUS')
  dynamic offerStatus;
  @JsonKey(name: 'SUBMITTED_JOB_INDUSTRY_IDS')
  List<dynamic>? submittedJobIndustryIds;
  @JsonKey(name: 'STATUS')
  List<JobStatus>? status;
  @JsonKey(name: 'STATUS_ID')
  int? statusId;
  @JsonKey(name: 'STATUS_NAME_DK')
  String? statusNameDK;
  @JsonKey(name: 'STATUS_NAME_EN')
  String? statusNameEN;
  @JsonKey(name: 'TENDER_FILES')
  List<TenderFiles>? tenderFiles;
  @JsonKey(name: 'SUB_PROJECTS')
  List<SubProjects>? subProjects;
  @JsonKey(name: 'NOTES')
  List<JobNotes>? notes;
  @JsonKey(name: 'CANCEL_STATUS')
  List<CancelStatus>? cancelStatus;
  @JsonKey(name: 'OFFER_FILE')
  String? offerFile;
  @JsonKey(name: 'OFFER_TYPE')
  String? offerType;
  @JsonKey(name: 'OFFER_SEEN')
  bool? offerSeen;
  @JsonKey(name: 'CONTACT_REAL_PHONE')
  dynamic contactRealPhone;
  @JsonKey(name: 'CUSTOMER_FAKE_EMAIL')
  String? customerFakeEmail;
  @JsonKey(name: 'PROJECT_DEADLINE_OFFER')
  String? projectDeadlineOffer;
  @JsonKey(name: 'PROJECT_DEADLINE_OFFER_COLOR')
  String? projectDeadlineOfferColor;
  @JsonKey(name: 'PROJECT_DEADLINE_OFFER_COLOR_CODE')
  String? projectDeadlineOfferColorCode;
  @JsonKey(name: 'IS_DELETED')
  int? isDeleted;
  @JsonKey(name: 'ALERT_STATUS')
  String? alertStatus;
  @JsonKey(name: 'ALERT_STAGE')
  String? alertStage;
  @JsonKey(name: 'PROJECT_ADDRESS_ID')
  String? projectAddressId;
  @JsonKey(name: 'PROJECT_ADGANGS_ADDRESS_ID')
  String? projectAdgangsAddressId;
  @JsonKey(name: 'PROJECT_PHOTO_DOCUMENTATION_FOLDER_ID')
  int? projectPhotoDocumentationFolderId;
  @JsonKey(name: 'OFFER_EXPIRE_DATE')
  String? offerExpireDate;
  @JsonKey(name: 'OFFER_EXPIRE_DATE_COLOR')
  String? offerExpireDateColor;
  @JsonKey(name: 'OFFER_EXPIRE_DATE_COLOR_CODE')
  String? offerExpireDateColorCode;

  PartnerJobModel({
    this.id,
    this.jobsList,
    this.callDate,
    this.companyFakePhone,
    this.contactFakePhone,
    this.customerId,
    this.customerEmail,
    this.customerMobile,
    this.customerName,
    this.createdFrom,
    this.partnerId,
    this.partnerName,
    this.projectDescription,
    this.projectCity,
    this.projectZip,
    this.projectAddress,
    this.projectId,
    this.projectName,
    this.projectTenderFolderId,
    this.projectParentId,
    this.status,
    this.statusNameDK,
    this.statusNameEN,
    this.tenderFiles,
    this.subProjects,
    this.contractType,
    this.offerId,
    this.offerStatus,
    this.projectOfferType,
    this.projectStatus,
    this.projectType,
    this.submittedJobIndustryIds,
    this.notes,
    this.cancelStatus,
    this.offerFile,
    this.offerType,
    this.offerSeen,
    this.contactRealPhone,
    this.customerFakeEmail,
    this.projectDeadlineOffer,
    this.projectDeadlineOfferColor,
    this.projectDeadlineOfferColorCode,
    this.isDeleted,
    this.alertStatus,
    this.projectAddressId,
    this.projectAdgangsAddressId,
    this.alertStage,
    this.parentProject,
    this.statusId,
    this.projectPhotoDocumentationFolderId,
    this.offerExpireDate,
    this.offerExpireDateColorCode,
    this.offerExpireDateColor,
  });

  PartnerJobModel.defaults()
      : id = 0,
        jobsList = null,
        callDate = null,
        companyFakePhone = null,
        contactFakePhone = null,
        customerId = 0,
        customerEmail = '',
        customerMobile = '',
        customerName = '',
        createdFrom = '',
        partnerId = 0,
        partnerName = '',
        projectDescription = '',
        projectCity = '',
        projectZip = '',
        projectAddress = '',
        projectId = '',
        projectName = '',
        projectTenderFolderId = 0,
        projectParentId = '',
        status = [],
        statusNameDK = '',
        statusNameEN = '',
        tenderFiles = [],
        subProjects = [],
        contractType = '',
        offerId = '',
        offerStatus = '',
        projectOfferType = '',
        projectStatus = '',
        projectType = '',
        submittedJobIndustryIds = [],
        notes = [],
        cancelStatus = [],
        offerFile = '',
        offerType = '',
        contactRealPhone = '',
        customerFakeEmail = '',
        projectDeadlineOffer = '',
        projectDeadlineOfferColor = '',
        projectDeadlineOfferColorCode = '',
        isDeleted = 0,
        alertStatus = '';

  factory PartnerJobModel.fromJson(Map<String, dynamic> json) =>
      _$PartnerJobModelFromJson(json);

  Map<String, dynamic> toJson() => _$PartnerJobModelToJson(this);

  static List<PartnerJobModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => PartnerJobModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class JobStatus {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'TITLE_EN')
  String? titleEn;
  @JsonKey(name: 'TITLE_DK')
  String? titleDk;
  @JsonKey(name: 'IS_SELECTED')
  bool? isSelected;

  JobStatus(
      {this.id = 0,
      this.isSelected = false,
      this.titleDk = "",
      this.titleEn = ""});

  JobStatus.defaults()
      : id = 0,
        titleEn = '',
        titleDk = '',
        isSelected = false;

  factory JobStatus.fromJson(Map<String, dynamic> json) =>
      _$JobStatusFromJson(json);

  Map<String, dynamic> toJson() => _$JobStatusToJson(this);
  static List<JobStatus> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => JobStatus.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class TenderFiles {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'PROJECT_ID')
  int? projectId;
  @JsonKey(name: 'PARTNER_ID')
  int? partnerId;
  @JsonKey(name: 'URL')
  String? url;
  @JsonKey(name: 'TYPE')
  String? type;

  TenderFiles({this.id, this.projectId, this.partnerId, this.url, this.type});
  factory TenderFiles.fromJson(Map<String, dynamic> json) =>
      _$TenderFilesFromJson(json);

  Map<String, dynamic> toJson() => _$TenderFilesToJson(this);
  static List<TenderFiles> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => TenderFiles.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class SubProjects {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'ENTERPRISE_INDUSTRY')
  String? enterpriseIndustry;
  @JsonKey(name: 'JOB_INDUSTRY')
  int? jobIndustry;
  @JsonKey(name: 'IS_OFFER_ACCEPTED')
  bool? isOfferAccepted;
  @JsonKey(name: 'IS_PARTNER')
  bool? isPartner;

  SubProjects({
    this.id,
    this.enterpriseIndustry,
    this.jobIndustry,
    this.isOfferAccepted,
    this.isPartner,
  });

  factory SubProjects.fromJson(Map<String, dynamic> json) =>
      _$SubProjectsFromJson(json);

  Map<String, dynamic> toJson() => _$SubProjectsToJson(this);

  static List<SubProjects> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => SubProjects.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class ParentProject {
  @JsonKey(name: 'ID')
  dynamic id;
  @JsonKey(name: 'PROJECT_OFFER_TYPE')
  dynamic projectOfferType;
  @JsonKey(name: 'PROJECT_TENDER_FOLDER_ID')
  dynamic projectTenderFolderId;
  @JsonKey(name: 'JOBS_LIST')
  List<dynamic>? jobsList;

  ParentProject({
    this.id,
    this.projectOfferType,
    this.projectTenderFolderId,
    this.jobsList,
  });

  factory ParentProject.fromJson(Map<String, dynamic> json) =>
      _$ParentProjectFromJson(json);

  Map<String, dynamic> toJson() => _$ParentProjectToJson(this);
  static List<ParentProject> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => ParentProject.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class JobNotes {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NOTE')
  String? note;
  @JsonKey(name: 'FILES')
  List<JobNoteFiles>? files;

  JobNotes({
    this.id,
    this.note,
    this.files,
  });

  factory JobNotes.fromJson(Map<String, dynamic> json) =>
      _$JobNotesFromJson(json);

  Map<String, dynamic> toJson() => _$JobNotesToJson(this);
  static List<JobNotes> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => JobNotes.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class JobNoteFiles {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'FILE_ID')
  int? fileId;
  @JsonKey(name: 'FILE_URL')
  String? fileURL;

  JobNoteFiles({
    this.id,
    this.fileId,
    this.fileURL,
  });

  factory JobNoteFiles.fromJson(Map<String, dynamic> json) =>
      _$JobNoteFilesFromJson(json);

  Map<String, dynamic> toJson() => _$JobNoteFilesToJson(this);
  static List<JobNoteFiles> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => JobNoteFiles.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class CancelStatus {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'IS_SELECTED')
  bool? isSelected;
  @JsonKey(name: 'TITLE_DK')
  String? titleDK;
  @JsonKey(name: 'TITLE_EN')
  String? titleEN;

  CancelStatus({
    this.id,
    this.isSelected,
    this.titleDK,
    this.titleEN,
  });

  factory CancelStatus.fromJson(Map<String, dynamic> json) =>
      _$CancelStatusFromJson(json);

  Map<String, dynamic> toJson() => _$CancelStatusToJson(this);
  static List<CancelStatus> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => CancelStatus.fromJson(i as Map<String, dynamic>))
      .toList();
}
