import 'package:json_annotation/json_annotation.dart';

part 'old_jobs_model.g.dart';

@JsonSerializable()
class OldPartnerJobsModel {
  OldPartnerJobsModel({
    this.firstName,
    this.lastName,
    this.address,
    this.dateReserved,
    this.tilbudStatus,
    this.customerMobile,
    this.mailingCity,
    this.mailingZip,
    this.mobile,
    this.title,
    this.contactId,
    this.idAppointment,
  });

  @JsonKey(name: 'firstname')
  final String? firstName;
  @JsonKey(name: 'lastname')
  final String? lastName;
  @JsonKey(name: 'mailingstreet')
  final String? address;
  @JsonKey(name: 'datereserve')
  final String? dateReserved;
  @JsonKey(name: 'tilbudstatus')
  final int? tilbudStatus;
  @JsonKey(name: 'customer_mobile')
  final String? customerMobile;
  @JsonKey(name: 'mailingcity')
  final String? mailingCity;
  @JsonKey(name: 'mailingzip')
  final String? mailingZip;
  @JsonKey(name: 'mobile')
  final String? mobile;
  @JsonKey(name: 'TITLE')
  final String? title;
  @JsonKey(name: 'id_appointment')
  final String? idAppointment;
  @JsonKey(name: 'contact_id')
  final String? contactId;

  factory OldPartnerJobsModel.fromJson(Map<String, dynamic> json) =>
      _$OldPartnerJobsModelFromJson(json);

  Map<String, dynamic> toJson() => _$OldPartnerJobsModelToJson(this);
  static List<OldPartnerJobsModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => OldPartnerJobsModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
