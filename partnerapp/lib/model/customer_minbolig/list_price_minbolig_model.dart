import 'package:json_annotation/json_annotation.dart';

part 'list_price_minbolig_model.g.dart';

@JsonSerializable()
class MinboligListPrice {
  @JsonKey(name: 'industry_price')
  Map<dynamic, dynamic>? industryPrice;

  @JsonKey(name: 'min_price_industries')
  Map<dynamic, dynamic>? minPriceIndustries;

  @JsonKey(name: 'result')
  List<MinboligListPriceResult>? result;

  MinboligListPrice({
    this.industryPrice,
    this.minPriceIndustries,
    this.result,
  });

  factory MinboligListPrice.fromJson(Map<String, dynamic> json) =>
      _$MinboligListPriceFromJson(json);

  Map<String, dynamic> toJson() => _$MinboligListPriceToJson(this);

  static List<MinboligListPrice> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => MinboligListPrice.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class MinboligListPriceResult {
  @JsonKey(name: 'ARCHIVE')
  String? archive;
  @JsonKey(name: 'CALCULATION_ID')
  String? calculationId;
  @JsonKey(name: 'CONTACT_ID')
  String? contactId;
  @JsonKey(name: 'CREATED')
  String? created;
  @JsonKey(name: 'DAYS')
  dynamic days;
  @JsonKey(name: 'FILE_ID')
  String? fileId;
  @JsonKey(name: 'ID')
  String? id;
  @JsonKey(name: 'IP_ADDRESS')
  String? ipAddress;
  @JsonKey(name: 'TOTALVAT')
  String? totalVat;
  @JsonKey(name: 'TOTAL_VAT_FEE')
  String? totalVatFee;
  @JsonKey(name: 'SIGNATURE')
  String? signature;
  @JsonKey(name: 'NOTE')
  String? note;
  @JsonKey(name: 'SERIES')
  dynamic series;
  @JsonKey(name: 'SERIES_ORIGINAL')
  List<String>? seriesOriginal;
  @JsonKey(name: 'INDUSTRY')
  String? industry;
  @JsonKey(name: 'JOB_INDUSTRY')
  String? jobIndustry;
  @JsonKey(name: 'OFFER_ID')
  String? offerId;
  @JsonKey(name: 'PARENT_OFFER_ID')
  String? parentOfferId;
  @JsonKey(name: 'PARTNER_ID')
  String? partnerId;
  @JsonKey(name: 'PROJECT_ID')
  String? projectId;
  @JsonKey(name: 'TOTAL')
  String? total;
  @JsonKey(name: 'WEEKS')
  String? weeks;
  @JsonKey(name: 'START_DATE')
  String? startDate;
  @JsonKey(name: 'IS_DELETED')
  String? isDeleted;

  MinboligListPriceResult({
    this.note,
    this.series,
    this.seriesOriginal,
    this.industry,
    this.jobIndustry,
    this.offerId,
    this.parentOfferId,
    this.partnerId,
    this.projectId,
    this.total,
    this.weeks,
    this.startDate,
    this.archive,
    this.calculationId,
    this.contactId,
    this.created,
    this.days,
    this.fileId,
    this.id,
    this.ipAddress,
  });

  MinboligListPriceResult.defaults()
      : note = '',
        series = null,
        seriesOriginal = [],
        industry = '',
        jobIndustry = '',
        offerId = '',
        parentOfferId = '',
        partnerId = '',
        projectId = '',
        total = '',
        weeks = '',
        startDate = '',
        archive = '',
        calculationId = '',
        contactId = '',
        created = '',
        days = null,
        fileId = '',
        id = '',
        ipAddress = '';

  factory MinboligListPriceResult.fromJson(Map<String, dynamic> json) =>
      _$MinboligListPriceResultFromJson(json);

  Map<String, dynamic> toJson() => _$MinboligListPriceResultToJson(this);

  static List<MinboligListPriceResult> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => MinboligListPriceResult.fromJson(i as Map<String, dynamic>))
      .toList();
}
