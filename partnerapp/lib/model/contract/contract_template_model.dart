import 'package:json_annotation/json_annotation.dart';

part 'contract_template_model.g.dart';

@JsonSerializable()
class ContractTemplateModel {
  @JsonKey(name: 'ID')
  String? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'TEMPLATE_ID')
  String? templateId;
  @JsonKey(name: 'TEMPLATE')
  dynamic template;
  @JsonKey(name: 'STEPS')
  dynamic steps;
  @JsonKey(name: 'MAPPING')
  dynamic mapping;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'FIELDS')
  List<ContractTemplateField?>? fields;

  ContractTemplateModel(
      {this.id,
      this.name,
      this.templateId,
      this.template,
      this.steps,
      this.mapping,
      this.description,
      this.fields});

  factory ContractTemplateModel.fromJson(Map<String, dynamic> json) =>
      _$ContractTemplateModelFromJson(json);

  Map<String, dynamic> toJson() => _$ContractTemplateModelToJson(this);

  static List<ContractTemplateModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => ContractTemplateModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class ContractTemplateField {
  @JsonKey(name: 'id')
  String? id;
  @JsonKey(name: 'label')
  String? label;
  @JsonKey(name: 'condition')
  String? condition;

  ContractTemplateField({this.id, this.condition, this.label});

  factory ContractTemplateField.fromJson(Map<String, dynamic> json) =>
      _$ContractTemplateFieldFromJson(json);

  Map<String, dynamic> toJson() => _$ContractTemplateFieldToJson(this);
}

@JsonSerializable()
class TemplateSteps {
  @JsonKey(name: 'label')
  String? label;
  @JsonKey(name: 'key')
  String? key;
  @JsonKey(name: 'Info')
  String? info;
  @JsonKey(name: 'fields')
  List<StepField?>? fields;

  TemplateSteps({this.label, this.key, this.info, this.fields});

  factory TemplateSteps.fromJson(Map<String, dynamic> json) =>
      _$TemplateStepsFromJson(json);

  Map<String, dynamic> toJson() => _$TemplateStepsToJson(this);
}

@JsonSerializable()
class StepField {
  @JsonKey(name: 'label')
  String? label;
  @JsonKey(name: 'key')
  int? key;
  @JsonKey(name: 'help')
  String? help;
  @JsonKey(name: 'field')
  int? field;
  @JsonKey(name: 'number')
  int? number;
  @JsonKey(name: 'default_value')
  String? defaultValue;
  @JsonKey(name: 'input')
  String? input;
  @JsonKey(name: 'mandatory')
  int? mandatory;
  @JsonKey(name: 'values')
  List<String>? values;
  @JsonKey(name: 'place_holder')
  String? placeHolder;
  @JsonKey(name: 'default_type')
  String? defaultType;
  @JsonKey(name: 'field_name')
  String? fieldName;

  StepField(
      {this.label,
      this.key,
      this.defaultType,
      this.defaultValue,
      this.field,
      this.fieldName,
      this.help,
      this.input,
      this.mandatory,
      this.number,
      this.placeHolder,
      this.values});

  factory StepField.fromJson(Map<String, dynamic> json) =>
      _$StepFieldFromJson(json);

  Map<String, dynamic> toJson() => _$StepFieldToJson(this);
}
