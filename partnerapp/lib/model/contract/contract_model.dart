import 'package:Haandvaerker.dk/model/payment/payment_stages_model.dart';
import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'contract_model.g.dart';

@JsonSerializable()
class ContractModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'TEMPLATE_ID')
  int? templateId;
  @JsonKey(name: 'DOCUMENT_ID')
  int? documentId;
  @JsonKey(name: 'PARENT_ID')
  int? parentId;
  @JsonKey(name: 'CONTRACT_LABEL')
  String? contractLabel;
  @JsonKey(name: 'CREATED_BY')
  String? createdBy;
  // @JsonKey(name: 'UPDATED_BY')
  // String? updatedBy;
  @JsonKey(name: 'CONTACT')
  int? contact;
  @JsonKey(name: 'COMPANY')
  int? company;
  @JsonKey(name: 'STATUS')
  String? status;
  @JsonKey(name: 'DATE_SIGNED_BY_CONTACT')
  String? dateSignedByContact;
  @JsonKey(name: 'DATE_SIGNED_BY_PARTNER')
  String? dateSignedByPartner;
  @JsonKey(name: 'CONTACT_SIGNATURE')
  int? contactSignature;
  @JsonKey(name: 'PARTNER_SIGNATURE')
  int? partnerSignature;
  @JsonKey(name: 'CONTACT_IP')
  String? contactIp;
  @JsonKey(name: 'PARTNER_IP')
  String? partnerIp;
  @JsonKey(name: 'FIXED_PRICE')
  int? fixedPrice;
  @JsonKey(name: 'FIXED_PRICE_DA')
  String? fixedPriceDa;
  @JsonKey(name: 'VAT_PRICE')
  double? vatPrice;
  @JsonKey(name: 'TRANSACTION_CUT')
  double? transactionCut;
  @JsonKey(name: 'TOTAL_AMOUNT_PAID')
  int? totalAmountPaid;
  @JsonKey(name: 'IS_RECALCULATION')
  int? isRecalculation;
  @JsonKey(name: 'INSURANCE')
  int? insurance;
  @JsonKey(name: 'INSURANCE_NOTE')
  String? insuranceNote;
  @JsonKey(name: 'INSURANCE_MIN_PRICE')
  int? insuranceMinPrice;
  @JsonKey(name: 'INSURANCE_MAX_PRICE')
  int? insuranceMaxPrice;
  @JsonKey(name: 'FROM_REJECT_OFFER')
  bool? fromRejectOffer;
  @JsonKey(name: 'OFFER_ID')
  int? offerId;
  @JsonKey(name: 'OFFER_VERSION_ID')
  int? offerVersionId;
  @JsonKey(name: 'CONTRACT_STATUS')
  String? contractStatus;
  @JsonKey(name: 'CONTACT_NAME')
  String? contactName;
  @JsonKey(name: 'CONTACT_LAST_NAME')
  String? contactLastName;
  @JsonKey(name: 'CONTACT_PHONE')
  String? contactPhone;
  @JsonKey(name: 'CONTACT_EMAIL')
  String? contactEmail;
  @JsonKey(name: 'COMPANY_NAME')
  String? companyName;
  @JsonKey(name: 'COMPANY_LAST_NAME')
  String? companyLastName;
  @JsonKey(name: 'COMPANY_PHONE')
  String? companyPhone;
  @JsonKey(name: 'COMPANY_EMAIL')
  String? companyEmail;
  @JsonKey(name: 'TRANSACTION')
  PaymentStageTransaction? transaction;
  @JsonKey(name: 'PROJECT')
  PartnerProjectsModel? project;
  @JsonKey(name: 'DOCUMENTS')
  List<Documents>? documents;
  @JsonKey(name: 'COMMENTS')
  List<dynamic>? comments;
  @JsonKey(name: 'ANNOTATIONS')
  List<dynamic>? annotations;

  ContractModel({
    this.id,
    this.templateId,
    this.documentId,
    this.parentId,
    this.contractLabel,
    this.createdBy,
    // this.updatedBy,
    this.contact,
    this.status,
    this.dateSignedByContact,
    this.dateSignedByPartner,
    this.contactSignature,
    this.partnerSignature,
    this.contactIp,
    this.partnerIp,
    this.fixedPrice,
    this.fixedPriceDa,
    this.vatPrice,
    this.transactionCut,
    this.totalAmountPaid,
    this.isRecalculation,
    this.insurance,
    this.insuranceNote,
    this.insuranceMinPrice,
    this.insuranceMaxPrice,
    this.fromRejectOffer,
    this.offerId,
    this.offerVersionId,
    this.contractStatus,
    this.contactName,
    this.contactLastName,
    this.contactPhone,
    this.contactEmail,
    this.companyName,
    this.companyLastName,
    this.companyPhone,
    this.companyEmail,
    this.transaction,
    this.project,
    this.documents,
    this.comments,
    this.annotations,
  });

  factory ContractModel.fromJson(Map<String, dynamic> json) =>
      _$ContractModelFromJson(json);

  Map<String, dynamic> toJson() => _$ContractModelToJson(this);

  static List<ContractModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => ContractModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class Documents {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'JSON_DATA')
  JsonData? jsonData;

  Documents({this.jsonData, this.id});

  factory Documents.fromJson(Map<String, dynamic> json) =>
      _$DocumentsFromJson(json);

  Map<String, dynamic> toJson() => _$DocumentsToJson(this);

  static List<Documents> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => Documents.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class JsonData {
  @JsonKey(name: 'projectId')
  int? projectId;
  @JsonKey(name: 'templateId')
  dynamic templateId;
  @JsonKey(name: 'contractTemplateId')
  dynamic contractTemplateId;
  @JsonKey(name: 'title')
  String? title;
  @JsonKey(name: 'companyId')
  int? companyId;
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'lastname')
  String? lastName;
  @JsonKey(name: 'address')
  String? address;
  @JsonKey(name: 'zipcode')
  dynamic zipcode;
  @JsonKey(name: 'email')
  String? email;
  @JsonKey(name: 'mobile')
  String? mobile;
  @JsonKey(name: 'city')
  String? city;

  @JsonKey(name: 'totalpricewithvatandfee')
  double? totalpricewithvatandfee;
  @JsonKey(name: 'totalpricewithvat')
  double? totalpricewithvat;
  @JsonKey(name: 'contactId')
  int? contactId;
  @JsonKey(name: 'cvr')
  String? cvr;
  @JsonKey(name: 'companyName')
  String? companyName;
  @JsonKey(name: 'offerId')
  int? offerId;
  @JsonKey(name: 'paymentStages')
  dynamic paymentStages;

  JsonData({
    this.address,
    this.city,
    this.companyId,
    this.contractTemplateId,
    this.email,
    this.lastName,
    this.mobile,
    this.name,
    this.projectId,
    this.templateId,
    this.title,
    this.zipcode,
    this.totalpricewithvatandfee,
    this.totalpricewithvat,
    this.contactId,
    this.companyName,
    this.cvr,
    this.offerId,
    this.paymentStages,
  });

  factory JsonData.fromJson(Map<String, dynamic> json) =>
      _$JsonDataFromJson(json);

  Map<String, dynamic> toJson() => _$JsonDataToJson(this);

  static List<JsonData> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => JsonData.fromJson(i as Map<String, dynamic>))
      .toList();
}
