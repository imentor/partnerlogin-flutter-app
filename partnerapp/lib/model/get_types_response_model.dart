import 'package:json_annotation/json_annotation.dart';

part 'get_types_response_model.g.dart';

@JsonSerializable()
class GetTypesResponseModel {
  bool? success;
  String? message;
  List<Data>? data;
  Time? time;

  GetTypesResponseModel({this.success, this.message, this.data, this.time});

  factory GetTypesResponseModel.fromJson(Map<String, dynamic> json) =>
      _$GetTypesResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$GetTypesResponseModelToJson(this);
}

@JsonSerializable()
class Data {
  @JsonKey(name: 'BRANCHE_ID')
  int? branchId;
  @JsonKey(name: 'BRANCHE')
  String? branche;
  @JsonKey(name: 'BRANCHE_EN')
  String? branchEn;
  @JsonKey(name: 'PLURAL')
  String? plural;
  @JsonKey(name: 'CVR_INDUSTRY')
  String? cvrIndustry;
  @JsonKey(name: 'M_CVR_INDUSTRY')
  String? mcvrIndustry;
  @JsonKey(name: 'AKTIVERET')
  int? aktiveret;
  @JsonKey(name: 'BRANCHE_TYPE_UI')
  int? branchTypeUI;
  @JsonKey(name: 'GUIDELINES')
  String? guidelines;
  @JsonKey(name: 'USER_FRIENDLY_NAME')
  String? userFriendlyName;
  @JsonKey(name: 'SORT_ORDER')
  int? sortOrder;
  @JsonKey(name: 'DIRECTORY_MAX_DISTANCE')
  dynamic directoryMaxDistance;
  @JsonKey(name: 'LOCK_IN_FRONTEND')
  int? lockInFrontEnd;
  @JsonKey(name: 'PICTURE_URL')
  String? pictureUrl;
  @JsonKey(name: 'URL')
  dynamic url;
  @JsonKey(name: 'DESCRIPTION')
  String? description1;
  @JsonKey(name: 'TEASER')
  String? teaser;
  @JsonKey(name: 'INTRO')
  String? intro;
  @JsonKey(name: 'FAQ1_QUESTION')
  String? faq1Question;
  @JsonKey(name: 'FAQ1_ANSWER')
  String? faq1Answer;
  @JsonKey(name: 'FAQ2_QUESTION')
  String? faq2Question;
  @JsonKey(name: 'FAQ2_ANSWER')
  String? faq2Answer;
  @JsonKey(name: 'FAQ3_QUESTION')
  String? faq3Question;
  @JsonKey(name: 'FAQ3_ANSWER')
  String? faq3Answer;
  @JsonKey(name: 'SubIndustries')
  List<SubIndustries>? subIndustries;
  @JsonKey(name: 'IndustryId')
  int? industryId;
  @JsonKey(name: 'IndustryName')
  String? industryName;
  @JsonKey(name: 'Description')
  String? description2;

  Data(
      {this.branchId,
      this.branche,
      this.branchEn,
      this.plural,
      this.cvrIndustry,
      this.mcvrIndustry,
      this.aktiveret,
      this.branchTypeUI,
      this.guidelines,
      this.userFriendlyName,
      this.sortOrder,
      this.directoryMaxDistance,
      this.lockInFrontEnd,
      this.pictureUrl,
      this.url,
      this.description1,
      this.teaser,
      this.intro,
      this.faq1Question,
      this.faq1Answer,
      this.faq2Question,
      this.faq2Answer,
      this.faq3Question,
      this.faq3Answer,
      this.subIndustries,
      this.industryId,
      this.industryName,
      this.description2});

  Data.defaults()
      : branchId = 0,
        branche = '',
        branchEn = '',
        plural = '',
        cvrIndustry = '',
        mcvrIndustry = '',
        aktiveret = 0,
        branchTypeUI = 0,
        guidelines = '',
        userFriendlyName = '',
        sortOrder = 0,
        directoryMaxDistance = '',
        lockInFrontEnd = 0,
        pictureUrl = '',
        url = '',
        description1 = '',
        teaser = '',
        intro = '',
        faq1Question = '',
        faq1Answer = '',
        faq2Question = '',
        faq2Answer = '',
        faq3Question = '',
        faq3Answer = '',
        subIndustries = [],
        industryId = 0,
        industryName = '',
        description2 = '';

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  Map<String, dynamic> toJson() => _$DataToJson(this);

  static List<Data> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => Data.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class SubIndustries {
  @JsonKey(name: 'SUBCATEGORY_ID')
  int? subcategoryId;
  @JsonKey(name: 'BRANCH_ID')
  int? branchId;
  @JsonKey(name: 'PARENT_ID')
  int? parentId;
  @JsonKey(name: 'SUBCATEGORY_NAME')
  String? subCategoryName;
  @JsonKey(name: 'SUBCATEGORY_NAME_EN')
  String? subCategoryNameEn;
  @JsonKey(name: 'PLURAL')
  dynamic plural;
  @JsonKey(name: 'GUIDELINES')
  String? guidelines;
  @JsonKey(name: 'AUTO_DISTRIBUTE')
  int? autoDistribute;
  @JsonKey(name: 'INCLUDE_INDUSTRY_SEARCH')
  int? includeIndustrySearch;
  @JsonKey(name: 'deleted')
  int? deleted;
  @JsonKey(name: 'PICTURE_URL')
  String? pictureUrl;
  @JsonKey(name: 'URL')
  dynamic url;
  @JsonKey(name: 'DESCRIPTION')
  dynamic description1;
  @JsonKey(name: 'TEASER')
  dynamic teaser;
  @JsonKey(name: 'INTRO')
  dynamic intro;
  @JsonKey(name: 'FAQ1_QUESTION')
  dynamic faq1Question;
  @JsonKey(name: 'FAQ1_ANSWER')
  dynamic faq1Answer;
  @JsonKey(name: 'FAQ2_QUESTION')
  dynamic faq2Question;
  @JsonKey(name: 'FAQ2_ANSWER')
  dynamic faq2Answer;
  @JsonKey(name: 'FAQ3_QUESTION')
  dynamic faq3Question;
  @JsonKey(name: 'FAQ3_ANSWER')
  dynamic faq3Answer;
  @JsonKey(name: 'HVDK_BUSINESSAREA')
  List<dynamic>? hvdkBusinessArea;
  @JsonKey(name: 'HVDK_BUSINESSAREA_OTHER')
  List<dynamic>? hvdkBusinessAreaOther;
  @JsonKey(name: 'HVDK_BUSINESSAREAKMO')
  List<dynamic>? hvdkBusinessAreaKmo;
  @JsonKey(name: 'HVDK_BUSINESSAREAKMO_OTHER')
  List<dynamic>? hvdkBusinessAreaKmoOther;
  @JsonKey(name: 'SubIndustrId')
  int? subIndustrId;
  @JsonKey(name: 'SubIndustryName')
  String? subIndustryName;
  @JsonKey(name: 'Description')
  String? description2;
  @JsonKey(name: 'taskTypes')
  List<TaskTypes?>? taskTypes;

  SubIndustries(
      {this.subcategoryId,
      this.branchId,
      this.parentId,
      this.subCategoryName,
      this.subCategoryNameEn,
      this.plural,
      this.guidelines,
      this.autoDistribute,
      this.includeIndustrySearch,
      this.deleted,
      this.pictureUrl,
      this.url,
      this.description1,
      this.teaser,
      this.intro,
      this.faq1Question,
      this.faq1Answer,
      this.faq2Question,
      this.faq2Answer,
      this.faq3Question,
      this.faq3Answer,
      this.hvdkBusinessAreaKmo,
      this.hvdkBusinessArea,
      this.hvdkBusinessAreaOther,
      this.hvdkBusinessAreaKmoOther,
      this.subIndustrId,
      this.subIndustryName,
      this.description2,
      this.taskTypes});

  factory SubIndustries.fromJson(Map<String, dynamic> json) =>
      _$SubIndustriesFromJson(json);

  Map<String, dynamic> toJson() => _$SubIndustriesToJson(this);
}

class TaskTypes {
  int? taskTypeId;
  String? name;
  String? taskTypePictureUrl;
  int? isPrimary;
  String? subcategoryName;
  int? subcategoryId;

  TaskTypes(
      {this.taskTypeId,
      this.name,
      this.taskTypePictureUrl,
      this.isPrimary,
      this.subcategoryName,
      this.subcategoryId});

  TaskTypes.fromJson(Map<String, dynamic> json) {
    taskTypeId = json['taskTypeId'];
    name = json['name'];
    taskTypePictureUrl = json['taskTypePictureUrl'];
    isPrimary = json['isPrimary'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['taskTypeId'] = taskTypeId;
    data['name'] = name;
    data['taskTypePictureUrl'] = taskTypePictureUrl;
    data['isPrimary'] = isPrimary;
    return data;
  }
}

class Time {
  String? start;
  String? end;
  String? duration;

  Time({this.start, this.end, this.duration});

  Time.fromJson(Map<String, dynamic> json) {
    start = json['start'];
    end = json['end'];
    duration = json['duration'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['start'] = start;
    data['end'] = end;
    data['duration'] = duration;
    return data;
  }
}
