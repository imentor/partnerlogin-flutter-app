import 'package:Haandvaerker.dk/model/contact/contact_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'subcontractor_invitation_model.g.dart';

@JsonSerializable()
class SubcontractorInvitation {
  @JsonKey(name: "ID")
  int? id;
  @JsonKey(name: "NAME")
  String? name;
  @JsonKey(name: "EXTERNAL_ID")
  String? externalId;
  @JsonKey(name: "CREATED_ON")
  String? createdOn;
  @JsonKey(name: "UPDATED_ON")
  String? updatedOn;
  @JsonKey(name: "CREATED_BY")
  int? createdBy;
  // @JsonKey(name: "UPDATED_BY")
  // String? updatedBy;
  @JsonKey(name: "RESPONSIBLE_PERSON")
  String? responsiblePerson;
  @JsonKey(name: "AVAILABLE_TO_EVERYONE")
  String? availableToEveryone;
  @JsonKey(name: "CREATED_BY_CRM_FORM")
  String? createdByCrmForm;
  @JsonKey(name: "COMPANY")
  int? company;
  @JsonKey(name: "CONTACT")
  String? contact;
  @JsonKey(name: "PIPELINE")
  int? pipeline;
  @JsonKey(name: "MOVED_ON")
  String? movedOn;
  @JsonKey(name: "MOVED_BY")
  int? movedBy;
  @JsonKey(name: "STAGE")
  String? stage;
  @JsonKey(name: "PREVIOUS_STAGE")
  String? previousStage;
  @JsonKey(name: "SOURCE")
  String? source;
  @JsonKey(name: "SOURCE_INFORMATION")
  String? sourceInformation;
  @JsonKey(name: "CURRENCY")
  String? currency;
  @JsonKey(name: "AMOUNT_CALCULATION_MODE")
  String? amountCalculationMode;
  @JsonKey(name: "AMOUNT")
  String? amount;
  @JsonKey(name: "TAX_TOTAL")
  String? taxTotal;
  // @JsonKey(name: "YOUR_COMPANY_DETAILS")
  // String? yourCompanyDetails;
  @JsonKey(name: "LAST_TIMELINE_ACTIVITY_BY")
  int? lastTimelineActivityBy;
  @JsonKey(name: "LAST_UPDATED_ON")
  String? lastUpdatedOn;
  @JsonKey(name: "PROJECT")
  String? project;
  @JsonKey(name: "DATE_SIGNED_BY_PARTNER")
  String? dateSignedByPartner;
  @JsonKey(name: "DATE_SIGNED_BY_SUBCONTRACTOR")
  String? dateSignedBySubcontractor;
  @JsonKey(name: "SUBCONTRACTOR")
  String? subcontractor;
  @JsonKey(name: "PARTNER_IP")
  String? partnerIp;
  @JsonKey(name: "SUBCONTRACTOR_IP")
  String? subcontractorIp;
  @JsonKey(name: "START_DATE")
  String? startDate;
  @JsonKey(name: "END_DATE")
  String? endDate;
  @JsonKey(name: "CREATED_FROM")
  String? createdFrom;
  // @JsonKey(name: "PARTNER_SIGNATURE")
  // int? partnerSignature;
  // @JsonKey(name: "SUBCONTRACTOR_SIGNATURE")
  // int? subcontractorSignature;
  @JsonKey(name: "COMPANY_NAME")
  String? companyName;
  @JsonKey(name: "COMPANY_LAST_NAME")
  String? companyLastName;
  @JsonKey(name: "COMPANY_PHONE")
  String? companyPhone;
  @JsonKey(name: "COMPANY_EMAIL")
  String? companyEmail;
  @JsonKey(name: "SUBCONTRACTOR_NAME")
  String? subcontractorName;
  @JsonKey(name: "SUBCONTRACTOR_LAST_NAME")
  String? subcontractorLastName;
  @JsonKey(name: "SUBCONTRACTOR_PHONE")
  String? subcontractorPhone;
  @JsonKey(name: "SUBCONTRACTOR_EMAIL")
  String? subcontractorEmail;
  @JsonKey(name: "ACCEPTED_FROM")
  String? acceptedFrom;
  // @JsonKey(name: "DOCUMENT_ID")
  // int? documentId;
  @JsonKey(name: "SUBCONTRACTOR_COMPANY_NAME")
  String? subcontractorCompanyName;
  @JsonKey(name: "SUBCONTRACTOR_CVR")
  String? subcontractorCvr;
  @JsonKey(name: "SUBCONTRACTOR_ADDRESS")
  String? subcontractorAddress;
  @JsonKey(name: "TRANSACTION_COST")
  String? transactionCost;
  @JsonKey(name: "TRANSACTION_CUT")
  String? transactionCut;
  @JsonKey(name: "TRANSACTION_COST_DA")
  String? transactionCostDa;
  @JsonKey(name: "CONTRACT")
  String? contract;
  @JsonKey(name: "FIXED_PRICE")
  String? fixedPrice;
  @JsonKey(name: "FIXED_PRICE_DA")
  String? fixedPriceDa;
  @JsonKey(name: "DESCRIPTION")
  String? description;
  @JsonKey(name: "PROJECT_ADDRESS")
  String? projectAddress;
  @JsonKey(name: "PAYMENT_STAGES")
  String? paymentStages;
  @JsonKey(name: "LOGIN_SHORT_URL")
  String? loginShortUrl;
  @JsonKey(name: "COMPANY_INFO")
  Contact? companyInfo;
  @JsonKey(name: "PROJECT_NAME")
  String? projectName;

  SubcontractorInvitation({
    this.id,
    this.name,
    this.externalId,
    this.createdOn,
    this.updatedOn,
    this.createdBy,
    // this.updatedBy,
    this.responsiblePerson,
    this.availableToEveryone,
    this.createdByCrmForm,
    this.company,
    this.contact,
    this.pipeline,
    this.movedOn,
    this.movedBy,
    this.stage,
    this.previousStage,
    this.source,
    this.sourceInformation,
    this.currency,
    this.amountCalculationMode,
    this.amount,
    this.taxTotal,
    // this.yourCompanyDetails,
    this.lastTimelineActivityBy,
    this.lastUpdatedOn,
    this.project,
    this.dateSignedByPartner,
    this.dateSignedBySubcontractor,
    this.subcontractor,
    this.partnerIp,
    this.subcontractorIp,
    this.startDate,
    this.endDate,
    this.createdFrom,
    // this.partnerSignature,
    // this.subcontractorSignature,
    this.companyName,
    this.companyLastName,
    this.companyPhone,
    this.companyEmail,
    this.subcontractorName,
    this.subcontractorLastName,
    this.subcontractorPhone,
    this.subcontractorEmail,
    this.acceptedFrom,
    // this.documentId,
    this.subcontractorCompanyName,
    this.subcontractorCvr,
    this.subcontractorAddress,
    this.transactionCost,
    this.transactionCut,
    this.transactionCostDa,
    this.contract,
    this.fixedPrice,
    this.fixedPriceDa,
    this.description,
    this.projectAddress,
    this.paymentStages,
    this.loginShortUrl,
    this.companyInfo,
    this.projectName,
  });

  factory SubcontractorInvitation.fromJson(Map<String, dynamic> json) =>
      _$SubcontractorInvitationFromJson(json);

  Map<String, dynamic> toJson() => _$SubcontractorInvitationToJson(this);

  static List<SubcontractorInvitation> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => SubcontractorInvitation.fromJson(i as Map<String, dynamic>))
      .toList();
}
