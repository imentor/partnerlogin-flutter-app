import 'package:json_annotation/json_annotation.dart';

part 'mester_partners_model.g.dart';

@JsonSerializable()
class MesterPartnersModel {
  dynamic id;
  dynamic pid;
  dynamic created;
  dynamic done;
  @JsonKey(name: 'final')
  dynamic finall;
  dynamic opgave2;
  dynamic numofman2;
  dynamic postnumber2;
  dynamic startdate2;
  dynamic timepris2;
  dynamic desc2;
  dynamic potentialid;
  dynamic appid;
  dynamic ip;
  List<dynamic>? imagelist;
  dynamic offer2;
  @JsonKey(name: 'nps_sent')
  dynamic npsSent;
  @JsonKey(name: 'nps_date')
  dynamic npsDate;
  dynamic reserve;
  dynamic productname;
  dynamic postnumbercity2;
  @JsonKey(name: 'id_lav')
  dynamic idLav;
  dynamic productimage;
  dynamic urlshort;

  MesterPartnersModel({
    this.id,
    this.pid,
    this.created,
    this.done,
    this.finall,
    this.opgave2,
    this.numofman2,
    this.postnumber2,
    this.startdate2,
    this.timepris2,
    this.desc2,
    this.potentialid,
    this.appid,
    this.ip,
    this.imagelist,
    this.offer2,
    this.npsSent,
    this.npsDate,
    this.reserve,
    this.productname,
    this.postnumbercity2,
    this.idLav,
    this.productimage,
    this.urlshort,
  });

  factory MesterPartnersModel.fromJson(Map<String, dynamic> json) =>
      _$MesterPartnersModelFromJson(json);

  Map<String, dynamic> toJson() => _$MesterPartnersModelToJson(this);
}
