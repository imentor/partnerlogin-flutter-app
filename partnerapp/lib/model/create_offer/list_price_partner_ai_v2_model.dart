import 'package:json_annotation/json_annotation.dart';

part 'list_price_partner_ai_v2_model.g.dart';

@JsonSerializable()
class ListPricePartnerAiV2Model {
  @JsonKey(name: 'ID')
  String? id;
  @JsonKey(name: 'ARCHIVE')
  String? archive;
  @JsonKey(name: 'CALCULATION_ID')
  String? caclulationId;
  @JsonKey(name: 'CONTACT_ID')
  String? contactId;
  @JsonKey(name: 'INDUSTRY')
  String? industry;
  @JsonKey(name: 'IS_DELETED')
  String? isDeleted;
  @JsonKey(name: 'JOB_INDUSTRY')
  String? jobIndustry;
  @JsonKey(name: 'PARTNER_IDS')
  dynamic partnerIds;
  @JsonKey(name: 'PENDING')
  String? pending;
  @JsonKey(name: 'PENDING_DATE')
  dynamic pendingDate;
  @JsonKey(name: 'PROJECT_ID')
  String? projectId;
  @JsonKey(name: 'SERIES')
  dynamic series;
  @JsonKey(name: 'SERIES_ORIGINAL')
  List<String>? seriesOriginal;
  @JsonKey(name: 'UPDATE_TEXT')
  String? updateText;
  @JsonKey(name: 'created')
  String? created;

  ListPricePartnerAiV2Model({
    this.id,
    this.archive,
    this.caclulationId,
    this.contactId,
    this.industry,
    this.isDeleted,
    this.jobIndustry,
    this.partnerIds,
    this.pending,
    this.pendingDate,
    this.projectId,
    this.series,
    this.seriesOriginal,
    this.updateText,
    this.created,
  });

  ListPricePartnerAiV2Model.defaults()
      : id = '',
        archive = '',
        caclulationId = '',
        contactId = '',
        industry = '',
        isDeleted = '',
        jobIndustry = '',
        partnerIds = '',
        pending = '',
        pendingDate = '',
        projectId = '',
        series = null,
        seriesOriginal = [],
        updateText = '',
        created = '';

  factory ListPricePartnerAiV2Model.fromJson(Map<String, dynamic> json) =>
      _$ListPricePartnerAiV2ModelFromJson(json);

  Map<String, dynamic> toJson() => _$ListPricePartnerAiV2ModelToJson(this);
  static List<ListPricePartnerAiV2Model> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => ListPricePartnerAiV2Model.fromJson(i as Map<String, dynamic>))
      .toList();
}
