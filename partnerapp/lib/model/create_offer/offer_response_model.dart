import 'package:json_annotation/json_annotation.dart';

part 'offer_response_model.g.dart';

@JsonSerializable()
class OfferPostResponseModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'ACCEPTED_ENTERPRISE_INDUSTRY_IDS')
  List<String>? acceptedEnterpriseIndustryIds;
  @JsonKey(name: 'ADDED_TO_BUDGET')
  bool? addedToBudget;
  @JsonKey(name: 'COMPANY_CVR')
  String? companyCVR;
  @JsonKey(name: 'COMPANY_EMAIL')
  String? companyEmail;
  @JsonKey(name: 'COMPANY_NAME')
  String? companyName;
  @JsonKey(name: 'CONTACT_ID')
  int? contactId;
  @JsonKey(name: 'CREATED_AT')
  String? createdAt;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'DISTANCE_IN_KM')
  double? distanceInKM;
  @JsonKey(name: 'END_DATE')
  dynamic endDate;
  @JsonKey(name: 'ENTERPRISE_INDUSTRY_ID')
  dynamic enterpriseIndustryId;
  @JsonKey(name: 'ENTERPRISE_INDUSTRY_IDS')
  List<String>? enterpriseIndustryIds;
  @JsonKey(name: 'EXPIRY_DATE')
  dynamic expiryDate;
  @JsonKey(name: 'HOMEOWNER')
  HomeOwnerOfferModel? homeOwner;
  @JsonKey(name: 'IMAGE_URL')
  String? imageURL;
  @JsonKey(name: 'INDUSTRY_IDS')
  List<dynamic>? industryIds;
  @JsonKey(name: 'IS_PARTNER')
  bool? isPartner;
  @JsonKey(name: 'JOB_INDUSTRY_ID')
  dynamic jobIndustryId;
  @JsonKey(name: 'LEAD_ID')
  int? leadId;
  @JsonKey(name: 'OFFER_FILE')
  dynamic offerFile;
  @JsonKey(name: 'PARTNER_PROFILE')
  PartnerProfileOfferModel? partnerProfile;
  @JsonKey(name: 'PROJECT_ID')
  int? projectId;
  @JsonKey(name: 'REFERENCE_NUMBER')
  int? referenceNumber;
  @JsonKey(name: 'START_DATE')
  dynamic startDate;
  @JsonKey(name: 'STATUS')
  String? status;
  @JsonKey(name: 'SUB_TOTAL_PRICE')
  double? subTotalPrice;
  @JsonKey(name: 'TASKS')
  List<TaskOfferModel?>? tasks;
  @JsonKey(name: 'TITLE')
  String? title;
  @JsonKey(name: 'TOTAL_AMOUNT_PAID')
  double? totalAmountPaid;
  @JsonKey(name: 'TOTAL_PRICE')
  double? totalPrice;
  @JsonKey(name: 'UPDATED_AT')
  String? updatedAt;
  @JsonKey(name: 'VAT')
  double? vat;

  OfferPostResponseModel({
    this.acceptedEnterpriseIndustryIds,
    this.addedToBudget,
    this.companyCVR,
    this.companyEmail,
    this.companyName,
    this.contactId,
    this.createdAt,
    this.description,
    this.distanceInKM,
    this.endDate,
    this.enterpriseIndustryId,
    this.enterpriseIndustryIds,
    this.expiryDate,
    this.homeOwner,
    this.id,
    this.imageURL,
    this.industryIds,
    this.isPartner,
    this.jobIndustryId,
    this.leadId,
    this.offerFile,
    this.partnerProfile,
    this.projectId,
    this.referenceNumber,
    this.startDate,
    this.status,
    this.subTotalPrice,
    this.tasks,
    this.title,
    this.totalAmountPaid,
    this.totalPrice,
    this.updatedAt,
    this.vat,
  });

  factory OfferPostResponseModel.fromJson(Map<String, dynamic> json) =>
      _$OfferPostResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$OfferPostResponseModelToJson(this);
  static List<OfferPostResponseModel> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => OfferPostResponseModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class HomeOwnerOfferModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'EMAIL')
  String? email;
  @JsonKey(name: 'MOBILE')
  String? mobile;
  @JsonKey(name: 'NAME')
  String? name;

  HomeOwnerOfferModel({
    this.id,
    this.email,
    this.mobile,
    this.name,
  });

  factory HomeOwnerOfferModel.fromJson(Map<String, dynamic> json) =>
      _$HomeOwnerOfferModelFromJson(json);

  Map<String, dynamic> toJson() => _$HomeOwnerOfferModelToJson(this);
  static List<HomeOwnerOfferModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => HomeOwnerOfferModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class PartnerProfileOfferModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'ADDRESS')
  String? address;
  @JsonKey(name: 'AVATAR')
  String? avatar;
  @JsonKey(name: 'EMPLOYEES_COUNT')
  int? employeesCount;
  @JsonKey(name: 'ESTABLISHED_DATE')
  dynamic establishedDate;
  @JsonKey(name: 'RATING_AVG')
  double? ratingAVG;
  @JsonKey(name: 'REVIEW_COUNT')
  int? reviewCount;
  @JsonKey(name: 'TITLE')
  String? title;
  @JsonKey(name: 'ZIP_CODE')
  String? zipCode;

  PartnerProfileOfferModel({
    this.id,
    this.address,
    this.avatar,
    this.employeesCount,
    this.establishedDate,
    this.ratingAVG,
    this.reviewCount,
    this.title,
    this.zipCode,
  });

  factory PartnerProfileOfferModel.fromJson(Map<String, dynamic> json) =>
      _$PartnerProfileOfferModelFromJson(json);

  Map<String, dynamic> toJson() => _$PartnerProfileOfferModelToJson(this);
  static List<PartnerProfileOfferModel> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => PartnerProfileOfferModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class TaskOfferModel {
  @JsonKey(name: 'description')
  String? description;
  @JsonKey(name: 'quantity')
  String? quantity;
  @JsonKey(name: 'title')
  String? title;
  @JsonKey(name: 'unit')
  String? unit;
  @JsonKey(name: 'value')
  String? value;

  TaskOfferModel({
    this.description,
    this.quantity,
    this.title,
    this.unit,
    this.value,
  });

  factory TaskOfferModel.fromJson(Map<String, dynamic> json) =>
      _$TaskOfferModelFromJson(json);

  Map<String, dynamic> toJson() => _$TaskOfferModelToJson(this);
  static List<TaskOfferModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => TaskOfferModel.fromJson(i as Map<String, dynamic>))
      .toList();
}
