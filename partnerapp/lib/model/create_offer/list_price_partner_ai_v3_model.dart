import 'package:json_annotation/json_annotation.dart';

part 'list_price_partner_ai_v3_model.g.dart';

@JsonSerializable()
class ListPricePartnerAiV3Model {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'CONTACT_ID')
  int? contactId;
  @JsonKey(name: 'PARTNER_IDS')
  dynamic partnerIds;
  @JsonKey(name: 'INDUSTRY')
  int? industry;
  @JsonKey(name: 'JOB_INDUSTRY')
  int? jobIndustry;
  @JsonKey(name: 'SERIES')
  dynamic series;
  @JsonKey(name: 'ARCHIVE')
  int? archive;
  @JsonKey(name: 'created')
  String? created;
  @JsonKey(name: 'PROJECT_ID')
  int? projectId;
  @JsonKey(name: 'CALCULATION_ID')
  int? caclulationId;
  @JsonKey(name: 'UPDATE_TEXT')
  dynamic updateText;
  @JsonKey(name: 'PENDING')
  int? pending;
  @JsonKey(name: 'PENDING_DATE')
  String? pendingDate;
  @JsonKey(name: 'IS_DELETED')
  int? isDeleted;
  @JsonKey(name: 'SERIES_ORIGINAL')
  List<String>? seriesOriginal;

  ListPricePartnerAiV3Model({
    this.id,
    this.archive,
    this.caclulationId,
    this.contactId,
    this.industry,
    this.isDeleted,
    this.jobIndustry,
    this.partnerIds,
    this.pending,
    this.pendingDate,
    this.projectId,
    this.series,
    this.updateText,
    this.created,
    this.seriesOriginal,
  });

  ListPricePartnerAiV3Model.defaults()
      : id = 0,
        archive = 0,
        caclulationId = 0,
        contactId = 0,
        industry = 0,
        isDeleted = 0,
        jobIndustry = 0,
        partnerIds = '',
        pending = 0,
        pendingDate = '',
        projectId = 0,
        series = null,
        updateText = '',
        created = '',
        seriesOriginal = [];

  factory ListPricePartnerAiV3Model.fromJson(Map<String, dynamic> json) =>
      _$ListPricePartnerAiV3ModelFromJson(json);

  Map<String, dynamic> toJson() => _$ListPricePartnerAiV3ModelToJson(this);
  static List<ListPricePartnerAiV3Model> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => ListPricePartnerAiV3Model.fromJson(i as Map<String, dynamic>))
      .toList();
}
