import 'package:json_annotation/json_annotation.dart';

part 'press_article_model.g.dart';

@JsonSerializable()
class PressArticleModel {
  @JsonKey(name: 'YEARS_WON')
  int? yearsWon;
  @JsonKey(name: 'Comment')
  String? comment;
  @JsonKey(name: 'Kategori')
  String? category;
  @JsonKey(name: 'partner_id')
  String? partnerId;
  @JsonKey(name: 'Region')
  String? region;
  @JsonKey(name: 'UF_CRM_COMPANY_CONTACT_PERSON')
  String? contactPerson;
  @JsonKey(name: 'Name')
  String? name;

  PressArticleModel(
      {this.category,
      this.comment,
      this.contactPerson,
      this.name,
      this.partnerId,
      this.region,
      this.yearsWon});

  factory PressArticleModel.fromJson(Map<String, dynamic> json) =>
      _$PressArticleModelFromJson(json);

  Map<String, dynamic> toJson() => _$PressArticleModelToJson(this);
}
