import 'package:json_annotation/json_annotation.dart';

part 'badge_model.g.dart';

@JsonSerializable()
class BadgeResponseModel {
  @JsonKey(name: 'Badges')
  List<BadgeModel?>? badges;

  BadgeResponseModel({this.badges});

  factory BadgeResponseModel.fromJson(Map<String, dynamic> json) =>
      _$BadgeResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$BadgeResponseModelToJson(this);
}

@JsonSerializable()
class BadgeModel {
  @JsonKey(name: "Id")
  int? id;
  @JsonKey(name: "CreateDate")
  String? createdDate;
  @JsonKey(name: "Title")
  String? title;
  @JsonKey(name: "SupplierId")
  int? supplierId;
  @JsonKey(name: "SupplierName")
  String? supplierName;
  @JsonKey(name: "BadgeImg")
  String? badgeImg;
  @JsonKey(name: "CoverImg")
  String? coverImg;
  @JsonKey(name: "BadgeType")
  String? badgeType;
  @JsonKey(name: "BadgeYear")
  String? badgeYear;
  @JsonKey(name: "Industry")
  String? industry;
  @JsonKey(name: "Area")
  String? area;
  @JsonKey(name: "Point")
  int? point;
  @JsonKey(name: "Position")
  int? position;
  @JsonKey(name: "BadgeMonth")
  String? badgeMonth;
  @JsonKey(name: "IsOK")
  bool? isOk;
  @JsonKey(name: "AverageRating")
  dynamic averageRating;
  @JsonKey(name: "TotalRating")
  dynamic totalRating;
  @JsonKey(name: "AuthorizationNo")
  dynamic authorizationNo;
  @JsonKey(name: "SubcategoryId")
  int? subcategoryId;
  @JsonKey(name: "Subcategory")
  String? subcategory;
  @JsonKey(name: "Url")
  String? url;
  @JsonKey(name: "BadgeWeek")
  int? badgeWeek;
  @JsonKey(name: "Error")
  String? error;
  @JsonKey(name: "Success")
  bool? success;

  BadgeModel({
    this.id,
    this.createdDate,
    this.title,
    this.supplierId,
    this.supplierName,
    this.badgeImg,
    this.coverImg,
    this.badgeType,
    this.badgeYear,
    this.industry,
    this.area,
    this.point,
    this.position,
    this.badgeMonth,
    this.isOk,
    this.averageRating,
    this.totalRating,
    this.authorizationNo,
    this.subcategoryId,
    this.subcategory,
    this.url,
    this.badgeWeek,
    this.error,
    this.success,
  });

  factory BadgeModel.fromJson(Map<String, dynamic> json) =>
      _$BadgeModelFromJson(json);

  Map<String, dynamic> toJson() => _$BadgeModelToJson(this);
}
