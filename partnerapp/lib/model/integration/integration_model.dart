import 'package:json_annotation/json_annotation.dart';

part 'integration_model.g.dart';

@JsonSerializable()
class IntegrationModel {
  IntegrationModel({
    this.id,
    this.levId,
    this.accessKey,
    this.organizationId,
    this.thirdPartName,
    this.accessToken,
    this.expireDate,
    this.isEnable,
    this.success,
    this.createDate,
    this.isFirstTime,
    this.lastViewDate,
    this.updateWeek,
    this.updateYear,
    this.manualUpdateDate,
    this.isNotWorking,
    this.totalClient,
    this.smsStatus,
    this.hvdkVIP,
    this.lastUpdated,
  });

  IntegrationModel.defaults()
      : id = 0,
        levId = 0,
        accessKey = '',
        organizationId = '',
        thirdPartName = '',
        accessToken = '',
        expireDate = '',
        isEnable = 0,
        success = 0,
        createDate = '',
        isFirstTime = 0,
        lastViewDate = '',
        updateWeek = 0,
        updateYear = 0,
        manualUpdateDate = '',
        isNotWorking = 0,
        totalClient = 0,
        smsStatus = 0,
        hvdkVIP = 0,
        lastUpdated = '';

  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'LEV_ID')
  final int? levId;
  @JsonKey(name: 'ACCESS_KEY')
  final String? accessKey;
  @JsonKey(name: 'ORGANIZATION_ID')
  final String? organizationId;
  @JsonKey(name: 'THIRDPART_NAME')
  final String? thirdPartName;
  @JsonKey(name: 'ACCESS_TOKEN')
  final String? accessToken;
  @JsonKey(name: 'EXPIRE_DATE')
  final String? expireDate;
  @JsonKey(name: 'IS_ENABLE')
  final int? isEnable;
  @JsonKey(name: 'SUCCESS')
  final int? success;
  @JsonKey(name: 'CREATE_DATE')
  final String? createDate;
  @JsonKey(name: 'IS_FIRST_TIME')
  final int? isFirstTime;
  @JsonKey(name: 'LAST_VIEW_DATE')
  final String? lastViewDate;
  @JsonKey(name: 'UPDATE_WEEK')
  final int? updateWeek;
  @JsonKey(name: 'UPDATE_YEAR')
  final int? updateYear;
  @JsonKey(name: 'MANUAL_UPDATE_DATE')
  final String? manualUpdateDate;
  @JsonKey(name: 'IS_NOT_WORKING')
  final int? isNotWorking;
  @JsonKey(name: 'TOTAL_CLIENT')
  final int? totalClient;
  @JsonKey(name: 'SMS_STATUS')
  final int? smsStatus;
  @JsonKey(name: 'HVDK_VIP')
  final int? hvdkVIP;
  @JsonKey(name: 'LAST_UPDATED')
  final String? lastUpdated;

  factory IntegrationModel.fromJson(Map<String, dynamic> json) =>
      _$IntegrationModelFromJson(json);

  Map<String, dynamic> toJson() => _$IntegrationModelToJson(this);

  static List<IntegrationModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => IntegrationModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
