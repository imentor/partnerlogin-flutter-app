import 'package:json_annotation/json_annotation.dart';

part 'deficiency_model.g.dart';

@JsonSerializable()
class DeficiencyObject {
  @JsonKey(name: 'preApproved')
  bool? preApproved;
  @JsonKey(name: 'approved')
  bool? approved;
  @JsonKey(name: 'lastUpdatedBy')
  String? lastUpdatedBy;
  @JsonKey(name: 'deficiency')
  List<DeficiencyModel?>? deficiencyList;

  DeficiencyObject(
      {this.approved,
      this.deficiencyList,
      this.lastUpdatedBy,
      this.preApproved});

  factory DeficiencyObject.fromJson(Map<String, dynamic> json) =>
      _$DeficiencyObjectFromJson(json);

  Map<String, dynamic> toJson() => _$DeficiencyObjectToJson(this);

  static List<DeficiencyObject> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => DeficiencyObject.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class DeficiencyModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'PROJECT_ID')
  int? projectId;
  @JsonKey(name: 'PARTNER')
  int? partner;
  @JsonKey(name: 'CONTACT')
  int? contact;
  @JsonKey(name: 'CREATED_BY')
  String? createdBy;
  @JsonKey(name: 'TITLE')
  String? title;
  @JsonKey(name: 'STATUS')
  String? status;
  @JsonKey(name: 'INDUSTRY')
  String? industry;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'END_DATE')
  String? endDate;
  @JsonKey(name: 'IMAGE_FILE_ID')
  int? imageFileId;
  @JsonKey(name: 'IMAGES')
  List<DeficiencyImage?>? images;
  @JsonKey(name: 'DATE_CREATED')
  String? dateCreated;
  @JsonKey(name: 'DATE_UPDATED')
  String? dateUpdated;
  @JsonKey(name: 'COMMENTS')
  List<DeficiencyComment?>? comments;

  DeficiencyModel(
      {this.id,
      this.contact,
      this.createdBy,
      this.dateCreated,
      this.dateUpdated,
      this.description,
      this.endDate,
      this.imageFileId,
      this.images,
      this.industry,
      this.partner,
      this.projectId,
      this.status,
      this.title});

  factory DeficiencyModel.fromJson(Map<String, dynamic> json) =>
      _$DeficiencyModelFromJson(json);

  Map<String, dynamic> toJson() => _$DeficiencyModelToJson(this);

  static List<DeficiencyModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => DeficiencyModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class DeficiencyComment {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'COMMENT')
  String? comment;
  @JsonKey(name: 'DATE_CREATED')
  String? dateCreated;
  @JsonKey(name: 'CREATED_BY')
  DeficiencyUser? createdBy;

  DeficiencyComment({this.id, this.comment, this.createdBy, this.dateCreated});

  factory DeficiencyComment.fromJson(Map<String, dynamic> json) =>
      _$DeficiencyCommentFromJson(json);

  Map<String, dynamic> toJson() => _$DeficiencyCommentToJson(this);

  static List<DeficiencyComment> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => DeficiencyComment.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class DeficiencyUser {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'COMPANY')
  String? company;
  @JsonKey(name: 'AVATAR')
  String? avatar;
  @JsonKey(name: 'MOBILE')
  String? mobile;
  @JsonKey(name: 'EMAIL')
  String? email;

  DeficiencyUser(
      {this.id, this.avatar, this.company, this.email, this.mobile, this.name});

  factory DeficiencyUser.fromJson(Map<String, dynamic> json) =>
      _$DeficiencyUserFromJson(json);

  Map<String, dynamic> toJson() => _$DeficiencyUserToJson(this);

  static List<DeficiencyUser> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => DeficiencyUser.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class DeficiencyImage {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'IMAGE_ID')
  int? imageId;
  @JsonKey(name: 'IMAGE_LINK')
  String? imageLink;
  @JsonKey(name: 'DATE_CREATED')
  String? dateCreated;

  DeficiencyImage({this.id, this.dateCreated, this.imageId, this.imageLink});

  factory DeficiencyImage.fromJson(Map<String, dynamic> json) =>
      _$DeficiencyImageFromJson(json);

  Map<String, dynamic> toJson() => _$DeficiencyImageToJson(this);

  static List<DeficiencyImage> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => DeficiencyImage.fromJson(i as Map<String, dynamic>))
      .toList();
}
