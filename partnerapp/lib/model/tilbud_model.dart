import 'package:json_annotation/json_annotation.dart';

part 'tilbud_model.g.dart';

@JsonSerializable()
class TilbudModel {
  @JsonKey(name: '0')
  String? s0;
  @JsonKey(name: '1')
  String? s1;
  @JsonKey(name: '2')
  String? s2;
  @JsonKey(name: '3')
  String? s3;
  @JsonKey(name: '4')
  String? s4;
  @JsonKey(name: '5')
  String? s5;
  @JsonKey(name: '6')
  String? s6;
  @JsonKey(name: '7')
  String? s7;
  @JsonKey(name: '8')
  String? s8;
  @JsonKey(name: '9')
  String? s9;
  @JsonKey(name: '10')
  String? s10;
  @JsonKey(name: '11')
  String? s11;
  @JsonKey(name: '12')
  String? s12;
  @JsonKey(name: '13')
  String? s13;
  @JsonKey(name: '14')
  String? s14;
  @JsonKey(name: '15')
  String? s15;
  @JsonKey(name: '16')
  String? s16;
  @JsonKey(name: '17')
  String? s17;
  @JsonKey(name: '18')
  String? s18;
  @JsonKey(name: '19')
  String? s19;
  @JsonKey(name: '20')
  String? s20;
  @JsonKey(name: '21')
  String? s21;
  @JsonKey(name: '22')
  String? s22;
  @JsonKey(name: '23')
  String? s23;
  @JsonKey(name: '24')
  String? s24;
  @JsonKey(name: '25')
  String? s25;
  @JsonKey(name: '26')
  String? s26;
  String? id;
  String? name;
  String? pid;
  String? appid;
  String? cid;
  @JsonKey(name: 'is_darft')
  dynamic isDraft;
  List<String>? files;
  Info? info;
  String? sent;
  String? createdtime;
  String? condition;
  String? deleted;
  String? image;
  dynamic notat;
  dynamic betingelser;
  dynamic bilag;
  @JsonKey(name: 'header_text')
  dynamic headerText;
  @JsonKey(name: 'header_image')
  dynamic headerImage;
  dynamic recommendations;
  dynamic tilbudId;
  dynamic firstname;
  dynamic lastname;
  dynamic mobile;
  String? referenceNumber;
  String? bilagfile;
  String? offerfile;
  String? imgfile;

  TilbudModel(
      {this.s0,
      this.s1,
      this.s2,
      this.s3,
      this.s4,
      this.s5,
      this.s6,
      this.s7,
      this.s8,
      this.s9,
      this.s10,
      this.s11,
      this.s12,
      this.s13,
      this.s14,
      this.s15,
      this.s16,
      this.s17,
      this.s18,
      this.s19,
      this.s20,
      this.s21,
      this.s22,
      this.s23,
      this.s24,
      this.s25,
      this.id,
      this.name,
      this.pid,
      this.appid,
      this.cid,
      this.isDraft,
      this.files,
      this.info,
      this.sent,
      this.createdtime,
      this.condition,
      this.deleted,
      this.image,
      this.notat,
      this.betingelser,
      this.bilag,
      this.headerText,
      this.headerImage,
      this.recommendations,
      this.referenceNumber,
      this.bilagfile,
      this.offerfile,
      this.imgfile,
      this.tilbudId,
      this.firstname,
      this.lastname,
      this.mobile});

  factory TilbudModel.fromJson(Map<String, dynamic> json) {
    return _$TilbudModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$TilbudModelToJson(this);
}

@JsonSerializable()
class Info {
  String? id;
  @JsonKey(name: 'customer_id')
  dynamic customerId;
  @JsonKey(name: 'currency_id')
  dynamic currencyId;
  String? date;
  @JsonKey(name: 'expiry_date')
  String? expiryDate;
  @JsonKey(name: 'expiry_date_detail')
  String? expiryDateDetail;
  @JsonKey(name: 'upload_token')
  String? uploadToken;
  @JsonKey(name: 'itme_type')
  String? itemType;
  List<Rows?>? rows;
  String? subject;
  String? description;
  dynamic subtotal;
  dynamic subtotalDiscount;
  @JsonKey(name: 'line_item_discount_type')
  bool? lineItemDiscountType;
  @JsonKey(name: 'subtotalDiscount_type')
  bool? subtotalDiscountType;
  @JsonKey(name: 'subtotal_tax')
  dynamic subtotaltax;
  dynamic subtotalShippingAndHandling;
  double? total;
  @JsonKey(name: 'reference_number')
  String? referenceNumber;
  @JsonKey(name: 'quotation_number')
  String? quotationNumber;
  @JsonKey(name: 'quotation_prefix')
  String? quotationPrefix;
  @JsonKey(name: 'is_draft')
  dynamic isDraft;
  @JsonKey(name: 'show_sub_total_tax')
  bool? showSubTotalTax;
  @JsonKey(name: 'show_sub_total_discount')
  bool? showSubTotalDiscount;
  @JsonKey(name: 'show_sub_total_shipping_and_handling')
  bool? showSubTotalShippingAndHandling;
  @JsonKey(name: 'show_line_item_tax')
  bool? showLineItemTax;
  @JsonKey(name: 'show_line_item_discount')
  bool? showLineItemDiscount;
  @JsonKey(name: 'show_line_item_description')
  bool? showLineItemDescription;
  @JsonKey(name: 'customer_note')
  String? customerNote;
  String? memo;
  String? tnc;
  dynamic subtotalTax;

  Info({
    this.id,
    this.customerId,
    this.currencyId,
    this.date,
    this.expiryDate,
    this.expiryDateDetail,
    this.uploadToken,
    this.itemType,
    this.rows,
    this.subject,
    this.description,
    this.subtotal,
    this.subtotalDiscount,
    this.lineItemDiscountType,
    this.subtotalDiscountType,
    this.subtotaltax,
    this.subtotalShippingAndHandling,
    this.total,
    this.referenceNumber,
    this.quotationNumber,
    this.quotationPrefix,
    this.isDraft,
    this.showSubTotalTax,
    this.showSubTotalDiscount,
    this.showSubTotalShippingAndHandling,
    this.showLineItemTax,
    this.showLineItemDiscount,
    this.showLineItemDescription,
    this.customerNote,
    this.memo,
    this.tnc,
    this.subtotalTax,
  });

  factory Info.fromJson(Map<String, dynamic> json) {
    if (json['quotation_number'] != null) {
      json['quotation_number'] = json['quotation_number'] is String
          ? json['quotation_number']
          : json['quotation_number'].toString();
    }

    if (json['quotation_prefix'] != null) {
      json['quotation_prefix'] = json['quotation_prefix'] is String
          ? json['quotation_prefix']
          : json['quotation_prefix'].toString();
    }
    if (json['reference_number'] != null) {
      json['reference_number'] = json['reference_number'] is String
          ? json['reference_number']
          : json['reference_number'].toString();
    }

    return _$InfoFromJson(json);
  }

  Map<String, dynamic> toJson() => _$InfoToJson(this);
}

@JsonSerializable()
class Rows {
  String? uuid;
  String? name;
  @JsonKey(name: 'selected_item')
  dynamic selectedItem;
  @JsonKey(name: 'show_custom_item')
  bool? showCustomItem;
  @JsonKey(name: 'custom_name')
  String? customName;
  dynamic quantity;
  double? unitPrice;
  double? discount;
  double? tax;
  double? type;
  double? amount;
  String? description;

  Rows(
      {this.uuid,
      this.name,
      this.selectedItem,
      this.showCustomItem,
      this.customName,
      this.quantity,
      this.unitPrice,
      this.discount,
      this.tax,
      this.type,
      this.amount,
      this.description});

  factory Rows.fromJson(Map<String, dynamic> json) => _$RowsFromJson(json);

  Map<String, dynamic> toJson() => _$RowsToJson(this);
}
