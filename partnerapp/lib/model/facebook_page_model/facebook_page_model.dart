import 'package:json_annotation/json_annotation.dart';

part 'facebook_page_model.g.dart';

@JsonSerializable()
class FacebookPageModel {
  FacebookPageModel(
      {this.accessToken,
      this.id,
      this.isDeleted,
      this.pageId,
      this.pageName,
      this.partnerId,
      this.postReview,
      this.thirdPartName,
      this.userName});

  @JsonKey(name: 'ID')
  final int? id;

  @JsonKey(name: 'PARTNER_ID')
  final String? partnerId;

  @JsonKey(name: 'USER_NAME')
  final String? userName;

  @JsonKey(name: 'THIRDPART_NAME')
  final String? thirdPartName;

  @JsonKey(name: 'PAGE_ACCESS_TOKEN')
  final String? accessToken;

  @JsonKey(name: 'PAGE_ID')
  final String? pageId;

  @JsonKey(name: 'PAGE_NAME')
  final String? pageName;

  @JsonKey(name: 'POST_REVIEW')
  final int? postReview;

  @JsonKey(name: 'IS_DELETED')
  final int? isDeleted;

  factory FacebookPageModel.fromJson(Map<String, dynamic> json) =>
      _$FacebookPageModelFromJson(json);

  Map<String, dynamic> toJson() => _$FacebookPageModelToJson(this);

  static List<FacebookPageModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => FacebookPageModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
