import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'offer_model.g.dart';

@JsonSerializable()
class OfferModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'TITLE')
  String? title;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'IMAGE_URL')
  String? imageUrl;
  @JsonKey(name: 'STATUS')
  String? status;
  @JsonKey(name: 'CONTACT_ID')
  int? contactId;
  @JsonKey(name: 'PROJECT_ID')
  int? projectId;
  @JsonKey(name: 'LEAD_ID')
  int? leadId;
  @JsonKey(name: 'TOTAL_PRICE')
  double? totalPrice;
  @JsonKey(name: 'SUB_TOTAL_PRICE')
  double? subtotalPrice;
  @JsonKey(name: 'VAT')
  double? vat;
  @JsonKey(name: 'COMPANY_NAME')
  String? companyName;
  @JsonKey(name: 'COMPANY_CVR')
  String? companyCvr;
  @JsonKey(name: 'COMPANY_EMAIL')
  String? companyEmail;
  @JsonKey(name: 'IS_PARTNER')
  bool? isPartner;
  @JsonKey(name: 'CREATED_AT')
  String? createdAt;
  @JsonKey(name: 'UPDATED_AT')
  String? updatedAt;
  @JsonKey(name: 'OFFER_FILE')
  String? offerFile;
  @JsonKey(name: 'TOTAL_VAT_FEE')
  double? totalVatFee;
  @JsonKey(name: 'OFFER_TYPE')
  String? offerType;
  @JsonKey(name: 'HOMEOWNER')
  Homeowner? homeowner;
  @JsonKey(name: 'CONTRACT_VERSIONS')
  List<ContractVersionModel>? contractVersions;
  @JsonKey(name: 'CONTRACT_TYPE')
  String? contractType;
  @JsonKey(name: 'PARENT_PROJECT_ID')
  int? parentProjectId;
  @JsonKey(name: 'EXPIRY')
  int? expiry;
  @JsonKey(name: 'TOTAL_VAT')
  double? totalVat;
  @JsonKey(name: 'LAST_UPDATED_BY')
  String? lastUpdatedBy;
  @JsonKey(name: 'CREATED_BY')
  String? createdBy;
  @JsonKey(name: 'CONTRACT_ID')
  int? contractId;
  @JsonKey(name: 'START_DATE')
  String? startDate;

  OfferModel({
    this.companyCvr,
    this.companyEmail,
    this.companyName,
    this.contactId,
    this.createdAt,
    this.description,
    this.id,
    this.imageUrl,
    this.isPartner,
    this.leadId,
    this.projectId,
    this.status,
    this.subtotalPrice,
    this.title,
    this.totalPrice,
    this.updatedAt,
    this.vat,
    this.offerFile,
    this.totalVatFee,
    this.offerType,
    this.homeowner,
    this.contractVersions,
    this.contractType,
    this.parentProjectId,
    this.expiry,
    this.totalVat,
    this.lastUpdatedBy,
    this.createdBy,
    this.contractId,
    this.startDate,
  });

  factory OfferModel.fromJson(Map<String, dynamic> json) =>
      _$OfferModelFromJson(json);

  Map<String, dynamic> toJson() => _$OfferModelToJson(this);

  static List<OfferModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => OfferModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class OfferFiles {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'URL')
  String? url;

  OfferFiles({this.id, this.name, this.url});

  factory OfferFiles.fromJson(Map<String, dynamic> json) =>
      _$OfferFilesFromJson(json);

  Map<String, dynamic> toJson() => _$OfferFilesToJson(this);

  static List<OfferFiles> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => OfferFiles.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class Homeowner {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'AVATAR')
  String? avatar;
  @JsonKey(name: 'COMPANY')
  String? company;
  @JsonKey(name: 'EMAIL')
  String? email;
  @JsonKey(name: 'MOBILE')
  String? mobile;
  @JsonKey(name: 'NAME')
  String? name;

  Homeowner({
    this.id,
    this.avatar,
    this.company,
    this.email,
    this.mobile,
    this.name,
  });

  factory Homeowner.fromJson(Map<String, dynamic> json) =>
      _$HomeownerFromJson(json);

  Map<String, dynamic> toJson() => _$HomeownerToJson(this);
  static List<Homeowner> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => Homeowner.fromJson(i as Map<String, dynamic>))
      .toList();
}
