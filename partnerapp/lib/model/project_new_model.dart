import 'package:json_annotation/json_annotation.dart';

part 'project_new_model.g.dart';

@JsonSerializable()
class ProjectNewModel {
  List<Projects>? projects;
  String? url;

  ProjectNewModel({this.projects, this.url});

  factory ProjectNewModel.fromJson(Map<String, dynamic> json) =>
      _$ProjectNewModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectNewModelToJson(this);
}

@JsonSerializable()
class Projects {
  @JsonKey(name: '0')
  dynamic s0;
  @JsonKey(name: '1')
  dynamic s1;
  @JsonKey(name: '2')
  dynamic s2;
  @JsonKey(name: '3')
  String? s3;
  @JsonKey(name: '4')
  String? s4;
  @JsonKey(name: '5')
  String? s5;
  @JsonKey(name: '6')
  String? s6;
  @JsonKey(name: '7')
  String? s7;
  @JsonKey(name: '8')
  String? s8;
  @JsonKey(name: '9')
  String? s9;
  dynamic id;
  dynamic lavid;
  dynamic pid;
  String? projectid;
  String? title;
  String? description;
  String? timeadd;
  String? creatorlevid;
  String? company;
  String? email;
  @JsonKey(name: 'IsDeleted')
  bool? isDeleted;
  @JsonKey(name: 'IsActive')
  bool? isActive;
  List<Images>? images;
  List<dynamic>? videos;

  Projects(
      {this.s0,
      this.s1,
      this.s2,
      this.s3,
      this.s4,
      this.s5,
      this.s6,
      this.s7,
      this.s8,
      this.s9,
      this.id,
      this.lavid,
      this.pid,
      this.projectid,
      this.title,
      this.description,
      this.timeadd,
      this.creatorlevid,
      this.company,
      this.email,
      this.images,
      this.videos,
      this.isActive,
      this.isDeleted});

  factory Projects.fromJson(Map<String, dynamic> json) {
    if (json.containsKey('projectid')) {
      json['projectid'] = json['projectid'] is int
          ? json['projectid'].toString()
          : json['projectid'];
    }

    return _$ProjectsFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ProjectsToJson(this);
}

@JsonSerializable()
class Images {
  int? id;
  String? image;
  @JsonKey(name: 'ImageOrder')
  int? imageOrder;
  bool? isPrimary;

  Images({this.id, this.image, this.imageOrder, this.isPrimary});

  factory Images.fromJson(Map<String, dynamic> json) => _$ImagesFromJson(json);

  Map<String, dynamic> toJson() => _$ImagesToJson(this);
}
