import 'package:json_annotation/json_annotation.dart';

part 'autocomplete_model.g.dart';

@JsonSerializable()
class AutoCompleteAddress {
  @JsonKey(name: 'data')
  AutoCompleteData? data;
  @JsonKey(name: 'stormodtagerpostnr')
  bool? stormodtagerpostnr;
  @JsonKey(name: 'type')
  String? type;
  @JsonKey(name: 'tekst')
  String? tekst;
  @JsonKey(name: 'forslagstekst')
  String? forslagstekst;
  @JsonKey(name: 'caretpos')
  int? caretpos;
  @JsonKey(name: 'x')
  double? x;
  @JsonKey(name: 'y')
  double? y;

  AutoCompleteAddress({
    this.caretpos,
    this.data,
    this.forslagstekst,
    this.stormodtagerpostnr,
    this.tekst,
    this.type,
    this.x,
    this.y,
  });

  factory AutoCompleteAddress.fromJson(Map<String, dynamic> json) =>
      _$AutoCompleteAddressFromJson(json);

  Map<String, dynamic> toJson() => _$AutoCompleteAddressToJson(this);

  static List<AutoCompleteAddress> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => AutoCompleteAddress.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class AutoCompleteData {
  @JsonKey(name: 'id')
  String? id;
  @JsonKey(name: 'status')
  int? status;
  @JsonKey(name: 'darstatus')
  int? darstatus;
  @JsonKey(name: 'vejkode')
  String? vejkode;
  @JsonKey(name: 'vejnavn')
  String? vejnavn;
  @JsonKey(name: 'adresseringsvejnavn')
  String? adresseringsvejnavn;
  @JsonKey(name: 'husnr')
  String? husnr;
  @JsonKey(name: 'etage')
  dynamic etage;
  @JsonKey(name: 'dør')
  dynamic dor;
  @JsonKey(name: 'supplerendebynavn')
  dynamic supplerendebynavn;
  @JsonKey(name: 'postnr')
  String? postnr;
  @JsonKey(name: 'postnrnavn')
  String? postnrnavn;
  @JsonKey(name: 'stormodtagerpostnr')
  dynamic stormodtagerpostnr;
  @JsonKey(name: 'stormodtagerpostnrnavn')
  dynamic stormodtagerpostnrnavn;
  @JsonKey(name: 'kommunekode')
  String? kommunekode;
  @JsonKey(name: 'adgangsadresseid')
  String? adgangsadresseid;
  @JsonKey(name: 'x')
  double? x;
  @JsonKey(name: 'y')
  double? y;
  @JsonKey(name: 'href')
  String? href;

  AutoCompleteData(
      {this.adgangsadresseid,
      this.adresseringsvejnavn,
      this.darstatus,
      this.dor,
      this.etage,
      this.href,
      this.husnr,
      this.id,
      this.kommunekode,
      this.postnr,
      this.postnrnavn,
      this.status,
      this.stormodtagerpostnr,
      this.stormodtagerpostnrnavn,
      this.supplerendebynavn,
      this.vejkode,
      this.vejnavn,
      this.x,
      this.y});

  factory AutoCompleteData.fromJson(Map<String, dynamic> json) =>
      _$AutoCompleteDataFromJson(json);

  Map<String, dynamic> toJson() => _$AutoCompleteDataToJson(this);

  static List<AutoCompleteData> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => AutoCompleteData.fromJson(i as Map<String, dynamic>))
          .toList();
}
