import 'package:json_annotation/json_annotation.dart';

part 'terms_response_model.g.dart';

@JsonSerializable()
class TermsResponseModel {
  String? id;
  String? title;
  String? description;
  String? pid;
  String? def;
  String? deleted;
  String? type;

  TermsResponseModel(
      {this.id,
      this.title,
      this.description,
      this.pid,
      this.def,
      this.deleted,
      this.type});

  factory TermsResponseModel.fromJson(Map<String, dynamic> json) =>
      _$TermsResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$TermsResponseModelToJson(this);
}
