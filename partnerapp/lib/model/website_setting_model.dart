import 'package:json_annotation/json_annotation.dart';

part 'website_setting_model.g.dart';

@JsonSerializable()
class WebsiteSettingModel {
  String? vtemplate;
  Settings? settings;
  Domain? domain;
  String? emaildomain;
  List<EmailInfo>? emails;
  String? image;
  String? url;
  List<Pages>? pages;
  List<Sort>? sort;

  dynamic title;
  List<Template>? templates;

  WebsiteSettingModel({
    this.vtemplate,
    this.settings,
    this.domain,
    this.emaildomain,
    this.emails,
    this.image,
    this.url,
    this.pages,
    this.sort,
    this.title,
    this.templates,
  });

  factory WebsiteSettingModel.fromJson(Map<String, dynamic> json) =>
      _$WebsiteSettingModelFromJson(json);

  Map<String, dynamic> toJson() => _$WebsiteSettingModelToJson(this);
}

@JsonSerializable()
class Settings {
  @JsonKey(name: 'Title')
  String? title;
  @JsonKey(name: 'Description')
  String? description;
  @JsonKey(name: 'Keywords')
  String? keywords;
  String? facebook;
  String? twitter;

  Settings(
      {this.title,
      this.description,
      this.keywords,
      this.facebook,
      this.twitter});

  factory Settings.fromJson(Map<String, dynamic> json) =>
      _$SettingsFromJson(json);

  Map<String, dynamic> toJson() => _$SettingsToJson(this);
}

@JsonSerializable()
class Domain {
  String? domain;
  List<dynamic>? services;
  String? dkuser;
  String? dkpassword;
  String? email1;
  String? email2;

  Domain(
      {this.domain,
      this.services,
      this.dkuser,
      this.dkpassword,
      this.email1,
      this.email2});

  factory Domain.fromJson(Map<String, dynamic> json) => _$DomainFromJson(json);

  Map<String, dynamic> toJson() => _$DomainToJson(this);
}

@JsonSerializable()
class EmailInfo {
  @JsonKey(name: 'recovery_email')
  final String? recoveryEmail;
  @JsonKey(name: 'storagequota_total')
  final num? storagequotaTotal;
  @JsonKey(name: 'storagequota_used')
  final num? storagequotaUsed;
  final String? username;

  EmailInfo(
      {this.recoveryEmail,
      this.storagequotaTotal,
      this.storagequotaUsed,
      this.username});

  factory EmailInfo.fromJson(Map<String, dynamic> json) =>
      _$EmailInfoFromJson(json);
  Map<String, dynamic> toJson() => _$EmailInfoToJson(this);
}

@JsonSerializable()
class Pages {
  @JsonKey(name: '0')
  String? s0;
  @JsonKey(name: '1')
  String? s1;
  @JsonKey(name: '2')
  String? s2;
  @JsonKey(name: '3')
  String? s3;
  @JsonKey(name: '4')
  String? s4;
  @JsonKey(name: '5')
  String? s5;
  @JsonKey(name: '6')
  String? s6;
  @JsonKey(name: '7')
  String? s7;
  @JsonKey(name: '8')
  String? s8;
  @JsonKey(name: '9')
  String? s9;
  String? id;
  String? levid;
  String? title;
  String? note;
  String? image;
  bool? active;
  String? page;
  String? deleted;
  String? sort;
  String? level;

  Pages(
      {this.s0,
      this.s1,
      this.s2,
      this.s3,
      this.s4,
      this.s5,
      this.s6,
      this.s7,
      this.s8,
      this.s9,
      this.id,
      this.levid,
      this.title,
      this.note,
      this.image,
      this.active,
      this.page,
      this.deleted,
      this.sort,
      this.level});

  factory Pages.fromJson(Map<String, dynamic> json) => _$PagesFromJson(json);

  Map<String, dynamic> toJson() => _$PagesToJson(this);
}

@JsonSerializable()
class Sort {
  int? level;
  int? sort;
  int? id;
  String? label;

  Sort({this.level, this.sort, this.id, this.label});

  factory Sort.fromJson(Map<String, dynamic> json) => _$SortFromJson(json);

  Map<String, dynamic> toJson() => _$SortToJson(this);
}

@JsonSerializable()
@JsonSerializable()
class Template {
  Template({this.id, this.img});

  String? id;
  String? img;

  factory Template.fromJson(Map<String, dynamic> json) =>
      _$TemplateFromJson(json);

  Map<String, dynamic> toJson() => _$TemplateToJson(this);
}
