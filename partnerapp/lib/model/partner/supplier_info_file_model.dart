import 'package:json_annotation/json_annotation.dart';

part 'supplier_info_file_model.g.dart';

@JsonSerializable()
class SupplierInfoFileModel {
  SupplierInfoFileModel({
    this.fullURL,
    this.id,
    this.isDeleted,
    this.sort,
  });

  @JsonKey(name: 'FULLURL')
  final String? fullURL;
  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'IS_DELETED')
  final int? isDeleted;
  @JsonKey(name: 'SORT')
  final int? sort;

  SupplierInfoFileModel.defaults()
      : fullURL = '',
        id = 0,
        isDeleted = 0,
        sort = 0;

  factory SupplierInfoFileModel.fromJson(Map<String, dynamic> json) =>
      _$SupplierInfoFileModelFromJson(json);

  Map<String, dynamic> toJson() => _$SupplierInfoFileModelToJson(this);
  static List<SupplierInfoFileModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => SupplierInfoFileModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
