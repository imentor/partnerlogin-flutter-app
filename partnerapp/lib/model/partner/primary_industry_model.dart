import 'package:json_annotation/json_annotation.dart';

part 'primary_industry_model.g.dart';

@JsonSerializable()
class PrimaryIndustry {
  PrimaryIndustry(
      {this.activated,
      this.branchId,
      this.business,
      this.businessTypeUi,
      this.directoryMaxDistance,
      this.id,
      this.lockInFrontEnd,
      this.userFriendlyName});

  @JsonKey(name: 'ID')
  final dynamic id;
  @JsonKey(name: 'BRANCH_ID')
  final dynamic branchId;
  @JsonKey(name: 'BUSINESS')
  final dynamic business;
  @JsonKey(name: 'ACTIVATED')
  final dynamic activated;
  @JsonKey(name: 'BUSINESS_TYPE_UI')
  final dynamic businessTypeUi;
  @JsonKey(name: 'USER_FRIENDLY_NAME')
  final dynamic userFriendlyName;
  @JsonKey(name: 'DIRECTORY_MAX_DISTANCE')
  final dynamic directoryMaxDistance;
  @JsonKey(name: 'LOCK_IN_FRONTEND')
  final dynamic lockInFrontEnd;

  factory PrimaryIndustry.fromJson(Map<String, dynamic> json) =>
      _$PrimaryIndustryFromJson(json);

  Map<String, dynamic> toJson() => _$PrimaryIndustryToJson(this);
  static List<PrimaryIndustry> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => PrimaryIndustry.fromJson(i as Map<String, dynamic>))
      .toList();
}
