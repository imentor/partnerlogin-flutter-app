import 'package:Haandvaerker.dk/model/packages/package_response_model.dart';
import 'package:Haandvaerker.dk/model/partner/primary_industry_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'partner_login_model.g.dart';

@JsonSerializable()
class PartnerLoginModel {
  PartnerLoginModel({
    this.info,
    this.user,
  });

  @JsonKey(name: 'info')
  final PartnerLoginModelInfo? info;
  @JsonKey(name: 'user')
  final PartnerUserModel? user;

  factory PartnerLoginModel.fromJson(Map<String, dynamic> json) =>
      _$PartnerLoginModelFromJson(json);

  Map<String, dynamic> toJson() => _$PartnerLoginModelToJson(this);

  static List<PartnerLoginModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => PartnerLoginModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class PartnerLoginModelInfo {
  PartnerLoginModelInfo({
    this.package,
    this.primaryIndustry,
    this.totalJobInvite,
    this.totalPrisen,
  });

  @JsonKey(name: 'package')
  final PackageData? package;
  @JsonKey(name: 'primary_industry')
  final PrimaryIndustry? primaryIndustry;
  @JsonKey(name: 'total_job_invite')
  final int? totalJobInvite;
  @JsonKey(name: 'total_prisen')
  final int? totalPrisen;

  factory PartnerLoginModelInfo.fromJson(Map<String, dynamic> json) =>
      _$PartnerLoginModelInfoFromJson(json);

  Map<String, dynamic> toJson() => _$PartnerLoginModelInfoToJson(this);

  static List<PartnerLoginModelInfo> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => PartnerLoginModelInfo.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class PartnerUserModel {
  PartnerUserModel({
    this.address,
    this.city,
    this.coverUrl,
    this.cvr,
    this.email,
    this.id,
    this.latitude,
    this.longitude,
    this.mobile,
    this.name,
    this.package,
    this.payproffAccountId,
    this.payproffUserId,
    this.payproffVerified,
    this.picture,
    this.primaryIndustryId,
    this.region,
    this.token,
    this.zip,
  });

  @JsonKey(name: 'address')
  final String? address;
  @JsonKey(name: 'city')
  final String? city;
  @JsonKey(name: 'coverUrl')
  final String? coverUrl;
  @JsonKey(name: 'cvr')
  final String? cvr;
  @JsonKey(name: 'email')
  final String? email;
  @JsonKey(name: 'id')
  final int? id;
  @JsonKey(name: 'latitude')
  final double? latitude;
  @JsonKey(name: 'longitude')
  final double? longitude;
  @JsonKey(name: 'mobile')
  final String? mobile;
  @JsonKey(name: 'name')
  final String? name;
  @JsonKey(name: 'package')
  final String? package;
  @JsonKey(name: 'payproff_account_id')
  final String? payproffAccountId;
  @JsonKey(name: 'payproff_user_id')
  final String? payproffUserId;
  @JsonKey(name: 'payproff_verified')
  final bool? payproffVerified;
  @JsonKey(name: 'picture')
  final String? picture;
  @JsonKey(name: 'primary_industry_id')
  final int? primaryIndustryId;
  @JsonKey(name: 'region')
  final String? region;
  @JsonKey(name: 'token')
  final String? token;
  @JsonKey(name: 'zip')
  final String? zip;

  factory PartnerUserModel.fromJson(Map<String, dynamic> json) =>
      _$PartnerUserModelFromJson(json);

  Map<String, dynamic> toJson() => _$PartnerUserModelToJson(this);

  static List<PartnerUserModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => PartnerUserModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class OtherAccounts {
  OtherAccounts({
    this.company,
    this.cvr,
    this.email,
    this.id,
  });
  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'COMPANY')
  final String? company;
  @JsonKey(name: 'CVR')
  final String? cvr;
  @JsonKey(name: 'EMAIL')
  final String? email;

  factory OtherAccounts.fromJson(Map<String, dynamic> json) =>
      _$OtherAccountsFromJson(json);

  Map<String, dynamic> toJson() => _$OtherAccountsToJson(this);

  static List<OtherAccounts> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => OtherAccounts.fromJson(i as Map<String, dynamic>))
      .toList();
}
