import 'package:json_annotation/json_annotation.dart';

part 'other_badges_model.g.dart';

@JsonSerializable()
class OtherBadgesModel {
  OtherBadgesModel({
    this.id,
    this.type,
    this.month,
    this.week,
    this.year,
  });

  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'TYPE')
  final int? type;
  @JsonKey(name: 'WEEK')
  final int? week;
  @JsonKey(name: 'MONTH')
  final int? month;
  @JsonKey(name: 'YEAR')
  final int? year;

  factory OtherBadgesModel.fromJson(Map<String, dynamic> json) =>
      _$OtherBadgesModelFromJson(json);

  Map<String, dynamic> toJson() => _$OtherBadgesModelToJson(this);
  static List<OtherBadgesModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => OtherBadgesModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
