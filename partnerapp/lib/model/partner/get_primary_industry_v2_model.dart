import 'package:json_annotation/json_annotation.dart';

part 'get_primary_industry_v2_model.g.dart';

@JsonSerializable()
class GetPrimaryIndustryV2Model {
  GetPrimaryIndustryV2Model({
    this.pictureURL,
    this.branche,
  });

  @JsonKey(name: 'PICTURE_URL')
  final String? pictureURL;
  @JsonKey(name: 'BRANCHE')
  final String? branche;

  factory GetPrimaryIndustryV2Model.fromJson(Map<String, dynamic> json) =>
      _$GetPrimaryIndustryV2ModelFromJson(json);

  Map<String, dynamic> toJson() => _$GetPrimaryIndustryV2ModelToJson(this);
}
