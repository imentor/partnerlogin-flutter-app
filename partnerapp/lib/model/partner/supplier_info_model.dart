import 'package:Haandvaerker.dk/model/packages/package_response_model.dart';
import 'package:Haandvaerker.dk/model/partner/bitrix_company_model.dart';
import 'package:Haandvaerker.dk/model/partner/case_project_model.dart';
import 'package:Haandvaerker.dk/model/partner/get_primary_industry_v2_model.dart';
import 'package:Haandvaerker.dk/model/partner/other_badges_model.dart';
import 'package:Haandvaerker.dk/model/partner/primary_industry_model.dart';
import 'package:Haandvaerker.dk/model/partner/prisen_result_model.dart';
import 'package:Haandvaerker.dk/model/partner/supplier_info_file_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'supplier_info_model.g.dart';

@JsonSerializable()
class SupplierInfoModel {
  SupplierInfoModel({
    this.gallery,
    this.bCrmCompany,
    this.primaryIndustry,
    this.getPrimaryIndustryV2,
    this.videos,
    this.projects,
    this.cPartnerTaskProfile,
    this.cPrimaryIndustry,
    this.prisenResults,
    this.mergedBadges,
    this.otherBadges,
    this.supplierPackage,
  });

  @JsonKey(name: 'Gallery')
  final List<SupplierInfoFileModel>? gallery;

  @JsonKey(name: 'b_crm_company')
  final BitrixCompanyModel? bCrmCompany;

  @JsonKey(name: 'PrimaryIndustry')
  final String? primaryIndustry;

  @JsonKey(name: 'GetPrimaryIndustryV2')
  final GetPrimaryIndustryV2Model? getPrimaryIndustryV2;

  @JsonKey(name: 'Videos')
  final List<SupplierInfoFileModel>? videos;

  @JsonKey(name: 'PROJECTS')
  final List<CaseProjectModel>? projects;

  @JsonKey(name: 'c_partner_task_profile')
  List<int>? cPartnerTaskProfile;

  @JsonKey(name: 'c_primary_industry')
  PrimaryIndustry? cPrimaryIndustry;

  @JsonKey(name: 'c_prisen_result')
  final List<PrisenResult>? prisenResults;

  @JsonKey(name: 'mergedBadges')
  final List<PrisenResult>? mergedBadges;

  @JsonKey(name: 'BADGESOTHER')
  final List<OtherBadgesModel>? otherBadges;

  @JsonKey(name: 'supplier_package')
  final PackageData? supplierPackage;

  factory SupplierInfoModel.fromJson(Map<String, dynamic> json) =>
      _$SupplierInfoModelFromJson(json);

  Map<String, dynamic> toJson() => _$SupplierInfoModelToJson(this);
  static List<SupplierInfoModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => SupplierInfoModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
