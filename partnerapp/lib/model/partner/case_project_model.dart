import 'package:Haandvaerker.dk/model/partner/supplier_info_file_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'case_project_model.g.dart';

@JsonSerializable()
class CaseProjectModel {
  CaseProjectModel({
    this.id,
    this.amount,
    this.amountString,
    this.area,
    this.city,
    this.company,
    this.cost,
    this.createdBy,
    this.createdOn,
    this.currency,
    this.description,
    this.externalId,
    this.finishedOn,
    this.location,
    this.manufactureIds,
    this.name,
    this.pictures,
    this.projectId,
    this.projectURL,
    this.reviewId,
    this.stage,
    this.subTaskType,
    this.tags,
    this.taskTypeIds,
    this.taskTypeIdsText,
    this.taxTotal,
    this.time,
    this.title,
    this.updatedOn,
    this.year,
    this.zip,
  });

  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'AMOUNT')
  final String? amount;
  @JsonKey(name: 'AMOUNTSTRING')
  final String? amountString;
  @JsonKey(name: 'AREA')
  final String? area;
  @JsonKey(name: 'CITY')
  final String? city;
  @JsonKey(name: 'COMPANY')
  final int? company;
  @JsonKey(name: 'COST')
  final String? cost;
  @JsonKey(name: 'CREATED_BY')
  final int? createdBy;
  @JsonKey(name: 'CREATED_ON')
  final String? createdOn;
  @JsonKey(name: 'CURRENCY')
  final String? currency;
  @JsonKey(name: 'DESCRIPTION')
  final String? description;
  @JsonKey(name: 'EXTERNAL_ID')
  final String? externalId;
  @JsonKey(name: 'FINISHED_ON')
  final String? finishedOn;
  @JsonKey(name: 'LOCATION')
  final String? location;
  @JsonKey(name: 'MANUFACTUREIDS')
  final dynamic manufactureIds;
  @JsonKey(name: 'NAME')
  final String? name;
  @JsonKey(name: 'PICTURES')
  List<SupplierInfoFileModel>? pictures;
  @JsonKey(name: 'PROJECTID')
  final dynamic projectId;
  @JsonKey(name: 'PROJECTURL')
  final String? projectURL;
  @JsonKey(name: 'REVIEWID')
  final String? reviewId;
  @JsonKey(name: 'STAGE')
  final String? stage;
  @JsonKey(name: 'SUBTASKTYPE')
  final dynamic subTaskType;
  @JsonKey(name: 'TAGS')
  final dynamic tags;
  @JsonKey(name: 'TASKTYPEIDS')
  final dynamic taskTypeIds;
  @JsonKey(name: 'TASKTYPEIDSTXT')
  final dynamic taskTypeIdsText;
  @JsonKey(name: 'TAX_TOTAL')
  final String? taxTotal;
  @JsonKey(name: 'TIME')
  final String? time;
  @JsonKey(name: 'TITLE')
  final String? title;
  @JsonKey(name: 'UPDATED_ON')
  final String? updatedOn;
  @JsonKey(name: 'YEAR')
  final String? year;
  @JsonKey(name: 'ZIP')
  final String? zip;

  factory CaseProjectModel.fromJson(Map<String, dynamic> json) =>
      _$CaseProjectModelFromJson(json);

  Map<String, dynamic> toJson() => _$CaseProjectModelToJson(this);

  static List<CaseProjectModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => CaseProjectModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
