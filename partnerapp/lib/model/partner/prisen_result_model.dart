import 'package:json_annotation/json_annotation.dart';

part 'prisen_result_model.g.dart';

@JsonSerializable()
class PrisenResult {
  PrisenResult({
    this.area,
    this.averageRating,
    this.id,
    this.industryId,
    this.industryName,
    this.logo,
    this.place,
    this.points,
    this.prisenId,
    this.supplierId,
    this.totalReviews,
    this.year,
    this.supplierName,
  });

  PrisenResult.defaults()
      : area = null,
        averageRating = null,
        id = null,
        industryId = null,
        industryName = '',
        logo = null,
        place = null,
        points = null,
        prisenId = null,
        supplierId = null,
        totalReviews = null,
        year = null,
        supplierName = '';

  @JsonKey(name: 'ID')
  final dynamic id;
  @JsonKey(name: 'PRISEN_ID')
  final dynamic prisenId;
  @JsonKey(name: 'AREA')
  final dynamic area;
  @JsonKey(name: 'INDUSTRY_ID')
  final dynamic industryId;
  @JsonKey(name: 'INDUSTRY_NAME')
  final String? industryName;
  @JsonKey(name: 'SUPPLIER_ID')
  final dynamic supplierId;
  @JsonKey(name: 'POINTS')
  final dynamic points;
  @JsonKey(name: 'TOTAL_REVIEWS')
  final dynamic totalReviews;
  @JsonKey(name: 'AVERAGE_RATING')
  final dynamic averageRating;
  @JsonKey(name: 'LOGO')
  final dynamic logo;
  @JsonKey(name: 'PLACE')
  final dynamic place;
  @JsonKey(name: 'YEAR')
  final dynamic year;
  @JsonKey(name: 'SUPPLIER_NAME')
  final String? supplierName;

  factory PrisenResult.fromJson(Map<String, dynamic> json) =>
      _$PrisenResultFromJson(json);

  Map<String, dynamic> toJson() => _$PrisenResultToJson(this);
  static List<PrisenResult> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => PrisenResult.fromJson(i as Map<String, dynamic>))
      .toList();
}
