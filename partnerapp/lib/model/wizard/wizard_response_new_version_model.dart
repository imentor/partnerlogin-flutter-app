import 'package:json_annotation/json_annotation.dart';

part 'wizard_response_new_version_model.g.dart';

@JsonSerializable()
class FullIndustryV2 {
  @JsonKey(name: 'BRANCHE_ID')
  int? brancheId;
  @JsonKey(name: 'BRANCHE')
  String? brancheDa;
  @JsonKey(name: 'BRANCHE_EN')
  String? brancheEn;

  FullIndustryV2({
    this.brancheId,
    this.brancheDa,
    this.brancheEn,
  });

  FullIndustryV2.defaults()
      : brancheId = 0,
        brancheDa = '',
        brancheEn = '';

  factory FullIndustryV2.fromJson(Map<String, dynamic> json) =>
      _$FullIndustryV2FromJson(json);

  Map<String, dynamic> toJson() => _$FullIndustryV2ToJson(this);
  static List<FullIndustryV2> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => FullIndustryV2.fromJson(i as Map<String, dynamic>))
      .toList();
}

class EnterpriseIndustryV2 {
  String? enterpriseId;
  List<JobIndustryV2>? jobIndustries;

  EnterpriseIndustryV2({this.enterpriseId, this.jobIndustries});
}

class JobIndustryV2 {
  String? jobIndustryId;
  List<dynamic>? descriptions;

  JobIndustryV2({this.jobIndustryId, this.descriptions});
}

@JsonSerializable()
class AllWizardTextV2 {
  @JsonKey(name: "producttypeid")
  int? producttypeid;
  @JsonKey(name: "producttype")
  String? producttypeDa;
  @JsonKey(name: "producttype_en")
  String? producttypeEn;
  @JsonKey(name: "mpic")
  String? mpic;
  @JsonKey(name: "general_description")
  String? generalDescription;
  @JsonKey(name: "subindustry")
  String? subindustry;
  @JsonKey(name: "industry")
  String? industry;

  AllWizardTextV2({
    this.producttypeid,
    this.producttypeDa,
    this.producttypeEn,
    this.mpic,
    this.generalDescription,
    this.subindustry,
    this.industry,
  });

  AllWizardTextV2.defaults()
      : producttypeid = 0,
        producttypeDa = '',
        producttypeEn = '',
        mpic = '',
        generalDescription = '',
        subindustry = '',
        industry = '';

  factory AllWizardTextV2.fromJson(Map<String, dynamic> json) =>
      _$AllWizardTextV2FromJson(json);

  Map<String, dynamic> toJson() => _$AllWizardTextV2ToJson(this);
  static List<AllWizardTextV2> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => AllWizardTextV2.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class ProductWizardModelV2 {
  @JsonKey(name: "productId")
  String? productId;
  @JsonKey(name: "producttype")
  String? producttype;
  @JsonKey(name: "productFields")
  List<ProductWizardFieldV2>? productFields;

  ProductWizardModelV2({this.productId, this.producttype, this.productFields});

  factory ProductWizardModelV2.fromJson(Map<String, dynamic> json) =>
      _$ProductWizardModelV2FromJson(json);

  Map<String, dynamic> toJson() => _$ProductWizardModelV2ToJson(this);
  static List<ProductWizardModelV2> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => ProductWizardModelV2.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class ProductWizardFieldV2 {
  @JsonKey(name: 'id')
  String? id;
  @JsonKey(name: 'productname')
  String? productName;
  @JsonKey(name: 'productId')
  int? productId;
  @JsonKey(name: 'parentproductid')
  int? parentproductid;
  @JsonKey(name: 'description')
  String? description;
  @JsonKey(name: 'productfield')
  int? productField;
  @JsonKey(name: 'mandatory')
  int? mandatory;
  @JsonKey(name: 'wizardlead')
  int? wizardLead;
  @JsonKey(name: 'values')
  List<ProductWizardFieldV2>? values;
  @JsonKey(name: 'children')
  List<ProductWizardFieldV2>? children;
  @JsonKey(name: 'only_description')
  int? onlyDescription;

  ProductWizardFieldV2({
    this.id,
    this.children,
    this.description,
    this.mandatory,
    this.parentproductid,
    this.productField,
    this.productId,
    this.productName,
    this.values,
    this.wizardLead,
    this.onlyDescription,
  });

  factory ProductWizardFieldV2.fromJson(Map<String, dynamic> json) =>
      _$ProductWizardFieldV2FromJson(json);

  Map<String, dynamic> toJson() => _$ProductWizardFieldV2ToJson(this);

  static List<ProductWizardFieldV2> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => ProductWizardFieldV2.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class CreateOfferWizardModelV2 {
  @JsonKey(name: "ID")
  int? id;
  @JsonKey(name: "PARTNER_ID")
  int? partnerId;
  @JsonKey(name: "CONTACT_ID")
  int? contactId;
  @JsonKey(name: "ADDRESS_ID")
  int? addressId;
  @JsonKey(name: "PROJECT_ID")
  int? projectId;
  @JsonKey(name: "OFFER_ID")
  int? offerId;
  @JsonKey(name: "RESERVATION_ID")
  int? reservationId;
  @JsonKey(name: "STEP")
  int? step;
  @JsonKey(name: "STATUS")
  String? status;
  @JsonKey(name: "PROJECT_INFO")
  Map<String, dynamic>? projectInfo;
  @JsonKey(name: "CONTACT_INFO")
  Map<String, dynamic>? contactInfo;

  CreateOfferWizardModelV2({
    this.addressId,
    this.contactId,
    this.id,
    this.offerId,
    this.partnerId,
    this.projectId,
    this.reservationId,
    this.status,
    this.step,
    this.contactInfo,
    this.projectInfo,
  });

  factory CreateOfferWizardModelV2.fromJson(Map<String, dynamic> json) =>
      _$CreateOfferWizardModelV2FromJson(json);

  Map<String, dynamic> toJson() => _$CreateOfferWizardModelV2ToJson(this);
  static List<CreateOfferWizardModelV2> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => CreateOfferWizardModelV2.fromJson(i as Map<String, dynamic>))
      .toList();
}
