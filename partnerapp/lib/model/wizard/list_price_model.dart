import 'package:json_annotation/json_annotation.dart';

part 'list_price_model.g.dart';

@JsonSerializable()
class ListPrice {
  @JsonKey(name: 'productTypeId')
  String? productTypeId;
  @JsonKey(name: 'productName')
  String? productName;
  @JsonKey(name: 'productNameEn')
  String? productNameEn;
  @JsonKey(name: 'listPricesMinbolig')
  List<ListPriceMinbolig>? listPricesMinbolig;

  ListPrice(
      {this.listPricesMinbolig,
      this.productName,
      this.productTypeId,
      this.productNameEn});

  factory ListPrice.fromJson(Map<String, dynamic> json) =>
      _$ListPriceFromJson(json);

  Map<String, dynamic> toJson() => _$ListPriceToJson(this);

  static List<ListPrice> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => ListPrice.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class ListPriceMinbolig {
  @JsonKey(name: 'ID')
  String? id;
  @JsonKey(name: 'CONTACT_ID')
  String? contactId;
  @JsonKey(name: 'INDUSTRY')
  String? industry;
  @JsonKey(name: 'JOB_INDUSTRY')
  String? jobIndustry;
  @JsonKey(name: 'SERIES')
  dynamic series;
  @JsonKey(name: 'SERIES_ORIGINAL')
  List<String>? seriesOriginal;
  @JsonKey(name: 'PROJECT_ID')
  String? projectId;
  @JsonKey(name: 'SUB_PROJECT_ID')
  String? subProjectId;
  @JsonKey(name: 'OFFER_ID')
  String? offerId;
  @JsonKey(name: 'PARTNER_ID')
  String? partnerId;
  @JsonKey(name: 'TOTAL')
  String? total;
  @JsonKey(name: 'TOTALVAT')
  String? totalVat;
  @JsonKey(name: 'TOTAL_VAT_FEE')
  String? totalVatFee;
  @JsonKey(name: 'SERIES_TEXT')
  dynamic seriesText;
  @JsonKey(name: 'PRICES')
  Map<String, dynamic>? prices;
  @JsonKey(name: 'INDUSTRY_NAME')
  String? industryName;
  @JsonKey(name: 'NOTE')
  String? note;
  @JsonKey(name: 'SIGNATURE')
  String? signature;
  @JsonKey(name: 'WEEKS')
  String? weeks;
  @JsonKey(name: 'START_DATE')
  String? startDate;
  @JsonKey(name: 'JOB_INDUSTRY_NAME')
  String? jobIndustryName;

  ListPriceMinbolig({
    this.contactId,
    this.id,
    this.industry,
    this.industryName,
    this.jobIndustry,
    this.note,
    this.offerId,
    this.partnerId,
    this.prices,
    this.projectId,
    this.series,
    this.seriesOriginal,
    this.seriesText,
    this.signature,
    this.startDate,
    this.subProjectId,
    this.total,
    this.totalVat,
    this.weeks,
    this.jobIndustryName,
    this.totalVatFee,
  });

  factory ListPriceMinbolig.fromJson(Map<String, dynamic> json) =>
      _$ListPriceMinboligFromJson(json);

  Map<String, dynamic> toJson() => _$ListPriceMinboligToJson(this);
  static List<ListPriceMinbolig> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => ListPriceMinbolig.fromJson(i as Map<String, dynamic>))
          .toList();
}
