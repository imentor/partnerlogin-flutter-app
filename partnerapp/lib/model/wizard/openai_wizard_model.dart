import 'package:json_annotation/json_annotation.dart';

part 'openai_wizard_model.g.dart';

@JsonSerializable()
class OpenaiAssistantWizardModel {
  @JsonKey(name: 'wizard')
  OpenaiWizardModel? wizard;
  @JsonKey(name: 'assistant')
  OpenaiAssistantModel assistant;

  OpenaiAssistantWizardModel({this.wizard, required this.assistant});

  factory OpenaiAssistantWizardModel.fromJson(Map<String, dynamic> json) =>
      _$OpenaiAssistantWizardModelFromJson(json);

  Map<String, dynamic> toJson() => _$OpenaiAssistantWizardModelToJson(this);

  static List<OpenaiAssistantWizardModel> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map(
          (i) => OpenaiAssistantWizardModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class OpenaiWizardModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'WIZARD_ID')
  String? wizardId;
  @JsonKey(name: 'CONTACT_ID')
  int? contactId;
  @JsonKey(name: 'ADDRESS_ID')
  int? addressId;
  @JsonKey(name: 'ASSISTANT_ID')
  String? assistantId;
  @JsonKey(name: 'THREAD_ID')
  String? threadId;
  @JsonKey(name: 'RUN_ID')
  String? runId;
  @JsonKey(name: 'CREATED_AT')
  String? createdAt;
  @JsonKey(name: 'UPDATED_AT')
  String? updatedAt;

  OpenaiWizardModel(
      {this.id,
      this.wizardId,
      this.contactId,
      this.addressId,
      this.assistantId,
      this.threadId,
      this.runId,
      this.createdAt,
      this.updatedAt});

  factory OpenaiWizardModel.fromJson(Map<String, dynamic> json) =>
      _$OpenaiWizardModelFromJson(json);

  Map<String, dynamic> toJson() => _$OpenaiWizardModelToJson(this);

  static List<OpenaiWizardModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => OpenaiWizardModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class OpenaiAssistantModel {
  @JsonKey(name: 'id')
  String? id;
  @JsonKey(name: 'object')
  String? object;
  @JsonKey(name: 'created_at')
  int? createdAt;
  @JsonKey(name: 'assistant_id')
  String? assistantId;
  @JsonKey(name: 'thread_id')
  String? threadId;
  @JsonKey(name: 'status')
  String? status;
  @JsonKey(name: 'started_at')
  int? startedAt;
  @JsonKey(name: 'expires_at')
  int? expiresAt;
  @JsonKey(name: 'cancelled_at')
  int? cancelledAt;
  @JsonKey(name: 'failed_at')
  int? failedAt;
  @JsonKey(name: 'completed_at')
  int? completedAt;
  @JsonKey(name: 'required_action')
  String? requiredAction;
  @JsonKey(name: 'last_error')
  String? lastError;
  @JsonKey(name: 'model')
  String? model;
  @JsonKey(name: 'instructions')
  String? instructions;
  @JsonKey(name: 'tools')
  List<dynamic>? tools;
  @JsonKey(name: 'tool_resources')
  Map<String, dynamic>? toolResources;
  @JsonKey(name: 'metadata')
  List<dynamic>? metadata;
  @JsonKey(name: 'temperature')
  int? temperature;
  @JsonKey(name: 'top_p')
  int? topP;
  @JsonKey(name: 'max_completion_tokens')
  dynamic maxCompletionTokens;
  @JsonKey(name: 'max_prompt_tokens')
  dynamic maxPromptTokens;
  @JsonKey(name: 'truncation_strategy')
  Map<String, dynamic>? truncationStrategy;
  @JsonKey(name: 'incomplete_details')
  dynamic incompleteDetails;
  @JsonKey(name: 'usage')
  dynamic usage;
  @JsonKey(name: 'response_format')
  Map<String, dynamic>? responseFormat;
  @JsonKey(name: 'tool_choice')
  String? toolChoice;
  @JsonKey(name: 'parallel_tool_calls')
  dynamic parallelToolCalls;

  OpenaiAssistantModel(
      {this.id,
      this.object,
      this.createdAt,
      this.assistantId,
      this.threadId,
      this.status,
      this.startedAt,
      this.expiresAt,
      this.cancelledAt,
      this.failedAt,
      this.completedAt,
      this.requiredAction,
      this.lastError,
      this.model,
      this.instructions,
      this.tools,
      this.toolResources,
      this.metadata,
      this.temperature,
      this.topP,
      this.maxCompletionTokens,
      this.maxPromptTokens,
      this.truncationStrategy,
      this.incompleteDetails,
      this.usage,
      this.responseFormat,
      this.toolChoice,
      this.parallelToolCalls});

  factory OpenaiAssistantModel.fromJson(Map<String, dynamic> json) =>
      _$OpenaiAssistantModelFromJson(json);

  Map<String, dynamic> toJson() => _$OpenaiAssistantModelToJson(this);

  static List<OpenaiAssistantModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => OpenaiAssistantModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class WizardAssistantMessagesModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'WIZARD_ID')
  String? wizardId;
  @JsonKey(name: 'ASSISTANT_ID')
  String? assistantId;
  @JsonKey(name: 'THREAD_ID')
  String? threadId;
  @JsonKey(name: 'MESSAGE_ID')
  String? messageId;
  @JsonKey(name: 'RUN_ID')
  String? runId;
  @JsonKey(name: 'PAYLOAD')
  Map<String, dynamic>? payload;
  @JsonKey(name: 'PROMPT')
  String? prompt;
  @JsonKey(name: 'MESSAGE')
  Map<String, dynamic>? message;
  @JsonKey(name: 'STATUS')
  String? status;
  @JsonKey(name: 'CREATED_AT')
  String? createdAt;
  @JsonKey(name: 'UPDATED_AT')
  String? updatedAt;

  WizardAssistantMessagesModel(
      {this.id,
      this.wizardId,
      this.assistantId,
      this.threadId,
      this.messageId,
      this.runId,
      this.payload,
      this.prompt,
      this.message,
      this.status,
      this.createdAt,
      this.updatedAt});

  factory WizardAssistantMessagesModel.fromJson(Map<String, dynamic> json) =>
      _$WizardAssistantMessagesModelFromJson(json);

  Map<String, dynamic> toJson() => _$WizardAssistantMessagesModelToJson(this);

  static List<WizardAssistantMessagesModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) =>
              WizardAssistantMessagesModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
