import 'package:json_annotation/json_annotation.dart';

part 'wizard_response_model.g.dart';

@JsonSerializable()
class FullIndustry {
  @JsonKey(name: 'BRANCHE_ID')
  int? brancheId;
  @JsonKey(name: 'BRANCHE')
  String? brancheDa;
  @JsonKey(name: 'BRANCHE_EN')
  String? brancheEn;

  FullIndustry({
    this.brancheId,
    this.brancheDa,
    this.brancheEn,
  });

  FullIndustry.defaults()
      : brancheId = 0,
        brancheDa = '',
        brancheEn = '';

  factory FullIndustry.fromJson(Map<String, dynamic> json) =>
      _$FullIndustryFromJson(json);

  Map<String, dynamic> toJson() => _$FullIndustryToJson(this);
  static List<FullIndustry> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => FullIndustry.fromJson(i as Map<String, dynamic>))
      .toList();
}

class EnterpriseIndustry {
  String? enterpriseId;
  List<JobIndustry>? jobIndustries;

  EnterpriseIndustry({this.enterpriseId, this.jobIndustries});
}

class JobIndustry {
  String? jobIndustryId;
  List<dynamic>? descriptions;

  JobIndustry({this.jobIndustryId, this.descriptions});
}

@JsonSerializable()
class AllWizardText {
  @JsonKey(name: "producttypeid")
  String? producttypeid;
  @JsonKey(name: "producttype")
  String? producttypeDa;
  @JsonKey(name: "producttype_en")
  String? producttypeEn;
  @JsonKey(name: "mpic")
  String? mpic;
  @JsonKey(name: "general_description")
  String? generalDescription;
  @JsonKey(name: "subindustry")
  String? subindustry;
  @JsonKey(name: "industry")
  String? industry;

  AllWizardText({
    this.producttypeid,
    this.producttypeDa,
    this.producttypeEn,
    this.mpic,
    this.generalDescription,
    this.subindustry,
    this.industry,
  });

  AllWizardText.defaults()
      : producttypeid = '',
        producttypeDa = '',
        producttypeEn = '',
        mpic = '',
        generalDescription = '',
        subindustry = '',
        industry = '';

  factory AllWizardText.fromJson(Map<String, dynamic> json) =>
      _$AllWizardTextFromJson(json);

  Map<String, dynamic> toJson() => _$AllWizardTextToJson(this);
  static List<AllWizardText> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => AllWizardText.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class ProductWizardModel {
  @JsonKey(name: "productId")
  String? productId;
  @JsonKey(name: "producttype")
  String? producttype;
  @JsonKey(name: "productFields")
  List<ProductWizardField>? productFields;

  ProductWizardModel({this.productId, this.producttype, this.productFields});

  factory ProductWizardModel.fromJson(Map<String, dynamic> json) =>
      _$ProductWizardModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProductWizardModelToJson(this);
  static List<ProductWizardModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => ProductWizardModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class ProductWizardField {
  @JsonKey(name: 'id')
  String? id;
  @JsonKey(name: 'productname')
  String? productName;
  @JsonKey(name: 'productId')
  String? productId;
  @JsonKey(name: 'parentproductid')
  String? parentproductid;
  @JsonKey(name: 'description')
  String? description;
  @JsonKey(name: 'productfield')
  String? productField;
  @JsonKey(name: 'mandatory')
  String? mandatory;
  @JsonKey(name: 'wizardlead')
  String? wizardLead;
  @JsonKey(name: 'values')
  List<ProductWizardField>? values;
  @JsonKey(name: 'children')
  List<ProductWizardField>? children;
  @JsonKey(name: 'only_description')
  int? onlyDescription;

  ProductWizardField({
    this.id,
    this.children,
    this.description,
    this.mandatory,
    this.parentproductid,
    this.productField,
    this.productId,
    this.productName,
    this.values,
    this.wizardLead,
    this.onlyDescription,
  });

  factory ProductWizardField.fromJson(Map<String, dynamic> json) =>
      _$ProductWizardFieldFromJson(json);

  Map<String, dynamic> toJson() => _$ProductWizardFieldToJson(this);

  static List<ProductWizardField> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => ProductWizardField.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class CreateOfferWizardModel {
  @JsonKey(name: "ID")
  int? id;
  @JsonKey(name: "PARTNER_ID")
  int? partnerId;
  @JsonKey(name: "CONTACT_ID")
  int? contactId;
  @JsonKey(name: "ADDRESS_ID")
  int? addressId;
  @JsonKey(name: "PROJECT_ID")
  int? projectId;
  @JsonKey(name: "OFFER_ID")
  int? offerId;
  @JsonKey(name: "RESERVATION_ID")
  int? reservationId;
  @JsonKey(name: "STEP")
  int? step;
  @JsonKey(name: "STATUS")
  String? status;
  @JsonKey(name: "PROJECT_INFO")
  Map<String, dynamic>? projectInfo;
  @JsonKey(name: "CONTACT_INFO")
  Map<String, dynamic>? contactInfo;

  CreateOfferWizardModel({
    this.addressId,
    this.contactId,
    this.id,
    this.offerId,
    this.partnerId,
    this.projectId,
    this.reservationId,
    this.status,
    this.step,
    this.contactInfo,
    this.projectInfo,
  });

  factory CreateOfferWizardModel.fromJson(Map<String, dynamic> json) =>
      _$CreateOfferWizardModelFromJson(json);

  Map<String, dynamic> toJson() => _$CreateOfferWizardModelToJson(this);
  static List<CreateOfferWizardModel> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => CreateOfferWizardModel.fromJson(i as Map<String, dynamic>))
      .toList();
}
