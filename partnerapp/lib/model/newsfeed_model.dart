import 'package:Haandvaerker.dk/model/messages/message_v2_response_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'newsfeed_model.g.dart';

@JsonSerializable()
class NewsFeedResponseModel {
  NewsFeedResponseModel({
    this.totalMarketPlace,
    this.totalContract,
    this.totalReviews,
    this.totalJobs,
    this.newsFeed,
    this.reviews,
    this.jobs,
    this.projects,
    this.reviewStats,
    this.logs,
  });

  @JsonKey(name: 'total_marketplace')
  final int? totalMarketPlace;
  @JsonKey(name: 'total_contract')
  final int? totalContract;
  @JsonKey(name: 'total_reviews')
  final int? totalReviews;
  @JsonKey(name: 'total_jobs')
  final int? totalJobs;
  @JsonKey(name: 'reviews')
  final List<dynamic>? reviews;

  @JsonKey(name: 'newsFeed')
  final NewsFeedModel? newsFeed;
  @JsonKey(name: 'jobs')
  final List<NewsFeedJobsModel>? jobs;
  @JsonKey(name: 'projects')
  final List<NewsFeedProjectsModel>? projects;
  @JsonKey(name: 'review_stats')
  final NewsFeedReviewStatsModel? reviewStats;
  @JsonKey(name: 'logs')
  final NewsFeedLogsModel? logs;

  factory NewsFeedResponseModel.fromJson(Map<String, dynamic> json) =>
      _$NewsFeedResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$NewsFeedResponseModelToJson(this);
  static List<NewsFeedResponseModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => NewsFeedResponseModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class NewsFeedModel {
  NewsFeedModel({
    this.messages,
    this.reminder,
    this.nps,
    this.news,
    this.url,
  });

  @JsonKey(name: 'messages')
  final List<MessageV2Items>? messages;
  @JsonKey(name: 'reminder')
  final List<dynamic>? reminder;
  @JsonKey(name: 'nps')
  final int? nps;
  @JsonKey(name: 'news')
  final List<dynamic>? news;

  @JsonKey(name: 'url')
  final String? url;

  factory NewsFeedModel.fromJson(Map<String, dynamic> json) =>
      _$NewsFeedModelFromJson(json);

  Map<String, dynamic> toJson() => _$NewsFeedModelToJson(this);
  static List<NewsFeedModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => NewsFeedModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class NewsFeedJobsModel {
  NewsFeedJobsModel({
    this.id,
  });

  @JsonKey(name: 'ID')
  final int? id;

  factory NewsFeedJobsModel.fromJson(Map<String, dynamic> json) =>
      _$NewsFeedJobsModelFromJson(json);

  Map<String, dynamic> toJson() => _$NewsFeedJobsModelToJson(this);
  static List<NewsFeedJobsModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => NewsFeedJobsModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class NewsFeedProjectsModel {
  NewsFeedProjectsModel({
    this.id,
    this.title,
    this.pictures,
    this.projectURL,
    this.stage,
  });

  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'TITLE')
  final String? title;
  @JsonKey(name: 'PICTURES')
  final List<dynamic>? pictures;
  @JsonKey(name: 'PROJECTURL')
  final String? projectURL;
  @JsonKey(name: 'STAGE')
  final String? stage;

  factory NewsFeedProjectsModel.fromJson(Map<String, dynamic> json) =>
      _$NewsFeedProjectsModelFromJson(json);

  Map<String, dynamic> toJson() => _$NewsFeedProjectsModelToJson(this);
  static List<NewsFeedProjectsModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => NewsFeedProjectsModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class NewsFeedReviewStatsModel {
  NewsFeedReviewStatsModel({
    this.rating,
    this.avg,
    this.agreementRating,
    this.priceRating,
    this.cleaningRating,
    this.workRating,
    this.contactRating,
    this.recommendCompanyRating,
    this.count,
  });

  @JsonKey(name: 'RATING')
  final Map<String, int>? rating;
  @JsonKey(name: 'avg')
  final double? avg;
  @JsonKey(name: 'AGREEMENTRATING')
  final int? agreementRating;
  @JsonKey(name: 'PRICERATING')
  final int? priceRating;
  @JsonKey(name: 'CLEANINGRATING')
  final int? cleaningRating;
  @JsonKey(name: 'WORKRATING')
  final int? workRating;
  @JsonKey(name: 'CONTACTRATING')
  final int? contactRating;
  @JsonKey(name: 'RECOMMENDCOMPANYRATING')
  final int? recommendCompanyRating;
  @JsonKey(name: 'count')
  final int? count;

  factory NewsFeedReviewStatsModel.fromJson(Map<String, dynamic> json) =>
      _$NewsFeedReviewStatsModelFromJson(json);

  Map<String, dynamic> toJson() => _$NewsFeedReviewStatsModelToJson(this);
  static List<NewsFeedReviewStatsModel> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => NewsFeedReviewStatsModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class NewsFeedLogsModel {
  NewsFeedLogsModel({
    this.profileViews,
    this.projectViews,
  });

  @JsonKey(name: "profile_views")
  final ProfileViews? profileViews;
  @JsonKey(name: 'project_views')
  final List<ProjectViewsData>? projectViews;

  factory NewsFeedLogsModel.fromJson(Map<String, dynamic> json) =>
      _$NewsFeedLogsModelFromJson(json);

  Map<String, dynamic> toJson() => _$NewsFeedLogsModelToJson(this);
  static List<NewsFeedLogsModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => NewsFeedLogsModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class ProfileViews {
  ProfileViews({
    this.data,
    this.total,
  });

  @JsonKey(name: 'data')
  final List<LogsData>? data;
  @JsonKey(name: 'total')
  final int? total;

  factory ProfileViews.fromJson(Map<String, dynamic> json) =>
      _$ProfileViewsFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileViewsToJson(this);
  static List<ProfileViews> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => ProfileViews.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class ProjectViewsData {
  ProjectViewsData({
    this.days,
    this.itemId,
    this.total,
  });

  ProjectViewsData.defaults()
      : days = [],
        itemId = '',
        total = 0;

  @JsonKey(name: 'days')
  final List<LogsData>? days;
  @JsonKey(name: 'item_id')
  final String? itemId;
  @JsonKey(name: 'total')
  final int? total;

  factory ProjectViewsData.fromJson(Map<String, dynamic> json) =>
      _$ProjectViewsDataFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectViewsDataToJson(this);
  static List<ProjectViewsData> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => ProjectViewsData.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class LogsData {
  LogsData({
    this.date,
    this.total,
  });

  @JsonKey(name: 'date')
  final String? date;
  @JsonKey(name: 'total')
  final int? total;

  factory LogsData.fromJson(Map<String, dynamic> json) =>
      _$LogsDataFromJson(json);

  Map<String, dynamic> toJson() => _$LogsDataToJson(this);
  static List<LogsData> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => LogsData.fromJson(i as Map<String, dynamic>))
      .toList();
}
