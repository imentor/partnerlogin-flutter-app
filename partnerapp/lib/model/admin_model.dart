import 'package:json_annotation/json_annotation.dart';

part 'admin_model.g.dart';

@JsonSerializable()
class AdminModel {
  String? userid;
  String? pid;
  String? email;
  String? password;
  String? name;
  List<Permission?>? permission;
  String? created;
  String? deleted;
  String? mobile;

  String? isemail;

  AdminModel(
      {this.userid,
      this.pid,
      this.email,
      this.password,
      this.name,
      this.permission,
      this.created,
      this.deleted,
      this.mobile,
      this.isemail});

  factory AdminModel.fromJson(Map<String, dynamic> json) =>
      _$AdminModelFromJson(json);

  Map<String, dynamic> toJson() => _$AdminModelToJson(this);
}

@JsonSerializable()
class Permission {
  String? module;
  String? value;

  Permission({this.module, this.value});

  factory Permission.fromJson(Map<String, dynamic> json) =>
      _$PermissionFromJson(json);
  Map<String, dynamic> toJson() => _$PermissionToJson(this);

  @override
  String toString() => 'Permission(module: $module, value: $value)';
}
