import 'package:json_annotation/json_annotation.dart';

part 'open_job_product_model.g.dart';

@JsonSerializable()
class OpenJobProduct {
  @JsonKey(name: '0')
  dynamic s0;
  @JsonKey(name: '1')
  dynamic s1;
  @JsonKey(name: '2')
  dynamic s2;
  @JsonKey(name: '3')
  dynamic s3;
  @JsonKey(name: '4')
  dynamic s4;
  @JsonKey(name: '5')
  dynamic s5;
  @JsonKey(name: '6')
  dynamic s6;
  dynamic productid;
  dynamic quantity;
  dynamic price;
  dynamic condition;
  dynamic parentproductid;
  dynamic productname;
  dynamic productfield;

  OpenJobProduct({
    this.s0,
    this.s1,
    this.s2,
    this.s3,
    this.s4,
    this.s5,
    this.s6,
    this.productid,
    this.quantity,
    this.price,
    this.condition,
    this.parentproductid,
    this.productname,
    this.productfield,
  });

  factory OpenJobProduct.fromJson(Map<String, dynamic> json) =>
      _$OpenJobProductFromJson(json);

  Map<String, dynamic> toJson() => _$OpenJobProductToJson(this);
}
