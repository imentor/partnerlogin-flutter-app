import 'package:Haandvaerker.dk/model/producent/producent_model.dart';
import 'package:Haandvaerker.dk/model/projects/extra_work_response_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'payment_stages_model.g.dart';

@JsonSerializable()
class PaymentStages {
  @JsonKey(name: 'CONTRACT')
  PaymentStageContract? contract;
  @JsonKey(name: 'STAGES')
  List<Stage>? stages;
  @JsonKey(name: 'TRANSACTIONS')
  List<PaymentStageTransaction>? transaction;
  @JsonKey(name: 'EXTRA_WORKS')
  List<ExtraWorkItems>? extraWork;
  @JsonKey(name: 'TOTAL_AMOUNT_STASHED')
  double? totalAmountStashed;
  @JsonKey(name: 'TOTAL_AMOUNT_VAT')
  double? totalAmountVat;
  @JsonKey(name: 'TOTAL_AMOUNT_PAID')
  double? totalAmountPaid;
  @JsonKey(name: 'BALANCE_AMOUNT')
  double? balanceAmount;
  @JsonKey(name: 'TOTAL_AMOUNT_PAID_EXTRA_WORK')
  double? totalAmountPaidExtraWork;
  @JsonKey(name: 'TOTAL_STASHED_EXTRA_WORK_AMOUNT')
  double? totalStashedExtraWorkAmount;

  @JsonKey(name: 'PRODUCENT')
  List<Producent>? producent;

  @JsonKey(name: 'SUBCONTRACTORS')
  List<Producent>? subcontractors;

  @JsonKey(name: 'TOTAL_AMOUNT_APPROVED_MINUS_EXTRA_WORK')
  double? totalAmountApprovedMinusExtraWork;

  PaymentStages({
    this.contract,
    this.stages,
    this.transaction,
    this.extraWork,
    this.balanceAmount,
    this.totalAmountPaid,
    this.totalAmountStashed,
    this.totalAmountVat,
    this.totalAmountPaidExtraWork,
    this.totalStashedExtraWorkAmount,
    this.producent,
    this.subcontractors,
    this.totalAmountApprovedMinusExtraWork,
  });

  factory PaymentStages.fromJson(Map<String, dynamic> json) =>
      _$PaymentStagesFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentStagesToJson(this);
  static List<PaymentStages> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => PaymentStages.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class Stage {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'TITLE')
  String? title;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'NOTES')
  String? notes;
  @JsonKey(name: 'CONTACT_ID')
  int? contactId;
  @JsonKey(name: 'PARTNER_ID')
  int? partnerId;
  @JsonKey(name: 'PROJECT_ID')
  int? projectId;
  @JsonKey(name: 'SUB_PROJECT_ID')
  int? subProjectId;
  @JsonKey(name: 'PRICE')
  double? price;
  @JsonKey(name: 'PRICE_VAT')
  double? priceVat;
  @JsonKey(name: 'PRICE_DEDUCTED')
  double? priceDeducted;
  @JsonKey(name: 'PERCENT')
  int? percent;
  @JsonKey(name: 'DEADLINE')
  String? deadline;
  @JsonKey(name: 'PAYMENT_RELEASED')
  double? paymentReleased;
  @JsonKey(name: 'STATUS')
  String? status;
  @JsonKey(name: 'TYPE')
  String? type;
  @JsonKey(name: 'CONTRACT_TYPE')
  String? contractType;
  @JsonKey(name: 'DECLINED_REASON')
  dynamic declinedReason;
  @JsonKey(name: 'PAYPROFF_TRANSACTION_ID')
  String? payproffTransactionId;
  @JsonKey(name: 'CONTRACT_ID')
  int? contractId;
  @JsonKey(name: 'INDUSTRY_ID')
  int? industryId;
  @JsonKey(name: 'JOB_INDUSTRY_ID')
  int? jobIndustryId;
  @JsonKey(name: 'PHOTOS')
  List<String>? photos;
  @JsonKey(name: 'INDUSTRY')
  String? industry;
  String? jobIndustryName;
  @JsonKey(name: 'TOTAL_AMOUNT_PAID')
  double? totalAmountPaid;
  @JsonKey(name: 'EXTRA_WORK_ID')
  int? extraWorkId;
  @JsonKey(name: 'FILES')
  List<dynamic>? files;
  @JsonKey(name: 'PAYMENT_TRANSACTIONS')
  List<PaymentTransaction>? paymentTransactions;
  @JsonKey(name: 'BALANCE')
  double? balance;
  @JsonKey(name: 'STATUS_EN')
  String? statusEn;
  @JsonKey(name: 'STATUS_DA')
  String? statusDa;
  @JsonKey(name: 'CREATED_AT')
  String? createdAt;
  @JsonKey(name: 'STASHED')
  bool? stashed;

  @JsonKey(name: 'SUBCONTRACTOR_ID')
  int? subcontractorId;
  @JsonKey(name: 'SUBCONTRACTOR_COMPANY_ID')
  int? subcontractorCompanyId;
  @JsonKey(name: 'PRODUCENT_ID')
  int? producentId;
  @JsonKey(name: 'PRODUCENT_COMPANY_ID')
  int? producentCompanyId;

  Stage({
    this.id,
    this.contactId,
    this.contractId,
    this.contractType,
    this.deadline,
    this.declinedReason,
    this.description,
    this.industry,
    this.industryId,
    this.jobIndustryId,
    this.notes,
    this.partnerId,
    this.paymentReleased,
    this.payproffTransactionId,
    this.percent,
    this.photos,
    this.price,
    this.priceDeducted,
    this.priceVat,
    this.projectId,
    this.status,
    this.subProjectId,
    this.title,
    this.type,
    this.jobIndustryName,
    this.totalAmountPaid,
    this.extraWorkId,
    this.files,
    this.paymentTransactions,
    this.balance,
    this.statusDa,
    this.statusEn,
    this.createdAt,
    this.stashed,
    this.producentCompanyId,
    this.producentId,
    this.subcontractorCompanyId,
    this.subcontractorId,
  });

  factory Stage.fromJson(Map<String, dynamic> json) => _$StageFromJson(json);

  Map<String, dynamic> toJson() => _$StageToJson(this);
  static List<Stage> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => Stage.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class PaymentStageContract {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'AMOUNT')
  double? amount;
  @JsonKey(name: 'VAT_PRICE')
  double? vatPrice;

  PaymentStageContract({
    this.id,
    this.amount,
    this.vatPrice,
  });

  factory PaymentStageContract.fromJson(Map<String, dynamic> json) =>
      _$PaymentStageContractFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentStageContractToJson(this);
  static List<PaymentStageContract> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => PaymentStageContract.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class PaymentStageTransaction {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'AMOUNT')
  int? amount;
  @JsonKey(name: 'CURRENT_STATUS')
  String? currentStatus;
  @JsonKey(name: 'DEPOSITED_AMOUNT')
  double? depositedAmount;
  @JsonKey(name: 'EXTRA_WORK_ID')
  int? extraWorkId;
  @JsonKey(name: 'UF_CRM_26_VAT_PRICE')
  double? vatPrice;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'CREATED_TIME')
  String? createdTime;
  @JsonKey(name: 'PAYPROFF_TRANSACTION_ID')
  String? payproffTransactionId;

  PaymentStageTransaction({
    this.id,
    this.amount,
    this.currentStatus,
    this.depositedAmount,
    this.extraWorkId,
    this.vatPrice,
    this.createdTime,
    this.payproffTransactionId,
  });

  factory PaymentStageTransaction.fromJson(Map<String, dynamic> json) =>
      _$PaymentStageTransactionFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentStageTransactionToJson(this);
  static List<PaymentStageTransaction> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => PaymentStageTransaction.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class PaymentTransaction {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'PAYMENT_STAGE_ID')
  int? paymentStageId;
  @JsonKey(name: 'AMOUNT')
  String? amount;
  @JsonKey(name: 'STATUS')
  String? status;
  @JsonKey(name: 'TRANSACTION_DATE')
  String? transactionDate;
  @JsonKey(name: 'CONTACT')
  Contact? contact;
  @JsonKey(name: 'REQUESTED_AMOUNT')
  String? requestedAmount;
  @JsonKey(name: 'COMPANY')
  Contact? company;
  @JsonKey(name: 'HOMEOWNERS_COMMENT')
  String? homeownersComment;
  @JsonKey(name: 'PARTNERS_COMMENT')
  String? partnersComment;
  @JsonKey(name: 'UPLOADED_FILES')
  List<Map<String, dynamic>>? uploadedFiles;

  PaymentTransaction({
    this.id,
    this.amount,
    this.status,
    this.paymentStageId,
    this.transactionDate,
    this.contact,
    this.requestedAmount,
    this.company,
    this.homeownersComment,
    this.partnersComment,
    this.uploadedFiles,
  });

  factory PaymentTransaction.fromJson(Map<String, dynamic> json) =>
      _$PaymentTransactionFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentTransactionToJson(this);
  static List<PaymentTransaction> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => PaymentTransaction.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class Contact {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'COMPANY')
  String? company;
  @JsonKey(name: 'EMAIL')
  String? email;
  @JsonKey(name: 'MOBILE')
  String? mobile;
  @JsonKey(name: 'AVATAR')
  String? avatar;
  @JsonKey(name: 'NAME')
  String? name;

  Contact({
    this.id,
    this.company,
    this.email,
    this.mobile,
    this.avatar,
    this.name,
  });

  factory Contact.fromJson(Map<String, dynamic> json) =>
      _$ContactFromJson(json);

  Map<String, dynamic> toJson() => _$ContactToJson(this);
  static List<Contact> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => Contact.fromJson(i as Map<String, dynamic>))
      .toList();
}
