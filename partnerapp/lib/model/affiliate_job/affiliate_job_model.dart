import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'affiliate_job_model.g.dart';

@JsonSerializable()
class AffiliateJob {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'STAGE')
  String? stage;
  @JsonKey(name: 'CONTACT_NAME')
  String? contactName;
  @JsonKey(name: 'OFFER_TITLE')
  String? offerTitle;
  @JsonKey(name: 'ADDRESS')
  String? address;
  @JsonKey(name: 'MOBILE')
  String? mobile;
  @JsonKey(name: 'EMAIL')
  String? email;
  @JsonKey(name: 'STATUS')
  String? status;
  @JsonKey(name: 'NOTES')
  List<JobNotes>? notes;
  @JsonKey(name: 'PROJECT')
  String? project;

  AffiliateJob({
    this.id,
    this.address,
    this.contactName,
    this.email,
    this.mobile,
    this.notes,
    this.offerTitle,
    this.stage,
    this.project,
  });

  factory AffiliateJob.fromJson(Map<String, dynamic> json) =>
      _$AffiliateJobFromJson(json);

  Map<String, dynamic> toJson() => _$AffiliateJobToJson(this);
  static List<AffiliateJob> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => AffiliateJob.fromJson(i as Map<String, dynamic>))
      .toList();
}
