import 'package:json_annotation/json_annotation.dart';

part 'partner_profil_model.g.dart';

@JsonSerializable()
class PartnerProfileModel {
  List<Map<String, String>>? bankOptions;
  int? pid;
  String? company;
  String? name;
  String? address;
  String? address2;
  int? zip;
  @JsonKey(name: 'service_reminder')
  int? serviceReminder;
  String? city;
  int? telephone;
  dynamic mobile;
  @JsonKey(name: 'setting_notification')
  dynamic settingNotification;
  String? email;
  String? accountnumber;
  String? cvr;
  String? isPBS;
  String? isKLIP;
  int? fipayment;
  int? epaysubscriptionid;
  int? mobilepay;
  String? partnertype;
  @JsonKey(name: 'sms_size')
  int? smsSize;
  @JsonKey(name: 'id_distancesms')
  String? idDistancesms;
  String? inactivesms;
  String? autobooking;
  @JsonKey(name: 'agreement_date')
  String? agreementDate;
  @JsonKey(name: 'demo_partner')
  int? demoPartner;
  int? monthlyp;
  dynamic onebeforemeeting;
  @JsonKey(name: 'onebefore_sms_email')
  dynamic onebeforeSmsEmail;
  dynamic twohourbeforemeeting;
  dynamic twohouraftermeeting;
  dynamic twodayaftermeeting;
  @JsonKey(name: 'twodayafter_sms_email')
  dynamic twodayafterSmsEmail;
  @JsonKey(name: 'twodayafter_days')
  dynamic twodayafterDays;
  dynamic tendayaftermeeting;
  @JsonKey(name: 'tendayafter_sms_email')
  dynamic tendayafterSmsEmail;
  @JsonKey(name: 'tendayafter_days')
  dynamic tendayafterDays;
  List<dynamic>? allzip;
  String? subpartnerjoblist;
  String? newsubpartnerjoblist;
  List<Subpartnerjob>? subpartnerjob;
  List<Smsdistance>? smsdistance;

  List<Holiday>? holiday;
  bool? haacredit;
  dynamic profiletype;
  List<Workflows>? workflows;
  bool? opsige;
  bool? opsigedone;
  String? startend;
  String? finishend;
  String? lytid;
  String? startdate;
  String? startendother;
  String? finishendother;
  int? divmonth;
  bool? opsigeother;
  String? opsigetext;
  int? opsigenew;
  String? dinerofirma;
  String? dineroapi;
  String? dineroapiupdate;
  String? economicapi;
  String? economicapiupdate;
  String? billyapi;
  String? billyapiupdate;
  String? facebookapiupdate;
  String? facebookapi;
  String? levandorregnr;
  String? levandorbankkonto;
  String? levandorupdate;
  Forretningsomr? forretningsomr;
  List<dynamic>? forretningsomrPartner;
  String? aftaleurl;
  String? partnerdistancesmsfirst;
  List<SmsdistanceList>? smsdistanceList;
  List<dynamic>? dataObjselect;
  List<dynamic>? newdataObjselect;
  List<Lavproduct>? lavproduct;

  List<Telefon>? telefon;
  List<Invoiceemail>? invoiceemail;

  PartnerProfileModel(
      {this.bankOptions,
      this.pid,
      this.company,
      this.name,
      this.address,
      this.address2,
      this.zip,
      this.serviceReminder,
      this.city,
      this.telephone,
      this.mobile,
      this.email,
      this.accountnumber,
      this.cvr,
      this.isPBS,
      this.isKLIP,
      this.fipayment,
      this.epaysubscriptionid,
      this.mobilepay,
      this.partnertype,
      this.smsSize,
      this.idDistancesms,
      this.inactivesms,
      this.autobooking,
      this.agreementDate,
      this.demoPartner,
      this.monthlyp,
      this.onebeforemeeting,
      this.onebeforeSmsEmail,
      this.twohourbeforemeeting,
      this.twohouraftermeeting,
      this.twodayaftermeeting,
      this.twodayafterSmsEmail,
      this.twodayafterDays,
      this.tendayaftermeeting,
      this.tendayafterSmsEmail,
      this.tendayafterDays,
      this.allzip,
      this.subpartnerjoblist,
      this.newsubpartnerjoblist,
      this.subpartnerjob,
      this.smsdistance,
      this.haacredit,
      this.profiletype,
      this.workflows,
      this.opsige,
      this.opsigedone,
      this.startend,
      this.finishend,
      this.lytid,
      this.startdate,
      this.startendother,
      this.finishendother,
      this.divmonth,
      this.opsigeother,
      this.opsigetext,
      this.opsigenew,
      this.dinerofirma,
      this.dineroapi,
      this.dineroapiupdate,
      this.economicapi,
      this.economicapiupdate,
      this.billyapi,
      this.billyapiupdate,
      this.facebookapiupdate,
      this.facebookapi,
      this.levandorregnr,
      this.levandorbankkonto,
      this.levandorupdate,
      this.forretningsomr,
      this.forretningsomrPartner,
      this.aftaleurl,
      this.partnerdistancesmsfirst,
      this.smsdistanceList,
      this.dataObjselect,
      this.newdataObjselect,
      this.lavproduct,
      this.telefon,
      this.settingNotification,
      this.invoiceemail});

  factory PartnerProfileModel.fromJson(Map<String, dynamic> json) {
    return _$PartnerProfileModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$PartnerProfileModelToJson(this);
}

@JsonSerializable()
class Holiday {
  @JsonKey(name: '0')
  String? s0;
  @JsonKey(name: '1')
  String? s1;
  @JsonKey(name: '2')
  String? s2;
  @JsonKey(name: '3')
  String? s3;
  @JsonKey(name: 'id_partner_holiday')
  String? idPartnerHoliday;
  @JsonKey(name: 'id_partner')
  String? idPartner;
  @JsonKey(name: 'holiday_from')
  String? holidayFrom;
  @JsonKey(name: 'holiday_to')
  String? holidayTo;

  Holiday(
      {this.s0,
      this.s1,
      this.s2,
      this.s3,
      this.idPartnerHoliday,
      this.idPartner,
      this.holidayFrom,
      this.holidayTo});

  factory Holiday.fromJson(Map<String, dynamic> json) =>
      _$HolidayFromJson(json);

  Map<String, dynamic> toJson() => _$HolidayToJson(this);
}

@JsonSerializable()
class Subpartnerjob {
  String? id;
  @JsonKey(name: 'parent_id')
  String? parentId;
  @JsonKey(name: 'country_id')
  String? countryId;
  String? name;
  String? price;
  int? valid;

  Subpartnerjob(
      {this.id,
      this.parentId,
      this.countryId,
      this.name,
      this.price,
      this.valid});

  factory Subpartnerjob.fromJson(Map<String, dynamic> json) =>
      _$SubpartnerjobFromJson(json);

  Map<String, dynamic> toJson() => _$SubpartnerjobToJson(this);
}

@JsonSerializable()
class Smsdistance {
  @JsonKey(name: '0')
  String? s0;
  @JsonKey(name: '1')
  String? s1;
  @JsonKey(name: '2')
  String? s2;
  @JsonKey(name: '3')
  String? s3;
  @JsonKey(name: '4')
  String? s4;
  @JsonKey(name: 'partnerdistancesms_id')
  String? partnerdistancesmsId;
  String? partnerdistancesms;
  String? presence;
  String? picklistValueid;
  String? sortorderid;
  @JsonKey(name: 'IndustryId')
  int? industryId;
  @JsonKey(name: 'IndustryName')
  String? industryName;
  @JsonKey(name: 'SubIndustries')
  List<SubIndustries>? subIndustries;

  Smsdistance(
      {this.s0,
      this.s1,
      this.s2,
      this.s3,
      this.s4,
      this.partnerdistancesmsId,
      this.partnerdistancesms,
      this.presence,
      this.picklistValueid,
      this.sortorderid,
      this.industryId,
      this.industryName,
      this.subIndustries});

  factory Smsdistance.fromJson(Map<String, dynamic> json) =>
      _$SmsdistanceFromJson(json);

  Map<String, dynamic> toJson() => _$SmsdistanceToJson(this);
}

@JsonSerializable()
class SubIndustries {
  @JsonKey(name: 'SubIndustrId')
  int? subIndustryId;
  @JsonKey(name: 'SubIndustryName')
  String? subIndustryName;

  SubIndustries({this.subIndustryId, this.subIndustryName});

  factory SubIndustries.fromJson(Map<String, dynamic> json) =>
      _$SubIndustriesFromJson(json);

  Map<String, dynamic> toJson() => _$SubIndustriesToJson(this);
}

@JsonSerializable()
class Invoiceemail {
  String? id;
  String? pid;
  String? email;
  String? create;

  Invoiceemail({this.id, this.pid, this.email, this.create});

  factory Invoiceemail.fromJson(Map<String, dynamic> json) =>
      _$InvoiceemailFromJson(json);

  Map<String, dynamic> toJson() => _$InvoiceemailToJson(this);
}

@JsonSerializable()
class Workflows {
  @JsonKey(name: '0')
  String? s0;
  @JsonKey(name: '1')
  String? s1;
  @JsonKey(name: '2')
  String? s2;
  @JsonKey(name: '3')
  String? s3;
  @JsonKey(name: '4')
  String? s4;
  @JsonKey(name: '5')
  String? s5;
  @JsonKey(name: '6')
  String? s6;
  @JsonKey(name: '7')
  String? s7;
  @JsonKey(name: '8')
  String? s8;
  @JsonKey(name: '9')
  String? s9;
  @JsonKey(name: '10')
  String? s10;
  @JsonKey(name: '11')
  String? s11;
  @JsonKey(name: '12')
  String? s12;
  @JsonKey(name: '13')
  String? s13;
  @JsonKey(name: '14')
  String? s14;
  @JsonKey(name: '15')
  String? s15;
  @JsonKey(name: '16')
  String? s16;
  @JsonKey(name: '17')
  String? s17;
  @JsonKey(name: '18')
  String? s18;
  @JsonKey(name: '19')
  String? s19;
  @JsonKey(name: '20')
  String? s20;
  @JsonKey(name: '21')
  String? s21;
  @JsonKey(name: '22')
  String? s22;
  @JsonKey(name: '23')
  String? s23;
  @JsonKey(name: 'workflow_id')
  String? workflowId;
  String? summary;
  String? conditions;
  @JsonKey(name: 'execution_condition')
  String? executionCondition;
  dynamic defaultworkflow;
  dynamic type;
  dynamic filtersavedinnew;
  @JsonKey(name: 'module_name')
  String? moduleName;
  @JsonKey(name: 'other_type')
  String? otherType;
  String? color;
  String? deleted;
  String? active;
  @JsonKey(name: 'created_time')
  String? createdTime;
  String? production;
  String? stat;
  @JsonKey(name: 'module_type')
  String? moduleType;
  String? note;
  String? title;
  String? profile;
  @JsonKey(name: 'profile_type')
  dynamic profileType;
  String? newpartner;
  String? approved;
  @JsonKey(name: 'approved_date')
  String? approvedDate;
  int? select;

  Workflows(
      {this.s0,
      this.s1,
      this.s2,
      this.s3,
      this.s4,
      this.s5,
      this.s6,
      this.s7,
      this.s8,
      this.s9,
      this.s10,
      this.s11,
      this.s12,
      this.s13,
      this.s14,
      this.s15,
      this.s16,
      this.s17,
      this.s18,
      this.s19,
      this.s20,
      this.s21,
      this.s22,
      this.s23,
      this.workflowId,
      this.summary,
      this.conditions,
      this.executionCondition,
      this.defaultworkflow,
      this.type,
      this.filtersavedinnew,
      this.moduleName,
      this.otherType,
      this.color,
      this.deleted,
      this.active,
      this.createdTime,
      this.production,
      this.stat,
      this.moduleType,
      this.note,
      this.title,
      this.profile,
      this.profileType,
      this.newpartner,
      this.approved,
      this.approvedDate,
      this.select});

  factory Workflows.fromJson(Map<String, dynamic> json) =>
      _$WorkflowsFromJson(json);

  Map<String, dynamic> toJson() => _$WorkflowsToJson(this);
}

@JsonSerializable()
class Forretningsomr {
  @JsonKey(name: '42')
  dynamic s42;
  @JsonKey(name: '47')
  dynamic s47;
  @JsonKey(name: '83')
  dynamic s83;
  @JsonKey(name: '84')
  dynamic s84;
  @JsonKey(name: '89')
  dynamic s89;
  @JsonKey(name: '95')
  dynamic s95;
  @JsonKey(name: '96')
  dynamic s96;
  @JsonKey(name: '109')
  dynamic s109;
  @JsonKey(name: '111')
  dynamic s111;
  @JsonKey(name: '114')
  dynamic s114;
  @JsonKey(name: '187')
  dynamic s187;
  @JsonKey(name: '219')
  dynamic s219;
  @JsonKey(name: '259')
  dynamic s259;
  @JsonKey(name: '326')
  dynamic s326;
  @JsonKey(name: '329')
  dynamic s329;

  Forretningsomr(
      {this.s42,
      this.s47,
      this.s83,
      this.s84,
      this.s89,
      this.s95,
      this.s96,
      this.s109,
      this.s111,
      this.s114,
      this.s187,
      this.s219,
      this.s259,
      this.s326,
      this.s329});

  factory Forretningsomr.fromJson(Map<String, dynamic> json) =>
      _$ForretningsomrFromJson(json);

  Map<String, dynamic> toJson() => _$ForretningsomrToJson(this);
}

class SmsdistanceList {
  String? text;
  String? value;

  SmsdistanceList({this.text, this.value});

  SmsdistanceList.fromJson(Map<String, dynamic> json) {
    text = json['text'] as String?;
    value = json['value'] as String?;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['text'] = text;
    data['value'] = value;
    return data;
  }
}

@JsonSerializable()
class Lavproduct {
  @JsonKey(name: 'IndustryId')
  int? industryId;
  @JsonKey(name: 'IndustryName')
  String? industryName;
  @JsonKey(name: 'SubIndustries')
  List<SubIndustries>? subIndustries;

  Lavproduct({this.industryId, this.industryName, this.subIndustries});

  factory Lavproduct.fromJson(Map<String, dynamic> json) =>
      _$LavproductFromJson(json);

  Map<String, dynamic> toJson() => _$LavproductToJson(this);
}

class Telefon {
  String? s0;
  String? s1;
  String? s2;
  String? s3;
  String? s4;
  String? id;
  String? pid;
  String? phone;
  String? marketplace;
  String? create;

  Telefon(
      {this.s0,
      this.s1,
      this.s2,
      this.s3,
      this.s4,
      this.id,
      this.pid,
      this.phone,
      this.marketplace,
      this.create});

  Telefon.fromJson(Map<String, dynamic> json) {
    s0 = json['0'] as String?;
    s1 = json['1'] as String?;
    s2 = json['2'] as String?;
    s3 = json['3'] as String?;
    s4 = json['4'] as String?;
    id = json['id'] as String?;
    pid = json['pid'] as String?;
    phone = json['phone'] as String?;
    marketplace = json['marketplace'] as String?;
    create = json['create'] as String?;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['0'] = s0;
    data['1'] = s1;
    data['2'] = s2;
    data['3'] = s3;
    data['4'] = s4;
    data['id'] = id;
    data['pid'] = pid;
    data['phone'] = phone;
    data['marketplace'] = marketplace;
    data['create'] = create;
    return data;
  }
}
