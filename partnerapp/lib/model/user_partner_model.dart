import 'package:json_annotation/json_annotation.dart';

part 'user_partner_model.g.dart';

@JsonSerializable()
class UserPartnerModel {
  String? appid;
  String? firstname;
  String? lastname;
  String? email;
  String? mobile;
  String? mailingzip;
  String? mailingstreet;
  String? mailingcity;

  UserPartnerModel(
      {this.appid,
      this.firstname,
      this.lastname,
      this.email,
      this.mobile,
      this.mailingzip,
      this.mailingstreet,
      this.mailingcity});

  factory UserPartnerModel.fromJson(Map<String, dynamic> json) =>
      _$UserPartnerModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserPartnerModelToJson(this);
}
