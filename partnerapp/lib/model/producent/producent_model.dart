import 'package:json_annotation/json_annotation.dart';

part 'producent_model.g.dart';

@JsonSerializable()
class Producent {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'STAGE')
  String? stage;
  @JsonKey(name: 'AMOUNT')
  String? amount;
  @JsonKey(name: 'PROJECT')
  String? project;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'CONTRACT')
  String? contract;

  Producent({
    this.id,
    this.name,
    this.amount,
    this.description,
    this.project,
    this.stage,
    this.contract,
  });

  factory Producent.fromJson(Map<String, dynamic> json) =>
      _$ProducentFromJson(json);

  Map<String, dynamic> toJson() => _$ProducentToJson(this);

  static List<Producent> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => Producent.fromJson(i as Map<String, dynamic>))
      .toList();
}
