class ProducentCompany {
  String? fullName;
  String? cvprNumber;
  String? cvr;
  String? address;

  ProducentCompany({this.address, this.cvprNumber, this.cvr, this.fullName});
}
