import 'package:Haandvaerker.dk/model/offer_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'offer_pagination_model.g.dart';

@JsonSerializable()
class OfferPaginationModel {
  @JsonKey(name: 'current_page')
  int? currentPage;
  @JsonKey(name: 'data')
  List<OfferModel>? data;
  @JsonKey(name: 'last_page')
  int? lastPage;
  @JsonKey(name: 'total')
  int? total;

  OfferPaginationModel(
      {this.currentPage, this.lastPage, this.total, this.data});

  factory OfferPaginationModel.fromJson(Map<String, dynamic> json) =>
      _$OfferPaginationModelFromJson(json);

  Map<String, dynamic> toJson() => _$OfferPaginationModelToJson(this);

  static List<OfferPaginationModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => OfferPaginationModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
