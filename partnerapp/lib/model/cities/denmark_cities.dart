import 'package:json_annotation/json_annotation.dart';

part 'denmark_cities.g.dart';

@JsonSerializable()
class DenmarkCities {
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'POSTAL_CODE')
  int? postalCode;
  @JsonKey(name: 'LATITUDE')
  String? latitude;
  @JsonKey(name: 'LONGITUDE')
  String? longitude;
  @JsonKey(name: 'PRISE_REGION')
  String? priseRegion;
  @JsonKey(name: 'OIO_REST_NUMBER')
  String? restNumber;

  DenmarkCities({
    this.id,
    this.latitude,
    this.longitude,
    this.name,
    this.postalCode,
    this.priseRegion,
    this.restNumber,
  });

  DenmarkCities.defaults()
      : id = 0,
        latitude = '',
        longitude = '',
        name = '',
        postalCode = 0,
        priseRegion = '',
        restNumber = '';

  factory DenmarkCities.fromJson(Map<String, dynamic> json) =>
      _$DenmarkCitiesFromJson(json);

  Map<String, dynamic> toJson() => _$DenmarkCitiesToJson(this);
  static List<DenmarkCities> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => DenmarkCities.fromJson(i as Map<String, dynamic>))
      .toList();
}
