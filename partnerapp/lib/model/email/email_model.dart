import 'package:json_annotation/json_annotation.dart';

part 'email_model.g.dart';

@JsonSerializable()
class EmailModel {
  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'PARTNER_ID')
  final int? partnerId;
  @JsonKey(name: 'EMAIL')
  final String? email;
  @JsonKey(name: 'DATE_CREATED')
  final String? dateCreated;

  EmailModel({this.dateCreated, this.email, this.id, this.partnerId});

  factory EmailModel.fromJson(Map<String, dynamic> json) =>
      _$EmailModelFromJson(json);

  EmailModel.defaults()
      : id = 0,
        partnerId = 0,
        email = '',
        dateCreated = '';

  Map<String, dynamic> toJson() => _$EmailModelToJson(this);
  static List<EmailModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => EmailModel.fromJson(i as Map<String, dynamic>))
      .toList();
}
