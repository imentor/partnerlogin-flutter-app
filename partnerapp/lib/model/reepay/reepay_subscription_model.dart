import 'package:json_annotation/json_annotation.dart';

part 'reepay_subscription_model.g.dart';

@JsonSerializable()
class ReepaySubscriptionModel {
  final bool? success;
  final String? message;
  final SubscriptionData? data;

  const ReepaySubscriptionModel({this.success, this.message, this.data});

  factory ReepaySubscriptionModel.fromJson(Map<String, dynamic> json) =>
      _$ReepaySubscriptionModelFromJson(json);

  Map<String, dynamic> toJson() => _$ReepaySubscriptionModelToJson(this);
}

@JsonSerializable()
class SubscriptionData {
  final int? page;
  final int? size;
  final int? count;
  final String? search;
  final List<SubscriptionContent>? content;
  @JsonKey(name: 'total_elements')
  final int? totalElements;
  @JsonKey(name: 'total_pages')
  final int? totalPages;

  const SubscriptionData(
      {this.page,
      this.size,
      this.count,
      this.search,
      this.content,
      this.totalElements,
      this.totalPages});

  factory SubscriptionData.fromJson(Map<String, dynamic> json) =>
      _$SubscriptionDataFromJson(json);

  Map<String, dynamic> toJson() => _$SubscriptionDataToJson(this);
}

@JsonSerializable()
class SubscriptionContent {
  final String? handle;
  final String? customer;
  final String? plan;
  final String? state;
  final bool? test;
  final double? amount;
  final int? quantity;
  final String? timezone;
  final String? created;
  final String? activated;
  final bool? renewing;
  @JsonKey(name: 'plan_version')
  final int? planVersion;
  @JsonKey(name: 'amount_incl_vat')
  final bool? amountInclVat;
  @JsonKey(name: 'start_date')
  final String? startDate;
  @JsonKey(name: 'end_date')
  final String? endDate;
  @JsonKey(name: 'grace_duration')
  final int? graceDuration;
  @JsonKey(name: 'next_period_start')
  final String? nextPeriodStart;
  @JsonKey(name: 'first_period_start')
  final String? firstPeriodStart;
  @JsonKey(name: 'is_cancelled')
  final bool? isCancelled;
  @JsonKey(name: 'in_trial')
  final bool? inTrial;
  @JsonKey(name: 'has_started')
  final bool? hasStarted;
  @JsonKey(name: 'renewal_count')
  final int? renewalCount;
  @JsonKey(name: 'payment_method_added')
  final bool? paymentMethodAdded;
  @JsonKey(name: 'failed_invoices')
  final int? failedInvoices;
  @JsonKey(name: 'failed_amount')
  final double? failedAmount;
  @JsonKey(name: 'cancelled_invoices')
  final int? cancelledInvoices;
  @JsonKey(name: 'cancelled_amount')
  final double? cancelledAmount;
  @JsonKey(name: 'pending_invoices')
  final int? pendingInvoices;
  @JsonKey(name: 'pending_amount')
  final double? pendingAmount;
  @JsonKey(name: 'dunning_invoices')
  final int? dunningInvoices;
  @JsonKey(name: 'dunning_amount')
  final double? dunningAmount;
  @JsonKey(name: 'settled_invoices')
  final int? settledInvoices;
  @JsonKey(name: 'settled_amount')
  final double? settledAmount;
  @JsonKey(name: 'refunded_amount')
  final double? refundedAmount;
  @JsonKey(name: 'pending_additional_costs')
  final num? pendingAdditionalCosts;
  @JsonKey(name: 'pending_additional_cost_amount')
  final double? pendingAdditionalCostAmount;
  @JsonKey(name: 'transferred_additional_costs')
  final num? transferredAdditionalCosts;
  @JsonKey(name: 'transferred_additional_cost_amount')
  final double? transferredAdditionalCostAmount;
  @JsonKey(name: 'pending_credits')
  final num? pendingCredits;
  @JsonKey(name: 'pending_credit_amount')
  final double? pendingCreditAmount;
  @JsonKey(name: 'transferred_credits')
  final num? transferredCredits;
  @JsonKey(name: 'transferred_credit_amount')
  final double? transferredCreditAmount;
  @JsonKey(name: 'hosted_page_links')
  final HostedPageLinks? hostedPageLinks;
  @JsonKey(name: 'pending_change')
  final PendingChange? pendingChange;
  @JsonKey(name: 'subscription_changes')
  final SubscriptionChanges? subscriptionChanges;

  const SubscriptionContent(
      {this.handle,
      this.customer,
      this.plan,
      this.state,
      this.test,
      this.amount,
      this.quantity,
      this.timezone,
      this.created,
      this.activated,
      this.renewing,
      this.planVersion,
      this.amountInclVat,
      this.startDate,
      this.endDate,
      this.graceDuration,
      this.nextPeriodStart,
      this.firstPeriodStart,
      this.isCancelled,
      this.inTrial,
      this.hasStarted,
      this.renewalCount,
      this.paymentMethodAdded,
      this.failedInvoices,
      this.failedAmount,
      this.cancelledInvoices,
      this.cancelledAmount,
      this.pendingInvoices,
      this.pendingAmount,
      this.dunningInvoices,
      this.dunningAmount,
      this.settledInvoices,
      this.settledAmount,
      this.refundedAmount,
      this.pendingAdditionalCosts,
      this.pendingAdditionalCostAmount,
      this.transferredAdditionalCosts,
      this.transferredAdditionalCostAmount,
      this.pendingCredits,
      this.pendingCreditAmount,
      this.transferredCredits,
      this.transferredCreditAmount,
      this.hostedPageLinks,
      this.pendingChange,
      this.subscriptionChanges});

  factory SubscriptionContent.fromJson(Map<String, dynamic> json) =>
      _$SubscriptionContentFromJson(json);

  Map<String, dynamic> toJson() => _$SubscriptionContentToJson(this);
}

@JsonSerializable()
class HostedPageLinks {
  final String? paymentInfo;

  const HostedPageLinks({this.paymentInfo});

  factory HostedPageLinks.fromJson(Map<String, dynamic> json) =>
      _$HostedPageLinksFromJson(json);

  Map<String, dynamic> toJson() => _$HostedPageLinksToJson(this);
}

@JsonSerializable()
class PendingChange {
  final double? amount;
  final bool? pending;
  final String? updated;
  final String? created;
  @JsonKey(name: 'amount_incl_vat')
  final bool? amountInclVat;

  const PendingChange(
      {this.amount,
      this.pending,
      this.updated,
      this.created,
      this.amountInclVat});

  factory PendingChange.fromJson(Map<String, dynamic> json) =>
      _$PendingChangeFromJson(json);

  Map<String, dynamic> toJson() => _$PendingChangeToJson(this);
}

@JsonSerializable()
class SubscriptionChanges {
  final double? amount;
  final bool? pending;
  final String? applied;
  final String? created;
  @JsonKey(name: 'amount_incl_vat')
  final bool? amountInclVat;

  const SubscriptionChanges(
      {this.amount,
      this.pending,
      this.applied,
      this.created,
      this.amountInclVat});

  factory SubscriptionChanges.fromJson(Map<String, dynamic> json) =>
      _$SubscriptionChangesFromJson(json);

  Map<String, dynamic> toJson() => _$SubscriptionChangesToJson(this);
}
