import 'package:json_annotation/json_annotation.dart';

part 'reepay_payment_method_model.g.dart';

@JsonSerializable()
class ReepayPaymentMethodModel {
  final bool? success;
  final String? message;
  final PaymentMethodData? data;

  const ReepayPaymentMethodModel({this.success, this.message, this.data});

  factory ReepayPaymentMethodModel.fromJson(Map<String, dynamic> json) =>
      _$ReepayPaymentMethodModelFromJson(json);

  Map<String, dynamic> toJson() => _$ReepayPaymentMethodModelToJson(this);
}

@JsonSerializable()
class PaymentMethodData {
  final List<PaymentCard>? cards;
  @JsonKey(name: 'mps_subscriptions')
  final List<dynamic>? mpsSubscriptions;

  const PaymentMethodData({this.cards, this.mpsSubscriptions});

  factory PaymentMethodData.fromJson(Map<String, dynamic> json) =>
      _$PaymentMethodDataFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentMethodDataToJson(this);
}

@JsonSerializable()
class PaymentCard {
  final String? id;
  final String? state;
  final String? customer;
  final String? reference;
  final String? created;
  final String? fingerprint;
  @JsonKey(name: 'gw_ref')
  final String? gwRef;
  @JsonKey(name: 'cart_type')
  final String? cartType;
  @JsonKey(name: 'exp_date')
  final String? expDate;
  @JsonKey(name: 'masked_card')
  final String? maskedCard;
  @JsonKey(name: 'card_country')
  final String? cartCountry;

  const PaymentCard(
      {this.id,
      this.state,
      this.customer,
      this.reference,
      this.created,
      this.fingerprint,
      this.gwRef,
      this.cartType,
      this.expDate,
      this.maskedCard,
      this.cartCountry});

  factory PaymentCard.fromJson(Map<String, dynamic> json) =>
      _$PaymentCardFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentCardToJson(this);
}
