import 'package:json_annotation/json_annotation.dart';

part 'reepay_invoice_model.g.dart';

@JsonSerializable()
class ReepayInvoiceModel {
  final bool? success;
  final String? message;
  final InvoiceData? data;

  const ReepayInvoiceModel({this.success, this.message, this.data});

  factory ReepayInvoiceModel.fromJson(Map<String, dynamic> json) =>
      _$ReepayInvoiceModelFromJson(json);

  Map<String, dynamic> toJson() => _$ReepayInvoiceModelToJson(this);
}

@JsonSerializable()
class InvoiceData {
  final int? page;
  final int? size;
  final int? count;
  final String? search;
  final List<InvoiceContent>? content;
  @JsonKey(name: 'total_elements')
  final int? totalElements;
  @JsonKey(name: 'total_pages')
  final int? totalPages;

  const InvoiceData(
      {this.page,
      this.size,
      this.count,
      this.search,
      this.content,
      this.totalElements,
      this.totalPages});

  factory InvoiceData.fromJson(Map<String, dynamic> json) =>
      _$InvoiceDataFromJson(json);

  Map<String, dynamic> toJson() => _$InvoiceDataToJson(this);
}

@JsonSerializable()
class InvoiceContent {
  final String? id;
  final String? handle;
  final String? customer;
  final String? subscription;
  final String? state;
  final String? type;
  final double? amount;
  final int? number;
  final String? currency;
  final String? due;
  final List<dynamic>? credits;
  final String? created;
  @JsonKey(name: 'dunning_plan')
  final String? dunningPlan;
  @JsonKey(name: 'discount_amount')
  final double? discountAmount;
  @JsonKey(name: 'org_amount')
  final double? orgAmount;
  @JsonKey(name: "amount_vat")
  final double? amountVat;
  @JsonKey(name: 'amount_ex_vat')
  final double? amountExVat;
  @JsonKey(name: 'settled_amount')
  final double? settledAmount;
  @JsonKey(name: 'refunded_amount')
  final double? refundedAmount;
  @JsonKey(name: 'order_lines')
  final List<OrderLine>? orderLines;
  @JsonKey(name: 'additional_costs')
  final List<dynamic>? additionalCosts;
  final List<dynamic>? transactions;
  @JsonKey(name: 'credit_notes')
  final List<dynamic>? creditNotes;
  @JsonKey(name: 'dunning_start')
  final String? dunningStart;
  @JsonKey(name: 'dunning_count')
  final int? dunningCount;

  InvoiceContent({
    this.id,
    this.handle,
    this.customer,
    this.subscription,
    this.state,
    this.type,
    this.amount,
    this.number,
    this.currency,
    this.due,
    this.credits,
    this.created,
    this.dunningPlan,
    this.discountAmount,
    this.orgAmount,
    this.amountVat,
    this.amountExVat,
    this.settledAmount,
    this.refundedAmount,
    this.orderLines,
    this.additionalCosts,
    this.transactions,
    this.creditNotes,
    this.dunningStart,
    this.dunningCount,
  });

  factory InvoiceContent.fromJson(Map<String, dynamic> json) =>
      _$InvoiceContentFromJson(json);

  Map<String, dynamic> toJson() => _$InvoiceContentToJson(this);
}

@JsonSerializable()
class OrderLine {
  final String? id;
  final String? orderText;
  final double? amount;
  final double? vat;
  final int? quantity;
  final String? origin;
  final String? timestamp;
  @JsonKey(name: 'amount_vat')
  final double? amountVat;
  @JsonKey(name: 'amount_ex_vat')
  final double? amountExVat;
  @JsonKey(name: 'unit_amount')
  final double? unitAmount;
  @JsonKey(name: 'unit_amount_vat')
  final double? unitAmountVat;
  @JsonKey(name: 'unit_amount_ex_vat')
  final double? unitAmountExVat;
  @JsonKey(name: 'amount_defined_incl_vat')
  final bool? amountDefinedInclVat;

  const OrderLine({
    this.id,
    this.orderText,
    this.amount,
    this.vat,
    this.quantity,
    this.origin,
    this.timestamp,
    this.amountVat,
    this.amountExVat,
    this.unitAmount,
    this.unitAmountVat,
    this.unitAmountExVat,
    this.amountDefinedInclVat,
  });

  factory OrderLine.fromJson(Map<String, dynamic> json) =>
      _$OrderLineFromJson(json);

  Map<String, dynamic> toJson() => _$OrderLineToJson(this);
}
