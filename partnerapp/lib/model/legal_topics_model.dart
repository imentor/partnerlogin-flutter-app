import 'package:json_annotation/json_annotation.dart';

part 'legal_topics_model.g.dart';

@JsonSerializable()
class LegalTopics {
  @JsonKey(name: 'ID')
  String? id;
  @JsonKey(name: 'IBLOCK_ID')
  String? iBlockId;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'PROPERTY_TOPIC_VALUE')
  String? propertyTopicValue;
  @JsonKey(name: 'CNT')
  String? cnt;

  LegalTopics(
      {this.id, this.iBlockId, this.name, this.propertyTopicValue, this.cnt});

  factory LegalTopics.fromJson(Map<String, dynamic> json) =>
      _$LegalTopicsFromJson(json);

  Map<String, dynamic> toJson() => _$LegalTopicsToJson(this);
}
