import 'package:json_annotation/json_annotation.dart';

part 'ws_checkout_response_model.g.dart';

@JsonSerializable()
class WebshopCheckoutResponseModel {
  final bool? success;
  @JsonKey(name: 'InvoiceNumber')
  final dynamic invoiceNumber;

  WebshopCheckoutResponseModel({
    this.success,
    this.invoiceNumber,
  });

  factory WebshopCheckoutResponseModel.fromJson(Map<String, dynamic> json) =>
      _$WebshopCheckoutResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$WebshopCheckoutResponseModelToJson(this);

  @override
  String toString() =>
      'WebshopCheckoutResponseModel(success: $success, invoiceNumber: $invoiceNumber)';
}
