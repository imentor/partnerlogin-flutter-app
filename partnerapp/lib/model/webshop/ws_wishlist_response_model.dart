import 'package:json_annotation/json_annotation.dart';

part 'ws_wishlist_response_model.g.dart';

@JsonSerializable()
class WebshopWishlistResponseModel {
  final bool? success;
  final Data? data;

  const WebshopWishlistResponseModel({
    this.success,
    this.data,
  });

  factory WebshopWishlistResponseModel.fromJson(Map<String, dynamic> json) =>
      _$WebshopWishlistResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$WebshopWishlistResponseModelToJson(this);

  @override
  String toString() =>
      'WebshopWishlistResponseModel(success: $success, data: $data)';
}

@JsonSerializable()
class Data {
  @JsonKey(name: 'current_page')
  final int? currentPage;
  @JsonKey(name: 'data')
  final List<WishList>? wishList;
  @JsonKey(name: 'first_page_url')
  final String? firstPageUrl;
  final int? from;
  @JsonKey(name: 'last_page')
  final int? lastPage;
  @JsonKey(name: 'last_page_url')
  final String? lastPageUrl;
  final List<Links>? links;
  @JsonKey(name: 'next_page_url')
  final dynamic nextPageUrl;
  final String? path;
  @JsonKey(name: 'per_page')
  final int? perPage;
  @JsonKey(name: 'prev_page_url')
  final dynamic prevPageUrl;
  final int? to;
  final int? total;

  const Data({
    this.currentPage,
    this.wishList,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.links,
    this.nextPageUrl,
    this.path,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);
  Map<String, dynamic> toJson() => _$DataToJson(this);

  @override
  String toString() {
    return 'Data(currentPage: $currentPage, wishList: $wishList, firstPageUrl: $firstPageUrl, from: $from, lastPage: $lastPage, lastPageUrl: $lastPageUrl, links: $links, nextPageUrl: $nextPageUrl, path: $path, perPage: $perPage, prevPageUrl: $prevPageUrl, to: $to, total: $total)';
  }
}

@JsonSerializable()
class WishList {
  @JsonKey(name: 'COMPANY_ID')
  final int? companyId;
  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'NAME')
  final String? name;
  @JsonKey(name: 'CODE')
  final String? code;
  @JsonKey(name: 'ACTIVE')
  final String? active;
  @JsonKey(name: 'PREVIEW_PICTURE')
  final int? previewPicture;
  @JsonKey(name: 'DETAIL_PICTURE')
  final int? detailPicture;
  @JsonKey(name: 'SORT')
  final int? sort;
  @JsonKey(name: 'XML_ID')
  final String? xmlId;
  @JsonKey(name: 'TIMESTAMP_X')
  final String? timeStampX;
  @JsonKey(name: 'DATE_CREATE')
  final String? dateCreate;
  @JsonKey(name: 'MODIFIED_BY')
  final int? modifiedBy;
  @JsonKey(name: 'CREATED_BY')
  final int? createdBy;
  @JsonKey(name: 'CATALOG_ID')
  final int? catalogId;
  @JsonKey(name: 'SECTION_ID')
  final dynamic sectionId;
  @JsonKey(name: 'DESCRIPTION')
  final String? description;
  @JsonKey(name: 'DESCRIPTION_TYPE')
  final String? descriptionType;
  @JsonKey(name: 'PRICE')
  final String? price;
  @JsonKey(name: 'CURRENCY_ID')
  final String? currencyId;
  @JsonKey(name: 'VAT_ID')
  final dynamic vatId;
  @JsonKey(name: 'VAT_INCLUDED')
  final String? vatIncluded;
  @JsonKey(name: 'MEASURE')
  final int? measure;
  @JsonKey(name: 'CATEGORY')
  final String? category;
  @JsonKey(name: 'CATEGORY_ID')
  final int? categoryId;
  @JsonKey(name: 'PRODUCT_GROUP')
  final String? productGroup;
  @JsonKey(name: 'SIZE_VARIATION')
  final String? sizeVariation;
  @JsonKey(name: 'NUMBER_VARIATION')
  final String? numberVariation;
  @JsonKey(name: 'NUMBERS')
  final String? numbers;
  @JsonKey(name: 'SIZE')
  final String? size;
  @JsonKey(name: 'PRODUCT_TITLE')
  final String? productTitle;
  @JsonKey(name: 'ORDER_EMAIL')
  final String? orderEmail;
  @JsonKey(name: 'PARTNER_EMAIL')
  final String? partnerEmail;
  @JsonKey(name: 'BACKPRINT')
  final String? backPrint;
  @JsonKey(name: 'IMAGE_URL')
  final String? imageUrl;
  @JsonKey(name: 'get_product_image')
  final List<GetProductImage>? getProductImage;

  const WishList({
    this.companyId,
    this.id,
    this.name,
    this.code,
    this.active,
    this.previewPicture,
    this.detailPicture,
    this.sort,
    this.xmlId,
    this.timeStampX,
    this.dateCreate,
    this.modifiedBy,
    this.createdBy,
    this.catalogId,
    this.sectionId,
    this.description,
    this.descriptionType,
    this.price,
    this.currencyId,
    this.vatId,
    this.vatIncluded,
    this.measure,
    this.category,
    this.categoryId,
    this.productGroup,
    this.sizeVariation,
    this.numberVariation,
    this.numbers,
    this.size,
    this.productTitle,
    this.orderEmail,
    this.partnerEmail,
    this.backPrint,
    this.imageUrl,
    this.getProductImage,
  });

  factory WishList.fromJson(Map<String, dynamic> json) =>
      _$WishListFromJson(json);
  Map<String, dynamic> toJson() => _$WishListToJson(this);

  @override
  String toString() {
    return 'WishList(companyId: $companyId, id: $id, name: $name, code: $code, active: $active, previewPicture: $previewPicture, detailPicture: $detailPicture, sort: $sort, xmlId: $xmlId, timeStampX: $timeStampX, dateCreate: $dateCreate, modifiedBy: $modifiedBy, createdBy: $createdBy, catalogId: $catalogId, sectionId: $sectionId, description: $description, descriptionType: $descriptionType, price: $price, currencyId: $currencyId, vatId: $vatId, vatIncluded: $vatIncluded, measure: $measure, category: $category, categoryId: $categoryId, productGroup: $productGroup, sizeVariation: $sizeVariation, numberVariation: $numberVariation, numbers: $numbers, size: $size, productTitle: $productTitle, orderEmail: $orderEmail, partnerEmail: $partnerEmail, backPrint: $backPrint, getProductImage: $getProductImage)';
  }
}

@JsonSerializable()
class GetProductImage {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'IBLOCK_PROPERTY_ID')
  int? iblockPropertyId;
  @JsonKey(name: 'IBLOCK_ELEMENT_ID')
  int? iblockElementId;
  @JsonKey(name: 'VALUE')
  String? value;
  @JsonKey(name: 'TIMESTAMP_X')
  String? timeStampX;
  @JsonKey(name: 'CONTENT_TYPE')
  String? contentType;
  @JsonKey(name: 'HEIGHT')
  int? height;
  @JsonKey(name: 'WIDTH')
  int? width;
  @JsonKey(name: 'FILE_SIZE')
  int? fileSize;
  @JsonKey(name: 'FILE_NAME')
  String? fileName;
  @JsonKey(name: 'ORIGINAL_NAME')
  String? originalName;
  @JsonKey(name: 'SUBDIR')
  String? subDir;
  String? showURL;

  GetProductImage({
    this.id,
    this.iblockPropertyId,
    this.iblockElementId,
    this.value,
    this.timeStampX,
    this.contentType,
    this.height,
    this.width,
    this.fileSize,
    this.fileName,
    this.originalName,
    this.subDir,
    this.showURL,
  });

  factory GetProductImage.fromJson(Map<String, dynamic> json) =>
      _$GetProductImageFromJson(json);

  Map<String, dynamic> toJson() => _$GetProductImageToJson(this);
}

@JsonSerializable()
class Links {
  String? url;

  dynamic label;

  bool? active;

  Links({this.url, this.label, this.active});

  factory Links.fromJson(Map<String, dynamic> json) => _$LinksFromJson(json);

  Map<String, dynamic> toJson() => _$LinksToJson(this);

  @override
  String toString() => 'Links(url: $url, label: $label, active: $active)';
}
