import 'package:json_annotation/json_annotation.dart';

part 'ws_coupon_response_model.g.dart';

@JsonSerializable()
class WebshopCouponResponseModel {
  @JsonKey(name: 'ID')
  final int? id;
  final bool? success;
  final String? status;
  final String? value;
  final String? unit;

  const WebshopCouponResponseModel({
    this.id,
    this.success,
    this.status,
    this.value,
    this.unit,
  });

  factory WebshopCouponResponseModel.fromJson(Map<String, dynamic> json) =>
      _$WebshopCouponResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$WebshopCouponResponseModelToJson(this);

  @override
  String toString() {
    return 'WebshopCouponReponseModel(success: $success, status: $status, value: $value, unit: $unit)';
  }
}
