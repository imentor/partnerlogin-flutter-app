import 'package:json_annotation/json_annotation.dart';

part 'ws_cart_response_model.g.dart';

@JsonSerializable()
class WebshopCartResponseModel {
  final bool? success;
  final Data? data;

  const WebshopCartResponseModel({
    this.success,
    this.data,
  });

  factory WebshopCartResponseModel.fromJson(Map<String, dynamic> json) =>
      _$WebshopCartResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$WebshopCartResponseModelToJson(this);

  @override
  String toString() =>
      'WebshopCartResponseModel(success: $success, data: $data)';
}

@JsonSerializable()
class Data {
  @JsonKey(name: 'current_page')
  final dynamic currentPage;
  @JsonKey(name: 'data')
  final List<Cart>? cart;
  @JsonKey(name: 'first_page_url')
  final dynamic firstPageUrl;
  final dynamic from;
  @JsonKey(name: 'last_page')
  final dynamic lastPage;
  @JsonKey(name: 'last_page_url')
  final dynamic lastPageUrl;
  final List<Link>? link;

  @JsonKey(name: 'next_page_url')
  final dynamic nextPageUrl;
  final dynamic path;
  @JsonKey(name: 'per_page')
  final dynamic perPage;
  @JsonKey(name: 'prev_page_url')
  final dynamic prevPageUrl;
  final dynamic to;
  final dynamic total;

  const Data({
    this.currentPage,
    this.cart,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.link,
    this.nextPageUrl,
    this.path,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);
  Map<String, dynamic> toJson() => _$DataToJson(this);

  @override
  String toString() {
    return 'Data(currentPage: $currentPage, cart: $cart, firstPageUrl: $firstPageUrl, from: $from, lastPage: $lastPage, lastPageUrl: $lastPageUrl, link: $link, nextPageUrl: $nextPageUrl, path: $path, perPage: $perPage, prevPageUrl: $prevPageUrl, to: $to, total: $total)';
  }
}

@JsonSerializable()
class Cart {
  @JsonKey(name: 'ID')
  final dynamic id;
  @JsonKey(name: 'DATE_CREATE')
  final dynamic dateCreated;
  @JsonKey(name: 'DATE_MODIFY')
  final dynamic dateModified;
  @JsonKey(name: 'CREATED_BY_ID')
  final dynamic createdById;
  @JsonKey(name: 'MODIFY_BY_ID')
  final dynamic modifyById;
  @JsonKey(name: 'ASSIGNED_BY_ID')
  final dynamic assignedById;
  @JsonKey(name: 'OPENED')
  final dynamic opened;
  @JsonKey(name: 'LEAD_ID')
  final dynamic leadId;
  @JsonKey(name: 'COMPANY_ID')
  final dynamic companyId;
  @JsonKey(name: 'CONTACT_ID')
  final dynamic contactId;
  @JsonKey(name: 'QUOTE_ID')
  final dynamic quoteId;
  // @JsonKey(name: '')
  @JsonKey(name: 'TITLE')
  final dynamic title;
  @JsonKey(name: 'PRODUCT_ID')
  final dynamic productId;
  @JsonKey(name: 'CATEGORY_ID')
  final dynamic categoryId;
  @JsonKey(name: 'STAGE_ID')
  final dynamic stadeId;
  @JsonKey(name: 'STAGE_SEMANTIC_ID')
  final dynamic stageSemanticId;
  @JsonKey(name: 'IS_NEW')
  final dynamic isNew;
  @JsonKey(name: 'IS_RECURRING')
  final dynamic isRecurring;
  @JsonKey(name: 'IS_RETURN_CUSTOMER')
  final dynamic isReturnCustomer;
  @JsonKey(name: 'IS_REPEATED_APPROACH')
  final dynamic isRepeatedApproach;
  @JsonKey(name: 'CLOSED')
  final dynamic closed;
  @JsonKey(name: 'TYPE_ID')
  final dynamic typeId;
  @JsonKey(name: 'OPPORTUNITY')
  final dynamic opportunity;
  @JsonKey(name: 'TAX_VALUE')
  final dynamic taxValue;
  @JsonKey(name: 'CURRENCY_ID')
  final dynamic currencyId;
  @JsonKey(name: 'OPPORTUNITY_ACCOUNT')
  final dynamic opportunityAccount;
  @JsonKey(name: 'TAX_VALUE_ACCOUNT')
  final dynamic taxValueAccount;
  @JsonKey(name: 'ACCOUNT_CURRENCY_ID')
  final dynamic accountCurrencyId;
  @JsonKey(name: 'PROBABILITY')
  final dynamic probability;
  @JsonKey(name: 'COMMENTS')
  final dynamic comments;
  @JsonKey(name: 'BEGINDATE')
  final dynamic beginDate;
  @JsonKey(name: 'CLOSEDATE')
  final dynamic closeDate;
  @JsonKey(name: 'EVENT_DATE')
  final dynamic eventDate;
  @JsonKey(name: 'EVENT_ID')
  final dynamic eventId;
  @JsonKey(name: 'EVENT_DESCRIPTION')
  final dynamic eventDescription;
  @JsonKey(name: 'EXCH_RATE')
  final dynamic exchangeRate;
  @JsonKey(name: 'LOCATION_ID')
  final dynamic locationId;
  @JsonKey(name: 'WEBFORM_ID')
  final dynamic webformId;
  @JsonKey(name: 'SOURCE_ID')
  final dynamic sourceId;
  @JsonKey(name: 'SOURCE_DESCRIPTION')
  final dynamic sourceDescription;
  @JsonKey(name: 'ORIGINATOR_ID')
  final dynamic originatorId;
  @JsonKey(name: 'ORIGIN_ID')
  final dynamic originId;
  @JsonKey(name: 'ADDITIONAL_INFO')
  final dynamic additionalInfo;
  @JsonKey(name: 'SEARCH_CONTENT')
  final dynamic searchContent;
  @JsonKey(name: 'IS_MANUAL_OPPORTUNITY')
  final dynamic isManualOpportunity;
  @JsonKey(name: 'ORDER_STAGE')
  final dynamic orderStage;
  @JsonKey(name: 'webshop_cart_products')
  final List<CartProduct>? cartProducts;

  const Cart({
    this.id,
    this.dateCreated,
    this.dateModified,
    this.createdById,
    this.modifyById,
    this.assignedById,
    this.opened,
    this.leadId,
    this.companyId,
    this.contactId,
    this.quoteId,
    this.title,
    this.productId,
    this.categoryId,
    this.stadeId,
    this.stageSemanticId,
    this.isNew,
    this.isRecurring,
    this.isReturnCustomer,
    this.isRepeatedApproach,
    this.closed,
    this.typeId,
    this.opportunity,
    this.taxValue,
    this.currencyId,
    this.opportunityAccount,
    this.taxValueAccount,
    this.accountCurrencyId,
    this.probability,
    this.comments,
    this.beginDate,
    this.closeDate,
    this.eventDate,
    this.eventId,
    this.eventDescription,
    this.exchangeRate,
    this.locationId,
    this.webformId,
    this.sourceId,
    this.sourceDescription,
    this.originatorId,
    this.originId,
    this.additionalInfo,
    this.searchContent,
    this.isManualOpportunity,
    this.orderStage,
    this.cartProducts,
  });

  factory Cart.fromJson(Map<String, dynamic> json) => _$CartFromJson(json);
  Map<String, dynamic> toJson() => _$CartToJson(this);

  @override
  String toString() {
    return 'Cart(id: $id, dateCreated: $dateCreated, dateModified: $dateModified, createdById: $createdById, modifyById: $modifyById, assignedById: $assignedById, opened: $opened, leadId: $leadId, companyId: $companyId, contactId: $contactId, quoteId: $quoteId, title: $title, productId: $productId, categoryId: $categoryId, stadeId: $stadeId, stageSemanticId: $stageSemanticId, isNew: $isNew, isRecurring: $isRecurring, isReturnCustomer: $isReturnCustomer, isRepeatedApproach: $isRepeatedApproach, closed: $closed, typeId: $typeId, opportunity: $opportunity, taxValue: $taxValue, currencyId: $currencyId, opportunityAccount: $opportunityAccount, taxValueAccount: $taxValueAccount, accountCurrencyId: $accountCurrencyId, probability: $probability, comments: $comments, beginDate: $beginDate, closeDate: $closeDate, eventDate: $eventDate, eventDescription: $eventDescription, exchangeRate: $exchangeRate, locationId: $locationId, webformId: $webformId, sourceId: $sourceId, sourceDescription: $sourceDescription, originatorId: $originatorId, originId: $originId, additionalInfo: $additionalInfo, searchContent: $searchContent, isManualOpportunity: $isManualOpportunity, orderStage: $orderStage, cartProducts: $cartProducts)';
  }
}

@JsonSerializable()
class CartProduct {
  @JsonKey(name: 'ID')
  final dynamic id;
  @JsonKey(name: 'OWNER_ID')
  final dynamic ownerId;
  @JsonKey(name: 'OWNER_TYPE')
  final dynamic ownerType;
  @JsonKey(name: 'PRODUCT_ID')
  final dynamic productId;
  @JsonKey(name: 'PRODUCT_NAME')
  final dynamic productName;
  @JsonKey(name: 'PRICE')
  final dynamic price;
  @JsonKey(name: 'PRICE_ACCOUNT')
  final dynamic priceAccount;
  @JsonKey(name: 'PRICE_EXCLUSIVE')
  final dynamic priceExclusive;
  @JsonKey(name: 'PRICE_NETTO')
  final dynamic priceNetto;
  @JsonKey(name: 'PRICE_BRUTTO')
  final dynamic priceBrutto;
  @JsonKey(name: 'QUANTITY')
  final dynamic quantity;
  @JsonKey(name: 'DISCOUNT_TYPE_ID')
  final dynamic discountTypeId;
  @JsonKey(name: 'DISCOUNT_RATE')
  final dynamic discountRate;
  @JsonKey(name: 'DISCOUNT_SUM')
  final dynamic discountSum;
  @JsonKey(name: 'TAX_RATE')
  final dynamic taxRate;
  @JsonKey(name: 'TAX_INCLUDED')
  final dynamic taxIncluded;
  @JsonKey(name: 'CUSTOMIZED')
  final dynamic customized;
  @JsonKey(name: 'MEASURE_CODE')
  final dynamic measureCode;
  @JsonKey(name: 'MEASURE_NAME')
  final dynamic measureName;
  @JsonKey(name: 'SORT')
  final dynamic sort;
  @JsonKey(name: 'ACTIVE')
  final dynamic active;
  @JsonKey(name: 'DESCRIPTION')
  final dynamic description;
  @JsonKey(name: 'CURRENCY_ID')
  final dynamic currencyId;
  @JsonKey(name: 'CATEGORY')
  final dynamic category;
  @JsonKey(name: 'PRODUCT_GROUP')
  final dynamic productGroup;
  @JsonKey(name: 'NUMBERS')
  final dynamic numbers;
  @JsonKey(name: 'SIZE')
  final dynamic size;
  @JsonKey(name: 'PRODUCT_TITLE')
  final dynamic productTitle;
  @JsonKey(name: 'PARTNER_EMAIL')
  final dynamic partnerEmail;
  @JsonKey(name: 'ORDER_EMAIL')
  final dynamic orderEmail;
  @JsonKey(name: 'BACKPRINT')
  final dynamic backprint;
  @JsonKey(name: 'IMAGE_URL')
  final String? imageUrl;
  @JsonKey(name: 'PDF_URL')
  final String? pdfUrl;
  @JsonKey(name: 'PDF_UNIT_PRICE')
  final dynamic pdfUnitPrice;
  @JsonKey(name: 'NEED_GRAPHIC_HELP')
  final String? needGraphicHelp;
  @JsonKey(name: 'get_product_image')
  final List<GetProductImageModel>? getProductImage;

  CartProduct({
    this.id,
    this.ownerId,
    this.ownerType,
    this.productId,
    this.productName,
    this.price,
    this.priceAccount,
    this.priceExclusive,
    this.priceNetto,
    this.priceBrutto,
    this.quantity,
    this.discountTypeId,
    this.discountRate,
    this.discountSum,
    this.taxRate,
    this.taxIncluded,
    this.customized,
    this.measureCode,
    this.measureName,
    this.sort,
    this.active,
    this.description,
    this.currencyId,
    this.category,
    this.productGroup,
    this.numbers,
    this.size,
    this.productTitle,
    this.partnerEmail,
    this.orderEmail,
    this.backprint,
    this.imageUrl,
    this.pdfUrl,
    this.pdfUnitPrice,
    this.needGraphicHelp,
    this.getProductImage,
  });

  factory CartProduct.fromJson(Map<String, dynamic> json) =>
      _$CartProductFromJson(json);
  Map<String, dynamic> toJson() => _$CartProductToJson(this);

  @override
  String toString() {
    return 'CartProduct(id: $id, ownerId: $ownerId, ownerType: $ownerType, productId: $productId, productName: $productName, price: $price, priceAccount: $priceAccount, priceExclusive: $priceExclusive, priceNetto: $priceNetto, priceBrutto: $priceBrutto, quantity: $quantity, discountTypeId: $discountTypeId, discountRate: $discountRate, discountSum: $discountSum, taxRate: $taxRate, taxIncluded: $taxIncluded, customized: $customized, measureCode: $measureCode, measureName: $measureName, sort: $sort, webshopUploadedImage: $getProductImage)';
  }
}

@JsonSerializable()
class WebshopUploadedImage {
  @JsonKey(name: 'ID')
  final dynamic id;
  @JsonKey(name: 'DEAL_ID')
  final dynamic dealId;
  @JsonKey(name: 'PRODUCT_ID')
  final dynamic productId;
  @JsonKey(name: 'IMAGE_NAME')
  final dynamic imageName;
  @JsonKey(name: 'SIZE')
  final dynamic size;
  @JsonKey(name: 'BACKPRINT')
  final dynamic backPrint;

  const WebshopUploadedImage({
    this.id,
    this.dealId,
    this.productId,
    this.imageName,
    this.size,
    this.backPrint,
  });

  factory WebshopUploadedImage.fromJson(Map<String, dynamic> json) =>
      _$WebshopUploadedImageFromJson(json);
  Map<String, dynamic> toJson() => _$WebshopUploadedImageToJson(this);

  @override
  String toString() {
    return 'WebshopUploadedImage(id: $id, dealId: $dealId, productId: $productId, imageName: $imageName, size: $size, backPrint: $backPrint)';
  }
}

@JsonSerializable()
class Link {
  final dynamic url;
  final String? label;
  final bool? active;

  const Link({
    this.url,
    this.label,
    this.active,
  });

  factory Link.fromJson(Map<String, dynamic> json) => _$LinkFromJson(json);
  Map<String, dynamic> toJson() => _$LinkToJson(this);

  @override
  String toString() => 'Link(url: $url, label: $label, active: $active)';
}

@JsonSerializable()
class GetProductImageModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'IBLOCK_PROPERTY_ID')
  int? iblockPropertyId;
  @JsonKey(name: 'IBLOCK_ELEMENT_ID')
  int? iblockElementId;
  @JsonKey(name: 'VALUE')
  String? value;
  @JsonKey(name: 'TIMESTAMP_X')
  String? timestampx;
  @JsonKey(name: 'CONTENT_TYPE')
  String? contentType;
  @JsonKey(name: 'HEIGHT')
  int? height;
  @JsonKey(name: 'WIDTH')
  int? width;
  @JsonKey(name: 'FILE_SIZE')
  int? fileSize;
  @JsonKey(name: 'FILE_NAME')
  String? fileName;
  @JsonKey(name: 'ORIGINAL_NAME')
  String? originalName;
  @JsonKey(name: 'SUBDIR')
  String? subDir;
  String? showURL;

  GetProductImageModel(
      {this.id,
      this.iblockPropertyId,
      this.iblockElementId,
      this.value,
      this.timestampx,
      this.contentType,
      this.height,
      this.width,
      this.fileSize,
      this.fileName,
      this.originalName,
      this.subDir,
      this.showURL});

  factory GetProductImageModel.fromJson(Map<String, dynamic> json) =>
      _$GetProductImageModelFromJson(json);

  Map<String, dynamic> toJson() => _$GetProductImageModelToJson(this);
}
