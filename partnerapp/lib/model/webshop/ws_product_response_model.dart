// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:json_annotation/json_annotation.dart';

part 'ws_product_response_model.g.dart';

@JsonSerializable()
class WebshopProductResponseModel {
  bool? success;
  //Data data;
  List<Product>? data;
  WebshopProductResponseModel({this.success, this.data});

  factory WebshopProductResponseModel.fromJson(Map<String, dynamic> json) =>
      _$WebshopProductResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$WebshopProductResponseModelToJson(this);

  @override
  String toString() =>
      'WebshopProductResponseModel(success: $success, data: $data)';
}

@JsonSerializable()
class Data {
  @JsonKey(name: 'current_page')
  int? currentPage;
  @JsonKey(name: 'data')
  List<Product>? products;
  @JsonKey(name: 'first_page_url')
  String? firstPageUrl;
  int? from;
  @JsonKey(name: 'last_page')
  int? lastPage;
  @JsonKey(name: 'last_page_url')
  String? lastPageUrl;
  List<Links>? links;
  @JsonKey(name: 'next_page_url')
  dynamic nextPageUrl;
  String? path;
  @JsonKey(name: 'per_page')
  int? perPage;
  @JsonKey(name: 'prev_page_url')
  dynamic prevPageUrl;
  int? to;
  int? total;

  Data(
      {this.currentPage,
      this.products,
      this.firstPageUrl,
      this.from,
      this.lastPage,
      this.lastPageUrl,
      this.links,
      this.nextPageUrl,
      this.path,
      this.perPage,
      this.prevPageUrl,
      this.to,
      this.total});

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  Map<String, dynamic> toJson() => _$DataToJson(this);
  static List<Data> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => Data.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class Product {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'CODE')
  dynamic code;
  @JsonKey(name: 'ACTIVE')
  String? active;
  @JsonKey(name: 'PREVIEW_PICTURE')
  dynamic previewPicture;
  @JsonKey(name: 'DETAIL_PICTURE')
  dynamic detailPicture;
  @JsonKey(name: 'sort')
  int? sort;
  @JsonKey(name: 'XML_ID')
  String? xmlId;
  @JsonKey(name: 'TIMESTAMP_X')
  String? timestampX;
  @JsonKey(name: 'DATE_CREATE')
  String? dateCreate;
  @JsonKey(name: 'MODIFIED_BY')
  int? modifiedBy;
  @JsonKey(name: 'CREATED_BY')
  int? createdBy;
  @JsonKey(name: 'CATALOG_ID')
  int? catalogId;
  @JsonKey(name: 'SECTION_ID')
  dynamic sectionId;
  @JsonKey(name: 'DESCRIPTION')
  dynamic description;
  @JsonKey(name: 'DESCRIPTION_TYPE')
  String? descriptionType;
  @JsonKey(name: 'PRICE')
  String? price;
  @JsonKey(name: 'CURRENCY_ID')
  String? currencyId;
  @JsonKey(name: 'VAT_ID')
  dynamic vatId;
  @JsonKey(name: 'VAT_INCLUDED')
  String? vatIncluded;
  @JsonKey(name: 'MEASURE')
  dynamic measure;
  @JsonKey(name: 'CATEGORY')
  String? category;
  @JsonKey(name: 'CATEGORY_ID')
  int? categoryId;
  @JsonKey(name: 'PRODUCT_GROUP')
  String? productGroup;
  @JsonKey(name: 'SIZE_VARIATION')
  String? sizeVariation;
  @JsonKey(name: 'NUMBER_VARIATION')
  String? numberVariation;
  @JsonKey(name: 'NUMBERS')
  String? numbers;
  @JsonKey(name: 'SIZE')
  String? size;
  @JsonKey(name: 'PRICE_IF_QTYEQ2')
  dynamic priceIfQtyEq2;
  @JsonKey(name: 'PRICE_IF_QTYEQGT3')
  dynamic priceIfQtyEq3;
  @JsonKey(name: 'get_product_image')
  List<GetProductImage>? getProductImage;
  @JsonKey(name: 'get_element_property')
  List<GetElementProperty>? getElementProperty;
  @JsonKey(name: 'WEBSITE')
  String? website;
  @JsonKey(name: "NEED_GRAPHIC_HELP")
  String? needGraphicHelp;
  @JsonKey(name: "PDF_UNIT_PRICE")
  String? pdfUnitPrice;
  @JsonKey(name: "PDF_URL")
  String? pdfUrl;

  Product({
    this.id,
    this.name,
    this.code,
    this.active,
    this.previewPicture,
    this.detailPicture,
    this.sort,
    this.xmlId,
    this.timestampX,
    this.dateCreate,
    this.modifiedBy,
    this.createdBy,
    this.catalogId,
    this.sectionId,
    this.description,
    this.descriptionType,
    this.price,
    this.currencyId,
    this.vatId,
    this.vatIncluded,
    this.measure,
    this.category,
    this.categoryId,
    this.productGroup,
    this.sizeVariation,
    this.numberVariation,
    this.numbers,
    this.size,
    this.getProductImage,
    this.getElementProperty,
    this.priceIfQtyEq2,
    this.priceIfQtyEq3,
    this.website,
    this.needGraphicHelp,
    this.pdfUnitPrice,
    this.pdfUrl,
  });

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);

  Map<String, dynamic> toJson() => _$ProductToJson(this);

  @override
  String toString() {
    return 'Product(id: $id, name: $name, code: $code, active: $active, previewPicture: $previewPicture, detailPicture: $detailPicture, sort: $sort, xmlId: $xmlId, timestampX: $timestampX, dateCreate: $dateCreate, modifiedBy: $modifiedBy, createdBy: $createdBy, catalogId: $catalogId, sectionId: $sectionId, description: $description, descriptionType: $descriptionType, price: $price, currencyId: $currencyId, vatId: $vatId, vatIncluded: $vatIncluded, measure: $measure, category: $category, categoryId: $categoryId, productGroup: $productGroup, sizeVariation: $sizeVariation, numberVariation: $numberVariation, numbers: $numbers, size: $size, priceIfQtyEq2: $priceIfQtyEq2, priceIfQtyEq3: $priceIfQtyEq3, getProductImage: $getProductImage, getElementProperty: $getElementProperty, website: $website, needGraphicHelp: $needGraphicHelp, pdfUnitPrice: $pdfUnitPrice, pdfUrl: $pdfUrl)';
  }
}

@JsonSerializable()
class GetProductImage {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'IBLOCK_PROPERTY_ID')
  int? iblockPropertyId;
  @JsonKey(name: 'IBLOCK_ELEMENT_ID')
  int? iblockElementId;
  @JsonKey(name: 'VALUE')
  String? value;
  @JsonKey(name: 'TIMESTAMP_X')
  String? timeStampX;
  @JsonKey(name: 'CONTENT_TYPE')
  String? contentType;
  @JsonKey(name: 'HEIGHT')
  int? height;
  @JsonKey(name: 'WIDTH')
  int? width;
  @JsonKey(name: 'FILE_SIZE')
  int? fileSize;
  @JsonKey(name: 'FILE_NAME')
  String? fileName;
  @JsonKey(name: 'ORIGINAL_NAME')
  String? originalName;
  String? showURL;
  String? downloadURL;

  GetProductImage(
      {this.id,
      this.iblockPropertyId,
      this.iblockElementId,
      this.value,
      this.timeStampX,
      this.contentType,
      this.height,
      this.width,
      this.fileSize,
      this.fileName,
      this.originalName,
      this.showURL,
      this.downloadURL});

  factory GetProductImage.fromJson(Map<String, dynamic> json) =>
      _$GetProductImageFromJson(json);

  Map<String, dynamic> toJson() => _$GetProductImageToJson(this);

  @override
  String toString() {
    return 'GetProductImage(id: $id, iblockPropertyId: $iblockPropertyId, iblockElementId: $iblockElementId, value: $value, timeStampX: $timeStampX, contentType: $contentType, height: $height, width: $width, fileSize: $fileSize, fileName: $fileName, originalName: $originalName, showURL: $showURL, downloadURL: $downloadURL)';
  }
}

@JsonSerializable()
class GetElementProperty {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'IBLOCK_PROPERTY_ID')
  int? iblockPropertyId;
  @JsonKey(name: 'IBLOCK_ELEMENT_ID')
  int? iblockElementId;
  @JsonKey(name: 'VALUE')
  String? value;
  @JsonKey(name: 'VALUE_TYPE')
  String? valueType;
  @JsonKey(name: 'VALUE_ENUM')
  int? valueEnum;
  @JsonKey(name: 'VALUE_NUM')
  String? valueNum;
  @JsonKey(name: 'DESCRIPTION')
  String? description;

  GetElementProperty(
      {this.id,
      this.iblockPropertyId,
      this.iblockElementId,
      this.value,
      this.valueType,
      this.valueEnum,
      this.valueNum,
      this.description});

  factory GetElementProperty.fromJson(Map<String, dynamic> json) =>
      _$GetElementPropertyFromJson(json);

  Map<String, dynamic> toJson() => _$GetElementPropertyToJson(this);

  @override
  String toString() {
    return 'GetElementProperty(id: $id, iblockPropertyId: $iblockPropertyId, iblockElementId: $iblockElementId, value: $value, valueType: $valueType, valueEnum: $valueEnum, valueNum: $valueNum, description: $description)';
  }
}

@JsonSerializable()
class Links {
  String? url;

  dynamic label;

  bool? active;

  Links({this.url, this.label, this.active});

  factory Links.fromJson(Map<String, dynamic> json) => _$LinksFromJson(json);

  Map<String, dynamic> toJson() => _$LinksToJson(this);

  @override
  String toString() => 'Links(url: $url, label: $label, active: $active)';
}
