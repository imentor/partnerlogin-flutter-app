import 'package:json_annotation/json_annotation.dart';

part 'ws_facebook_ads_response_model.g.dart';

@JsonSerializable()
class FacebookAd {
  @JsonKey(name: 'AdId')
  String? adId;
  @JsonKey(name: 'StartDate')
  String? startDate;
  @JsonKey(name: 'EndDate')
  String? endDate;
  @JsonKey(name: 'ImageURL')
  String? imageURL;
  @JsonKey(name: 'AdName')
  String? adName;
  @JsonKey(name: 'AdDescription')
  String? adDescription;
  @JsonKey(name: 'Reach')
  int? reach;
  @JsonKey(name: 'Clicks')
  int? clicks;
  @JsonKey(name: 'AmountSpent')
  int? amountSpent;
  @JsonKey(name: 'AdStatus')
  String? adStatus;
  @JsonKey(name: 'BudgetRemaining')
  int? budgetRemaining;
  @JsonKey(name: 'LifetimeBudget')
  int? lifetimeBudget;
  @JsonKey(name: 'DailyBudget')
  int? dailyBudget;
  @JsonKey(name: 'TotalItemCount')
  int? totalItemCount;
  @JsonKey(name: 'LastPage')
  int? lastPage;

  FacebookAd(
      {this.adId,
      this.startDate,
      this.endDate,
      this.imageURL,
      this.adName,
      this.adDescription,
      this.reach,
      this.clicks,
      this.amountSpent,
      this.adStatus,
      this.budgetRemaining,
      this.lifetimeBudget,
      this.dailyBudget,
      this.totalItemCount,
      this.lastPage});

  factory FacebookAd.fromJson(Map<String, dynamic> json) =>
      _$FacebookAdFromJson(json);

  Map<String, dynamic> toJson() => _$FacebookAdToJson(this);
}
