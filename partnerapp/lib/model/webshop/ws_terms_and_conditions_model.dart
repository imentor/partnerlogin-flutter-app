import 'package:json_annotation/json_annotation.dart';

part 'ws_terms_and_conditions_model.g.dart';

class TermsAndConditionsResponseModel {
  bool? success;
  List<TermsAndConditionsResponse>? data;

  TermsAndConditionsResponseModel({this.success, this.data});

  TermsAndConditionsResponseModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data!.add(TermsAndConditionsResponse.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['success'] = success;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

@JsonSerializable()
class TermsAndConditionsResponse {
  @JsonKey(name: 'ID')
  String? id;
  @JsonKey(name: 'IBLOCK_ID')
  String? iblockId;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'PROPERTY_TERMS_AND_CONDITIONS_VALUE')
  PropertyTermsAndConditionsValue? propertyTermsAndConditionsValue;
  @JsonKey(name: 'CNT')
  String? cnt;

  TermsAndConditionsResponse(
      {this.id,
      this.iblockId,
      this.name,
      this.propertyTermsAndConditionsValue,
      this.cnt});

  factory TermsAndConditionsResponse.fromJson(Map<String, dynamic> json) =>
      _$TermsAndConditionsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$TermsAndConditionsResponseToJson(this);
}

@JsonSerializable()
class PropertyTermsAndConditionsValue {
  @JsonKey(name: 'TEXT')
  String? text;
  @JsonKey(name: 'TYPE')
  String? type;

  PropertyTermsAndConditionsValue({this.text, this.type});

  factory PropertyTermsAndConditionsValue.fromJson(Map<String, dynamic> json) =>
      _$PropertyTermsAndConditionsValueFromJson(json);

  Map<String, dynamic> toJson() =>
      _$PropertyTermsAndConditionsValueToJson(this);
}
