import 'package:json_annotation/json_annotation.dart';

part 'ws_add_product_model.g.dart';

@JsonSerializable()
class WsAddProductModel {
  final bool? success;
  final AddProductData? data;
  final String? message;

  const WsAddProductModel({this.success, this.data, this.message});

  factory WsAddProductModel.fromJson(Map<String, dynamic> json) =>
      _$WsAddProductModelFromJson(json);

  Map<String, dynamic> toJson() => _$WsAddProductModelToJson(this);
}

@JsonSerializable()
class AddProductData {
  final dynamic result;
  final dynamic time;

  const AddProductData({
    this.result,
    this.time,
  });

  factory AddProductData.fromJson(Map<String, dynamic> json) =>
      _$AddProductDataFromJson(json);

  Map<String, dynamic> toJson() => _$AddProductDataToJson(this);
}
