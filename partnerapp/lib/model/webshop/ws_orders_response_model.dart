import 'package:json_annotation/json_annotation.dart';

part 'ws_orders_response_model.g.dart';

@JsonSerializable()
class WebshopOrdersResponseModel {
  bool? success;
  Data? data;

  WebshopOrdersResponseModel({
    this.success,
    this.data,
  });

  factory WebshopOrdersResponseModel.fromJson(Map<String, dynamic> json) =>
      _$WebshopOrdersResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$WebshopOrdersResponseModelToJson(this);

  @override
  String toString() =>
      'WebshopOrdersResponseModel(success: $success, data: $data)';
}

@JsonSerializable()
class Data {
  @JsonKey(name: 'current_page')
  int? currentPage;
  @JsonKey(name: 'data')
  List<Order>? products;
  @JsonKey(name: 'first_page_url')
  String? firstPageUrl;
  int? from;
  @JsonKey(name: 'last_page')
  int? lastPage;
  @JsonKey(name: 'last_page_url')
  String? lastPageUrl;
  List<Links>? links;
  @JsonKey(name: 'next_page_url')
  dynamic nextPageUrl;
  String? path;
  @JsonKey(name: 'per_page')
  int? perPage;
  @JsonKey(name: 'prev_page_url')
  dynamic prevPageUrl;
  int? to;
  int? total;
  Data({
    this.currentPage,
    this.products,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.links,
    this.nextPageUrl,
    this.path,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  Map<String, dynamic> toJson() => _$DataToJson(this);

  @override
  String toString() {
    return 'Data(currentPage: $currentPage, products: $products, firstPageUrl: $firstPageUrl, from: $from, lastPage: $lastPage, lastPageUrl: $lastPageUrl, links: $links, nextPageUrl: $nextPageUrl, path: $path, perPage: $perPage, prevPageUrl: $prevPageUrl, to: $to, total: $total)';
  }
}

@JsonSerializable()
class Order {
  @JsonKey(name: 'ID')
  dynamic id;
  @JsonKey(name: 'LID')
  dynamic lid;
  @JsonKey(name: 'PERSON_TYPE_ID')
  dynamic personTypeId;
  @JsonKey(name: 'PAYED')
  dynamic payed;
  @JsonKey(name: 'DATE_PAYED')
  dynamic datePayed;
  @JsonKey(name: 'EMP_PAYED_ID')
  dynamic empPayedId;
  @JsonKey(name: 'CANCELED')
  dynamic cancelled;
  @JsonKey(name: 'DATE_CANCELED')
  dynamic dateCancelled;
  @JsonKey(name: 'EMP_CANCELED_ID')
  dynamic empCancelledId;
  @JsonKey(name: 'REASON_CANCELED')
  dynamic reasonCancelled;
  @JsonKey(name: 'STATUS_ID')
  dynamic statudId;
  @JsonKey(name: 'DATE_STATUS')
  dynamic dateStatus;
  @JsonKey(name: 'EMP_STATUS_ID')
  dynamic empStatusId;
  @JsonKey(name: 'PRICE_DELIVERY')
  dynamic priceDelivery;
  @JsonKey(name: 'PRICE_PAYMENT')
  dynamic pricePayment;
  @JsonKey(name: 'ALLOW_DELIVERY')
  dynamic allowDelivery;
  @JsonKey(name: 'DATE_ALLOW_DELIVERY')
  dynamic dateAllowdelivery;
  @JsonKey(name: 'EMP_ALLOW_DELIVERY_ID')
  dynamic empAllowDeliveryId;
  @JsonKey(name: 'DEDUCTED')
  dynamic deducted;
  @JsonKey(name: 'DATE_DEDUCTED')
  dynamic dateDeducted;
  @JsonKey(name: 'EMP_DEDUCTED_ID')
  dynamic empDeductedId;
  @JsonKey(name: 'REASON_UNDO_DEDUCTED')
  dynamic reasonUndoDeducted;
  @JsonKey(name: 'MARKED')
  dynamic marked;
  @JsonKey(name: 'DATE_MARKED')
  dynamic dateMarked;
  @JsonKey(name: 'EMP_MARKED_ID')
  dynamic empMarkedId;
  @JsonKey(name: 'REASON_MARKED')
  dynamic reasonMarked;
  @JsonKey(name: 'RESERVED')
  dynamic reserved;
  @JsonKey(name: 'PRICE')
  dynamic price;
  @JsonKey(name: 'CURRENCY')
  dynamic currency;
  @JsonKey(name: 'DISCOUNT_VALUE')
  dynamic discountValue;
  @JsonKey(name: 'USER_ID')
  dynamic userId;
  @JsonKey(name: 'PAY_SYSTEM_ID')
  dynamic paySystemId;
  @JsonKey(name: 'DELIVERY_ID')
  dynamic deliveryId;
  @JsonKey(name: 'DATE_INSERT')
  dynamic dateInsert;
  @JsonKey(name: 'DATE_UPDATE')
  dynamic dateUpdate;
  @JsonKey(name: 'USER_DESCRIPTION')
  dynamic userDescription;
  @JsonKey(name: 'ADDITIONAL_INFO')
  dynamic additionalInfo;
  @JsonKey(name: 'PS_STATUS')
  dynamic psStatus;
  @JsonKey(name: 'PS_STATUS_CODE')
  dynamic psStatusCode;
  @JsonKey(name: 'PS_STATUS_DESCRIPTION')
  dynamic psStatusDescription;
  @JsonKey(name: 'PS_STATUS_MESSAGE')
  dynamic psStatusMessage;
  @JsonKey(name: 'PS_SUM')
  dynamic psSum;
  @JsonKey(name: 'PS_CURRENCY')
  dynamic psCurrency;
  @JsonKey(name: 'PS_RESPONSE_DATE')
  dynamic psResponseDate;
  @JsonKey(name: 'COMMENTS')
  dynamic comments;
  @JsonKey(name: 'TAX_VALUE')
  dynamic taxValue;
  @JsonKey(name: 'STAT_GID')
  dynamic statGid;
  @JsonKey(name: 'SUM_PAID')
  dynamic sumPaid;
  @JsonKey(name: 'IS_RECURRING')
  dynamic isRecurring;
  @JsonKey(name: 'RECURRING_ID')
  dynamic recurringId;
  @JsonKey(name: 'PAY_VOUCHER_NUM')
  dynamic payVoucherNum;
  @JsonKey(name: 'PAY_VOUCHER_DATE')
  dynamic payVoucherDate;
  @JsonKey(name: 'LOCKED_BY')
  dynamic lockedBy;
  @JsonKey(name: 'DATE_LOCK')
  dynamic dateLock;
  @JsonKey(name: 'RECOUNT_FLAG')
  dynamic recountFlag;
  @JsonKey(name: 'AFFILIATE_ID')
  dynamic affiliateId;
  @JsonKey(name: 'DELIVERY_DOC_NUM')
  dynamic deliveryDocNum;
  @JsonKey(name: 'DELIVERY_DOC_DATE')
  dynamic deliveryDocDate;
  @JsonKey(name: 'UPDATED_1C')
  dynamic updated1c;
  @JsonKey(name: 'STORE_ID')
  dynamic storeId;
  @JsonKey(name: 'ORDER_TOPIC')
  dynamic orderTopic;
  @JsonKey(name: 'CREATED_BY')
  dynamic createdBy;
  @JsonKey(name: 'RESPONSIBLE_ID')
  dynamic responsibleId;
  @JsonKey(name: 'COMPANY_ID')
  dynamic companyId;
  @JsonKey(name: 'DATE_PAY_BEFORE')
  dynamic datePayBefore;
  @JsonKey(name: 'DATE_BILL')
  dynamic dateBill;
  @JsonKey(name: 'ACCOUNT_NUMBER')
  dynamic accountNumber;
  @JsonKey(name: 'TRACKING_NUMBER')
  dynamic trackingNumber;
  @JsonKey(name: 'XML_ID')
  dynamic xmlId;
  @JsonKey(name: 'ID_1C')
  dynamic id1c;
  @JsonKey(name: 'VERSION_1C')
  dynamic version1c;
  @JsonKey(name: 'VERSION')
  dynamic version;
  @JsonKey(name: 'EXTERNAL_ORDER')
  dynamic externalOrder;
  @JsonKey(name: 'RUNNING')
  dynamic running;
  @JsonKey(name: 'BX_USER_ID')
  dynamic bxUserId;
  @JsonKey(name: 'SEARCH_CONTENT')
  dynamic searchContent;
  @JsonKey(name: 'UF_COMPANY_ID')
  dynamic ufCompanyId;
  @JsonKey(name: 'STAGE_ID')
  dynamic stageId;
  @JsonKey(name: 'OWNER_ID')
  dynamic ownerId;
  @JsonKey(name: 'webshop_invoice_products')
  List<InvoiceProduct>? invoiceProducts;

  Order({
    this.id,
    this.lid,
    this.personTypeId,
    this.payed,
    this.datePayed,
    this.empPayedId,
    this.cancelled,
    this.dateCancelled,
    this.empCancelledId,
    this.reasonCancelled,
    this.statudId,
    this.dateStatus,
    this.empStatusId,
    this.priceDelivery,
    this.pricePayment,
    this.allowDelivery,
    this.dateAllowdelivery,
    this.empAllowDeliveryId,
    this.deducted,
    this.dateDeducted,
    this.empDeductedId,
    this.reasonUndoDeducted,
    this.marked,
    this.dateMarked,
    this.empMarkedId,
    this.reasonMarked,
    this.reserved,
    this.price,
    this.currency,
    this.discountValue,
    this.userId,
    this.paySystemId,
    this.deliveryId,
    this.dateInsert,
    this.dateUpdate,
    this.userDescription,
    this.additionalInfo,
    this.psStatus,
    this.psStatusCode,
    this.psStatusDescription,
    this.psStatusMessage,
    this.psSum,
    this.psCurrency,
    this.psResponseDate,
    this.comments,
    this.taxValue,
    this.statGid,
    this.sumPaid,
    this.isRecurring,
    this.recurringId,
    this.payVoucherNum,
    this.payVoucherDate,
    this.lockedBy,
    this.dateLock,
    this.recountFlag,
    this.affiliateId,
    this.deliveryDocNum,
    this.deliveryDocDate,
    this.updated1c,
    this.storeId,
    this.orderTopic,
    this.createdBy,
    this.responsibleId,
    this.companyId,
    this.datePayBefore,
    this.dateBill,
    this.accountNumber,
    this.trackingNumber,
    this.xmlId,
    this.id1c,
    this.version1c,
    this.version,
    this.externalOrder,
    this.running,
    this.bxUserId,
    this.searchContent,
    this.ufCompanyId,
    this.stageId,
    this.ownerId,
    this.invoiceProducts,
  });

  factory Order.fromJson(Map<String, dynamic> json) => _$OrderFromJson(json);
  Map<String, dynamic> toJson() => _$OrderToJson(this);

  @override
  String toString() {
    return 'Order(id: $id, lid: $lid, personTypeId: $personTypeId, payed: $payed, datePayed: $datePayed, empPayedId: $empPayedId, cancelled: $cancelled, dateCancelled: $dateCancelled, empCancelledId: $empCancelledId, reasonCancelled: $reasonCancelled, statudId: $statudId, dateStatus: $dateStatus, empStatusId: $empStatusId, priceDelivery: $priceDelivery, pricePayment: $pricePayment, allowDelivery: $allowDelivery, dateAllowdelivery: $dateAllowdelivery, empAllowDeliveryId: $empAllowDeliveryId, deducted: $deducted, dateDeducted: $dateDeducted, empDeductedId: $empDeductedId, reasonUndoDeducted: $reasonUndoDeducted, marked: $marked, dateMarked: $dateMarked, empMarkedId: $empMarkedId, reasonMarked: $reasonMarked, reserved: $reserved, price: $price, currency: $currency, discountValue: $discountValue, userId: $userId, paySystemId: $paySystemId, deliveryId: $deliveryId, dateInsert: $dateInsert, dateUpdate: $dateUpdate, userDescription: $userDescription, additionalInfo: $additionalInfo, psStatus: $psStatus, psStatusCode: $psStatusCode, psStatusDescription: $psStatusDescription, psStatusMessage: $psStatusMessage, psSum: $psSum, psCurrency: $psCurrency, psResponseDate: $psResponseDate, comments: $comments, taxValue: $taxValue, statGid: $statGid, sumPaid: $sumPaid, isRecurring: $isRecurring, recurringId: $recurringId, payVoucherNum: $payVoucherNum, payVoucherDate: $payVoucherDate, lockedBy: $lockedBy, dateLock: $dateLock, recountFlag: $recountFlag, affiliateId: $affiliateId, deliveryDocNum: $deliveryDocNum, deliveryDocDate: $deliveryDocDate, updated1c: $updated1c, storeId: $storeId, orderTopic: $orderTopic, createdBy: $createdBy, responsibleId: $responsibleId, companyId: $companyId, datePayBefore: $datePayBefore, dateBill: $dateBill, accountNumber: $accountNumber, trackingNumber: $trackingNumber, xmlId: $xmlId, id1c: $id1c, version1c: $version1c, version: $version, externalOrder: $externalOrder, running: $running, bxUserId: $bxUserId, searchContent: $searchContent, ufCompanyId: $ufCompanyId, stageId: $stageId, ownerId: $ownerId, invoiceProducts: $invoiceProducts)';
  }
}

@JsonSerializable()
class InvoiceProduct {
  @JsonKey(name: 'ID')
  dynamic id;
  @JsonKey(name: 'FUSER_ID')
  dynamic fuserId;
  @JsonKey(name: 'ORDER_ID')
  dynamic orderId;
  @JsonKey(name: 'PRODUCT_ID')
  dynamic productId;
  @JsonKey(name: 'PRODUCT_PRICE_ID')
  dynamic productPriceId;
  @JsonKey(name: 'PRICE_TYPE_ID')
  dynamic priceTypeId;
  @JsonKey(name: 'PRICE')
  dynamic price;
  @JsonKey(name: 'CURRENCY')
  dynamic currency;
  @JsonKey(name: 'BASE_PRICE')
  dynamic basePrice;
  @JsonKey(name: 'VAT_INCLUDED')
  dynamic vatIncluded;
  @JsonKey(name: 'DATE_INSERT')
  dynamic dateInsert;
  @JsonKey(name: 'DATE_UPDATE')
  dynamic dateUpdate;
  @JsonKey(name: 'DATE_REFRESH')
  dynamic dateRefresh;
  @JsonKey(name: 'WEIGHT')
  dynamic weight;
  @JsonKey(name: 'QUANTITY')
  dynamic quantity;
  @JsonKey(name: 'LID')
  dynamic lid;
  @JsonKey(name: 'DELAY')
  dynamic delay;
  @JsonKey(name: 'NAME')
  dynamic name;
  @JsonKey(name: 'CAN_BUY')
  dynamic canBuy;
  @JsonKey(name: 'MARKING_CODE_GROUP')
  dynamic markingCodeGroup;
  @JsonKey(name: 'MODULE')
  dynamic module;
  @JsonKey(name: 'CALLBACK_FUNC')
  dynamic callbackFunc;
  @JsonKey(name: 'NOTES')
  dynamic notes;
  @JsonKey(name: 'ORDER_CALLBACK_FUNC')
  dynamic orderCallbackFunc;
  @JsonKey(name: 'DETAIL_PAGE_URL')
  dynamic detailPageUrl;
  @JsonKey(name: 'DISCOUNT_PRICE')
  dynamic discountPrice;
  @JsonKey(name: 'CANCEL_CALLBACK_FUNC')
  dynamic cancelCallbackFunc;
  @JsonKey(name: 'PAY_CALLBACK_FUNC')
  dynamic payCallbackFunc;
  @JsonKey(name: 'PRODUCT_PROVIDER_CLASS')
  dynamic productProviderClass;
  @JsonKey(name: 'CATALOG_XML_ID')
  dynamic catalogXmlId;
  @JsonKey(name: 'PRODUCT_XML_ID')
  dynamic productXmlId;
  @JsonKey(name: 'DISCOUNT_NAME')
  dynamic discountName;
  @JsonKey(name: 'DISCOUNT_VALUE')
  dynamic discountValue;
  @JsonKey(name: 'DISCOUNT_COUPON')
  dynamic discountCoupon;
  @JsonKey(name: 'VAT_RATE')
  dynamic vatRate;
  @JsonKey(name: 'SUBSCRIBE')
  dynamic subscribe;
  @JsonKey(name: 'DEDUCTED')
  dynamic deducted;
  @JsonKey(name: 'RESERVED')
  dynamic reserved;
  @JsonKey(name: 'BARCODE_MULTI')
  dynamic barcodeMulti;
  @JsonKey(name: 'RESERVE_QUANTITY')
  dynamic reserveQuantity;
  @JsonKey(name: 'CUSTOM_PRICE')
  dynamic customPrice;
  @JsonKey(name: 'DIMENSIONS')
  dynamic dimensions;
  @JsonKey(name: 'TYPE')
  dynamic type;
  @JsonKey(name: 'SET_PARENT_ID')
  dynamic setParentId;
  @JsonKey(name: 'MEASURE_CODE')
  dynamic measureCode;
  @JsonKey(name: 'MEASURE_NAME')
  dynamic measureName;
  @JsonKey(name: 'RECOMMENDATION')
  dynamic recommendation;
  @JsonKey(name: 'XML_ID')
  dynamic xmlId;
  @JsonKey(name: 'SORT')
  dynamic sort;
  @JsonKey(name: 'VALUE_ID')
  dynamic valueId;
  @JsonKey(name: 'UF_DEAL_ID')
  dynamic ufDealId;
  @JsonKey(name: 'UF_QUOTE_ID')
  dynamic ufQuoteId;
  @JsonKey(name: 'UF_COMPANY_ID')
  dynamic ufCompanyId;
  @JsonKey(name: 'UF_CONTACT_ID')
  dynamic ufContactId;
  @JsonKey(name: 'UF_MYCOMPANY_ID')
  dynamic ufMyCompanyId;
  @JsonKey(name: 'UF_CRM_5F7AB268690E0')
  dynamic ufCrm5F7AB268690E0;
  @JsonKey(name: 'UF_CRM_5F7ADCDB34147')
  dynamic ufCrmUF5F7ADCDB34147;
  @JsonKey(name: 'CODE')
  dynamic code;
  @JsonKey(name: 'ACTIVE')
  dynamic active;
  @JsonKey(name: 'PREVIEW_PICTURE')
  dynamic previewPicture;
  @JsonKey(name: 'DETAIL_PICTURE')
  dynamic detailPicture;
  @JsonKey(name: 'TIMESTAMP_X')
  dynamic timeStampX;
  @JsonKey(name: 'DATE_CREATE')
  dynamic dateCreate;
  @JsonKey(name: 'MODIFIED_BY')
  dynamic modifiedBy;
  @JsonKey(name: 'CREATED_BY')
  dynamic createdBy;
  @JsonKey(name: 'CATALOG_ID')
  dynamic catalogId;
  @JsonKey(name: 'SECTION_ID')
  dynamic sectionId;
  @JsonKey(name: 'DESCRIPTION')
  dynamic description;
  @JsonKey(name: 'DESCRIPTION_TYPE')
  dynamic descriptionType;
  @JsonKey(name: 'CURRENCY_ID')
  dynamic currencyId;
  @JsonKey(name: 'VAT_ID')
  dynamic vatId;
  @JsonKey(name: 'MEASURE')
  dynamic measure;
  @JsonKey(name: 'CATEGORY')
  dynamic category;
  @JsonKey(name: 'CATEGORY_ID')
  dynamic categoryId;
  @JsonKey(name: 'PRODUCT_GROUP')
  dynamic productGroup;
  @JsonKey(name: 'NUMBERS')
  dynamic numbers;
  @JsonKey(name: 'SIZE')
  dynamic size;
  @JsonKey(name: 'PRODUCT_TITLE')
  dynamic productTitle;
  @JsonKey(name: 'ORDER_EMAIL')
  dynamic orderEmail;
  @JsonKey(name: 'PARTNER_EMAIL')
  dynamic partnerEmail;
  @JsonKey(name: 'BACKPRINT')
  dynamic backPrint;
  @JsonKey(name: 'webshop_uploaded_image')
  List<UploadedImage>? uploadedImage;

  InvoiceProduct({
    this.id,
    this.fuserId,
    this.orderId,
    this.productId,
    this.productPriceId,
    this.priceTypeId,
    this.price,
    this.currency,
    this.basePrice,
    this.vatIncluded,
    this.dateInsert,
    this.dateUpdate,
    this.dateRefresh,
    this.weight,
    this.quantity,
    this.lid,
    this.delay,
    this.name,
    this.canBuy,
    this.markingCodeGroup,
    this.module,
    this.callbackFunc,
    this.notes,
    this.orderCallbackFunc,
    this.detailPageUrl,
    this.discountPrice,
    this.cancelCallbackFunc,
    this.payCallbackFunc,
    this.productProviderClass,
    this.catalogXmlId,
    this.productXmlId,
    this.discountName,
    this.discountValue,
    this.discountCoupon,
    this.vatRate,
    this.subscribe,
    this.deducted,
    this.reserved,
    this.barcodeMulti,
    this.reserveQuantity,
    this.customPrice,
    this.dimensions,
    this.type,
    this.setParentId,
    this.measureCode,
    this.measureName,
    this.recommendation,
    this.xmlId,
    this.sort,
    this.valueId,
    this.ufDealId,
    this.ufQuoteId,
    this.ufCompanyId,
    this.ufContactId,
    this.ufMyCompanyId,
    this.ufCrm5F7AB268690E0,
    this.ufCrmUF5F7ADCDB34147,
    this.code,
    this.active,
    this.previewPicture,
    this.detailPicture,
    this.timeStampX,
    this.dateCreate,
    this.modifiedBy,
    this.createdBy,
    this.catalogId,
    this.sectionId,
    this.description,
    this.descriptionType,
    this.currencyId,
    this.vatId,
    this.measure,
    this.category,
    this.categoryId,
    this.productGroup,
    this.numbers,
    this.size,
    this.productTitle,
    this.orderEmail,
    this.partnerEmail,
    this.backPrint,
    this.uploadedImage,
  });

  factory InvoiceProduct.fromJson(Map<String, dynamic> json) =>
      _$InvoiceProductFromJson(json);
  Map<String, dynamic> toJson() => _$InvoiceProductToJson(this);

  @override
  String toString() {
    return 'InvoiceProduct(id: $id, fuserId: $fuserId, orderId: $orderId, productId: $productId, productPriceId: $productPriceId, priceTypeId: $priceTypeId, price: $price, currency: $currency, basePrice: $basePrice, vatIncluded: $vatIncluded, dateInsert: $dateInsert, dateUpdate: $dateUpdate, dateRefresh: $dateRefresh, weight: $weight, quantity: $quantity, lid: $lid, delay: $delay, name: $name, canBuy: $canBuy, markingCodeGroup: $markingCodeGroup, module: $module, callbackFunc: $callbackFunc, notes: $notes, orderCallbackFunc: $orderCallbackFunc, detailPageUrl: $detailPageUrl, discountPrice: $discountPrice, cancelCallbackFunc: $cancelCallbackFunc, payCallbackFunc: $payCallbackFunc, productProviderClass: $productProviderClass, catalogXmlId: $catalogXmlId, productXmlId: $productXmlId, discountName: $discountName, discountValue: $discountValue, discountCoupon: $discountCoupon, vatRate: $vatRate, subscribe: $subscribe, deducted: $deducted, reserved: $reserved, barcodeMulti: $barcodeMulti, reserveQuantity: $reserveQuantity, customPrice: $customPrice, dimensions: $dimensions, type: $type, setParentId: $setParentId, measureCode: $measureCode, measureName: $measureName, recommendation: $recommendation, xmlId: $xmlId, sort: $sort, valueId: $valueId, ufDealId: $ufDealId, ufQuoteId: $ufQuoteId, ufCompanyId: $ufCompanyId, ufContactId: $ufContactId, ufMyCompanyId: $ufMyCompanyId, ufCrm5F7AB268690E0: $ufCrm5F7AB268690E0, ufCrmUF5F7ADCDB34147: $ufCrmUF5F7ADCDB34147, uploadedImage: $uploadedImage)';
  }
}

@JsonSerializable()
class UploadedImage {
  @JsonKey(name: 'ID')
  dynamic id;
  @JsonKey(name: 'DEAL_ID')
  dynamic dealId;
  @JsonKey(name: 'PRODUCT_ID')
  dynamic productId;
  @JsonKey(name: 'IMAGE_NAME')
  dynamic imageName;
  @JsonKey(name: 'SIZE')
  dynamic size;
  @JsonKey(name: 'BACKPRINT')
  dynamic backPrint;

  UploadedImage({
    this.id,
    this.dealId,
    this.productId,
    this.imageName,
    this.size,
    this.backPrint,
  });

  factory UploadedImage.fromJson(Map<String, dynamic> json) =>
      _$UploadedImageFromJson(json);
  Map<String, dynamic> toJson() => _$UploadedImageToJson(this);

  @override
  String toString() {
    return 'UploadedImage(id: $id, dealId: $dealId, productId: $productId, imageName: $imageName, size: $size, backPrint: $backPrint)';
  }
}

@JsonSerializable()
class Links {
  String? url;

  dynamic label;

  bool? active;

  Links({this.url, this.label, this.active});

  factory Links.fromJson(Map<String, dynamic> json) => _$LinksFromJson(json);

  Map<String, dynamic> toJson() => _$LinksToJson(this);

  @override
  String toString() => 'Links(url: $url, label: $label, active: $active)';
}
