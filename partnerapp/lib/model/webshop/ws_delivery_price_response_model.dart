import 'package:json_annotation/json_annotation.dart';

part 'ws_delivery_price_response_model.g.dart';

/// {
///    "success": true,
///     "data": {
///        "CURRENCY": "DKK",
///     "PRICE": "00.00",
///       "PERIOD": {
///          "FROM": "0",
///        "TO": "0",
///           "TYPE": "D"
///       }
///    }
/// }

@JsonSerializable()
class WebshopDeliveryPriceResponseModel {
  final bool? success;
  final WebshopDeliveryPriceData? data;

  WebshopDeliveryPriceResponseModel({this.success, this.data});

  factory WebshopDeliveryPriceResponseModel.fromJson(
          Map<String, dynamic> json) =>
      _$WebshopDeliveryPriceResponseModelFromJson(json);

  Map<String, dynamic> toJson() =>
      _$WebshopDeliveryPriceResponseModelToJson(this);
}

@JsonSerializable()
class WebshopDeliveryPriceData {
  @JsonKey(name: "CURRENCY")
  final String? currency;
  @JsonKey(name: "PRICE")
  final String? price;
  @JsonKey(name: "PERIOD")
  final WebshopDeliveryPricePeriod? period;

  WebshopDeliveryPriceData({
    this.currency,
    this.price,
    this.period,
  });

  factory WebshopDeliveryPriceData.fromJson(Map<String, dynamic> json) =>
      _$WebshopDeliveryPriceDataFromJson(json);

  Map<String, dynamic> toJson() => _$WebshopDeliveryPriceDataToJson(this);
}

@JsonSerializable()
class WebshopDeliveryPricePeriod {
  @JsonKey(name: 'FROM')
  final String? from;
  @JsonKey(name: 'TO')
  final String? to;
  @JsonKey(name: 'TYPE')
  final String? type;

  WebshopDeliveryPricePeriod({
    this.from,
    this.to,
    this.type,
  });

  factory WebshopDeliveryPricePeriod.fromJson(Map<String, dynamic> json) =>
      _$WebshopDeliveryPricePeriodFromJson(json);

  Map<String, dynamic> toJson() => _$WebshopDeliveryPricePeriodToJson(this);
}
