import 'package:json_annotation/json_annotation.dart';

part 'ws_category_response_model.g.dart';

@JsonSerializable()
class WebshopCategoryResponseModel {
  bool? success;
  List<WebshopCategoryModel>? data;

  WebshopCategoryResponseModel({this.success, this.data});

  factory WebshopCategoryResponseModel.fromJson(Map<String, dynamic> json) =>
      _$WebshopCategoryResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$WebshopCategoryResponseModelToJson(this);

  @override
  String toString() =>
      'WebshopCategoryResponseModel(success: $success, data: $data)';
}

@JsonSerializable()
class WebshopCategoryModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'PROPERTY_ID')
  int? propertyId;
  @JsonKey(name: 'VALUE')
  String? value;
  @JsonKey(name: 'DEF')
  String? def;
  @JsonKey(name: 'SORT')
  int? sort;
  @JsonKey(name: 'XML_ID')
  String? xmlid;
  @JsonKey(name: 'TMP_ID')
  dynamic tmpid;
  @JsonKey(name: 'PRODUCT_COUNT')
  int? productCount;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'IMAGE_URL')
  String? imageUrl;

  WebshopCategoryModel({
    this.id,
    this.propertyId,
    this.value,
    this.def,
    this.sort,
    this.xmlid,
    this.tmpid,
    this.productCount,
    this.description,
    this.imageUrl,
  });

  factory WebshopCategoryModel.fromJson(Map<String, dynamic> json) =>
      _$WebshopCategoryModelFromJson(json);

  Map<String, dynamic> toJson() => _$WebshopCategoryModelToJson(this);

  @override
  String toString() {
    return 'WebshopCategoryModel(id: $id, propertyId: $propertyId, value: $value, def: $def, sort: $sort, xmlid: $xmlid, tmpid: $tmpid, productCount: $productCount, description: $description, imageUrl: $imageUrl)';
  }
}
