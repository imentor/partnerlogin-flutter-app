import 'package:json_annotation/json_annotation.dart';

part 'ws_google_ads_response_model.g.dart';

@JsonSerializable()
class GoogleAd {
  @JsonKey(name: 'AdId')
  final dynamic adId;
  @JsonKey(name: 'campaignID')
  final dynamic campaignID;
  @JsonKey(name: 'AdName')
  final String? adName;
  @JsonKey(name: 'adGroupName')
  final String? adGroupName;
  @JsonKey(name: 'campaignName')
  final String? campaignName;
  @JsonKey(name: 'AdGroupId')
  final dynamic adGroupId;
  // ENABLED, PAUSED, DISABLED
  @JsonKey(name: 'AdStatus')
  final dynamic adStatus;
  @JsonKey(name: 'campaignStatus')
  final dynamic campaignStatus;
  @JsonKey(name: 'CampaignStartDate')
  final String? dateStart;
  @JsonKey(name: 'CampaignEtartDate')
  final String? dateEnd;
  @JsonKey(name: 'Clicks')
  final int? clicks;
  @JsonKey(name: 'Impressions')
  final int? impressions;
  @JsonKey(name: 'Cost')
  final double? cost;
  @JsonKey(name: 'Budget')
  final double? budget;
  @JsonKey(name: 'TotalItemCount')
  final int? totalItemCount;
  // @JsonKey(name: 'TagId')
  // final String tagId;

  const GoogleAd(
      {this.adId,
      this.adName,
      this.adGroupId,
      this.adStatus,
      this.dateStart,
      this.dateEnd,
      this.clicks,
      this.impressions,
      this.cost,
      this.budget,
      // this.tagId,
      this.adGroupName,
      this.campaignID,
      this.campaignName,
      this.campaignStatus,
      this.totalItemCount});

  factory GoogleAd.fromJson(Map<String, dynamic> json) =>
      _$GoogleAdFromJson(json);
  Map<String, dynamic> toJson() => _$GoogleAdToJson(this);
}
