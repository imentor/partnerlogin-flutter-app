import 'package:json_annotation/json_annotation.dart';

part 'address_response_model.g.dart';

@JsonSerializable()
class AddressResponseModel {
  AddressResponseModel({this.tekst, this.adresse});

  @JsonKey(name: 'tekst')
  final String? tekst;
  @JsonKey(name: 'data')
  Adresse? adresse;

  factory AddressResponseModel.fromJson(Map<String, dynamic> json) =>
      _$AddressResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$AddressResponseModelToJson(this);
  static List<AddressResponseModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => AddressResponseModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class Adresse {
  Adresse({
    this.id,
    this.status,
    this.darstatus,
    this.vejkode,
    this.vejnavn,
    this.adresseringsvejnavn,
    this.husnr,
    this.etage,
    this.dR,
    this.supplerendebynavn,
    this.postnr,
    this.postnrnavn,
    this.stormodtagerpostnr,
    this.stormodtagerpostnrnavn,
    this.kommunekode,
    this.adgangsadresseid,
    this.x,
    this.y,
    this.href,
  });

  @JsonKey(name: 'id')
  final String? id;
  @JsonKey(name: 'status')
  final int? status;
  @JsonKey(name: 'darstatus')
  final int? darstatus;
  @JsonKey(name: 'vejkode')
  final String? vejkode;
  @JsonKey(name: 'vejnavn')
  final String? vejnavn;
  @JsonKey(name: 'adresseringsvejnavn')
  final String? adresseringsvejnavn;
  @JsonKey(name: 'husnr')
  final String? husnr;
  @JsonKey(name: 'etage')
  final dynamic etage;
  @JsonKey(name: 'dør')
  final dynamic dR;
  @JsonKey(name: 'supplerendebynavn')
  final dynamic supplerendebynavn;
  @JsonKey(name: 'postnr')
  final String? postnr;
  @JsonKey(name: 'postnrnavn')
  final String? postnrnavn;
  @JsonKey(name: 'stormodtagerpostnr')
  final dynamic stormodtagerpostnr;
  @JsonKey(name: 'stormodtagerpostnrnavn')
  final dynamic stormodtagerpostnrnavn;
  @JsonKey(name: 'kommunekode')
  final String? kommunekode;
  @JsonKey(name: 'adgangsadresseid')
  final String? adgangsadresseid;
  @JsonKey(name: 'x')
  final double? x;
  @JsonKey(name: 'y')
  final double? y;
  @JsonKey(name: 'href')
  final String? href;

  factory Adresse.fromJson(Map<String, dynamic> json) =>
      _$AdresseFromJson(json);

  Map<String, dynamic> toJson() => _$AdresseToJson(this);
  static List<Adresse> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => Adresse.fromJson(i as Map<String, dynamic>))
      .toList();
}
