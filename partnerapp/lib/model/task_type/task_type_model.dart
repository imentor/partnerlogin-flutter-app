import 'package:json_annotation/json_annotation.dart';

part 'task_type_model.g.dart';

@JsonSerializable()
class ProjectTaskTypes {
  @JsonKey(name: 'industryId')
  int? industryId;
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'name_en')
  String? nameEngLishTranslation;
  @JsonKey(name: 'industryPictureUrl')
  String? industryPictureUrl;
  @JsonKey(name: 'industryDescription')
  String? industryDescription;
  @JsonKey(name: 'MINBOLIG_DESC_DA')
  String? industryDescriptionDa;
  @JsonKey(name: 'MINBOLIG_DESC_EN')
  String? industryDescriptionEn;
  @JsonKey(name: 'subcategories')
  List<Subcategories>? subcategories;

  ProjectTaskTypes({
    this.industryDescription,
    this.industryDescriptionDa,
    this.industryDescriptionEn,
    this.industryId,
    this.industryPictureUrl,
    this.name,
    this.nameEngLishTranslation,
    this.subcategories,
  });

  ProjectTaskTypes.defaults()
      : industryDescription = '',
        industryDescriptionDa = '',
        industryDescriptionEn = '',
        industryId = 0,
        industryPictureUrl = '',
        name = '',
        nameEngLishTranslation = '',
        subcategories = [];

  factory ProjectTaskTypes.fromJson(Map<String, dynamic> json) =>
      _$ProjectTaskTypesFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectTaskTypesToJson(this);

  static List<ProjectTaskTypes> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => ProjectTaskTypes.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class Subcategories {
  @JsonKey(name: 'subcategoryId')
  int? subcategoryId;
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'taskTypes')
  List<SubcategoriesTaskTypes>? taskTypes;

  Subcategories({
    this.subcategoryId,
    this.name,
    this.taskTypes,
  });

  factory Subcategories.fromJson(Map<String, dynamic> json) =>
      _$SubcategoriesFromJson(json);

  Map<String, dynamic> toJson() => _$SubcategoriesToJson(this);

  static List<Subcategories> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => Subcategories.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class SubcategoriesTaskTypes {
  @JsonKey(name: 'taskTypeId')
  int? taskTypeId;
  @JsonKey(name: 'name')
  String? name;

  SubcategoriesTaskTypes({
    this.taskTypeId,
    this.name,
  });

  factory SubcategoriesTaskTypes.fromJson(Map<String, dynamic> json) =>
      _$SubcategoriesTaskTypesFromJson(json);

  Map<String, dynamic> toJson() => _$SubcategoriesTaskTypesToJson(this);

  static List<SubcategoriesTaskTypes> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => SubcategoriesTaskTypes.fromJson(i as Map<String, dynamic>))
      .toList();
}
