import 'package:json_annotation/json_annotation.dart';

part 'file_data_model.g.dart';

@JsonSerializable()
class FileData {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'NAME_DA')
  String? nameDa;
  @JsonKey(name: 'TYPE')
  String? type;
  @JsonKey(name: 'PARENT_ID')
  int? parentId;
  @JsonKey(name: 'SIZE')
  int? size;
  @JsonKey(name: 'DOWNLOAD_URL')
  String? downloadUrl;
  @JsonKey(name: 'BITRIX_URL')
  String? bitrixUrl;
  @JsonKey(name: 'TAGS')
  List<dynamic>? tags;
  @JsonKey(name: 'SHARED_BY')
  dynamic sharedBy;
  @JsonKey(name: 'CATEGORY')
  String? category;
  @JsonKey(name: 'TITLE')
  String? title;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'TAG_USER_IDS')
  List<dynamic>? tagUserIds;
  @JsonKey(name: 'CHILDREN_COUNT')
  int? childrenCount;
  @JsonKey(name: 'CREATED_AT')
  String? createdAt;
  @JsonKey(name: 'UPDATED_AT')
  String? updatedAt;
  @JsonKey(name: 'INFO')
  final FileDataInfo? info;
  @JsonKey(name: 'PARTNER_ID')
  int? partnerId;

  FileData({
    this.id,
    this.name,
    this.type,
    this.parentId,
    this.size,
    this.downloadUrl,
    this.bitrixUrl,
    this.tags,
    this.sharedBy,
    this.category,
    this.title,
    this.description,
    this.tagUserIds,
    this.childrenCount,
    this.createdAt,
    this.updatedAt,
    this.info,
    this.nameDa,
    this.partnerId,
  });

  factory FileData.fromJson(Map<String, dynamic> json) =>
      _$FileDataFromJson(json);

  Map<String, dynamic> toJson() => _$FileDataToJson(this);
  static List<FileData> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => FileData.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class PublicFileData {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'UF_ADDRESS_ID')
  int? addressId;
  @JsonKey(name: 'UF_ADDRESS')
  String? address;
  @JsonKey(name: 'UF_ADDRESS_TYPE')
  String? addressType;
  @JsonKey(name: 'UF_YEAR')
  dynamic year;
  @JsonKey(name: 'UF_DESCRIPTION')
  String? description;
  @JsonKey(name: 'UF_LINK')
  String? link;
  @JsonKey(name: 'UF_CREATED_AT')
  String? createdAt;
  @JsonKey(name: 'UF_UPDATED_AT')
  String? updatedAt;
  @JsonKey(name: 'UF_IS_DELETED')
  int? isDeleted;

  PublicFileData({
    this.id,
    this.addressId,
    this.address,
    this.addressType,
    this.year,
    this.description,
    this.link,
    this.createdAt,
    this.updatedAt,
    this.isDeleted,
  });

  factory PublicFileData.fromJson(Map<String, dynamic> json) =>
      _$PublicFileDataFromJson(json);

  Map<String, dynamic> toJson() => _$PublicFileDataToJson(this);
  static List<PublicFileData> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => PublicFileData.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class FileDataInfo {
  @JsonKey(name: 'userId')
  final int? userId;
  @JsonKey(name: 'projectId')
  final int? projectId;
  @JsonKey(name: 'type')
  final String? type;
  @JsonKey(name: 'category')
  final String? category;

  FileDataInfo({
    this.userId,
    this.projectId,
    this.type,
    this.category,
  });

  factory FileDataInfo.fromJson(Map<String, dynamic> json) =>
      _$FileDataInfoFromJson(json);

  Map<String, dynamic> toJson() => _$FileDataInfoToJson(this);
  static List<FileDataInfo> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => FileDataInfo.fromJson(i as Map<String, dynamic>))
      .toList();
}
