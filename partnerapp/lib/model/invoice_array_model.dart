import 'package:json_annotation/json_annotation.dart';

part 'invoice_array_model.g.dart';

@JsonSerializable()
class Invoice {
  @JsonKey(name: '0')
  String? s0;
  @JsonKey(name: '1')
  String? s1;
  @JsonKey(name: '2')
  String? s2;
  @JsonKey(name: '3')
  String? s3;
  @JsonKey(name: '4')
  String? s4;
  @JsonKey(name: '5')
  String? s5;
  @JsonKey(name: '6')
  String? s6;
  @JsonKey(name: '7')
  String? s7;
  @JsonKey(name: '8')
  String? s8;
  @JsonKey(name: '9')
  String? s9;
  @JsonKey(name: '10')
  String? s10;
  @JsonKey(name: '11')
  String? s11;
  @JsonKey(name: 'id_partner_invoice')
  String? idPartnerInvoice;
  @JsonKey(name: 'id_partner')
  String? idPartner;
  String? timeinvoice;
  String? total;
  String? invoicedate;
  String? payment;
  String? status;
  String? type;
  @JsonKey(name: 'invoice_status')
  String? invoiceStatus;
  @JsonKey(name: 'invoice_desc')
  String? invoiceDesc;
  String? nextmonth;
  @JsonKey(name: 'payment_status')
  String? paymentStatus;
  String? downloadurl;

  Invoice(
      {this.s0,
      this.s1,
      this.s2,
      this.s3,
      this.s4,
      this.s5,
      this.s6,
      this.s7,
      this.s8,
      this.s9,
      this.s10,
      this.s11,
      this.idPartnerInvoice,
      this.idPartner,
      this.timeinvoice,
      this.total,
      this.invoicedate,
      this.payment,
      this.status,
      this.type,
      this.invoiceStatus,
      this.invoiceDesc,
      this.nextmonth,
      this.paymentStatus,
      this.downloadurl});

  factory Invoice.fromJson(Map<String, dynamic> json) =>
      _$InvoiceFromJson(json);

  Map<String, dynamic> toJson() => _$InvoiceToJson(this);
}
