import 'package:json_annotation/json_annotation.dart';

part 'kanikke_popups_model.g.dart';

@JsonSerializable()
class KanIkkePopupsModel {
  KanIkkePopupsModel(
      {this.kanikkePopupText,
      this.kanikkeWarningPopupText,
      this.callFirstBeforeOffer,
      this.kanikkeConfirmationAcceptableStage,
      this.kanikkeConfirmationWarningStage});

  @JsonKey(name: 'kanikkePopupText')
  final String? kanikkePopupText;
  @JsonKey(name: 'kanikkeWarningPopupText')
  final String? kanikkeWarningPopupText;
  @JsonKey(name: 'callFirstBeforeOffer')
  final String? callFirstBeforeOffer;
  @JsonKey(name: 'kanikkeConfirmationAcceptableStage')
  final String? kanikkeConfirmationAcceptableStage;
  @JsonKey(name: 'kanikkeConfirmationWarningStage')
  final String? kanikkeConfirmationWarningStage;

  factory KanIkkePopupsModel.fromJson(Map<String, dynamic> json) =>
      _$KanIkkePopupsModelFromJson(json);

  Map<String, dynamic> toJson() => _$KanIkkePopupsModelToJson(this);
  static List<KanIkkePopupsModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => KanIkkePopupsModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
