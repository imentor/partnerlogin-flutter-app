import 'package:Haandvaerker.dk/model/projects/approved_projects_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'project_invitation_model.g.dart';

@JsonSerializable()
class ProjectInvitationModel {
  @JsonKey(name: 'INVITATION_ID')
  int? invitationId;
  @JsonKey(name: 'STATUS')
  String? status;
  @JsonKey(name: 'PROJECT')
  ApprovedProjectsModel? project;

  ProjectInvitationModel({
    this.invitationId,
    this.status,
    this.project,
  });

  factory ProjectInvitationModel.fromJson(Map<String, dynamic> json) =>
      _$ProjectInvitationModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectInvitationModelToJson(this);

  static List<ProjectInvitationModel> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => ProjectInvitationModel.fromJson(i as Map<String, dynamic>))
      .toList();
}
