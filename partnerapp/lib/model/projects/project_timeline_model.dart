import 'package:json_annotation/json_annotation.dart';

part 'project_timeline_model.g.dart';

@JsonSerializable()
class ProjectTimelineModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'PROJECT_ID')
  int? projectId;
  @JsonKey(name: 'VERSION')
  int? version;
  @JsonKey(name: 'TIMELINE_STATUS')
  String? timelineStatus;
  @JsonKey(name: 'PARTNER_ID')
  int? partnerId;
  @JsonKey(name: 'CONTACT_ID')
  int? contactId;
  @JsonKey(name: 'CREATED_BY')
  String? createdBy;
  @JsonKey(name: 'UPDATED_BY')
  String? updatedBy;
  @JsonKey(name: 'SUBMITTED')
  int? submitted;
  @JsonKey(name: 'SUBMITTED_BY')
  String? submittedBy;
  @JsonKey(name: 'TASKS')
  List<ProjectTask>? tasks;

  ProjectTimelineModel(
      {this.id,
      this.projectId,
      this.version,
      this.timelineStatus,
      this.partnerId,
      this.contactId,
      this.createdBy,
      this.updatedBy,
      this.submitted,
      this.submittedBy,
      this.tasks});

  factory ProjectTimelineModel.fromJson(Map<String, dynamic> json) =>
      _$ProjectTimelineModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectTimelineModelToJson(this);
}

@JsonSerializable()
class ProjectTask {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'PROJECT_ID')
  int? projectId;
  @JsonKey(name: 'TIMELINE_ID')
  int? timelineId;
  @JsonKey(name: 'PARENT_TASK_ID')
  int? parentTaskId;
  @JsonKey(name: 'TASK_NAME')
  String? taskName;
  @JsonKey(name: 'TASK_DESCRIPTION')
  String? taskDescription;
  @JsonKey(name: 'PARTIAL_PAYMENT_PRICE')
  double? partialPaymentPrice;
  @JsonKey(name: 'START_DATE')
  String? startDate;
  @JsonKey(name: 'END_DATE')
  String? endDate;
  @JsonKey(name: 'PROGRESS')
  int? progress;

  ProjectTask(
      {this.id,
      this.projectId,
      this.timelineId,
      this.parentTaskId,
      this.taskName,
      this.taskDescription,
      this.partialPaymentPrice,
      this.startDate,
      this.endDate,
      this.progress});

  factory ProjectTask.fromJson(Map<String, dynamic> json) =>
      _$ProjectTaskFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectTaskToJson(this);
}
