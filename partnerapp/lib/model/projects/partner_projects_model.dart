import 'package:json_annotation/json_annotation.dart';

part 'partner_projects_model.g.dart';

@JsonSerializable()
class PartnerProjectsModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'CONTRACT_TYPE')
  String? contractType;
  @JsonKey(name: 'CONTRACT_ID')
  dynamic contractId;
  @JsonKey(name: 'STATUS')
  String? status;
  @JsonKey(name: 'IS_FINAL')
  bool? isFinal;
  @JsonKey(name: 'TOTAL_OFFERS')
  int? totalOffers;
  @JsonKey(name: 'FOLDER_ID')
  int? folderId;
  @JsonKey(name: 'TENDER_FOLDER_ID')
  int? tenderFolderId;
  @JsonKey(name: 'FOLDER_LINK')
  String? folderLink;
  @JsonKey(name: 'IS_ACTIVE')
  int? isActive;
  @JsonKey(name: 'PARENT_ID')
  int? parentId;
  @JsonKey(name: 'DEFICIENCY_FOLDER_ID')
  int? deficiencyFolderId;
  @JsonKey(name: 'DATE_CREATED')
  String? dateCreated;
  @JsonKey(name: 'DATE_UPDATED')
  String? dateUpdated;
  @JsonKey(name: 'INDUSTRY_INFO')
  IndustryInfo? industryInfo;
  @JsonKey(name: 'INDUSTRY_INFO_NEW')
  IndustryInfoNew? industryInfoNew;
  @JsonKey(name: 'HOMEOWNER')
  HomeOwner? homeOwner;
  @JsonKey(name: 'PARTNER')
  Partner? partner;
  @JsonKey(name: 'ADDRESS')
  Address? address;
  @JsonKey(name: 'TAGS')
  List<ProjectTag>? tags;
  @JsonKey(name: 'TILES')
  List<Todo>? tiles;
  @JsonKey(name: 'TODOS')
  List<Todo>? todos;
  @JsonKey(name: 'OFFERS')
  List<Offer>? offers;
  @JsonKey(name: 'PROJECT_PARENT_ID')
  String? parentProjectId;
  @JsonKey(name: 'JOBS_LIST')
  dynamic jobList;
  @JsonKey(name: 'PAYMENT_STAGES')
  List<ProjectPaymentStages>? paymentStages;
  @JsonKey(name: 'CONTRACT')
  Map<String, dynamic>? contract;
  @JsonKey(name: 'BUDGET')
  List<Budget>? budget;
  @JsonKey(name: 'ENTERPRISE_INDUSTRY_IDS')
  List<String>? enterpriseIndustryIds;
  @JsonKey(name: 'PHOTO_DOCUMENTATION_FOLDER_ID')
  int? photoDocumentationFolderId;
  @JsonKey(name: 'PARENT_PHOTO_DOCUMENTATION_FOLDER_ID')
  int? parentPhotoDocumentationFolderId;
  @JsonKey(name: 'JOB_INDUSTRY')
  int? jobIndustry;
  @JsonKey(name: 'CONTRACT_VERSIONS')
  List<ContractVersionModel>? contractVersions;
  @JsonKey(name: 'IS_VERSION')
  bool? isVersion;
  @JsonKey(name: 'ENTERPRISE_INDUSTRY')
  int? enterpriseIndustry;

  PartnerProjectsModel({
    this.id,
    this.name,
    this.description,
    this.contractType,
    this.status,
    this.isFinal,
    this.totalOffers,
    this.folderId,
    this.tenderFolderId,
    this.folderLink,
    this.isActive,
    this.parentId,
    this.deficiencyFolderId,
    this.dateCreated,
    this.dateUpdated,
    this.industryInfo,
    this.homeOwner,
    this.partner,
    this.address,
    this.tags,
    this.todos,
    this.industryInfoNew,
    this.offers,
    this.parentProjectId,
    this.contractId,
    this.contract,
    this.budget,
    this.enterpriseIndustryIds,
    this.jobList,
    this.paymentStages,
    this.photoDocumentationFolderId,
    this.parentPhotoDocumentationFolderId,
    this.jobIndustry,
    this.contractVersions,
    this.isVersion,
    this.enterpriseIndustry,
  });

  factory PartnerProjectsModel.fromJson(Map<String, dynamic> json) =>
      _$PartnerProjectsModelFromJson(json);

  Map<String, dynamic> toJson() => _$PartnerProjectsModelToJson(this);

  static List<PartnerProjectsModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => PartnerProjectsModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class ProjectPaymentStages {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'UF_CONTRACT_ID')
  int? contractId;
  @JsonKey(name: 'UF_PROJECT')
  String? project;
  @JsonKey(name: 'UF_SUB_PROJECT')
  String? subProject;
  @JsonKey(name: 'UF_PAYMENT_RELEASED')
  String? paymentReleased;
  @JsonKey(name: 'UF_PRICE')
  String? price;
  @JsonKey(name: 'UF_PRICE_VAT')
  String? priceVat;
  @JsonKey(name: 'UF_PERCENT')
  int? percent;
  @JsonKey(name: 'UF_TYPE')
  String? type;
  @JsonKey(name: 'UF_IS_DELETED')
  int? isDeleted;
  @JsonKey(name: 'UF_CONTRACT_TYPE')
  String? contractType;
  @JsonKey(name: 'UF_TRANSACTION_FEE')
  String? transactionFee;
  @JsonKey(name: 'UF_SUBCONTRACTOR_ID')
  int? subcontractorId;

  ProjectPaymentStages({
    this.contractType,
    this.id,
    this.isDeleted,
    this.paymentReleased,
    this.percent,
    this.price,
    this.priceVat,
    this.project,
    this.subProject,
    this.transactionFee,
    this.type,
    this.contractId,
    this.subcontractorId,
  });

  factory ProjectPaymentStages.fromJson(Map<String, dynamic> json) =>
      _$ProjectPaymentStagesFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectPaymentStagesToJson(this);
}

@JsonSerializable()
class Offer {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'UF_PROJECT')
  String? project;
  @JsonKey(name: 'UF_COMPANY')
  String? company;
  @JsonKey(name: 'UF_HOMEOWNER')
  String? homeOwner;
  @JsonKey(name: 'UF_CONTRACT_ID')
  String? contractId;
  @JsonKey(name: 'UF_STATUS')
  String? status;

  Offer({
    this.company,
    this.homeOwner,
    this.id,
    this.project,
    this.contractId,
    this.status,
  });

  Offer.defaults()
      : company = '',
        homeOwner = 'false',
        id = 0,
        project = '',
        contractId = '',
        status = '';

  factory Offer.fromJson(Map<String, dynamic> json) => _$OfferFromJson(json);

  Map<String, dynamic> toJson() => _$OfferToJson(this);
}

@JsonSerializable()
class IndustryInfoNew {
  @JsonKey(name: 'INDUSTRY')
  List<Industry>? industry;

  IndustryInfoNew({
    this.industry,
  });

  factory IndustryInfoNew.fromJson(Map<String, dynamic> json) =>
      _$IndustryInfoNewFromJson(json);

  Map<String, dynamic> toJson() => _$IndustryInfoNewToJson(this);
}

@JsonSerializable()
class Industry {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'NAME_EN')
  String? nameEn;

  Industry({this.id, this.name, this.nameEn});

  factory Industry.fromJson(Map<String, dynamic> json) =>
      _$IndustryFromJson(json);

  Map<String, dynamic> toJson() => _$IndustryToJson(this);
}

@JsonSerializable()
class IndustryInfo {
  @JsonKey(name: 'INDUSTRY_ID')
  int? industryId;
  @JsonKey(name: 'INDUSTRY_NAME')
  String? industryName;
  @JsonKey(name: 'SUBCATEGORY_ID')
  int? subcategoryId;
  @JsonKey(name: 'SUBCATEGORY_NAME')
  String? subbcategoryName;
  @JsonKey(name: 'TASK_TYPE_ID')
  int? taskTypeId;
  @JsonKey(name: 'TASK_TYPE_NAME')
  String? taskTypeName;

  IndustryInfo(
      {this.industryId,
      this.industryName,
      this.subcategoryId,
      this.subbcategoryName,
      this.taskTypeId,
      this.taskTypeName});

  factory IndustryInfo.fromJson(Map<String, dynamic> json) =>
      _$IndustryInfoFromJson(json);

  Map<String, dynamic> toJson() => _$IndustryInfoToJson(this);
}

@JsonSerializable()
class Partner {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;

  Partner({
    this.id,
    this.name,
  });

  factory Partner.fromJson(Map<String, dynamic> json) =>
      _$PartnerFromJson(json);

  Map<String, dynamic> toJson() => _$PartnerToJson(this);
}

@JsonSerializable()
class HomeOwner {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'EMAIL')
  String? email;
  @JsonKey(name: 'MOBILE')
  String? mobile;

  HomeOwner({this.id, this.name, this.email, this.mobile});

  factory HomeOwner.fromJson(Map<String, dynamic> json) =>
      _$HomeOwnerFromJson(json);

  Map<String, dynamic> toJson() => _$HomeOwnerToJson(this);
}

@JsonSerializable()
class Address {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'ADDRESS_ID')
  String? addressId;
  @JsonKey(name: 'ADDRESS_TITLE')
  String? address;
  @JsonKey(name: 'ADGANGSADDRESS_ID')
  String? adgangsAddressId;
  @JsonKey(name: 'ROAD')
  String? road;
  @JsonKey(name: 'ZIP')
  String? zip;
  @JsonKey(name: 'POST_NUMBER')
  String? postNumber;
  @JsonKey(name: 'IMAGE')
  String? image;
  @JsonKey(name: 'LATITUDE')
  String? latitude;
  @JsonKey(name: 'LONGITUDE')
  String? longitude;
  @JsonKey(name: 'ADDRESS')
  String? addressTitle;

  Address({
    this.id,
    this.address,
    this.zip,
    this.road,
    this.postNumber,
    this.image,
    this.latitude,
    this.longitude,
    this.adgangsAddressId,
    this.addressId,
    this.addressTitle,
  });

  factory Address.fromJson(Map<String, dynamic> json) =>
      _$AddressFromJson(json);

  Map<String, dynamic> toJson() => _$AddressToJson(this);
}

@JsonSerializable()
class ProjectTag {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'EN_GB')
  String? en;
  @JsonKey(name: 'DA_DK')
  String? da;

  ProjectTag({this.id, this.en, this.da});

  factory ProjectTag.fromJson(Map<String, dynamic> json) =>
      _$ProjectTagFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectTagToJson(this);
}

@JsonSerializable()
class Budget {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'PROJECT_ID')
  int? projectId;
  @JsonKey(name: 'CONTACT_ID')
  int? contactId;
  @JsonKey(name: 'INDUSTRY_ID')
  int? industryId;
  @JsonKey(name: 'TITLE')
  String? title;
  @JsonKey(name: 'CATEGORY')
  String? category;
  @JsonKey(name: 'AMOUNT')
  double? amount;

  Budget({
    this.amount,
    this.category,
    this.contactId,
    this.id,
    this.industryId,
    this.projectId,
    this.title,
  });

  factory Budget.fromJson(Map<String, dynamic> json) => _$BudgetFromJson(json);

  static List<Budget> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => Budget.fromJson(i as Map<String, dynamic>))
      .toList();

  Map<String, dynamic> toJson() => _$BudgetToJson(this);
}

@JsonSerializable()
class Todo {
  @JsonKey(name: 'CODE')
  String? code;
  @JsonKey(name: 'IS_DONE')
  bool? isDone;
  @JsonKey(name: 'IS_SKIPPED')
  bool? isSkip;
  @JsonKey(name: 'TITLE_EN')
  String? titleEn;
  @JsonKey(name: 'TITLE_DA')
  String? titleDa;
  @JsonKey(name: 'DESCRIPTION_EN')
  String? descEn;
  @JsonKey(name: 'DESCRIPTION_DA')
  String? descDa;
  @JsonKey(name: 'IS_ACTIVE')
  bool? isActive;

  Todo({
    this.code,
    this.isDone,
    this.isSkip,
    this.titleEn,
    this.titleDa,
    this.descEn,
    this.descDa,
    this.isActive,
  });

  Todo.defaults()
      : code = '',
        isDone = false,
        isSkip = false,
        titleEn = '',
        titleDa = '',
        descEn = '',
        descDa = '',
        isActive = false;

  factory Todo.fromJson(Map<String, dynamic> json) => _$TodoFromJson(json);
  Map<String, dynamic> toJson() => _$TodoToJson(this);

  static List<Todo> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => Todo.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class ContractVersionModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'COMPANY')
  int? companyId;
  @JsonKey(name: 'CONTACT')
  int? contactId;
  @JsonKey(name: 'STAGE')
  String? stage;
  @JsonKey(name: 'TEMPLATE')
  int? template;
  @JsonKey(name: 'PROJECT_ID')
  String? projectId;
  @JsonKey(name: 'CONTACT_IP')
  String? contactIp;
  @JsonKey(name: 'PARTNER_IP')
  String? partnerIp;
  @JsonKey(name: 'DATE_SIGNED_BY_CONTACT')
  String? dateSignedByContact;
  @JsonKey(name: 'DATE_SIGNED_BY_PARTNER')
  String? dateSignedByPartner;
  @JsonKey(name: 'CONTRACT_STATUS')
  String? contractStatus;
  @JsonKey(name: 'CREATED_ON')
  String? createdOn;

  ContractVersionModel({
    this.id,
    this.name,
    this.companyId,
    this.contactId,
    this.stage,
    this.template,
    this.projectId,
    this.contactIp,
    this.partnerIp,
    this.dateSignedByContact,
    this.dateSignedByPartner,
    this.contractStatus,
    this.createdOn,
  });

  factory ContractVersionModel.fromJson(Map<String, dynamic> json) =>
      _$ContractVersionModelFromJson(json);

  Map<String, dynamic> toJson() => _$ContractVersionModelToJson(this);

  static List<ContractVersionModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => ContractVersionModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
