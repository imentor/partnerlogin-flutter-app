import 'package:Haandvaerker.dk/model/projects/contacts_with_contracts_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'project_message_model.g.dart';

@JsonSerializable()
class ProjectMessageModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'THREAD_ID')
  int? threadId;
  @JsonKey(name: 'CONTACT_ID')
  int? contactId;
  @JsonKey(name: 'PARTNER_ID')
  int? partnerId;
  @JsonKey(name: 'PROJECT_ID')
  int? projectId;
  @JsonKey(name: 'SEND_DATE')
  String? sendDate;
  @JsonKey(name: 'TITLE')
  String? title;
  @JsonKey(name: 'MESSAGE')
  String? message;
  @JsonKey(name: 'SEEN')
  int? seen;
  @JsonKey(name: 'SEEN_DATE')
  String? seenDate;
  @JsonKey(name: 'FILES')
  List<dynamic>? files;
  @JsonKey(name: 'IS_FROM_PARTNER')
  bool? isFromPartner;
  @JsonKey(name: 'ACTIONS')
  ProjectMessageActionModel? action;
  @JsonKey(name: 'CONTACT')
  ContactWithContractModel? contact;
  @JsonKey(name: 'PARTNER')
  ContactWithContractModel? partner;
  @JsonKey(name: 'PROJECT')
  MessageProject? project;

  ProjectMessageModel(
      {this.action,
      this.project,
      this.contact,
      this.contactId,
      this.files,
      this.id,
      this.isFromPartner,
      this.message,
      this.partner,
      this.partnerId,
      this.projectId,
      this.seen,
      this.seenDate,
      this.sendDate,
      this.threadId,
      this.title});

  factory ProjectMessageModel.fromJson(Map<String, dynamic> json) =>
      _$ProjectMessageModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectMessageModelToJson(this);

  static List<ProjectMessageModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => ProjectMessageModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class MessageProject {
  MessageProject({this.id, this.name, this.projectDescription});

  @JsonKey(name: 'ID')
  final dynamic id;
  @JsonKey(name: 'NAME')
  final String? name;
  @JsonKey(name: 'PROJECT_DESCRIPTION')
  final String? projectDescription;

  factory MessageProject.fromJson(Map<String, dynamic> json) =>
      _$MessageProjectFromJson(json);

  Map<String, dynamic> toJson() => _$MessageProjectToJson(this);

  static List<MessageProject> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => MessageProject.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class ProjectMessageActionModel {
  @JsonKey(name: 'IS_DONE')
  bool? isDone;
  @JsonKey(name: 'ITEMS')
  List<ActionItemModel>? items;

  ProjectMessageActionModel({this.isDone, this.items});

  factory ProjectMessageActionModel.fromJson(Map<String, dynamic> json) =>
      _$ProjectMessageActionModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectMessageActionModelToJson(this);

  static List<ProjectMessageActionModel> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => ProjectMessageActionModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class ActionItemModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'MESSAGE_ID')
  int? messageId;
  @JsonKey(name: 'ACTION')
  String? action;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'DATA')
  String? data;

  ActionItemModel({
    this.action,
    this.data,
    this.description,
    this.id,
    this.messageId,
  });

  factory ActionItemModel.fromJson(Map<String, dynamic> json) =>
      _$ActionItemModelFromJson(json);

  Map<String, dynamic> toJson() => _$ActionItemModelToJson(this);

  static List<ActionItemModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => ActionItemModel.fromJson(i as Map<String, dynamic>))
      .toList();
}
