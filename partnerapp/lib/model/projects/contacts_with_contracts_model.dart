import 'package:json_annotation/json_annotation.dart';

part 'contacts_with_contracts_model.g.dart';

@JsonSerializable()
class ContactWithContractModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'EMAIL')
  String? email;
  @JsonKey(name: 'COMPANY')
  String? company;
  @JsonKey(name: 'AVATAR')
  String? avatar;
  @JsonKey(name: 'PROJECT_ID')
  String? projectId;
  @JsonKey(name: 'CONTRACT_ID')
  int? contractId;
  @JsonKey(name: 'CONTRACT_STATUS')
  String? contractStatus;

  ContactWithContractModel(
      {this.id,
      this.name,
      this.email,
      this.company,
      this.avatar,
      this.contractId,
      this.contractStatus,
      this.projectId});

  factory ContactWithContractModel.fromJson(Map<String, dynamic> json) =>
      _$ContactWithContractModelFromJson(json);

  Map<String, dynamic> toJson() => _$ContactWithContractModelToJson(this);

  static List<ContactWithContractModel> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => ContactWithContractModel.fromJson(i as Map<String, dynamic>))
      .toList();
}
