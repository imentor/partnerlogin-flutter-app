import 'package:json_annotation/json_annotation.dart';

part 'partner_contact_model.g.dart';

@JsonSerializable()
class PartnerContactModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'EMAIL')
  String? email;
  @JsonKey(name: 'ZIP')
  int? zip;
  @JsonKey(name: 'MOBILE')
  int? mobile;
  @JsonKey(name: 'PROFILE_PICTURE')
  String? profilePicture;
  @JsonKey(name: 'IS_ACTIVE')
  int? isActive;

  PartnerContactModel(
      {this.id,
      this.name,
      this.email,
      this.zip,
      this.isActive,
      this.mobile,
      this.profilePicture});

  factory PartnerContactModel.fromJson(Map<String, dynamic> json) =>
      _$PartnerContactModelFromJson(json);

  Map<String, dynamic> toJson() => _$PartnerContactModelToJson(this);

  static List<PartnerContactModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => PartnerContactModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
