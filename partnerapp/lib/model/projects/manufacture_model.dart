import 'package:json_annotation/json_annotation.dart';

part 'manufacture_model.g.dart';

@JsonSerializable()
class ManufactureModel {
  ManufactureModel({
    this.bgImg,
    this.industryId,
    this.manufactureId,
    this.name,
    this.taskTypes,
  });

  @JsonKey(name: 'BG_IMG')
  final String? bgImg;
  @JsonKey(name: 'INDUSTRY_ID')
  final String? industryId;
  @JsonKey(name: 'MANUFACTURE_ID')
  final int? manufactureId;
  @JsonKey(name: 'NAME')
  final String? name;
  @JsonKey(name: 'TASK_TYPES')
  final String? taskTypes;

  factory ManufactureModel.fromJson(Map<String, dynamic> json) =>
      _$ManufactureModelFromJson(json);

  Map<String, dynamic> toJson() => _$ManufactureModelToJson(this);

  static List<ManufactureModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => ManufactureModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
