import 'package:json_annotation/json_annotation.dart';

part 'extra_work_response_model.g.dart';

@JsonSerializable()
class ExtraWorkResponseModel {
  @JsonKey(name: 'current_page')
  int? currentPage;
  @JsonKey(name: 'from')
  int? from;
  @JsonKey(name: 'last_page')
  int? lastPage;
  @JsonKey(name: 'perPage')
  int? perPage;
  @JsonKey(name: 'total')
  int? total;
  @JsonKey(name: 'items')
  Map<String, List<ExtraWorkItems>>? items;

  @JsonKey(name: 'total_extra_work')
  double? totalExtraWork;
  @JsonKey(name: 'total_approved_extra_work')
  double? totalApprovedExtraWork;
  @JsonKey(name: 'total_pending_extra_work')
  double? totalPendingExtraWork;
  @JsonKey(name: 'total_paid_extra_work')
  double? totalPaidExtraWork;

  @JsonKey(name: 'totalExtraWork')
  Map<String, dynamic>? totalExtraWorkMap;
  @JsonKey(name: 'totalExtraWorkPending')
  Map<String, dynamic>? totalExtraWorkPending;
  @JsonKey(name: 'totalExtraWorkApproved')
  Map<String, dynamic>? totalExtraWorkApproved;
  @JsonKey(name: 'totalExtraWorkPaid')
  Map<String, dynamic>? totalExtraWorkPaid;

  @JsonKey(name: 'total_pending_minus_extra_work')
  double? totalPendingMinusExtraWork;
  @JsonKey(name: 'total_approved_minus_extra_work')
  double? totalApprovedMinusExtraWork;

  ExtraWorkResponseModel({
    this.currentPage,
    this.from,
    this.lastPage,
    this.perPage,
    this.total,
    this.items,
    this.totalApprovedExtraWork,
    this.totalExtraWork,
    this.totalPaidExtraWork,
    this.totalPendingExtraWork,
    this.totalExtraWorkApproved,
    this.totalExtraWorkMap,
    this.totalExtraWorkPaid,
    this.totalExtraWorkPending,
    this.totalPendingMinusExtraWork,
    this.totalApprovedMinusExtraWork,
  });

  factory ExtraWorkResponseModel.fromJson(Map<String, dynamic> json) =>
      _$ExtraWorkResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$ExtraWorkResponseModelToJson(this);

  static List<ExtraWorkResponseModel> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => ExtraWorkResponseModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class ExtraWorkItems {
  @JsonKey(name: 'ADDRESS')
  String? address;
  @JsonKey(name: 'AMOUNT')
  String? amount;
  @JsonKey(name: 'COMPANY')
  int? company;
  @JsonKey(name: 'CONTACT')
  int? contact;
  @JsonKey(name: 'CONTRACT_ID')
  String? contractId;
  @JsonKey(name: 'CREATED_BY')
  String? createdBy;
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'DOCUMENT_TEMPLATE_ID')
  int? documentTemplateId;
  @JsonKey(name: 'ENTITY_TYPE_ID')
  int? entityTypeId;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'DATE_SIGNED_BY_CONTACT')
  String? dateSignedByContact;
  @JsonKey(name: 'DATE_SIGNED_BY_PARTNER')
  String? dateSignedByPartner;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'LAST_UPDATED_BY')
  String? lastUpdatedBy;
  @JsonKey(name: 'STAGE')
  String? stage;
  @JsonKey(name: 'CREATED_FROM')
  String? createdFrom;
  @JsonKey(name: 'END_DATE')
  String? endDate;
  @JsonKey(name: 'PROJECT_ID')
  String? projectId;
  @JsonKey(name: 'TOTAL')
  String? total;
  @JsonKey(name: 'TOTAL_VAT')
  String? totalVat;
  @JsonKey(name: 'TYPE')
  String? type;
  @JsonKey(name: 'CREATED_ON')
  String? createdOn;

  ExtraWorkItems({
    this.address,
    this.amount,
    this.company,
    this.contact,
    this.contractId,
    this.createdBy,
    this.id,
    this.documentTemplateId,
    this.entityTypeId,
    this.name,
    this.dateSignedByContact,
    this.dateSignedByPartner,
    this.description,
    this.lastUpdatedBy,
    this.stage,
    this.createdFrom,
    this.endDate,
    this.projectId,
    this.total,
    this.totalVat,
    this.type,
    this.createdOn,
  });

  ExtraWorkItems.defaults()
      : address = '',
        amount = '',
        company = 0,
        contact = 0,
        contractId = '',
        createdBy = '',
        id = 0,
        documentTemplateId = 0,
        entityTypeId = 0,
        name = '',
        dateSignedByContact = '',
        dateSignedByPartner = '',
        description = '',
        lastUpdatedBy = '',
        stage = '',
        createdFrom = '',
        endDate = '',
        projectId = '',
        total = '',
        totalVat = '';

  factory ExtraWorkItems.fromJson(Map<String, dynamic> json) =>
      _$ExtraWorkItemsFromJson(json);

  Map<String, dynamic> toJson() => _$ExtraWorkItemsToJson(this);

  static List<ExtraWorkItems> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => ExtraWorkItems.fromJson(i as Map<String, dynamic>))
      .toList();
}
