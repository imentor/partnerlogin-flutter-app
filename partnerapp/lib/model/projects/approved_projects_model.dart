import 'package:json_annotation/json_annotation.dart';

part 'approved_projects_model.g.dart';

@JsonSerializable()
class ApprovedProjectsModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'CONTRACT_TYPE')
  String? contractType;
  @JsonKey(name: 'STATUS')
  String? status;
  @JsonKey(name: 'IS_FINAL')
  bool? isFinal;
  @JsonKey(name: 'TOTAL_OFFERS')
  int? totalOffers;
  @JsonKey(name: 'FOLDER_ID')
  int? folderId;
  @JsonKey(name: 'FOLDER_LINK')
  String? folderLink;
  @JsonKey(name: 'IS_ACTIVE')
  int? isActive;
  @JsonKey(name: 'PARENT_ID')
  int? parentId;
  @JsonKey(name: 'DEFICIENCY_FOLDER_ID')
  int? deficiencyFolderId;
  @JsonKey(name: 'DATE_CREATED')
  String? dateCreated;
  @JsonKey(name: 'DATE_UPDATED')
  String? dateUpdated;
  @JsonKey(name: 'INDUSTRY_INFO')
  IndustryInfo? industryInfo;
  @JsonKey(name: 'HOMEOWNER')
  HomeOwner? homeOwner;
  @JsonKey(name: 'PARTNER')
  dynamic partner;
  @JsonKey(name: 'ADDRESS')
  Address? address;
  @JsonKey(name: 'TAGS')
  List<ApprovedProjectTag>? tags;
  @JsonKey(name: 'TODOS')
  List<dynamic>? todos;

  ApprovedProjectsModel(
      {this.id,
      this.name,
      this.description,
      this.contractType,
      this.status,
      this.isFinal,
      this.totalOffers,
      this.folderId,
      this.folderLink,
      this.isActive,
      this.parentId,
      this.deficiencyFolderId,
      this.dateCreated,
      this.dateUpdated,
      this.industryInfo,
      this.homeOwner,
      this.partner,
      this.address,
      this.tags,
      this.todos});

  factory ApprovedProjectsModel.fromJson(Map<String, dynamic> json) =>
      _$ApprovedProjectsModelFromJson(json);

  Map<String, dynamic> toJson() => _$ApprovedProjectsModelToJson(this);

  static List<ApprovedProjectsModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => ApprovedProjectsModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class IndustryInfo {
  @JsonKey(name: 'INDUSTRY_ID')
  int? industryId;
  @JsonKey(name: 'INDUSTRY_NAME')
  String? industryName;
  @JsonKey(name: 'SUBCATEGORY_ID')
  int? subcategoryId;
  @JsonKey(name: 'SUBCATEGORY_NAME')
  String? subbcategoryName;
  @JsonKey(name: 'TASK_TYPE_ID')
  int? taskTypeId;
  @JsonKey(name: 'TASK_TYPE_NAME')
  String? taskTypeName;

  IndustryInfo(
      {this.industryId,
      this.industryName,
      this.subcategoryId,
      this.subbcategoryName,
      this.taskTypeId,
      this.taskTypeName});

  factory IndustryInfo.fromJson(Map<String, dynamic> json) =>
      _$IndustryInfoFromJson(json);

  Map<String, dynamic> toJson() => _$IndustryInfoToJson(this);
}

@JsonSerializable()
class HomeOwner {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'EMAIL')
  String? email;
  @JsonKey(name: 'MOBILE')
  String? mobile;

  HomeOwner({this.id, this.name, this.email, this.mobile});

  factory HomeOwner.fromJson(Map<String, dynamic> json) =>
      _$HomeOwnerFromJson(json);

  Map<String, dynamic> toJson() => _$HomeOwnerToJson(this);
}

@JsonSerializable()
class Address {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'ADDRESS')
  String? address;
  @JsonKey(name: 'ROAD')
  String? road;
  @JsonKey(name: 'ZIP')
  String? zip;
  @JsonKey(name: 'POST_NUMBER')
  String? postNumber;
  @JsonKey(name: 'IMAGE')
  String? image;
  @JsonKey(name: 'LATITUDE')
  String? latitude;
  @JsonKey(name: 'LONGITUDE')
  String? longitude;

  Address(
      {this.id,
      this.address,
      this.zip,
      this.road,
      this.postNumber,
      this.image,
      this.latitude,
      this.longitude});

  factory Address.fromJson(Map<String, dynamic> json) =>
      _$AddressFromJson(json);

  Map<String, dynamic> toJson() => _$AddressToJson(this);
}

@JsonSerializable()
class ApprovedProjectTag {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'EN_GB')
  String? en;
  @JsonKey(name: 'DA_DK')
  String? da;

  ApprovedProjectTag({this.id, this.en, this.da});

  factory ApprovedProjectTag.fromJson(Map<String, dynamic> json) =>
      _$ApprovedProjectTagFromJson(json);

  Map<String, dynamic> toJson() => _$ApprovedProjectTagToJson(this);
}
