import 'package:json_annotation/json_annotation.dart';

part 'message_v2_response_model.g.dart';

@JsonSerializable()
class MessageV2ResponseModel {
  @JsonKey(name: 'current_page')
  int? currentPage;
  @JsonKey(name: 'last_page')
  int? lastPage;
  @JsonKey(name: 'perPage')
  int? perPage;
  @JsonKey(name: 'to')
  int? to;
  @JsonKey(name: 'items')
  List<MessageV2Items>? items;
  @JsonKey(name: 'unread')
  int? unread;

  MessageV2ResponseModel({
    this.currentPage,
    this.lastPage,
    this.perPage,
    this.to,
    this.items,
    this.unread,
  });

  factory MessageV2ResponseModel.fromJson(Map<String, dynamic> json) =>
      _$MessageV2ResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$MessageV2ResponseModelToJson(this);

  static List<MessageV2ResponseModel> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => MessageV2ResponseModel.fromJson(i as Map<String, dynamic>))
      .toList();

  MessageV2ResponseModel.defaults()
      : currentPage = 0,
        lastPage = 0,
        perPage = 0,
        to = 0,
        unread = 0,
        items = [];
}

@JsonSerializable()
class MessageV2Items {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'THREAD_ID')
  int? threadId;
  @JsonKey(name: 'SEEN')
  int? seen;
  @JsonKey(name: 'THREAD_COUNT')
  int? threadCount;
  @JsonKey(name: 'TITLE')
  String? title;
  @JsonKey(name: 'CONTACT')
  MessageV2Contact? contact;
  @JsonKey(name: 'CONTACT_ID')
  int? contactId;
  @JsonKey(name: 'PARTNER')
  MessageV2Partner? partner;
  @JsonKey(name: 'PROJECT')
  MessageV2Project? project;
  @JsonKey(name: 'IS_FAVORITE')
  bool? isFavorite;
  @JsonKey(name: 'IS_FROM_HAAN')
  bool? isFromHaan;
  @JsonKey(name: 'IS_FROM_LEGAL')
  bool? isFromLegal;
  @JsonKey(name: 'IS_FROM_PARTNER')
  bool? isFromPartner;
  @JsonKey(name: 'IS_FROM_USER')
  bool? isFromUser;
  @JsonKey(name: 'SEND_DATE')
  String? sendDate;
  @JsonKey(name: 'SEEN_DATE')
  String? seenDate;
  @JsonKey(name: 'MESSAGE')
  String? message;
  @JsonKey(name: 'ACTIONS')
  MessageV2Actions? actions;
  @JsonKey(name: 'FILES')
  List<dynamic>? files;
  @JsonKey(name: 'PROJECT_ID')
  int? projectId;

  MessageV2Items({
    this.id,
    this.threadId,
    this.threadCount,
    this.seen,
    this.title,
    this.contact,
    this.contactId,
    this.partner,
    this.project,
    this.isFavorite,
    this.isFromHaan,
    this.isFromLegal,
    this.isFromPartner,
    this.isFromUser,
    this.sendDate,
    this.seenDate,
    this.message,
    this.actions,
    this.files,
    this.projectId,
  });

  factory MessageV2Items.fromJson(Map<String, dynamic> json) =>
      _$MessageV2ItemsFromJson(json);

  Map<String, dynamic> toJson() => _$MessageV2ItemsToJson(this);
  static List<MessageV2Items> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => MessageV2Items.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class MessageV2Contact {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'COMPANY')
  String? company;
  @JsonKey(name: 'EMAIL')
  String? email;
  @JsonKey(name: 'MOBILE')
  String? mobile;
  @JsonKey(name: 'AVATAR')
  String? avatar;
  @JsonKey(name: 'NAME')
  String? name;

  MessageV2Contact({
    this.id,
    this.company,
    this.email,
    this.mobile,
    this.avatar,
    this.name,
  });

  factory MessageV2Contact.fromJson(Map<String, dynamic> json) =>
      _$MessageV2ContactFromJson(json);

  Map<String, dynamic> toJson() => _$MessageV2ContactToJson(this);
  static List<MessageV2Contact> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => MessageV2Contact.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class MessageV2Project {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'PROJECT_DESCRIPTION')
  String? projectDescription;

  MessageV2Project({
    this.id,
    this.name,
    this.projectDescription,
  });

  factory MessageV2Project.fromJson(Map<String, dynamic> json) =>
      _$MessageV2ProjectFromJson(json);

  Map<String, dynamic> toJson() => _$MessageV2ProjectToJson(this);
  static List<MessageV2Project> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => MessageV2Project.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class MessageV2Partner {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'COMPANY')
  String? company;
  @JsonKey(name: 'EMAIL')
  String? email;
  @JsonKey(name: 'MOBILE')
  String? mobile;
  @JsonKey(name: 'AVATAR')
  String? avatar;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'CVR')
  String? cvr;

  MessageV2Partner({
    this.id,
    this.company,
    this.email,
    this.mobile,
    this.avatar,
    this.name,
    this.cvr,
  });

  factory MessageV2Partner.fromJson(Map<String, dynamic> json) =>
      _$MessageV2PartnerFromJson(json);

  Map<String, dynamic> toJson() => _$MessageV2PartnerToJson(this);
  static List<MessageV2Partner> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => MessageV2Partner.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class MessageV2Actions {
  @JsonKey(name: 'IS_DONE')
  bool? isDone;
  @JsonKey(name: 'ITEMS')
  List<MessageV2ActionItem>? items;

  MessageV2Actions({
    this.isDone,
    this.items,
  });

  factory MessageV2Actions.fromJson(Map<String, dynamic> json) =>
      _$MessageV2ActionsFromJson(json);

  Map<String, dynamic> toJson() => _$MessageV2ActionsToJson(this);
  static List<MessageV2Actions> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => MessageV2Actions.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class MessageV2ActionItem {
  @JsonKey(name: 'ACTION')
  String? action;
  @JsonKey(name: 'DATA')
  String? data;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'MESSAGE_ID')
  int? messageId;
  @JsonKey(name: 'TYPE')
  String? type;
  @JsonKey(name: 'IS_DONE')
  bool? isDone;

  MessageV2ActionItem({
    this.action,
    this.data,
    this.description,
    this.id,
    this.messageId,
    this.type,
    this.isDone,
  });

  factory MessageV2ActionItem.fromJson(Map<String, dynamic> json) =>
      _$MessageV2ActionItemFromJson(json);

  Map<String, dynamic> toJson() => _$MessageV2ActionItemToJson(this);
  static List<MessageV2ActionItem> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => MessageV2ActionItem.fromJson(i as Map<String, dynamic>))
          .toList();
}
