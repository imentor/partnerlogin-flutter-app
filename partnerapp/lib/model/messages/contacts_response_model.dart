import 'package:json_annotation/json_annotation.dart';

part 'contacts_response_model.g.dart';

@JsonSerializable()
class ContactsResponseModel {
  @JsonKey(name: 'EMAIL')
  String? email;
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'IS_ACTIVE')
  int? isActive;
  @JsonKey(name: 'IS_VERIFIED')
  int? isVerified;
  @JsonKey(name: 'MOBILE')
  String? mobile;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'PACKAGE')
  String? package;
  @JsonKey(name: 'PROFILE_PICTURE')
  String? profilePicture;
  @JsonKey(name: 'TYPE')
  String? type;
  @JsonKey(name: 'ZIP')
  String? zip;

  ContactsResponseModel({
    this.email,
    this.id,
    this.isActive,
    this.isVerified,
    this.mobile,
    this.name,
    this.package,
    this.profilePicture,
    this.type,
    this.zip,
  });

  ContactsResponseModel.defaults()
      : email = '',
        id = 0,
        isActive = 0,
        isVerified = 0,
        mobile = '',
        name = '',
        package = '',
        profilePicture = '',
        type = '',
        zip = '';

  factory ContactsResponseModel.fromJson(Map<String, dynamic> json) =>
      _$ContactsResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$ContactsResponseModelToJson(this);
  static List<ContactsResponseModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => ContactsResponseModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
