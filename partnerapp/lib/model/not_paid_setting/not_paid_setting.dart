import 'package:json_annotation/json_annotation.dart';

part 'not_paid_setting.g.dart';

@JsonSerializable()
class NotPaidSetting {
  @JsonKey(name: 'showPopup')
  bool? showPopup;
  @JsonKey(name: 'allowClosePopup')
  bool? allowClosePopup;
  @JsonKey(name: 'createTicketOnClosePopup')
  bool? createTicketOnClosePopup;
  @JsonKey(name: 'createTicketOnPayNow')
  bool? createTicketOnPayNow;
  @JsonKey(name: 'showBanner')
  bool? showBanner;
  @JsonKey(name: 'showPackages')
  List<dynamic>? showPackages;
  @JsonKey(name: 'popupHeader')
  String? popupHeader;
  @JsonKey(name: 'popupBody')
  String? popupBody;
  @JsonKey(name: 'popupButton')
  String? popupButton;
  @JsonKey(name: 'popupUrl')
  String? popupUrl;
  @JsonKey(name: 'bannerBody')
  String? bannerBody;
  @JsonKey(name: 'bannerButton')
  String? bannerButton;
  @JsonKey(name: 'bannerUrl')
  String? bannerUrl;
  @JsonKey(name: 'notPaid')
  bool? notPaid;

  NotPaidSetting({
    this.allowClosePopup,
    this.bannerBody,
    this.bannerButton,
    this.bannerUrl,
    this.createTicketOnClosePopup,
    this.createTicketOnPayNow,
    this.popupBody,
    this.popupButton,
    this.popupHeader,
    this.popupUrl,
    this.showBanner,
    this.showPackages,
    this.showPopup,
    this.notPaid,
  });

  factory NotPaidSetting.fromJson(Map<String, dynamic> json) =>
      _$NotPaidSettingFromJson(json);

  Map<String, dynamic> toJson() => _$NotPaidSettingToJson(this);

  static List<NotPaidSetting> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => NotPaidSetting.fromJson(i as Map<String, dynamic>))
      .toList();
}
