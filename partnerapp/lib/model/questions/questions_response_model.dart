import 'package:json_annotation/json_annotation.dart';

part 'questions_response_model.g.dart';

@JsonSerializable()
class QuestionResponseModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'UF_PARTNER_ID')
  int? partnerId;
  @JsonKey(name: 'UF_CONTACT_ID')
  int? contactId;
  @JsonKey(name: 'UF_PROJECT_ID')
  int? projectId;
  @JsonKey(name: 'UF_PARENT_ID')
  int? parentId;
  @JsonKey(name: 'UF_INDUSTRY_ID')
  String? industryId;
  @JsonKey(name: 'UF_MESSAGE')
  String? message;
  @JsonKey(name: 'UF_MESSAGE_FROM')
  int? messageFrom;
  @JsonKey(name: 'UF_DATE_CREATED')
  String? dateCreated;
  @JsonKey(name: 'UF_DELETED')
  int? deleted;
  @JsonKey(name: 'UF_DESCRIPTION_ID')
  String? descriptionId;
  @JsonKey(name: 'UF_JOB_INDUSTRY_ID')
  String? jobIndustryId;
  @JsonKey(name: 'TOTAL_REPLY')
  int? totalReply;
  @JsonKey(name: 'INDUSTRY')
  List<Industry>? industry;

  QuestionResponseModel({
    this.id,
    this.partnerId,
    this.contactId,
    this.projectId,
    this.parentId,
    this.industryId,
    this.message,
    this.messageFrom,
    this.dateCreated,
    this.deleted,
    this.descriptionId,
    this.jobIndustryId,
    this.totalReply,
    this.industry,
  });

  QuestionResponseModel.defaults()
      : id = 0,
        partnerId = 0,
        contactId = 0,
        projectId = 0,
        parentId = 0,
        industryId = "",
        message = "",
        messageFrom = 0,
        dateCreated = "",
        deleted = 0,
        descriptionId = "",
        jobIndustryId = "",
        totalReply = 0,
        industry = [];

  factory QuestionResponseModel.fromJson(Map<String, dynamic> json) =>
      _$QuestionResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$QuestionResponseModelToJson(this);
  static List<QuestionResponseModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => QuestionResponseModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class Industry {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;

  Industry({this.id, this.name});

  factory Industry.fromJson(Map<String, dynamic> json) =>
      _$IndustryFromJson(json);

  Map<String, dynamic> toJson() => _$IndustryToJson(this);
  static List<Industry> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => Industry.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class ChildQuestionResponseModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'UF_PARTNER_ID')
  int? partnerId;
  @JsonKey(name: 'UF_CONTACT_ID')
  int? contactId;
  @JsonKey(name: 'UF_PROJECT_ID')
  int? projectId;
  @JsonKey(name: 'UF_PARENT_ID')
  int? parentId;
  @JsonKey(name: 'UF_INDUSTRY_ID')
  int? industryId;
  @JsonKey(name: 'UF_MESSAGE')
  String? message;
  @JsonKey(name: 'UF_MESSAGE_FROM')
  int? messageFromId;
  @JsonKey(name: 'UF_DATE_CREATED')
  String? dateCreated;
  @JsonKey(name: 'UF_DELETED')
  int? isDeleted;
  @JsonKey(name: 'UF_DESCRIPTION_ID')
  String? descriptionId;
  @JsonKey(name: 'UF_JOB_INDUSTRY_ID')
  String? jobIndustryId;

  ChildQuestionResponseModel({
    this.id,
    this.partnerId,
    this.contactId,
    this.projectId,
    this.parentId,
    this.industryId,
    this.message,
    this.messageFromId,
    this.dateCreated,
    this.isDeleted,
    this.descriptionId,
    this.jobIndustryId,
  });

  factory ChildQuestionResponseModel.fromJson(Map<String, dynamic> json) =>
      _$ChildQuestionResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$ChildQuestionResponseModelToJson(this);
  static List<ChildQuestionResponseModel> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map(
          (i) => ChildQuestionResponseModel.fromJson(i as Map<String, dynamic>))
      .toList();
}
