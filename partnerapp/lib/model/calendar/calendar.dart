import 'package:json_annotation/json_annotation.dart';

part 'calendar.g.dart';

@JsonSerializable()
class Calendar {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'CONTACT_ID')
  int? contactId;
  @JsonKey(name: 'PARTNER_ID')
  int? partnerId;
  @JsonKey(name: 'PROJECT_ID')
  int? projectId;
  @JsonKey(name: 'PROJECT_NAME')
  String? projectName;
  @JsonKey(name: 'TITLE')
  String? title;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'DATE')
  String? date;
  @JsonKey(name: 'END_DATE')
  String? endDate;
  @JsonKey(name: 'START_TIME')
  String? startTime;
  @JsonKey(name: 'END_TIME')
  String? endTime;
  @JsonKey(name: 'TYPE')
  String? type;
  @JsonKey(name: 'COLOR')
  String? color;
  @JsonKey(name: 'LATITUDE')
  String? latitude;
  @JsonKey(name: 'LONGITUDE')
  String? longitude;
  @JsonKey(name: 'TODO_TYPE')
  String? todoType;

  Calendar(
      {this.color,
      this.contactId,
      this.date,
      this.description,
      this.endDate,
      this.endTime,
      this.id,
      this.latitude,
      this.longitude,
      this.partnerId,
      this.projectId,
      this.projectName,
      this.startTime,
      this.title,
      this.todoType,
      this.type});

  Calendar.defaults()
      : color = '',
        contactId = 0,
        date = '',
        description = '',
        endDate = '',
        endTime = '',
        id = 0,
        latitude = '',
        longitude = '',
        partnerId = 0,
        projectId = 0,
        projectName = '',
        startTime = '',
        title = '',
        todoType = '',
        type = '';

  factory Calendar.fromJson(Map<String, dynamic> json) =>
      _$CalendarFromJson(json);

  Map<String, dynamic> toJson() => _$CalendarToJson(this);
  static List<Calendar> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => Calendar.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class CalendarTypes {
  @JsonKey(name: 'TYPE')
  String? type;
  @JsonKey(name: 'COLOR')
  String? color;
  @JsonKey(name: 'TYPE_DA')
  String? typeDa;

  CalendarTypes({this.color, this.type, this.typeDa});

  CalendarTypes.defaults()
      : color = '',
        type = '',
        typeDa = '';

  factory CalendarTypes.fromJson(Map<String, dynamic> json) =>
      _$CalendarTypesFromJson(json);

  Map<String, dynamic> toJson() => _$CalendarTypesToJson(this);
  static List<CalendarTypes> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => CalendarTypes.fromJson(i as Map<String, dynamic>))
      .toList();
}
