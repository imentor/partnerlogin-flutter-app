import 'package:json_annotation/json_annotation.dart';

part 'kanikke_settings_response_model.g.dart';

@JsonSerializable()
class KanIkkeSettingsResponseModel {
  KanIkkeSettingsResponseModel({
    this.kanIkkeSettings,
    this.userKanIkke,
    this.blacklist,
  });

  @JsonKey(name: 'settings')
  final KanIkkeSettingsModel? kanIkkeSettings;
  @JsonKey(name: 'userKanikke')
  final UserKanIkkeModel? userKanIkke;
  @JsonKey(name: 'blacklist')
  final BlacklistModel? blacklist;

  factory KanIkkeSettingsResponseModel.fromJson(Map<String, dynamic> json) =>
      _$KanIkkeSettingsResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$KanIkkeSettingsResponseModelToJson(this);
  static List<KanIkkeSettingsResponseModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) =>
              KanIkkeSettingsResponseModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class KanIkkeSettingsModel {
  KanIkkeSettingsModel({
    this.perDay,
    this.perWeek,
    this.perMonth,
    this.percentage,
    this.maxCustomerCall,
    this.maxLimitForBlacklist,
    this.maxDurationForBlacklist,
    this.featuresLimitation,
  });

  @JsonKey(name: 'perDay')
  final int? perDay;
  @JsonKey(name: 'perWeek')
  final int? perWeek;
  @JsonKey(name: 'perMonth')
  final int? perMonth;
  @JsonKey(name: 'percentage')
  final int? percentage;
  @JsonKey(name: 'maxCustomerCall')
  final int? maxCustomerCall;
  @JsonKey(name: 'maxLimitForBlacklist')
  final int? maxLimitForBlacklist;
  @JsonKey(name: 'maxDurationForBlacklist')
  final int? maxDurationForBlacklist;
  @JsonKey(name: 'featuresLimitation')
  final List<String>? featuresLimitation;

  factory KanIkkeSettingsModel.fromJson(Map<String, dynamic> json) =>
      _$KanIkkeSettingsModelFromJson(json);

  Map<String, dynamic> toJson() => _$KanIkkeSettingsModelToJson(this);
  static List<KanIkkeSettingsModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => KanIkkeSettingsModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class UserKanIkkeModel {
  UserKanIkkeModel({
    this.perDay,
    this.perWeek,
    this.perMonth,
  });

  @JsonKey(name: 'perDay')
  final UserCountModel? perDay;
  @JsonKey(name: 'perWeek')
  final UserCountModel? perWeek;
  @JsonKey(name: 'perMonth')
  final UserCountModel? perMonth;

  factory UserKanIkkeModel.fromJson(Map<String, dynamic> json) =>
      _$UserKanIkkeModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserKanIkkeModelToJson(this);
  static List<UserKanIkkeModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => UserKanIkkeModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class UserCountModel {
  UserCountModel({this.count, this.latestCreatedAt});

  @JsonKey(name: 'count')
  final int? count;
  @JsonKey(name: 'latestCreatedAt')
  final String? latestCreatedAt;

  factory UserCountModel.fromJson(Map<String, dynamic> json) =>
      _$UserCountModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserCountModelToJson(this);
  static List<UserCountModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => UserCountModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class BlacklistModel {
  BlacklistModel({
    this.isBlackListed,
    this.warningForBlackList,
    this.warningPopupText,
    this.popupText,
    this.startDate,
    this.endDate,
    this.totalBlacklist,
  });

  @JsonKey(name: 'isBlackListed')
  final bool? isBlackListed;
  @JsonKey(name: 'warningForBlackList')
  final bool? warningForBlackList;
  @JsonKey(name: 'warningPopupText')
  final String? warningPopupText;
  @JsonKey(name: 'popupText')
  final String? popupText;
  @JsonKey(name: 'startDate')
  final String? startDate;
  @JsonKey(name: 'endDate')
  final String? endDate;
  @JsonKey(name: 'totalBlacklist')
  final int? totalBlacklist;

  factory BlacklistModel.fromJson(Map<String, dynamic> json) =>
      _$BlacklistModelFromJson(json);

  Map<String, dynamic> toJson() => _$BlacklistModelToJson(this);
  static List<BlacklistModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => BlacklistModel.fromJson(i as Map<String, dynamic>))
      .toList();
}
