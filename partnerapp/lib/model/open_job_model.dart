import 'package:Haandvaerker.dk/model/open_job_mester_info_model.dart';
import 'package:Haandvaerker.dk/model/open_job_product_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'open_job_model.g.dart';

@JsonSerializable()
class OpenJob {
  @JsonKey(name: 'id_partner_smsopal')
  dynamic idPartnerSmsopal;
  @JsonKey(name: 'id_partner')
  dynamic idPartner;
  dynamic timeadd;
  dynamic timesend;
  dynamic destination;
  dynamic sent;
  dynamic appid;
  dynamic timed;
  dynamic reply;
  dynamic replytext;
  dynamic timereply;
  dynamic smssort;
  dynamic price;
  dynamic smsresend;
  dynamic smsresendtime;
  dynamic level;
  dynamic seen;
  dynamic deleted;
  dynamic timeseen;
  dynamic timedelete;
  dynamic matchingai;
  dynamic sent2;
  dynamic timesend2;
  dynamic amount;
  dynamic rate;
  dynamic gratis;
  dynamic mester;
  OpenJobMesterInfo? mesterinfo;
  dynamic website;
  dynamic mailingzip;
  dynamic mailingcity;
  dynamic image;
  @JsonKey(name: 'image_active')
  dynamic imageActive;
  dynamic tilbuddescription;
  @JsonKey(name: 'potential_id')
  dynamic potentialId;
  dynamic finalcheck;
  @JsonKey(name: 'contact_id')
  dynamic contactId;
  @JsonKey(name: 'comment_to_partner')
  dynamic commentToPartner;
  @JsonKey(name: 'appointment_date')
  dynamic appointmentDate;
  @JsonKey(name: 'id_times')
  dynamic idTimes;
  dynamic numpartner;
  dynamic modeprice;
  @JsonKey(name: 'partner_price')
  dynamic partnerPrice;
  dynamic productimage;
  dynamic productname;
  List<OpenJobProduct>? product;
  dynamic shorturl;
  dynamic other352781;
  dynamic othername352781;
  dynamic other352783;
  dynamic othername352783;
  dynamic other352785;
  dynamic othername352785;
  dynamic other352787;
  dynamic othername352787;
  dynamic other352788;
  dynamic othername352788;
  dynamic other352789;
  dynamic othername352789;
  dynamic other352793;
  dynamic othername352793;
  dynamic other352795;
  dynamic othername352795;
  dynamic other352796;
  dynamic othername352796;
  dynamic other352798;
  dynamic othername352798;
  dynamic other352800;
  dynamic othername352800;
  dynamic other352801;
  dynamic othername352801;
  dynamic other352803;
  dynamic othername352803;
  dynamic other352805;
  dynamic othername352805;
  dynamic other352806;
  dynamic othername352806;
  dynamic other352809;
  dynamic othername352809;
  dynamic other352808;
  dynamic othername352808;
  dynamic henttilbud;
  dynamic reserved;
  dynamic producttitle;
  @JsonKey(name: 'comment_to_customer')
  dynamic commentToCustomer;
  dynamic monthlyp;

  OpenJob({
    this.idPartnerSmsopal,
    this.idPartner,
    this.timeadd,
    this.timesend,
    this.destination,
    this.sent,
    this.appid,
    this.timed,
    this.reply,
    this.replytext,
    this.timereply,
    this.smssort,
    this.price,
    this.smsresend,
    this.smsresendtime,
    this.level,
    this.seen,
    this.deleted,
    this.timeseen,
    this.timedelete,
    this.matchingai,
    this.sent2,
    this.timesend2,
    this.amount,
    this.rate,
    this.gratis,
    this.mester,
    this.mesterinfo,
    this.website,
    this.mailingzip,
    this.mailingcity,
    this.image,
    this.imageActive,
    this.tilbuddescription,
    this.potentialId,
    this.finalcheck,
    this.contactId,
    this.commentToPartner,
    this.appointmentDate,
    this.idTimes,
    this.numpartner,
    this.modeprice,
    this.partnerPrice,
    this.productimage,
    this.productname,
    this.product,
    this.shorturl,
    this.other352781,
    this.othername352781,
    this.other352783,
    this.othername352783,
    this.other352788,
    this.othername352788,
    this.other352793,
    this.othername352793,
    this.other352795,
    this.othername352795,
    this.other352798,
    this.othername352798,
    this.other352805,
    this.othername352805,
    this.other352806,
    this.othername352806,
    this.other352809,
    this.othername352809,
    this.other352808,
    this.othername352808,
    this.henttilbud,
    this.reserved,
    this.producttitle,
    this.commentToCustomer,
    this.monthlyp,
  });

  factory OpenJob.fromJson(Map<String, dynamic> json) =>
      _$OpenJobFromJson(json);

  Map<String, dynamic> toJson() => _$OpenJobToJson(this);

  @override
  String toString() {
    return 'shorturl: $shorturl';
  }
}
