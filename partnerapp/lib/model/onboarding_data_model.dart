import 'package:json_annotation/json_annotation.dart';

part 'onboarding_data_model.g.dart';

@JsonSerializable()
class OnboardingDataModel {
  @JsonKey(name: 'percentage')
  final int? percentage;
  @JsonKey(name: 'steps')
  final List<OnboardingStepsModel>? steps;

  OnboardingDataModel(this.percentage, this.steps);

  factory OnboardingDataModel.fromJson(Map<String, dynamic> json) =>
      _$OnboardingDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$OnboardingDataModelToJson(this);
  static List<OnboardingDataModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => OnboardingDataModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class OnboardingStepsModel {
  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'UF_TITLE_EN')
  final String? titleEn;
  @JsonKey(name: 'UF_TITLE_DK')
  final String? titleDk;
  @JsonKey(name: 'UF_DESCRIPTION_EN')
  final String? descriptionEn;
  @JsonKey(name: 'UF_DESCRIPTION_DK')
  final String? descriptionDk;
  @JsonKey(name: 'UF_CODE')
  final String? ufCode;
  @JsonKey(name: 'UF_VIDEO_URL')
  final String? videoUrl;
  @JsonKey(name: 'UF_POINTS')
  final int? points;
  @JsonKey(name: 'UF_SORT')
  final int? sort;
  @JsonKey(name: 'UF_DELETED')
  final int? deleted;
  @JsonKey(name: 'UF_DURATION')
  final int? duration;
  @JsonKey(name: 'UF_DONE')
  final bool? done;

  OnboardingStepsModel(
      this.id,
      this.titleEn,
      this.titleDk,
      this.descriptionEn,
      this.descriptionDk,
      this.ufCode,
      this.videoUrl,
      this.points,
      this.sort,
      this.deleted,
      this.duration,
      this.done);

  factory OnboardingStepsModel.fromJson(Map<String, dynamic> json) =>
      _$OnboardingStepsModelFromJson(json);

  Map<String, dynamic> toJson() => _$OnboardingStepsModelToJson(this);
  static List<OnboardingStepsModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => OnboardingStepsModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
