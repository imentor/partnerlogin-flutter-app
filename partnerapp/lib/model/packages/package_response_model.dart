// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:json_annotation/json_annotation.dart';

part 'package_response_model.g.dart';

@JsonSerializable()
class PackageResponseModel {
  @JsonKey(name: 'success')
  bool? success;
  @JsonKey(name: 'message')
  String? message;
  @JsonKey(name: 'data')
  PackageData? data;

  PackageResponseModel({this.success, this.message, this.data});

  factory PackageResponseModel.fromJson(Map<String, dynamic> json) =>
      _$PackageResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$PackageResponseModelToJson(this);
}

@JsonSerializable()
class PackageData {
  @JsonKey(name: 'ACTIVE')
  bool? active;
  @JsonKey(name: 'CONTRACT_FILE')
  String? contractFile;
  @JsonKey(name: 'ID')
  String? id;
  @JsonKey(name: 'COMPANY')
  String? company;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'TITLE')
  String? title;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'PRICE')
  String? price;
  @JsonKey(name: 'STARTUP_FEE')
  String? startUpFee;
  @JsonKey(name: 'PRICE_LIST')
  List<Price>? priceList;
  @JsonKey(name: 'PACKAGE_HANDLE')
  String? packageHandle;
  @JsonKey(name: 'PACKAGE_FEATURES')
  List<PackageFeature>? packageFeatures;
  @JsonKey(name: 'PACKAGE_ADDONS')
  List<PackageFeature>? packageAddOns;
  @JsonKey(name: 'INTERVAL')
  dynamic interval;
  @JsonKey(name: 'STATUS')
  String? status;
  @JsonKey(name: 'DATE_CREATED')
  String? dateCreated;
  @JsonKey(name: 'nummode')
  int? nummode;
  @JsonKey(name: 'reserve_size')
  String? reserveSize;
  @JsonKey(name: 'booking')
  String? booking;
  @JsonKey(name: 'onboarding')
  Map<String, dynamic>? onboarding;
  @JsonKey(name: 'has_canceled')
  final bool? hasCanceled;
  @JsonKey(name: 'play_marketplace_video')
  final bool? playMarketPlaceVideo;
  @JsonKey(name: 'PENDING_ONBOARDING')
  final bool? pendingOnboarding;
  @JsonKey(name: 'ONBOARDING_URL')
  final String? onboardingUrl;
  @JsonKey(name: 'SEMI_BLACKLIST')
  final int? semiBlacklist;
  @JsonKey(name: 'NOT_PAID')
  bool? notPaid;
  @JsonKey(name: 'NOT_PAID_POPUP')
  bool? notPaidPopup;
  @JsonKey(name: 'NOT_PAID_ALERT')
  bool? notPaidAlert;
  @JsonKey(name: 'marketplace_notpaid')
  bool? marketplaceNotPaid;

  PackageData({
    this.active,
    this.id,
    this.company,
    this.name,
    this.title,
    this.description,
    this.price,
    this.startUpFee,
    this.priceList,
    this.packageHandle,
    this.packageFeatures,
    this.packageAddOns,
    this.interval,
    this.status,
    this.dateCreated,
    this.nummode,
    this.reserveSize,
    this.booking,
    this.onboarding,
    this.hasCanceled,
    this.playMarketPlaceVideo,
    this.pendingOnboarding,
    this.onboardingUrl,
    this.semiBlacklist,
    this.notPaid,
    this.contractFile,
    this.notPaidAlert,
    this.notPaidPopup,
    this.marketplaceNotPaid,
  });

  factory PackageData.fromJson(Map<String, dynamic> json) =>
      _$PackageDataFromJson(json);

  Map<String, dynamic> toJson() => _$PackageDataToJson(this);
}

@JsonSerializable()
class Price {
  @JsonKey(name: "monthlyprice")
  String? monthlyPrice;
  @JsonKey(name: "stratupprice")
  String? startUpPrice;
  @JsonKey(name: "onetimeprice")
  String? oneTimePrice;

  Price({
    this.monthlyPrice,
    this.startUpPrice,
    this.oneTimePrice,
  });

  factory Price.fromJson(Map<String, dynamic> json) => _$PriceFromJson(json);
  Map<String, dynamic> toJson() => _$PriceToJson(this);
}

@JsonSerializable()
class PackageFeature {
  @JsonKey(name: "NAME")
  String? name;
  @JsonKey(name: "ID")
  String? id;
  @JsonKey(name: "SHORT_CODE")
  String? shortCode;
  @JsonKey(name: "ADDONS")
  String? addOns;
  @JsonKey(name: "TYPE")
  dynamic type;
  @JsonKey(name: "LIMITATION")
  dynamic limitation;

  PackageFeature({
    this.name,
    this.id,
    this.shortCode,
    this.addOns,
    this.type,
    this.limitation,
  });

  factory PackageFeature.fromJson(Map<String, dynamic> json) =>
      _$PackageFeatureFromJson(json);

  Map<String, dynamic> toJson() => _$PackageFeatureToJson(this);
}
