import 'package:json_annotation/json_annotation.dart';

part 'faq_model.g.dart';

@JsonSerializable()
class FAQModel {
  FAQModel({
    this.id,
    this.answerDA,
    this.answerEN,
    this.questionDA,
    this.questionEN,
    this.stage,
  });

  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'ANSWER_DA')
  final String? answerDA;
  @JsonKey(name: 'ANSWER_EN')
  final String? answerEN;
  @JsonKey(name: 'QUESTION_DA')
  final String? questionDA;
  @JsonKey(name: 'QUESTION_EN')
  final String? questionEN;
  @JsonKey(name: 'STAGE')
  final String? stage;

  factory FAQModel.fromJson(Map<String, dynamic> json) =>
      _$FAQModelFromJson(json);

  Map<String, dynamic> toJson() => _$FAQModelToJson(this);
  static List<FAQModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => FAQModel.fromJson(i as Map<String, dynamic>))
      .toList();
}
