import 'package:json_annotation/json_annotation.dart';

part 'recommendation_model.g.dart';

@JsonSerializable()
class RecommendationModel {
  RecommendationModel({
    this.currentPage,
    this.lastPage,
    this.items = const <Recommendation>[],
  });

  @JsonKey(name: 'current_page')
  final int? currentPage;
  @JsonKey(name: 'last_page')
  final int? lastPage;
  @JsonKey(name: 'items')
  final List<Recommendation> items;

  factory RecommendationModel.fromJson(Map<String, dynamic> json) =>
      _$RecommendationModelFromJson(json);

  Map<String, dynamic> toJson() => _$RecommendationModelToJson(this);
  static List<RecommendationModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => RecommendationModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class Recommendation {
  Recommendation({
    this.agreementRating,
    this.branch,
    this.cleaningRating,
    this.comment,
    this.contact,
    this.contactRating,
    this.date,
    this.description,
    this.id,
    this.partner,
    this.priceRating,
    this.projectId,
    this.rating,
    this.recommendRating,
    this.reviewId,
    this.status,
    this.title,
    this.workRating,
    this.createdOnShow,
    this.headline,
  });

  @JsonKey(name: 'AGREEMENT_RATING')
  final dynamic agreementRating;
  @JsonKey(name: 'BRANCH')
  final String? branch;
  @JsonKey(name: 'CLEANING_RATING')
  final dynamic cleaningRating;
  @JsonKey(name: 'COMMENT')
  final String? comment;
  @JsonKey(name: 'CONTACT')
  final RecommendationContact? contact;
  @JsonKey(name: 'CONTACT_RATING')
  final dynamic contactRating;
  @JsonKey(name: 'DATE')
  final String? date;
  @JsonKey(name: 'DESCRIPTION')
  final String? description;
  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'PARTNER')
  final dynamic partner;
  @JsonKey(name: 'PRICE_RATING')
  final dynamic priceRating;
  @JsonKey(name: 'PROJECT_ID')
  final int? projectId;
  @JsonKey(name: 'RATING')
  final dynamic rating;
  @JsonKey(name: 'RECOMMEND_RATING')
  final dynamic recommendRating;
  @JsonKey(name: 'REVIEW_ID')
  final int? reviewId;
  @JsonKey(name: 'STATUS')
  final String? status;
  @JsonKey(name: 'TITLE')
  final String? title;
  @JsonKey(name: 'WORK_RATING')
  final dynamic workRating;
  @JsonKey(name: 'CREATED_ON_SHOW')
  final String? createdOnShow;
  @JsonKey(name: 'HEADLINE')
  final String? headline;

  factory Recommendation.fromJson(Map<String, dynamic> json) =>
      _$RecommendationFromJson(json);

  Map<String, dynamic> toJson() => _$RecommendationToJson(this);
  static List<Recommendation> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => Recommendation.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class RecommendationContact {
  RecommendationContact({
    this.avatar,
    this.company,
    this.email,
    this.id,
    this.mobile,
    this.name,
  });

  @JsonKey(name: 'AVATAR')
  final String? avatar;
  @JsonKey(name: 'COMPANY')
  final String? company;
  @JsonKey(name: 'EMAIL')
  final String? email;
  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'MOBILE')
  final String? mobile;
  @JsonKey(name: 'NAME')
  final String? name;

  factory RecommendationContact.fromJson(Map<String, dynamic> json) =>
      _$RecommendationContactFromJson(json);

  Map<String, dynamic> toJson() => _$RecommendationContactToJson(this);
  static List<RecommendationContact> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => RecommendationContact.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class RecommendationDownloadImage {
  RecommendationDownloadImage({
    this.url,
    this.base64,
  });

  @JsonKey(name: 'url')
  final String? url;
  @JsonKey(name: 'base64')
  final String? base64;

  factory RecommendationDownloadImage.fromJson(Map<String, dynamic> json) =>
      _$RecommendationDownloadImageFromJson(json);

  Map<String, dynamic> toJson() => _$RecommendationDownloadImageToJson(this);
  static List<RecommendationDownloadImage> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) =>
              RecommendationDownloadImage.fromJson(i as Map<String, dynamic>))
          .toList();
}
