import 'package:Haandvaerker.dk/model/recommendation/partners_reviews_on_client.dart';
import 'package:json_annotation/json_annotation.dart';

part 'partners_reviews_model.g.dart';

@JsonSerializable()
class PartnerReviewsModel {
  @JsonKey(name: 'current_page')
  int? currentPage;
  @JsonKey(name: 'data')
  List<PartnersReviewsOnClient>? data;
  @JsonKey(name: 'first_page_url')
  String? firstPageUrl;
  @JsonKey(name: 'from')
  int? from;
  @JsonKey(name: 'last_page')
  int? lastPage;
  @JsonKey(name: 'last_page_url')
  String? lastPageUrl;
  @JsonKey(name: 'links')
  List<LinksModel>? links;
  @JsonKey(name: 'next_page_url')
  String? nextPageUrl;
  @JsonKey(name: 'path')
  String? path;
  @JsonKey(name: 'per_page')
  int? perPage;
  @JsonKey(name: 'prev_page_url')
  String? prevPageUrl;
  @JsonKey(name: 'to')
  int? to;
  @JsonKey(name: 'total')
  int? total;

  PartnerReviewsModel({
    this.currentPage,
    this.data,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.links,
    this.nextPageUrl,
    this.path,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  factory PartnerReviewsModel.fromJson(Map<String, dynamic> json) =>
      _$PartnerReviewsModelFromJson(json);
  Map<String, dynamic> toJson() => _$PartnerReviewsModelToJson(this);
  static List<PartnerReviewsModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => PartnerReviewsModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class LinksModel {
  @JsonKey(name: 'url')
  String? url;
  @JsonKey(name: 'label')
  String? label;
  @JsonKey(name: 'active')
  bool? active;

  LinksModel({
    this.url,
    this.label,
    this.active,
  });

  factory LinksModel.fromJson(Map<String, dynamic> json) =>
      _$LinksModelFromJson(json);
  Map<String, dynamic> toJson() => _$LinksModelToJson(this);
  static List<LinksModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => LinksModel.fromJson(i as Map<String, dynamic>))
      .toList();
}
