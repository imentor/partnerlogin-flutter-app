import 'package:json_annotation/json_annotation.dart';

part 'invited_customer_model.g.dart';

@JsonSerializable()
class InvitedCustomerModel {
  InvitedCustomerModel({
    this.customerEmail,
    this.customerImage,
    this.customerName,
    this.cutomerPhone,
    this.dateCreated,
    this.dateUpdated,
    this.id,
    this.isDeleted,
    this.partnerId,
  });

  @JsonKey(name: 'CUSTOMER_EMAIL')
  final String? customerEmail;
  @JsonKey(name: 'CUSTOMER_IMAGE')
  final String? customerImage;
  @JsonKey(name: 'CUSTOMER_NAME')
  final String? customerName;
  @JsonKey(name: 'CUSTOMER_PHONE')
  final String? cutomerPhone;
  @JsonKey(name: 'DATE_CREATED')
  final String? dateCreated;
  @JsonKey(name: 'DATE_UPDATED')
  final String? dateUpdated;
  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'IS_DELETED')
  final int? isDeleted;
  @JsonKey(name: 'PARTNER_ID')
  final int? partnerId;

  factory InvitedCustomerModel.fromJson(Map<String, dynamic> json) =>
      _$InvitedCustomerModelFromJson(json);

  Map<String, dynamic> toJson() => _$InvitedCustomerModelToJson(this);
  static List<InvitedCustomerModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => InvitedCustomerModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
