import 'package:json_annotation/json_annotation.dart';

part 'company_reviews_model.g.dart';

@JsonSerializable()
class CompanyReviewsModel {
  @JsonKey(name: 'CompanyReviewId')
  int? companyReviewId;
  @JsonKey(name: 'BrancheIDPrisen')
  int? brancheIdPrisen;
  @JsonKey(name: 'Branche')
  String? branche;

  CompanyReviewsModel({
    this.companyReviewId,
    this.brancheIdPrisen,
    this.branche,
  });

  CompanyReviewsModel.defaults()
      : companyReviewId = 0,
        brancheIdPrisen = 0,
        branche = '';

  factory CompanyReviewsModel.fromJson(Map<String, dynamic> json) =>
      _$CompanyReviewsModelFromJson(json);

  Map<String, dynamic> toJson() => _$CompanyReviewsModelToJson(this);
  static List<CompanyReviewsModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => CompanyReviewsModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
