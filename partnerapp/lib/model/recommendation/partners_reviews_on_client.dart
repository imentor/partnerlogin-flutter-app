import 'package:json_annotation/json_annotation.dart';

part 'partners_reviews_on_client.g.dart';

@JsonSerializable()
class PartnersReviewsOnClient {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'UF_CUSTOMER_ID')
  String? ufCustomerId;
  @JsonKey(name: 'UF_LEV_ID')
  String? ufLevId;
  @JsonKey(name: 'UF_REVIEW_DATE')
  String? ufReviewDate;
  @JsonKey(name: 'UF_HEADLINE')
  String? ufHeadline;
  @JsonKey(name: 'UF_COMMENT')
  String? ufComment;
  @JsonKey(name: 'UF_ANSWER')
  String? ufAnswer;
  @JsonKey(name: 'UF_CUSTOMER_SUPPORT_COMMENT')
  String? ufCustomerSupportComment;
  @JsonKey(name: 'UF_CONTACTED')
  int? ufContacted;
  @JsonKey(name: 'UF_ENTERPRISE_SUM')
  int? ufEnterpriseSum;
  @JsonKey(name: 'UF_VERIFICATION_SOURCE')
  String? ufVerificationSource;
  @JsonKey(name: 'UF_STATUS')
  String? ufStatus;
  @JsonKey(name: 'UF_APPROVAL_DATE')
  String? ufApprovalDate;
  @JsonKey(name: 'UF_RATING_OVERALL')
  double? ufRatingOverall;
  @JsonKey(name: 'UF_RATING_COMMUNICATION')
  double? ufRatingCommunication;
  @JsonKey(name: 'UF_RATING_TIME')
  double? ufRatingTime;
  @JsonKey(name: 'UF_RATING_REALISTIC_EXPECTATIONS')
  double? ufRatingRealisticExpectations;
  @JsonKey(name: 'UF_RATING_FINANCIAL_SECURITY')
  double? ufRatingFinancialSecurity;
  @JsonKey(name: 'UF_RATING_COLLABORATION')
  double? ufRatingCollaboration;
  @JsonKey(name: 'UF_RATING_ACCESS')
  dynamic ufRatingAccess;
  @JsonKey(name: 'HOMEOWNER')
  Reviewer? homeOwnerName;
  @JsonKey(name: 'SUPPLIERINFO')
  Reviewer? supplierInfo;

  PartnersReviewsOnClient({
    this.homeOwnerName,
    this.id,
    this.supplierInfo,
    this.ufAnswer,
    this.ufApprovalDate,
    this.ufComment,
    this.ufContacted,
    this.ufCustomerId,
    this.ufCustomerSupportComment,
    this.ufEnterpriseSum,
    this.ufHeadline,
    this.ufLevId,
    this.ufRatingAccess,
    this.ufRatingCollaboration,
    this.ufRatingCommunication,
    this.ufRatingFinancialSecurity,
    this.ufRatingOverall,
    this.ufRatingRealisticExpectations,
    this.ufRatingTime,
    this.ufReviewDate,
    this.ufStatus,
    this.ufVerificationSource,
  });

  factory PartnersReviewsOnClient.fromJson(Map<String, dynamic> json) =>
      _$PartnersReviewsOnClientFromJson(json);

  Map<String, dynamic> toJson() => _$PartnersReviewsOnClientToJson(this);

  static List<PartnersReviewsOnClient> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => PartnersReviewsOnClient.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class Reviewer {
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'AVATAR')
  String? avatar;

  Reviewer({this.name, this.avatar});

  factory Reviewer.fromJson(Map<String, dynamic> json) =>
      _$ReviewerFromJson(json);

  Map<String, dynamic> toJson() => _$ReviewerToJson(this);

  static List<Reviewer> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => Reviewer.fromJson(i as Map<String, dynamic>))
      .toList();
}
