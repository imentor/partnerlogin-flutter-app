import 'package:json_annotation/json_annotation.dart';

part 'open_job_mester_info_model.g.dart';

@JsonSerializable()
class OpenJobMesterInfo {
  @JsonKey(name: '0')
  dynamic s0;
  @JsonKey(name: '1')
  dynamic s1;
  @JsonKey(name: '2')
  dynamic s2;
  @JsonKey(name: '3')
  dynamic s3;
  @JsonKey(name: '4')
  dynamic s4;
  @JsonKey(name: '5')
  dynamic s5;
  @JsonKey(name: '6')
  dynamic s6;
  @JsonKey(name: '7')
  dynamic s7;
  @JsonKey(name: '8')
  dynamic s8;
  @JsonKey(name: '9')
  dynamic s9;
  @JsonKey(name: '10')
  dynamic s10;
  @JsonKey(name: '11')
  dynamic s11;
  @JsonKey(name: '12')
  dynamic s12;
  @JsonKey(name: '13')
  dynamic s13;
  @JsonKey(name: '14')
  dynamic s14;
  @JsonKey(name: '15')
  dynamic s15;
  @JsonKey(name: '16')
  dynamic s16;
  @JsonKey(name: '17')
  dynamic s17;
  @JsonKey(name: '18')
  dynamic s18;
  @JsonKey(name: '19')
  dynamic s19;
  @JsonKey(name: '20')
  dynamic s20;
  @JsonKey(name: '21')
  dynamic s21;
  dynamic appid;
  dynamic created;
  dynamic desc1;
  dynamic dk;
  dynamic done;
  dynamic duration1;
  dynamic en;
  @JsonKey(name: 'final')
  dynamic finall;
  dynamic id;
  List<dynamic>? imagelist;
  dynamic ip;
  @JsonKey(name: 'nps_date')
  dynamic npsDate;
  @JsonKey(name: 'nps_sent')
  dynamic npsSent;
  dynamic numofman1;
  dynamic offer1;
  dynamic opgave1;
  dynamic pid;
  dynamic postnumber1;
  dynamic potentialid;
  dynamic startdate1;
  dynamic subopgave1;
  dynamic timepris1;

  OpenJobMesterInfo({
    this.s0,
    this.s1,
    this.s2,
    this.s3,
    this.s4,
    this.s5,
    this.s6,
    this.s7,
    this.s8,
    this.s9,
    this.s10,
    this.s11,
    this.s12,
    this.s13,
    this.s14,
    this.s15,
    this.s16,
    this.s17,
    this.s18,
    this.s19,
    this.s20,
    this.s21,
    this.appid,
    this.created,
    this.desc1,
    this.dk,
    this.done,
    this.duration1,
    this.en,
    this.finall,
    this.id,
    this.ip,
    this.npsDate,
    this.npsSent,
    this.numofman1,
    this.offer1,
    this.opgave1,
    this.pid,
    this.postnumber1,
    this.potentialid,
    this.startdate1,
    this.subopgave1,
    this.timepris1,
  });

  factory OpenJobMesterInfo.fromJson(Map<String, dynamic> json) =>
      _$OpenJobMesterInfoFromJson(json);

  Map<String, dynamic> toJson() => _$OpenJobMesterInfoToJson(this);
}
