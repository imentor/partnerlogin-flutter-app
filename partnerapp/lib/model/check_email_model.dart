import 'package:json_annotation/json_annotation.dart';

part 'check_email_model.g.dart';

@JsonSerializable()
class CheckEmailModel {
  final bool? activeEmail;
  final String? email;

  CheckEmailModel({
    this.activeEmail,
    this.email,
  });

  factory CheckEmailModel.fromJson(Map<String, dynamic> json) =>
      _$CheckEmailModelFromJson(json);
  Map<String, dynamic> toJson() => _$CheckEmailModelToJson(this);

  @override
  String toString() =>
      'CheckEmailModel(activeEmail: $activeEmail, email: $email)';
}
