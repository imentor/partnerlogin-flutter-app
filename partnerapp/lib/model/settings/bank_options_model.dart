import 'package:json_annotation/json_annotation.dart';

part 'bank_options_model.g.dart';

@JsonSerializable()
class BankOptionsModel {
  BankOptionsModel({
    this.id,
    this.bank,
    this.dateCreated,
  });

  BankOptionsModel.defaults()
      : id = 0,
        bank = '',
        dateCreated = '';

  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'BANK')
  final String? bank;
  @JsonKey(name: 'DATE_CREATED')
  final String? dateCreated;

  factory BankOptionsModel.fromJson(Map<String, dynamic> json) =>
      _$BankOptionsModelFromJson(json);

  Map<String, dynamic> toJson() => _$BankOptionsModelToJson(this);
  static List<BankOptionsModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => BankOptionsModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
