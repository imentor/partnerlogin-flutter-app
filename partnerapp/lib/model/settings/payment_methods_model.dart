import 'package:json_annotation/json_annotation.dart';

part 'payment_methods_model.g.dart';

@JsonSerializable()
class PaymentMethodsModel {
  PaymentMethodsModel({
    this.id,
    this.levId,
    this.partnerId,
    this.registrationNumber,
    this.bankAccount,
    this.bank,
    this.cvr,
    this.deleted,
    this.error,
    this.sftp,
    this.sftpDate,
    this.dateCreated,
  });

  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'LEV_ID')
  final int? levId;
  @JsonKey(name: 'PARTNER_ID')
  final int? partnerId;
  @JsonKey(name: 'REGISTRATION_NUMBER')
  final String? registrationNumber;
  @JsonKey(name: 'BANK_ACCOUNT')
  final String? bankAccount;
  @JsonKey(name: 'BANK')
  final String? bank;
  @JsonKey(name: 'CVR')
  final String? cvr;
  @JsonKey(name: 'DELETED')
  final int? deleted;
  @JsonKey(name: 'ERROR')
  final int? error;
  @JsonKey(name: 'SFTP')
  final int? sftp;
  @JsonKey(name: 'SFTP_DATE')
  final String? sftpDate;
  @JsonKey(name: 'DATE_CREATED')
  final String? dateCreated;

  factory PaymentMethodsModel.fromJson(Map<String, dynamic> json) =>
      _$PaymentMethodsModelFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentMethodsModelToJson(this);
  static List<PaymentMethodsModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => PaymentMethodsModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
