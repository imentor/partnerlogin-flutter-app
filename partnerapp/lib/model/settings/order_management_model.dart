import 'package:json_annotation/json_annotation.dart';

part 'order_management_model.g.dart';

@JsonSerializable()
class OrderManagementModel {
  OrderManagementModel({
    this.id,
    this.partnerId,
    this.apiKey,
    this.dateCreated,
  });

  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'PARTNER_ID')
  final int? partnerId;
  @JsonKey(name: 'API_KEY')
  final String? apiKey;
  @JsonKey(name: 'DATE_CREATED')
  final String? dateCreated;

  factory OrderManagementModel.fromJson(Map<String, dynamic> json) =>
      _$OrderManagementModelFromJson(json);

  Map<String, dynamic> toJson() => _$OrderManagementModelToJson(this);
  static List<OrderManagementModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => OrderManagementModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
