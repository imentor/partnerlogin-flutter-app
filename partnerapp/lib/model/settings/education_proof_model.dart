import 'package:json_annotation/json_annotation.dart';

part 'education_proof_model.g.dart';

@JsonSerializable()
class EducationProofModel {
  EducationProofModel({
    this.id,
    this.description,
    this.branche,
    this.pictureURL,
    this.stage,
  });

  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'DESCRIPTION')
  final String? description;
  @JsonKey(name: 'BRANCHE')
  final String? branche;
  @JsonKey(name: 'PICTURE_URL')
  final String? pictureURL;
  @JsonKey(name: 'STAGE')
  final String? stage;

  factory EducationProofModel.fromJson(Map<String, dynamic> json) =>
      _$EducationProofModelFromJson(json);

  Map<String, dynamic> toJson() => _$EducationProofModelToJson(this);
  static List<EducationProofModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => EducationProofModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
