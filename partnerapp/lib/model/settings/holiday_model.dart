import 'package:json_annotation/json_annotation.dart';

part 'holiday_model.g.dart';

@JsonSerializable()
class HolidayModel {
  HolidayModel({
    this.dateCreated,
    this.holidayFrom,
    this.holidayTo,
    this.id,
    this.partnerId,
  });

  @JsonKey(name: 'DATE_CREATED')
  final String? dateCreated;
  @JsonKey(name: 'HOLIDAY_FROM')
  final String? holidayFrom;
  @JsonKey(name: 'HOLIDAY_TO')
  final String? holidayTo;
  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'PARTNER_ID')
  final int? partnerId;

  factory HolidayModel.fromJson(Map<String, dynamic> json) =>
      _$HolidayModelFromJson(json);

  Map<String, dynamic> toJson() => _$HolidayModelToJson(this);
  static List<HolidayModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => HolidayModel.fromJson(i as Map<String, dynamic>))
      .toList();
}
