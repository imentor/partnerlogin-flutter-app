import 'package:json_annotation/json_annotation.dart';

part 'regions_model.g.dart';

@JsonSerializable()
class RegionModel {
  RegionModel({
    this.id,
    this.name,
    this.created,
    this.regionId,
  });

  RegionModel.defaults()
      : id = 0,
        name = '',
        created = '',
        regionId = 0;

  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'NAME')
  final String? name;
  @JsonKey(name: 'CREATED')
  final String? created;
  @JsonKey(name: 'REGION_ID')
  final int? regionId;

  factory RegionModel.fromJson(Map<String, dynamic> json) =>
      _$RegionModelFromJson(json);

  Map<String, dynamic> toJson() => _$RegionModelToJson(this);
  static List<RegionModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => RegionModel.fromJson(i as Map<String, dynamic>))
      .toList();
}
