import 'package:json_annotation/json_annotation.dart';

part 'master_info_model.g.dart';

@JsonSerializable()
class MasterInfoGeneralModel {
  MasterInfoGeneralModel({
    required this.labelDA,
    required this.labelEN,
    required this.subLabelDA,
    required this.subLabelEN,
    required this.labelType,
    required this.companyLowSize,
    required this.companyAssignmentSize,
    required this.startWorkMonth,
    required this.endWorkMonth,
    required this.choicesDA,
    required this.choicesEN,
    required this.choicesDA2,
    required this.choicesEN2,
    required this.help,
    required this.value,
    required this.name,
  });

  @JsonKey(name: 'LABEL_DA', defaultValue: '')
  final String labelDA;
  @JsonKey(name: 'LABEL_EN', defaultValue: '')
  final String labelEN;
  @JsonKey(name: 'SUB_LABEL_DA', defaultValue: '')
  final String subLabelDA;
  @JsonKey(name: 'SUB_LABEL_EN', defaultValue: '')
  final String subLabelEN;
  @JsonKey(name: 'LABEL_TYPE', defaultValue: '')
  final String labelType;
  @JsonKey(name: 'HELP', defaultValue: '')
  final String help;
  @JsonKey(name: 'UF_CRM_COMPANY_LOW_SIZE')
  final int? companyLowSize;
  @JsonKey(name: 'UF_CRM_COMPANY_ASSIGNMENT_SIZE')
  final int? companyAssignmentSize;
  @JsonKey(name: 'UF_STARTWORKMONTH')
  final int? startWorkMonth;
  @JsonKey(name: 'UF_ENDWORKMONTH')
  final int? endWorkMonth;
  @JsonKey(name: 'da', defaultValue: <String>[])
  final List<String> choicesDA;
  @JsonKey(name: 'en', defaultValue: <String>[])
  final List<String> choicesEN;
  @JsonKey(name: 'da_2', defaultValue: <String>[])
  final List<String> choicesDA2;
  @JsonKey(name: 'en_2', defaultValue: <String>[])
  final List<String> choicesEN2;
  @JsonKey(name: 'value')
  final dynamic value;
  @JsonKey(name: 'NAME')
  final String? name;

  factory MasterInfoGeneralModel.fromJson(Map<String, dynamic> json) =>
      _$MasterInfoGeneralModelFromJson(json);

  Map<String, dynamic> toJson() => _$MasterInfoGeneralModelToJson(this);
  static List<MasterInfoGeneralModel> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => MasterInfoGeneralModel.fromJson(i as Map<String, dynamic>))
      .toList();
}
