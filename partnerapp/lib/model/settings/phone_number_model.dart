import 'package:json_annotation/json_annotation.dart';

part 'phone_number_model.g.dart';

@JsonSerializable()
class PhoneNumberModel {
  PhoneNumberModel({
    this.id,
    this.partnerId,
    this.phone,
    this.dateCreated,
  });

  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'PARTNER_ID')
  final int? partnerId;
  @JsonKey(name: 'PHONE')
  final String? phone;
  @JsonKey(name: 'DATE_CREATED')
  final String? dateCreated;

  factory PhoneNumberModel.fromJson(Map<String, dynamic> json) =>
      _$PhoneNumberModelFromJson(json);

  Map<String, dynamic> toJson() => _$PhoneNumberModelToJson(this);
  static List<PhoneNumberModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => PhoneNumberModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
