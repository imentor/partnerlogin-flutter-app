import 'package:json_annotation/json_annotation.dart';

part 'udbud_response_model.g.dart';

@JsonSerializable()
class UdbudResponseModel {
  List<UdbudModel>? data;

  UdbudResponseModel({this.data});

  factory UdbudResponseModel.fromJson(Map<String, dynamic> json) =>
      _$UdbudResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$UdbudResponseModelToJson(this);
}

@JsonSerializable()
class UdbudModel {
  @JsonKey(name: 'ID')
  final String? id;
  @JsonKey(name: 'TENDER_ID')
  final String? tenderId;
  @JsonKey(name: 'TENDER_TITLE')
  final String? tenderTitle;
  @JsonKey(name: 'PROCUREMENT_TITLE')
  final String? procurementTitle;
  @JsonKey(name: 'PROCUREMENT_DESCRIPTION')
  final String? procurementDescription;
  @JsonKey(name: 'DATE_CREATED')
  final String? dateCreated;
  @JsonKey(name: 'DATE_EDITED')
  final String? dateEdited;
  @JsonKey(name: 'POSTAL_CODE')
  final String? postalCode;
  @JsonKey(name: 'CITY')
  final String? city;
  @JsonKey(name: 'TENDER_DEADLINE')
  final String? tenderDeadline;
  @JsonKey(name: 'FORM_URL_ID')
  final String? formUrlId;
  @JsonKey(name: 'ACTIVE_TENDER')
  final String? activeTender;

  UdbudModel({
    this.id,
    this.tenderId,
    this.tenderTitle,
    this.procurementTitle,
    this.procurementDescription,
    this.dateCreated,
    this.dateEdited,
    this.postalCode,
    this.city,
    this.tenderDeadline,
    this.formUrlId,
    this.activeTender,
  });

  factory UdbudModel.fromJson(Map<String, dynamic> json) =>
      _$UdbudModelFromJson(json);

  Map<String, dynamic> toJson() => _$UdbudModelToJson(this);
}
