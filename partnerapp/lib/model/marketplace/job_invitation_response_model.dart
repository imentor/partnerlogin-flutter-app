import 'package:Haandvaerker.dk/model/marketplace/marketplace_v2_response_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'job_invitation_response_model.g.dart';

@JsonSerializable()
class JobInvitationPagination {
  @JsonKey(name: 'total')
  int? total;
  @JsonKey(name: 'current_page')
  int? currentPage;
  @JsonKey(name: 'last_page')
  int? lastPage;
  @JsonKey(name: 'items')
  List<JobInvitationResponseModel>? jobInvites;

  JobInvitationPagination(
      {this.currentPage, this.jobInvites, this.lastPage, this.total});

  factory JobInvitationPagination.empty() => JobInvitationPagination();

  factory JobInvitationPagination.fromJson(Map<String, dynamic> json) =>
      _$JobInvitationPaginationFromJson(json);

  Map<String, dynamic> toJson() => _$JobInvitationPaginationToJson(this);
}

@JsonSerializable()
class JobInvitationResponseModel {
  @JsonKey(name: 'INVITATION')
  InvitationModel? invitation;
  @JsonKey(name: 'INVITATION_ID')
  int? invitationId;
  @JsonKey(name: 'PROJECT')
  MarketPlaceV2Items? project;
  @JsonKey(name: 'STATUS')
  String? status;

  JobInvitationResponseModel({
    this.invitation,
    this.invitationId,
    this.project,
    this.status,
  });

  factory JobInvitationResponseModel.fromJson(Map<String, dynamic> json) =>
      _$JobInvitationResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$JobInvitationResponseModelToJson(this);

  static List<JobInvitationResponseModel> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map(
          (i) => JobInvitationResponseModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

class MarketPlaceV2Item {}

@JsonSerializable()
class InvitationModel {
  @JsonKey(name: 'CONTACT_ID')
  int? contactId;
  @JsonKey(name: 'CREATED_AT')
  String? createdAt;
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'IS_DELETED')
  int? isDeleted;
  @JsonKey(name: 'JOB_INDUSTRY')
  int? jobIndustry;
  @JsonKey(name: 'PARENT_PROJECT')
  int? parentProject;
  @JsonKey(name: 'PARTNER_ID')
  int? partnerId;
  @JsonKey(name: 'PROJECT_ID')
  int? projectId;
  @JsonKey(name: 'STATUS')
  String? status;
  @JsonKey(name: 'SUB_PROJECT')
  dynamic subProject;
  @JsonKey(name: 'UPDATED_AT')
  String? updatedAt;

  InvitationModel({
    this.contactId,
    this.createdAt,
    this.id,
    this.isDeleted,
    this.jobIndustry,
    this.parentProject,
    this.partnerId,
    this.projectId,
    this.status,
    this.subProject,
    this.updatedAt,
  });

  factory InvitationModel.fromJson(Map<String, dynamic> json) =>
      _$InvitationModelFromJson(json);

  Map<String, dynamic> toJson() => _$InvitationModelToJson(this);

  static List<InvitationModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => InvitationModel.fromJson(i as Map<String, dynamic>))
      .toList();
}
