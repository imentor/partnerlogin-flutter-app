import 'package:json_annotation/json_annotation.dart';

part 'check_reservation_model.g.dart';

@JsonSerializable()
class CheckBitrixReservationResponseModel {
  @JsonKey(name: 'success')
  bool? success;
  @JsonKey(name: 'message')
  String? message;
  @JsonKey(name: 'price')
  double? price;

  CheckBitrixReservationResponseModel({
    this.success,
    this.message,
    this.price,
  });

  factory CheckBitrixReservationResponseModel.fromJson(
          Map<String, dynamic> json) =>
      _$CheckBitrixReservationResponseModelFromJson(json);

  Map<String, dynamic> toJson() =>
      _$CheckBitrixReservationResponseModelToJson(this);

  static List<CheckBitrixReservationResponseModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => CheckBitrixReservationResponseModel.fromJson(
              i as Map<String, dynamic>))
          .toList();
}
