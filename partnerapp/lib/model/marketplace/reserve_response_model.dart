import 'package:json_annotation/json_annotation.dart';

part 'reserve_response_model.g.dart';

@JsonSerializable()
class ReserveResponseModel {
  @JsonKey(name: 'CONTACT_ID')
  int? contactId;
  @JsonKey(name: 'CREATED_AT')
  String? createdAt;
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'PARTNER_ID')
  int? partnerId;
  @JsonKey(name: 'PROJECT_ID')
  String? projectId;
  @JsonKey(name: 'STATUS_ID')
  int? statusId;
  @JsonKey(name: 'UPDATED_AT')
  String? updatedAt;

  ReserveResponseModel({
    this.contactId,
    this.createdAt,
    this.id,
    this.partnerId,
    this.projectId,
    this.statusId,
    this.updatedAt,
  });

  factory ReserveResponseModel.fromJson(Map<String, dynamic> json) =>
      _$ReserveResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$ReserveResponseModelToJson(this);
  static List<ReserveResponseModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => ReserveResponseModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
