import 'package:json_annotation/json_annotation.dart';

part 'marketplace_v2_response_model.g.dart';

@JsonSerializable()
class MarketPlaceV2ResponseModel {
  @JsonKey(name: 'current_page')
  int? currentPage;
  @JsonKey(name: 'first_page_url')
  String? firstPageUrl;
  @JsonKey(name: 'from')
  int? from;
  @JsonKey(name: 'last_page')
  int? lastPage;
  @JsonKey(name: 'last_page_url')
  String? lastPageUrl;
  @JsonKey(name: 'next_page_url')
  dynamic nextPageUrl;
  @JsonKey(name: 'path')
  String? path;
  @JsonKey(name: 'perPage')
  int? perPage;
  @JsonKey(name: 'prev_page_url')
  dynamic prevPageUrl;
  @JsonKey(name: 'to')
  int? to;
  @JsonKey(name: 'total')
  int? total;
  @JsonKey(name: 'items', defaultValue: [])
  List<MarketPlaceV2Items>? items;

  MarketPlaceV2ResponseModel({
    this.currentPage,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.nextPageUrl,
    this.path,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
    this.items,
  });

  factory MarketPlaceV2ResponseModel.fromJson(Map<String, dynamic> json) =>
      _$MarketPlaceV2ResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$MarketPlaceV2ResponseModelToJson(this);
  static List<MarketPlaceV2ResponseModel> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map(
          (i) => MarketPlaceV2ResponseModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class MarketPlaceV2Items {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'ADDRESS')
  MarketPlaceV2Address? address;
  @JsonKey(name: 'JOBS_LIST')
  dynamic jobsList;
  @JsonKey(name: 'MATCHING')
  int? matching;
  @JsonKey(name: 'BUDGET_FOLDER_ID')
  int? budgetFolderId;
  @JsonKey(name: 'COMPLEXITY')
  String? complexity;
  @JsonKey(name: 'CONTRACT')
  dynamic contract;
  @JsonKey(name: 'CONTRACT_ID')
  dynamic contractId;
  @JsonKey(name: 'CONTRACT_STATUS')
  String? contractStatus;
  @JsonKey(name: 'CONTRACT_TYPE')
  String? contractType;
  @JsonKey(name: 'CREATED_FROM')
  String? createdFrom;
  @JsonKey(name: 'DATE_CREATED')
  String? dateCreated;
  @JsonKey(name: 'DATE_UPDATED')
  String? dateUpdated;
  @JsonKey(name: 'DEFICIENCY_FOLDER_ID')
  int? deficiencyFolderId;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'ENTERPRISE_INDUSTRY')
  int? enterpriseIndustry;
  @JsonKey(name: 'ENTERPRISE_INDUSTRY_IDS')
  List<String>? enterpriseIndustryIds;
  @JsonKey(name: 'ENTERPRISE_INDUSTRY_IDS_NEW')
  List<String>? enterpriseIndustryIdsNew;
  @JsonKey(name: 'FOLDER_ID')
  int? folderId;
  @JsonKey(name: 'FOLDER_LINK')
  String? folderLink;
  @JsonKey(name: 'HAS_CHILD')
  bool? hasChild;
  @JsonKey(name: 'HOMEOWNER')
  MarketPlaceV2Homeowner? homeowner;
  @JsonKey(name: 'INDUSTRY_IDS')
  dynamic industryIds;
  @JsonKey(name: 'INDUSTRY_INFO')
  MarketPlaceV2IndustryInfo? industryInfo;
  @JsonKey(name: 'INDUSTRY_INFO_NEW')
  MarketPlaceV2IndustryInfoNew? industryInfoNew;
  @JsonKey(name: 'INFO')
  String? info;
  @JsonKey(name: 'IS_ACTIVE')
  int? isActive;
  @JsonKey(name: 'IS_FINAL')
  bool? isFinal;
  @JsonKey(name: 'JOB_INDUSTRY')
  int? jobIndustry;
  @JsonKey(name: 'LABEL')
  String? label;
  @JsonKey(name: 'MEASUREMENT')
  String? measurement;
  @JsonKey(name: 'MEETING_ID')
  int? meetingId;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'OFFERS')
  dynamic offers;
  @JsonKey(name: 'OFFER_TYPE')
  String? offerType;
  @JsonKey(name: 'PARENT_ID')
  int? parentId;
  @JsonKey(name: 'PARTNER')
  MarketPlaceV2Partner? partner;
  @JsonKey(name: 'PAYMENT_STAGES')
  dynamic paymentStages;
  @JsonKey(name: 'QUESTION_ANSWER_URL')
  String? questionAnswerUrl;
  @JsonKey(name: 'STATUS')
  String? status;
  @JsonKey(name: 'TAGS')
  dynamic tags;
  @JsonKey(name: 'TENDER_FOLDER_ID')
  int? tenderFolderId;
  @JsonKey(name: 'TOTAL_CHILD_OFFERS')
  int? totalChildOffers;
  @JsonKey(name: 'TOTAL_OFFERS')
  int? totalOffers;
  @JsonKey(name: 'TYPE')
  String? type;
  @JsonKey(name: 'PERSONAL_INFORMATION')
  List<Map<dynamic, dynamic>>? personalInformation;
  @JsonKey(name: 'MAXIMUM_OFFER')
  int? maximumOffer;
  @JsonKey(name: 'START_DATE')
  String? startDate;
  @JsonKey(name: 'END_DATE')
  String? endDate;
  @JsonKey(name: 'DEADLINE_OFFER')
  String? deadlineOffer;
  @JsonKey(name: 'DEADLINE_OFFER_COLOR')
  String? deadlineOfferColor;
  @JsonKey(name: 'DEADLINE_OFFER_COLOR_CODE')
  String? deadlineOfferColorCode;

  MarketPlaceV2Items({
    this.id,
    this.address,
    this.jobsList,
    this.matching,
    this.budgetFolderId,
    this.complexity,
    this.contract,
    this.contractId,
    this.contractStatus,
    this.contractType,
    this.createdFrom,
    this.dateCreated,
    this.dateUpdated,
    this.deficiencyFolderId,
    this.description,
    this.enterpriseIndustry,
    this.enterpriseIndustryIds,
    this.enterpriseIndustryIdsNew,
    this.folderId,
    this.folderLink,
    this.hasChild,
    this.homeowner,
    this.industryIds,
    this.industryInfo,
    this.industryInfoNew,
    this.info,
    this.isActive,
    this.isFinal,
    this.jobIndustry,
    this.label,
    this.measurement,
    this.meetingId,
    this.name,
    this.offers,
    this.offerType,
    this.parentId,
    this.partner,
    this.paymentStages,
    this.questionAnswerUrl,
    this.status,
    this.tags,
    this.tenderFolderId,
    this.totalChildOffers,
    this.totalOffers,
    this.type,
    this.personalInformation,
    this.maximumOffer,
    this.endDate,
    this.startDate,
    this.deadlineOffer,
    this.deadlineOfferColor,
    this.deadlineOfferColorCode,
  });

  factory MarketPlaceV2Items.fromJson(Map<String, dynamic> json) =>
      _$MarketPlaceV2ItemsFromJson(json);

  Map<String, dynamic> toJson() => _$MarketPlaceV2ItemsToJson(this);
  static List<MarketPlaceV2Items> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => MarketPlaceV2Items.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class MarketPlaceV2Address {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'ADDRESS')
  String? address;
  @JsonKey(name: 'IMAGE')
  String? image;
  @JsonKey(name: 'LATITUDE')
  String? latitude;
  @JsonKey(name: 'LONGITUDE')
  String? longitude;
  @JsonKey(name: 'POST_NUMBER')
  String? postNumber;
  @JsonKey(name: 'ROAD')
  String? road;
  @JsonKey(name: 'CITY')
  String? city;
  @JsonKey(name: 'ZIP')
  String? zip;

  MarketPlaceV2Address({
    this.id,
    this.address,
    this.image,
    this.latitude,
    this.longitude,
    this.postNumber,
    this.road,
    this.city,
    this.zip,
  });

  factory MarketPlaceV2Address.fromJson(Map<String, dynamic> json) =>
      _$MarketPlaceV2AddressFromJson(json);

  Map<String, dynamic> toJson() => _$MarketPlaceV2AddressToJson(this);
  static List<MarketPlaceV2Address> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => MarketPlaceV2Address.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class MarketPlaceV2Homeowner {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'AVATAR')
  String? avatar;
  @JsonKey(name: 'COMPANY')
  String? company;
  @JsonKey(name: 'EMAIL')
  String? email;
  @JsonKey(name: 'MOBILE')
  String? mobile;
  @JsonKey(name: 'NAME')
  String? name;

  MarketPlaceV2Homeowner({
    this.id,
    this.avatar,
    this.company,
    this.email,
    this.mobile,
    this.name,
  });

  factory MarketPlaceV2Homeowner.fromJson(Map<String, dynamic> json) =>
      _$MarketPlaceV2HomeownerFromJson(json);

  Map<String, dynamic> toJson() => _$MarketPlaceV2HomeownerToJson(this);
  static List<MarketPlaceV2Homeowner> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => MarketPlaceV2Homeowner.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class MarketPlaceV2IndustryInfo {
  @JsonKey(name: 'INDUSTRY_ID')
  int? industryId;
  @JsonKey(name: 'INDUSTRY_NAME')
  String? industryName;
  @JsonKey(name: 'SUBCATEGORY_ID')
  dynamic subcategoryId;
  @JsonKey(name: 'SUBCATEGORY_NAME')
  dynamic subcategoryName;
  @JsonKey(name: 'TASK_TYPE_ID')
  dynamic taskTypeId;
  @JsonKey(name: 'TASK_TYPE_NAME')
  dynamic taskTypeName;

  MarketPlaceV2IndustryInfo({
    this.industryId,
    this.industryName,
    this.subcategoryId,
    this.subcategoryName,
    this.taskTypeId,
    this.taskTypeName,
  });

  factory MarketPlaceV2IndustryInfo.fromJson(Map<String, dynamic> json) =>
      _$MarketPlaceV2IndustryInfoFromJson(json);

  Map<String, dynamic> toJson() => _$MarketPlaceV2IndustryInfoToJson(this);
  static List<MarketPlaceV2IndustryInfo> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => MarketPlaceV2IndustryInfo.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class MarketPlaceV2IndustryInfoNew {
  @JsonKey(name: 'INDUSTRY')
  List<MarketPlaceV2Industry>? industry;
  @JsonKey(name: 'SUBCATEGORY')
  dynamic subcategory;
  @JsonKey(name: 'TASK_TYPE')
  dynamic taskType;

  MarketPlaceV2IndustryInfoNew({
    this.industry,
    this.subcategory,
    this.taskType,
  });

  factory MarketPlaceV2IndustryInfoNew.fromJson(Map<String, dynamic> json) =>
      _$MarketPlaceV2IndustryInfoNewFromJson(json);

  Map<String, dynamic> toJson() => _$MarketPlaceV2IndustryInfoNewToJson(this);
  static List<MarketPlaceV2IndustryInfoNew> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) =>
              MarketPlaceV2IndustryInfoNew.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class MarketPlaceV2Industry {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'NAME_EN')
  String? nameEn;

  MarketPlaceV2Industry({
    this.id,
    this.name,
    this.nameEn,
  });

  factory MarketPlaceV2Industry.fromJson(Map<String, dynamic> json) =>
      _$MarketPlaceV2IndustryFromJson(json);

  Map<String, dynamic> toJson() => _$MarketPlaceV2IndustryToJson(this);
  static List<MarketPlaceV2Industry> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => MarketPlaceV2Industry.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class MarketPlaceV2Partner {
  @JsonKey(name: 'ID')
  dynamic id;
  @JsonKey(name: 'AVATAR')
  String? avatar;
  @JsonKey(name: 'COMPANY')
  dynamic company;
  @JsonKey(name: 'CVR')
  dynamic cvr;
  @JsonKey(name: 'EMAIL')
  dynamic email;
  @JsonKey(name: 'MOBILE')
  dynamic mobile;
  @JsonKey(name: 'NAME')
  dynamic name;

  MarketPlaceV2Partner({
    this.id,
    this.avatar,
    this.company,
    this.cvr,
    this.email,
    this.mobile,
    this.name,
  });

  factory MarketPlaceV2Partner.fromJson(Map<String, dynamic> json) =>
      _$MarketPlaceV2PartnerFromJson(json);

  Map<String, dynamic> toJson() => _$MarketPlaceV2PartnerToJson(this);
  static List<MarketPlaceV2Partner> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => MarketPlaceV2Partner.fromJson(i as Map<String, dynamic>))
          .toList();
}
