import 'package:json_annotation/json_annotation.dart';

part 'mester_to_mester_job_model.g.dart';

@JsonSerializable()
class MesterToMesterJobModel {
  MesterToMesterJobModel({
    this.currentPage,
    this.perPage,
    this.lastPage,
    this.total,
    this.items,
  });

  @JsonKey(name: 'current_page')
  final int? currentPage;
  @JsonKey(name: 'perPage')
  final int? perPage;
  @JsonKey(name: 'last_page')
  final int? lastPage;
  @JsonKey(name: 'total')
  final int? total;
  @JsonKey(name: 'items')
  final List<MesterToMesterJobItem>? items;

  factory MesterToMesterJobModel.fromJson(Map<String, dynamic> json) =>
      _$MesterToMesterJobModelFromJson(json);

  Map<String, dynamic> toJson() => _$MesterToMesterJobModelToJson(this);

  static List<MesterToMesterJobModel> fromCollection(dynamic e) => (e
          as List<dynamic>)
      .map((i) => MesterToMesterJobModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class MesterToMesterJobItem {
  MesterToMesterJobItem({
    this.id,
    this.industryId,
    this.subIndustryId,
    this.type,
    this.title,
    this.description,
    this.industry,
    this.subIndustry,
    this.postalCode,
    this.city,
    this.duration,
    this.startDate,
    this.budget,
    this.numberOfStaff,
    this.numberOfOffers,
    this.status,
    this.deleted,
    this.created,
    this.images,
    this.files,
    this.partners,
  });

  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'INDUSTRY_ID')
  final int? industryId;
  @JsonKey(name: 'SUB_INDUSTRY_ID')
  final int? subIndustryId;
  @JsonKey(name: 'TYPE')
  final String? type;
  @JsonKey(name: 'TITLE')
  final String? title;
  @JsonKey(name: 'DESCRIPTION')
  final String? description;
  @JsonKey(name: 'INDUSTRY')
  final String? industry;
  @JsonKey(name: 'SUB_INDUSTRY')
  final String? subIndustry;
  @JsonKey(name: 'POSTAL_CODE')
  final String? postalCode;
  @JsonKey(name: 'CITY')
  final String? city;
  @JsonKey(name: 'DURATION')
  final String? duration;
  @JsonKey(name: 'START_DATE')
  final String? startDate;
  @JsonKey(name: 'BUDGET')
  final num? budget;
  @JsonKey(name: 'NUMBER_OF_STAFF')
  final int? numberOfStaff;
  @JsonKey(name: 'NUMBER_OF_OFFERS')
  final int? numberOfOffers;
  @JsonKey(name: 'STATUS')
  final String? status;
  @JsonKey(name: 'DELETED')
  final bool? deleted;
  @JsonKey(name: 'CREATED')
  final String? created;
  @JsonKey(name: 'IMAGES')
  final List<String>? images;
  @JsonKey(name: 'FILES')
  final List<String>? files;
  @JsonKey(name: 'PARTNERS')
  final List<MesterJobPartnerModel>? partners;

  factory MesterToMesterJobItem.fromJson(Map<String, dynamic> json) =>
      _$MesterToMesterJobItemFromJson(json);

  Map<String, dynamic> toJson() => _$MesterToMesterJobItemToJson(this);

  static List<MesterToMesterJobItem> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => MesterToMesterJobItem.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class MesterJobPartnerModel {
  MesterJobPartnerModel({
    this.id,
    this.name,
    this.company,
    this.cvr,
    this.avatar,
    this.mobile,
    this.email,
  });

  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'NAME')
  final String? name;
  @JsonKey(name: 'COMPANY')
  final String? company;
  @JsonKey(name: 'CVR')
  final String? cvr;
  @JsonKey(name: 'AVATAR')
  final String? avatar;
  @JsonKey(name: 'MOBILE')
  final String? mobile;
  @JsonKey(name: 'EMAIL')
  final String? email;

  factory MesterJobPartnerModel.fromJson(Map<String, dynamic> json) =>
      _$MesterJobPartnerModelFromJson(json);

  Map<String, dynamic> toJson() => _$MesterJobPartnerModelToJson(this);

  static List<MesterJobPartnerModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => MesterJobPartnerModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
