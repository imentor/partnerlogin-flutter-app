import 'package:json_annotation/json_annotation.dart';

part 'membership_benefits_model.g.dart';

@JsonSerializable()
class MembershipItem {
  @JsonKey(name: 'ID')
  String? id;
  @JsonKey(name: 'IBLOCK_ID')
  String? iblockId;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'CODE')
  String? code;
  @JsonKey(name: 'PROPERTY_IMAGE_URL_VALUE')
  String? propertyImageUrlValue;
  @JsonKey(name: 'PROPERTY_DESCRIPTION_VALUE')
  PROPERTYDESCRIPTIONVALUE? propertyDescriptionValue;
  @JsonKey(name: 'PROPERTY_CODE_VALUE')
  String? propertyCodeValue;
  @JsonKey(name: 'PROPERTY_LINK_VALUE')
  String? propertyLinkValue;
  @JsonKey(name: 'PROPERTY_BUTTON_VALUE')
  String? propertyButtonValue;
  @JsonKey(name: 'CNT')
  String? cnt;
  @JsonKey(name: 'PROPERTY_CONTACT_NAME_VALUE')
  String? propertyContactNameValue;
  @JsonKey(name: 'PROPERTY_CONTACT_EMAIL_VALUE')
  String? propertyContactEmailValue;
  @JsonKey(name: 'PROPERTY_CONTACT_PHONE_VALUE')
  String? propertyContactPhoneValue;
  @JsonKey(name: 'PROPERTY_CONTACT_PHOTO_VALUE')
  String? propertyContactPhotoValue;
  @JsonKey(name: 'PROPERTY_CONTACT_MEMBERS_CODE_VALUE')
  String? propertyContactMembersCodeValue;
  @JsonKey(name: 'PROPERTY_CONTACT_FROM_ZIP_VALUE')
  String? propertyContactFromZipValue;
  @JsonKey(name: 'PROPERTY_CONTACT_TO_ZIP_VALUE')
  String? propertyContactToZipValue;

  MembershipItem(
      {this.name,
      this.id,
      this.iblockId,
      this.propertyImageUrlValue,
      this.propertyDescriptionValue,
      this.propertyCodeValue,
      this.propertyLinkValue,
      this.propertyButtonValue,
      this.propertyContactEmailValue,
      this.propertyContactFromZipValue,
      this.propertyContactToZipValue,
      this.cnt,
      this.propertyContactMembersCodeValue,
      this.propertyContactNameValue,
      this.propertyContactPhoneValue,
      this.propertyContactPhotoValue,
      this.code});

  factory MembershipItem.fromJson(Map<String, dynamic> json) =>
      _$MembershipItemFromJson(json);

  Map<String, dynamic> toJson() => _$MembershipItemToJson(this);

  static List<MembershipItem> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => MembershipItem.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class PROPERTYDESCRIPTIONVALUE {
  @JsonKey(name: 'TEXT')
  String? text;
  @JsonKey(name: 'TYPE')
  String? type;

  PROPERTYDESCRIPTIONVALUE({this.text, this.type});

  factory PROPERTYDESCRIPTIONVALUE.fromJson(Map<String, dynamic> json) =>
      _$PROPERTYDESCRIPTIONVALUEFromJson(json);

  Map<String, dynamic> toJson() => _$PROPERTYDESCRIPTIONVALUEToJson(this);
}
