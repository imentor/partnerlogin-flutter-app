import 'package:json_annotation/json_annotation.dart';

part 'generic_response_model.g.dart';

@JsonSerializable()
class GenericResponseModel {
  const GenericResponseModel({
    this.success,
    this.message,
  });

  final bool? success;
  final String? message;

  factory GenericResponseModel.fromJson(Map<String, dynamic> json) =>
      _$GenericResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$GenericResponseModelToJson(this);

  @override
  String toString() =>
      'GenericResponseModel(success: $success, message: $message)';
}

class GenericUrlResponseModel {
  GenericUrlResponseModel({
    this.success,
    this.url,
  });

  final bool? success;
  final String? url;

  factory GenericUrlResponseModel.fromJson(Map<String, dynamic> json) {
    return GenericUrlResponseModel(
      success: json['success'] as bool?,
      url: json['url'] as String?,
    );
  }

  @override
  String toString() {
    return 'success: $success, message: $url';
  }
}

class GenericMessageUrlResponseModel {
  GenericMessageUrlResponseModel({
    this.success,
    this.url,
    this.message,
  });

  final String? message;
  final bool? success;
  final String? url;

  factory GenericMessageUrlResponseModel.fromJson(Map<String, dynamic> json) {
    return GenericMessageUrlResponseModel(
      success: json['success'] as bool?,
      url: json['url'] as String?,
      message: json['message'] as String?,
    );
  }

  @override
  String toString() {
    return 'success: $success, url: $url, message: $message';
  }
}

class GenericResultResponseModel {
  GenericResultResponseModel({
    this.success,
    this.result,
  });

  final bool? success;
  final dynamic result;

  factory GenericResultResponseModel.fromJson(Map<String, dynamic> json) {
    return GenericResultResponseModel(
      success: json['success'] as bool?,
      result: json['result'],
    );
  }

  @override
  String toString() {
    return 'success: $success, result: $result';
  }
}

class GenericEnddateResponseModel {
  GenericEnddateResponseModel({
    this.success,
    this.enddate,
    this.message,
  });

  final String? message;
  final bool? success;
  final String? enddate;

  factory GenericEnddateResponseModel.fromJson(Map<String, dynamic> json) {
    return GenericEnddateResponseModel(
      success: json['success'] as bool?,
      enddate: json['enddate'] as String?,
      message: json['message'] as String?,
    );
  }

  @override
  String toString() {
    return 'success: $success, enddate: $enddate, message: $message';
  }
}

class GenericSuccessResponseModel {
  GenericSuccessResponseModel({
    this.success,
  });

  final bool? success;

  factory GenericSuccessResponseModel.fromJson(Map<String, dynamic> json) {
    return GenericSuccessResponseModel(
      success: json['success'] as bool?,
    );
  }

  @override
  String toString() {
    return 'success: $success';
  }
}

class TokenRegisterCheckResponseModel {
  TokenRegisterCheckResponseModel({
    this.register,
  });

  final bool? register;

  factory TokenRegisterCheckResponseModel.fromJson(Map<String, dynamic> json) {
    return TokenRegisterCheckResponseModel(
      register: json['register'] as bool?,
    );
  }

  @override
  String toString() {
    return 'register: $register';
  }
}

class MessageReadResponseModel {
  MessageReadResponseModel({
    this.success,
    this.inbox,
    this.message,
  });

  final int? inbox;
  final bool? success;
  final String? message;

  factory MessageReadResponseModel.fromJson(Map<String, dynamic> json) {
    return MessageReadResponseModel(
      inbox: json['inbox'] as int?,
      success: json['success'] as bool?,
      message: json['message'] as String?,
    );
  }

  @override
  String toString() {
    return 'inbox: $inbox, message: $message, success: $success';
  }
}

class AddUserPartnerResponseModel {
  AddUserPartnerResponseModel({
    this.success,
    this.appid,
  });

  final String? appid;
  final bool? success;

  factory AddUserPartnerResponseModel.fromJson(Map<String, dynamic> json) {
    return AddUserPartnerResponseModel(
      appid: json['appid'] as String?,
      success: json['success'] as bool?,
    );
  }

  @override
  String toString() {
    return 'appid: $appid, success: $success';
  }
}

class ImgUrlResponseModel {
  ImgUrlResponseModel({
    this.success,
    this.url,
    this.img,
  });

  final bool? success;
  final String? url;
  final String? img;

  factory ImgUrlResponseModel.fromJson(Map<String, dynamic> json) {
    return ImgUrlResponseModel(
      success: json['success'] as bool?,
      url: json['url'] as String?,
      img: json['img'] as String?,
    );
  }

  @override
  String toString() {
    return 'success: $success, message: $url';
  }
}

@JsonSerializable()
class GenericAdResponseModel {
  GenericAdResponseModel({
    this.success,
    this.result,
  });

  final bool? success;
  @JsonKey(name: 'Result')
  final AdResult? result;

  factory GenericAdResponseModel.fromJson(Map<String, dynamic> json) =>
      _$GenericAdResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$GenericAdResponseModelToJson(this);
}

@JsonSerializable()
class AdResult {
  AdResult({
    this.adGroupId,
    this.adId,
  });

  final dynamic adGroupId;
  final dynamic adId;

  factory AdResult.fromJson(Map<String, dynamic> json) =>
      _$AdResultFromJson(json);
  Map<String, dynamic> toJson() => _$AdResultToJson(this);
}

class PdfImageUrlResponseModel {
  PdfImageUrlResponseModel({this.success, this.url, this.img, this.pdf});

  final bool? success;
  final String? url;
  final String? img;
  final String? pdf;

  factory PdfImageUrlResponseModel.fromJson(Map<String, dynamic> json) {
    return PdfImageUrlResponseModel(
      success: json['success'] as bool?,
      url: json['url'] as String?,
      img: json['img'] as String?,
      pdf: json['pdf'] as String?,
    );
  }

  @override
  String toString() {
    return 'success: $success, message: $url , pdf: $pdf';
  }
}

class KeyResponseModel {
  KeyResponseModel({this.key});

  final String? key;

  factory KeyResponseModel.fromJson(Map<String, dynamic> json) {
    return KeyResponseModel(
      key: json['key'] as String?,
    );
  }

  @override
  String toString() {
    return 'key: $key';
  }
}

class AvailableResponseModel {
  AvailableResponseModel({this.available});

  final bool? available;

  factory AvailableResponseModel.fromJson(Map<String, dynamic> json) {
    return AvailableResponseModel(
      available: json['available'] as bool?,
    );
  }

  @override
  String toString() {
    return 'key: $available';
  }
}

class ChangeCreditResponseModel {
  ChangeCreditResponseModel({this.changecredit});

  final bool? changecredit;

  factory ChangeCreditResponseModel.fromJson(Map<String, dynamic> json) {
    return ChangeCreditResponseModel(
      changecredit: json['changecredit'] as bool?,
    );
  }

  @override
  String toString() {
    return 'key: $changecredit';
  }
}

class SaveTasksResponseModel {
  bool? success;
  String? message;
  SaveTasksData? data;

  SaveTasksResponseModel({this.success, this.message, this.data});

  SaveTasksResponseModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    data = json['data'] != null ? SaveTasksData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['success'] = success;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class SaveTasksData {
  int? cPartnerTaskProfile;
  int? cPartnerFirstLogin;
  bool? cPartnerLavSort;
  bool? cPartnerLavCronProduct;

  SaveTasksData(
      {this.cPartnerTaskProfile,
      this.cPartnerFirstLogin,
      this.cPartnerLavSort,
      this.cPartnerLavCronProduct});

  SaveTasksData.fromJson(Map<String, dynamic> json) {
    cPartnerTaskProfile = json['c_partner_task_profile'];
    cPartnerFirstLogin = json['c_partner_first_login'];
    cPartnerLavSort = json['c_partner_lav_sort'];
    cPartnerLavCronProduct = json['c_partner_lav_cron_product'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['c_partner_task_profile'] = cPartnerTaskProfile;
    data['c_partner_first_login'] = cPartnerFirstLogin;
    data['c_partner_lav_sort'] = cPartnerLavSort;
    data['c_partner_lav_cron_product'] = cPartnerLavCronProduct;
    return data;
  }
}

class MinboligApiResponse {
  bool? success;
  String? message;
  dynamic data;

  MinboligApiResponse({this.success, this.message, this.data});

  MinboligApiResponse.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    data = json['data'];
  }
}
