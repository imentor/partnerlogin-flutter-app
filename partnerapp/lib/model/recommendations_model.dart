import 'package:json_annotation/json_annotation.dart';

part 'recommendations_model.g.dart';

@JsonSerializable()
class RecommendationsModel {
  @JsonKey(name: 'ReviewId')
  int? reviewId;
  @JsonKey(name: 'AssignmentId')
  dynamic assignmentId;
  @JsonKey(name: 'AssignmentTitle')
  String? assignmentTitle;
  @JsonKey(name: 'AssignmentDescription')
  String? assignmentDescription;
  @JsonKey(name: 'AssignmentIsCompleted')
  bool? assignmentIsCompleted;
  @JsonKey(name: 'AssignmentDistributedCount')
  int? assignmentDistributedCount;
  @JsonKey(name: 'Rating')
  dynamic rating;
  @JsonKey(name: 'ContactRating')
  dynamic contactRating;
  @JsonKey(name: 'AgreementRating')
  dynamic agreementRating;
  @JsonKey(name: 'PriceRating')
  dynamic priceRating;
  @JsonKey(name: 'CleaningRating')
  dynamic cleaningRating;
  @JsonKey(name: 'WorkRating')
  dynamic workRating;
  @JsonKey(name: 'RecommendCompanyRating')
  dynamic recommendCompanyRating;
  @JsonKey(name: 'RatingComment')
  String? ratingComment;
  @JsonKey(name: 'SupplierRatingComment')
  String? supplierRatingComment;
  @JsonKey(name: 'RatedBy')
  String? ratedBy;
  @JsonKey(name: 'ProfilePicPath')
  String? profilePicPath;
  @JsonKey(name: 'RequesterId')
  int? requesterId;
  @JsonKey(name: 'RatedDate')
  String? ratedDate;
  @JsonKey(name: 'CommentEditDate')
  dynamic commentEditDate;
  @JsonKey(name: 'SupplierCommentDate')
  String? supplierCommentDate;
  @JsonKey(name: 'SupplierCommentEditDate')
  dynamic supplierCommentEditDate;
  @JsonKey(name: 'RatedFrom')
  String? ratedFrom;
  @JsonKey(name: 'ProjectId')
  dynamic projectId;
  @JsonKey(name: 'ProjectImageUrl')
  dynamic projectImageUrl;
  @JsonKey(name: 'ProjectTitle')
  dynamic projectTitle;
  @JsonKey(name: 'SupplirId')
  int? supplirId;
  @JsonKey(name: 'Firma')
  String? firma;
  @JsonKey(name: 'ManufactureList')
  String? manufactureList;
  @JsonKey(name: 'ValidationStatus')
  String? validationStatus;
  @JsonKey(name: 'ValidatedDate')
  String? validationDate;
  @JsonKey(name: 'Industry')
  dynamic industry;
  @JsonKey(name: 'TaskId')
  dynamic taskId;
  @JsonKey(name: 'Task')
  String? task;

  RecommendationsModel(
      {this.reviewId,
      this.assignmentId,
      this.assignmentTitle,
      this.assignmentDescription,
      this.assignmentIsCompleted,
      this.assignmentDistributedCount,
      this.rating,
      this.contactRating,
      this.agreementRating,
      this.priceRating,
      this.cleaningRating,
      this.workRating,
      this.recommendCompanyRating,
      this.ratingComment,
      this.supplierRatingComment,
      this.ratedBy,
      this.profilePicPath,
      this.requesterId,
      this.ratedDate,
      this.commentEditDate,
      this.supplierCommentDate,
      this.supplierCommentEditDate,
      this.ratedFrom,
      this.projectId,
      this.projectImageUrl,
      this.projectTitle,
      this.supplirId,
      this.firma,
      this.manufactureList,
      this.validationDate,
      this.validationStatus,
      this.task,
      this.taskId,
      this.industry});

  factory RecommendationsModel.fromJson(Map<String, dynamic> json) =>
      _$RecommendationsModelFromJson(json);

  Map<String, dynamic> toJson() => _$RecommendationsModelToJson(this);
}
