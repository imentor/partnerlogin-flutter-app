import 'package:json_annotation/json_annotation.dart';

part 'offer_versions_list_model.g.dart';

@JsonSerializable()
class OfferVersionListModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'REFERENCE_NUMBER')
  int? referenceNumber;
  @JsonKey(name: 'TITLE')
  String? title;
  @JsonKey(name: 'DESCRIPTION')
  String? description;
  @JsonKey(name: 'IMAGE_URL')
  String? imageUrl;
  @JsonKey(name: 'STATUS')
  String? status;
  @JsonKey(name: 'PREVIOUS_STATUS')
  String? previousStatus;
  @JsonKey(name: 'CONTACT_ID')
  int? contactId;
  @JsonKey(name: 'PROJECT_ID')
  int? projectId;
  @JsonKey(name: 'PARENT_PROJECT_ID')
  int? parentProjectId;
  @JsonKey(name: 'CONTRACT_ID')
  int? contractId;
  @JsonKey(name: 'CONTRACT_TYPE')
  String? contractType;
  @JsonKey(name: 'OFFER_TYPE')
  String? offerType;
  @JsonKey(name: 'OFFER_SEEN')
  bool? offerSeen;
  @JsonKey(name: 'LEAD_ID')
  int? leadId;
  @JsonKey(name: 'PROJECT_ADDRESS')
  String? projectAddress;
  @JsonKey(name: 'SUB_TOTAL_PRICE')
  dynamic subTotalPrice;
  @JsonKey(name: 'REJECTED_OFFER_PRICE')
  dynamic rejectedOfferPrice;
  @JsonKey(name: 'REJECTED_OFFER_PRICE_VAT')
  dynamic rejectedOfferPriceVat;
  @JsonKey(name: 'REJECTED_OFFER_PRICE_VAT_FEE')
  dynamic rejectedOfferPriceVatFee;
  @JsonKey(name: 'VAT')
  dynamic vat;
  @JsonKey(name: 'TOTAL_PRICE')
  dynamic totalPrice;
  @JsonKey(name: 'TOTAL_VAT')
  dynamic totalVat;
  @JsonKey(name: 'TOTAL_VAT_FEE')
  dynamic totalVatFee;
  @JsonKey(name: 'TOTAL_AMOUNT_PAID')
  dynamic totalAmountPaid;
  @JsonKey(name: 'ADDED_TO_BUDGET')
  bool? addedToBudget;
  @JsonKey(name: 'COMPANY_NAME')
  String? companyName;
  @JsonKey(name: 'COMPANY_CVR')
  String? companyCvr;
  @JsonKey(name: 'COMPANY_EMAIL')
  String? companyEmail;
  @JsonKey(name: 'IS_PARTNER')
  bool? isPartner;
  @JsonKey(name: 'PARTNER_PROFILE')
  OfferListPartnerProfileModel? partnerProfile;
  @JsonKey(name: 'HOMEOWNER')
  HomeownerModel? homeowner;
  @JsonKey(name: 'START_DATE')
  String? startDate;
  @JsonKey(name: 'END_DATE')
  String? endDate;
  @JsonKey(name: 'EXPIRY')
  int? expiry;
  @JsonKey(name: 'EXPIRY_DATE')
  String? expiryDate;
  @JsonKey(name: 'OFFER_FILE')
  String? offerFile;
  @JsonKey(name: 'OFFER_FILES')
  List<dynamic>? offerFiles;
  // @JsonKey(name: 'TASKS')
  // List<TasksModel>? tasks;
  @JsonKey(name: 'JOB_INDUSTRY_ID')
  int? jobIndustryId;
  @JsonKey(name: 'JOB_INDUSTRY_NAME')
  String? jobIndustryName;
  @JsonKey(name: 'JOB_INDUSTRY_NAME_EN')
  String? jobIndustryNameEn;
  @JsonKey(name: 'SERIES')
  dynamic series;
  @JsonKey(name: 'SERIES_ORIGINAL')
  dynamic seriesOriginal;
  @JsonKey(name: 'JOBS_LIST')
  List<dynamic>? jobsList;
  @JsonKey(name: 'ENTERPRISE_INDUSTRY_ID')
  int? enterpriseIndustryId;
  @JsonKey(name: 'INDUSTRY_IDS')
  List<int>? industryIds;
  @JsonKey(name: 'ENTERPRISE_INDUSTRY_IDS')
  List<String>? enterpriseIndustryIds;
  @JsonKey(name: 'ACCEPTED_ENTERPRISE_INDUSTRY_IDS')
  List<String>? acceptedEnterpriseIndustryIds;
  @JsonKey(name: 'DISTANCE_IN_KM')
  dynamic distanceInKm;
  @JsonKey(name: 'ACCEPTED_VERSION_ID')
  int? acceptedVersionId;
  @JsonKey(name: 'REJECTED_VERSION_ID')
  int? rejectedVersionId;
  @JsonKey(name: 'OFFER_VERSIONS')
  List<OfferVersionsModel>? offerVersions;
  @JsonKey(name: 'CREATED_BY')
  String? createdBy;
  @JsonKey(name: 'CREATED_AT')
  String? createdAt;
  @JsonKey(name: 'UPDATED_AT')
  String? updatedAt;

  OfferVersionListModel({
    this.id,
    this.referenceNumber,
    this.title,
    this.description,
    this.imageUrl,
    this.status,
    this.previousStatus,
    this.contactId,
    this.projectId,
    this.parentProjectId,
    this.contractId,
    this.contractType,
    this.offerType,
    this.offerSeen,
    this.leadId,
    this.projectAddress,
    this.subTotalPrice,
    this.rejectedOfferPrice,
    this.rejectedOfferPriceVat,
    this.rejectedOfferPriceVatFee,
    this.vat,
    this.totalPrice,
    this.totalVat,
    this.totalVatFee,
    this.totalAmountPaid,
    this.addedToBudget,
    this.companyName,
    this.companyCvr,
    this.companyEmail,
    this.isPartner,
    this.partnerProfile,
    this.homeowner,
    this.startDate,
    this.endDate,
    this.expiry,
    this.expiryDate,
    this.offerFile,
    this.offerFiles,
    // this.tasks,
    this.jobIndustryId,
    this.jobIndustryName,
    this.jobIndustryNameEn,
    this.series,
    this.seriesOriginal,
    this.jobsList,
    this.enterpriseIndustryId,
    this.industryIds,
    this.enterpriseIndustryIds,
    this.acceptedEnterpriseIndustryIds,
    this.distanceInKm,
    this.acceptedVersionId,
    this.rejectedVersionId,
    this.offerVersions,
    this.createdBy,
    this.createdAt,
    this.updatedAt,
  });

  factory OfferVersionListModel.fromJson(Map<String, dynamic> json) =>
      _$OfferVersionListModelFromJson(json);

  Map<String, dynamic> toJson() => _$OfferVersionListModelToJson(this);
  static List<OfferVersionListModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => OfferVersionListModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class OfferListPartnerProfileModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'TITLE')
  String? title;
  @JsonKey(name: 'ZIP_CODE')
  String? zipCode;
  @JsonKey(name: 'INSURANCE')
  bool? insurance;
  @JsonKey(name: 'ADDRESS')
  String? address;
  @JsonKey(name: 'EMPLOYEES_COUNT')
  int? employeesCount;
  @JsonKey(name: 'ESTABLISHED_DATE')
  String? establishedDate;
  @JsonKey(name: 'REVIEW_COUNT')
  int? reviewCount;
  @JsonKey(name: 'RATING_AVG')
  dynamic ratingAvg;
  @JsonKey(name: 'AVATAR')
  String? avatar;
  @JsonKey(name: 'FAKE_EMAIL')
  String? fakeEmail;
  @JsonKey(name: 'FAKE_PHONE')
  String? fakePhone;

  OfferListPartnerProfileModel({
    this.id,
    this.title,
    this.zipCode,
    this.insurance,
    this.address,
    this.employeesCount,
    this.establishedDate,
    this.reviewCount,
    this.ratingAvg,
    this.avatar,
    this.fakeEmail,
    this.fakePhone,
  });

  factory OfferListPartnerProfileModel.fromJson(Map<String, dynamic> json) =>
      _$OfferListPartnerProfileModelFromJson(json);

  Map<String, dynamic> toJson() => _$OfferListPartnerProfileModelToJson(this);
  static List<OfferListPartnerProfileModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) =>
              OfferListPartnerProfileModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class HomeownerModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'COMPANY')
  String? company;
  @JsonKey(name: 'AVATAR')
  String? avatar;
  @JsonKey(name: 'MOBILE')
  String? mobile;
  @JsonKey(name: 'EMAIL')
  String? email;
  @JsonKey(name: 'FAKE_EMAIL')
  String? fakeEmail;
  @JsonKey(name: 'FAKE_PHONE')
  String? fakePhone;

  HomeownerModel({
    this.id,
    this.name,
    this.company,
    this.avatar,
    this.mobile,
    this.email,
    this.fakeEmail,
    this.fakePhone,
  });

  factory HomeownerModel.fromJson(Map<String, dynamic> json) =>
      _$HomeownerModelFromJson(json);

  Map<String, dynamic> toJson() => _$HomeownerModelToJson(this);
  static List<HomeownerModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => HomeownerModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class TasksModel {
  @JsonKey(name: 'title')
  String? title;
  @JsonKey(name: 'description')
  String? description;
  @JsonKey(name: 'unit')
  String? unit;
  @JsonKey(name: 'value')
  String? value;
  @JsonKey(name: 'quantity')
  String? quantity;

  TasksModel({
    this.title,
    this.description,
    this.unit,
    this.value,
    this.quantity,
  });

  factory TasksModel.fromJson(Map<String, dynamic> json) =>
      _$TasksModelFromJson(json);

  Map<String, dynamic> toJson() => _$TasksModelToJson(this);
  static List<TasksModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => TasksModel.fromJson(i as Map<String, dynamic>))
      .toList();
}

@JsonSerializable()
class OfferVersionsModel {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'UF_OFFER_ID')
  int? ufOfferId;
  @JsonKey(name: 'UF_TOTAL_PRICE')
  String? ufTotalPrice;
  @JsonKey(name: 'UF_SUB_TOTAL_PRICE')
  String? ufSubTotalPrice;
  @JsonKey(name: 'UF_VAT')
  String? ufVat;
  @JsonKey(name: 'UF_CREATED_FROM')
  String? ufCreatedFrom;
  @JsonKey(name: 'UF_STATUS')
  String? ufStatus;
  @JsonKey(name: 'UF_CREATED_BY')
  String? ufCreatedBy;
  @JsonKey(name: 'UF_CREATED_AT')
  String? ufCreatedAt;
  @JsonKey(name: 'UF_FEE')
  String? ufFee;
  @JsonKey(name: 'UF_TOTAL_VAT')
  String? ufTotalVat;
  @JsonKey(name: 'UF_TOTAL_VAT_FEE')
  String? ufTotalVatFee;
  @JsonKey(name: 'UF_UPDATED_AT')
  String? ufUpdatedAt;
  @JsonKey(name: 'UF_VERSION')
  int? ufVersion;
  @JsonKey(name: 'UF_SEEN')
  int? ufSeen;
  @JsonKey(name: 'UF_DECLINE_REASON')
  String? ufDeclineReason;
  @JsonKey(name: 'UF_REJECTED_OFFER_PRICE')
  String? ufRejectedOfferPrice;
  @JsonKey(name: 'UF_DECLINED_FROM')
  String? ufDeclinedFrom;
  @JsonKey(name: 'UF_FILE')
  dynamic ufFile;
  @JsonKey(name: 'UF_TYPE')
  String? ufType;
  @JsonKey(name: 'OFFER_FILE')
  String? offerFile;
  @JsonKey(name: 'CONTRACT_ID')
  int? contractId;

  OfferVersionsModel({
    this.id,
    this.ufOfferId,
    this.ufTotalPrice,
    this.ufSubTotalPrice,
    this.ufVat,
    this.ufCreatedFrom,
    this.ufStatus,
    this.ufCreatedBy,
    this.ufCreatedAt,
    this.ufFee,
    this.ufTotalVat,
    this.ufTotalVatFee,
    this.ufUpdatedAt,
    this.ufVersion,
    this.ufSeen,
    this.ufDeclineReason,
    this.ufRejectedOfferPrice,
    this.ufDeclinedFrom,
    this.ufFile,
    this.ufType,
    this.contractId,
  });

  factory OfferVersionsModel.fromJson(Map<String, dynamic> json) =>
      _$OfferVersionsModelFromJson(json);

  Map<String, dynamic> toJson() => _$OfferVersionsModelToJson(this);
  static List<OfferVersionsModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => OfferVersionsModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
