import 'package:json_annotation/json_annotation.dart';

part 'contact_model.g.dart';

@JsonSerializable()
class Contact {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'COMPANY')
  String? company;
  @JsonKey(name: 'AVATAR')
  String? avatar;
  @JsonKey(name: 'MOBILE')
  String? mobile;
  @JsonKey(name: 'EMAIL')
  String? email;

  Contact(
      {this.avatar, this.company, this.email, this.id, this.mobile, this.name});

  factory Contact.fromJson(Map<String, dynamic> json) =>
      _$ContactFromJson(json);

  Map<String, dynamic> toJson() => _$ContactToJson(this);

  static List<Contact> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => Contact.fromJson(i as Map<String, dynamic>))
      .toList();
}
