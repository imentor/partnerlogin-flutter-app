import 'package:json_annotation/json_annotation.dart';

part 'klima_get_response_model.g.dart';

class KlimaGetResponseModel {
  bool? success;
  String? message;
  Data? data;

  KlimaGetResponseModel({this.success, this.message, this.data});

  KlimaGetResponseModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['success'] = success;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

@JsonSerializable()
class Data {
  List<Main?>? main;
  List<Main?>? draft;

  Data({this.main, this.draft});

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  Map<String, dynamic> toJson() => _$DataToJson(this);
}

@JsonSerializable()
class UserGroup {
  String? userplanId;
  String? title;
  dynamic file;
  List<DataList?>? fileList;
  dynamic image;
  List<DataList?>? imageList;
  @JsonKey(name: 'user_type')
  List<UserType?>? userType;
  String? info;
  @JsonKey(name: 'user_survey')
  dynamic usersurvey;

  UserGroup(
      {this.userplanId,
      this.title,
      this.file,
      this.fileList,
      this.image,
      this.imageList,
      this.userType,
      this.info,
      this.usersurvey});

  factory UserGroup.fromJson(Map<String, dynamic> json) =>
      _$UserGroupFromJson(json);

  Map<String, dynamic> toJson() => _$UserGroupToJson(this);
}

class DataList {
  String? id;
  String? title;
  String? url;
  String? description;

  DataList({this.id, this.title, this.url});

  DataList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    url = json['url'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['url'] = url;
    return data;
  }
}

class UserType {
  String? usertypeId;
  String? title;
  String? typeid;
  String? contactId;
  String? before;
  String? after;
  dynamic info;

  UserType(
      {this.usertypeId,
      this.title,
      this.typeid,
      this.contactId,
      this.before,
      this.after,
      this.info});

  UserType.fromJson(Map<String, dynamic> json) {
    usertypeId = json['usertypeId'];
    title = json['title'];
    typeid = json['typeid'];
    contactId = json['ContactId'];
    before = json['before'];
    after = json['after'];
    info = json['info'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['usertypeId'] = usertypeId;
    data['title'] = title;
    data['typeid'] = typeid;
    data['ContactId'] = contactId;
    data['before'] = before;
    data['after'] = after;
    data['info'] = info;
    return data;
  }
}

@JsonSerializable()
class Main {
  String? klimaId;
  @JsonKey(name: 'created_date')
  String? createdDate;
  String? company;
  String? contact;
  String? offer;
  dynamic info;
  @JsonKey(name: 'user_group')
  List<UserGroup?>? userGroup;
  @JsonKey(name: 'user_survey')
  dynamic usersurvey;
  @JsonKey(name: 'report_sent')
  dynamic reportSent;
  @JsonKey(name: 'report_sent_date')
  dynamic reportSentDate;

  Main(
      {this.klimaId,
      this.createdDate,
      this.company,
      this.contact,
      this.offer,
      this.info,
      this.userGroup,
      this.usersurvey,
      this.reportSent,
      this.reportSentDate});

  factory Main.fromJson(Map<String, dynamic> json) => _$MainFromJson(json);

  Map<String, dynamic> toJson() => _$MainToJson(this);
}
