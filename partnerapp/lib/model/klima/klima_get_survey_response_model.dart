import 'package:json_annotation/json_annotation.dart';

part 'klima_get_survey_response_model.g.dart';

@JsonSerializable()
class KlimaGetSurveyResponseModel {
  bool? success;
  String? message;

  Data? data;

  KlimaGetSurveyResponseModel({this.success, this.message, this.data});

  factory KlimaGetSurveyResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] is List) {
      json['data'] = null;
    }

    return _$KlimaGetSurveyResponseModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$KlimaGetSurveyResponseModelToJson(this);
}

@JsonSerializable()
class Data {
  List<Main?>? main;

  List<dynamic>? draft;

  Data({this.main, this.draft});

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  Map<String, dynamic> toJson() => _$DataToJson(this);
}

@JsonSerializable()
class Main {
  dynamic klimasurveryId;
  @JsonKey(name: 'created_date')
  String? createdDate;
  String? company;
  String? contact;
  String? info;
  List<Economy?>? economy;
  List<Social?>? social;
  List<Environment?>? environment;
  List<Fag?>? fag;

  Main(
      {this.klimasurveryId,
      this.createdDate,
      this.company,
      this.contact,
      this.info,
      this.economy,
      this.social,
      this.environment,
      this.fag});

  factory Main.fromJson(Map<String, dynamic> json) => _$MainFromJson(json);

  Map<String, dynamic> toJson() => _$MainToJson(this);
}

class Economy {
  dynamic usereconomyId;
  String? title;
  String? economicId;
  dynamic value;
  dynamic onlyOverall;

  Economy(
      {this.usereconomyId,
      this.title,
      this.economicId,
      this.value,
      this.onlyOverall});

  Economy.fromJson(Map<String, dynamic> json) {
    usereconomyId = json['usereconomyId'];
    title = json['title'];
    economicId = json['economicId'];
    value = json['value'];
    onlyOverall = json['onlyOverall'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['usereconomyId'] = usereconomyId;
    data['title'] = title;
    data['economicId'] = economicId;
    data['value'] = value;
    data['onlyOverall'] = onlyOverall;
    return data;
  }
}

class Social {
  dynamic usersocialId;
  String? title;
  String? socialId;
  dynamic value;
  dynamic onlyOverall;

  Social(
      {this.usersocialId,
      this.title,
      this.socialId,
      this.value,
      this.onlyOverall});

  Social.fromJson(Map<String, dynamic> json) {
    usersocialId = json['usersocialId'];
    title = json['title'];
    socialId = json['socialId'];
    value = json['value'];
    onlyOverall = json['onlyOverall'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['usersocialId'] = usersocialId;
    data['title'] = title;
    data['socialId'] = socialId;
    data['value'] = value;
    data['onlyOverall'] = onlyOverall;

    return data;
  }
}

class Environment {
  dynamic userenvironmentId;
  String? title;
  String? environmentId;
  dynamic value;
  dynamic onlyOverall;

  Environment(
      {this.userenvironmentId,
      this.title,
      this.environmentId,
      this.value,
      this.onlyOverall});

  Environment.fromJson(Map<String, dynamic> json) {
    userenvironmentId = json['userenvironmentId'];
    title = json['title'];
    environmentId = json['environmentId'];
    value = json['value'];
    onlyOverall = json['onlyOverall'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['userenvironmentId'] = userenvironmentId;
    data['title'] = title;
    data['environmentId'] = environmentId;
    data['value'] = value;
    data['onlyOverall'] = onlyOverall;
    return data;
  }
}

class Fag {
  dynamic userfagId;
  String? title;
  String? fagId;
  dynamic value;
  dynamic onlyOverall;

  Fag({this.userfagId, this.title, this.fagId, this.value, this.onlyOverall});

  Fag.fromJson(Map<String, dynamic> json) {
    userfagId = json['userfagId'];
    title = json['title'];
    fagId = json['fagId'];
    value = json['value'];
    onlyOverall = json['onlyOverall'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['userfagId'] = userfagId;
    data['title'] = title;
    data['fagId'] = fagId;
    data['value'] = value;
    data['onlyOverall'] = onlyOverall;
    return data;
  }
}
