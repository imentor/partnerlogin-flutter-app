import 'package:json_annotation/json_annotation.dart';

part 'klima_type_response_model.g.dart';

@JsonSerializable()
class Data {
  @JsonKey(name: 'ID')
  String? id;
  @JsonKey(name: 'IBLOCK_ID')
  String? iBlockId;
  @JsonKey(name: 'NAME')
  String? name;
  @JsonKey(name: 'PROPERTY_LOGO_VALUE')
  String? propertyLogoValue;
  @JsonKey(name: 'PROPERTY_SHORT_VALUE')
  String? propertyShortValue;
  @JsonKey(name: 'PROPERTY_TYPE_VALUE')
  String? propertyTypeValue;
  @JsonKey(name: 'PROPERTY_TYPE_ENUM_ID')
  String? propertyTypeEnumId;
  @JsonKey(name: 'PROPERTY_BEFORE_VALUE')
  String? propertyBeforeValue;
  @JsonKey(name: 'PROPERTY_AFTER_VALUE')
  String? propertyAfterValue;
  @JsonKey(name: 'CNT')
  String? cnt;

  Data(
      {this.id,
      this.iBlockId,
      this.name,
      this.propertyAfterValue,
      this.propertyBeforeValue,
      this.propertyLogoValue,
      this.propertyShortValue,
      this.propertyTypeEnumId,
      this.propertyTypeValue,
      this.cnt});

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  Map<String, dynamic> toJson() => _$DataToJson(this);
}
