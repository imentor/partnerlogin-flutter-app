import 'package:json_annotation/json_annotation.dart';

part 'dynamic_story_model.g.dart';

@JsonSerializable()
class DynamicStoryModel {
  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'POPUP_LOG_ID')
  final int? popupLogId;
  @JsonKey(name: 'PARTNER_ID')
  final int? partnerId;
  @JsonKey(name: 'PARENT_ID')
  final int? parentId;
  @JsonKey(name: 'TITLE')
  final String? title;
  @JsonKey(name: 'PAGE')
  final String? page;
  @JsonKey(name: 'TYPE')
  final String? type;
  @JsonKey(name: 'DESCRIPTION_HTML')
  final String? descriptionHtml;
  @JsonKey(name: 'DESCRIPTION_TEXT')
  final String? descriptionText;
  @JsonKey(name: 'IMAGE_URL')
  final String? imageUrl;
  @JsonKey(name: 'VIDEO_URL')
  final String? videoUrl;
  @JsonKey(name: 'WEB_URL')
  final String? webUrl;
  @JsonKey(name: 'APP_URL')
  final String? appUrl;
  @JsonKey(name: 'REDIRECT_URL')
  final String? redirectUrl;
  @JsonKey(name: 'DELAY')
  final int? delay;
  @JsonKey(name: 'ENABLE_CLOSE')
  final int? enableClose;
  @JsonKey(name: 'IMAGE_WIDTH')
  final dynamic imageWidth;
  @JsonKey(name: 'IMAGE_HEIGHT')
  final dynamic imageHeight;
  @JsonKey(name: 'VIDEO_WIDTH')
  final dynamic videoWidth;
  @JsonKey(name: 'VIDEO_HEIGHT')
  final dynamic videoHeight;
  @JsonKey(name: 'DEVICE')
  final String? device;
  @JsonKey(name: 'LOCATION')
  final String? location;
  @JsonKey(name: 'MAX_SHOW')
  final int? maxShow;
  @JsonKey(name: 'SEEN_COUNT')
  final int? seenCount;
  @JsonKey(name: 'SHOW_POPUP')
  final bool? showPopup;
  @JsonKey(name: 'STORY')
  final int? story;
  @JsonKey(name: 'PACKAGES')
  final List<String>? packages;
  @JsonKey(name: 'STORY_ITEMS')
  final List<StoryItemsModel>? storyItems;

  DynamicStoryModel(
      this.id,
      this.popupLogId,
      this.partnerId,
      this.parentId,
      this.title,
      this.page,
      this.type,
      this.descriptionHtml,
      this.descriptionText,
      this.imageUrl,
      this.videoUrl,
      this.webUrl,
      this.appUrl,
      this.redirectUrl,
      this.delay,
      this.enableClose,
      this.imageWidth,
      this.imageHeight,
      this.videoWidth,
      this.videoHeight,
      this.device,
      this.location,
      this.maxShow,
      this.seenCount,
      this.showPopup,
      this.story,
      this.packages,
      this.storyItems);

  factory DynamicStoryModel.fromJson(Map<String, dynamic> json) =>
      _$DynamicStoryModelFromJson(json);

  Map<String, dynamic> toJson() => _$DynamicStoryModelToJson(this);
  static List<DynamicStoryModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => DynamicStoryModel.fromJson(i as Map<String, dynamic>))
          .toList();
}

@JsonSerializable()
class StoryItemsModel {
  @JsonKey(name: 'ID')
  final int? id;
  @JsonKey(name: 'POPUP_LOG_ID')
  final int? popupLogId;
  @JsonKey(name: 'PARTNER_ID')
  final int? partnerId;
  @JsonKey(name: 'PARENT_ID')
  final int? parentId;
  @JsonKey(name: 'TITLE')
  final String? title;
  @JsonKey(name: 'PAGE')
  final String? page;
  @JsonKey(name: 'TYPE')
  final String? type;
  @JsonKey(name: 'DESCRIPTION_HTML')
  final String? descriptionHtml;
  @JsonKey(name: 'DESCRIPTION_TEXT')
  final String? descriptionText;
  @JsonKey(name: 'IMAGE_URL')
  final String? imageUrl;
  @JsonKey(name: 'VIDEO_URL')
  final String? videoUrl;
  @JsonKey(name: 'WEB_URL')
  final String? webUrl;
  @JsonKey(name: 'APP_URL')
  final String? appUrl;
  @JsonKey(name: 'REDIRECT_URL')
  final String? redirectUrl;
  @JsonKey(name: 'DELAY')
  final int? delay;
  @JsonKey(name: 'ENABLE_CLOSE')
  final int? enableClose;
  @JsonKey(name: 'IMAGE_WIDTH')
  final dynamic imageWidth;
  @JsonKey(name: 'IMAGE_HEIGHT')
  final dynamic imageHeight;
  @JsonKey(name: 'VIDEO_WIDTH')
  final dynamic videoWidth;
  @JsonKey(name: 'VIDEO_HEIGHT')
  final dynamic videoHeight;
  @JsonKey(name: 'DEVICE')
  final String? device;
  @JsonKey(name: 'LOCATION')
  final String? location;
  @JsonKey(name: 'MAX_SHOW')
  final int? maxShow;
  @JsonKey(name: 'SEEN_COUNT')
  final int? seenCount;
  @JsonKey(name: 'SHOW_POPUP')
  final bool? showPopup;
  @JsonKey(name: 'STORY')
  final int? story;
  @JsonKey(name: 'PACKAGES')
  final List<String>? packages;

  StoryItemsModel(
      this.id,
      this.popupLogId,
      this.partnerId,
      this.parentId,
      this.title,
      this.page,
      this.type,
      this.descriptionHtml,
      this.descriptionText,
      this.imageUrl,
      this.videoUrl,
      this.webUrl,
      this.appUrl,
      this.redirectUrl,
      this.delay,
      this.enableClose,
      this.imageWidth,
      this.imageHeight,
      this.videoWidth,
      this.videoHeight,
      this.device,
      this.location,
      this.maxShow,
      this.seenCount,
      this.showPopup,
      this.story,
      this.packages);

  factory StoryItemsModel.fromJson(Map<String, dynamic> json) =>
      _$StoryItemsModelFromJson(json);

  Map<String, dynamic> toJson() => _$StoryItemsModelToJson(this);
  static List<StoryItemsModel> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => StoryItemsModel.fromJson(i as Map<String, dynamic>))
      .toList();
}
