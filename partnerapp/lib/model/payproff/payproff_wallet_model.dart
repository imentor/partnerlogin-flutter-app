import 'package:json_annotation/json_annotation.dart';

part 'payproff_wallet_model.g.dart';

@JsonSerializable()
class PayproffWallet {
  @JsonKey(name: 'ID', defaultValue: 0)
  final int id;
  @JsonKey(name: 'WALLET_ID', defaultValue: '')
  final String walletId;
  @JsonKey(name: 'IBAN', defaultValue: '')
  final String iban;
  @JsonKey(name: 'UPDATED_AT', defaultValue: '')
  final String updatedAt;

  PayproffWallet({
    required this.id,
    required this.walletId,
    required this.iban,
    required this.updatedAt,
  });

  factory PayproffWallet.fromJson(Map<String, dynamic> json) =>
      _$PayproffWalletFromJson(json);

  Map<String, dynamic> toJson() => _$PayproffWalletToJson(this);
  static List<PayproffWallet> fromCollection(dynamic e) => (e as List<dynamic>)
      .map((i) => PayproffWallet.fromJson(i as Map<String, dynamic>))
      .toList();
}
