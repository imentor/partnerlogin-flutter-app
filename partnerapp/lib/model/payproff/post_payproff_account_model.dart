import 'package:json_annotation/json_annotation.dart';

part 'post_payproff_account_model.g.dart';

@JsonSerializable()
class PostPayproffAccountModel {
  String? address;
  String? birthDate;
  String? city;
  String? companyName;
  String? country;
  String? currency;
  String? cvr;
  String? displayLanguage;
  String? email;
  String? firstName;
  bool? isPep;
  String? lastName;
  String? middleName;
  int? phoneNumber;
  String? phoneNumberAreaCode;
  int? socialSecurityNumber;
  String? state;
  String? zipCode;
  String? iban;

  PostPayproffAccountModel(
      {this.address,
      this.birthDate,
      this.city,
      this.companyName,
      this.country,
      this.currency,
      this.cvr,
      this.displayLanguage,
      this.email,
      this.firstName,
      this.isPep,
      this.lastName,
      this.middleName,
      this.phoneNumber,
      this.phoneNumberAreaCode,
      this.socialSecurityNumber,
      this.state,
      this.zipCode,
      this.iban});

  factory PostPayproffAccountModel.fromJson(Map<String, dynamic> json) =>
      _$PostPayproffAccountModelFromJson(json);

  Map<String, dynamic> toJson() => _$PostPayproffAccountModelToJson(this);
}
