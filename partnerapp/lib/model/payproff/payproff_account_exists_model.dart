import 'package:json_annotation/json_annotation.dart';

part 'payproff_account_exists_model.g.dart';

@JsonSerializable()
class PayproffAccountExists {
  @JsonKey(name: 'email', defaultValue: '')
  final String email;
  @JsonKey(name: 'exists', defaultValue: false)
  final bool exists;
  @JsonKey(name: 'user_email', defaultValue: '')
  final String userEmail;
  @JsonKey(name: 'payproff_verified', defaultValue: false)
  final bool payproffVerified;
  @JsonKey(name: 'payproff_url', defaultValue: '')
  final String payproffUrl;
  @JsonKey(name: 'status', defaultValue: '')
  final String status;
  @JsonKey(name: 'status_updated', defaultValue: '')
  final String statusUpdated;

  PayproffAccountExists({
    required this.email,
    required this.exists,
    required this.userEmail,
    required this.payproffVerified,
    required this.payproffUrl,
    required this.status,
    required this.statusUpdated,
  });

  factory PayproffAccountExists.fromJson(Map<String, dynamic> json) =>
      _$PayproffAccountExistsFromJson(json);

  Map<String, dynamic> toJson() => _$PayproffAccountExistsToJson(this);
  static List<PayproffAccountExists> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => PayproffAccountExists.fromJson(i as Map<String, dynamic>))
          .toList();
}
