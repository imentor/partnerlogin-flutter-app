import 'package:json_annotation/json_annotation.dart';

part 'latest_job_feed_model.g.dart';

@JsonSerializable()
class LatestJobFeedModel {
  @JsonKey(name: 'ID')
  int? jobId;
  @JsonKey(name: 'PROJECT_NAME')
  String? projectName;
  @JsonKey(name: 'DEADLINE_OFFER')
  String? deadlineOffer;
  @JsonKey(name: 'COLOR')
  String? color;
  @JsonKey(name: 'CODE')
  String? colorCode;
  @JsonKey(name: 'CONTACT_NAME')
  String? contactName;

  LatestJobFeedModel(
      {this.jobId,
      this.projectName,
      this.deadlineOffer,
      this.color,
      this.colorCode,
      this.contactName});

  factory LatestJobFeedModel.fromJson(Map<String, dynamic> json) =>
      _$LatestJobFeedModelFromJson(json);

  Map<String, dynamic> toJson() => _$LatestJobFeedModelToJson(this);
  static List<LatestJobFeedModel> fromCollection(dynamic e) =>
      (e as List<dynamic>)
          .map((i) => LatestJobFeedModel.fromJson(i as Map<String, dynamic>))
          .toList();
}
