import 'package:json_annotation/json_annotation.dart';

part 'partner_haa_model.g.dart';

@JsonSerializable()
class PartnerHaaModel {
  @JsonKey(name: 'LevID')
  int? levID;
  @JsonKey(name: 'Firma')
  String? firma;
  @JsonKey(name: 'Kontaktperson')
  String? kontaktperson;
  @JsonKey(name: 'Adresse')
  String? adresse;
  @JsonKey(name: 'Postnr')
  String? postnr;
  @JsonKey(name: 'Fax')
  dynamic fax;
  @JsonKey(name: 'Telefon')
  String? telefon;
  @JsonKey(name: 'Mobil')
  String? mobil;
  @JsonKey(name: 'Email')
  String? email;
  @JsonKey(name: 'Web')
  String? web;
  String? password;
  @JsonKey(name: 'Dato_oprettelse')
  String? datoOprettelse;
  @JsonKey(name: 'Cvrnr')
  String? cvrnr;
  @JsonKey(name: 'InternNote')
  String? internNote;
  @JsonKey(name: 'SectorID')
  int? sectorID;
  @JsonKey(name: 'InDebt')
  bool? inDebt;
  @JsonKey(name: 'IsTest')
  bool? isTest;
  @JsonKey(name: 'BrancheOrganisation')
  int? brancheOrganisation;
  @JsonKey(name: 'Status')
  int? status;
  @JsonKey(name: 'Unsubscribed')
  bool? unsubscribed;
  @JsonKey(name: 'MaxJobs')
  dynamic maxJobs;
  @JsonKey(name: 'OfferSent')
  bool? offerSent;
  @JsonKey(name: 'MiscText')
  dynamic miscText;
  @JsonKey(name: 'BadPayer')
  bool? badPayer;
  @JsonKey(name: 'Accountance')
  String? accountance;
  @JsonKey(name: 'Parent')
  dynamic parent;
  @JsonKey(name: 'Description')
  String? description;
  @JsonKey(name: 'Logo')
  dynamic logo;
  @JsonKey(name: 'Picture')
  dynamic picture;
  @JsonKey(name: 'New')
  bool? snew;
  @JsonKey(name: 'MaxDistanceCenterPostnr')
  String? maxDistanceCenterPostnr;
  @JsonKey(name: 'GodkendtTilAutomatiskDistribution')
  String? godkendtTilAutomatiskDistribution;
  @JsonKey(name: 'BetalingsModel')
  dynamic betalingsModel;
  @JsonKey(name: 'BetalingsMetode')
  String? betalingsMetode;
  @JsonKey(name: 'TilladeVaelgLoebendeOrdrer')
  bool? tilladeVaelgLoebendeOrdrer;
  @JsonKey(name: 'FakturaPeriodeType')
  dynamic fakturaPeriodeType;
  @JsonKey(name: 'FakturaAntalDageEfterDistribution')
  dynamic fakturaAntalDageEfterDistribution;
  @JsonKey(name: 'MobileAccessGUID')
  String? mobileAccessGUID;
  @JsonKey(name: 'LeverandorProfilId')
  int? leverandorProfilId;
  @JsonKey(name: 'PaymentModelChangeDate')
  dynamic paymentModelChangeDate;
  @JsonKey(name: 'SubscriptionCancelDate')
  dynamic subscriptionCancelDate;
  @JsonKey(name: 'SubscriptionCancelReason')
  dynamic subscriptionCancelReason;
  @JsonKey(name: 'SubscriptionCancelReasonText')
  dynamic subscriptionCancelReasonText;
  @JsonKey(name: 'TrialUntilDate')
  dynamic trialUntilDate;
  @JsonKey(name: 'EfterbetalingOprettelseDato')
  dynamic efterbetalingOprettelseDato;
  @JsonKey(name: 'MailSendProfilReminder')
  dynamic mailSendProfilReminder;
  @JsonKey(name: 'AllowNets')
  bool? allowNets;
  @JsonKey(name: 'EnablePublicPage')
  bool? enablePublicPage;
  @JsonKey(name: 'OfferId')
  dynamic offerId;
  @JsonKey(name: 'DebtCollection')
  bool? debtCollection;
  @JsonKey(name: 'AllowLightProduct')
  dynamic allowLightProduct;
  @JsonKey(name: 'IsNetsApproved')
  bool? isNetsApproved;
  @JsonKey(name: 'AllowIntroOffer')
  bool? allowIntroOffer;
  @JsonKey(name: 'FreeGuideTasks')
  bool? freeGuideTasks;
  @JsonKey(name: 'Authorization')
  String? authorization;
  @JsonKey(name: 'AllDayService')
  bool? allDayService;
  @JsonKey(name: 'ProfilePictureId')
  dynamic profilePictureId;
  @JsonKey(name: 'FreeRider')
  bool? freeRider;
  @JsonKey(name: 'MiniProfile')
  bool? miniProfile;
  @JsonKey(name: 'NotUsingAppLogin')
  bool? notUsingAppLogin;
  @JsonKey(name: 'UseEmail')
  bool? useEmail;
  @JsonKey(name: 'UseSMS')
  bool? useSMS;
  @JsonKey(name: 'SmsMode')
  int? smsMode;
  @JsonKey(name: 'LockIndustryTree')
  bool? lockIndustryTree;
  @JsonKey(name: 'SupplierAlias')
  String? supplierAlias;
  @JsonKey(name: 'ShowInAdvertising')
  bool? showInAdvertising;
  @JsonKey(name: 'PrimaryIndustryId')
  int? primaryIndustryId;
  @JsonKey(name: 'IsManufacturer')
  bool? isManufacturer;
  @JsonKey(name: 'IsAssociation')
  bool? isAssociation;
  @JsonKey(name: 'Deleted')
  bool? deleted;
  @JsonKey(name: 'IMentor')
  bool? iMentor;
  dynamic product;
  dynamic expert;
  dynamic introduction;
  @JsonKey(name: 'MProfileStatus')
  dynamic mProfileStatus;
  @JsonKey(name: 'BgPosition')
  String? bgPosition;
  @JsonKey(name: 'MetaText')
  String? metaText;
  @JsonKey(name: 'MetaHeader')
  String? metaHeader;
  @JsonKey(name: 'Feature')
  String? feature;
  @JsonKey(name: 'Tags')
  dynamic tags;
  @JsonKey(name: 'ImentorTel')
  dynamic imentorTel;
  dynamic adProtection;
  @JsonKey(name: 'BGImg')
  dynamic bGImg;
  @JsonKey(name: 'ExpireDate')
  dynamic expireDate;
  @JsonKey(name: 'ExpireStatus')
  dynamic expireStatus;
  @JsonKey(name: 'AbonnementTransaktions')
  List<dynamic>? abonnementTransaktions;
  @JsonKey(name: 'AutoDistributionReservations')
  List<dynamic>? autoDistributionReservations;
  @JsonKey(name: 'CompanyReviews')
  List<dynamic>? companyReviews;
  @JsonKey(name: 'DistributionAttempts')
  List<dynamic>? distributionAttempts;
  @JsonKey(name: 'DistributionTransaktions')
  List<dynamic>? distributionTransaktions;
  @JsonKey(name: 'Invoices')
  List<dynamic>? invoices;
  @JsonKey(name: 'LevAreas')
  List<dynamic>? levAreas;
  @JsonKey(name: 'LevClientTypes')
  List<dynamic>? levClientTypes;
  @JsonKey(name: 'LeverandorAudits')
  List<dynamic>? leverandorAudits;
  @JsonKey(name: 'LeverandorAutomatiskDistributionHistories')
  List<dynamic>? leverandorAutomatiskDistributionHistories;
  @JsonKey(name: 'LeverandorContactPersons')
  List<dynamic>? leverandorContactPersons;
  @JsonKey(name: 'LeverandorOpgavers')
  List<dynamic>? leverandorOpgavers;
  @JsonKey(name: 'LeverandorStatusHistories')
  List<dynamic>? leverandorStatusHistories;
  @JsonKey(name: 'LevInternalNotes')
  List<dynamic>? levInternalNotes;
  @JsonKey(name: 'LevJobValues')
  List<dynamic>? levJobValues;
  @JsonKey(name: 'LevSubcategories')
  List<dynamic>? levSubcategories;
  @JsonKey(name: 'MobileLeverandorerNotes')
  List<dynamic>? mobileLeverandorerNotes;
  @JsonKey(name: 'Offers')
  List<dynamic>? offers;
  @JsonKey(name: 'Orders')
  List<dynamic>? orders;
  @JsonKey(name: 'SalesPersonLeverandoerChangeHistories')
  List<dynamic>? salesPersonLeverandoerChangeHistories;
  @JsonKey(name: 'SalesPersonLeverandorers')
  List<dynamic>? salesPersonLeverandorers;
  @JsonKey(name: 'StandbyPeriods')
  List<dynamic>? standbyPeriods;
  @JsonKey(name: 'SupplierCollaborators')
  List<dynamic>? supplierCollaborators;
  @JsonKey(name: 'SupplierCollaborators1')
  List<dynamic>? supplierCollaborators1;
  @JsonKey(name: 'SupplierEmailTypeRecipients')
  List<dynamic>? supplierEmailTypeRecipients;
  @JsonKey(name: 'SupplierFavoriteClicks')
  List<dynamic>? supplierFavoriteClicks;
  @JsonKey(name: 'SupplierFollowups')
  List<dynamic>? supplierFollowups;
  @JsonKey(name: 'SupplierIndustryDirectories')
  List<dynamic>? supplierIndustryDirectories;
  @JsonKey(name: 'SupplierIndustryDirectorySubcategories')
  List<dynamic>? supplierIndustryDirectorySubcategories;
  @JsonKey(name: 'SupplierIndustryOrganizations')
  List<dynamic>? supplierIndustryOrganizations;
  @JsonKey(name: 'SupplierOffers')
  List<dynamic>? supplierOffers;
  @JsonKey(name: 'SupplierOffices')
  List<dynamic>? supplierOffices;
  @JsonKey(name: 'SupplierOffices1')
  List<dynamic>? supplierOffices1;
  @JsonKey(name: 'SupplierSearch')
  dynamic supplierSearch;
  @JsonKey(name: 'SupplierSubindustries')
  List<dynamic>? supplierSubindustries;
  @JsonKey(name: 'SupplierSubscriptions')
  List<dynamic>? supplierSubscriptions;
  @JsonKey(name: 'TrackingEvents')
  List<dynamic>? trackingEvents;
  @JsonKey(name: 'Transaktions')
  List<dynamic>? transaktions;
  @JsonKey(name: 'SupplierDomains')
  List<dynamic>? supplierDomains;
  @JsonKey(name: 'ProjectFacebookOverview')
  dynamic projectFacebookOverview;
  @JsonKey(name: 'CustomerJourneyStatus')
  dynamic customerJourneyStatus;
  @JsonKey(name: 'SupplierCases')
  List<dynamic>? supplierCases;
  @JsonKey(name: 'SupplierCases1')
  List<dynamic>? supplierCases1;
  @JsonKey(name: 'SupplierCases2')
  List<dynamic>? supplierCases2;
  @JsonKey(name: 'ProjectAdditionalSuppliers')
  List<dynamic>? projectAdditionalSuppliers;
  @JsonKey(name: 'SupplierManufacturers')
  List<dynamic>? supplierManufacturers;
  @JsonKey(name: 'SupplierManufacturers1')
  List<dynamic>? supplierManufacturers1;
  @JsonKey(name: 'RatingTasktypes')
  List<dynamic>? ratingTasktypes;
  @JsonKey(name: 'SupplierAwards')
  List<dynamic>? supplierAwards;
  @JsonKey(name: 'ArticleManufactures')
  List<dynamic>? articleManufactures;
  @JsonKey(name: 'PartnerSuppliers')
  List<dynamic>? partnerSuppliers;
  @JsonKey(name: 'SupplierIndustries')
  List<dynamic>? supplierIndustries;
  @JsonKey(name: 'SupplierImages')
  List<dynamic>? supplierImages;
  @JsonKey(name: 'SupplierProfileImages')
  List<dynamic>? supplierProfileImages;
  @JsonKey(name: 'SupplierCaseManufacturers')
  List<dynamic>? supplierCaseManufacturers;
  @JsonKey(name: 'SupplierSpecialFeatures')
  List<dynamic>? supplierSpecialFeatures;
  @JsonKey(name: 'CaseMaterials')
  List<dynamic>? caseMaterials;
  @JsonKey(name: 'PrisenSponsers')
  List<dynamic>? prisenSponsers;
  @JsonKey(name: 'PrisenResults')
  List<dynamic>? prisenResults;
  @JsonKey(name: 'SupplierIntegrations')
  List<dynamic>? supplierIntegrations;
  @JsonKey(name: 'SupplierOpenHours')
  List<dynamic>? supplierOpenHours;
  @JsonKey(name: 'SupplierWebs')
  List<dynamic>? supplierWebs;
  @JsonKey(name: 'RatingRequests')
  List<dynamic>? ratingRequests;
  @JsonKey(name: 'Leverandorers')
  List<dynamic>? leverandorers;
  @JsonKey(name: 'SupplierBadges')
  List<dynamic>? supplierBadges;
  @JsonKey(name: 'Leverandorer1')
  dynamic leverandorer1;
  @JsonKey(name: 'BrancheOrganisation1')
  dynamic brancheOrganisation1;
  @JsonKey(name: 'Brancher')
  dynamic brancher;
  @JsonKey(name: 'BrancheTypeUI')
  dynamic brancheTypeUI;
  @JsonKey(name: 'LeverandorProfil')
  dynamic leverandorProfil;
  @JsonKey(name: 'LevStatus')
  dynamic levStatus;
  @JsonKey(name: 'Offer')
  dynamic offer;
  @JsonKey(name: 'AntalAnsatte')
  int? antalAnsatte;
  List<ResultImages>? resultImages;
  @JsonKey(name: 'Etableringsaar')
  int? etableringsaar;
  String? url;
  String? urltag;
  String? ratingwidget;
  String? commentwidget;
  String? premonth;
  List<ResultVideos>? resultVideos;
  String? encodeupdate;
  bool? uploadpic;

  PartnerHaaModel(
      {this.levID,
      this.firma,
      this.kontaktperson,
      this.adresse,
      this.postnr,
      this.fax,
      this.telefon,
      this.mobil,
      this.email,
      this.web,
      this.password,
      this.datoOprettelse,
      this.cvrnr,
      this.internNote,
      this.sectorID,
      this.inDebt,
      this.isTest,
      this.brancheOrganisation,
      this.status,
      this.unsubscribed,
      this.maxJobs,
      this.offerSent,
      this.miscText,
      this.badPayer,
      this.accountance,
      this.parent,
      this.description,
      this.logo,
      this.picture,
      this.snew,
      this.maxDistanceCenterPostnr,
      this.godkendtTilAutomatiskDistribution,
      this.betalingsModel,
      this.betalingsMetode,
      this.tilladeVaelgLoebendeOrdrer,
      this.fakturaPeriodeType,
      this.fakturaAntalDageEfterDistribution,
      this.mobileAccessGUID,
      this.leverandorProfilId,
      this.paymentModelChangeDate,
      this.subscriptionCancelDate,
      this.subscriptionCancelReason,
      this.subscriptionCancelReasonText,
      this.trialUntilDate,
      this.efterbetalingOprettelseDato,
      this.mailSendProfilReminder,
      this.allowNets,
      this.enablePublicPage,
      this.offerId,
      this.debtCollection,
      this.allowLightProduct,
      this.isNetsApproved,
      this.allowIntroOffer,
      this.freeGuideTasks,
      this.authorization,
      this.allDayService,
      this.profilePictureId,
      this.freeRider,
      this.miniProfile,
      this.notUsingAppLogin,
      this.useEmail,
      this.useSMS,
      this.smsMode,
      this.lockIndustryTree,
      this.supplierAlias,
      this.showInAdvertising,
      this.primaryIndustryId,
      this.isManufacturer,
      this.isAssociation,
      this.deleted,
      this.iMentor,
      this.product,
      this.expert,
      this.introduction,
      this.mProfileStatus,
      this.bgPosition,
      this.metaText,
      this.metaHeader,
      this.feature,
      this.tags,
      this.imentorTel,
      this.adProtection,
      this.bGImg,
      this.expireDate,
      this.expireStatus,
      this.abonnementTransaktions,
      this.autoDistributionReservations,
      this.companyReviews,
      this.distributionAttempts,
      this.distributionTransaktions,
      this.invoices,
      this.levAreas,
      this.levClientTypes,
      this.leverandorAudits,
      this.leverandorAutomatiskDistributionHistories,
      this.leverandorContactPersons,
      this.leverandorOpgavers,
      this.leverandorStatusHistories,
      this.levInternalNotes,
      this.levJobValues,
      this.levSubcategories,
      this.mobileLeverandorerNotes,
      this.offers,
      this.orders,
      this.salesPersonLeverandoerChangeHistories,
      this.salesPersonLeverandorers,
      this.standbyPeriods,
      this.supplierCollaborators,
      this.supplierCollaborators1,
      this.supplierEmailTypeRecipients,
      this.supplierFavoriteClicks,
      this.supplierFollowups,
      this.supplierIndustryDirectories,
      this.supplierIndustryDirectorySubcategories,
      this.supplierIndustryOrganizations,
      this.supplierOffers,
      this.supplierOffices,
      this.supplierOffices1,
      this.supplierSearch,
      this.supplierSubindustries,
      this.supplierSubscriptions,
      this.trackingEvents,
      this.transaktions,
      this.supplierDomains,
      this.projectFacebookOverview,
      this.customerJourneyStatus,
      this.supplierCases,
      this.supplierCases1,
      this.supplierCases2,
      this.projectAdditionalSuppliers,
      this.supplierManufacturers,
      this.supplierManufacturers1,
      this.ratingTasktypes,
      this.supplierAwards,
      this.articleManufactures,
      this.partnerSuppliers,
      this.supplierIndustries,
      this.supplierImages,
      this.supplierProfileImages,
      this.supplierCaseManufacturers,
      this.supplierSpecialFeatures,
      this.caseMaterials,
      this.prisenSponsers,
      this.prisenResults,
      this.supplierIntegrations,
      this.supplierOpenHours,
      this.supplierWebs,
      this.ratingRequests,
      this.leverandorers,
      this.supplierBadges,
      this.leverandorer1,
      this.brancheOrganisation1,
      this.brancher,
      this.brancheTypeUI,
      this.leverandorProfil,
      this.levStatus,
      this.offer,
      this.antalAnsatte,
      this.resultImages,
      this.etableringsaar,
      this.url,
      this.urltag,
      this.ratingwidget,
      this.commentwidget,
      this.premonth,
      this.resultVideos,
      this.encodeupdate,
      this.uploadpic});

  factory PartnerHaaModel.fromJson(Map<String, dynamic> json) =>
      _$PartnerHaaModelFromJson(json);

  Map<String, dynamic> toJson() => _$PartnerHaaModelToJson(this);
}

@JsonSerializable()
class ResultImages {
  @JsonKey(name: 'SupplierId')
  int? supplierId;
  @JsonKey(name: 'ImageId')
  int? imageId;
  @JsonKey(name: 'isPrimary')
  bool? isPrimary;
  @JsonKey(name: 'ImageTypeName')
  dynamic imageTypeName;
  @JsonKey(name: 'ImageTypeId')
  int? imageTypeId;
  @JsonKey(name: 'ImageSource')
  int? imageSource;
  @JsonKey(name: 'ImageSizeType')
  int? imageSizeType;
  @JsonKey(name: 'ImageOrder')
  int? imageOrder;
  @JsonKey(name: 'Orientation')
  dynamic orientation;
  @JsonKey(name: 'ExternalImageId')
  dynamic externalImageId;
  @JsonKey(name: 'ImageUrl')
  dynamic imageUrl;
  @JsonKey(name: 'IsDeleted')
  bool? isDeleted;
  @JsonKey(name: 'FileLocation')
  String? fileLocation;
  @JsonKey(name: 'Rotate')
  int? rotate;
  @JsonKey(name: 'Description')
  String? description;
  @JsonKey(name: 'CdnImageData')
  dynamic cdnImageData;
  @JsonKey(name: 'ImageData')
  dynamic imageData;

  ResultImages(
      {this.supplierId,
      this.imageId,
      this.isPrimary,
      this.imageTypeName,
      this.imageTypeId,
      this.imageSource,
      this.imageSizeType,
      this.imageOrder,
      this.orientation,
      this.externalImageId,
      this.imageUrl,
      this.isDeleted,
      this.fileLocation,
      this.rotate,
      this.description,
      this.cdnImageData,
      this.imageData});

  factory ResultImages.fromJson(Map<String, dynamic> json) =>
      _$ResultImagesFromJson(json);

  Map<String, dynamic> toJson() => _$ResultImagesToJson(this);
}

@JsonSerializable()
class ResultVideos {
  int? id;
  @JsonKey(name: 'Title')
  String? title;
  @JsonKey(name: 'ItemId')
  int? itemId;
  @JsonKey(name: 'Cat')
  String? cat;
  @JsonKey(name: 'IsDelete')
  bool? isDelete;
  @JsonKey(name: 'IsEnable')
  bool? isEnable;
  @JsonKey(name: 'VideoUrl')
  String? videoUrl;
  @JsonKey(name: 'Type')
  String? type;
  @JsonKey(name: 'CreateDate')
  String? createDate;
  @JsonKey(name: 'CoverImage')
  String? coverImage;
  @JsonKey(name: 'Error')
  String? error;
  @JsonKey(name: 'Success')
  bool? success;

  ResultVideos(
      {this.id,
      this.title,
      this.itemId,
      this.cat,
      this.isDelete,
      this.isEnable,
      this.videoUrl,
      this.type,
      this.createDate,
      this.coverImage,
      this.error,
      this.success});

  factory ResultVideos.fromJson(Map<String, dynamic> json) =>
      _$ResultVideosFromJson(json);

  Map<String, dynamic> toJson() => _$ResultVideosToJson(this);
}
