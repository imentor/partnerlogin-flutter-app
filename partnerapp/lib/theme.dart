import 'package:flutter/material.dart';

class PartnerAppColors {
  PartnerAppColors._();

  static const blue = Color(0xff069DBF);
  static const green = Color(0xff00DA58);
  static const darkBlue = Color(0xff003645);
  static const black = Color(0xff1b1b1b);
  static const facebookBlue = Color(0xff3b5998);
  static const accentBlue = Color(0xff00c7cf);
  static const orangeApple = Color(0xffFF6F00);
  static const crushedPineapple = Color(0xFFEFCD46);
  static const Color malachite = Color(0xff4DCF55);
  static const grey = Color(0xffB5B5B5);
  static const grey1 = Color(0xff707070);
  static const red = Color.fromRGBO(207, 0, 15, 1.0);
  static const Color spanishGrey = Color(0xff979797);
  static const mitidBlue = Color(0xff3664AF);
  static const approveGreen = Color(0xff4DCF56);
  static const partialYellow = Color(0xffFFC400);
  static const pendingBlue = Color(0xff19A3C0);
  static const declinedRed = Color(0xffFC0001);
  static const refundBlue = Color(0xff0075FF);
  static const lightYellow = Color(0xffDE9A00);
}

ThemeData get appTheme {
  return ThemeData(
    useMaterial3: false,
    colorScheme: const ColorScheme.light(
      primary: PartnerAppColors.blue,
      onPrimary: PartnerAppColors.darkBlue,
      secondary: PartnerAppColors.green,
      onSecondary: PartnerAppColors.grey,
      onTertiary: PartnerAppColors.grey1,
      surface: Colors.white,
      onSurfaceVariant: PartnerAppColors.darkBlue,
      onTertiaryContainer: PartnerAppColors.black,
      error: PartnerAppColors.red,
    ),
    scaffoldBackgroundColor: const Color(0xffFCFCFC),
  ).applyTextTheme();
}

extension _ThemeDataX on ThemeData {
  ThemeData applyTextTheme() {
    return copyWith(
      scaffoldBackgroundColor: const Color(0xffFCFCFC),
      appBarTheme: const AppBarTheme(color: Colors.white),
      textButtonTheme: const TextButtonThemeData(
        style: ButtonStyle(
          textStyle: WidgetStatePropertyAll(
            TextStyle(
              fontFamily: 'NotoSans',
              fontSize: 10,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      ),
      primaryTextTheme: primaryTextTheme.copyWith(
        titleLarge: textTheme.titleLarge?.copyWith(
          fontFamily: 'NotoSans',
          fontSize: 20,
          fontWeight: FontWeight.w600,
          color: colorScheme.onPrimary,
          height: 1.35,
        ),
        titleMedium: primaryTextTheme.titleMedium?.copyWith(
          fontFamily: 'NotoSans',
          fontSize: 18,
          fontWeight: FontWeight.w400,
          color: colorScheme.onPrimary,
          height: 1.33,
        ),
        titleSmall: primaryTextTheme.titleSmall?.copyWith(
          fontFamily: 'NotoSans',
          fontSize: 16,
          fontWeight: FontWeight.w600,
          color: colorScheme.onPrimary,
          height: 1.375,
        ),
        labelSmall: primaryTextTheme.labelSmall?.copyWith(
          fontFamily: 'NotoSans',
          fontSize: 8,
          fontWeight: FontWeight.w600,
          color: colorScheme.onPrimary,
          height: 3,
        ),
        bodyLarge: primaryTextTheme.bodyLarge?.copyWith(
          fontFamily: 'NotoSans',
          fontSize: 12,
          fontWeight: FontWeight.w600,
          color: colorScheme.onPrimary,
          height: 2,
        ),
        bodySmall: primaryTextTheme.bodySmall?.copyWith(
          fontFamily: 'NotoSans',
          fontSize: 13,
          fontWeight: FontWeight.w400,
          color: colorScheme.onTertiary,
          height: 1.4,
        ),
      ),
      textTheme: textTheme.copyWith(
          headlineLarge: textTheme.headlineLarge?.copyWith(
            fontFamily: 'Gibson',
            fontSize: 24,
            fontWeight: FontWeight.bold,
            color: colorScheme.onSurfaceVariant,
          ),
          headlineMedium: textTheme.headlineMedium?.copyWith(
            fontFamily: 'Gibson',
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: colorScheme.onSurfaceVariant,
          ),
          headlineSmall: textTheme.headlineSmall?.copyWith(
            fontFamily: 'Gibson',
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: colorScheme.onSurfaceVariant,
          ),
          titleLarge: textTheme.titleLarge?.copyWith(
            fontFamily: 'NotoSans',
            fontSize: 20,
            fontWeight: FontWeight.w600,
            color: colorScheme.onPrimary,
            height: 1.35,
          ),
          titleMedium: textTheme.titleMedium?.copyWith(
            fontFamily: 'Gibson',
            fontWeight: FontWeight.normal,
            fontSize: 16,
            color: colorScheme.onSurfaceVariant,
          ),
          titleSmall: textTheme.titleMedium?.copyWith(
            fontFamily: 'Gibson',
            fontWeight: FontWeight.normal,
            fontSize: 14,
            color: colorScheme.onSurfaceVariant,
          ),
          bodyLarge: textTheme.titleMedium?.copyWith(
            fontFamily: 'Gibson',
            color: colorScheme.onSurfaceVariant,
          ),
          bodyMedium: TextStyle(
            fontFamily: 'Gibson',
            fontSize: 14.0,
            color: colorScheme.onSurfaceVariant,
            fontWeight: FontWeight.w600,
            height: 1.6,
            letterSpacing: -0.5,
          ),
          labelMedium: TextStyle(
            fontFamily: 'Gibson',
            fontSize: 20.0,
            color: colorScheme.primary,
            fontWeight: FontWeight.w700,
            height: 1.6,
          ),
          labelSmall: const TextStyle(
            fontFamily: 'Gibson',
            fontSize: 10.0,
            color: Colors.black45,
            fontWeight: FontWeight.w400,
            height: 1.6,
          )),
    );
  }
}
