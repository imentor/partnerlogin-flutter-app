import 'package:Haandvaerker.dk/_config/env.dart';
import 'package:Haandvaerker.dk/main.dart';

Future<void> main() async {
  await mainCommon(Environment.prod);
}
