import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:Haandvaerker.dk/firebase_options.dart';
import 'package:Haandvaerker.dk/providers.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/interceptor/dio_request_retrier.dart';
import 'package:Haandvaerker.dk/services/interceptor/retry_interceptor.dart';
import 'package:Haandvaerker.dk/services/local_notification/local_notification_service.dart';
import 'package:Haandvaerker.dk/services/main/main_service.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/routing/routing.dart';
import 'package:Haandvaerker.dk/ui/screens/app_loading_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/login/intro_screen.dart';
import 'package:Haandvaerker.dk/ui/screens/reset_password/reset_email_password.dart';
import 'package:Haandvaerker.dk/ui/screens/reset_password/reset_password.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/encryption_helper.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/app_links/app_links_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/offer/offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/version_checker/version_checker_viewmodel.dart';
import 'package:app_links/app_links.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_background_geolocation/flutter_background_geolocation.dart'
    as bg;
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:scaled_app/scaled_app.dart';
import 'package:sentry_dio/sentry_dio.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

@pragma('vm:entry-point')
void headlessTask(bg.HeadlessEvent headlessEvent) async {
  log('[BackgroundGeolocation HeadlessTask]: $headlessEvent');
  // Implement a 'case' for only those events you're interested in.
  switch (headlessEvent.name) {
    case bg.Event.TERMINATE:
      bg.State state = headlessEvent.event;
      log('- State: $state');
      break;
    case bg.Event.HEARTBEAT:
      bg.HeartbeatEvent event = headlessEvent.event;
      log('- HeartbeatEvent: $event');
      break;
    case bg.Event.LOCATION:
      bg.Location location = headlessEvent.event;
      log('- Location: $location');
      break;
    case bg.Event.MOTIONCHANGE:
      bg.Location location = headlessEvent.event;
      log('- Location: $location');
      break;
    case bg.Event.GEOFENCE:
      bg.GeofenceEvent geofenceEvent = headlessEvent.event;
      log('- GeofenceEvent: $geofenceEvent');
      break;
    case bg.Event.GEOFENCESCHANGE:
      bg.GeofencesChangeEvent event = headlessEvent.event;
      log('- GeofencesChangeEvent: $event');
      break;
    case bg.Event.SCHEDULE:
      bg.State state = headlessEvent.event;
      log('- State: $state');
      break;
    case bg.Event.ACTIVITYCHANGE:
      bg.ActivityChangeEvent event = headlessEvent.event;
      log('ActivityChangeEvent: $event');
      break;
    case bg.Event.HTTP:
      bg.HttpEvent response = headlessEvent.event;
      log('HttpEvent: $response');
      break;
    case bg.Event.POWERSAVECHANGE:
      bool enabled = headlessEvent.event;
      log('ProviderChangeEvent: $enabled');
      break;
    case bg.Event.CONNECTIVITYCHANGE:
      bg.ConnectivityChangeEvent event = headlessEvent.event;
      log('ConnectivityChangeEvent: $event');
      break;
    case bg.Event.ENABLEDCHANGE:
      bool enabled = headlessEvent.event;
      log('EnabledChangeEvent: $enabled');
      break;
  }
}

Future<void> mainCommon(String env) async {
  runZonedGuarded(
    () async {
      final widgetsBinding = ScaledWidgetsFlutterBinding.ensureInitialized(
          scaleFactor: (deviceSize) {
        const double mockUpSize = 430;
        return deviceSize.width / mockUpSize;
      });
      FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);

      await applic.initialize(env);

      final SharedPreferences prefs = await SharedPreferences.getInstance();

      const storage = FlutterSecureStorage(
          aOptions: AndroidOptions(encryptedSharedPreferences: true),
          iOptions: IOSOptions());

      if (!(prefs.getBool('app_installed') ?? false)) {
        await storage.deleteAll();
        prefs.setBool('app_installed', true);
      }

      if ((await storage.read(key: 'encryption_key')) == null ||
          (await storage.read(key: 'encryption_iv')) == null) {
        final generatedEncryption = EncryptionHelper.generateRandomKeyAndIV();

        await storage.write(
            key: 'encryption_key', value: generatedEncryption.key);
        await storage.write(
            key: 'encryption_iv', value: generatedEncryption.iv);
      }

      final tokenDateKey = (await storage.read(key: 'tokenDateValidity'));
      if (tokenDateKey != null) {
        final encryptionKey = await storage.read(key: 'encryption_key');
        final encryptionIv = await storage.read(key: 'encryption_iv');

        final AesKeys keys =
            AesKeys(key: encryptionKey ?? '', iv: encryptionIv ?? '');

        if (EncryptionHelper.decrypt(tokenDateKey, keys).isNotEmpty) {
          final tokenDateValidity =
              DateTime.parse(EncryptionHelper.decrypt(tokenDateKey, keys));

          if (DateTime.now().isAfter(tokenDateValidity)) {
            await storage.deleteAll();
          }
        }
      }

      final defaultScreen = (await storage.read(key: 'bitrix_token')) != null
          ? const AppSplashScreen()
          : const IntroScreen();

      await EasyLocalization.ensureInitialized();

      await Firebase.initializeApp(
        // name: env,
        options: DefaultFirebaseOptions.currentPlatform,
      );

      if (kDebugMode) {
        HttpOverrides.global = _DevHttpOverrides();
      }

      if (kReleaseMode) {
        final pInfo = await PackageInfo.fromPlatform();
        await SentryFlutter.init((options) {
          options.dsn = Links.sentryDsn;
          options.release = 'partnerapp@${pInfo.buildNumber}';
          options.enableAppHangTracking = false;
        });

        FlutterError.onError = (errorDetails) async {
          await FirebaseCrashlytics.instance
              .recordFlutterFatalError(errorDetails);
        };

        PlatformDispatcher.instance.onError = (error, stack) {
          FirebaseCrashlytics.instance.recordError(error, stack, fatal: true);
          return true;
        };
      }

      const notificationSettings = InitializationSettings(
        android: AndroidInitializationSettings('@mipmap/ic_launcher'),
        iOS: DarwinInitializationSettings(
          requestAlertPermission: true,
          requestBadgePermission: true,
          requestSoundPermission: true,
        ),
      );

      await flutterLocalNotificationsPlugin.initialize(
        notificationSettings,
        onDidReceiveNotificationResponse: onSelectNotification,
        onDidReceiveBackgroundNotificationResponse: onSelectNotification,
      );

      final dio = Dio()
        ..interceptors.add(
          RetryOnConnectionChangeInterceptor(
            storage: SecuredStorageService(
                storage: storage, sharedPreferences: prefs),
            requestRetrier: DioRequestRetrier(
              dio: Dio(),
              connectivity: Connectivity(),
            ),
          ),
        );

      if (kReleaseMode) {
        dio.addSentry();
      }

      runApp(
        MultiProvider(
          providers: listOfProviders(
              dio: dio, storage: storage, sharedPreferences: prefs),
          child: EasyLocalization(
            supportedLocales: const [Locale('en'), Locale('da', 'DK')],
            path: 'assets/i10n',
            fallbackLocale:
                kDebugMode ? const Locale('en') : const Locale('da', 'DK'),
            child: MyApp(defaultScreen: defaultScreen),
          ),
        ),
      );

      bg.BackgroundGeolocation.registerHeadlessTask(headlessTask);
    },
    (error, stack) async {
      if (kReleaseMode) {
        await Sentry.captureException(error, stackTrace: stack);
      }
    },
  );
}

class MyApp extends StatefulWidget {
  const MyApp({super.key, required this.defaultScreen});

  final Widget defaultScreen;

  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  final loadingKey = GlobalKey<NavigatorState>();

  StreamSubscription<Uri>? _linkSubscription;
  late AppLinks _appLinks;

  void _initDeepLinkListener() async {
    final appLinkVm = context.read<AppLinksViewModel>();
    final userVm = context.read<UserViewModel>();
    final offerVm = context.read<OfferViewModel>();

    _appLinks = AppLinks();

    _linkSubscription = _appLinks.uriLinkStream.listen((uri) async {
      if (uri.toString().contains('/login')) {
        if (uri.toString().contains('page=credit')) {
          await launchUrl(uri, mode: LaunchMode.inAppWebView);
        } else {
          modalManager.showLoadingModal();

          if (!mounted) return;

          await tryCatchWrapper(
                  context: myGlobals.mainNavigatorKey!.currentContext!,
                  function:
                      appLinkVm.processUri(uri: uri, fromInitialLink: false))
              .whenComplete(() async {
            if (appLinkVm.appLinkToken.isNotEmpty) {
              await appLinkVm.loginFromAppLink().whenComplete(() {
                if (appLinkVm.appLinkLogin != null) {
                  if (mounted) {
                    modalManager.hideLoadingModal();
                  }
                  userVm.userModel = appLinkVm.appLinkLogin;
                  final currentState = myGlobals.mainNavigatorKey?.currentState;
                  if (currentState != null) {
                    currentState.pushReplacement(
                      MaterialPageRoute(
                          builder: (context) => const AppSplashScreen(
                                isFromAppLink: true,
                              )),
                    );
                  }
                } else {
                  modalManager.hideLoadingModal();
                }
              });
            } else {
              modalManager.hideLoadingModal();
              if (appLinkVm.pageRedirect.isNotEmpty) {
                switch (appLinkVm.pageRedirect) {
                  case 'projectdashboard':
                  case 'directcontract':
                  case 'paymentstage':
                  case 'directextrawork':
                  case 'uploadinvoice':
                  case 'contractdraft':
                  case 'directcontractver':
                    if (appLinkVm.projectId.isNotEmpty) {
                      changeDrawerRoute(Routes.projectDashboard, arguments: {
                        'projectId': int.parse(appLinkVm.projectId)
                      });
                    }
                    break;
                  case 'market':
                    if (appLinkVm.projectId.isNotEmpty) {
                      changeDrawerRoute(Routes.marketPlaceScreen,
                          arguments: true);
                    }
                    break;
                  case 'minekunder':
                    if (appLinkVm.projectId.isNotEmpty &&
                        appLinkVm.contactId.isNotEmpty) {
                      changeDrawerRoute(Routes.yourClient,
                          arguments: {'isFromAppLink': true});
                    }
                    break;
                  case 'priceCalculator':
                    changeDrawerRoute(Routes.priceCalculator);
                    appLinkVm.resetLoginAppLink();
                    break;
                  case 'cancelContract':
                    changeDrawerRoute(Routes.cancelContract);
                    break;
                  case 'newofferversion':
                    if (appLinkVm.offerId.isNotEmpty) {
                      changeDrawerRoute(Routes.editOfferLanding);
                    }
                    break;
                  case 'opretoffer':
                  case 'uploadoffer':
                    if (appLinkVm.signOffer) {
                      if (appLinkVm.offerVersion.isNotEmpty) {
                        offerVm.viewSignatureOfferVersionId =
                            int.parse(appLinkVm.offerVersion);
                      }
                      changeDrawerRoute(Routes.signatureOffer);
                    }
                    break;
                  case 'offerByCs':
                    if (appLinkVm.projectId.isNotEmpty) {
                      changeDrawerRoute(Routes.yourClient,
                          arguments: {'isFromAppLink': true});
                    }
                    break;
                  case 'subcontract':
                    if (appLinkVm.projectId.isNotEmpty &&
                        appLinkVm.subcontractorId.isNotEmpty) {
                      changeDrawerRoute(Routes.subcontractorAcceptInvite,
                          arguments: {
                            'subContractId':
                                int.parse(appLinkVm.subcontractorId),
                            'projectId': int.parse(appLinkVm.projectId)
                          });
                    }
                    break;
                  default:
                }
              }
            }
          });
        }
      } else if (uri.toString().contains('/invo')) {
        await launchUrl(uri, mode: LaunchMode.inAppWebView);
      } else if (uri.toString().contains('glemte-password')) {
        myGlobals.mainNavigatorKey?.currentState!.push(
          MaterialPageRoute(builder: (context) => const ResetEmailPassword()),
        );
      } else if (uri.toString().contains('nulstille-kodeord')) {
        appLinkVm.extractParamsFromLink(link: uri).then((value) {
          if (value.containsKey('email')) {
            myGlobals.mainNavigatorKey?.currentState!.push(
              MaterialPageRoute(
                  builder: (context) => ResetPassword(
                        email: value['email'],
                      )),
            );
          }
        });
      }
    }, onError: (e) {
      modalManager.hideLoadingModal();
      log("message applinks error: $e");
    });

    Uri? initialLink = await _appLinks.getInitialLink();
    if (initialLink != null) {
      if (initialLink.toString().contains('/login')) {
        if (initialLink.toString().contains('page=credit')) {
          await launchUrl(initialLink, mode: LaunchMode.inAppWebView);
        } else {
          await appLinkVm
              .processUri(uri: initialLink, fromInitialLink: true)
              .then((value) async {
            if (appLinkVm.appLinkToken.isNotEmpty) {
              modalManager.showLoadingModal();

              await appLinkVm.loginFromAppLink().whenComplete(() {
                if (appLinkVm.appLinkLogin != null) {
                  if (mounted) {
                    modalManager.hideLoadingModal();
                  }
                  userVm.userModel = appLinkVm.appLinkLogin;
                  myGlobals.mainNavigatorKey!.currentState?.pushReplacement(
                    MaterialPageRoute(
                        builder: (context) => const AppSplashScreen(
                              isFromAppLink: true,
                            )),
                  );
                } else {
                  modalManager.hideLoadingModal();
                }
              });
            }
          });
        }
      } else if (initialLink.toString().contains('/invo')) {
        await appLinkVm.processOutsideUri(uri: initialLink).then((value) async {
          if (value) {
            await launchUrl(initialLink, mode: LaunchMode.inAppWebView);
          }
        });
      } else if (initialLink.toString().contains('glemte-password')) {
        myGlobals.mainNavigatorKey?.currentState!.push(
          MaterialPageRoute(builder: (context) => const ResetEmailPassword()),
        );
      } else if (initialLink.toString().contains('nulstille-kodeord')) {
        appLinkVm.extractParamsFromLink(link: initialLink).then((value) {
          if (value.containsKey('email')) {
            myGlobals.mainNavigatorKey?.currentState!.push(
              MaterialPageRoute(
                  builder: (context) => ResetPassword(
                        email: value['email'],
                      )),
            );
          }
        });
      }
    }
  }

  @override
  void dispose() {
    _linkSubscription?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final mainService = context.read<MainService>();
      final versionCheckerVm = context.read<VersionCheckerViewmodel>();

      await versionCheckerVm.checkUpdateAvailable();
      _initDeepLinkListener();
      final language = await mainService.getLanguage();

      if (language == Language.english || language == Language.danish) {
        final locale = Locale(language == Language.english ? 'en' : 'da',
            language == Language.english ? null : 'DK');

        if (!mounted) return;

        context.setLocale(locale);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return MaterialApp(
      title: Strings.appName,
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      navigatorKey: myGlobals.mainNavigatorKey,
      theme: appTheme,
      debugShowCheckedModeBanner: false,
      navigatorObservers: [context.read<FirebaseAnalyticsObserver>()],
      home: widget.defaultScreen,
      onGenerateRoute: RouteGenerator.generateMainRoute,
      builder: (context, child) => AnnotatedRegion(
        value: SystemUiOverlayStyle.light.copyWith(
          systemNavigationBarIconBrightness: Brightness.dark,
          systemNavigationBarColor: Colors.white,
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark,
        ),
        child: MediaQuery(
          data: MediaQuery.of(context).copyWith(),
          child: child!,
        ),
      ),
    );
  }
}

class _DevHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (cert, host, port) => true;
  }
}
