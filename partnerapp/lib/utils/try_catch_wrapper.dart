import 'dart:developer';
import 'dart:io';

import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/ui/components/common_dialogs.dart';
import 'package:Haandvaerker.dk/ui/components/modals/modal_manager.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/custom_exceptions.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<T?> tryCatchWrapper<T>({
  required BuildContext? context,
  required Future<T>? function,
  T? cancelValue,
}) async {
  try {
    return await function;
  } on SocketException catch (e) {
    log('SocketException: ${e.message}');

    modalManager.hideLoadingModal();

    await Sentry.captureMessage('SocketException: ${e.message}')
        .whenComplete(() async {
      if (context != null && context.mounted) {
        await showOkAlertDialog(
          context: context,
          title: tr('error_connection_title'),
          message: tr('error_connection_content'),
          okLabel: tr('ok'),
        );
      }
    });
  } on SessionException catch (e) {
    log('SessionException: ${e.errorMessage()}');

    modalManager.hideLoadingModal();

    await Sentry.captureMessage('SessionException: ${e.errorMessage()}')
        .whenComplete(() async {
      if (context != null && context.mounted) {
        await showOkAlertDialog(
          context: context,
          title: tr('error_session_title'),
          message: tr('error_session_content'),
          okLabel: tr('ok'),
        );
      }
      final sharedPreferences = await SharedPreferences.getInstance();
      await sharedPreferences.clear();
      if (context != null && context.mounted) {
        Navigator.of(context, rootNavigator: true)
            .pushReplacementNamed(Routes.login);
      }
    });
  } on SendEmailException catch (e) {
    log('SendEmailException: ${e.errorMessage()}');
    modalManager.hideLoadingModal();

    await Sentry.captureMessage('SendEmailException: ${e.errorMessage()}')
        .whenComplete(() async {
      if (context != null && context.mounted) {
        await showOkAlertDialog(
          context: context,
          title: tr('error'),
          message: tr('unable_to_send_email'),
          okLabel: tr('ok'),
        );
      }
    });
  } on NoPermissionException catch (e) {
    log('NoPermissionException: ${e.errorMessage()}');
    modalManager.hideLoadingModal();

    await Sentry.captureMessage('NoPermissionException: ${e.errorMessage()}');

    final sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.clear();
    if (context != null && context.mounted) {
      await showOkAlertDialog(
        context: context,
        title: tr('error'),
        message: tr(
          e.errorMessage(),
        ),
        okLabel: tr('ok'),
      );
    }

    return cancelValue;
  } on AdminException catch (e) {
    log('AdminException: ${e.errorMessage()}');
    modalManager.hideLoadingModal();

    await Sentry.captureMessage('AdminException: ${e.errorMessage()}');

    final sharedPreferences = await SharedPreferences.getInstance();

    if (context != null && context.mounted) {
      await sharedPreferences.clear();
      if (context.mounted) {
        await showOkAlertDialog(
          context: context,
          title: tr('incorrect_login'),
          message: tr('please_try_again'),
          okLabel: tr('ok'),
        );
      }
    }
    return cancelValue;
  } on TypeError catch (e) {
    log('TypeError: $e');
    modalManager.hideLoadingModal();

    await Sentry.captureMessage('TypeError: $e');

    if (context != null && context.mounted) {
      await showOkAlertDialog(
        context: context,
        title: tr('error'),
        message: tr('error_unknown_content'),
        okLabel: tr('ok'),
      );
    }

    return cancelValue;
  } on ApiException catch (e) {
    log('ApiException: ${e.message}');
    modalManager.hideLoadingModal();

    await Sentry.captureMessage('ApiException: ${e.message}');

    if (!e.message.contains('Kan ikke afgive tilbud error')) {
      if (context != null && context.mounted) {
        await commonSuccessOrFailedDialog(
            context: context,
            isSuccess: false,
            showTitle: false,
            message: e.message);
        // await showOkAlertDialog(
        //   context: context,
        //   title: tr('error'),
        //   message: e.message,
        //   okLabel: tr('ok'),
        // );
      }
    } else if (e.message.contains('Failed to logout, please try again.')) {
      const storage = FlutterSecureStorage(
          aOptions: AndroidOptions(encryptedSharedPreferences: true),
          iOptions: IOSOptions());
      await storage.deleteAll().whenComplete(() {
        if (context != null && context.mounted) {
          Navigator.pushNamedAndRemoveUntil(
              context, Routes.login, (r) => false);
        }
      });
    }
  } on CancelException catch (e) {
    log('Cancel Exception: $e');
    modalManager.hideLoadingModal();

    await Sentry.captureMessage('TypeError: $e').whenComplete(() async {
      if (context != null && context.mounted) {
        await showOkAlertDialog(
          context: context,
          title: tr('error'),
          message: tr('error_unknown_content'),
          okLabel: tr('ok'),
        );
      }
    });
  }
  return null;
}
