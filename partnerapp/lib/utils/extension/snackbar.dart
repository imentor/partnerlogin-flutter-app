import 'package:Haandvaerker.dk/utils/element_icons.dart';
import 'package:Haandvaerker.dk/utils/gaps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

extension SnackBarX on BuildContext {
  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> showSnackBar({
    String? message,
    Icon? leading,
    bool isFloating = false,
    EdgeInsets? margin,
    Widget? content,
  }) {
    ScaffoldMessenger.of(this).clearSnackBars();
    return ScaffoldMessenger.of(this).showSnackBar(
      SnackBar(
        behavior: isFloating ? SnackBarBehavior.floating : null,
        margin: margin,
        content: content ??
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                if (leading != null)
                  Padding(
                    padding: const EdgeInsets.only(right: 5),
                    child: leading,
                  ),
                Text(message ?? tr('success')),
              ],
            ),
      ),
    );
  }

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> showErrorSnackBar({
    String? message,
    bool isFloating = false,
    EdgeInsets? margin,
    Widget? content,
  }) {
    ScaffoldMessenger.of(this).clearSnackBars();
    return ScaffoldMessenger.of(this).showSnackBar(
      SnackBar(
        behavior: isFloating ? SnackBarBehavior.floating : null,
        margin: margin,
        content: content ??
            Row(
              children: <Widget>[
                const Icon(ElementIcons.warning_outline, color: Colors.red),
                SmartGaps.gapW5,
                Text(
                  message ?? tr('error'),
                  style: const TextStyle(color: Colors.white),
                ),
              ],
            ),
      ),
    );
  }

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> showCustomSnackBar(
    SnackBar widget,
  ) {
    ScaffoldMessenger.of(this).clearSnackBars();
    return ScaffoldMessenger.of(this).showSnackBar(widget);
  }
}
