import 'package:flutter/material.dart';

extension PartnerAppColorThemes on BuildContext {
  //ColorScheme
  ColorScheme get colorScheme => Theme.of(this).colorScheme;
}
