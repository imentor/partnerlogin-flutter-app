import 'package:flutter/material.dart';

extension PartnerTextThemes on BuildContext {
  //

  //PRIMARY TEXT THEME
  TextStyle get pttTitleLarge => Theme.of(this).primaryTextTheme.titleLarge!;
  TextStyle get pttTitleMedium => Theme.of(this).primaryTextTheme.titleMedium!;
  TextStyle get pttTitleSmall => Theme.of(this).primaryTextTheme.titleSmall!;
  TextStyle get pttLabelSmall => Theme.of(this).primaryTextTheme.labelSmall!;
  TextStyle get pttBodyLarge => Theme.of(this).primaryTextTheme.bodyLarge!;
  TextStyle get pttBodySmall => Theme.of(this).primaryTextTheme.bodySmall!;
  //
}
