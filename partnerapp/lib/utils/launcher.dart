import 'package:url_launcher/url_launcher.dart';

class Launcher {
  launchEmail(String? email) async {
    final Uri uri =
        Uri(scheme: 'mailto', path: email, queryParameters: {'subject': ''});

    if (await canLaunchUrl(uri)) {
      await launchUrlLocal(uri.toString());
    } else {
      throw 'Could not launch';
    }
  }

  launchPhoneCall(String? phoneNumber) async {
    var uri = 'tel:$phoneNumber';
    if (await canLaunchUrl(Uri.parse(Uri.encodeFull(uri)))) {
      await launchUrlLocal(Uri.encodeFull(uri));
    }
  }

  launchSms(String phoneNumber) async {
    var uri = 'sms:$phoneNumber';
    if (await canLaunchUrl(Uri.parse(Uri.encodeFull(uri)))) {
      await launchUrlLocal(Uri.encodeFull(uri));
    }
  }

  launchUrlLocal(String url) async {
    final finalUrl =
        (url.contains('%') || url.contains('+')) ? url : Uri.encodeFull(url);

    if (await canLaunchUrl(Uri.parse(finalUrl))) {
      await launchUrl(Uri.parse(finalUrl),
          mode: LaunchMode.externalApplication);
    } else {
      throw 'Could not launch $finalUrl';
    }
  }
}
