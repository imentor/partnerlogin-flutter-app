// ignore_for_file: unused_local_variable

import 'dart:developer';
import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:package_info_plus/package_info_plus.dart';
//import 'package:sentry/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

enum AppEnvironment { production, development, testing, local }

Future<dynamic> getSentryEnvEvent(
//Future<Event> getSentryEnvEvent(
    dynamic exception,
    dynamic stackTrace,
    String username) async {
  Map<AppEnvironment, String> appEnvironment = {
    AppEnvironment.production: 'Production',
    AppEnvironment.development: 'Development',
    AppEnvironment.testing: 'Testing',
    AppEnvironment.local: 'Local',
  };

  PackageInfo packageInfo = await PackageInfo.fromPlatform();

  if (Platform.isIOS) {
    final IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
    // return Event(
    //   userContext: User(username: username, email: username),
    //   release: packageInfo.version + '+' + packageInfo.buildNumber,
    //   environment: appEnvironment[isInDebugMode
    //       ? AppEnvironment.development
    //       : AppEnvironment.production],
    //   extra: <String, dynamic>{
    //     'name': iosDeviceInfo.name,
    //     'model': iosDeviceInfo.model,
    //     'systemName': iosDeviceInfo.systemName,
    //     'systemVersion': iosDeviceInfo.systemVersion,
    //     'localizedModel': iosDeviceInfo.trdModel,
    //     'utsname': iosDeviceInfo.utsname.sysname,
    //     'identifierForVendor': iosDeviceInfo.identifierForVendor,
    //     'isPhysicalDevice': iosDeviceInfo.isPhysicalDevice,
    //     'drawerRoute': APPLIC.currentDrawerRoute,
    //     'username': username,
    //   },
    //   exception: exception,
    //   stackTrace: stackTrace,
    // );
  }

  if (Platform.isAndroid) {
    final AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
    // return Event(
    //   userContext: User(username: username, email: username),
    //   release: packageInfo.version + '+' + packageInfo.buildNumber,
    //   environment: appEnvironment[isInDebugMode
    //       ? AppEnvironment.development
    //       : AppEnvironment.production],
    //   extra: <String, dynamic>{
    //     'type': androidDeviceInfo.type,
    //     'model': androidDeviceInfo.model,
    //     'device': androidDeviceInfo.device,
    //     'id': androidDeviceInfo.id,
    //     'androidId': androidDeviceInfo.androidId,
    //     'brand': androidDeviceInfo.brand,
    //     'display': androidDeviceInfo.display,
    //     'hardware': androidDeviceInfo.hardware,
    //     'manufacturer': androidDeviceInfo.manufacturer,
    //     'product': androidDeviceInfo.product,
    //     'version': androidDeviceInfo.version.release,
    //     'supported32BitAbis': androidDeviceInfo.supported32BitAbis,
    //     'supported64BitAbis': androidDeviceInfo.supported64BitAbis,
    //     'supportedAbis': androidDeviceInfo.supportedAbis,
    //     'isPhysicalDevice': androidDeviceInfo.isPhysicalDevice,
    //     'drawerRoute': APPLIC.currentDrawerRoute,
    //     'username': username,
    //   },
    //   exception: exception,
    //   stackTrace: stackTrace,
    // );
  }

  // return Event(
  //   release: packageInfo.version + '+' + packageInfo.buildNumber,
  //   environment: appEnvironment[
  //       isInDebugMode ? AppEnvironment.development : AppEnvironment.production],
  //   exception: exception,
  //   stackTrace: stackTrace,
  // );
}

Future<void> reportError(Object error, StackTrace stackTrace) async {
  if (isInDebugMode) {
    log('No Sending report to sentry.io as mode is debugging DartError');
    // Print the full stacktrace in debug mode.
    log("$stackTrace");
    return;
  } else {
    try {
      // final Event event = await getSentryEnvEvent(error, stackTrace, '');
      //log('Sending report to sentry.io $event');
      //await sentry.capture(event: event);
    } catch (e) {
      log('Sending report to sentry.io failed: $e');
      log('Original error: $error');
    }
  }
}

Future<void> reportErrorDebugAndRelease(
    Object error, StackTrace stackTrace) async {
  try {
    var sharedPreferences = await SharedPreferences.getInstance();
    String? username = '';

    if (sharedPreferences.getString('username') != null) {
      username = sharedPreferences.getString('username');
    }
    //final Event event = await getSentryEnvEvent(error, stackTrace, username);
    //log('Sending report to sentry.io $event');
    //await sentry.capture(event: event);
  } catch (e) {
    log('Sending report to sentry.io failed: $e');
    log('Original error: $error');
  }
}

bool get isInDebugMode {
  bool inDebugMode = false;
  assert(inDebugMode = true);
  return inDebugMode;
}
