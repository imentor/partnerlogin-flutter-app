class SessionException implements Exception {
  String errorMessage() {
    return 'Session Expired';
  }
}

class SendEmailException implements Exception {
  String errorMessage() {
    return 'Unable to Send Email';
  }
}

class NoPermissionException implements Exception {
  String errorMessage() {
    return 'missing_permissions';
  }
}

class AdminException implements Exception {
  String errorMessage() {
    return 'missing_admin_role';
  }
}
