import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:openid_client/openid_client_io.dart';
import 'package:url_launcher/url_launcher.dart';

Future<Map<String, dynamic>> mitidCallback() async {
  final issuer = await Issuer.discover(Uri.parse(APPLIC.criiptoUrl));
  final client = Client(issuer, APPLIC.criiptoClientId);

  final authenticator = Authenticator(client,
      scopes: ['openid'], port: 4000, urlLancher: urlLauncher);

  final c = await authenticator.authorize();

  closeInAppWebView();

  return c.toJson();
}

Future<void> urlLauncher(String url) async {
  await launchUrl(Uri.parse(url), mode: LaunchMode.inAppWebView);
}
