import 'package:flutter/material.dart';

class SmartGaps {
  //

  //[VERTICAL GAPS]
  static const gapH2 = SizedBox(height: 2);
  static const gapH4 = SizedBox(height: 4);
  static const gapH5 = SizedBox(height: 5);
  static const gapH6 = SizedBox(height: 6);
  static const gapH7 = SizedBox(height: 7);
  static const gapH8 = SizedBox(height: 8);
  static const gapH10 = SizedBox(height: 10);
  static const gapH11 = SizedBox(height: 11);
  static const gapH12 = SizedBox(height: 12);
  static const gapH13 = SizedBox(height: 13);
  static const gapH14 = SizedBox(height: 14);
  static const gapH15 = SizedBox(height: 15);
  static const gapH16 = SizedBox(height: 16);
  static const gapH18 = SizedBox(height: 18);
  static const gapH20 = SizedBox(height: 20);
  static const gapH24 = SizedBox(height: 24);
  static const gapH25 = SizedBox(height: 25);
  static const gapH30 = SizedBox(height: 30);
  static const gapH40 = SizedBox(height: 40);
  static const gapH45 = SizedBox(height: 45);
  static const gapH50 = SizedBox(height: 50);
  static const gapH60 = SizedBox(height: 60);
  static const gapH65 = SizedBox(height: 65);
  static const gapH67 = SizedBox(height: 67);
  static const gapH80 = SizedBox(height: 80);
  static const gapH100 = SizedBox(height: 100);
  static const gapH140 = SizedBox(height: 140);

  //[HORIZONTAL GAPS]
  static const gapW2 = SizedBox(width: 2);
  static const gapW5 = SizedBox(width: 5);
  static const gapW8 = SizedBox(width: 8);
  static const gapW10 = SizedBox(width: 10);
  static const gapW12 = SizedBox(width: 12);
  static const gapW14 = SizedBox(width: 14);
  static const gapW15 = SizedBox(width: 15);
  static const gapW20 = SizedBox(width: 20);
  static const gapW30 = SizedBox(width: 30);
  static const gapW33 = SizedBox(width: 33);

  //
}
