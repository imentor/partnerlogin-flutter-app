import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class CustomCurrencyFormatter extends TextInputFormatter {
  final CurrencyTextInputFormatter currencyFormatter;
  final bool isPositive;

  CustomCurrencyFormatter({required this.isPositive})
      : currencyFormatter = CurrencyTextInputFormatter(
          NumberFormat.currency(
            locale: 'da',
            decimalDigits: 2,
            symbol: '',
          ),
        );

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    TextEditingValue formattedValue =
        currencyFormatter.formatEditUpdate(oldValue, newValue);

    String newText = formattedValue.text;
    if (!isPositive && newText.isNotEmpty && !newText.startsWith('-')) {
      newText = '-$newText';
    }

    return formattedValue.copyWith(
      text: newText,
      selection: TextSelection.collapsed(offset: newText.length),
    );
  }
}
