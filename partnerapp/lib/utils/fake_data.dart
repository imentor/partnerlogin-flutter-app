import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/model/marketplace/marketplace_v2_response_model.dart';

final fakeData = FakeDatas();

class FakeDatas {
  final partnerJob = PartnerJobModel(
    customerName: 'Customer Name',
    projectName: 'Random Project Name',
    status: [
      JobStatus(id: 1, titleDk: 'Ny opgave', titleEn: 'New task'),
      JobStatus(id: 2, titleDk: 'I dialog', titleEn: 'In negotiation'),
    ],
  );

  final marketplaceItem = MarketPlaceV2Items(
    name: 'Some lengthy name - you have that will be used for partner jobs',
    description: 'Some lengthy dummy description for partner jobs',
  );
}
