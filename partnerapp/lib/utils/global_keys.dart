import 'package:flutter/material.dart';

MyGlobalKeys myGlobals = MyGlobalKeys();

class MyGlobalKeys {
  GlobalKey? _homeSaffoldKey;
  GlobalKey<ScaffoldState>? _innerScreenScaffoldKey;
  GlobalKey<NavigatorState>? _mainNavigatorKey;
  GlobalKey<NavigatorState>? _drawerRouteKey;
  // GlobalKey _loginSaffoldKey;

  MyGlobalKeys() {
    _homeSaffoldKey = GlobalKey();
    _mainNavigatorKey = GlobalKey<NavigatorState>();
    _drawerRouteKey = GlobalKey<NavigatorState>();
    _innerScreenScaffoldKey = GlobalKey<ScaffoldState>();
    // _loginSaffoldKey = GlobalKey();
  }
  GlobalKey? get homeScaffoldKey =>
      _homeSaffoldKey; //used for the rest of the app
  // GlobalKey get loginScaffoldKey =>s
  //     _loginSaffoldKey;
  GlobalKey<NavigatorState>? get mainNavigatorKey => _mainNavigatorKey;
  GlobalKey<NavigatorState>? get drawerRouteKey => _drawerRouteKey;
  GlobalKey<ScaffoldState>? get innerScreenScaffoldKey =>
      _innerScreenScaffoldKey;
}
