class Caster {
  static T? cast<T>(x) => x is T ? x : null;
}
