import 'dart:convert';

import 'package:Haandvaerker.dk/_config/env.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class APPLIC {
  bool useBitrixApiMaster = false;

  // List of supported languages
  final List<String> supportedLanguages = ['da', 'en'];

  static String predefLanguage =
      kDebugMode ? Language.english : Language.danish;

  // Returns the list of supported Locales
  Iterable<Locale> supportedLocales() =>
      supportedLanguages.map<Locale>((lang) => Locale(lang));

  //SETTING ENV
  static late Map<String, dynamic> _config;

  /// Initialize app_config.json file
  Future<void> initialize(String env) async {
    String configString = '';
    switch (env) {
      case Environment.dev:
        configString =
            await rootBundle.loadString('config/app_config_dev.json');
        break;
      case Environment.prod:
        configString =
            await rootBundle.loadString('config/app_config_prod.json');
        break;
      default:
        configString =
            await rootBundle.loadString('config/app_config_dev.json');
        break;
    }
    _config = json.decode(configString) as Map<String, dynamic>;
  }

  String get env => _config['env'] as String;

  String get bitrixApiUrl => _config['bitrix_api'] as String;

  String get firebaseAndroidApiKey =>
      _config['firebase_android_api_key'] as String;

  String get firebaseAndroidAppId =>
      _config['firebase_android_app_id'] as String;

  String get firebaseSenderId => _config['firebase_sender_id'] as String;

  String get firebaseDatabaseUrl => _config['firebase_database_url'] as String;

  String get firebaseStorageBucket =>
      _config['firebase_storage_bucket'] as String;

  String get firebaseIosApiKey => _config['firebase_ios_api_key'] as String;

  String get firebaseIosAppId => _config['firebase_ios_app_id'] as String;

  String get firebaseIosClientId => _config['firebase_ios_client_id'] as String;

  static String get criiptoUrl => _config['criipto_url'] as String;

  static String get criiptoClientId => _config['criipto_client_id'] as String;
  //_config['bitrix_api'] as String;

  ///
  /// Internals
  ///
  static final APPLIC _applic = APPLIC._internal();

  static String? currentDrawerRoute = Routes.newsFeed;
  static String previousDrawerRoute = '';
  static CancelToken cancelToken = CancelToken();
  static bool isInForeground = true;

  factory APPLIC() {
    return _applic;
  }

  APPLIC._internal();
}

APPLIC applic = APPLIC();
