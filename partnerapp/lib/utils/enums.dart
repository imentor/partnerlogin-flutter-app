enum Position { left, right, first, last }

enum NavigateRegisterType { back, next, done }

enum MailBoxType { inbox, drafts, sent, trash, junk }
