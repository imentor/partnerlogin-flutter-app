import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:math' as math;
import 'dart:ui' as ui;

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:image/image.dart' as img;
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';

enum DateFormatType {
  standardDate('dd. MMM yyyy'), // e.g., 25. Sep 2024
  twelveHourTime("hh:mm a"), // e.g., 02:30 PM
  dateTimeWithTwelveHour('dd. MM yyyy hh:mm a'), // e.g., 25. 09 2024 02:30 PM
  fullMonthDate('dd. MMMM yyyy'), // e.g., 25. September 2024
  hourMinuteTime("hh:mm"), // e.g., 02:30
  dateTimeWith24Hour('dd. MM yyyy HH:mm'), // e.g., 25. 09 2024 14:30
  isoDate('yyyy-MM-dd'), // e.g., 2024-09-25
  dateTimeWithAmPm('yyyy-MM-dd hh:mm a'), // e.g., 2024-09-25 02:30 PM
  eventDateTime("dd. MMM yyyy hh:mm a"), // e.g., 25. Sep 2024 02:30 PM
  fullMonthYearDate('dd MMMM yyyy'), // e.g., 25 September 2024
  descriptiveDate('MMMM dd, yyyy'), // e.g., September 25, 2024
  europeanDate('dd. MM. yyyy'), // e.g., 25. 09. 2024
  transactionDetailDateTime(
      'd MMMM yyyy - H.mm'), // e.g., 25 September 2024 - 14.30
  usDateFormat('MM/dd/yyyy'), // e.g., 09/25/2024
  shortDateInternational('dd/MM/yyyy'), // e.g., 25/09/2024
  standardDate2('dd.MM.yyyy'); // e.g., 25. 9 2024

  final String pattern;

  const DateFormatType(this.pattern);

  DateFormat get formatter => DateFormat(pattern);
}

class Formatter {
  static String formatDateTime({required DateFormatType type, DateTime? date}) {
    if (date == null) return '';

    try {
      return type.formatter.format(date);
    } catch (e) {
      return '';
    }
  }

  static String formatDateStrings(
      {required DateFormatType type, String? dateString}) {
    if ((dateString ?? '').isEmpty) return '';

    try {
      final date = parsedDates(dateString!);
      if (date == null) return '';

      return type.formatter.format(date);
    } catch (e) {
      return '';
    }
  }

  static DateTime? parsedDates(String dateString) {
    for (final format in DateFormatType.values) {
      try {
        final formattedString = format.formatter.parse(dateString);

        final noMillisecondsFormat =
            DateFormat(format.pattern.replaceAll('.SSS', ''));
        final reformattedString = noMillisecondsFormat.format(formattedString);
        if (reformattedString == dateString.split(' ').first) {
          return formattedString;
        }
      } catch (e) {
        //
      }
    }
    return null;
  }

  static String getBaseName(File file) {
    return p.basename(file.path);
  }

  static Image imageFromBase64String(
      String base64String, double? height, double? width,
      {String prefix = ''}) {
    var mPrefix = prefix;
    try {
      final bStr = base64String.substring(mPrefix.length);
      return Image.memory(
        base64Decode(bStr),
        height: height,
        width: width,
      );
    } on FormatException catch (e) {
      log("$e");
      mPrefix = "data:image/png;base64,";
      final bStr = base64String.substring(mPrefix.length);
      return Image.memory(
        base64Decode(bStr),
        height: height,
        width: width,
      );
    }
  }

  static Future<String> fileToBase64String(File file) async {
    final imageBytes = await file.readAsBytes();
    final String base64Image = base64Encode(imageBytes);
    return base64Image;
  }

  Uint8List dataFromBase64String(String base64String) {
    return base64Decode(base64String);
  }

  String base64String(Uint8List data) {
    return base64Encode(data);
  }

  static Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
  }

  static Future<String> createImageFileFromString(
      {required prename, required uniqueId, required String encodedStr}) async {
    //var prefix = "data:image/jpg;base64,";
    //var bStr = encodedStr.substring(prefix.length);
    Uint8List bytes = base64.decode(encodedStr);
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = File("$dir/$prename$uniqueId.jpg");
    await file.writeAsBytes(bytes);
    return file.absolute.path;
  }

  static Future<String> createPdfFileFromString(
      {required prename, required uniqueId, required String encodedStr}) async {
    //var prefix = "data:image/jpg;base64,";
    //var bStr = encodedStr.substring(prefix.length);
    final byteData = await rootBundle.load(encodedStr);

    Directory appDocDir = await getApplicationDocumentsDirectory();
    String imagesAppDirectory = appDocDir.path;
    final file =
        await File('$imagesAppDirectory/$encodedStr').create(recursive: true);

    await file.writeAsBytes(byteData.buffer
        .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

    // Uint8List bytes = base64.decode(encodedStr);
    // String dir = (await getApplicationDocumentsDirectory()).path;
    // File file = File("$dir/$prename$uniqueId.pdf");
    // await file.writeAsBytes(bytes);
    return file.absolute.path;
  }

  static Future<File> createFileFromBytes(
      Uint8List bytes, String name, String ext) async {
    String dir = (await getApplicationDocumentsDirectory()).path;
    String uniqueId = math.Random().nextInt(100).toString();
    final newName = '$name$uniqueId.$ext';
    File file = File("$dir/$newName");
    await file.writeAsBytes(bytes);
    return file;
  }

  static Future<File> returnFileFromString(
      {required prename, required uniqueId, required String encodedStr}) async {
    //var prefix = "data:image/jpg;base64,";
    //var bStr = encodedStr.substring(prefix.length);
    Uint8List bytes = base64.decode(encodedStr);
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = File("$dir/$prename$uniqueId.jpg");
    await file.writeAsBytes(bytes);
    return file;
  }

  static String getConcatinatedPrefix(String fullString) {
    String bStr = '';

    if (fullString.contains("data:image/jpg;base64,")) {
      const prefix = "data:image/jpg;base64,";
      bStr = fullString.substring(prefix.length);
    } else if (fullString.contains("data:image/png;base64,")) {
      const prefix = "data:image/png;base64,";
      bStr = fullString.substring(prefix.length);
    } else if (fullString.contains("data:image/jpeg;base64,")) {
      const prefix = "data:image/jpeg;base64,";
      bStr = fullString.substring(prefix.length);
    }

    return bStr;
  }

  static Future<String?> networkImageToBase64(String? imageUrl) async {
    final http.Response response = await http.get(Uri.parse(imageUrl!));
    final bytes = response.bodyBytes;
    return base64Encode(bytes);
  }

  static Future<File> compressImageToRightSize(File file) async {
    // Read the image file as bytes
    Uint8List imageBytes = await file.readAsBytes();

    // Decode the image
    img.Image? image = img.decodeImage(imageBytes);

    if (image == null) {
      throw Exception("Failed to decode image.");
    }

    // Check if resizing is necessary
    if (image.height < 2000 && image.width < 2000) {
      return file;
    }

    // Calculate new dimensions
    int targetWidth = 1280;
    int targetHeight = (image.height * 1280 / image.width).round();

    // Resize the image
    img.Image resizedImage =
        img.copyResize(image, width: targetWidth, height: targetHeight);

    // Encode the resized image
    Uint8List resizedBytes =
        Uint8List.fromList(img.encodeJpg(resizedImage, quality: 100));

    // Create a new file for the resized image
    File compressedFile =
        File(file.path.replaceFirst('.jpg', '_compressed.jpg'));
    await compressedFile.writeAsBytes(resizedBytes);

    return compressedFile;
  }

  static Future<double> getAspectRatio(File file) async {
    // Read the image file as bytes
    Uint8List imageBytes = await file.readAsBytes();

    // Decode the image
    img.Image? image = img.decodeImage(imageBytes);

    if (image == null) {
      throw Exception("Failed to decode image.");
    }

    // Get the width and height of the image
    int width = image.width;
    int height = image.height;

    // Calculate and return the aspect ratio
    return width / height;
  }

  static String getSlash(String original) {
    return original.replaceAll(RegExp('/'), '');
  }

  static DateTime addMonths(DateTime date, int monthsToAdd) {
    int newMonth = date.month + monthsToAdd;
    int newYear = date.year;

    // Adjust the year and month to ensure the month doesn't exceed 12
    if (newMonth > 12) {
      newYear += (newMonth - 1) ~/ 12;
      newMonth = newMonth % 12;
      if (newMonth == 0) {
        newMonth = 12;
        newYear -= 1;
      }
    }

    // Correct usage of newYear and newMonth
    return DateTime(newYear, newMonth, date.day);
  }

  static String curencyFormat(
      {required num amount, bool withoutSymbol = false, String sign = ''}) {
    if (amount == 0) {
      return '00 ${withoutSymbol ? '' : 'kr.'}';
    }

    switch (sign) {
      case 'positive':
        sign = '+ ';
        break;
      case 'negative':
        sign = '- ';
        break;
    }

    return '$sign${NumberFormat.currency(locale: 'da', symbol: withoutSymbol ? '' : 'kr.', decimalDigits: amount == 0 ? 0 : 2).format(amount)}';
  }

  static String sanitizeCurrency(String input) {
    String sanitized = input.replaceAll(RegExp(r'[^\d,\.]'), '');

    if (sanitized.contains(',') && sanitized.contains('.')) {
      sanitized = sanitized.replaceAll(',', '');
    } else if (sanitized.contains(',')) {
      sanitized = sanitized.replaceAll(',', '.');
    }
    return sanitized;
  }

  static String sanitizeCurrencyFromFormatter(String input) {
    // Allow both Danish and non-Danish formats
    final regex = RegExp(r'^(\d{1,3}(\.\d{3})*(,\d{1,2})?)|(\d+(.\d{1,2})?)$');

    if (!regex.hasMatch(input)) {
      throw FormatException('Invalid currency format: $input');
    }

    if (input.contains(',') && input.contains('.')) {
      // Strictly Danish format
      return input.replaceAll('.', '').replaceAll(',', '.');
    } else if (input.contains('.')) {
      // Non-Danish format like `12000.00`
      return input; // Already correct
    } else if (input.contains(',')) {
      // Danish format
      return input.replaceAll('.', '').replaceAll(',', '.');
    }

    throw FormatException('Unexpected currency format: $input');
  }
}

class CurrencyInputFormatter extends TextInputFormatter {
  CurrencyInputFormatter({this.currentLocale});

  final String? currentLocale;

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    double value = double.parse(newValue.text);

    //final String defaultLocale = Platform.localeName;

    //final formatter = NumberFormat.simpleCurrency(locale: currentLocale);
    final formatter = NumberFormat("#,##0.00", currentLocale);

    String newText = formatter.format(value / 100);

    return newValue.copyWith(
        text: newText,
        selection: TextSelection.collapsed(offset: newText.length));
  }
}
