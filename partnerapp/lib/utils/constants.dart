class Strings {
  static const String appName = 'Håndværker.dk PartnerLogin';
  static const String kundematchTelNumber = '7022 8014';
  static const String creditCardTelNumer = '70228014';
  static const String auth = 'salesPerson';
  static const String zendeskAccountKey = '4WGnAq4BouiYmiDk1YmcjiPvDtsOaXEw';
  static const String uxcamKey = 'kni0ujrxhty44qu';
  //'AIzaSyD6CQqAg0q-4oPNatzPqc35LmH5BUNR-Ok';
  static const String testGoogleMapKey =
      'AIzaSyDTbyft_7dEVpd-0PxhlK8MaAO0hGSNk1U';
}

class Links {
  static const String appStoreUrl = 'https://apps.apple.com/app/id1529848721';
  static const String playStoreUrl =
      'https://play.google.com/store/apps/details?id=dk.haandvaerker.partnerapp';
  static const String sentryDsn =
      'https://22c1cad2f8b64df786ccf0920c25bba5@o177088.ingest.sentry.io/5202283';
  static const String baseUrl =
      'https://www.kundematch.dk/PartnerMobileService.php';
  static const String testUrl =
      'https://jsonplaceholder.typicode.com/todos'; //testApi
  static const String registerUrl =
      'https://www.xn--hndvrker-9zan.dk/om/kontakt-os?ref=pm';
  static const String viewImageUrl =
      'https://www.kundematch.dk/HenttilbudImg.php?img=';
  static const String uploadTaskUrl =
      'https://www.kundematch.dk/crm/libraries/productform/product.php';
  static const String creditCardUrl =
      'http://www.kundematch.dk/newpartner/kredit.php?id=';
  static const String creditCardHaaUrl =
      'http://www.kundematch.dk/newpartner/kredithaa.php?id=';
  static const String baseDownloadUrl = 'https://www.kundematch.dk/newpartner/';
  static const String subscriptionAudioDownloadUrl =
      'https://www.kundematch.dk/newpartner/audio.php?id=';
  static const String subscriptionAppointmentDownloadUrl =
      'https://www.kundematch.dk/newpartner/downloada.php?id=';
  static const String cloudinaryBase =
      'https://res.cloudinary.com/kundematch/image/upload/';
  static const String termsAndConditionsLink =
      'https://www.håndværker.dk/om/betingelser?ref=pm';
  static const String widgetImgLink =
      'https://widget.haandvaerker.dk/microsites/ratingnew.php?id=';
  static const String agirasRedirect =
      'https://www.ageras.dk/revisor-byggebranchen?utm_source=haandvaerkerdk&utm_medium=affiliate&utm_content=haandvaerkerdk';
  static const String newCrmKundematch = 'https://newcrm.kundematch.dk';
  static const String paymentValidation =
      'https://www.kundematch.dk/crm/libraries/productform/product.php?mode=checkinvoice&addsid=';
  static const String bitrixChatWidget =
      'https://widget.haandvaerker.dk/microsites/chat.php?';
  static const String offerImageUrl = 'https://www.xn--hndvrker-9zan.dk/';
  static const String termsOfUseUrl =
      "https://www.haandvaerker.dk/privatlivspolitik";
  static const String privacyPolicyUrl =
      "https://www.haandvaerker.dk/privatlivspolitik";
  static const String masterCrm = "https://mastercrm.haandvaerker.dk";
  static const String bitrixProdBaseUrl =
      "https://bitrixapi.haandvaerker.dk/v1";
  static const String bitrixDevBaseUrl =
      "https://bitrixapidev.haandvaerker.dk/v1";
  static const String bitrixMasterBaseUrl =
      "https://bitrixapimaster.haandvaerker.dk/v1";
  static const String hvdkBusiness =
      "https://www.haandvaerker.dk/business?ref=bm";
  static const String prisenImageUrl =
      "https://www.kundematch.dk/static/img/det-perfekte-match.webp";
  static const String createOfferVideo =
      "https://www.haandvaerker.dk/video/afgiv_tilbud.mp4";
}

class ProjectInvitationStatus {
  static const String accepted = 'accepted';
  static const String pending = 'pending';
  static const String rejected = 'rejected';
}

class ContractCommentStatus {
  static const String approved = 'approved';
  static const String pending = 'pending';
  static const String declined = 'declined';
}

class ExtraActions {
  static const String refresh = 'refresh';
  static const String newMessage = 'new_message';
  static const String adBoosting = 'ad_boosting';
}

class PaymentStageStatus {
  static const String paid = 'paid';
  static const String pending = 'pending';
  static const String partiallyPaid = 'partially_paid';
  static const String requested = 'requested';
  static const String declined = 'declined';
  static const String ongoing = 'ongoing';
}

class Language {
  static const String english = 'English';
  static const String danish = 'Danish';
}

class ImagePaths {
  static const String wideLogo = 'assets/images/new-haandvaerker-logo.png';
  static const String logo = 'assets/images/h.png';
  static const String imgPlaceholder = 'assets/images/imgplaceholder.png';
  static const String hired = 'assets/images/hired.png';
  static const String files = 'assets/images/files.png';
  static const String target = 'assets/images/target.png';
  static const String photo1 = 'assets/images/photo1.png';
  static const String photo2 = 'assets/images/photo2.png';
  static const String shareFbPhoto = 'assets/images/Sharefbpng.jpeg';
  static const String dinero = 'assets/images/recommendations/dinero.svg';
  static const String vismaeconomic = 'assets/images/recommendations/visma.svg';
  static const String billy = 'assets/images/recommendations/billy.svg';
  static const String singleHouse = 'assets/images/single-house.png';
  static const String singleHouseGrey = 'assets/images/single-house-grey.png';
  static const String shield = 'assets/images/shield.png';
  static const String halfHouse = 'assets/images/half-house.png';
  static const String picturePrimaryColor =
      'assets/images/picture-primarycolor.png';
  static const String arrowSquareUpRight =
      'assets/images/arrow-square-up-right.svg';
  static const String shutter = 'assets/images/ic_shutter_1.png';
  static const String switchCamera = 'assets/images/ic_switch_camera_3.png';
  static const String unit = 'assets/images/mobile.png';
  static const String tinyEdit = 'assets/images/edit.png';
  static const String discount = 'assets/images/discount.png';
  static const String handshake = 'assets/images/handshake.png';
  static const String document = 'assets/images/document.png';
  static const String picture = 'assets/images/picture.png';
  static const String gear = 'assets/images/gear.png';
  static const String houseLogo = 'assets/images/img_1.png';
  static const String haandvaerkerDk = 'assets/images/img_0.png';
  static const String sampleProjectFbAd = 'assets/images/sample_project_ad.png';
  static const String sampleProfileFbAd =
      'assets/images/mobile_profile_sample.png';
  static const String ordrestyringLogo = 'assets/images/ordrestyring.png';
  static const String sampleReviewFbAd =
      'assets/images/sample_facebook_review_boost.png';
  static const String applepay = 'assets/images/ApplePay-Subscribe.png';
  static const String sampleImageCover = 'assets/images/default_image.jpeg';
}

class DocumentPaths {
  static const String dineroIntegrationPdf =
      'assets/documents/Dinero_vejledning.pdf';
  static const String billyIntegrationPdf =
      'assets/documents/Integrate_Billy_with_hvdk-page-001-converted-merged.pdf';

  // 'assets/documents/Billy_vejledning.pdf';
  static const String economicIntegrationPdf =
      'assets/documents/Economic_vejledning.pdf';
  static const String facebookIntegrationPdf =
      'assets/documents/Facebook_vejledning_integration.pdf';
}

class NetworkPaths {
  static const String dinerVideo =
      'https://www.youtube.com/watch?v=kbznMBh-LTs&feature=youtu.be';
  static const String dineroPage = 'https://dinero.dk/';
  static const String economicPage =
      'https://secure.e-conomic.com/secure/api1/requestaccess.aspx?appPublicToken=WZr2juvCWXAt4gMuou0RTXvMR0BNFpfJHsnq3M2MQbw1';
  static const String billyPage = 'https://www.billy.dk/';
}

class SvgIcons {
  // fresh icons
  static const String menuIconOpen = 'assets/images/menu_icon_open.svg';
  static const String menuIconClose = 'assets/images/menu_icon_close.svg';
  static const String payproffIcon = 'assets/images/payproff_logo.svg';
  static const String createOfferIcon = 'assets/images/create_offer_icon.svg';
  static const String partnerIcon = 'assets/images/partner.svg';
  static const String producentIcon = 'assets/images/producent_icon.svg';
  static const String paidExchange = 'assets/images/paid_exchange.svg';
  static const String marketplace = 'assets/images/marketplace.svg';
  static const String phoneCall = 'assets/images/phone_call.svg';
  static const String accountUser = 'assets/images/account_user.svg';
  static const String homeV2 = 'assets/images/home_v2.svg';
  static const String infoV2 = 'assets/images/info_v2.svg';
  static const String logout = 'assets/images/logout.svg';
  static const String customer = 'assets/images/customer.svg';
  static const String settings = 'assets/images/settings.svg';
  static const String mainContractSolidLock =
      'assets/images/main_contract_solid_lock.svg';
  static const String professionalContractSolidLock =
      'assets/images/professional_contract_solid_lock.svg';
  static const String lockIcon = 'assets/images/lock_icon.svg';
  static const String calendarCheck = 'assets/images/calendar_check.svg';
  static const String calendar = 'assets/images/calendar-with-dates.svg';
  static const String mapPin = 'assets/images/map-pin.svg';
  static const String profile = 'assets/images/profile_pic.svg';
  static const String appBarMessage = 'assets/images/appBarMessage.svg';
  static const String manageProject = 'assets/images/manage_project.svg';
  static const String offersCard = 'assets/images/offers_card.svg';
  static const String calculator = 'assets/images/calculator-dark.svg';
  static const String profileImg = 'assets/images/profile_img.svg';
  static const String partnerJobIcon = 'assets/images/partner_job_icon.svg';
  static const String feedbackIconUrl = 'assets/images/feedback.svg';
  static const String ecommerceIconUrl = 'assets/images/ecommerce.svg';
  static const String billIconUrl = 'assets/images/bill.svg';
  static const String homeIconUrl = 'assets/images/home.svg'; // used
  static const String fullLogoUrl =
      'assets/images/new-haandvaerker-logo.svg'; // used
  static const String sendIconUrl = 'assets/images/send.svg'; // used
  static const String mailIconUrl = 'assets/images/envelope.svg'; // used
  static const String customersIconUrl = 'assets/images/inbox.svg'; // used
  static const String newMenuIconUrl = 'assets/images/align-left.svg'; // used
  static const String menuBarsIconUrl = 'assets/images/menuBars.svg'; // used
  static const String recommendationsIconUrl =
      'assets/images/j-star.svg'; // used
  static const String projectsIconUrl = 'assets/images/j-picture.svg'; // used
  static const String marketplaceIconUrl =
      'assets/images/credit-card.svg'; // used
  static const String mesterIconUrl = 'assets/images/share-alt-f.svg'; // used
  static const String websiteIconUrl = 'assets/images/computer.svg'; // used
  static const String settingsIconUrl = 'assets/images/cog.svg'; // used
  static const String faqIconUrl = 'assets/images/j-info.svg'; // used
  static const String logoutIconUrl = 'assets/images/log-out.svg'; // used
  static const String handshakeIconUrl = 'assets/images/deal.svg'; // used
  static const String chatIconUrl = 'assets/images/chat.svg'; // used
  static const String phoneIconUrl = 'assets/images/phone.svg'; // used
  static const String calendarIconUrl = 'assets/images/calendar-alt.svg';
  static const String pricetagIconUrl = 'assets/images/price-alt.svg';
  static const String opgaveContentIconUrl = 'assets/images/check-list.svg';
  static const String createOfferIconUrl = 'assets/images/curriculum.svg';
  static const String listIconUrl = 'assets/images/list.svg';
  static const String shieldIconUrl = 'assets/images/shield.svg';
  static const String sitemapIconUrl = 'assets/images/sitemap.svg';
  static const String singleHIconUrl = 'assets/images/h.svg';
  static const String speakerIconUrl = 'assets/images/speaker.svg';
  static const String choosingIconUrl = 'assets/images/choosing.svg';
  static const String galleriesIconUrl = 'assets/images/galleries.svg';
  static const String ratingIconUrl = 'assets/images/rating-ico.svg';
  static const String affiliateIconUrl = 'assets/images/affiliate.svg';
  static const String uploadImage = 'assets/images/uploadImage.svg';
  static const String pictureIcon = 'assets/images/picture.svg';
  static const String qualityIcon = 'assets/images/quality.svg';
  static const String documentsIcon = 'assets/images/documents-blue.svg';
  static const String taskListIcon = 'assets/images/task-list.svg';
  static const String singleHouse = 'assets/images/single-house.svg';
  static const String singleHouseGrey = 'assets/images/single-house-grey.svg';
  static const String plant = 'assets/images/plant.svg';
  static const String garbage = 'assets/images/garbage.svg';
  static const String flash = 'assets/images/flash.svg';
  static const String co2 = 'assets/images/co2.svg';
  static const String reportFeatherFlag = 'assets/images/feather-flag.svg';
  static const String applepay = 'assets/images/ApplePay-Subscribe.svg';
  static const String educationIconBlack =
      'assets/images/educationIconBlack.svg';
  static const String educationIconBlue = 'assets/images/educationIconBlue.svg';
  static const String miljoIcon = 'assets/images/miljo-green.svg';
  static const String okonomiIcon = 'assets/images/okonomi-green.svg';
  static const String socialIcon = 'assets/images/social-green.svg';
  // static String newMenuIconUrl = 'assets/images/menuBars.svg';
  static const String wallet = 'assets/images/wallet.svg';
  static const String calendarEvents = 'assets/images/calendar-events.svg';
  static const String timeline = 'assets/images/timeline.svg';
  static const String projectMessages = 'assets/images/project-messages.svg';
  static const String projectFiles = 'assets/images/project-files.svg';
  static const String offers = 'assets/images/offers.svg';
  static const String viewContract = 'assets/images/view-contract.svg';
  static const String requestPayout = 'assets/images/request-payout.svg';
  static const String rateYourCustomer = 'assets/images/rate_your_customer.svg';
  static const String forwardArrowIcon = 'assets/images/forward.svg';
  static const String replyArrowIcon = 'assets/images/forward.svg';
  static const String extraWorkEdit = 'assets/images/extra_work_edit.svg';
  static const String extraWorkIcon = 'assets/images/addon.svg';
  static const String airtoxPromo = 'assets/images/airtoxPromo.png';
  static const String walletDkk = 'assets/images/wallet_ddk.svg';
  static const String photoDoc = 'assets/images/photo_doc.svg';
  static const String deficiency = 'assets/images/deficiency.svg';
  static const String awaiting = 'assets/images/awaiting.svg';
  static const String tenderFolder = 'assets/images/tender_folder.svg';
  static const String questionCirlce = 'assets/images/question-mark.svg';

  static const String docFileFormat = 'assets/images/docx_file_format.svg';
  static const String pdfFileFormat = 'assets/images/pdf_file_format.svg';
  static const String imageFileFormat = 'assets/images/image_file_format.svg';
  static const String excelFileFormat = 'assets/images/xlsx_file_format.svg';

  static const String deletedCustomer = 'assets/images/deleted_customer.svg';
  static const String writeReview = 'assets/images/write_feedback.svg';
}

class Keys {
  static const String loginEmailTF = 'email_login';
  static const String loginPasswordTF = 'email_password';
  static const String wishTF = 'wish_field';
  static const String successBackButton = 'success_back_button';
  static const String getRecommName = 'get_recommendation_name';
  static const String getRecomEmail = 'get_recommendation_email';
  static const String getRecomPhone = 'get_recommendation_phone';
  static const String getRecomScrollview = 'get_recommendation_scrollview';
  static const String getRecomButton = 'get_recommendation_send_button';
  static const String readAndAnswerScrollView = 'read_answer_scroll_view';
  static const String autoPilotScrollView = 'auto_pilot_scroll_view';
  static const String bannersScrollView = 'banners_scroll_view';
  static const String dineroRecommendationKey = 'dinero_recommendation';
  static const String economicRecommendationKey = 'economic_recommendation';
  static const String billyRecommendationKey = 'billy_recommendation';
  static const String customersScrollView = 'customer_scroll_view';
  static const String myProjectsScrollView = 'my_project_scroll_view';
  static const String marketplaceScrollView = 'marketplace_scroll_view';
  static const String membershipBenefitsScrollView =
      'membership_benefits_scroll_view';
  static const String badgeScrollView = 'badge_scroll_view';
  static const String freemium = 'freemium';
}

class PackageHandle {
  static const String freemium = 'freemium';
  static const String vaekst = 'vaekst';
}

class VideoPaths {
  static const String membershipBenefitsBanner =
      'assets/videos/membership_benefits_banner.mp4';
}
