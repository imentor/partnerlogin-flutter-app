import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image/image.dart' as img;
import 'package:image_editor_plus/image_editor_plus.dart';
import 'package:image_editor_plus/options.dart';
import 'package:path/path.dart' as path;

const double maxFileSizeInMB = 500.0;

/// Convert files to [FormData] with an optional [key].
Future<FormData> filesToFormData(List<File> files, String key) async {
  final formData = FormData();
  final filemap = await Future.wait([
    ...files.map((f) async {
      final fname = f.path.split('/').last;
      return MultipartFile.fromFileSync(f.path, filename: fname);
    })
  ]);

  for (final entry in filemap) {
    formData.files.add(MapEntry(key, entry));
  }

  return formData;
}

bool isUnderMaxImageSize(File image) {
  int fileSizeInBytes = image.lengthSync();
  double fileSizeInMB = fileSizeInBytes / (1024 * 1024);

  return fileSizeInMB < maxFileSizeInMB;
}

Future<File?> resizeImage(File image) async {
  try {
    Uint8List imageBytes = await image.readAsBytes();
    img.Image? originalImage = img.decodeImage(imageBytes);

    if (originalImage != null) {
      img.Image resizedImage = img.copyResize(originalImage,
          width: 1024); // Resize width to 1024, maintaining aspect ratio
      File resizedFile =
          await File(image.path).writeAsBytes(img.encodeJpg(resizedImage));
      return resizedFile;
    }

    return null;
  } catch (e) {
    return null;
  }
}

Future<File?> editImage(
    {required BuildContext context,
    required Uint8List imageData,
    required String path}) async {
  ///

  final editedImage = await Navigator.push(
    context,
    MaterialPageRoute(
      builder: (context) => ImageEditor(
        image: imageData,
        imagePickerOption: const ImagePickerOption(
          captureFromCamera: false,
          pickFromGallery: false,
        ),
      ),
    ),
  );

  if (editedImage != null) {
    return await convertToFile(data: editedImage, path: path);
  } else {
    return null;
  }

  ///
}

Future<File> convertToFile(
    {required Uint8List data, required String path}) async {
  File file = await File(path).create();
  file.writeAsBytesSync(data);

  return file;
}

bool isImageFile(String filePath) {
  final imageExtensions = ['.jpg', '.jpeg', '.png', '.gif', '.bmp', '.webp'];
  return imageExtensions.any((ext) => filePath.toLowerCase().endsWith(ext));
}

bool isDocumentFile(String filePath) {
  final documentExtensions = [
    '.pdf',
    '.doc',
    '.docx',
    '.xls',
    '.xlsx',
    '.ppt',
    '.pptx',
    '.txt',
    '.rtf',
    '.odt',
    '.ods',
    '.odp'
  ];
  return documentExtensions.any((ext) => filePath.toLowerCase().endsWith(ext));
}

String getNameFromFile(String filePath) {
  return path.basename(filePath);
}
