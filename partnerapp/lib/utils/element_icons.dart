// ignore_for_file: constant_identifier_names

import 'package:flutter/widgets.dart';

class ElementIcons {
  ElementIcons._();

  static const _kFontFam = 'ElementIcons';
  static const String? _kFontPkg = null;

  static const IconData platform_eleme =
      IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData lollipop =
      IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData ice_tea =
      IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData potato_strips =
      IconData(0xe803, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData ice_cream_square =
      IconData(0xe804, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData ice_cream_round =
      IconData(0xe805, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData coffee_cup =
      IconData(0xe806, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData hot_water =
      IconData(0xe807, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData water_cup =
      IconData(0xe808, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData view =
      IconData(0xe809, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData finished =
      IconData(0xe80a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData arrow_up =
      IconData(0xe80b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData key =
      IconData(0xe80c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData sunrise_1 =
      IconData(0xe80d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData sunrise =
      IconData(0xe80e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData medal_1 =
      IconData(0xe80f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData medal =
      IconData(0xe810, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData bell =
      IconData(0xe811, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData close_notification =
      IconData(0xe812, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData male =
      IconData(0xe813, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData female =
      IconData(0xe814, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData shopping_cart_full =
      IconData(0xe815, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData smoking =
      IconData(0xe816, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData pie_chart =
      IconData(0xe817, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData data_analysis =
      IconData(0xe818, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData takeaway_box =
      IconData(0xe819, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData paperclip =
      IconData(0xe81a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData caret_bottom =
      IconData(0xe81b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData date =
      IconData(0xe81c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData upload2 =
      IconData(0xe81d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData caret_top =
      IconData(0xe81e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData info =
      IconData(0xe81f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData remove =
      IconData(0xe820, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_goods =
      IconData(0xe821, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData user_solid =
      IconData(0xe822, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_claim =
      IconData(0xe823, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_check =
      IconData(0xe824, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_management =
      IconData(0xe825, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_home =
      IconData(0xe826, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_operation =
      IconData(0xe827, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData video_pause =
      IconData(0xe828, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData warning =
      IconData(0xe829, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData question =
      IconData(0xe82a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_release =
      IconData(0xe82b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_ticket =
      IconData(0xe82c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_promotion =
      IconData(0xe82d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_grid =
      IconData(0xe82e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_data =
      IconData(0xe82f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_unfold =
      IconData(0xe830, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_fold =
      IconData(0xe831, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_platform =
      IconData(0xe832, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_order =
      IconData(0xe833, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_opportunity =
      IconData(0xe834, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_custom =
      IconData(0xe835, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData video_play =
      IconData(0xe836, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_tools =
      IconData(0xe837, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_finance =
      IconData(0xe838, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData goods =
      IconData(0xe839, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_cooperation =
      IconData(0xe83a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData upload =
      IconData(0xe83b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_comment =
      IconData(0xe83c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_flag =
      IconData(0xe83d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData sort_up =
      IconData(0xe83e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_marketing =
      IconData(0xe83f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData c_scale_to_original =
      IconData(0xe840, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData sort_down =
      IconData(0xe841, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData delete_solid =
      IconData(0xe842, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData eleme =
      IconData(0xe843, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_help =
      IconData(0xe844, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_shop =
      IconData(0xe845, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData s_open =
      IconData(0xe846, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData circle_plus =
      IconData(0xe847, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData picture =
      IconData(0xe848, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData caret_right =
      IconData(0xe849, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData location =
      IconData(0xe84a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData caret_left =
      IconData(0xe84b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData milk_tea =
      IconData(0xe84c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData loading =
      IconData(0xe84d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData wind_power =
      IconData(0xe84e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData watch =
      IconData(0xe84f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData phone =
      IconData(0xe850, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData video_camera_solid =
      IconData(0xe851, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData star_on =
      IconData(0xe852, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData more =
      IconData(0xe853, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData share =
      IconData(0xe854, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData menu =
      IconData(0xe855, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData message_solid =
      IconData(0xe856, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData d_caret =
      IconData(0xe857, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData camera_solid =
      IconData(0xe858, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData success =
      IconData(0xe859, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData error =
      IconData(0xe85a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData folder_add =
      IconData(0xe85b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData folder_opened =
      IconData(0xe85c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData folder =
      IconData(0xe85d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData edit =
      IconData(0xe85e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData circle_close =
      IconData(0xe85f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData camera =
      IconData(0xe860, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData search =
      IconData(0xe861, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData zoom_in =
      IconData(0xe862, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData zoom_out =
      IconData(0xe863, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData monitor =
      IconData(0xe864, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData attract =
      IconData(0xe865, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData folder_remove =
      IconData(0xe866, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData folder_delete =
      IconData(0xe867, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData mobile =
      IconData(0xe868, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData video_camera =
      IconData(0xe869, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData folder_checked =
      IconData(0xe86a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData scissors =
      IconData(0xe86b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData tickets =
      IconData(0xe86c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData document_remove =
      IconData(0xe86d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData umbrella =
      IconData(0xe86e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData headset =
      IconData(0xe86f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData document_delete =
      IconData(0xe870, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData document_copy =
      IconData(0xe871, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData brush =
      IconData(0xe872, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData data_line =
      IconData(0xe873, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData document_checked =
      IconData(0xe874, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData mouse =
      IconData(0xe875, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData coordinate =
      IconData(0xe876, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData document =
      IconData(0xe877, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData printer =
      IconData(0xe878, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData download =
      IconData(0xe879, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData document_add =
      IconData(0xe87a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData magic_stick =
      IconData(0xe87b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData reading =
      IconData(0xe87c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData data_board =
      IconData(0xe87d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData no_smoking =
      IconData(0xe87e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData house =
      IconData(0xe87f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData table_lamp =
      IconData(0xe880, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData guide =
      IconData(0xe881, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData help =
      IconData(0xe882, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData turn_off_microphone =
      IconData(0xe883, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData microphone =
      IconData(0xe884, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData user =
      IconData(0xe885, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData heavy_rain =
      IconData(0xe886, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData stopwatch =
      IconData(0xe887, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData mic =
      IconData(0xe888, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData lightning =
      IconData(0xe889, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData unlock =
      IconData(0xe88a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData refresh =
      IconData(0xe88b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData ice_cream =
      IconData(0xe88c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData dessert =
      IconData(0xe88d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData rank =
      IconData(0xe88e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData lock =
      IconData(0xe88f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData light_rain =
      IconData(0xe890, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData baseball =
      IconData(0xe891, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData position =
      IconData(0xe892, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData news =
      IconData(0xe893, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData ship =
      IconData(0xe894, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData postcard =
      IconData(0xe895, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData soccer =
      IconData(0xe896, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData top =
      IconData(0xe897, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData sort =
      IconData(0xe898, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData sugar =
      IconData(0xe899, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData ice_drink =
      IconData(0xe89a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData coffee =
      IconData(0xe89b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData tableware =
      IconData(0xe89c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData burger =
      IconData(0xe89d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData service =
      IconData(0xe89e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData mobile_phone =
      IconData(0xe89f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData top_right =
      IconData(0xe8a0, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData top_left =
      IconData(0xe8a1, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData watch_1 =
      IconData(0xe8a2, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData football =
      IconData(0xe8a3, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData basketball =
      IconData(0xe8a4, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData chat_line_square =
      IconData(0xe8a5, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData message =
      IconData(0xe8a6, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData office_building =
      IconData(0xe8a7, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData school =
      IconData(0xe8a8, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData truck =
      IconData(0xe8a9, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData bicycle =
      IconData(0xe8aa, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData toilet_paper =
      IconData(0xe8ab, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData notebook_2 =
      IconData(0xe8ac, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData price_tag =
      IconData(0xe8ad, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData discount =
      IconData(0xe8ae, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData notebook_1 =
      IconData(0xe8af, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData files =
      IconData(0xe8b0, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData wallet =
      IconData(0xe8b1, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData coin =
      IconData(0xe8b2, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData collection =
      IconData(0xe8b3, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData receiving =
      IconData(0xe8b4, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData money =
      IconData(0xe8b5, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData bank_card =
      IconData(0xe8b6, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData picture_outline =
      IconData(0xe8b7, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData box =
      IconData(0xe8b8, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData picture_outline_round =
      IconData(0xe8b9, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData suitcase_1 =
      IconData(0xe8ba, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData suitcase =
      IconData(0xe8bb, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData film =
      IconData(0xe8bc, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData collection_tag =
      IconData(0xe8bd, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData edit_outline =
      IconData(0xe8be, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData shopping_cart_2 =
      IconData(0xe8bf, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData shopping_cart_1 =
      IconData(0xe8c0, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData shopping_bag_1 =
      IconData(0xe8c1, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData shopping_bag_2 =
      IconData(0xe8c2, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData present =
      IconData(0xe8c3, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData open =
      IconData(0xe8c4, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData connection =
      IconData(0xe8c5, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData link =
      IconData(0xe8c6, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData cpu =
      IconData(0xe8c7, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData thumb =
      IconData(0xe8c8, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData turn_off =
      IconData(0xe8c9, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData set_up =
      IconData(0xe8ca, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData chat_round =
      IconData(0xe8cb, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData chat_line_round =
      IconData(0xe8cc, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData chat_square =
      IconData(0xe8cd, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData chat_dot_round =
      IconData(0xe8ce, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData chat_dot_square =
      IconData(0xe8cf, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData star_off =
      IconData(0xe8d0, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData copy_document =
      IconData(0xe8d1, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData full_screen =
      IconData(0xe8d2, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData switch_button =
      IconData(0xe8d3, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData aim =
      IconData(0xe8d4, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData crop =
      IconData(0xe8d5, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData odometer =
      IconData(0xe8d6, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData time =
      IconData(0xe8d7, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData circle_check =
      IconData(0xe8d8, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData remove_outline =
      IconData(0xe8d9, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData circle_plus_outline =
      IconData(0xe8da, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData bangzhu =
      IconData(0xe8db, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData trophy =
      IconData(0xe8dc, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData trophy_1 =
      IconData(0xe8dd, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData first_aid_kit =
      IconData(0xe8de, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData discover =
      IconData(0xe8df, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData place =
      IconData(0xe8e0, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData location_outline =
      IconData(0xe8e1, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData location_information =
      IconData(0xe8e2, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData add_location =
      IconData(0xe8e3, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData delete_location =
      IconData(0xe8e4, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData map_location =
      IconData(0xe8e5, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData alarm_clock =
      IconData(0xe8e6, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData timer =
      IconData(0xe8e7, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData right =
      IconData(0xe8e8, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData sell =
      IconData(0xe8e9, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData knife_fork =
      IconData(0xe8ea, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData orange =
      IconData(0xe8eb, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData pear =
      IconData(0xe8ec, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData fork_spoon =
      IconData(0xe8ed, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData sold_out =
      IconData(0xe8ee, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData back =
      IconData(0xe8ef, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData bottom =
      IconData(0xe8f0, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData delete =
      IconData(0xe8f1, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData chicken =
      IconData(0xe8f2, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData apple =
      IconData(0xe8f3, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData cherry =
      IconData(0xe8f4, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData food =
      IconData(0xe8f5, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData minus =
      IconData(0xe8f6, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData bottom_right =
      IconData(0xe8f7, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData bottom_left =
      IconData(0xe8f8, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData plus =
      IconData(0xe8f9, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData dish_1 =
      IconData(0xe8fa, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData watermelon =
      IconData(0xe8fb, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData grape =
      IconData(0xe8fc, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData dish =
      IconData(0xe8fd, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData check =
      IconData(0xe8fe, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData d_arrow_right =
      IconData(0xe8ff, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData moon_night =
      IconData(0xe900, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData moon =
      IconData(0xe901, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData cloudy_and_sunny =
      IconData(0xe902, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData partly_cloudy =
      IconData(0xe903, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData cloudy =
      IconData(0xe904, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData sunny =
      IconData(0xe905, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData arrow_left =
      IconData(0xe906, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData d_arrow_left =
      IconData(0xe907, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData refresh_left =
      IconData(0xe908, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData close =
      IconData(0xe909, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData refresh_right =
      IconData(0xe90a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData refrigerator =
      IconData(0xe90b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData goblet_square_full =
      IconData(0xe90c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData goblet_square =
      IconData(0xe90d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData warning_outline =
      IconData(0xe90e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData goblet_full =
      IconData(0xe90f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData setting =
      IconData(0xe910, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData phone_outline =
      IconData(0xe911, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData goblet =
      IconData(0xe912, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData cold_drink =
      IconData(0xe913, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData more_outline =
      IconData(0xe914, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData arrow_right =
      IconData(0xe915, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData arrow_down =
      IconData(0xe916, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData sunset =
      IconData(0xe917, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
