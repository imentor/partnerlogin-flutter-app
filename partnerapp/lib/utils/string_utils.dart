extension StringCasingExtension on String {
  String toCapitalizedFirst() =>
      length > 0 ? '${this[0].toUpperCase()}${substring(1).toLowerCase()}' : '';
  String toTitleCase() => replaceAll(RegExp(' +'), ' ')
      .split(' ')
      .map((str) => str.toCapitalizedFirst())
      .join(' ');
}
