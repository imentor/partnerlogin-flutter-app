import 'dart:io';

import 'package:Haandvaerker.dk/model/payment/payment_stages_model.dart';
import 'package:Haandvaerker.dk/model/projects/extra_work_response_model.dart';
import 'package:Haandvaerker.dk/services/request_payment/request_payment_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PaymentViewModel extends BaseViewModel {
  PaymentViewModel({required this.context});

  final BuildContext context;

  PaymentStages _paymentStages = PaymentStages();
  PaymentStages get paymentStages => _paymentStages;

  Stage _chosenStage = Stage();
  Stage get chosenStage => _chosenStage;

  Stage _paymentStageFile = Stage();
  Stage get paymentStageFile => _paymentStageFile;

  List<File> _files = [];
  List<File> get files => _files;

  List<Stage> _paymentStagesList = [];
  List<Stage> get paymentStagesList => _paymentStagesList;

  Map<String, List<Stage>> _paymentStagesGrouped = {};
  Map<String, List<Stage>> get paymentStagesGrouped => _paymentStagesGrouped;

  List<String> _currentPhotos = [];
  List<String> get currentPhotos => _currentPhotos;

  List<File> _currentUploadedPhotos = [];
  List<File> get currentUploadedPhotos => _currentUploadedPhotos;

  int _selectedPhotoIndex = 0;
  int get selectedPhotoIndex => _selectedPhotoIndex;

  List<GlobalKey> _photoKeys = [];
  List<GlobalKey> get photoKeys => _photoKeys;

  double _contractPrice = 0;
  double get contractPrice => _contractPrice;

  double _totalReleased = 0;
  double get totalReleased => _totalReleased;

  double _totalBalance = 0;
  double get totalBalance => _totalBalance;

  double _totalMangel = 0;
  double get totalMangel => _totalMangel;

  double _wallet = 0;
  double get wallet => _wallet;

  bool _fileUploadValidator = false;
  bool get fileUploadValidator => _fileUploadValidator;

  List<String> acceptedStatuses = [
    'PaymentStashed',
    'Shipped',
    'AwaitingSellerVerification',
    'PartialPaymentReleaseInitiated',
    'PaymentReleaseInitiated',
    'PaymentReleased',
    'PendingRemainingPayment',
    'RemainingPaymentInitiated'
  ];

  void reset() {
    _paymentStages = PaymentStages();
    _chosenStage = Stage();
    _paymentStageFile = Stage();
    _files = [];
    _paymentStagesList = [];
    _paymentStagesGrouped = {};
    _currentPhotos = [];
    _currentUploadedPhotos = [];
    _selectedPhotoIndex = 0;
    _photoKeys = [];
    _contractPrice = 0;
    _totalReleased = 0;
    _totalBalance = 0;
    _totalMangel = 0;
    _wallet = 0;
    _fileUploadValidator = false;

    notifyListeners();
  }

  set paymentStageFile(Stage value) {
    _paymentStageFile = value;

    notifyListeners();
  }

  set chosenStage(Stage value) {
    _currentUploadedPhotos.clear();
    _fileUploadValidator = false;
    _chosenStage = value;
    notifyListeners();
  }

  set currentPhotos(List<String> photos) {
    _currentPhotos = photos;
    notifyListeners();
  }

  set selectedPhotoIndex(int index) {
    _selectedPhotoIndex = index;
    notifyListeners();
  }

  set currentUploadedPhotos(List<File> images) {
    _fileUploadValidator = false;
    _currentUploadedPhotos = images;
    notifyListeners();
  }

  set fileUploadValidator(bool valid) {
    _fileUploadValidator = valid;
    notifyListeners();
  }

  void addFile(File file) {
    _files.add(file);
    notifyListeners();
  }

  void removeFile(File file) {
    _files.remove(file);
    notifyListeners();
  }

  void setStage(Stage value) {
    _chosenStage = value;
    _currentPhotos.clear();
    _currentUploadedPhotos.clear();
    _photoKeys.clear();
    if (value.photos!.isNotEmpty) {
      _currentPhotos = [...value.photos!];
      _photoKeys = List.generate(_currentPhotos.length, (index) => GlobalKey());
    }
    _selectedPhotoIndex = 0;
    notifyListeners();
  }

  Future<void> getPaymentStagesV2({required int projectId}) async {
    var service = context.read<RequestPaymentService>();

    setBusy(true);
    final response =
        await service.paymentStages(projectId: projectId, type: 'normal');

    if (response != null) {
      _paymentStages = PaymentStages.fromJson(response.data);
    }

    setBusy(false);

    notifyListeners();
  }

  Future<void> getPaymentStages({required projectId}) async {
    var service = context.read<RequestPaymentService>();

    setBusy(true);
    final response =
        await service.paymentStages(projectId: projectId, type: 'normal');

    if (response != null) {
      final paymentStage = PaymentStages.fromJson(response.data);
      if ((response.success ?? false) &&
          (paymentStage.stages ?? <Stage>[]).isNotEmpty) {
        _paymentStages = paymentStage;

        final Map<String, List<Stage>> tempPaymentStagesGrouped = {};

        //GROUPING NORMAL PAYMENT STAGE AND DEFICIENCY WITH JOB INDUSTRY ID
        String latestIndustry = '';
        for (var element in (paymentStage.stages ?? <Stage>[])) {
          if (element.contractType == 'normal' ||
              element.contractType == 'fast-track') {
            if (tempPaymentStagesGrouped
                .containsKey(element.jobIndustryId.toString())) {
              List<Stage> stagesAlready = [
                ...(tempPaymentStagesGrouped[element.jobIndustryId.toString()]
                    as List<Stage>)
              ];

              stagesAlready.add(element);

              tempPaymentStagesGrouped[element.jobIndustryId.toString()] =
                  stagesAlready;
            } else {
              if (element.jobIndustryId != null) {
                tempPaymentStagesGrouped.addAll({
                  element.jobIndustryId.toString(): [element]
                });
              }

              if (element.type == 'deficiency' &&
                  tempPaymentStagesGrouped.containsKey(latestIndustry)) {
                List<Stage> stagesAlready = [
                  ...(tempPaymentStagesGrouped[latestIndustry] as List<Stage>)
                ];

                stagesAlready.add(element);

                tempPaymentStagesGrouped[latestIndustry] = stagesAlready;
              }
            }
          }
          latestIndustry = element.jobIndustryId.toString();
        }
        //

        //GETTING EXTRA WORK
        List<Stage> extraWorkStages = [];

        if ((paymentStages.extraWork ?? []).isNotEmpty) {
          for (final extra in (paymentStage.extraWork ?? <ExtraWorkItems>[])) {
            final tempExtra = (paymentStages.transaction ?? []).firstWhere(
              (element) => element.extraWorkId == extra.id,
              orElse: () => PaymentStageTransaction(),
            );

            if (acceptedStatuses.contains(tempExtra.currentStatus)) {
              final extraWorkStage = (paymentStage.stages ?? []).firstWhere(
                (stage) => stage.extraWorkId == extra.id,
                orElse: () => Stage(),
              );

              final index = (paymentStage.stages ?? []).indexWhere(
                    (stage) => stage.extraWorkId == extra.id,
                  ) +
                  1;

              if (extraWorkStage.id != null) {
                extraWorkStages.add(extraWorkStage);
              }

              if (index > 0 && index < (paymentStage.stages ?? []).length) {
                if ((paymentStage.stages ?? [])[index].contractType ==
                        'extra_work' &&
                    (paymentStage.stages ?? [])[index].type == 'deficiency') {
                  extraWorkStages.add((paymentStage.stages ?? [])[index]);
                }
              }
            }
          }
        }

        if (extraWorkStages.isNotEmpty) {
          tempPaymentStagesGrouped.addAll({'extra_work': extraWorkStages});
        }
        //

        //CALCULATE TILES
        double tempWallet = 0;
        double tempTotalReleased = 0;
        double tempTotalBalance = 0;
        double tempTotalMangel = 0;

        for (var map in tempPaymentStagesGrouped.entries) {
          for (var element in map.value) {
            tempWallet += (element.priceDeducted ?? 0);
            tempTotalReleased += (element.totalAmountPaid ?? 0);

            if (element.type == 'normal') {
              tempTotalBalance +=
                  (element.priceDeducted ?? 0) - (element.totalAmountPaid ?? 0);
            } else if (element.type == 'deficiency') {
              tempTotalMangel +=
                  (element.priceDeducted ?? 0) - (element.totalAmountPaid ?? 0);
            }
          }
        }
        //

        _wallet = tempWallet;
        _totalReleased = tempTotalReleased;
        _totalBalance = tempTotalBalance;
        _totalMangel = tempTotalMangel;

        _paymentStagesGrouped = tempPaymentStagesGrouped;
      }
    }

    notifyListeners();
    setBusy(false);
  }

  Future<void> uploadFiles({
    required int photoDocFolderId,
    required String tagUserIds,
    required List<File> selectedFiles,
  }) async {
    final service = context.read<RequestPaymentService>();
    PaymentStages newPaymentStages = PaymentStages();
    setBusy(true);

    Map<String, dynamic> payload = {
      "parentId": photoDocFolderId,
      "category": 'Documentation',
      "tagUserIds": tagUserIds,
      "projectId": paymentStageFile.projectId,
      "title": "Byggewallet Betalingsfil ${paymentStageFile.id}",
      "description": "Byggewallet Betalingsfil ${paymentStageFile.id}",
      "paymentStageId": "${paymentStageFile.id}",
    };
    for (int index = 0; index < selectedFiles.length; index++) {
      final fileData = MultipartFile.fromFileSync(selectedFiles[index].path,
          filename: selectedFiles[index].path);

      payload.putIfAbsent('files[$index]', () => fileData);
    }

    final formDataOne = FormData.fromMap(payload);

    await service.uploadPaymentStageFiles(payload: formDataOne);

    final response = await service.paymentStages(
        projectId: paymentStageFile.projectId, type: 'normal');
    if (response != null) {
      newPaymentStages = PaymentStages.fromJson(response.data);
    }

    setBusy(false);

    if ((newPaymentStages.stages ?? [])
        .where((element) => element.id == paymentStageFile.id)
        .isNotEmpty) {
      paymentStageFile = newPaymentStages.stages!
          .firstWhere((element) => element.id == paymentStageFile.id);
    }
  }

  Future<void> requestDeficiency() async {
    var service = context.read<RequestPaymentService>();

    await service.requestPayment(
      projectId: _chosenStage.projectId!,
      paymentStageId: _chosenStage.id!,
      notes: '',
    );
  }

  Future<void> requestPayment(
      {required String notes,
      required int photoDocFolderId,
      required String tagUserIds,
      bool showLoading = true}) async {
    var service = context.read<RequestPaymentService>();
    if (showLoading) setBusy(true);

    await service.requestPayment(
      projectId: _chosenStage.projectId!,
      paymentStageId: _chosenStage.id!,
      notes: notes,
    );

    if (_currentUploadedPhotos.isNotEmpty) {
      Map<String, dynamic> payload = {
        "parentId": photoDocFolderId,
        "category": 'Documentation',
        "tagUserIds": tagUserIds,
        "projectId": _chosenStage.projectId!,
        "title": "Byggewallet Betalingsfil ${_chosenStage.id!}",
        "description": "Byggewallet Betalingsfil ${_chosenStage.id!}",
        "paymentStageId": _chosenStage.id!,
      };

      for (int index = 0; index < _currentUploadedPhotos.length; index++) {
        final fileData = MultipartFile.fromFileSync(
            _currentUploadedPhotos[index].path,
            filename: _currentUploadedPhotos[index].path);

        payload.putIfAbsent('files[$index]', () => fileData);
      }

      final formDataOne = FormData.fromMap(payload);

      await service.uploadPaymentStageFiles(payload: formDataOne);

      // await service.uploadPaymentStagePhotos(
      //   paymentStageId: _chosenStage.id,
      //   files: _currentUploadedPhotos,
      // );
    }
    setBusy(false);
  }
}
