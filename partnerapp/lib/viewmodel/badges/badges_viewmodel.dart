import 'dart:developer';
import 'dart:io';

import 'package:Haandvaerker.dk/model/badges/badge_model.dart';
import 'package:Haandvaerker.dk/model/badges/press_article_model.dart';
import 'package:Haandvaerker.dk/model/partner/prisen_result_model.dart';
import 'package:Haandvaerker.dk/model/partner/supplier_info_model.dart';
// import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/badge/badge_service.dart';
import 'package:Haandvaerker.dk/viewmodel/app/app_drawer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/widgets_recommendations_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

class BadgesViewModel extends BaseViewModel {
  BadgesViewModel({required this.context});

  final BuildContext context;

  List<BadgeModel?> _badges = [];
  List<BadgeModel?> get badges => _badges;

  bool _hasKlimaBadge = false;
  bool get hasKlimaBadge => _hasKlimaBadge;
  bool _hasPrisenBadge = false;
  bool get hasPrisenBadge => _hasPrisenBadge;

  SupplierInfoModel? _supplierProfileModel;
  SupplierInfoModel? get supplierProfileModel => _supplierProfileModel;

  PressArticleModel _article = PressArticleModel();
  PressArticleModel get article => _article;

  List<PrisenResult> _newBadges = [];
  List<PrisenResult> get newBadges => _newBadges;

  Map<String, String> _badgeImages = {};
  Map<String, String> get badgeImages => _badgeImages;

  Map<String, Uint8List?> _pdfData = {};
  Map<String, Uint8List?> get pdfData => _pdfData;

  bool _isDownloading = false;
  bool get isDownloading => _isDownloading;

  set badgeImages(Map<String, String> badges) {
    _badgeImages = badges;
    notifyListeners();
  }

  set isDownloading(bool downloading) {
    _isDownloading = downloading;
    notifyListeners();
  }

  set pdfData(Map<String, Uint8List?> data) {
    _pdfData = data;
    notifyListeners();
  }

  Future<bool?> saveFiles(
      {required String url,
      required String filename,
      bool isDocs = false,
      bool isFromAsset = false}) async {
    _isDownloading = true;

    try {
      Directory directory = Directory("");
      // Get the app's documents directory.
      if (Platform.isAndroid) {
        directory = Directory("/storage/emulated/0/Download");
      } else {
        directory = await getApplicationDocumentsDirectory();
      }

      if (isFromAsset) {
        final path = '${directory.path}/$filename';

        // Load the image from the assets
        final byteData = await rootBundle.load(url);

        // Create the file
        final file = File(path);

        // Write the byte data to the file
        await file.writeAsBytes(byteData.buffer.asUint8List());

        return true;
      } else {
        // Send an HTTP GET request to the URL.
        final response = await Dio()
            .get(url, options: Options(responseType: ResponseType.bytes));

        if (response.statusCode == 200) {
          // Create a file to save the image.
          final path = '${directory.path}/$filename';

          File file = File(path);

          // This is to check whether
          // There are existing files of the same name
          // If there are, then we add an iterator and
          // Download the file again with (iterator)
          if (await file.exists()) {
            final files = Directory(directory.path).listSync();
            final paths = files.map((e) => e.path);
            int index = 0;
            if (paths.isNotEmpty) {
              for (final path in paths) {
                if (path.contains(filename)) {
                  index++;
                }
              }
              file = File(
                  '${directory.path}/${filename.split('.').first}(${index + 1}).${filename.split('.').last}');
            }
          }

          await file.writeAsBytes(response.data);

          _isDownloading = false;

          return true;
        } else {
          _isDownloading = false;

          return false;
        }
      }
    } catch (e) {
      log("message a $e");
      _isDownloading = false;

      return false;
    }
  }

  // used when user switch accounts, to reset viewmodel value
  void reset() async {
    _badges = [];
    _hasKlimaBadge = false;
    _hasPrisenBadge = false;
    _supplierProfileModel = null;
    _article = PressArticleModel();
    _newBadges = [];
    _badgeImages = {};
    _pdfData = {};
    _isDownloading = false;

    notifyListeners();
  }

  Future<void> getPressArticle() async {
    setBusy(true);
    var service = context.read<BadgeService>();
    var response = await service.getPressArticle();
    _article = response;
    notifyListeners();
    setBusy(false);
  }

  Future<void> getPrisenBadges() async {
    setBusy(true);

    var supplierProfile = context.read<WidgetsViewModel>().company!;

    List<PrisenResult> temp = <PrisenResult>[];

    temp.addAll(supplierProfile.prisenResults ?? []);
    temp.addAll(supplierProfile.mergedBadges ?? []);

    _newBadges = [...temp];

    //This is for checking whether partner has [KLIMA]
    _hasKlimaBadge =
        supplierProfile.otherBadges?.any((element) => element.type == 1997) ??
            false;

    notifyListeners();

    ///[GET ALL BADGE IMAGES HERE]
    ///!DO NOTE THAT THE KEYS FOR THE MAP ARE USED AS THE FILE NAMES
    Map<String, String> badges = {};
    Map<String, Uint8List?> data = {};

    ///This is to check if the partner is a winner for [PRISEN 2023]
    ///We need to check to be able to display the correct [FILES] for the winners

    final isPartner2023Winner = supplierProfile.prisenResults
            ?.any((element) => element.year == 2024 && element.place == 1) ??
        false;

    ///If [WINNER], the partner will be able to view and download the following files:
    ///[WINNER LIST], [WINNER DIPLOMA (LANDSCAPE & PORTRAIT)], [AI LOGO TEMPLATE FOR PRINT]
    if (isPartner2023Winner) {
      ///
      final winnerData = supplierProfile.prisenResults!.firstWhere(
          (element) => element.year == 2024 && element.place == 1,
          orElse: () => PrisenResult.defaults());

      const winners = 'assets/documents/Download Guide Digitale.pdf';
      badges['digital_prize_package'] = winners;
      // data['digital_prize_package'] = await _getByteData(url: winners);

      final landscape =
          'https://widget5.haandvaerker.dk/image_2_pdf/rendered/Lanscape/${(winnerData.industryName! == 'VVS&Blik') ? 'VVS & Blik' : winnerData.industryName!}_${winnerData.area.replaceFirst('Region ', '')}_${winnerData.supplierName!.replaceAll('/', '_')}_${DateTime.now().year}_${getHaaid()}.pdf';
      badges['digital_diploma_landscape'] = landscape;
      data['digital_diploma_landscape'] = await _getByteData(url: landscape);

      final portrait =
          'https://widget5.haandvaerker.dk/image_2_pdf/rendered/Portrait/${(winnerData.industryName! == 'VVS&Blik') ? 'VVS & Blik' : winnerData.industryName!}_${winnerData.area.replaceFirst('Region ', '')}_${winnerData.supplierName!.replaceAll('/', '_')}_${DateTime.now().year}_${getHaaid()}.pdf';
      badges['digital_diploma_portrait'] = portrait;
      data['digital_diploma_portrait'] = await _getByteData(url: portrait);

      const logoTemplate = 'assets/documents/prisen2024.ai';
      badges['logo_ai_template'] = logoTemplate;

      ///
    } else {
      const finalist = 'assets/documents/Download Guide Digitale.pdf';
      badges['digital_prize_package'] = finalist;
      // data['digital_prize_package'] = await _getByteData(url: finalist);
    }

    ///Check for [Winner's Article] here
    final article = await _checkForWinnersArticle();

    if (article != null) {
      badges['your_press_release'] = article;
    }

    /// Check whether the partner has a [CLIMATE DIPLIOMA]
    final climateUrl =
        'https://www.haandvaerker.dk/prisenAssets/${getHaaid()}.pdf';
    final climateDiploma = await _getByteData(url: climateUrl);

    if (climateDiploma != null) {
      badges['climate_diploma'] = climateUrl;
      data['climate_diploma'] = climateDiploma;
    }

    ///Check whether the partner has a [KLIMA BADGE]
    if (_hasKlimaBadge) {
      // badges['klima - ${getHaaid()}.jpg'] =
      //     "https://widget5.haandvaerker.dk/klima/rendered/klima_${getHaaid()}.png";
    }

    ///Cycle through all other badges the partner has
    for (int i = 0; i < _newBadges.length; i++) {
      badges['mail - ${i + 1}.jpg'] =
          "https://widget5.haandvaerker.dk/prisen/winnersBadge/mail/rendered/${getHaaid()}/${i + 1}.jpg";
      badges['small - ${i + 1}.jpg'] =
          "https://widget5.haandvaerker.dk/prisen/winnersBadge/mailsmall/rendered/${getHaaid()}/${i + 1}.jpg";
      badges['stream - ${i + 1}.jpg'] =
          "https://widget5.haandvaerker.dk/prisen/winnersBadge/streamer/rendered/${getHaaid()}/${i + 1}.png";
    }

    badgeImages = badges;
    pdfData = data;

    setBusy(false);
  }

  Future<String?> _checkForWinnersArticle() async {
    ///

    try {
      final response = await Dio().get(
          'https://widget5.haandvaerker.dk/prisen/2023-winnersArticle/${getHaaid()}.docx');

      if (response.statusCode == 200) {
        return 'https://widget5.haandvaerker.dk/prisen/2023-winnersArticle/${getHaaid()}.docx';
      }
    } catch (e) {
      log(e.toString());
      return null;
    }

    return null;

    ///
  }

  Future<Uint8List?> _getByteData({required String url}) async {
    ///

    try {
      final response = await Dio()
          .get(url, options: Options(responseType: ResponseType.bytes));

      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      log(e.toString());
      return null;
    }

    return null;

    ///
  }

  getName() {
    final userVm = context.read<UserViewModel>();
    return userVm.userModel?.user?.name ?? '';
  }

  getHaaid() {
    final userVm = context.read<UserViewModel>();
    return userVm.userModel?.user?.id ?? '';
  }

  getCompanyName() {
    final userVm = context.read<UserViewModel>();
    return userVm.userModel?.user?.name ?? '';
  }

  Future<void> getCompany() async {
    var supplierProfile =
        context.read<AppDrawerViewModel>().supplierProfileModel;

    _supplierProfileModel = supplierProfile;
  }
}
