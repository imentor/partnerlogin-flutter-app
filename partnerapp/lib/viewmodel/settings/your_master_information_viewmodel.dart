import 'package:Haandvaerker.dk/model/settings/master_info_model.dart';
import 'package:Haandvaerker.dk/services/settings/settings_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class YourMasterInfoViewModel extends BaseViewModel {
  //

  YourMasterInfoViewModel({required this.context});
  final BuildContext context;

  //

  List<MasterInfoGeneralModel> _masterInfo = <MasterInfoGeneralModel>[];
  List<MasterInfoGeneralModel> get masterInfo => _masterInfo;

  Map<String, dynamic> _masterInfoData = <String, dynamic>{};
  Map<String, dynamic> get masterInfoData => _masterInfoData;

  bool _validate = false;
  bool get validate => _validate;

  void reset() {
    _masterInfo = [];
    _masterInfoData = {};
    _validate = false;

    notifyListeners();
  }

  set masterInfo(List<MasterInfoGeneralModel> info) {
    _masterInfo = info;
    notifyListeners();
  }

  set masterInfoData(Map<String, dynamic> data) {
    _masterInfoData = data;
    notifyListeners();
  }

  set validate(bool validate) {
    _validate = validate;
    notifyListeners();
  }

  //

  Future<void> getMasterInfo() async {
    //
    setBusy(true);

    validate = false;

    final settingsService = context.read<SettingsService>();

    final response = await settingsService.getMasterInfo();

    masterInfo = response;

    Map<String, dynamic> tempData = <String, dynamic>{};

    for (final info in _masterInfo) {
      final name = info.name!;
      if (name == 'UF_CRM_COMPANY_LOW_SIZE') {
        tempData[name] = {
          'key': GlobalKey(),
          'valid': true,
          'value': [info.companyLowSize, info.companyAssignmentSize],
        };
      } else if (name == 'UF_STARTWORKMONTH') {
        tempData[name] = {
          'key': GlobalKey(),
          'valid': info.startWorkMonth != null && info.endWorkMonth != null,
          'value': [info.startWorkMonth, info.endWorkMonth],
        };
      } else {
        if (info.value == null || info.value.contains(',')) {
          if (info.value == null) {
            if (info.labelType != 'CHECKBOX') {
              tempData[name] = {
                'key': GlobalKey(),
                'valid': false,
                'value': null,
              };
            } else {
              tempData[name] = {
                'key': GlobalKey(),
                'valid': false,
                'value': List.generate(info.choicesDA.length, (index) => 0),
              };
            }
          } else {
            //THIS IS TO CHECK WHETHER THE INITIAL VALUES
            //MATCH THE LENGTH OF THE CHOICES
            List<int> value = info.value
                .toString()
                .split(',')
                .map((e) => int.parse(e))
                .toList();

            if (value.length > info.choicesDA.length) {
              List<int> tempValue = <int>[];
              for (int i = 0; i < info.choicesDA.length; i++) {
                tempValue.add(value[i]);
              }

              value = tempValue;
            }
            tempData[name] = {
              'key': GlobalKey(),
              'valid': true,
              'value': value,
            };
          }
        } else {
          if (info.labelType == 'CHECKBOX') {
            tempData[name] = {
              'key': GlobalKey(),
              'valid': true,
              'value': List.generate(info.choicesDA.length, (index) {
                if (int.parse(info.value) == (index + 1)) {
                  return 1;
                } else {
                  return 0;
                }
              }),
            };
          } else {
            tempData[name] = {
              'key': GlobalKey(),
              'valid': true,
              'value': info.value,
            };
          }
        }
      }
    }

    masterInfoData = tempData;

    setBusy(false);
    //
  }

  Future<bool> saveMasterInfo() async {
    //

    final settingService = context.read<SettingsService>();

    final payload = {
      'UF_CRM_COMPANY_LOW_SIZE': List<dynamic>.from(
              _masterInfoData['UF_CRM_COMPANY_LOW_SIZE']['value'])
          .first,
      'UF_CRM_COMPANY_ASSIGNMENT_SIZE': List<dynamic>.from(
              _masterInfoData['UF_CRM_COMPANY_LOW_SIZE']['value'])
          .last,
      'UF_STARTWORKMONTH':
          List<dynamic>.from(_masterInfoData['UF_STARTWORKMONTH']['value'])
              .first,
      'UF_ENDWORKMONTH':
          List<dynamic>.from(_masterInfoData['UF_STARTWORKMONTH']['value'])
              .last,
      'UF_CRM_COMPANY_PHONE_SELECT': List<dynamic>.from(
                  _masterInfoData['UF_CRM_COMPANY_PHONE_SELECT']['value'])
              .indexWhere((element) => element == 1) +
          1,
      'UF_CRM_COMPANY_HOUR_SELECT': List<dynamic>.from(
                  _masterInfoData['UF_CRM_COMPANY_HOUR_SELECT']['value'])
              .indexWhere((element) => element == 1) +
          1,
      'UF_CRM_COMPANY_WHAT_TASK_INTEREST_YOU': List<dynamic>.from(
              _masterInfoData['UF_CRM_COMPANY_WHAT_TASK_INTEREST_YOU']['value'])
          .join(','),
      'UF_CRM_COMPANY_WHEN_CAN_YOU_TAKE_TASK': [
        ...List<dynamic>.from(
            _masterInfoData['UF_CRM_COMPANY_WHEN_CAN_YOU_TAKE_TASK']['value']),
        1
      ].join(','),
      'UF_CRM_COMPANY_LANGUAGE': List<dynamic>.from(
              _masterInfoData['UF_CRM_COMPANY_LANGUAGE']['value'])
          .join(','),
      'UF_CRM_COMPANY_COMPANYTYPE': List<dynamic>.from(
              _masterInfoData['UF_CRM_COMPANY_COMPANYTYPE']['value'])
          .join(','),
      'UF_CRM_COMPANY_TAKE_NEW_TASKS': List<dynamic>.from(
              _masterInfoData['UF_CRM_COMPANY_TAKE_NEW_TASKS']['value'])
          .join(','),
      'UF_CRM_COMPANY_DIALOG': _masterInfoData['UF_CRM_COMPANY_DIALOG']
          ['value'],
      'UF_CRM_COMPANY_READY_TO_BUY':
          _masterInfoData['UF_CRM_COMPANY_READY_TO_BUY']['value'],
      'UF_CRM_COMPANY_INVOICE': _masterInfoData['UF_CRM_COMPANY_INVOICE']
          ['value'],
      'UF_CRM_COMPANY_OFFER': _masterInfoData['UF_CRM_COMPANY_OFFER']['value'],
      'UF_CRM_COMPANY_CRM': _masterInfoData['UF_CRM_COMPANY_CRM']['value'],
    };

    final response = await settingService.saveMasterInfo(payload: payload);

    return response;

    //
  }
}
