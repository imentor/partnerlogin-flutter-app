import 'dart:developer';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/settings/holiday_model.dart';
import 'package:Haandvaerker.dk/services/settings/settings_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
// import 'package:Haandvaerker.dk/model/partner_profil_model.dart';
import 'package:provider/provider.dart';

class HolidaysViewModel extends BaseViewModel {
  HolidaysViewModel({required this.context});

  final BuildContext context;

  // PartnerProfileModel _partner = PartnerProfileModel();
  // PartnerProfileModel get partner => _partner;

  List<HolidayModel> _partnerHolidays = <HolidayModel>[];
  List<HolidayModel> get partnerHolidays => _partnerHolidays;

  List<Map<String, DateTime>> _holidays = <Map<String, DateTime>>[];
  List<Map<String, DateTime>> get holidays => _holidays;

  DateTime? _nearestAvailableDate;
  DateTime? get nearestAvailableDate => _nearestAvailableDate;

  DateTime? _fromDate;
  DateTime? get fromDate => _fromDate;

  DateTime? _toDate;
  DateTime? get toDate => _toDate;

  List<DateTime> _availableDates = [];
  List<DateTime> get availableDates => _availableDates;

  List<DateTime> _fromAvailableDates = [];
  List<DateTime> get fromAvailableDates => _fromAvailableDates;

  List<DateTime> _toAvailableDates = [];
  List<DateTime> get toAvailableDates => _toAvailableDates;

  bool _isGettingHolidayData = false;
  bool get isGettingHolidayData => _isGettingHolidayData;

  void reset() {
    _partnerHolidays = [];
    _holidays = [];
    _nearestAvailableDate = null;
    _fromDate = null;
    _toDate = null;
    _availableDates = [];
    _fromAvailableDates = [];
    _toAvailableDates = [];
    _isGettingHolidayData = false;

    notifyListeners();
  }

  set partnerHolidays(List<HolidayModel> holidays) {
    _partnerHolidays = holidays;
    notifyListeners();
  }

  set holidays(List<Map<String, DateTime>> days) {
    _holidays = days;
    notifyListeners();
  }

  set fromDate(DateTime? date) {
    _fromDate = date;
    notifyListeners();
  }

  set toDate(DateTime? date) {
    _toDate = date;
    notifyListeners();
  }

  set nearestAvailableDate(DateTime? date) {
    _nearestAvailableDate = date;
    notifyListeners();
  }

  set availableDates(List<DateTime> dates) {
    _availableDates = dates;
    notifyListeners();
  }

  set fromAvailableDates(List<DateTime> dates) {
    _fromAvailableDates = dates;
    notifyListeners();
  }

  set toAvailableDates(List<DateTime> dates) {
    _toAvailableDates = dates;
    notifyListeners();
  }

  set isGettingHolidayData(bool value) {
    _isGettingHolidayData = value;
    notifyListeners();
  }

  void setAvailDates(List<Map<String, DateTime>> holidays) {
    // Converting dates provided to UTC
    // So that all things like DST don't affect subtraction and addition on dates
    DateTime startDate = DateTime(2018);
    DateTime endDate = DateTime(2100);

    // Created a list to hold all dates
    List<DateTime> daysInFormat = [];

    // Starting a loop with the initial value as the Start Date
    // With an increment of 1 day on each loop
    // With condition current value of loop is smaller than or same as end date
    for (DateTime i = startDate;
        i.isBefore(endDate) || i.isAtSameMomentAs(endDate);
        i = i.add(const Duration(days: 1))) {
      daysInFormat.add(DateTime(i.year, i.month, i.day));
    }

    List<DateTime> availDays = [];
    List<DateTime> notAvaiDays = [];

    for (final day in daysInFormat.toSet().toList()) {
      for (final element in holidays) {
        final from = element['from']!;
        final to = element['to']!;

        if (!day.isAtSameMomentAs(from) &&
            !day.isAtSameMomentAs(to) &&
            (day.isAfter(to) || day.isBefore(from))) {
          availDays.add(day);
        } else {
          notAvaiDays.add(day);
        }
      }
    }

    log(notAvaiDays.toString());
    availDays.toSet().toList();
    notAvaiDays.toSet().toList();
    availDays.removeWhere((element) => notAvaiDays.contains(element));
    for (final days in notAvaiDays) {
      log("$days: ${availDays.contains(days)}");
    }
    availableDates = availDays;
    fromAvailableDates = availDays;
    toAvailableDates = availDays;

    findNearestToPresent(dates: availDays, compareToDate: DateTime.now());
  }

  void findNearestToPresent(
      {required List<DateTime> dates, required DateTime compareToDate}) {
    nearestAvailableDate = dates.reduce((a, b) =>
        a.difference(compareToDate).abs() < b.difference(compareToDate).abs()
            ? a
            : b);
  }

  void setInitialData() {
    _partnerHolidays = <HolidayModel>[];
    _holidays = [];
    _availableDates = <DateTime>[];
    _fromAvailableDates = <DateTime>[];
    _toAvailableDates = <DateTime>[];
    _fromDate = null;
    _toDate = null;
    _nearestAvailableDate = null;
    notifyListeners();
  }

  Future<void> getHolidays() async {
    setInitialData();
    final service = context.read<SettingsService>();
    final response = await service.getHolidays();

    if (response.success!) {
      partnerHolidays =
          HolidayModel.fromCollection(response.data as List<dynamic>);

      List<Map<String, DateTime>> mappedDays = <Map<String, DateTime>>[];

      for (final days in partnerHolidays) {
        mappedDays.add({
          'from': DateTime.parse(days.holidayFrom!),
          'to': DateTime.parse(days.holidayTo!),
        });
      }

      holidays = mappedDays;
      if (_holidays.isNotEmpty) {
        setAvailDates(holidays);
      }
    }
  }

  Future<MinboligApiResponse> addHoliday({
    required String from,
    required String to,
  }) async {
    final service = context.read<SettingsService>();

    final response = await service.addHoliday(from: from, to: to);

    if (response.success!) {
      final addedHoliday =
          HolidayModel.fromJson(response.data as Map<String, dynamic>);

      final holidaysModel = <HolidayModel>[..._partnerHolidays];
      holidaysModel.add(addedHoliday);

      List<Map<String, DateTime>> mappedDays = <Map<String, DateTime>>[];
      partnerHolidays = holidaysModel;
      for (final days in partnerHolidays) {
        mappedDays.add({
          'from': DateTime.parse(days.holidayFrom!),
          'to': DateTime.parse(days.holidayTo!),
        });
      }

      holidays = mappedDays;
      setAvailDates(holidays);
    }
    return response;
  }

  Future<MinboligApiResponse> deleteHoliday({required int holidayId}) async {
    final service = context.read<SettingsService>();

    return await service.deleteHoliday(holidayId: holidayId);
  }

  // Future<void> getPartnerProfile() async {
  //   var service = Provider.of<SettingsService>(context, listen: false);

  //   _partner = await service.holidayPartnerProfile(holiday: 1, array: 'no');
  // }

  // Future<GenericResponseModel> serviceFree({
  //   required String datechooseqfreestart,
  //   required String datechooseqfreeend,
  // }) async {
  //   var service = Provider.of<SettingsService>(context, listen: false);

  //   return service.serviceFree(
  //       datechooseqfreestart: datechooseqfreestart,
  //       datechooseqfreeend: datechooseqfreeend);
  // }

  // Future<GenericResponseModel> serviceFreeRemove({
  //   required holidayid,
  // }) async {
  //   var service = Provider.of<SettingsService>(context, listen: false);

  //   return service.serviceFreeRemove(holidayid: holidayid);
  // }
}
