import 'package:Haandvaerker.dk/model/settings/phone_number_model.dart';
import 'package:Haandvaerker.dk/services/settings/settings_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PhoneNumbersViewModel extends BaseViewModel {
  PhoneNumbersViewModel({this.context});

  BuildContext? context;

  List<PhoneNumberModel> _currentPhoneNumbers = [];
  List<PhoneNumberModel> get currentPhoneNumbers => _currentPhoneNumbers;

  void reset() {
    _currentPhoneNumbers = [];

    notifyListeners();
  }

  set currentPhoneNumbers(List<PhoneNumberModel> phoneNumbers) {
    _currentPhoneNumbers = phoneNumbers;
    notifyListeners();
  }

  Future<void> getPhoneNumbers() async {
    final service = context!.read<SettingsService>();

    _currentPhoneNumbers.clear();

    setBusy(true);
    currentPhoneNumbers = await service.getPhoneNumbers();
    setBusy(false);
  }

  Future<void> addPhoneNumber({required String phoneNumber}) async {
    final service = context!.read<SettingsService>();

    final added = await service.addPhoneNumber(phoneNumber: phoneNumber);

    _currentPhoneNumbers.add(added);
    notifyListeners();
  }

  Future<void> deletePhoneNumber({required int phoneId}) async {
    final service = context!.read<SettingsService>();

    final deleted = await service.deletePhoneNumber(phoneId: phoneId);

    if (deleted) {
      _currentPhoneNumbers.removeWhere((element) => element.id == phoneId);
    }

    notifyListeners();
  }
}
