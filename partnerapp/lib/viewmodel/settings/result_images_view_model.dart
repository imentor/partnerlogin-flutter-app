import 'dart:io';

import 'package:Haandvaerker.dk/model/partner/supplier_info_file_model.dart';
import 'package:Haandvaerker.dk/services/settings/settings_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
// import 'package:flutter_uploader/flutter_uploader.dart';

class ResultImagesViewModel extends BaseViewModel {
  ResultImagesViewModel({required this.context});
  final BuildContext context;

  List<SupplierInfoFileModel> _value = <SupplierInfoFileModel>[];
  // bool _uploadingImage = false;

  List<SupplierInfoFileModel> get value => _value;
  // bool get isUploadingImage => _uploadingImage;

  String _defaultProfileImage = '';
  String get defaultProfileImage => _defaultProfileImage;

  Map<int, int> _imageSorting = <int, int>{};

  void reset() {
    _value = [];
    _defaultProfileImage = '';
    _imageSorting = <int, int>{};

    notifyListeners();
  }

  set value(List<SupplierInfoFileModel> pictures) {
    _value = pictures;

    final tempSorting = <int, int>{};
    for (int i = 0; i < _value.length; i++) {
      tempSorting[_value[i].id!] = i;
    }
    _imageSorting = tempSorting;
    notifyListeners();
  }

  set defaultProfileImage(String image) {
    _defaultProfileImage = image;
    notifyListeners();
  }

  Future<void> uploadProfileImage({required File file}) async {
    // _uploadingImage = true;
    // notifyListeners();
    // final service = Provider.of<SettingsService>(context, listen: false);
    // await service.uploadProfileImage(file: file);
    // _uploadingImage = false;
    // notifyListeners();
  }

  Future<bool> deleteProfilePicture({required int pictureId}) async {
    final service = context.read<SettingsService>();

    final response = await service.deleteProfilePicture(pictureId: pictureId);

    final pictures = <SupplierInfoFileModel>[..._value];
    pictures.removeWhere((element) => element.id == pictureId);

    _value = pictures;

    notifyListeners();
    return response;
  }

  Future<void> uploadProfilePicture({required List<File> files}) async {
    final service = context.read<SettingsService>();

    final uploadedImages = await service.uploadProfilePicture(files: files);

    final existingImages = <SupplierInfoFileModel>[..._value];
    existingImages.addAll(uploadedImages);

    _value = existingImages;
    notifyListeners();
  }

  Future<void> _updateProfilePictureSorting() async {
    final service = context.read<SettingsService>();

    final parsedSorting = <String, int>{};
    for (final key in _imageSorting.keys) {
      parsedSorting[key.toString()] = _imageSorting[key]!;
    }

    await service.updateProfilePictureSorting(sorting: parsedSorting);
  }

  Future<void> editProfileImage(
      {required int pictureId,
      required File picture,
      required int index}) async {
    final service = context.read<SettingsService>();

    final editedResponse = await service.editProfilePicture(
        pictureId: pictureId, picture: picture);

    //WE MAKE A TEMPORARY COPY OF THE LIST OF PICTURES
    final images = <SupplierInfoFileModel>[..._value];

    //WE REMOVE THE IMAGE FROM THE LIST
    images.removeAt(index);

    //THEN REPLACE IT ON THE SAME INDEX WITH THE EDITED ONE
    images.insert(index, editedResponse);

    //WE THEN ASSIGN IT BACK TO OUR IMAGES STATE
    value = images;
  }

  // Future<String> streamUploadProfileImage(
  //     {required FlutterUploader uploader,
  //     required String tag,
  //     required FileItem? fileItem}) async {
  //   _uploadingImage = true;
  //   notifyListeners();
  //   final service = Provider.of<SettingsService>(context, listen: false);
  //   var response = await service.streamUploadProfileImage(
  //       uploader: uploader, fileItem: fileItem, tag: tag);
  //   _uploadingImage = false;
  //   notifyListeners();

  //   return response;
  // }

  Future<void> shiftPhoto({
    required bool forward,
    required int photoId,
    bool pushToEnd = false,
  }) async {
    //

    //PREREQ
    /**
     * IF THE IMAGE IS ALREADY AT BOTH ENDS OF THE LIST
     * THE SHIFT BUTTONS WILL BE DISABLE 
     * SUCH THAT THEY WONT BE ABLE TO SHIFT THE IMAGES
     */

    //MAKE A COPY OF THE EXISTING LIST OF IMAGES
    //WE'LL BE UTILZING THE [ID] ONLY
    final images = <int>[..._value.map((e) => e.id!)];

    //GET THE INDEX OF THE SHIFTED PHOTO
    final index = images.indexOf(photoId);

    //WE REMOVE THE IMAGE AT ITS INDEX
    images.removeAt(index);

    //GENERAL ALGORITHM FOR SHIFTING
    if (forward) {
      if (pushToEnd) {
        images.add(photoId);
      } else {
        images.insert(index + 1, photoId);
      }
    } else {
      if (pushToEnd) {
        images.insert(0, photoId);
      } else {
        images.insert(index - 1, photoId);
      }
    }

    //WE CREATE A NEW LIST OF THE REARRANGED IMAGES
    //WITH ITS VALUES AND ASSIGN IT BACK TO OUR MAIN LIST OF IMAGES
    final imageValues = <SupplierInfoFileModel>[];
    for (final image in images) {
      imageValues.add(_value.firstWhere(
        (element) => element.id == image,
        orElse: () => SupplierInfoFileModel.defaults(),
      ));
    }

    value = imageValues;

    notifyListeners();

    //CALL THE API THAT UPDATES THE SORTING OF THE IMAGES
    //UPDATING THE SORTING ONLY TAKES A SHORT TIME
    //THAT'S WHY WE JUST CALL IT RIGHT EVERY AFTER
    await _updateProfilePictureSorting();
  }
}
