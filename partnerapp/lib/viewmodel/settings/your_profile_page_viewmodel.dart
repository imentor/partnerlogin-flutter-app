// import 'package:Haandvaerker.dk/model/generic_response_model.dart';
// import 'package:Haandvaerker.dk/model/partner_haa_model.dart';
import 'package:Haandvaerker.dk/services/settings/settings_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class YourProfilePageViewModel extends BaseViewModel {
  YourProfilePageViewModel({required this.context});
  final BuildContext context;

  // GenericSuccessResponseModel _profileInformation =
  //     GenericSuccessResponseModel();
  // GenericSuccessResponseModel _profileDescription =
  //     GenericSuccessResponseModel();
  // GenericSuccessResponseModel get profileInformation => _profileInformation;
  // GenericSuccessResponseModel get profileDescription => _profileDescription;

  String _profileText = '';
  String get profileText => _profileText;

  void reset() {
    _profileText = '';

    notifyListeners();
  }

  set profileText(String text) {
    _profileText = text;
    notifyListeners();
  }

  // Future<void> updateProfileInformation(PartnerHaaModel data) async {
  //   setBusy(true);
  //   final service = Provider.of<SettingsService>(context, listen: false);
  //   _profileInformation = await service.updateProfileInformation(data);
  //   setBusy(false);
  // }

  // Future<void> updateProfileDescription({required String description}) async {
  //   setBusy(true);
  //   final service = Provider.of<SettingsService>(context, listen: false);
  //   _profileDescription =
  //       await service.updateProfileDescription(description: description);
  //   setBusy(false);
  // }

  Future<bool> updateSupplierInfo(
      {required Map<String, dynamic> payload}) async {
    final service = context.read<SettingsService>();

    final response = await service.updateSupplierInfo(payload: payload);

    return response.success!;
  }

  Map<String, dynamic> getChanges(
      Map<String, dynamic> oldMap, Map<String, dynamic> newMap) {
    final changes = <String, dynamic>{};
    for (final key in oldMap.keys) {
      if (oldMap[key] != newMap[key]) {
        changes[key] = newMap[key];
      }
    }
    return changes;
  }
}
