import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/settings/settings_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class SubscriptionViewModel extends BaseViewModel {
  SubscriptionViewModel({required this.context});

  final BuildContext context;

  String _endDate = '';
  String get endDate => _endDate;

  bool _hasCanceled = false;
  bool get hasCanceled => _hasCanceled;

  bool? _isCancelling = false;
  bool? get isCancelling => _isCancelling;

  void reset() {
    _endDate = '';
    _hasCanceled = false;
    _isCancelling = false;

    notifyListeners();
  }

  set endDate(String date) {
    _endDate = date;
    notifyListeners();
  }

  set hasCanceled(bool cancel) {
    _hasCanceled = cancel;
    notifyListeners();
  }

  set isCancelling(bool? cancel) {
    _isCancelling = cancel;
    notifyListeners();
  }

  Future<bool> downloadPdf(
      {required String fileName, required String fileLink}) async {
    setBusy(true);

    var service = context.read<SettingsService>();
    final response =
        await service.downloadPdf(fileName: fileName, fileLink: fileLink);

    setBusy(false);
    return response;
  }

  Future<MinboligApiResponse> messageCs({required String message}) async {
    var service = context.read<SettingsService>();

    return await service.messageCs(message: message);
  }

  Future<String> cancelSubscriptionPartner(
      {required String description}) async {
    final service = context.read<SettingsService>();

    return await service.cancelSubscriptionPartner(description: description);
  }
}
