import 'dart:developer';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/get_types_response_model.dart';
import 'package:Haandvaerker.dk/model/partner/bitrix_company_model.dart';
import 'package:Haandvaerker.dk/model/partner/supplier_info_model.dart';
// import 'package:Haandvaerker.dk/model/partner_profil_model.dart' as prt;
import 'package:Haandvaerker.dk/model/settings/regions_model.dart';
import 'package:Haandvaerker.dk/model/website_setting_model.dart';
import 'package:Haandvaerker.dk/services/main/main_service.dart';
import 'package:Haandvaerker.dk/services/settings/settings_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class JobTypesViewModel extends BaseViewModel {
  JobTypesViewModel({required this.context});

  final BuildContext context;

  SupplierInfoModel? _partnerJobtypes;
  SupplierInfoModel? get partnerJobtypes => _partnerJobtypes;

  List<int> _tasks = [];
  List<int> get tasks => _tasks;

  List<int> _taskTemplate = [];
  List<int> get taskTemplate => _taskTemplate;

  List<int> _industryTasks = [];
  List<int> get industryTasks => _industryTasks;

  Map<int, Map<int, List<int>>> _taskMap = {};
  Map<int, Map<int, List<int>>> get taskMap => _taskMap;

  BitrixCompanyModel? _company;
  BitrixCompanyModel? get company => _company;

  List<Data> _industryList = [];
  List<Data> get industryList => _industryList;

  List<RegionModel> _allRegions = [];
  List<RegionModel> get allRegions => _allRegions;

  List<int> _selectedRegions = [];
  List<int> get selectedRegions => _selectedRegions;

  bool _isRegionUpdating = false;
  bool get isRegionUpdating => _isRegionUpdating;

  bool _isEnterpriseUpdating = false;
  bool get isEnterpriseUpdating => _isEnterpriseUpdating;

  int? _currentEnterprise;
  int? get currentEnterprise => _currentEnterprise;

  List<bool> _enterpriseBoxValues = [false, false];
  List<bool> get enterpriseBoxValues => _enterpriseBoxValues;

  void reset() {
    _partnerJobtypes = null;
    _tasks = [];
    _taskTemplate = [];
    _industryTasks = [];
    _taskMap = {};
    _company = null;
    _industryList = [];
    _allRegions = [];
    _selectedRegions = [];
    _isRegionUpdating = false;
    _isEnterpriseUpdating = false;
    _currentEnterprise = null;
    _enterpriseBoxValues = [false, false];

    notifyListeners();
  }

  //SETTERS

  set allRegions(List<RegionModel> regions) {
    _allRegions = regions;
    notifyListeners();
  }

  set selectedRegions(List<int> regions) {
    _selectedRegions = regions;
    notifyListeners();
  }

  set isRegionUpdating(bool updating) {
    _isRegionUpdating = updating;
    notifyListeners();
  }

  set isEnterpriseUpdating(bool updating) {
    _isEnterpriseUpdating = updating;
    notifyListeners();
  }

  set company(BitrixCompanyModel? company) {
    _company = company;
    notifyListeners();
  }

  set currentEnterprise(int? enterprise) {
    _currentEnterprise = enterprise;
    notifyListeners();
  }

  set enterpriseBoxValues(List<bool> values) {
    _enterpriseBoxValues = values;
    notifyListeners();
  }

  set tasks(List<int> tasks) {
    _tasks = tasks;
    notifyListeners();
  }

  set taskTemplate(List<int> tasks) {
    _taskTemplate = tasks;
    notifyListeners();
  }

  set industryTasks(List<int> tasks) {
    _industryTasks = tasks;
    notifyListeners();
  }

  set taskMap(Map<int, Map<int, List<int>>> map) {
    _taskMap = map;
    notifyListeners();
  }

  set partnerJobtypes(SupplierInfoModel? response) {
    _partnerJobtypes = response;
    notifyListeners();
  }

  // METHODS

  //GET ALL REGIONS
  Future<void> getAllRegions() async {
    final service = context.read<SettingsService>();

    final regionResponse = await service.getAllRegions();
    if (regionResponse != null) {
      allRegions = RegionModel.fromCollection(regionResponse.data);
    }
  }

  //GET PARTNER'S REGIONS
  Future<void> getRegions() async {
    final service = context.read<SettingsService>();

    selectedRegions = await service.getRegions();
  }

  //EDIT REGIONS
  Future<MinboligApiResponse> editRegions() async {
    final service = context.read<SettingsService>();

    isRegionUpdating = true;
    return await service.editRegions(regionIds: _selectedRegions);
  }

  //EDIT SUPPLIER INFO CUSTOM FIELDS
  Future<MinboligApiResponse> updateSupplierInfo(
      {required FormData fields}) async {
    final service = context.read<SettingsService>();

    return await service.updateSupplierInfoCustomFields(fields: fields);
  }

  Future<void> getPartnerProfile() async {
    var service = context.read<SettingsService>();
    // _partner = await service.oversigtPartnerProfile(typeMeeting: 1);
    final industryResponse = await service.getTaskTypesFromCRM();
    List<Data> tempIndustryList = [];

    if (industryResponse != null) {
      tempIndustryList = Data.fromCollection(industryResponse.data);
    }

    _selectedRegions = <int>[
      ..._selectedRegions,
      _allRegions
          .firstWhere(
            (element) =>
                element.name!.toLowerCase() ==
                _company!.areaText!.toLowerCase(),
            orElse: () => RegionModel.defaults(),
          )
          .id!,
    ];

    _industryTasks.clear();
    _taskMap.clear();
    try {
      for (final industry in tempIndustryList) {
        final parentCategories = industry.subIndustries!
            .where((element) => element.parentId == 0)
            .toList();

        for (final parent in parentCategories) {
          final childCategories = industry.subIndustries!
              .where((element) => element.parentId == parent.subcategoryId!)
              .toList();

          if (_taskMap[industry.industryId] == null) {
            _taskMap.putIfAbsent(
                industry.industryId!,
                () => {
                      parent.subIndustrId!:
                          childCategories.map((e) => e.subIndustrId!).toList(),
                    });
          } else {
            _taskMap[industry.industryId]!.addAll({
              parent.subIndustrId!:
                  childCategories.map((e) => e.subIndustrId!).toList(),
            });
          }
        }
      }

      for (final industry in _taskMap.keys) {
        innerLoop:
        for (final sub in _taskMap[industry]!.keys) {
          if (_tasks.contains(sub)) {
            _industryTasks.add(industry);
            break innerLoop;
          }
          if (_tasks
              .any((element) => _taskMap[industry]![sub]!.contains(element))) {
            _industryTasks.add(industry);
            break innerLoop;
          }
        }
      }

      _industryList = [...tempIndustryList];

      log("TASKMAP: $_taskMap");
      log("INDUSTRY TASKS: $_industryTasks");
    } catch (e) {
      log("ERROR:$e");
    }

    notifyListeners();
  }

  Future<void> getSupplierProfileById() async {
    //

    var service = context.read<MainService>();
    final response = await service.getSupplierInfo();

    if (response != null) {
      if (response.success!) {
        //

        _partnerJobtypes = SupplierInfoModel.fromJson(
            Map<String, dynamic>.from(response.data));

        _getCompany();

        _tasks.clear();
        _taskTemplate.clear();

        if (_partnerJobtypes != null) {
          //

          _tasks = [...(_partnerJobtypes?.cPartnerTaskProfile ?? [])];
          _taskTemplate = [...(_partnerJobtypes?.cPartnerTaskProfile ?? [])];

          log("TASKS: $_tasks");
          log("TASKS TEMPLATE : $_taskTemplate");

          notifyListeners();

          //
        }

        //
      }
    }
  }

  Future<void> _getCompany() async {
    if (_partnerJobtypes != null && _partnerJobtypes!.bCrmCompany != null) {
      company = _partnerJobtypes!.bCrmCompany!;
    }
    if (_company != null && _company!.enterprise != null) {
      currentEnterprise = _company!.enterprise!;
    }
    if (_currentEnterprise != null) {
      switch (_currentEnterprise) {
        case 1:
          enterpriseBoxValues = [true, false];
          break;
        case 2:
          enterpriseBoxValues = [false, true];
          break;
        default:
          enterpriseBoxValues = [true, true];
          break;
      }
    }
  }

  Future<MinboligApiResponse?> newproducttask() async {
    var service = context.read<SettingsService>();

    return await service.savePartnerTaskProfile(
      categories: _tasks,
      sort: convertToSortListCRM(),
      taskTypes: [],
    );
  }

  List<Sort> convertToSortListCRM() {
    List<Sort> items = [];

    for (var element in _tasks) {
      items.add(Sort(id: element));
    }

    return items;
  }

  List<SubIndustries?> getInternalSubIndustries(
      {required int? parentId, required List<SubIndustries?> subinds}) {
    List<SubIndustries?> internal = [];

    internal =
        subinds.where((element) => element?.parentId == parentId).toList();
    return internal;
  }

  List<SubIndustries?> getTopSubIndustries(
      {required List<SubIndustries?>? subinds}) {
    List<SubIndustries?> tops = [];

    tops = subinds!.where((element) => element!.parentId == 0).toList();

    return tops;
  }

  TaskTypes getTaskTypesCRM({required int index}) {
    var subId = _tasks[index];
    TaskTypes taskTypes = TaskTypes();

    for (var item in _industryList) {
      for (var sub in item.subIndustries!) {
        if (sub.subIndustrId == subId) {
          taskTypes = TaskTypes(
              subcategoryName: sub.subIndustryName,
              subcategoryId: sub.subcategoryId);
        }
      }
    }

    return taskTypes;
  }

  bool isSubIndustrySelected(int? id, List<SubIndustries?>? subCategories) {
    bool isSelected = false;
    for (var item in _industryList) {
      for (var tasks in item.subIndustries!) {
        if (tasks.subIndustrId == id) {
          var internal =
              getInternalSubIndustries(parentId: id, subinds: subCategories!);
          var internalList = internal.map((e) => e!.subIndustrId).toList();

          if (internalList.isNotEmpty) {
            var list =
                partnerJobtypes?.cPartnerTaskProfile?.map((e) => e).toList() ??
                    [];

            for (var chosenItem in list) {
              if (internalList.contains(chosenItem)) {
                isSelected = true;
                break;
              }
            }
          } else {
            var list =
                partnerJobtypes?.cPartnerTaskProfile?.map((e) => e).toList() ??
                    [];

            isSelected = list.contains(id);
            break;
          }
        }
      }
    }

    return isSelected;
  }

  bool isTaskTypeSelected(int? id) {
    var list =
        partnerJobtypes?.cPartnerTaskProfile?.map((e) => e).toList() ?? [];

    if (list.isEmpty) {
      return false;
    }

    return list.contains(id);
  }

  bool isIndustrySelectedNew(Data product) {
    bool isSelected = false;

    var list = tasks.map((e) => e).toList();

    if (list.isNotEmpty) {
      for (var item in product.subIndustries!) {
        if (item.taskTypes?.isNotEmpty ?? false) {
          if (isSubIndustrySelected(item.subIndustrId, product.subIndustries)) {
            isSelected = true;
            break;
          }
        } else {
          if (list.contains(item.subcategoryId)) {
            isSelected = true;
            break;
          }
        }
      }
    }

    return isSelected;
  }
}
