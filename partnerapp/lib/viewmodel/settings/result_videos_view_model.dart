import 'dart:io';

import 'package:Haandvaerker.dk/model/partner/supplier_info_file_model.dart';
import 'package:Haandvaerker.dk/services/settings/settings_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ResultVideosViewModel extends BaseViewModel {
  ResultVideosViewModel({required this.context});
  final BuildContext context;

  List<SupplierInfoFileModel> _value = [];
  bool _busy = false;
  bool _uploadingVideo = false;

  List<SupplierInfoFileModel> get value => _value;
  bool get isBusy => _busy;
  bool get isUploadingImage => _uploadingVideo;

  void reset() {
    _value = [];
    _busy = false;
    _uploadingVideo = false;

    notifyListeners();
  }

  set value(List<SupplierInfoFileModel> videos) {
    _value = videos;
    notifyListeners();
  }

  Future<void> uploadProfileVideo({required File video}) async {
    _uploadingVideo = true;
    notifyListeners();

    final service = context.read<SettingsService>();

    final response = await service.uploadProfileVideo(video: video);

    final videos = <SupplierInfoFileModel>[...value];

    videos.add(response);

    _value = videos;

    _uploadingVideo = false;
    notifyListeners();
  }

  Future<void> deleteProfileVideo({required int videoId}) async {
    _busy = true;
    notifyListeners();
    final service = context.read<SettingsService>();
    await service.deleteProfileVideo(videoId: videoId);

    final videos = <SupplierInfoFileModel>[...value];

    videos.removeWhere((element) => element.id == videoId);
    _value = videos;

    _busy = false;
    notifyListeners();
  }
}
