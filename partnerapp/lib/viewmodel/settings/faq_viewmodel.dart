import 'package:Haandvaerker.dk/model/faq/faq_model.dart';
import 'package:Haandvaerker.dk/services/main/main_service.dart';
// import 'package:Haandvaerker.dk/services/main_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
// import 'package:provider/provider.dart';

class FaqViewModel extends BaseViewModel {
  FaqViewModel({required this.context});

  final BuildContext context;

  List<FAQModel> _faqItems = <FAQModel>[];
  List<FAQModel> get faqItems => _faqItems;

  void reset() {
    _faqItems = [];

    notifyListeners();
  }

  set faqItems(List<FAQModel> items) {
    _faqItems = items;
    notifyListeners();
  }

  Future<void> initFaqItems() async {
    final service = context.read<MainService>();
    setBusy(true);
    final response = await service.getFAQs();
    if (response != null) {
      if (response.data != null) {
        faqItems = FAQModel.fromCollection(response.data);
      }
    }
    setBusy(false);
  }

  List<FAQModel> filter(String? filter) {
    List<FAQModel> temp = [];

    if (faqItems.isNotEmpty) {
      for (var item in faqItems.where((element) =>
          element.questionDA!.toLowerCase().contains(filter!.toLowerCase()))) {
        if (!temp.contains(item)) {
          temp.add(item);
        }
      }

      for (var item in faqItems.where((element) =>
          element.answerDA!.toLowerCase().contains(filter!.toLowerCase()))) {
        if (!temp.contains(item)) {
          temp.add(item);
        }
      }
    }

    return temp;
  }
}
