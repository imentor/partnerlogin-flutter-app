import 'dart:convert';
import 'dart:developer';
import 'dart:ui';

import 'package:Haandvaerker.dk/model/create_offer/list_price_partner_ai_v2_model.dart';
import 'package:Haandvaerker.dk/model/create_offer/list_price_partner_ai_v3_model.dart';
import 'package:Haandvaerker.dk/model/create_offer/offer_response_model.dart';
import 'package:Haandvaerker.dk/model/customer_minbolig/list_price_minbolig_model.dart';
import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/offer_versions_list_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/services/booking_wizard/booking_wizard_service.dart';
import 'package:Haandvaerker.dk/services/offer/offer_service.dart';
import 'package:Haandvaerker.dk/services/settings/settings_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/recommendations/widgets_recommendations_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/settings/job_types_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_signaturepad/signaturepad.dart';

class CreateOfferViewModel extends BaseViewModel {
  CreateOfferViewModel({required this.context});
  final BuildContext context;

  PartnerJobModel? _activeJob;
  PartnerJobModel? get activeJob => _activeJob;

  List<ListPricePartnerAiV2Model> _listPricePartnerAiV2 = [];
  List<ListPricePartnerAiV2Model> get listPricePartnerAiV2 =>
      _listPricePartnerAiV2;

  List<ListPricePartnerAiV3Model> _listPricePartnerAiV3 = [];
  List<ListPricePartnerAiV3Model> get listPricePartnerAiV3 =>
      _listPricePartnerAiV3;

  List<List<ListPricePartnerAiV2Model>> _primaryOfferList = [];
  List<List<ListPricePartnerAiV2Model>> get primaryOfferList =>
      _primaryOfferList;

  List<List<ListPricePartnerAiV3Model>> _primaryOfferListV3 = [];
  List<List<ListPricePartnerAiV3Model>> get primaryOfferListV3 =>
      _primaryOfferListV3;

  List<List<ListPricePartnerAiV2Model>> _secondaryOfferList = [];
  List<List<ListPricePartnerAiV2Model>> get secondaryOfferList =>
      _secondaryOfferList;

  List<List<ListPricePartnerAiV3Model>> _secondaryOfferListV3 = [];
  List<List<ListPricePartnerAiV3Model>> get secondaryOfferListV3 =>
      _secondaryOfferListV3;

  List<int> _indexesToEdit = [];
  List<int> get indexesToEdit => _indexesToEdit;

  Map<String, TextEditingController> _controllers =
      <String, TextEditingController>{};
  Map<String, TextEditingController> get controllers => _controllers;

  Map<String, TextEditingController> _utilityControllers =
      <String, TextEditingController>{};
  Map<String, TextEditingController> get utilityControllers =>
      _utilityControllers;

  double _subValueWithoutVat = 0.0;
  double get subValueWithoutVat => _subValueWithoutVat;

  double _vatValue = 0.0;
  double get vatValue => _vatValue;

  double _totalValue = 0.0;
  double get totalValue => _totalValue;

  int? _numberOfWeeksToFinish;
  int? get numberOfWeeksToFinish => _numberOfWeeksToFinish;

  String _startDate = '';
  String get startDate => _startDate;

  String _convertedDate = '';
  String get convertedDate => _convertedDate;

  String _offerSignatureValid = '30';
  String get offerSignatureValid => _offerSignatureValid;

  List<int> _offerReviewId = <int>[];
  List<int> get offerReviewId => _offerReviewId;

  bool _sendingRequestOffer = false;
  bool get sendingRequestOffer => _sendingRequestOffer;

  bool _saveAsDefaultText = false;
  bool get saveAsDefaultText => _saveAsDefaultText;

  bool _confirmConditions = false;
  bool get confirmConditions => _confirmConditions;

  String _signature = '';
  String get signature => _signature;

  bool _uploadUpdateOfferAndList = false;
  bool get uploadUpdateOfferAndList => _uploadUpdateOfferAndList;

  bool _popMyCustomer = false;
  bool get popMyCustomer => _popMyCustomer;

  bool _isSigned = false;
  bool get isSigned => _isSigned;

  bool _signatureValidate = false;
  bool get signatureValidate => _signatureValidate;

  bool _isOfferUploaded = false;
  bool get isOfferUploaded => _isOfferUploaded;

  bool _isNewOfferVersion = false;
  bool get isNewOfferVersion => _isNewOfferVersion;

  bool _isOfferListLoading = false;
  bool get isOfferListLoading => _isOfferListLoading;

  String _conditionsText = '';
  String get conditionsText => _conditionsText;

  OfferVersionListModel? _offerVersionList;
  OfferVersionListModel? get offerVersionList => _offerVersionList;

  List<String> _jobLists = [];
  List<String> get jobLists => _jobLists;

  Map<String, dynamic> _templateDescriptions = {};
  Map<String, dynamic> get templateDescriptions => _templateDescriptions;

  Map<String, dynamic> _editableDescriptions = {};
  Map<String, dynamic> get editableDescriptions => _editableDescriptions;

  Map<String, dynamic> _updatedDescriptions = {};
  Map<String, dynamic> get updatedDescriptions => _updatedDescriptions;

  Map<String, dynamic> _deletedDescriptions = {};
  Map<String, dynamic> get deletedDescriptions => _deletedDescriptions;

  Map<String, dynamic> _addedDescriptions = {};
  Map<String, dynamic> get addedDescriptions => _addedDescriptions;

  Map<String, dynamic> _addDescription = {};
  Map<String, dynamic> get addDescription => _addDescription;

  List<dynamic> _industryFullMap = [];
  List<dynamic> get industryFullMap => _industryFullMap;

  int _currentWizardStep = 0;
  int get currentWizardStep => _currentWizardStep;

  bool _toUpdateOffer = false;
  bool get toUpdateOffer => _toUpdateOffer;

  final List<GlobalKey<FormState>> _formKeys = [
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
  ];
  List<GlobalKey<FormState>> get formKeys => _formKeys;

  bool _isNewReserve = false;
  bool get isNewReserve => _isNewReserve;

  RequestResponse _createOfferRequest = RequestResponse.idle;
  RequestResponse get createOfferRequest => _createOfferRequest;

  GlobalKey<SfSignaturePadState>? _signaturePadKey;
  GlobalKey<SfSignaturePadState>? get signaturePadKey => _signaturePadKey;

  String _csUserId = '';
  String get csUserId => _csUserId;

  int _offerVersionId = 0;
  int get offerVersionId => _offerVersionId;

  bool _hasAcceptedAiPopup = false;
  bool get hasAcceptedAiPopup => _hasAcceptedAiPopup;

  set hasAcceptedAiPopup(bool value) {
    _hasAcceptedAiPopup = value;
    notifyListeners();
  }

  final _rowBolig = [
    {
      'title': '',
      'description': '',
      'unit': '',
      'value': '',
      'quantity': '',
    }
  ];

  void setValuesToDefault() {
    _controllers.clear();
    _utilityControllers.clear();
    _primaryOfferListV3.clear();
    _secondaryOfferListV3.clear();
    _numberOfWeeksToFinish = null;
    _startDate = '';
    _subValueWithoutVat = 0.0;
    _vatValue = 0.0;
    _totalValue = 0.0;
    _sendingRequestOffer = false;
    _saveAsDefaultText = false;
    _confirmConditions = false;
    _popMyCustomer = false;
    _signature = '';
    _offerSignatureValid = '30';
    _offerReviewId = [];
    _uploadUpdateOfferAndList = false;
    _isSigned = false;
    _signatureValidate = false;
    _currentWizardStep = 0;
    _createOfferRequest = RequestResponse.idle;
    _signaturePadKey = GlobalKey();
    _csUserId = '';
    _offerVersionId = 0;

    notifyListeners();
  }

  void reset() {
    _controllers.clear();
    _utilityControllers.clear();
    _primaryOfferListV3.clear();
    _secondaryOfferListV3.clear();
    _numberOfWeeksToFinish = null;
    _startDate = '';
    _subValueWithoutVat = 0.0;
    _vatValue = 0.0;
    _totalValue = 0.0;
    _sendingRequestOffer = false;
    _saveAsDefaultText = false;
    _confirmConditions = false;
    _popMyCustomer = false;
    _signature = '';
    _offerSignatureValid = '30';
    _offerReviewId = [];
    _uploadUpdateOfferAndList = false;
    _isSigned = false;
    _signatureValidate = false;
    _currentWizardStep = 0;
    _createOfferRequest = RequestResponse.idle;
    _signaturePadKey = GlobalKey();
    _csUserId = '';
    _offerVersionId = 0;

    notifyListeners();
  }

  set offerVersionId(int value) {
    _offerVersionId = value;

    notifyListeners();
  }

  set csUserId(String value) {
    _csUserId = value;

    notifyListeners();
  }

  set listPricePartnerAiV2(
      List<ListPricePartnerAiV2Model> listPricePartnerAiV2) {
    _listPricePartnerAiV2 = listPricePartnerAiV2;
    notifyListeners();
  }

  set listPricePartnerAiV3(
      List<ListPricePartnerAiV3Model> listPricePartnerAiV3) {
    _listPricePartnerAiV3 = listPricePartnerAiV3;
    notifyListeners();
  }

  set primaryOfferList(List<List<ListPricePartnerAiV2Model>> primaryOfferList) {
    _primaryOfferList = primaryOfferList;
    notifyListeners();
  }

  set primaryOfferListV3(
      List<List<ListPricePartnerAiV3Model>> primaryOfferList) {
    _primaryOfferListV3 = primaryOfferList;
    notifyListeners();
  }

  set secondaryOfferList(
      List<List<ListPricePartnerAiV2Model>> secondaryOfferList) {
    _secondaryOfferList = secondaryOfferList;
    notifyListeners();
  }

  set secondaryOfferListV3(
      List<List<ListPricePartnerAiV3Model>> secondaryOfferList) {
    _secondaryOfferListV3 = secondaryOfferList;
    notifyListeners();
  }

  set activeJob(PartnerJobModel? job) {
    _activeJob = job;
    notifyListeners();
  }

  set controllers(Map<String, TextEditingController> controllers) {
    _controllers = controllers;
    notifyListeners();
  }

  set utilityControllers(Map<String, TextEditingController> controllers) {
    _utilityControllers = controllers;
    notifyListeners();
  }

  set indexesToEdit(List<int> indexes) {
    _indexesToEdit = indexes;
    notifyListeners();
  }

  void _initCalculationValues() {
    _subValueWithoutVat = 0.0;
    _vatValue = 0.0;
    _totalValue = 0.0;
  }

  void updateSubValueWithoutVat() {
    double value = 0;
    for (final controller in _controllers.values) {
      if (controller.text.isNotEmpty) {
        value += double.parse(
            controller.text.replaceAll('.', '').replaceAll(',', '.').trim());
      }
    }

    _subValueWithoutVat = value;
    _vatValue = value * 0.25;
    _totalValue = value + _vatValue;
    notifyListeners();
  }

  set numberOfWeeksToFinish(int? numberOfWeeks) {
    _numberOfWeeksToFinish = numberOfWeeks;
    notifyListeners();
  }

  set startDate(String startDate) {
    _startDate = startDate;
    notifyListeners();
  }

  set isOfferListLoading(bool isOfferListLoading) {
    _isOfferListLoading = isOfferListLoading;
    notifyListeners();
  }

  set isNewOfferVersion(bool isNewOfferVersion) {
    _isNewOfferVersion = isNewOfferVersion;
    notifyListeners();
  }

  set sendingRequestOffer(bool value) {
    _sendingRequestOffer = value;
    notifyListeners();
  }

  set saveAsDefaultText(bool value) {
    _saveAsDefaultText = value;
    notifyListeners();
  }

  set confirmConditions(bool value) {
    _confirmConditions = value;
    notifyListeners();
  }

  set signature(String signature) {
    _signature = signature;
    notifyListeners();
  }

  set offerSignatureValid(String value) {
    _offerSignatureValid = value;
    notifyListeners();
  }

  set offerReviewId(List<int> value) {
    _offerReviewId = value;
    notifyListeners();
  }

  set uploadUpdateOfferAndList(bool value) {
    _uploadUpdateOfferAndList = value;
    notifyListeners();
  }

  set popMyCustomer(bool value) {
    _popMyCustomer = value;
    notifyListeners();
  }

  set isSigned(bool isSigned) {
    _isSigned = isSigned;
    notifyListeners();
  }

  set signatureValidate(bool validate) {
    _signatureValidate = validate;
    notifyListeners();
  }

  set conditionsText(String text) {
    _conditionsText = text;
    notifyListeners();
  }

  set editableDescriptions(Map<String, dynamic> data) {
    _editableDescriptions = data;
    notifyListeners();
  }

  set templateDescriptions(Map<String, dynamic> data) {
    _templateDescriptions = data;
    notifyListeners();
  }

  set updatedDescriptions(Map<String, dynamic> data) {
    _updatedDescriptions = data;
    notifyListeners();
  }

  set deletedDescriptions(Map<String, dynamic> data) {
    _deletedDescriptions = data;
    notifyListeners();
  }

  set addedDescriptions(Map<String, dynamic> data) {
    _addedDescriptions = data;
    notifyListeners();
  }

  set addDescription(Map<String, dynamic> add) {
    _addDescription = add;
    notifyListeners();
  }

  set currentWizardStep(int step) {
    _currentWizardStep = step;
    notifyListeners();
  }

  set toUpdateOffer(bool update) {
    _toUpdateOffer = update;
    notifyListeners();
  }

  set isNewReserve(bool reserve) {
    _isNewReserve = reserve;
    notifyListeners();
  }

  set createOfferRequest(RequestResponse request) {
    _createOfferRequest = request;
    notifyListeners();
  }

  Future<Map<String, dynamic>?> getAiCalculationPriceV3(
      {required String productType, required String industry}) async {
    setBusy(true);

    final service = context.read<BookingWizardService>();

    String subTask = '';

    Map<dynamic, dynamic> industryMap = industryFullMap.firstWhere(
      (value) => value['BRANCHE_ID'] == int.parse(industry),
      orElse: () => {},
    );
    if (industryMap.containsKey('SubIndustries')) {
      List<dynamic> subIndustryList = industryMap['SubIndustries'];

      subTask = subIndustryList
          .map((value) => value['SUBCATEGORY_ID'])
          .toList()
          .join(',');
    }

    final response = await service.getAiCalculationPriceV3(
        addressId: activeJob!.projectAddressId!,
        adgangsAddressId: activeJob!.projectAdgangsAddressId!,
        contactId: activeJob!.customerId!,
        industry: industry,
        productType: productType,
        subTasks: subTask);

    setBusy(false);

    return response?.data;
  }

  Future<void> getListPricePartnerAiV2({
    required int parentProjectId,
    required int contactId,
  }) async {
    var createOfferService = context.read<OfferService>();
    final jobTypesVm = context.read<JobTypesViewModel>();
    final companyOfferNote = context.read<WidgetsViewModel>().companyOfferText;

    setBusy(true);

    if (companyOfferNote.isNotEmpty) {
      saveAsDefaultText = true;
    } else {
      saveAsDefaultText = false;
    }

    var listPricesV2 = await createOfferService.getListPricePartnerAiV2(
        parentProjectId: parentProjectId, contactId: contactId);

    await jobTypesVm.getSupplierProfileById();

    _listPricePartnerAiV2 = listPricesV2;

    _primaryOfferList.clear();
    _secondaryOfferList.clear();
    _jobLists.clear();
    _initCalculationValues();
    _currentWizardStep = 0;

    if ((_activeJob?.contractType ?? '') == 'Fagentreprise') {
      List<String> jobIndustries = [];

      for (final job in (activeJob?.jobsList ?? [])) {
        if (job is List) {
          for (final item in job) {
            jobIndustries.add(item.toString().split(':').first);
          }
        } else {
          jobIndustries.add(job.toString().split(':').first);
        }
      }

      _jobLists = jobIndustries;

      final jobListFilter = _listPricePartnerAiV2
          .where((element) => jobIndustries.contains(element.jobIndustry))
          .toList();

      final secondaryJobList = _listPricePartnerAiV2
          .where((element) => !jobIndustries.contains(element.jobIndustry))
          .toList();

      List<String> primaryLayerFilter = [];

      List<String> secondaryLayerFilter = [];

      primaryLayerFilter = primaryLayerFilter.toSet().toList();
      secondaryLayerFilter = secondaryLayerFilter.toSet().toList();

      List<String> uniqueEnterprises = _listPricePartnerAiV2
          .where((e) => e.industry != null)
          .map((ea) => ea.industry != null ? ea.industry! : '')
          .toSet()
          .toList();

      for (final enterprise in uniqueEnterprises) {
        if (primaryLayerFilter.isEmpty) {
          _primaryOfferList.add(jobListFilter
              .where((element) => element.industry == enterprise)
              .toList());

          _secondaryOfferList.add(secondaryJobList
              .where((element) => element.industry == enterprise)
              .toList());
        } else {
          _primaryOfferList.add(
            _listPricePartnerAiV2
                .where((element) => element.industry == enterprise)
                .toList()
                .where((element) => primaryLayerFilter.contains(element.id))
                .toList(),
          );
          _secondaryOfferList.add(
            _listPricePartnerAiV2
                .where((element) => element.industry == enterprise)
                .toList()
                .where((element) => !secondaryLayerFilter.contains(element.id))
                .toList(),
          );
        }
      }
    } else {
      // bool hasOfferAccepted = false;

      // hasOfferAccepted = _listPricePartnerAiV2.any((e) {
      //   if (e.partnerIds != null) if (e.partnerIds
      //       .toString()
      //       .contains(_activeJob.partnerId.toString())) return true;

      //   return false;
      // });

      List<String> jobIndustries = [];

      for (final job in (activeJob?.jobsList ?? [])) {
        if (job is List) {
          for (final item in job) {
            jobIndustries.add(item.toString().split(':').first);
          }
        } else {
          jobIndustries.add(job.toString().split(':').first);
        }
      }

      if (jobIndustries.length != 1 &&
          jobIndustries.contains("1062") &&
          _activeJob!.contractType == "Hovedentreprise") {
        jobIndustries.removeWhere((element) => element == "1062");
      }

      _jobLists = jobIndustries;

      final jobListFilter = _listPricePartnerAiV2
          .where((element) => jobIndustries.contains(element.jobIndustry))
          .toList();

      List<String> uniqueEnterprises = jobListFilter
          .where((e) => e.industry != null)
          .map((e) => (e.industry ?? ''))
          .toSet()
          .toList();

      // if (hasOfferAccepted) {
      for (final enterprise in uniqueEnterprises) {
        _primaryOfferList.add(
          jobListFilter
              .where((element) => element.industry == enterprise)
              .toList(),
        );
        _secondaryOfferList = [];
      }
      // } else {
      //   for (final enterprise in uniqueEnterprises) {
      //     _secondaryOfferList.add(
      //       _listPricePartnerAiV2
      //           .where((element) => element.industry == enterprise)
      //           .toList(),
      //     );
      //     _primaryOfferList = [];
      //   }
      // }
    }

    _controllers.clear();
    for (final offer in _primaryOfferList) {
      for (var element in offer) {
        _controllers['${element.jobIndustry}-${element.industry}'] =
            TextEditingController();
      }
    }

    _utilityControllers.clear();
    _utilityControllers.addAll({
      'days': TextEditingController(),
      'date': TextEditingController(),
      'standardText': TextEditingController(text: companyOfferNote),
    });

    _conditionsText = '';
    _primaryOfferList.removeWhere((element) => element.isEmpty);
    _secondaryOfferList.removeWhere((element) => element.isEmpty);

    _editableDescriptions.clear();
    _templateDescriptions.clear();
    _addedDescriptions.clear();
    _updatedDescriptions.clear();
    _deletedDescriptions.clear();
    _addDescription.clear();

    for (final offers in _primaryOfferList) {
      for (final offer in offers) {
        _editableDescriptions.putIfAbsent(
          '${offer.industry}_${offer.jobIndustry}',
          () => offer.series == null
              ? <String>[]
              : offer.series is List
                  ? offer.series
                  : <String>[...offer.series.values],
        );
        _templateDescriptions.putIfAbsent(
          '${offer.industry}_${offer.jobIndustry}',
          () => offer.series == null
              ? <String>[]
              : offer.series is List
                  ? offer.series
                  : <String>[...offer.series.values],
        );
        _addDescription.putIfAbsent(
            '${offer.industry}_${offer.jobIndustry}', () => false);
      }
    }

    log('EDITABLE DESCRIPTIONS: $_editableDescriptions');
    log('TEMPLATE DESCRIPTIONS: $_templateDescriptions');

    notifyListeners();
    setBusy(false);
  }

  Future<void> getListPricePartnerAiV3({
    required int parentProjectId,
    required int contactId,
  }) async {
    var createOfferService = context.read<OfferService>();
    final companyOfferNote = context.read<WidgetsViewModel>().companyOfferText;
    final settingService = context.read<SettingsService>();

    setBusy(true);

    if (industryFullMap.isEmpty) {
      final industryResponse = await settingService.getTaskTypesFromCRMnoId();

      if (industryResponse != null) {
        _industryFullMap = industryResponse.data as List<dynamic>;

        log("message _industryFullMap $_industryFullMap");
      }
    }

    if (companyOfferNote.isNotEmpty) {
      saveAsDefaultText = true;
    } else {
      saveAsDefaultText = false;
    }

    final payload = {
      'projectId': parentProjectId,
      'contactId': contactId,
    };

    final responses = await Future.wait([
      createOfferService.getListPricePartnerAiV3(payload: payload),
    ]);

    if (responses[0] == null) {
      setBusy(false);
      return;
    }

    _listPricePartnerAiV3 = responses[0] as List<ListPricePartnerAiV3Model>;

    _primaryOfferListV3.clear();
    _secondaryOfferListV3.clear();
    _jobLists.clear();
    _initCalculationValues();
    _currentWizardStep = 0;

    if ((_activeJob?.contractType ?? '') == 'Fagentreprise') {
      List<String> jobIndustries = [];

      for (final job in (activeJob?.jobsList ?? [])) {
        if (job is List) {
          for (final item in job) {
            jobIndustries.add(item.toString().split(':').first);
          }
        } else {
          jobIndustries.add(job.toString().split(':').first);
        }
      }

      log('jobIndustries: $jobIndustries');
      _jobLists = jobIndustries;

      final jobListFilter = _listPricePartnerAiV3
          .where((element) => element.archive == 0)
          .where((element) =>
              jobIndustries.contains(element.jobIndustry.toString()))
          .toList();

      final secondaryJobList = _listPricePartnerAiV3
          .where((element) => element.archive == 0)
          .where((element) =>
              !jobIndustries.contains(element.jobIndustry.toString()))
          .toList();

      log('jobListFilter: $jobListFilter');
      log('secondaryJobList: $secondaryJobList');

      List<String> primaryLayerFilter = [];

      List<String> secondaryLayerFilter = [];

      primaryLayerFilter = primaryLayerFilter.toSet().toList();
      secondaryLayerFilter = secondaryLayerFilter.toSet().toList();

      log('primaryLayerFilter: $primaryLayerFilter');
      log('secondaryLayerFilter: $secondaryLayerFilter');

      List<int> uniqueEnterprises = _listPricePartnerAiV3
          .where((element) => element.archive == 0)
          .where((e) => e.industry != null)
          .map((ea) => ea.industry != null ? ea.industry! : 0)
          .toSet()
          .toList();

      log('jobIndustries: $uniqueEnterprises');

      for (final enterprise in uniqueEnterprises) {
        if (primaryLayerFilter.isEmpty) {
          _primaryOfferListV3.add(jobListFilter
              .where((element) => element.industry == enterprise)
              .toList());

          _secondaryOfferListV3.add(secondaryJobList
              .where((element) => element.industry == enterprise)
              .toList());
        } else {
          _primaryOfferListV3.add(
            _listPricePartnerAiV3
                .where((element) => element.archive == 0)
                .where((element) => element.industry == enterprise)
                .toList()
                .where((element) =>
                    primaryLayerFilter.contains(element.id.toString()))
                .toList(),
          );
          _secondaryOfferListV3.add(
            _listPricePartnerAiV3
                .where((element) => element.archive == 0)
                .where((element) => element.industry == enterprise)
                .toList()
                .where((element) =>
                    !secondaryLayerFilter.contains(element.id.toString()))
                .toList(),
          );
        }
      }
    } else {
      List<String> jobIndustries = [];

      for (final job in (activeJob?.jobsList ?? [])) {
        if (job is List) {
          for (final item in job) {
            jobIndustries.add(item.toString().split(':').first);
          }
        } else {
          jobIndustries.add(job.toString().split(':').first);
        }
      }

      if (jobIndustries.length != 1 &&
          jobIndustries.contains("1062") &&
          _activeJob!.contractType == "Hovedentreprise") {
        jobIndustries.removeWhere((element) => element == "1062");
      }

      _jobLists = jobIndustries;

      final jobListFilter = _listPricePartnerAiV3
          .where((element) => element.archive == 0)
          .where((element) =>
              jobIndustries.contains(element.jobIndustry.toString()))
          .toList();

      List<Object> uniqueEnterprises = jobListFilter
          .where((e) => e.industry != null)
          .map((e) => (e.industry ?? ''))
          .toSet()
          .toList();

      for (final enterprise in uniqueEnterprises) {
        _primaryOfferListV3.add(
          jobListFilter
              .where((element) => element.industry == enterprise)
              .toList(),
        );
        _secondaryOfferListV3 = [];
      }
    }

    _controllers.clear();
    for (final offer in _primaryOfferListV3) {
      for (var element in offer) {
        _controllers['${element.jobIndustry}-${element.industry}'] =
            TextEditingController();
      }
    }

    _utilityControllers.clear();
    _utilityControllers.addAll({
      'days': TextEditingController(),
      'date': TextEditingController(),
      'standardText': TextEditingController(text: companyOfferNote),
    });

    _conditionsText = '';
    _primaryOfferListV3.removeWhere((element) => element.isEmpty);
    _secondaryOfferListV3.removeWhere((element) => element.isEmpty);

    _editableDescriptions.clear();
    _templateDescriptions.clear();
    _addedDescriptions.clear();
    _updatedDescriptions.clear();
    _deletedDescriptions.clear();
    _addDescription.clear();

    for (final offers in _primaryOfferListV3) {
      for (final offer in offers) {
        _editableDescriptions.putIfAbsent(
          '${offer.industry}_${offer.jobIndustry}',
          () => offer.series == null
              ? <String>[]
              : offer.series is List
                  ? offer.series
                  : <String>[...offer.series.values],
        );
        _templateDescriptions.putIfAbsent(
          '${offer.industry}_${offer.jobIndustry}',
          () => offer.series == null
              ? <String>[]
              : offer.series is List
                  ? offer.series
                  : <String>[...offer.series.values],
        );
        _addDescription.putIfAbsent(
            '${offer.industry}_${offer.jobIndustry}', () => false);
      }
    }

    log('EDITABLE DESCRIPTIONS: $_editableDescriptions');
    log('TEMPLATE DESCRIPTIONS: $_templateDescriptions');

    notifyListeners();
    setBusy(false);
  }

  Future<void> getListPriceMinbolig({
    required int projectId,
    required int contactId,
  }) async {
    final offerService = context.read<OfferService>();
    final jobTypesVm = context.read<JobTypesViewModel>();
    final settingService = context.read<SettingsService>();

    setBusy(true);

    if (industryFullMap.isEmpty) {
      final industryResponse = await settingService.getTaskTypesFromCRMnoId();

      if (industryResponse != null) {
        _industryFullMap = industryResponse.data as List<dynamic>;
      }
    }

    final listPriceResponse = await offerService.getListPriceMinbolig(
        contactId: contactId, projectId: projectId);
    await jobTypesVm.getSupplierProfileById();

    _primaryOfferListV3.clear();
    _secondaryOfferListV3.clear();
    _initCalculationValues();

    if (listPriceResponse != null) {
      if (listPriceResponse.success!) {
        final minBoligResponse = MinboligListPrice.fromJson(
            listPriceResponse.data as Map<String, dynamic>);

        List<String> jobLists = [];

        for (final job in activeJob!.jobsList as List) {
          if (job is List) {
            for (final item in job) {
              jobLists.add(item.toString().split(':').first);
            }
          } else {
            jobLists.add(job.toString().split(':').first);
          }
        }

        if (jobLists.length != 1 &&
            jobLists.contains("1062") &&
            _activeJob!.contractType == "Hovedentreprise") {
          jobLists.removeWhere((element) => element == "1062");
        }

        final minboligResults = minBoligResponse.result!
            .where((element) =>
                element.partnerId == activeJob!.partnerId.toString())
            .toList();
        List<ListPricePartnerAiV3Model> listPriceV3 = minboligResults
            .map(
              (listPrice) => ListPricePartnerAiV3Model(
                archive: int.parse(listPrice.archive ?? '0'),
                id: int.parse(listPrice.id ?? '0'),
                caclulationId: int.parse(listPrice.calculationId ?? '0'),
                contactId: int.parse(listPrice.contactId ?? '0'),
                industry: int.parse(listPrice.industry ?? '0'),
                isDeleted: int.parse(listPrice.isDeleted ?? '0'),
                jobIndustry: int.parse(listPrice.jobIndustry ?? '0'),
                partnerIds: '',
                // pending: '',
                pendingDate: null,
                projectId: int.parse(listPrice.projectId ?? '0'),
                series: listPrice.series,
                // seriesOriginal: listPrice.seriesOriginal,
                updateText: '',
                created: listPrice.created,
              ),
            )
            .toList();

        final jobListFilter = listPriceV3
            .where(
                (element) => jobLists.contains(element.jobIndustry.toString()))
            .toList();

        List<int> uniqueEnterprises =
            jobListFilter.map((e) => e.industry!).toSet().toList();

        for (final enterprise in uniqueEnterprises) {
          _primaryOfferListV3.add(
            jobListFilter
                .where((element) => element.industry == enterprise)
                .toList(),
          );
          _secondaryOfferListV3 = [];
        }

        _controllers.clear();
        for (final offer in _primaryOfferListV3) {
          for (var element in offer) {
            _controllers['${element.jobIndustry}-${element.industry}'] =
                TextEditingController(
              text: minboligResults
                  .firstWhere(
                      (e) =>
                          e.jobIndustry == element.jobIndustry.toString() &&
                          e.industry == element.industry.toString(),
                      orElse: () => MinboligListPriceResult.defaults())
                  .total,
            );
            _subValueWithoutVat += double.parse(minboligResults
                .firstWhere(
                    (e) =>
                        e.jobIndustry == element.jobIndustry.toString() &&
                        e.industry == element.industry.toString(),
                    orElse: () => MinboligListPriceResult.defaults())
                .total!);
          }
        }

        _utilityControllers.clear();
        _utilityControllers.addAll({
          'days': TextEditingController(text: minboligResults.first.weeks),
          'date': TextEditingController(text: minboligResults.first.startDate),
          'standardText':
              TextEditingController(text: minboligResults.first.note),
        });

        _totalValue = _subValueWithoutVat * 1.25;
        _vatValue = _subValueWithoutVat * 0.25;
        _startDate = minboligResults.first.startDate!;
        _numberOfWeeksToFinish = int.parse(minboligResults.first.weeks!);

        _primaryOfferListV3.removeWhere((element) => element.isEmpty);
        _secondaryOfferListV3.removeWhere((element) => element.isEmpty);

        _editableDescriptions.clear();
        _templateDescriptions.clear();
        _addedDescriptions.clear();
        _updatedDescriptions.clear();
        _deletedDescriptions.clear();
        _addDescription.clear();

        for (final offers in _primaryOfferListV3) {
          for (final offer in offers) {
            _editableDescriptions.putIfAbsent(
              '${offer.industry}_${offer.jobIndustry}',
              () => offer.series == null
                  ? ''
                  : offer.series is List
                      ? offer.series
                      : <String>[...offer.series.values],
            );
            _templateDescriptions.putIfAbsent(
              '${offer.industry}_${offer.jobIndustry}',
              () => offer.series == null
                  ? ''
                  : offer.series is List
                      ? offer.series
                      : <String>[...offer.series.values],
            );
            _addDescription.putIfAbsent(
                '${offer.industry}_${offer.jobIndustry}', () => false);
          }
        }

        log('EDITABLE DESCRIPTIONS: $_editableDescriptions');
        log('TEMPLATE DESCRIPTIONS: $_templateDescriptions');

        notifyListeners();
        setBusy(false);
      }
    }

    //END
  }

  Future<RequestResponse> sendRequestOffer(
      {required Map<String, dynamic> payload}) async {
    var createOfferService = context.read<OfferService>();

    final response =
        await createOfferService.sendRequestOffer(payload: payload);

    if (response) return RequestResponse.success;
    return RequestResponse.failed;
  }

  Future<List<OfferPostResponseModel>> partnerOffer(
      {required Map<String, dynamic> payload}) async {
    var createOfferService = context.read<OfferService>();

    final response = await createOfferService.partnerOffer(payload: payload);

    return response;
  }

  Future<bool> partnerOfferVersion(
      {required Map<String, dynamic> offerVersionPayload}) async {
    final createOfferService = context.read<OfferService>();

    final response = await createOfferService.partnerOfferVersion(
        offerVersionPayload: offerVersionPayload);

    if (response != null) {
      return response.success ?? false;
    } else {
      return false;
    }
  }

  Future<void> getOfferById({required int offerId}) async {
    final createOfferService = context.read<OfferService>();
    final response = await createOfferService.getOfferById(offerId: offerId);

    if (response != null) {
      final responseData = response.data as Map<String, dynamic>;
      final responseContractId = responseData['CONTRACT_ID'];
      if (responseContractId.runtimeType == String) {
        responseData['CONTRACT_ID'] = int.tryParse(responseContractId);
      }

      _offerVersionList = OfferVersionListModel.fromJson(response.data);
    }
  }

  Future<bool> saveListPrice({required Map<String, dynamic> payload}) async {
    var createOfferService = context.read<OfferService>();

    final response = await createOfferService.saveListPrice(payload: payload);

    return response;
  }

  String getStandardTitle() {
    List<String> temp = [];

    final tenderVm = context.read<TenderFolderViewModel>();

    for (var ent in _primaryOfferListV3) {
      for (var job in ent) {
        final productType = tenderVm.wizardTexts
                .firstWhere(
                  (element) => element.producttypeid == job.industry.toString(),
                  orElse: () => AllWizardText.defaults(),
                )
                .producttypeDa ??
            '';
        final branch = tenderVm.fullIndustries
                .firstWhere(
                  (element) =>
                      element.brancheId.toString() ==
                      job.jobIndustry.toString(),
                  orElse: () => FullIndustry.defaults(),
                )
                .brancheDa ??
            '';
        temp.add('$productType-$branch');
      }
    }

    return temp.join(" ");
  }

  void convertDate() {
    String year = _startDate.split('-').first;
    String month = _startDate.split('-')[1];
    String day = _startDate.split('-').last;
    String monthTranslated = '';

    month = month.characters.first == '0' ? month.replaceAll('0', '') : month;

    switch (month) {
      case '1':
        monthTranslated = 'january';
        break;
      case '2':
        monthTranslated = 'february';
        break;
      case '3':
        monthTranslated = 'march';
        break;
      case '4':
        monthTranslated = 'april';
        break;
      case '5':
        monthTranslated = 'may';
        break;
      case '6':
        monthTranslated = 'june';
        break;
      case '7':
        monthTranslated = 'july';
        break;
      case '8':
        monthTranslated = 'august';
        break;
      case '9':
        monthTranslated = 'september';
        break;
      case '10':
        monthTranslated = 'october';
        break;
      case '11':
        monthTranslated = 'november';
        break;
      default:
        monthTranslated = 'december';
        break;
    }

    _convertedDate = '$day. $monthTranslated $year';
    notifyListeners();
  }

  String formatDate(String? date) {
    if (date != null && date.isNotEmpty) {
      final parsedDate = DateTime.parse(date);
      final year = parsedDate.year;
      final day = parsedDate.day;
      String month = parsedDate.month.toString();

      if (month.isNotEmpty && month.length == 1) {
        month = '0$month';
      }

      return '$day.$month.$year';
    }

    return '';
  }

  Future<MinboligApiResponse> updateOffer({
    required int offerId,
  }) async {
    final offerService = context.read<OfferService>();
    final response = await offerService.updateOffer(
      payload: {
        'offer_id': offerId,
        'subTotal': _subValueWithoutVat.ceil(),
        'total': _totalValue.ceil(),
        'vat': _vatValue.ceil(),
      },
    );
    return response;
  }

  Future<MinboligApiResponse> updateListPrice({
    required Map<String, dynamic> payload,
  }) async {
    final offerService = context.read<OfferService>();
    final response = await offerService.updateListPrice(payload: payload);
    return response;
  }

  Future<void> updateCompanyOfferNote({required String offerNote}) async {
    final offerService = context.read<OfferService>();
    context.read<WidgetsViewModel>().companyOfferText = offerNote;

    await offerService.updateCompanyOfferNote(offerNote: offerNote);
  }

  Future<void> createOffer() async {
    ///

    _isOfferUploaded = false;
    if (!_isSigned) {
      signatureValidate = true;
    } else {
      ///

      ///Convert signature [IMAGE] -> [BASE64]
      if (_signaturePadKey != null && _signaturePadKey?.currentState != null) {
        final data =
            await _signaturePadKey!.currentState!.toImage(pixelRatio: 1.0);
        await data.toByteData(format: ImageByteFormat.png).then((bytes) async {
          if (bytes != null) {
            final signature =
                'data:image/png;base64,${base64.encode(bytes.buffer.asUint8List())}';

            uploadUpdateOfferAndList = true;

            if (_toUpdateOffer) {
              await _updateOffer(signature: signature);
            } else {
              ///

              if (_activeJob!.contractType == 'Fagentreprise') {
                ///

                await _submitOffer(isFagen: true, signature: signature);

                ///

                ///
              } else {
                ///

                await _submitOffer(isFagen: false, signature: signature);

                ///
              }

              ///
            }
          }
        });
      }
    }
  }

  Future<void> _submitOffer({
    required bool isFagen,
    required String signature,
  }) async {
    final offerService = context.read<OfferService>();

    Map<String, dynamic> payload = {};
    Map<String, dynamic> offerVersionPayload = {};
    final currentJob = _activeJob!;

    String projectId = isFagen
        ? currentJob.projectId.toString()
        : currentJob.projectParentId != null
            ? currentJob.projectParentId != currentJob.projectId.toString()
                ? currentJob.projectId.toString()
                : currentJob.projectParentId
            : currentJob.projectId.toString();

    final note = _utilityControllers['standardText']!.text.trim();

    List<Map<String, dynamic>> payloads = [];

    for (final offer in _primaryOfferListV3) {
      for (final job in offer) {
        ///

        final String key = '${job.industry}_${job.jobIndustry}';
        final jobDescription = _setJobDescription(key: key);
        final submittedJobDescription = {
          'submitted': jobDescription['submitted'],
        };

        payloads.add({
          'project_id': currentJob.projectParentId ?? job.projectId,
          'sub_project_id':
              isFagen || (_jobLists.length == 1 && _jobLists.first == '1062')
                  ? currentJob.projectId
                  : 0,
          'contact_id': job.contactId,
          'calculation_id': 0,
          'industry_id': job.industry.toString(),
          'job_description': submittedJobDescription,
          'total': double.parse(
              _controllers['${job.jobIndustry}-${job.industry}']!
                  .text
                  .trim()
                  .replaceAll('.', '')
                  .replaceAll(',', '.')),
          'note': note,
          'job_industry_id': job.jobIndustry.toString(),
          'signature': signature,
          'weeks': _numberOfWeeksToFinish.toString(),
          'days': _numberOfWeeksToFinish.toString(),
          'start_date': _startDate,
          'offer_id': _activeJob!.offerId,
          'parent_offer_id': _activeJob!.offerId,
          'reviews_id': offerReviewId,
          'partner_id': _activeJob!.partnerId
        });

        ///
      }
    }

    payload['projectId'] = int.parse(projectId);
    payload['saveListPriceV2'] = payloads;
    payload['createdFrom'] = "partner-app";
    payload['expiry'] = int.parse(_offerSignatureValid);

    if (!isFagen && _jobLists.length == 1 && _jobLists.first == '1062') {
      payload['parentProjectId'] = currentJob.projectParentId!;
      payload['jobIndustry'] = '1062';
    }

    payload['offers'] = [
      {
        'title': 'Tilbud på projektnr. $projectId til ${getStandardTitle()}',
        'description': note.isEmpty
            ? 'Tilbud på projektnr. $projectId til ${getStandardTitle()}'
            : note,
        'subtotalPrice': _subValueWithoutVat,
        'vat': _vatValue,
        'totalPrice': _totalValue,
        'tasks': _rowBolig,
        'signature': signature,
        'createdFrom': 'partner-app',
      }
    ];

    if (_isNewOfferVersion) {
      offerVersionPayload['offerId'] = _activeJob!.offerId;
      offerVersionPayload['createdFrom'] = 'partner-app';
      offerVersionPayload['subTotalPrice'] = _subValueWithoutVat;
      offerVersionPayload['vat'] = _vatValue;
      offerVersionPayload['totalPrice'] = _totalValue;
      offerVersionPayload['saveListPriceV2'] = payloads;
      offerVersionPayload['projectId'] = int.parse(projectId);
      offerVersionPayload['expiry'] = int.parse(_offerSignatureValid);

      if (csUserId.isNotEmpty) {
        offerVersionPayload['csUserId'] = csUserId;
        offerVersionPayload['contact_id'] = currentJob.customerId;

        if (offerVersionId > 0) {
          offerVersionPayload['offerVersionId'] = offerVersionId;
          await offerService.createOfferVersionByCs(
              payload: offerVersionPayload);
          _csUserId = '';
          _offerVersionId = 0;
        } else {
          await offerService.createOfferByCs(payload: offerVersionPayload);
          _csUserId = '';
        }
      } else {
        await Future.value(
                partnerOfferVersion(offerVersionPayload: offerVersionPayload))
            .then((val) async {
          if (val) {
            createOfferRequest = RequestResponse.success;
            uploadUpdateOfferAndList = false;
          } else {
            createOfferRequest = RequestResponse.failed;
            uploadUpdateOfferAndList = false;
          }
        });
      }
    } else {
      await Future.wait([
        partnerOffer(payload: payload),
        updateCompanyOfferNote(offerNote: _saveAsDefaultText ? note : ''),
      ]);
    }

    _isOfferUploaded = true;

    // if (offerResponse.isNotEmpty) {
    //   ///
    //   for (var element in payloads) {
    //     element['offer_id'] = offerResponse.first.id;
    //     element['parent_offer_id'] = offerResponse.first.id;
    //   }

    //   if (payloads.isNotEmpty) {
    //     final saveListPriceResponses = await Future.wait(payloads
    //         .map((payload) => saveListPrice(payload: payload))
    //         .toList());

    //     if (saveListPriceResponses.isNotEmpty) {
    //       uploadUpdateOfferAndList = false;
    //       if (saveListPriceResponses.any((element) => element == false)) {
    //         createOfferRequest = RequestResponse.failed;
    //       } else {
    //         createOfferRequest = RequestResponse.success;
    //       }
    //     }
    //   }

    //   ///
    // }

    ///
    ///
    notifyListeners();
  }

  Future<void> _updateOffer({required String signature}) async {
    ///

    await getOfferById(offerId: _activeJob!.offerId);

    if ((_offerVersionList?.offerSeen ?? true) != true) {
      final updateResponse = await updateOffer(offerId: _activeJob!.offerId);

      if (updateResponse.success!) {
        ///

        final payloads = [];

        for (final offer in _primaryOfferListV3) {
          for (final job in offer) {
            ///

            final String key = '${job.industry}_${job.jobIndustry}';
            final jobDescription = _setJobDescription(key: key);

            final note = _utilityControllers['standardText']!.text.trim();
            payloads.add({
              'offer_id': _activeJob!.offerId,
              'calculation_id': 0,
              'contact_id': job.contactId,
              'days': _numberOfWeeksToFinish,
              'industry_id': job.industry,
              'job_description': jobDescription,
              'job_industry_id': job.jobIndustry,
              'note': note,
              'project_id': job.projectId,
              //REVIEWS ID IS SOON TO BE FILLED
              'reivewsId': '',
              'signature': signature,
              'start_date': _startDate,
              'total': double.parse(
                _controllers['${job.jobIndustry}-${job.industry}']!
                    .text
                    .trim()
                    .replaceAll('.', '')
                    .replaceAll(',', '.'),
              ),
              'weeks': _numberOfWeeksToFinish,
            });

            if (_saveAsDefaultText) {
              await Future.wait([
                updateCompanyOfferNote(offerNote: note),
                ...payloads.map((payload) => updateListPrice(payload: payload))
              ]);
            } else {
              await Future.wait([
                updateCompanyOfferNote(offerNote: ''),
                ...payloads.map((payload) => updateListPrice(payload: payload))
              ]);
            }

            uploadUpdateOfferAndList = false;
            createOfferRequest = RequestResponse.success;

            ///
          }
        }

        ///
      } else {
        ///

        uploadUpdateOfferAndList = false;
        createOfferRequest = RequestResponse.failed;

        ///
      }
    } else {
      _isOfferUploaded = false;
    }

    ///
  }

  Map<String, dynamic> _setJobDescription({required String key}) {
    ///

    return {
      'added':
          _addedDescriptions.containsKey(key) ? _addedDescriptions[key] : {},
      'updated': _updatedDescriptions.containsKey(key)
          ? _updatedDescriptions[key]
          : {},
      'deleted': _deletedDescriptions.containsKey(key)
          ? _deletedDescriptions[key]
          : {},
      'submitted': <String>[..._editableDescriptions[key]]
          .where((element) => element != '')
          .toList(),
    };

    ///
  }
}

enum RequestResponse {
  success,
  failed,
  idle,
  pre,
  extra,
  loading,
  signing,
}
