import 'dart:io';

import 'package:Haandvaerker.dk/model/customer_minbolig/old_jobs_model.dart';
import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/model/file/file_data_model.dart';
import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/file/file_service.dart';
import 'package:Haandvaerker.dk/services/offer/offer_service.dart';
import 'package:Haandvaerker.dk/services/wizard/wizard_service.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

enum NoteStatus {
  adding,
  loading,
  done,
  idle,
}

class OldTilbudStatus {
  final int value;
  final String text;
  final int filterValue;

  OldTilbudStatus({
    required this.value,
    required this.text,
    required this.filterValue,
  });

  OldTilbudStatus.defaults()
      : value = 0,
        text = '',
        filterValue = 0;
}

class TenderFolderViewModel extends BaseViewModel {
  TenderFolderViewModel({required this.context});
  final BuildContext context;

  List<FileData> _files = [];
  List<FileData> get files => _files;

  List<FileData> _tenderFiles = [];
  List<FileData> get tenderFiles => _tenderFiles;

  List<FileData> _tenderFilesBreadcrumbs = [];
  List<FileData> get tenderFilesBreadcrumbs => _tenderFilesBreadcrumbs;

  List<PublicFileData> _publicFiles = [];
  List<PublicFileData> get publicFiles => _publicFiles;

  List<FileData> _archiveFiles = [];
  List<FileData> get archiveFiles => _archiveFiles;

  List<FullIndustry> _fullIndustries = [];
  List<FullIndustry> get fullIndustries => _fullIndustries;

  List<EnterpriseIndustry> _listPriceAi = [];
  List<EnterpriseIndustry> get listPriceAi => _listPriceAi;

  List<AllWizardText> _wizardTexts = [];
  List<AllWizardText> get wizardTexts => _wizardTexts;

  MinboligApiResponse? _minboligListPrice;
  MinboligApiResponse? get minboligApiResponse => _minboligListPrice;

  MinboligApiResponse? _addNoteResponse;
  MinboligApiResponse? get addNoteResponse => _addNoteResponse;

  MinboligApiResponse? _deleteNoteResponse;
  MinboligApiResponse? get deleteNoteResponse => _deleteNoteResponse;

  MinboligApiResponse? _cancelJobResponse;
  MinboligApiResponse? get cancelJobResponse => _cancelJobResponse;

  String _cancelJobErrorMessage = '';
  String get cancelJobErrorMessage => _cancelJobErrorMessage;

  int _currentJobIndex = 0;
  int get currentJobIndex => _currentJobIndex;

  bool _cancelJobConfirm = false;
  bool get cancelJobConfirm => _cancelJobConfirm;

  String _reminderDate = '';
  String get reminderDate => _reminderDate;

  bool _importantNoticeConfirm = false;
  bool get importantNoticeConfirm => _importantNoticeConfirm;

  List<OldTilbudStatus> oldtilbudStatus = [
    OldTilbudStatus(value: 0, text: 'new_task', filterValue: 1),
    OldTilbudStatus(value: 1, text: 'in_dialogue', filterValue: 2),
    OldTilbudStatus(value: 2, text: 'offers_have_been_sent', filterValue: 3),
    OldTilbudStatus(value: 3, text: 'won_the_task', filterValue: 5),
    OldTilbudStatus(value: 5, text: 'did_not_win_the_task', filterValue: 0),
  ];

  int _currentStatusId = 1;
  int get currentStatusId => _currentStatusId;

  bool _isJobStatusUpdating = false;
  bool get isJobStatusUpdating => _isJobStatusUpdating;

  bool _isNoteTaking = false;
  bool get isNoteTaking => _isNoteTaking;

  bool _isNoteDeleting = false;
  bool get isNoteDeleting => _isNoteDeleting;

  bool _noteFromList = false;
  bool get noteFromList => _noteFromList;

  bool _isCancel = false;
  bool get isCancel => _isCancel;

  bool _hasContactedFakePhone = false;
  bool get hasContactedFakePhone => _hasContactedFakePhone;

  List<File> _currentFiles = [];
  List<File> get currentFiles => _currentFiles;

  List<CancelStatus> _surveyItems = [];
  List<CancelStatus> get surveyItems => _surveyItems;

  int? _selectedSurveyItem;
  int? get selectedSurveyItem => _selectedSurveyItem;

  TextEditingController _notesController = TextEditingController();
  TextEditingController get notesController => _notesController;

  RequestResponse _jobUpdateStatus = RequestResponse.idle;
  RequestResponse get jobUpdateStatus => _jobUpdateStatus;

  NoteStatus _noteStatus = NoteStatus.idle;
  NoteStatus get noteStatus => _noteStatus;

  PartnerJobModel? _currentSelectedJob;
  PartnerJobModel? get currentSelectedJob => _currentSelectedJob;

  OldPartnerJobsModel? _selectedOldJob;
  OldPartnerJobsModel? get selectedOldJob => _selectedOldJob;

  GlobalKey<FormState> cancelOfferFormKey = GlobalKey<FormState>();

  String _day = '';
  String get day => _day;

  String _month = '';
  String get month => _month;

  String _year = '';
  String get year => _year;

  String _projectDeadlineOffer = '';
  String get projectDeadlineOffer => _projectDeadlineOffer;

  final notesFormKey = GlobalKey<FormState>();

  void reset() {
    _files = [];
    _tenderFiles = [];
    _tenderFilesBreadcrumbs = [];
    _publicFiles = [];
    _archiveFiles = [];
    _fullIndustries = [];
    _listPriceAi = [];
    _wizardTexts = [];
    _minboligListPrice = null;
    _addNoteResponse = null;
    _deleteNoteResponse = null;
    _cancelJobResponse = null;
    _cancelJobErrorMessage = '';
    _currentJobIndex = 0;
    _cancelJobConfirm = false;
    _reminderDate = '';
    _importantNoticeConfirm = false;
    _currentStatusId = 1;
    _isJobStatusUpdating = false;
    _isNoteTaking = false;
    _isNoteDeleting = false;
    _noteFromList = false;
    _isCancel = false;
    _hasContactedFakePhone = false;
    _currentFiles = [];
    _surveyItems = [];
    _selectedSurveyItem = null;
    _notesController = TextEditingController();
    _jobUpdateStatus = RequestResponse.idle;
    _noteStatus = NoteStatus.idle;
    _currentSelectedJob = null;
    _selectedOldJob = null;
    _day = '';
    _month = '';
    _year = '';
    _projectDeadlineOffer = '';

    notifyListeners();
  }

  set hasContactedFakePhone(bool contact) {
    _hasContactedFakePhone = contact;
    notifyListeners();
  }

  set currentStatusId(int statusId) {
    _currentStatusId = statusId;
    notifyListeners();
  }

  set isJobStatusUpdating(bool val) {
    _isJobStatusUpdating = val;
    notifyListeners();
  }

  set jobUpdateStatus(RequestResponse response) {
    _jobUpdateStatus = response;
    notifyListeners();
  }

  set files(List<FileData> files) {
    _files = files;
    notifyListeners();
  }

  set publicFiles(List<PublicFileData> publicFiles) {
    _publicFiles = publicFiles;
    notifyListeners();
  }

  set fullIndustries(List<FullIndustry> fullIndustries) {
    _fullIndustries = fullIndustries;
    notifyListeners();
  }

  set listPriceAi(List<EnterpriseIndustry> listPriceAi) {
    _listPriceAi = listPriceAi;
    notifyListeners();
  }

  set wizardTexts(List<AllWizardText> wizardTexts) {
    _wizardTexts = wizardTexts;
    notifyListeners();
  }

  set archiveFiles(List<FileData> archiveFiles) {
    _archiveFiles = archiveFiles;
    notifyListeners();
  }

  set minboligListPrice(MinboligApiResponse? listPrice) {
    _minboligListPrice = listPrice;
    notifyListeners();
  }

  set isNoteTaking(bool notes) {
    _isNoteTaking = notes;
    notifyListeners();
  }

  set isNoteDeleting(bool notes) {
    _isNoteDeleting = notes;
    notifyListeners();
  }

  set notesController(TextEditingController notes) {
    _notesController = notesController;
    notifyListeners();
  }

  set currentFiles(List<File> images) {
    _currentFiles = images;
    notifyListeners();
  }

  set noteStatus(NoteStatus status) {
    _noteStatus = status;
    notifyListeners();
  }

  set addNoteResponse(MinboligApiResponse? add) {
    _addNoteResponse = add;
    notifyListeners();
  }

  set deleteNoteResponse(MinboligApiResponse? delete) {
    _deleteNoteResponse = delete;
    notifyListeners();
  }

  set noteFromList(bool note) {
    _noteFromList = note;
    notifyListeners();
  }

  set surveyItems(List<CancelStatus> status) {
    _surveyItems = status;
    notifyListeners();
  }

  set selectedSurveyItem(int? item) {
    _selectedSurveyItem = item;
    notifyListeners();
  }

  set isCancel(bool cancel) {
    _isCancel = cancel;
    notifyListeners();
  }

  set currentSelectedJob(PartnerJobModel? job) {
    _currentSelectedJob = job;
    if (job != null) {
      _currentStatusId = job.statusId!;
      _surveyItems = job.cancelStatus!;
    }

    notifyListeners();
  }

  set currentJobIndex(int index) {
    _currentJobIndex = index;
    notifyListeners();
  }

  set cancelJobErrorMessage(String message) {
    _cancelJobErrorMessage = message;
    notifyListeners();
  }

  set cancelJobConfirm(bool confirm) {
    _cancelJobConfirm = confirm;
    notifyListeners();
  }

  set reminderDate(String date) {
    _reminderDate = date;
    notifyListeners();
  }

  set importantNoticeConfirm(bool confirm) {
    _importantNoticeConfirm = confirm;
    notifyListeners();
  }

  set selectedOldJob(OldPartnerJobsModel? oldJob) {
    _selectedOldJob = oldJob;
    notifyListeners();
  }

  void formatDate(String date) {
    String day;
    String month;
    String year;
    if (date.isNotEmpty) {
      if (date.contains("/")) {
        if (date.split("/").first.isNotEmpty &&
            date.split("/").first.length == 4) {
          year = date.split("/").first;
          day = date.split("/").last;
          if (date.split("/")[1].isNotEmpty && date.split("/")[1].length < 2) {
            month = '0${date.split("/")[1]}';
          } else {
            month = date.split("/")[1];
          }

          _day = day;
          _month = month;
          _year = year;
          _projectDeadlineOffer = '$year-$month-$day';
        } else {
          day = date.split("/").first;
          year = date.split("/").last;
          if (date.split("/")[1].isNotEmpty && date.split("/")[1].length < 2) {
            month = '0${date.split("/")[1]}';
          } else {
            month = date.split("/")[1];
          }

          _day = day;
          _month = month;
          _year = year;
          _projectDeadlineOffer = '$year-$month-$day';
        }
      } else if (date.contains("-")) {
        if (date.split("-").first.isNotEmpty &&
            date.split("-").first.length == 4) {
          year = date.split("-").first;
          day = date.split("-").last;
          if (date.split("-")[1].isNotEmpty && date.split("-")[1].length < 2) {
            month = '0${date.split("-")[1]}';
          } else {
            month = date.split("-")[1];
          }

          _day = day;
          _month = month;
          _year = year;
          _projectDeadlineOffer = '$year-$month-$day';
        } else {
          day = date.split("-").first;
          year = date.split("-").last;
          if (date.split("-")[1].isNotEmpty && date.split("-")[1].length < 2) {
            month = '0${date.split("-")[1]}';
          } else {
            month = date.split("-")[1];
          }

          _day = day;
          _month = month;
          _year = year;
          _projectDeadlineOffer = '$year-$month-$day';
        }
      }
    }
  }

  void updatedTenderFilesBreadcrumbs(
      {required bool toAdd, required FileData selectedFile}) {
    if (toAdd) {
      _tenderFilesBreadcrumbs.add(selectedFile);
    }
    notifyListeners();
  }

  int calculateDifference(DateTime projectDeadlineOffer) {
    return (projectDeadlineOffer.difference(DateTime.now()).inHours / 24)
            .round() +
        1;
  }

  Color projectDeadlineOfferColor(String projectDeadlineOfferColorCode) {
    if (projectDeadlineOfferColorCode.isNotEmpty) {
      return Color(int.parse(
          '0xff${projectDeadlineOfferColorCode.replaceAll(RegExp('[^A-Za-z0-9]'), '')}'));
    }
    return PartnerAppColors.darkBlue;
  }

  Future<void> getFiles({required int folderId}) async {
    var fileService = context.read<FilesService>();
    setBusy(true);
    var files = await fileService.getFiles(folderId: folderId);

    if (files != null) {
      _files = files;
    }

    notifyListeners();
    setBusy(false);
  }

  Future<void> getTenderFiles({required int folderId}) async {
    setBusy(true);
    var fileService = context.read<FilesService>();
    var files = await fileService.getFiles(folderId: folderId);

    if (files != null) {
      _tenderFiles = files;
    }

    notifyListeners();
    setBusy(false);
  }

  Future<void> getArchivedFiles({required int folderId}) async {
    var fileService = context.read<FilesService>();
    setBusy(true);
    var archiveFiles = await fileService.getFiles(folderId: folderId);

    if (archiveFiles != null) {
      _archiveFiles = archiveFiles;
    }

    notifyListeners();
    setBusy(false);
  }

  Future<void> getPublicFiles({required int projectParentId}) async {
    var fileService = context.read<FilesService>();
    setBusy(true);
    var publicFiles =
        await fileService.getPublicFiles(projectParentId: projectParentId);

    _publicFiles = publicFiles;

    notifyListeners();
    setBusy(false);
  }

  void setIndustry(
      {required List<AllWizardText> list1,
      required List<FullIndustry> list3}) async {
    _wizardTexts = [...list1];
    _fullIndustries = [...list3];

    notifyListeners();
  }

  Future<void> getListPriceAi(
      int parentProjectId, int contactId, PartnerJobModel activeJob) async {
    final wizardService = context.read<WizardService>();
    List<String> jobIndustries = [];
    List<EnterpriseIndustry> temp = [];

    setBusy(true);
    final listPriceAi = await wizardService.getListPriceAi(
        parentProjectId: parentProjectId, contactId: contactId);

    for (final job in activeJob.jobsList as List) {
      if (job is List) {
        for (final item in job) {
          jobIndustries.add(item.toString().split(':').first);
        }
      } else {
        jobIndustries.add(job.toString().split(':').first);
      }
    }

    if (listPriceAi != null) {
      for (final price in listPriceAi) {
        price.jobIndustries!.removeWhere(
            (element) => !jobIndustries.contains(element.jobIndustryId));
        if (price.jobIndustries!.isNotEmpty) temp.add(price);
      }

      _listPriceAi = temp;
    }

    notifyListeners();
    setBusy(false);
  }

  Future<void> getMinboligListPrice(
      {required int contactId, required int projectId}) async {
    final offerService = context.read<OfferService>();
    setBusy(true);
    var listPrice = await offerService.getListPriceMinbolig(
        contactId: contactId, projectId: projectId);

    _minboligListPrice = listPrice;
    notifyListeners();
    setBusy(false);
  }

  Future<bool> archiveOldJob(
      {required int jobId, required int contactId}) async {
    var wizardService = context.read<WizardService>();

    setBusy(true);
    var archiveResponse =
        await wizardService.archiveOldJob(jobId: jobId, contactId: contactId);

    return archiveResponse.success ?? false;
  }

  Future<bool> updateArchiveJob(
      {required int jobId, required int isDeleted}) async {
    var wizardService = context.read<WizardService>();
    setBusy(true);
    var updateJobStatusResponse = await wizardService
        .jobUpdate(jobId: jobId, payload: {"isDeleted": isDeleted});

    return updateJobStatusResponse.success ?? false;
  }

  Future<bool> updateJobStatus(
      {required int jobId, required int statusId}) async {
    var wizardService = context.read<WizardService>();
    setBusy(true);
    var updateJobStatusResponse =
        await wizardService.updateJobStatus(jobId: jobId, statusId: statusId);

    return updateJobStatusResponse;
  }

  Future<void> addNote({required FormData payload}) async {
    final wizardService = context.read<WizardService>();
    final addNoteResponse = await wizardService.addNote(payload: payload);

    _addNoteResponse = addNoteResponse;
    notifyListeners();
  }

  Future<void> deleteNote({
    required int jobId,
    required int noteId,
  }) async {
    final wizardService = context.read<WizardService>();
    final deleteNoteResponse = await wizardService.deleteNote(
      jobId: jobId,
      noteId: noteId,
    );

    _deleteNoteResponse = deleteNoteResponse;
    notifyListeners();
  }

  Future<void> cancelJob(
      {required int cancelStatusId,
      required int jobId,
      String? reminderDate}) async {
    final wizardService = context.read<WizardService>();
    final Map<String, dynamic> payLoad = {
      'cancelStatusId': cancelStatusId,
      'jobId': jobId,
    };

    if (reminderDate != null && reminderDate.isNotEmpty) {
      payLoad.putIfAbsent('reminderDate', () => reminderDate);
    }

    final cancelJobResponse = await wizardService.cancelJob(
      payload: payLoad,
    );

    _cancelJobResponse = cancelJobResponse;
    notifyListeners();
  }

  Future<bool> uploadFiles(
      {required List<PlatformFile> files,
      required String type,
      required FileData parentFile,
      required int projectId}) async {
    setBusy(true);
    var fileService = context.read<FilesService>();

    Map<String, dynamic> jsonData = {
      'parentId': parentFile.id,
      'category': type,
      'projectId': projectId
    };

    for (int index = 0; index < files.length; index++) {
      final fileData = MultipartFile.fromFileSync(files[index].path!,
          filename: files[index].name);

      jsonData.putIfAbsent('files[$index]', () => fileData);
    }

    final formDataOne = FormData.fromMap(jsonData);

    final response =
        await fileService.uploadMultipleFiles(payload: formDataOne);

    if (!response.success!) throw ApiException(message: response.message!);

    await Future.delayed(const Duration(seconds: 2));

    var archiveFiles = await fileService.getFiles(folderId: parentFile.id!);

    if (archiveFiles != null) {
      _tenderFiles = archiveFiles;
    }

    notifyListeners();
    setBusy(false);

    return true;
  }

  Future<bool> jobUpdate({required int jobId, required String note}) async {
    var wizardService = context.read<WizardService>();
    setBusy(true);
    var updateJobStatusResponse = await wizardService
        .jobUpdate(jobId: jobId, payload: {"reminderNote": note});

    return updateJobStatusResponse.success ?? false;
  }
}
