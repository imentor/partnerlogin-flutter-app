import 'package:Haandvaerker.dk/model/customer_minbolig/list_price_minbolig_model.dart';
import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/offer/offer_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SeeOfferViewModel extends BaseViewModel {
  SeeOfferViewModel({required this.context});
  final BuildContext context;

  bool _isSuccess = false;
  bool get isSuccess => _isSuccess;

  String _errorMessage = '';
  String get errorMessage => _errorMessage;

  List<SeeOfferJobListView> _seeOfferJobList = [];
  List<SeeOfferJobListView> get seeOfferJobList => _seeOfferJobList;

  MinboligListPrice? _minboligListPrice;
  MinboligListPrice? get minboligListPrice => _minboligListPrice;

  PartnerJobModel? _activeJob;
  PartnerJobModel? get activeJob => _activeJob;

  double _total = 0;
  double get total => _total;

  double _totalWithVat = 0;
  double get totalWithVat => _totalWithVat;

  void reset() {
    _isSuccess = false;
    _errorMessage = '';
    _seeOfferJobList = [];
    _minboligListPrice = null;
    _activeJob = null;
    _total = 0;
    _totalWithVat = 0;

    notifyListeners();
  }

  set activeJob(PartnerJobModel? job) {
    _activeJob = job;
    notifyListeners();
  }

  Future<void> getListPriceMinbolig({
    required int projectId,
    required int contactId,
    int? offerVersionId,
  }) async {
    final offerService = context.read<OfferService>();
    MinboligApiResponse? minboligResponse;
    setBusy(true);
    if (offerVersionId == null) {
      minboligResponse = await offerService.getListPriceMinbolig(
          contactId: contactId, projectId: projectId);
    } else {
      minboligResponse = await offerService.getListPriceMinboligOfferVersion(
          contactId: contactId,
          offerVersionId: offerVersionId,
          projectId: projectId);
    }

    if (minboligResponse != null) {
      _minboligListPrice = MinboligListPrice.fromJson(
          minboligResponse.data as Map<String, dynamic>);

      if ((minboligResponse.success ?? false)) {
        _isSuccess = true;

        List<String> jobLists = [];

        for (final job in (activeJob?.jobsList ?? []) as List) {
          if (job is List) {
            for (final item in job) {
              jobLists.add(item.toString().split(':').first);
            }
          } else {
            jobLists.add(job.toString().split(':').first);
          }
        }

        if (jobLists.length != 1 &&
            jobLists.contains("1062") &&
            _activeJob?.contractType == "Hovedentreprise") {
          jobLists.removeWhere((element) => element == "1062");
        }

        List<String> industries = (_minboligListPrice?.minPriceIndustries ?? {})
            .keys
            .toSet()
            .toList() as List<String>;
        List<String> jobIndustryIds = [];
        List<SeeOfferJobListView> jobIndustries = [];

        for (final industry in industries) {
          if ((_minboligListPrice?.minPriceIndustries ?? {})
              .containsKey(industry)) {
            jobIndustryIds.addAll(_minboligListPrice!
                .minPriceIndustries![industry].keys
                .toList());
          }
        }
        jobIndustryIds = jobIndustryIds.toSet().toList();
        List<String> toRemove = [];
        for (final jobs in jobIndustryIds) {
          if (!jobLists.contains(jobs)) {
            toRemove.add(jobs);
          }
        }
        for (final remove in toRemove) {
          jobIndustryIds.removeWhere((element) => element == remove);
        }

        _total = 0;

        for (final jobIndustry in jobIndustryIds) {
          List<SeeOfferEnterpriseList> enterprises = [];
          for (final industry in industries) {
            if ((_minboligListPrice?.result ?? []).any((element) =>
                element.industry == industry &&
                element.jobIndustry == jobIndustry)) {
              _total += double.parse((_minboligListPrice?.result ?? [])
                      .firstWhere(
                        (element) =>
                            element.industry == industry &&
                            element.jobIndustry == jobIndustry,
                        orElse: () => MinboligListPriceResult.defaults(),
                      )
                      .total ??
                  '0');
              enterprises.add(
                SeeOfferEnterpriseList(
                  enterpriseId: industry,
                  descriptions: <String>[
                    ...(_minboligListPrice?.result ?? [])
                                .firstWhere((element) => element.industry == industry && element.jobIndustry == jobIndustry,
                                    orElse: () =>
                                        MinboligListPriceResult.defaults())
                                .series ==
                            null
                        ? (_minboligListPrice?.result ?? [])
                                .firstWhere((element) => element.industry == industry && element.jobIndustry == jobIndustry,
                                    orElse: () =>
                                        MinboligListPriceResult.defaults())
                                .seriesOriginal ??
                            []
                        : (_minboligListPrice?.result ?? [])
                                .firstWhere((element) => element.industry == industry && element.jobIndustry == jobIndustry,
                                    orElse: () =>
                                        MinboligListPriceResult.defaults())
                                .series is List
                            ? (_minboligListPrice?.result ?? [])
                                .firstWhere((element) => element.industry == industry && element.jobIndustry == jobIndustry,
                                    orElse: () =>
                                        MinboligListPriceResult.defaults())
                                .series
                            : Map.from((_minboligListPrice?.result ?? [])
                                    .firstWhere(
                                        (element) => element.industry == industry && element.jobIndustry == jobIndustry,
                                        orElse: () => MinboligListPriceResult.defaults())
                                    .series)
                                .values
                                .toList(),
                  ],
                  total: (_minboligListPrice?.result ?? [])
                          .firstWhere(
                              (element) =>
                                  element.industry == industry &&
                                  element.jobIndustry == jobIndustry,
                              orElse: () => MinboligListPriceResult.defaults())
                          .total ??
                      '0',
                ),
              );
            }
          }
          jobIndustries.add(
            SeeOfferJobListView(
              jobIndustryId: jobIndustry,
              enterprises: enterprises,
            ),
          );
        }
        _totalWithVat = _total * 1.25;
        _seeOfferJobList = jobIndustries;
      } else {
        _isSuccess = false;
        _errorMessage = minboligResponse.message ?? '';
      }
    }

    notifyListeners();
    setBusy(false);
  }
}

class SeeOfferJobListView {
  String jobIndustryId;
  List<SeeOfferEnterpriseList> enterprises;

  SeeOfferJobListView({
    required this.jobIndustryId,
    required this.enterprises,
  });
}

class SeeOfferEnterpriseList {
  String enterpriseId;
  List<String> descriptions;
  String total;

  SeeOfferEnterpriseList({
    required this.enterpriseId,
    required this.descriptions,
    required this.total,
  });
}
