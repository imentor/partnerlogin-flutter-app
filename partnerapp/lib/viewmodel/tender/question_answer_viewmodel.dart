import 'package:Haandvaerker.dk/model/questions/questions_response_model.dart';
import 'package:Haandvaerker.dk/services/question/questions_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class QuestionAnswerViewModel extends BaseViewModel {
  QuestionAnswerViewModel({required this.context});
  final BuildContext context;

  String _getQuestion = '';
  String get getQuestion => _getQuestion;

  List<QuestionResponseModel> _questionsList = [];
  List<QuestionResponseModel> get questionsList => _questionsList;

  List<QuestionResponseModel> _activeQuestionList = [];
  List<QuestionResponseModel> get activeQuestionList => _activeQuestionList;

  List<ChildQuestionResponseModel> _childQuestionsList = [];
  List<ChildQuestionResponseModel> get childQuestionsList =>
      _childQuestionsList;

  void reset() {
    _getQuestion = '';
    _questionsList = [];
    _activeQuestionList = [];
    _childQuestionsList = [];

    notifyListeners();
  }

  set getQuestion(String getQuestion) {
    _getQuestion = getQuestion;
    notifyListeners();
  }

  set questionsList(List<QuestionResponseModel> questionsList) {
    _questionsList = questionsList;
    notifyListeners();
  }

  set activeQuestionList(List<QuestionResponseModel> activeQuestionList) {
    _activeQuestionList = activeQuestionList;
    notifyListeners();
  }

  set childQuestionsList(List<ChildQuestionResponseModel> childQuestionsList) {
    _childQuestionsList = childQuestionsList;
    notifyListeners();
  }

  Future<void> getAllQuestions({required int projectParentId}) async {
    var questionsService = context.read<QuestionsService>();
    setBusy(true);
    var questionsList = await questionsService.getAllQuestions(
        projectParentId: projectParentId);

    _questionsList = questionsList;

    notifyListeners();
    setBusy(false);
  }

  Future<void> getChildQuestions(
      {required int projectId, required int parentId}) async {
    var questionsService = context.read<QuestionsService>();
    setBusy(true);
    var childQuestionsList = await questionsService.getChildQuestions(
        projectId: projectId, parentId: parentId);

    _childQuestionsList = childQuestionsList;

    notifyListeners();
    setBusy(false);
  }

  Future<QuestionResponseModel> addQuestion({
    required int projectId,
    required String contactId,
    required String descriptionId,
    required String industryId,
    required String jobIndustryId,
    required String question,
  }) async {
    var questionsService = context.read<QuestionsService>();
    setBusy(true);
    var questionResponse = await questionsService.addQuestion(
      projectId: projectId,
      contactId: contactId,
      descriptionId: descriptionId,
      industryId: industryId,
      jobIndustryId: jobIndustryId,
      question: question,
    );

    setBusy(false);
    return questionResponse;
  }

  Future<ChildQuestionResponseModel> replyQuestion({
    required int projectId,
    required int parentId,
    required String message,
  }) async {
    var questionsService = context.read<QuestionsService>();
    setBusy(true);
    var questionResponse = await questionsService.replyQuestion(
      projectId: projectId,
      parentId: parentId,
      message: message,
    );
    setBusy(false);
    return questionResponse;
  }
}
