import 'package:flutter/material.dart';

class LaborTaskToggleViewModel with ChangeNotifier {
  bool _isTask = true;

  bool get isTask => _isTask;

  void setTask(bool value) {
    _isTask = value;
    notifyListeners();
  }
}
