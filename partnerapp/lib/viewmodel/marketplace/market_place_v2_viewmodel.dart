import 'package:Haandvaerker.dk/model/cities/denmark_cities.dart';
import 'package:Haandvaerker.dk/model/get_types_response_model.dart'
    as response_model;
import 'package:Haandvaerker.dk/model/marketplace/check_reservation_model.dart';
import 'package:Haandvaerker.dk/model/marketplace/marketplace_v2_response_model.dart';
import 'package:Haandvaerker.dk/model/packages/package_response_model.dart';
import 'package:Haandvaerker.dk/model/settings/regions_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/services/marketplace/marketplace/marketplace_service.dart';
import 'package:Haandvaerker.dk/services/settings/settings_service.dart';
import 'package:Haandvaerker.dk/services/wizard/wizard_service.dart';
import 'package:Haandvaerker.dk/theme.dart';
import 'package:Haandvaerker.dk/ui/components/package/package_popup_dialog.dart';
import 'package:Haandvaerker.dk/ui/screens/marketplace/marketplace/components/job/reserve/reserve_dialog.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/packages/packages_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/clients_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MarketPlaceV2ViewModel extends BaseViewModel {
  MarketPlaceV2ViewModel({required this.context});

  final BuildContext context;

  List<MarketPlaceV2Items> _marketJobs = <MarketPlaceV2Items>[];
  List<MarketPlaceV2Items> get marketJobs => _marketJobs;

  List<EnterpriseIndustry> _listPriceAi = [];
  List<EnterpriseIndustry> get listPriceAi => _listPriceAi;

  List<EnterpriseIndustry> _subListPriceAi = [];
  List<EnterpriseIndustry> get subListPriceAi => _subListPriceAi;

  List<DenmarkCities> _denmarkCities = [];
  List<DenmarkCities> get denmarkCities => _denmarkCities;

  List<response_model.Data> _fullIndustries = [];
  List<response_model.Data> get fullIndustries => _fullIndustries;

  List<RegionModel> _regionList = [];
  List<RegionModel> get regionList => _regionList;

  List<DenmarkCities> _savedFilterDenmarkCities = [];
  List<DenmarkCities> get savedFilterDenmarkCities => _savedFilterDenmarkCities;

  List<response_model.Data> _savedFilterFullIndustries = [];
  List<response_model.Data> get savedFilterFullIndustries =>
      _savedFilterFullIndustries;

  List<RegionModel> _savedFilterRegionList = [];
  List<RegionModel> get savedFilterRegionList => _savedFilterRegionList;

  Map<String, dynamic> _reserved = <String, dynamic>{};
  Map<String, dynamic> get reserved => _reserved;

  Map<String, dynamic> _currentMarketplacePayload = {};
  Map<String, dynamic> get currentMarketplacePayload =>
      _currentMarketplacePayload;

  int _marketPlaceFilterId = 0;
  int get marketPlaceFilterId => _marketPlaceFilterId;

  int _currentPage = 1;
  int get currentPage => _currentPage;

  int? _lastPage = 1;
  int? get lastPage => _lastPage;

  int? _enterprise;
  int? get enterprise => _enterprise;

  int _total = 1;
  int? get total => _total;

  bool _fromNewsFeed = false;
  bool get fromNewsFeed => _fromNewsFeed;

  bool _toVideoDialog = false;
  bool get toVideoDialog => _toVideoDialog;

  bool _cancelDialog = false;
  bool get cancelDialog => _cancelDialog;

  bool _isVideoDone = false;
  bool get isVideoDone => _isVideoDone;

  bool _initialMarketplaceLoad = true;
  bool get initialMarketplaceLoad => _initialMarketplaceLoad;

  bool _isReserving = false;
  bool get isReserving => _isReserving;

  bool _canReserveMore = true;
  bool get canReserveMore => _canReserveMore;

  bool _toReserveMore = false;
  bool get toReserveMore => _toReserveMore;

  bool _isGettingListPrice = false;
  bool get isGettingListPrice => _isGettingListPrice;

  String _day = '';
  String get day => _day;

  String _year = '';
  String get year => _year;

  String _month = '';
  String get month => _month;

  String _startDate = '';
  String get startDate => _startDate;

  String _endDate = '';
  String get endDate => _endDate;

  String _deadlineOffer = '';
  String get deadlineOffer => _deadlineOffer;

  String _reserveFailedMessage = '';
  String get reserveFailedMessage => _reserveFailedMessage;

  Color _deadlineOfferColorCode = PartnerAppColors.darkBlue;
  Color get deadlineOfferColorCode => _deadlineOfferColorCode;

  RequestResponse _isReserveSuccess = RequestResponse.idle;
  RequestResponse get isReserveSuccess => _isReserveSuccess;

  CheckBitrixReservationResponseModel? _checkReserveResponse;
  CheckBitrixReservationResponseModel? get checkReserveResponse =>
      _checkReserveResponse;

  MarketPlaceV2Items? _currentJobItem;
  MarketPlaceV2Items? get currentJobItem => _currentJobItem;

  bool _agreeAcceptOfferDeadline = false;
  bool get agreeAcceptOfferDeadline => _agreeAcceptOfferDeadline;

  bool _isTimeoutEnable = false;
  bool get isTimeoutEnable => _isTimeoutEnable;

  final List<MarketPlaceV2Items> _readMarketplaceJobs = [];
  List<MarketPlaceV2Items> get readMarketplaceJobs => _readMarketplaceJobs;

  final List<MarketPlaceV2Items> _sentReadMarketplaceJobs = [];
  List<MarketPlaceV2Items> get sentReadMarketplaceJobs =>
      _sentReadMarketplaceJobs;

  void addReadMarketplaceJobs(int index) {
    final readJob = marketJobs[index];

    if (!readMarketplaceJobs.contains(readJob)) {
      _readMarketplaceJobs.add(readJob);
    }

    timeout();

    notifyListeners();
  }

  Future<void> timeout() async {
    final service = context.read<MarketPlaceService>();

    if (!isTimeoutEnable) {
      _isTimeoutEnable = true;
      await Future.delayed(const Duration(seconds: 2)).whenComplete(() async {
        List<MarketPlaceV2Items> tempList = [];

        for (var element in readMarketplaceJobs.toList()) {
          if (!sentReadMarketplaceJobs.contains(element)) {
            tempList.add(element);
            _sentReadMarketplaceJobs.add(element);
          }
        }

        if (tempList.isNotEmpty) {
          await service.jobsSeen(
              projectIds: [...tempList.map((value) => value.id ?? 0)]);
        }

        _isTimeoutEnable = false;
      });
    }
  }

  set agreeAcceptOfferDeadline(bool value) {
    _agreeAcceptOfferDeadline = value;

    notifyListeners();
  }

  Future<PackageData?> checkPackage() async {
    final service = context.read<MarketPlaceService>();

    final response = await service.checkPackage();

    if (response != null) {
      return PackageData.fromJson(response.data);
    }

    return null;
  }

  void reset() {
    _marketJobs = [];
    _listPriceAi = [];
    _subListPriceAi = [];
    _denmarkCities = [];
    _fullIndustries = [];
    _regionList = [];
    _savedFilterDenmarkCities = [];
    _savedFilterFullIndustries = [];
    _savedFilterRegionList = [];
    _reserved = {};
    _currentMarketplacePayload = {};
    _marketPlaceFilterId = 0;
    _currentPage = 1;
    _lastPage = 1;
    _enterprise = 0;
    _total = 1;
    _fromNewsFeed = false;
    _toVideoDialog = false;
    _cancelDialog = false;
    _isVideoDone = false;
    _initialMarketplaceLoad = true;
    _isReserving = false;
    _canReserveMore = true;
    _toReserveMore = false;
    _isGettingListPrice = false;
    _day = '';
    _year = '';
    _month = '';
    _startDate = '';
    _endDate = '';
    _deadlineOffer = '';
    _reserveFailedMessage = '';
    _deadlineOfferColorCode = PartnerAppColors.darkBlue;
    _isReserveSuccess = RequestResponse.idle;
    _checkReserveResponse = null;
    _currentJobItem = null;
    _agreeAcceptOfferDeadline = false;

    notifyListeners();
  }

  set busy(bool value) {
    setBusy(value);
    notifyListeners();
  }

  set initialMarketplaceLoad(bool value) {
    _initialMarketplaceLoad = value;
    notifyListeners();
  }

  set listPriceAi(List<EnterpriseIndustry> listPriceAi) {
    _listPriceAi = listPriceAi;
    notifyListeners();
  }

  set currentPage(int currentPage) {
    _currentPage = currentPage;
    notifyListeners();
  }

  set lastPage(int? lastPage) {
    _lastPage = lastPage;
    notifyListeners();
  }

  set fromNewsFeed(bool news) {
    _fromNewsFeed = news;
    notifyListeners();
  }

  set toVideoDialog(bool toVideo) {
    _toVideoDialog = toVideo;
    notifyListeners();
  }

  set cancelDialog(bool cancel) {
    _cancelDialog = cancel;
    notifyListeners();
  }

  set isVideoDone(bool done) {
    _isVideoDone = done;
    notifyListeners();
  }

  set marketJobs(List<MarketPlaceV2Items> marketJobs) {
    _marketJobs = marketJobs;
    notifyListeners();
  }

  set enterprise(int? enterprise) {
    _enterprise = enterprise;
    notifyListeners();
  }

  set isReserving(bool reserving) {
    _isReserving = reserving;
    notifyListeners();
  }

  set isReserveSuccess(RequestResponse reserveSuccess) {
    _isReserveSuccess = reserveSuccess;
    notifyListeners();
  }

  set reserveFailedMessage(String failedMessage) {
    _reserveFailedMessage = failedMessage;
    notifyListeners();
  }

  set canReserveMore(bool more) {
    _canReserveMore = more;
    notifyListeners();
  }

  set checkReserveResponse(CheckBitrixReservationResponseModel? reserve) {
    _checkReserveResponse = reserve;
    notifyListeners();
  }

  set toReserveMore(bool reserve) {
    _toReserveMore = reserve;
    notifyListeners();
  }

  set currentJobItem(MarketPlaceV2Items? item) {
    _currentJobItem = item;
    notifyListeners();
  }

  set isGettingListPrice(bool listPrice) {
    _isGettingListPrice = listPrice;
    notifyListeners();
  }

  set startDate(String startDate) {
    _startDate = startDate;
    notifyListeners();
  }

  set endDate(String endDate) {
    _endDate = endDate;
    notifyListeners();
  }

  void setReserveValuesToDefault() {
    _isReserving = true;
    _reserved = {};
    _reserveFailedMessage = '';
    _canReserveMore = true;
    _checkReserveResponse = null;
    _toReserveMore = false;
    _cancelDialog = false;
    _toVideoDialog = false;
    _isVideoDone = false;
    _agreeAcceptOfferDeadline = false;
    notifyListeners();
  }

  void setMarketPlaceToDefault() {
    _day = '';
    _year = '';
    _month = '';
    _isReserving = false;
    _isReserveSuccess = RequestResponse.idle;
    _reserved = {};
    _agreeAcceptOfferDeadline = false;
  }

  void formatDate(String date) {
    if (date.isNotEmpty) {
      if (date.contains("/")) {
        if (date.split("/").first.isNotEmpty) {
          if (date.split("/").first.length == 4) {
            final year = date.split("/").first;
            final month = date.split("/")[1];
            final day = date.split("/").last;

            _day = day;
            _year = year;
            _month = month;
            _deadlineOffer = '$day.$month.$year';

            notifyListeners();
          } else {
            final day = date.split("/").first;
            final month = date.split("/")[1];
            final year = date.split("/").last;

            _day = day;
            _year = year;
            _month = month;
            _deadlineOffer = '$day.$month.$year';

            notifyListeners();
          }
        }
      } else if (date.contains("-")) {
        if (date.split("-").first.isNotEmpty) {
          if (date.split("-").first.length == 4) {
            final year = date.split("-").first;
            final month = date.split("-")[1];
            final day = date.split("-").last;

            _day = day;
            _year = year;
            _month = month;
            _deadlineOffer = '$day.$month.$year';

            notifyListeners();
          } else {
            final day = date.split("-").first;
            final month = date.split("-")[1];
            final year = date.split("-").last;

            _day = day;
            _year = year;
            _month = month;
            _deadlineOffer = '$day.$month.$year';

            notifyListeners();
          }
        }
      }
    }
  }

  void formatStartDate(String startDate) {
    if (startDate.isNotEmpty) {
      if (startDate.contains("/")) {
        if (startDate.split("/").first.isNotEmpty) {
          if (startDate.split("/").first.length == 4) {
            final year = startDate.split("/").first;
            final month = startDate.split("/")[1];
            final day = startDate.split("/").last;

            _startDate = '$year-$month-$day';

            notifyListeners();
          } else {
            final day = startDate.split("/").first;
            final month = startDate.split("/")[1];
            final year = startDate.split("/").last;

            _startDate = '$year-$month-$day';

            notifyListeners();
          }
        }
      } else if (startDate.contains("-")) {
        if (startDate.split("-").first.isNotEmpty) {
          if (startDate.split("-").first.length == 4) {
            final year = startDate.split("-").first;
            final month = startDate.split("-")[1];
            final day = startDate.split("-").last;

            _startDate = '$year-$month-$day';

            notifyListeners();
          } else {
            final day = startDate.split("-").first;
            final month = startDate.split("-")[1];
            final year = startDate.split("-").last;

            _startDate = '$year-$month-$day';

            notifyListeners();
          }
        }
      }
    }
  }

  void formatEndDate(String endDate) {
    if (endDate.isNotEmpty) {
      if (endDate.contains("/")) {
        if (endDate.split("/").first.isNotEmpty) {
          if (endDate.split("/").first.length == 4) {
            final year = endDate.split("/").first;
            final month = endDate.split("/")[1];
            final day = endDate.split("/").last;

            _endDate = '$year-$month-$day';

            notifyListeners();
          } else {
            final day = endDate.split("/").first;
            final month = endDate.split("/")[1];
            final year = endDate.split("/").last;

            _endDate = '$year-$month-$day';

            notifyListeners();
          }
        }
      } else if (endDate.contains("-")) {
        if (endDate.split("-").first.isNotEmpty) {
          if (endDate.split("-").first.length == 4) {
            final year = endDate.split("-").first;
            final month = endDate.split("-")[1];
            final day = endDate.split("-").last;

            _endDate = '$year-$month-$day';

            notifyListeners();
          } else {
            final day = endDate.split("-").first;
            final month = endDate.split("-")[1];
            final year = endDate.split("-").last;

            _endDate = '$year-$month-$day';

            notifyListeners();
          }
        }
      }
    }
  }

  void initializeDeadlineOffer() {
    _deadlineOffer = '';
    _deadlineOfferColorCode = PartnerAppColors.darkBlue;

    notifyListeners();
  }

  void formatDeadlineOfferColorCode(String deadlineOfferColorCode) {
    if (deadlineOfferColorCode.isNotEmpty) {
      _deadlineOfferColorCode = Color(int.parse(
          '0xff${deadlineOfferColorCode.replaceAll(RegExp('[^A-Za-z0-9]'), '')}'));
    }

    notifyListeners();
  }

  Future<List<MarketPlaceV2Items>?> getMarketplaceById(
      {required int projectId}) async {
    final marketPlaceService = context.read<MarketPlaceService>();

    final marketJobsResponse = await marketPlaceService.marketplaceV2(
        payload: {'page': 1, 'size': 10, 'projectId': projectId});

    if (marketJobsResponse != null) {
      return marketJobsResponse.items ?? [];
    } else {
      return null;
    }
  }

  Future<void> nextPageMarketplace() async {
    final marketPlaceService = context.read<MarketPlaceService>();

    setBusy(true);

    final tempPayload = {...currentMarketplacePayload};
    tempPayload['page'] = _currentPage;

    final marketJobsResponse =
        await marketPlaceService.marketplaceV2(payload: tempPayload);

    if (marketJobsResponse != null) {
      _marketJobs = marketJobsResponse.items ?? [];

      _lastPage = marketJobsResponse.lastPage;
    }

    _currentMarketplacePayload = {...tempPayload};

    notifyListeners();
    setBusy(false);
  }

  Future<void> saveMarketplaceFilter(
      {required Map<String, dynamic> payload}) async {
    final marketPlaceService = context.read<MarketPlaceService>();

    setBusy(true);

    final filtersResponse = await marketPlaceService.getMarketplaceFilters();

    if (filtersResponse?.data is List) {
      List<dynamic> filters = filtersResponse?.data as List<dynamic>;

      for (var element in filters) {
        await marketPlaceService.removeMarketplaceFilter(
            filterId: element['ID']);
      }
    }

    Map<String, dynamic> tempPayload = enterprise == 3
        ? {
            'page': 1,
            'size': 10,
          }
        : enterprise == 2
            ? {'page': 1, 'size': 10, 'contractType': 'Fagentreprise'}
            : {'page': 1, 'size': 10, 'contractType': 'Hovedentreprise'};

    tempPayload.addAll(payload);

    Map<String, String> stringPayload = {
      for (var entry in tempPayload.entries) entry.key: entry.value.toString()
    };

    Uri uri = Uri(
      queryParameters: stringPayload,
    );

    await marketPlaceService.saveMarketplaceFilters(filter: '?${uri.query}');

    final marketJobsResponse =
        await marketPlaceService.marketplaceV2(payload: tempPayload);

    _currentMarketplacePayload = {...tempPayload};

    if (marketJobsResponse != null) {
      _marketJobs = marketJobsResponse.items ?? [];

      _lastPage = marketJobsResponse.lastPage;
    }

    _currentPage = 1;

    final response = await marketPlaceService.getMarketplaceFilters();

    if (response != null) {
      if ((response.data is List) && (response.data as List).isNotEmpty) {
        _marketPlaceFilterId = (response.data as List).first['ID'];

        var uri = Uri.parse(
            "${applic.bitrixApiUrl}/partner/projects/marketplaceV2${(response.data as List).first['FILTER']}");

        String jobIndustry = uri.queryParameters['jobIndustry'] ?? '';
        String region = uri.queryParameters['region'] ?? '';
        String zip = uri.queryParameters['zip'] ?? '';

        if (jobIndustry.isNotEmpty) {
          final tempJobIndustry = jobIndustry.split(',').toSet().toList();
          for (var job in tempJobIndustry) {
            _savedFilterFullIndustries.add(
              _fullIndustries.firstWhere(
                (element) => element.branchId == int.parse(job),
                orElse: () => response_model.Data(),
              ),
            );
          }
          payload.addAll({
            "jobIndustry":
                _savedFilterFullIndustries.map((e) => e.branchId).join(',')
          });
        }

        if (region.isNotEmpty) {
          final tempRegion = region.split(',').toSet().toList();
          for (var reg in tempRegion) {
            _savedFilterRegionList.add(
              _regionList.firstWhere(
                (element) => (element.name ?? '').contains(reg),
                orElse: () => RegionModel.defaults(),
              ),
            );
          }
          payload.addAll({
            "region": _savedFilterRegionList
                .map((e) => (e.name ?? '').split(' ').last)
                .join(',')
          });
        }

        if (zip.isNotEmpty) {
          final tempZip = zip.split(',');

          for (var zips in tempZip) {
            _savedFilterDenmarkCities.add(
              _denmarkCities.firstWhere(
                (element) => element.postalCode == int.parse(zips),
                orElse: () => DenmarkCities(),
              ),
            );
          }
          payload.addAll({
            "zip":
                _savedFilterDenmarkCities.map((e) => (e.name ?? '')).join(',')
          });
        }
      }
    }

    setBusy(false);

    notifyListeners();
  }

  Future<void> removeMarketplaceFilter() async {
    setBusy(true);

    final marketPlaceService = context.read<MarketPlaceService>();

    final filtersResponse = await marketPlaceService.getMarketplaceFilters();

    if (filtersResponse?.data is List) {
      List<dynamic> filters = filtersResponse?.data as List<dynamic>;

      for (var element in filters) {
        await marketPlaceService.removeMarketplaceFilter(
            filterId: element['ID']);
      }
    }

    _savedFilterDenmarkCities = [];
    _savedFilterFullIndustries = [];
    _savedFilterRegionList = [];
    _marketPlaceFilterId = 0;

    Map<String, dynamic> payload = enterprise == 3
        ? {
            'page': 1,
            'size': 10,
          }
        : enterprise == 2
            ? {'page': 1, 'size': 10, 'contractType': 'Fagentreprise'}
            : {'page': 1, 'size': 10, 'contractType': 'Hovedentreprise'};

    final marketJobsResponse =
        await marketPlaceService.marketplaceV2(payload: payload);

    _currentMarketplacePayload = {...payload};

    if (marketJobsResponse != null) {
      _marketJobs = marketJobsResponse.items ?? [];

      _lastPage = marketJobsResponse.lastPage;
    }

    _currentPage = 1;

    setBusy(false);

    notifyListeners();
  }

  Future<void> marketplaceV2({Map<String, dynamic>? payload}) async {
    final marketPlaceService = context.read<MarketPlaceService>();

    setBusy(true);

    final marketJobsResponse =
        await marketPlaceService.marketplaceV2(payload: payload);

    if (marketJobsResponse != null) {
      _marketJobs = marketJobsResponse.items ?? [];

      _lastPage = marketJobsResponse.lastPage;
    }

    _currentMarketplacePayload = {...payload!};

    notifyListeners();
    setBusy(false);
  }

  Future<void> getSupplierInfoIndustry(
      {bool isInit = false, int? enterprise}) async {
    final marketPlaceService = context.read<MarketPlaceService>();
    var settingService = context.read<SettingsService>();

    List<response_model.Data> tempFullIndustries = [];
    List<DenmarkCities> tempDenmarkCities = [];
    List<RegionModel> tempRegionList = [];

    if (isInit) {
      _enterprise = enterprise;
    }

    Map<String, dynamic> payload = {};

    if (_enterprise != null) {
      if (_enterprise == 3) {
        payload = {
          'page': _currentPage,
          'size': 10,
        };
      } else if (_enterprise == 2) {
        payload = {
          'page': _currentPage,
          'size': 10,
          'contractType': 'Fagentreprise',
        };
      } else {
        payload = {
          'page': _currentPage,
          'size': 10,
          'contractType': 'Hovedentreprise',
        };
      }
    } else {
      payload = {
        'page': _currentPage,
        'size': 10,
      };
    }

    final response = await marketPlaceService.getMarketplaceFilters();

    if (response != null) {
      if ((response.data is List) && (response.data as List).isNotEmpty) {
        final industriesResponse = await settingService.getTaskTypesFromCRM();
        if (industriesResponse != null) {
          tempFullIndustries =
              response_model.Data.fromCollection(industriesResponse.data);
        }

        final citiesResponse = await settingService.getDenmarkCities();
        if (citiesResponse != null) {
          tempDenmarkCities = DenmarkCities.fromCollection(citiesResponse.data);
        }

        final regionResponse = await settingService.getAllRegions();
        if (regionResponse != null) {
          tempRegionList = RegionModel.fromCollection(regionResponse.data);
        }

        _marketPlaceFilterId = (response.data as List).first['ID'];

        var uri = Uri.parse(
            "${applic.bitrixApiUrl}/partner/projects/marketplaceV2${(response.data as List).first['FILTER']}");

        String jobIndustry = uri.queryParameters['jobIndustry'] ?? '';
        String region = uri.queryParameters['region'] ?? '';
        String zip = uri.queryParameters['zip'] ?? '';

        if (jobIndustry.isNotEmpty) {
          final tempJobIndustry = jobIndustry.split(',').toSet().toList();
          _savedFilterFullIndustries.clear();

          for (var job in tempJobIndustry) {
            _savedFilterFullIndustries.add(
              tempFullIndustries.firstWhere(
                (element) => element.branchId == int.parse(job),
                orElse: () => response_model.Data(),
              ),
            );
          }
          payload.addAll({
            "jobIndustry":
                savedFilterFullIndustries.map((e) => e.branchId).join(',')
          });
        }

        if (region.isNotEmpty) {
          final tempRegion = region.split(',').toList().toSet();
          _savedFilterRegionList.clear();
          for (var reg in tempRegion) {
            _savedFilterRegionList.add(
              tempRegionList.firstWhere(
                (element) => (element.name ?? '').contains(reg),
                orElse: () => RegionModel.defaults(),
              ),
            );
          }
          payload.addAll({
            "region": _savedFilterRegionList
                .map((e) => (e.name ?? '').split(' ').last)
                .join(',')
          });
        }

        if (zip.isNotEmpty) {
          final tempZip = zip.split(',');

          for (var zips in tempZip) {
            _savedFilterDenmarkCities.add(
              tempDenmarkCities.firstWhere(
                (element) => element.postalCode == int.parse(zips),
                orElse: () => DenmarkCities.defaults(),
              ),
            );
          }
          payload.addAll({
            "zip":
                _savedFilterDenmarkCities.map((e) => (e.name ?? '')).join(',')
          });
        }

        _fullIndustries = [...tempFullIndustries];
        _denmarkCities = [...tempDenmarkCities];
        _regionList = [...tempRegionList];
      }
    }

    await marketplaceV2(payload: payload).then((_) {
      if (_marketJobs.isEmpty) {
        emptyMarketplaceNotification();
      }
    });

    notifyListeners();
  }

  Future<void> getListPriceAi({
    int? parentProjectId,
    required MarketPlaceV2Items item,
    int? jobIndustryId,
  }) async {
    isGettingListPrice = true;

    final wizardService = context.read<WizardService>();

    final listPriceAi = await wizardService.getListPriceAi(
        parentProjectId: parentProjectId!, contactId: item.homeowner!.id!);

    if (listPriceAi != null) {
      if ((item.contractType ?? '') == 'Hovedentreprise') {
        if (jobIndustryId != null && jobIndustryId != 1062) {
          for (var i = 0; i < listPriceAi.length; i++) {
            listPriceAi[i]
                .jobIndustries!
                .removeWhere((element) => element.jobIndustryId == '1062');
          }
        } else {
          for (var i = 0; i < listPriceAi.length; i++) {
            listPriceAi[i]
                .jobIndustries!
                .retainWhere((element) => element.jobIndustryId == '1062');
          }
          listPriceAi
              .retainWhere((element) => element.jobIndustries!.isNotEmpty);
        }
        _listPriceAi = listPriceAi;
      } else {
        List<String> jobIndustries = [];

        for (final job in item.jobsList as List) {
          if (job is List) {
            for (final item in job) {
              jobIndustries.add(item.toString().split(':').first);
            }
          } else {
            jobIndustries.add(job.toString().split(':').first);
          }
        }

        List<EnterpriseIndustry> temp = [];
        List<EnterpriseIndustry> tempSub = [];

        for (final price in listPriceAi) {
          List<JobIndustry> primary = [];
          List<JobIndustry> secondary = [];
          for (final job in price.jobIndustries!) {
            if (jobIndustries.contains(job.jobIndustryId)) {
              primary.add(job);
            } else {
              secondary.add(job);
            }
          }
          if (primary.isNotEmpty) {
            temp.add(
              EnterpriseIndustry(
                enterpriseId: price.enterpriseId,
                jobIndustries: primary,
              ),
            );
          }

          if (secondary.isNotEmpty) {
            tempSub.add(
              EnterpriseIndustry(
                enterpriseId: price.enterpriseId,
                jobIndustries: secondary,
              ),
            );
          }
        }

        _listPriceAi = temp;
        _subListPriceAi = tempSub;

        // List<EnterpriseIndustry> tempSub = [];
        // final tempSubList = [];
        // tempSubList.addAll(listPriceAi);

        // for (final price in tempSubList) {
        //   price.jobIndustries.removeWhere(
        //       (element) => jobIndustries.contains(element.jobIndustryId));
        //   if (price.jobIndustries.isNotEmpty) tempSub.add(price);
        // }

        // _subListPriceAi = tempSub;
      }
    }

    isGettingListPrice = false;
    notifyListeners();
  }

  Future<void> reserveJob({
    required int jobId,
    String? contractType,
    required bool toBuy,
    int mesterId = 0,
  }) async {
    final marketPlaceService = context.read<MarketPlaceService>();

    final reserveResponse = await marketPlaceService.reserveJob(
        jobId: jobId, toBuy: toBuy, mesterId: mesterId);

    _reserved = reserveResponse;

    notifyListeners();

    // if (_reserved.containsKey('success') && _reserved['success']) {
    //   Map<String, dynamic> payload = {...currentMarketplacePayload};
    //   payload['page'] = _currentPage;

    //   await marketplaceV2(payload: payload);
    // }
  }

  Future<void> jobsView({required int projectId}) async {
    final marketPlaceService = context.read<MarketPlaceService>();
    await marketPlaceService.jobsView(projectId: projectId);
  }

  Future<void> checkReserveMore({required int projectId}) async {
    final martketPlaceService = context.read<MarketPlaceService>();

    checkReserveResponse =
        await martketPlaceService.checkReserveMore(projectId: projectId);
  }

  Future<bool> viewMarketPlaceVideo() async {
    final martketPlaceService = context.read<MarketPlaceService>();

    return await martketPlaceService.marketplaceViewVideo();
  }

  Future<void> initReservationDialogs({
    required BuildContext context,
    required MarketPlaceV2Items jobItem,
  }) async {
    ///

    setReserveValuesToDefault();
    currentJobItem = jobItem;
    isReserving = true;

    await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return const PopScope(
          canPop: false,
          child: ReserveDialog(),
        );
      },
    );

    ///
  }

  Future<void> updateReservationDialogs({required BuildContext context}) async {
    ///

    final packageVm = context.read<PackagesViewModel>();
    final jobsVm = context.read<ClientsViewModel>();

    if (packageVm.marketPlaceReserve.feature.id == '' ||
        !packageVm.marketPlaceReserve.status) {
      Navigator.pop(context);
      await showUnAvailableDialog(
        context,
        packageVm.marketPlaceReserve,
        popTwice: false,
      );
    } else if ((packageVm.packagesResponse!.data!.nummode! <
        int.parse(packageVm.packagesResponse!.data!.reserveSize!))) {
      isReserveSuccess = RequestResponse.idle;
    } else {
      if (packageVm.playMarketPlaceVideo) {
        jobsVm.statusIds = '1,2';
        jobsVm.size = 2;
        jobsVm.withOffers = false;
        if (_currentJobItem?.id != null) {
          await Future.wait([
            jobsVm.getPartnerJobsV2(),
            checkReserveMore(projectId: _currentJobItem!.id!),
          ]);
        }
      }
      isReserveSuccess = RequestResponse.pre;
    }

    isReserving = false;

    ///
  }

  Future<void> normalReserve({
    required BuildContext context,
    bool fromVideo = false,
  }) async {
    // Check if _currentJobItem is not null and its id is not null
    if (_currentJobItem != null && _currentJobItem!.id != null) {
      isReserving = true;

      if (fromVideo) {
        await viewMarketPlaceVideo();
      }

      await reserveJob(
        jobId: _currentJobItem!.id!,
        contractType: _currentJobItem!.contractType,
        toBuy: false,
      );

      isReserving = false;

      if (_reserved.containsKey('success') && _reserved['success']) {
        isReserveSuccess = RequestResponse.success;

        // Future.delayed(const Duration(milliseconds: 800), () {
        //   Navigator.pop(context);
        //   if (_reserved['success']) {
        //     log("message lol");
        //     context.read<CreateOfferViewModel>().isNewReserve = true;
        //     changeDrawerReplaceRoute(Routes.yourClient);
        //     setMarketPlaceToDefault();
        //   }
        // });
      } else {
        if (context.mounted) {
          isReserveSuccess = RequestResponse.failed;
          reserveFailedMessage = tr('reserve_failed');
        }
      }
    } else {
      // Handle the case when _currentJobItem or its id is null
    }
  }

  Future<void> buyReserveSlots({required BuildContext context}) async {
    ///

    isReserving = true;

    await reserveJob(jobId: _currentJobItem!.id!, toBuy: true);

    isReserving = false;

    final reserveSuccess = _reserved['success'];

    if (reserveSuccess) {
      isReserveSuccess = RequestResponse.success;
    } else {
      isReserveSuccess = RequestResponse.failed;
      if (context.mounted) {
        reserveFailedMessage = tr('reserve_failed');
      }
    }

    ///
  }

  Future<void> emptyMarketplaceNotification() async {
    final marketPlaceService = context.read<MarketPlaceService>();
    await marketPlaceService.emptyMarketplaceNotification();
  }

  Future<void> getSortFilters() async {
    final settingService = context.read<SettingsService>();

    final taskTypeResponse = await settingService.getTaskTypesFromCRM();
    final citiesResponse = await settingService.getDenmarkCities();
    final regionResponse = await settingService.getAllRegions();

    if (taskTypeResponse != null) {
      _fullIndustries =
          response_model.Data.fromCollection(taskTypeResponse.data);
    }

    if (citiesResponse != null) {
      _denmarkCities = DenmarkCities.fromCollection(citiesResponse.data);
    }

    if (regionResponse != null) {
      _regionList = RegionModel.fromCollection(regionResponse.data);
    }

    notifyListeners();
  }
}
