import 'dart:developer';

import 'package:Haandvaerker.dk/model/marketplace/udbud_response_model.dart';
import 'package:Haandvaerker.dk/services/marketplace/marketplace/marketplace_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

enum SortMode { none, city, createdDate, editDate, deadlineDate }

class UdbudJobsViewModel extends BaseViewModel {
  UdbudJobsViewModel({required this.context});

  final BuildContext context;

  List<UdbudModel>? _allJobs = [];
  List<UdbudModel>? get allJobs => _allJobs;

  List<UdbudModel>? _filteredJobs = [];
  List<UdbudModel>? get filteredJobs => _filteredJobs;

  String _searchKey = "";
  String get searchKey => _searchKey;

  SortMode? _sortMode = SortMode.none;
  SortMode? get sortMode => _sortMode;

  List<String?> _allUniqueCities = [];
  List<String?> get allUniqueCities => _allUniqueCities;

  List<String?> _selectedCities = [];
  List<String?> get selectedCities => _selectedCities;

  void reset() {
    _allJobs = [];
    _filteredJobs = [];
    _searchKey = "";
    _sortMode = SortMode.none;
    _allUniqueCities = [];
    _selectedCities = [];

    notifyListeners();
  }

  Future<void> getUdbudJobs({bool isRefresh = false}) async {
    if (isRefresh == false) {
      setBusy(true);
    }
    try {
      final service = context.read<MarketPlaceService>();
      final response = await service.getUdbudJobs();
      _allJobs = response.data;
      // set all by default
      _filteredJobs = _allJobs;
      // get unique cities, to be used in filter
      _allUniqueCities = allJobs!.map((job) => job.city).toSet().toList();
    } catch (e) {
      log('${'getUdbudJobs error: '.toUpperCase()}$e');
    }
    setBusy(false);
  }

  void onSearchKeyChanged(String newSearchKey) {
    _searchKey = newSearchKey;
    notifyListeners();
    _filter();
  }

  void onSelectAllCity(bool isSelectAll) {
    //if true, select all cities,
    if (isSelectAll) {
      _selectedCities = _allUniqueCities;
    }
    //else deselect all cities
    else {
      _selectedCities = [];
    }

    notifyListeners();
    _filter();
  }

  void onCityFilter(String newSelectedCity) {
    if (_selectedCities.contains(newSelectedCity)) {
      //remove if already selected
      _selectedCities.remove(newSelectedCity);
    } else {
      _selectedCities.add(newSelectedCity);
    }
    debugPrint('_selectedCities: $_selectedCities');
    notifyListeners();
    _filter();
  }

  void onSortModeChanged(SortMode? sortMode) {
    _sortMode = sortMode;
    notifyListeners();
    _filter();
  }

  void _filter() {
    // search key filter
    if (_searchKey.isEmpty) {
      _filteredJobs = _allJobs;
    } else {
      _filteredJobs = _allJobs!
          .where((job) => (job.tenderTitle
                  .toString()
                  .toLowerCase()
                  .contains(_searchKey.toLowerCase()) ||
              job.city
                  .toString()
                  .toLowerCase()
                  .contains(_searchKey.toLowerCase())))
          .toList();
    }

    // city filter
    if (_selectedCities.isNotEmpty) {
      _filteredJobs = [
        ..._filteredJobs!.where((job) => _selectedCities.contains(job.city))
      ];
    }

    // sort filter
    if (_sortMode == SortMode.city) {
      _filteredJobs!.sort((a, b) => a.city!.compareTo(b.city!));
    } else if (_sortMode == SortMode.createdDate) {
      _filteredJobs!.sort((a, b) => a.dateCreated!.compareTo(b.dateCreated!));
    } else if (_sortMode == SortMode.editDate) {
      _filteredJobs!.sort((a, b) => a.dateEdited!.compareTo(b.dateEdited!));
    } else if (_sortMode == SortMode.deadlineDate) {
      _filteredJobs!
          .sort((a, b) => a.tenderDeadline!.compareTo(b.tenderDeadline!));
    }

    notifyListeners();
  }
}
