import 'package:Haandvaerker.dk/model/marketplace/job_invitation_response_model.dart';
import 'package:Haandvaerker.dk/services/marketplace/job_invitation/job_invitation_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class JobInvitationsViewModel extends BaseViewModel {
  JobInvitationsViewModel({required this.context});

  final BuildContext context;

  bool _expandFilter = false;
  bool get expandFilter => _expandFilter;

  JobInvitationPagination _invitationPagination =
      JobInvitationPagination.empty();
  JobInvitationPagination get invitationPagination => _invitationPagination;

  final List<String> filterStatuses = [
    'accepted',
    'pending',
    'rejected',
  ];

  String _currentFilterStatus = 'pending';
  String get currentFilterStatus => _currentFilterStatus;

  RequestResponse _isResponding = RequestResponse.idle;
  RequestResponse get isResponding => _isResponding;

  int? _currentInvitationId = 0;
  int? get currentInvitationId => _currentInvitationId;

  String? _currentInvitationStatus = '';
  String? get currentInvitationStatus => _currentInvitationStatus;

  void reset() {
    _expandFilter = false;
    _invitationPagination = JobInvitationPagination.empty();
    _currentFilterStatus = 'pending';
    _isResponding = RequestResponse.idle;
    _currentInvitationId = 0;
    _currentInvitationStatus = '';

    notifyListeners();
  }

  set expandFilter(bool filter) {
    _expandFilter = filter;
    notifyListeners();
  }

  set currentFilterStatus(String status) {
    _currentFilterStatus = status;
    notifyListeners();
  }

  set isResponding(RequestResponse response) {
    _isResponding = response;
    notifyListeners();
  }

  set currentInvitationId(int? id) {
    _currentInvitationId = id;
    notifyListeners();
  }

  set currentInvitationStatus(String? status) {
    _currentInvitationStatus = status;
    notifyListeners();
  }

  Future<void> getInvitation({required int page}) async {
    final jobInvitationService = context.read<JobInvitationService>();

    setBusy(true);

    final invitationsResponse = await jobInvitationService
        .getInvitationPagination(page: page, status: _currentFilterStatus);

    if (invitationsResponse != null) {
      if (!invitationsResponse.success!) throw invitationsResponse.message!;

      _invitationPagination = JobInvitationPagination.fromJson(
          invitationsResponse.data as Map<String, dynamic>);
    }

    notifyListeners();
    setBusy(false);
  }

  Future<void> acceptDeclineInvitation({
    required int invitationId,
    required int isAccept,
  }) async {
    final jobInvitationService = context.read<JobInvitationService>();

    final response = await jobInvitationService.acceptDeclineInvitation(
      invitationId: invitationId,
      isAccept: isAccept,
    );

    if (response['success'] == true && response['message'] == "Created.") {
      _isResponding = RequestResponse.success;
    }

    getInvitation(page: _invitationPagination.currentPage!);

    notifyListeners();
  }
}
