import 'dart:developer';
import 'dart:io';
import 'dart:math' as math;
import 'dart:typed_data';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/recommendation/company_reviews_model.dart';
import 'package:Haandvaerker.dk/model/recommendation/partners_reviews_model.dart';
import 'package:Haandvaerker.dk/model/recommendation/partners_reviews_on_client.dart';
import 'package:Haandvaerker.dk/model/recommendation/recommendation_model.dart';
// import 'package:Haandvaerker.dk/model/recommendations_detail_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
// import 'package:Haandvaerker.dk/services/message_service.dart';
import 'package:Haandvaerker.dk/services/recommendation/recommendation_service.dart';
import 'package:Haandvaerker.dk/services/wizard/wizard_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:file_selector/file_selector.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

class RecommendationReadAnswerViewModel extends BaseViewModel {
  RecommendationReadAnswerViewModel({required this.context});
  final BuildContext context;

  List<Recommendation> _recommendations = [];
  List<Recommendation> get recommendations => _recommendations;

  // List<PartnersReviewsOnClient> _partnersReviewsDoneOnClient = [];
  // List<PartnersReviewsOnClient> get partnersReviewsDoneOnClient =>
  //     _partnersReviewsDoneOnClient;

  List<CompanyReviewsModel> _companyReviews = [];
  List<CompanyReviewsModel> get companyReviews => _companyReviews;

  List<FullIndustry> _industries = [];
  List<FullIndustry> get industries => _industries;

  List<XFile> _selectedFileAndroid = [];
  List<XFile> get selectedFileAndroid => _selectedFileAndroid;

  List<PlatformFile> _selectedFileIOS = [];
  List<PlatformFile> get selectedFileIOS => _selectedFileIOS;

  PartnerReviewsModel? _partnerReviewsModel;
  PartnerReviewsModel? get partnerReviewsModel => _partnerReviewsModel;

  File? _file;
  File? get file => _file;

  bool _loadingDetails = false;
  bool get loadingDetails => _loadingDetails;

  bool _getRecommendationData = false;
  bool get getRecommendationData => _getRecommendationData;

  bool _isGettingRecommendationList = false;
  bool get isGettingRecommendationList => _isGettingRecommendationList;

  int _currentPage = 1;
  int get currentPage => _currentPage;

  int _lastPage = 1;
  int get lastPage => _lastPage;

  int _firstPage = 1;
  int get firstPage => _firstPage;

  double _currentRating = 0;
  double get currentRating => _currentRating;

  String _createdOnShow = '';
  String get createdOnShow => _createdOnShow;

  String _ratingCriteria = '';
  String get ratingCriteria => _ratingCriteria;

  String _reviewImageUrl = '';
  String get reviewImageUrl => _reviewImageUrl;

  String _reviewImageBase64 = '';
  String get reviewImageBase64 => _reviewImageBase64;

  String _reportReason = '';
  String get reportReason => _reportReason;

  String? _fileName;
  String? get fileName => _fileName;

  TextEditingController? _searchController;
  TextEditingController? get searchController => _searchController;

  FocusNode? _searchFocusNode;
  FocusNode? get searchFocusNode => _searchFocusNode;

  void reset() {
    _recommendations = [];
    _companyReviews = [];
    _industries = [];
    _selectedFileAndroid = [];
    _selectedFileIOS = [];
    _partnerReviewsModel = null;
    _file = null;
    _loadingDetails = false;
    _getRecommendationData = false;
    _isGettingRecommendationList = false;
    _currentPage = 1;
    _lastPage = 1;
    _firstPage = 1;
    _currentRating = 0;
    _createdOnShow = '';
    _ratingCriteria = '';
    _reviewImageUrl = '';
    _reviewImageBase64 = '';
    _reportReason = '';
    _fileName = null;
    _searchController = null;
    _searchFocusNode = null;

    notifyListeners();
  }

  set recommendations(List<Recommendation> recommendations) {
    _recommendations = recommendations;
    notifyListeners();
  }

  set companyReviews(List<CompanyReviewsModel> reviews) {
    _companyReviews = reviews;
    notifyListeners();
  }

  set industries(List<FullIndustry> industries) {
    _industries = industries;
    notifyListeners();
  }

  set loadingDetails(bool value) {
    _loadingDetails = value;
    notifyListeners();
  }

  set getRecommendationData(bool value) {
    _getRecommendationData = value;
    notifyListeners();
  }

  set currentPage(int page) {
    _currentPage = page;
    notifyListeners();
  }

  set lastPage(int page) {
    _lastPage = page;
    notifyListeners();
  }

  set firstPage(int page) {
    _firstPage = page;
    notifyListeners();
  }

  set currentRating(double rating) {
    _currentRating = rating;
    notifyListeners();
  }

  set createdOnShow(String date) {
    _createdOnShow = date;
    notifyListeners();
  }

  set ratingCriteria(String rating) {
    _ratingCriteria = rating;
    notifyListeners();
  }

  set reviewImageUrl(String url) {
    _reviewImageUrl = url;
    notifyListeners();
  }

  set reviewImageBase64(String base64) {
    _reviewImageBase64 = base64;
    notifyListeners();
  }

  set reportReason(String reason) {
    _reportReason = reason;
    notifyListeners();
  }

  Future<MinboligApiResponse> commentOnReview({
    required int reviewId,
    required String supplierComment,
  }) async {
    final service = context.read<RecommendationService>();

    return await service.commentOnReview(
      reviewId: reviewId,
      supplierComment: supplierComment,
    );
  }

  Future<MinboligApiResponse> createTradesmanComplaint({
    required String complaint,
    int? category,
    required String headline,
    required int reviewId,
    File? file,
    String? fileName,
  }) async {
    final service = context.read<RecommendationService>();

    return await service.createTradesmanComplaint(
      complaint: complaint,
      category: category,
      headline: headline,
      reviewId: reviewId,
      file: file,
      fileName: fileName,
    );
  }

  Future<MinboligApiResponse> savePartnerReviewOnHomeowner({
    required int customerId,
    required String headline,
    required String comment,
    required int enterpriseSum,
    required int ratingCommunication,
    required int ratingTime,
    required int ratingRealisticExpectations,
    required int ratingFinancialSecurity,
    required int ratingCollaboration,
    required int rating,
    required int ratingAccess,
  }) async {
    var service = context.read<RecommendationService>();

    try {
      var response = await service.savePartnerReviewOnHomeowner(
          customerId: customerId,
          headline: headline,
          comment: comment,
          enterpriseSum: 0,
          ratingCommunication: ratingCommunication,
          ratingTime: ratingTime,
          ratingRealisticExpectations: ratingRealisticExpectations,
          ratingFinancialSecurity: ratingFinancialSecurity,
          ratingCollaboration: ratingCollaboration,
          rating: rating,
          ratingAccess: ratingAccess);
      notifyListeners();

      return response;
    } catch (e) {
      log("$e");
    }

    throw Exception('error saving partner review');
  }

  // Future<List<PartnersReviewsOnClient>> getPartnersReviewsDoneOnClient() async {
  //   var service = context.read<RecommendationService>();
  //   try {
  //     var response = await service.getPartnersReviewsDoneOnClient();
  //     if (response.success!) {
  //       final overall = PartnersReviewsOnClient.fromCollection(response.data);
  //       _partnersReviewsDoneOnClient =
  //           overall.where((element) => element.ufStatus != 'DELETED').toList();
  //     }
  //     notifyListeners();
  //     return _partnersReviewsDoneOnClient;
  //   } catch (e) {
  //     log(e);
  //   }
  //   throw Exception('error getting partner review');
  // }

  Future<List<PartnersReviewsOnClient>> getPartnersReviewsDoneOnClientV2(
      {String? homeowner}) async {
    final service = context.read<RecommendationService>();

    try {
      final response = await service.getPartnersReviewsDoneOnClientV2(
          homeowner: homeowner ?? '', page: _currentPage);

      if (response != null) {
        // final overall =
        //     PartnersReviewsOnClient.fromCollection(response.data['data']);
        // _partnersReviewsDoneOnClient =
        //     overall.where((element) => element.ufStatus != 'DELETED').toList();
        _partnerReviewsModel = PartnerReviewsModel.fromJson(response.data);
        _lastPage = PartnerReviewsModel.fromJson(response.data).lastPage ?? 1;
        _firstPage = PartnerReviewsModel.fromJson(response.data).from ?? 1;
      }

      notifyListeners();

      return _partnerReviewsModel?.data ?? <PartnersReviewsOnClient>[];
    } catch (e) {
      log("$e");
    }

    throw Exception('error getting partner review');
  }

  Future<bool> getReviewImageDownload({required int reviewId}) async {
    final service = context.read<RecommendationService>();

    final response = await service.getReviewImageV2(reviewId: reviewId);

    if (response != null) {
      _reviewImageUrl = response.url ?? '';
      _reviewImageBase64 = response.base64 ?? '';

      log(_reviewImageUrl);

      String name = 'review_$reviewId';

      try {
        if (Platform.isAndroid) {
          final appDirectory = Directory('/storage/emulated/0/Download');
          final randid = math.Random().nextInt(10000);
          String savePath =
              '${appDirectory.path}/PartnerLogin/${name}_$randid.jpg';
          log(savePath);
          final downloadResponse =
              await Dio().download(_reviewImageUrl, savePath);

          return downloadResponse.statusCode == 200;
        } else if (Platform.isIOS) {
          final appDirectory = await getApplicationDocumentsDirectory();
          final randomId = math.Random().nextInt(10000);
          var filePath =
              "${appDirectory.path}/PartnerLogin/${name}_$randomId.jpg";
          var file = File(filePath);
          while (file.existsSync()) {
            file = File(filePath);
          }
          final downloadResponse =
              await Dio().download(_reviewImageUrl, filePath);

          await file.copy(filePath);

          return downloadResponse.statusCode == 200;
        } else {
          return false;
        }
        //
      } catch (e) {
        log('Error Image Download: $e');
        return false;
      }
    } else {
      return false;
    }
  }

  Future<bool?> deleteReviewById({required reviewId}) async {
    var service = context.read<RecommendationService>();

    try {
      var response = await service.deleteReviewById(reviewId: reviewId);

      notifyListeners();

      return response.success;
    } catch (e) {
      log("$e");
    }

    throw Exception('error getting partner review');
  }

  Future<bool?> updateSavePartnerReviewonHomeowner(
      {required reviewId,
      required customerId,
      required int enterpriseSum,
      required String comment,
      required double communicationRating,
      required String headline,
      required double timeRating,
      required double accessRating,
      required double financialRating,
      required double realisticExpectationRating,
      required double collaborationRating}) async {
    var service = context.read<RecommendationService>();

    try {
      var response = await service.updateSavePartnerReviewonHomeowner(
          accessRating: accessRating,
          collaborationRating: collaborationRating,
          comment: comment,
          communicationRating: communicationRating,
          customerId: customerId,
          enterpriseSum: enterpriseSum,
          financialRating: financialRating,
          headline: headline,
          realisticExpectationRating: realisticExpectationRating,
          reviewId: reviewId,
          timeRating: timeRating);

      notifyListeners();

      return response.success;
    } catch (e) {
      log("$e");
    }

    throw Exception('error getting partner review');
  }

  Future<Uri?> getReviewImageShare({required int reviewId}) async {
    final service = context.read<RecommendationService>();

    final response = await service.getReviewImageV2(reviewId: reviewId);

    Uri url;

    if (response != null) {
      _reviewImageUrl = response.url ?? '';
      _reviewImageBase64 = response.base64 ?? '';

      log(_reviewImageUrl);

      url = Uri.parse(_reviewImageUrl);

      return url;
    } else {
      return null;
    }
  }

  Future<void> getRecommendations({
    required int rating,
    String search = '',
  }) async {
    final service = context.read<RecommendationService>();

    _isGettingRecommendationList = true;

    if (_ratingCriteria.isNotEmpty) {
      switch (_ratingCriteria) {
        case 'all':
          final recommendationResponse = await service
              .getRecommendations(
            page: _currentPage,
            rating: rating,
            search: search,
          )
              .whenComplete(() {
            _isGettingRecommendationList = false;
          });

          if (recommendationResponse != null) {
            lastPage = recommendationResponse.lastPage!;
            recommendations = recommendationResponse.items;
          }
          break;
        case '5':
          final recommendationResponse = await service
              .getRecommendations(
            page: _currentPage,
            rating: rating,
            search: search,
          )
              .whenComplete(() {
            _isGettingRecommendationList = false;
          });

          if (recommendationResponse != null) {
            lastPage = recommendationResponse.lastPage!;
            recommendations = recommendationResponse.items;
          }
          break;
        default:
          final recommendationResponse = await service
              .getRecommendations(
            page: _currentPage,
            rating: rating,
            search: search,
          )
              .whenComplete(() {
            _isGettingRecommendationList = false;
          });

          if (recommendationResponse != null) {
            lastPage = recommendationResponse.lastPage!;
            recommendationResponse.items.sort((a, b) {
              dynamic rateA;
              dynamic rateB;
              if (a.rating.runtimeType == String) {
                if (a.rating.contains('.')) {
                  rateA = double.parse(a.rating);
                } else {
                  rateA = int.parse(a.rating);
                }
              } else {
                rateA = a.rating;
              }
              if (b.rating.runtimeType == String) {
                if (b.rating.contains('.')) {
                  rateB = double.parse(b.rating);
                } else {
                  rateB = int.parse(b.rating);
                }
              } else {
                rateB = b.rating;
              }

              return rateA.compareTo(rateB);
            });
            recommendations = recommendationResponse.items;
          }
          break;
      }
    } else {
      final recommendationResponse = await service
          .getRecommendations(
        page: _currentPage,
        rating: rating,
        search: search,
      )
          .whenComplete(() {
        _isGettingRecommendationList = false;
      });

      if (recommendationResponse != null) {
        lastPage = recommendationResponse.lastPage!;
        recommendations = recommendationResponse.items;
      }
    }

    notifyListeners();
  }

  Future<void> getIndustries() async {
    final service = context.read<WizardService>();
    final response = await service.getFullIndustry();

    if (response != null) {
      _industries = response;
    }

    notifyListeners();
  }

  Future<void> getReviewsCompanyV2() async {
    final recommendationService = context.read<RecommendationService>();

    final response = await recommendationService.getReviewsCompanyV2();

    companyReviews = response;
  }

  Future<void> setFileForReport(
      {XFile? xFile, PlatformFile? platformFile}) async {
    String fileName = '';
    String fileExt = '';
    if (xFile != null) {
      fileName = xFile.name.replaceAll(RegExp('[^A-Za-z0-9]'), '');
      fileExt = xFile.mimeType!.split('/').last;
      final bytes = xFile.readAsBytes();
      _file = File.fromRawPath(bytes as Uint8List);
      _fileName = '$fileName.$fileExt';
    }

    if (platformFile != null) {
      fileName = platformFile.name;
      fileExt = platformFile.extension ?? '';
      _file = File(platformFile.path ?? '');
      _fileName = '$fileName.$fileExt';
    }

    notifyListeners();
  }

  Future<void> addSelectedFileAndroid({XFile? file}) async {
    if (file != null) {
      if (_selectedFileAndroid.isEmpty) {
        _selectedFileAndroid.add(file);
      } else {
        deleteSelectedFileAndroid(index: 0)
            .whenComplete(() => _selectedFileAndroid.add(file));
      }
    }

    notifyListeners();
  }

  Future<void> deleteSelectedFileAndroid({int? index}) async {
    if (index != null) {
      _selectedFileAndroid.removeAt(index);
      _file = null;
      _fileName = null;
    }

    notifyListeners();
  }

  Future<void> addSelectedFileIOS({PlatformFile? file}) async {
    if (file != null) {
      if (_selectedFileIOS.isEmpty) {
        _selectedFileIOS.add(file);
      } else {
        deleteSelectedFileAndroid(index: 0)
            .whenComplete(() => _selectedFileIOS.add(file));
      }
    }

    notifyListeners();
  }

  Future<void> deleteSelectedFileIOS({int? index}) async {
    if (index != null) {
      _selectedFileIOS.removeAt(index);
      _file = null;
      _fileName = null;
    }

    notifyListeners();
  }

  Future<void> initReportRecommend() async {
    _reportReason = '';
    _selectedFileAndroid = [];
    _selectedFileIOS = [];
    _file = null;
    _fileName = null;
    notifyListeners();
  }

  int checkRecommendationWithinLastMonth() {
    var currentDate = DateTime.now();
    var lastMonth =
        DateTime(currentDate.year, currentDate.month - 1, currentDate.day);

    int count = 0;

    for (var element in recommendations) {
      if (DateTime.parse(element.date!).isAfter(lastMonth)) {
        count++;
      }
    }

    return count;
  }

  String createdOnShowFormatter(String date) {
    String year;
    String month;
    String day;
    String tempMonth;
    if (date.isNotEmpty) {
      if (date.contains('-')) {
        if (date.split('-').first.isNotEmpty &&
            date.split('-').first.length == 4) {
          year = date.split('-').first;
          day = date.split('-').last;

          if (date.split('-')[1].isNotEmpty && date.split('-')[1].length < 2) {
            tempMonth = '0${date.split('-')[1]}';
          } else {
            tempMonth = date.split('-')[1];
          }

          switch (tempMonth) {
            case '01':
              month = 'Januar';
              break;
            case '02':
              month = 'Februar';
              break;
            case '03':
              month = 'Marts';
              break;
            case '04':
              month = 'April';
              break;
            case '05':
              month = 'Maj';
              break;
            case '06':
              month = 'Juni';
              break;
            case '07':
              month = 'Juli';
              break;
            case '08':
              month = 'August';
              break;
            case '09':
              month = 'September';
              break;
            case '10':
              month = 'Oktober';
              break;
            case '11':
              month = 'November';
              break;
            case '12':
              month = 'December';
              break;
            default:
              month = date.split('-')[1];
              break;
          }

          return '$day. $month $year';
        } else if (date.split('-').first.isNotEmpty &&
            date.split('-').first.length != 4) {
          day = date.split('-').first;
          year = date.split('-').last;

          if (date.split('-')[1].isNotEmpty && date.split('-')[1].length < 2) {
            tempMonth = '0${date.split('-')[1]}';
          } else {
            tempMonth = date.split('-')[1];
          }

          switch (tempMonth) {
            case '01':
              month = 'Januar';
              break;
            case '02':
              month = 'Februar';
              break;
            case '03':
              month = 'Marts';
              break;
            case '04':
              month = 'April';
              break;
            case '05':
              month = 'Maj';
              break;
            case '06':
              month = 'Juni';
              break;
            case '07':
              month = 'Juli';
              break;
            case '08':
              month = 'August';
              break;
            case '09':
              month = 'September';
              break;
            case '10':
              month = 'Oktober';
              break;
            case '11':
              month = 'November';
              break;
            case '12':
              month = 'December';
              break;
            default:
              month = date.split('-')[1];
              break;
          }

          return '$day. $month $year';
        } else {
          return '';
        }
      } else if (date.contains('/')) {
        if (date.split('/').first.isNotEmpty &&
            date.split('/').first.length == 4) {
          year = date.split('/').first;
          day = date.split('/').last;

          if (date.split('/')[1].isNotEmpty && date.split('/')[1].length < 2) {
            tempMonth = '0${date.split('/')[1]}';
          } else {
            tempMonth = date.split('/')[1];
          }

          switch (tempMonth) {
            case '01':
              month = 'Januar';
              break;
            case '02':
              month = 'Februar';
              break;
            case '03':
              month = 'Marts';
              break;
            case '04':
              month = 'April';
              break;
            case '05':
              month = 'Maj';
              break;
            case '06':
              month = 'Juni';
              break;
            case '07':
              month = 'Juli';
              break;
            case '08':
              month = 'August';
              break;
            case '09':
              month = 'September';
              break;
            case '10':
              month = 'Oktober';
              break;
            case '11':
              month = 'November';
              break;
            case '12':
              month = 'December';
              break;
            default:
              month = date.split('/')[1];
              break;
          }

          return '$day. $month $year';
        } else if (date.split('/').first.isNotEmpty &&
            date.split('/').first.length != 4) {
          day = date.split('/').first;
          year = date.split('/').last;

          if (date.split('/')[1].isNotEmpty && date.split('/')[1].length < 2) {
            tempMonth = '0${date.split('/')[1]}';
          } else {
            tempMonth = date.split('/')[1];
          }

          switch (tempMonth) {
            case '01':
              month = 'Januar';
              break;
            case '02':
              month = 'Februar';
              break;
            case '03':
              month = 'Marts';
              break;
            case '04':
              month = 'April';
              break;
            case '05':
              month = 'Maj';
              break;
            case '06':
              month = 'Juni';
              break;
            case '07':
              month = 'Juli';
              break;
            case '08':
              month = 'August';
              break;
            case '09':
              month = 'September';
              break;
            case '10':
              month = 'Oktober';
              break;
            case '11':
              month = 'November';
              break;
            case '12':
              month = 'December';
              break;
            default:
              month = date.split('/')[1];
              break;
          }

          return '$day. $month $year';
        } else {
          return '';
        }
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  void setSearchControllers() {
    _searchController = TextEditingController();
    _searchFocusNode = FocusNode();
  }
}
