import 'package:Haandvaerker.dk/model/partner/supplier_info_model.dart';
import 'package:Haandvaerker.dk/model/partner_haa_model.dart';
import 'package:Haandvaerker.dk/model/recommendation/invited_customer_model.dart';
import 'package:Haandvaerker.dk/services/main/main_service.dart';
import 'package:Haandvaerker.dk/services/recommendation/recommendation_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WidgetsViewModel extends BaseViewModel {
  WidgetsViewModel({required this.context});
  final BuildContext context;

  PartnerHaaModel _partner = PartnerHaaModel();
  PartnerHaaModel get partner => _partner;

  SupplierInfoModel? _company;
  SupplierInfoModel? get company => _company;

  String _recommendationBanner = '';
  String get recommendationBanner => _recommendationBanner;

  String _testimonialBanner = '';
  String get testimonialBanner => _testimonialBanner;

  List<InvitedCustomerModel> _invitedCustomers = <InvitedCustomerModel>[];
  List<InvitedCustomerModel> get invitedCustomers => _invitedCustomers;

  String _companyOfferText = '';
  String get companyOfferText => _companyOfferText;

  void reset() {
    _partner = PartnerHaaModel();
    _company = null;
    _recommendationBanner = '';
    _testimonialBanner = '';
    _invitedCustomers = <InvitedCustomerModel>[];
    _companyOfferText = '';

    notifyListeners();
  }

  void setBannerValues() {
    _recommendationBanner =
        "<script type=\"text/javascript\" async=\"\" src=\"https://widget.haandvaerker.dk/Content/3.0/js/modules/HaandverkerDKWidget.js\"></script><div class=\"hvdkwidget\" data-firmaid=\"${context.read<UserViewModel>().userModel?.user?.id}\" data-kat=\"rate\" data-firma=\"${context.read<UserViewModel>().userModel?.user?.name}\" data-nommer=\"\"></div>";

    _testimonialBanner =
        "<script type=\"text/javascript\" async=\"\" src=\"https://widget.haandvaerker.dk/Content/3.0/js/modules/HaandverkerDKWidget.js\"></script><div class=\"hvdkwidget\" data-firmaid=\"${context.read<UserViewModel>().userModel?.user?.id}\" data-kat=\"comment\" data-firma=\"${context.read<UserViewModel>().userModel?.user?.name}\" data-nommer=\"5\" data-order=\"1\"></div>";

    notifyListeners();
  }

  set company(SupplierInfoModel? company) {
    _company = company;

    notifyListeners();
  }

  set invitedCustomers(List<InvitedCustomerModel> customers) {
    _invitedCustomers = customers;
    notifyListeners();
  }

  set companyOfferText(String text) {
    _companyOfferText = text;
    notifyListeners();
  }

  //BITRIX COUNTERPART
  Future<bool?> getSupplierInfo() async {
    final recommendationService = context.read<MainService>();

    final response = await recommendationService.getSupplierInfo();

    if (response != null) {
      if (response.success!) {
        company =
            SupplierInfoModel.fromJson(response.data as Map<String, dynamic>);

        companyOfferText = company!.bCrmCompany!.companyOfferNote ?? '';
      }

      return response.success!;
    }
    return null;
  }

  Future<void> getInvitedCustomers() async {
    //

    final recommendationService = context.read<RecommendationService>();
    final response = await recommendationService.getInvitedCustomers();

    if (response != null) {
      invitedCustomers = response;
    }

    //
  }

  Future<bool> inviteCustomer({
    required String name,
    required String email,
    required String phone,
  }) async {
    //

    final recommendationService = context.read<RecommendationService>();

    return await recommendationService.inviteCustomer(
      name: name,
      email: email,
      phone: phone,
    );

    //
  }

  Future<bool> deleteInvitedCustomer({required String inviteId}) async {
    //

    final recommendationService = context.read<RecommendationService>();

    return await recommendationService.deleteInvitedCustomer(
      inviteId: inviteId,
    );

    //
  }
}
