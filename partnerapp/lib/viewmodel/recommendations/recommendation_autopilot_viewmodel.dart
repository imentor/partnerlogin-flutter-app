import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/integration/integration_model.dart';
import 'package:Haandvaerker.dk/services/contract/contract_service.dart';
import 'package:Haandvaerker.dk/services/recommendation/recommendation_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RecommendationAutoPilotViewModel extends BaseViewModel {
  RecommendationAutoPilotViewModel({required this.context});
  final BuildContext context;

  List<IntegrationModel> _integrations = <IntegrationModel>[];
  List<IntegrationModel> get integrations => _integrations;

  String _fbRecommendationBanner = 'assets/images/integrations_fb_banner.jpeg';
  String get fbRecommendationBanner => _fbRecommendationBanner;

  String _termsAndConditionsLabel = '';
  String get termsAndConditionsLabel => _termsAndConditionsLabel;

  String _termsAndConditionsHtml = '';
  String get termsAndConditionsHtml => _termsAndConditionsHtml;

  String _dineroFirma = '';
  String get dineroFirma => _dineroFirma;

  String _dinero = '';
  String get dinero => _dinero;

  String _facebook = '';
  String get facebook => _facebook;

  String _economic = '';
  String get economic => _economic;

  String _billy = '';
  String get billy => _billy;

  void reset() {
    _integrations = <IntegrationModel>[];
    _fbRecommendationBanner = 'assets/images/integrations_fb_banner.jpeg';
    _termsAndConditionsLabel = '';
    _termsAndConditionsHtml = '';
    _dineroFirma = '';
    _dinero = '';
    _facebook = '';
    _economic = '';
    _billy = '';

    notifyListeners();
  }

  set dineroFirma(String text) {
    _dineroFirma = text;
    notifyListeners();
  }

  set dinero(String text) {
    _dinero = text;
    notifyListeners();
  }

  set facebook(String text) {
    _facebook = text;
    notifyListeners();
  }

  set economic(String text) {
    _economic = text;
    notifyListeners();
  }

  set billy(String text) {
    _billy = text;
    notifyListeners();
  }

  set integrations(List<IntegrationModel> integrations) {
    _integrations = integrations;
    notifyListeners();
  }

  Future<MinboligApiResponse?> getSupplierIntegrations() async {
    final recommendationService = context.read<RecommendationService>();

    final contractService = context.read<ContractService>();
    final termsAndConditionsResponse =
        await contractService.getTermsAndConditionsV3();
    final recommendationServiceResponse =
        await recommendationService.getSupplierIntegrations();

    if (termsAndConditionsResponse != null) {
      _termsAndConditionsLabel = termsAndConditionsResponse.data['LABEL'];
      _termsAndConditionsHtml = termsAndConditionsResponse.data['HTML'];
    }

    notifyListeners();

    if (recommendationServiceResponse != null) {
      return recommendationServiceResponse;
    } else {
      return null;
    }
  }

  Future<MinboligApiResponse> saveSupplierIntegration(
      {required Map<String, dynamic> payload}) async {
    final recommendationService = context.read<RecommendationService>();

    return await recommendationService.saveSupplierIntegration(
        payload: payload);
  }
}
