import 'package:Haandvaerker.dk/model/partner/partner_login_model.dart';
import 'package:Haandvaerker.dk/services/helper/helper_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AppLinksViewModel extends BaseViewModel {
  AppLinksViewModel({required this.context});

  BuildContext context;

  PartnerLoginModel? _appLinkLogin;
  PartnerLoginModel? get appLinkLogin => _appLinkLogin;

  String _pageRedirect = '';
  String get pageRedirect => _pageRedirect;

  String _appLinkToken = '';
  String get appLinkToken => _appLinkToken;

  String _projectId = '';
  String get projectId => _projectId;

  String _contractId = '';
  String get contractId => _contractId;

  String _contactId = '';
  String get contactId => _contactId;

  String _offerId = '';
  String get offerId => _offerId;

  String _type = '';
  String get type => _type;

  String _csUserId = '';
  String get csUserId => _csUserId;

  String _offerVersion = '';
  String get offerVersion => _offerVersion;

  String _subcontractorId = '';
  String get subcontractorId => _subcontractorId;

  bool _signOffer = false;
  bool get signOffer => _signOffer;

  Future<Map<String, dynamic>> extractParamsFromLink(
      {required Uri link}) async {
    String urlString = link.toString();

    if (urlString.contains('/#')) {
      urlString = urlString.split('/#').join();
    }

    final parsedUri = Uri.parse(urlString);
    return parsedUri.queryParameters;
  }

  void resetInitialAppLink() {
    final service = context.read<HelperService>();

    service.setinitialAppLink(link: '');
  }

  Future<void> loginFromAppLink() async {
    final service = context.read<HelperService>();

    final response = await service.validateParamsToken(token: appLinkToken);
    _appLinkLogin = response;

    notifyListeners();
  }

  Future<bool> processOutsideUri({required Uri uri}) async {
    final service = context.read<HelperService>();

    final cacheInitialAppLink = service.previousInitialAppLink();
    if (cacheInitialAppLink != uri.toString()) {
      service.setinitialAppLink(link: uri.toString());

      return true;
    } else {
      return false;
    }
  }

  Future<void> processUri(
      {required Uri uri, required bool fromInitialLink}) async {
    final service = context.read<HelperService>();
    if (fromInitialLink) {
      final cacheInitialAppLink = service.previousInitialAppLink();
      if (cacheInitialAppLink != uri.toString()) {
        service.setinitialAppLink(link: uri.toString());

        await decodeAppLinks(uri: uri, service: service);
      }
    } else {
      await decodeAppLinks(uri: uri, service: service);
    }
  }

  Future<void> decodeAppLinks(
      {required Uri uri, required HelperService service}) async {
    String urlString = uri.toString();

    if (urlString.contains('/#')) {
      urlString = urlString.split('/#').join();
    }

    final parsedUri = Uri.parse(urlString);
    final queryParameters = parsedUri.queryParameters;

    String code = queryParameters['code'].toString();
    String page = queryParameters['page'].toString();

    if (queryParameters['projectId'] != null) {
      _projectId = queryParameters['projectId'].toString();
    }
    if (queryParameters['contractId'] != null) {
      _contractId = queryParameters['contractId'].toString();
    }
    if (queryParameters['contactID'] != null) {
      _contactId = queryParameters['contactID'].toString();
    }
    if (queryParameters['offerid'] != null) {
      _offerId = queryParameters['offerid'].toString();
    }
    if (queryParameters['type'] != null) {
      _type = queryParameters['type'].toString();
    }
    if (queryParameters['csUserId'] != null) {
      _csUserId = queryParameters['csUserId'].toString();
    }
    if (queryParameters['offerVersion'] != null) {
      _offerVersion = queryParameters['offerVersion'].toString();
    }
    if (queryParameters['signOffer'] != null &&
        queryParameters['signOffer'] is bool) {
      _signOffer = queryParameters['signOffer'] as bool;
    }
    if (queryParameters['subcontractId'] != null) {
      _subcontractorId = queryParameters['subcontractId'].toString();
    }

    if (code.isNotEmpty || code != 'null' || code != 'Null') {
      final decodeResponse = await service.decodeAndValidateCode(code: code);

      if (decodeResponse['response'] != null &&
          "${decodeResponse['response']['ID']}" != decodeResponse['userId']) {
        _appLinkToken = decodeResponse['response']['TOKEN'];
      }

      _pageRedirect = page;
    }
    notifyListeners();
  }

  void resetLoginAppLink() {
    _pageRedirect = '';
    _appLinkToken = '';
    _appLinkLogin = null;
    _projectId = '';
    _contractId = '';
    _contactId = '';
    _offerId = '';
    _type = '';
    _csUserId = '';

    notifyListeners();
  }
}
