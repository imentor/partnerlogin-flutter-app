import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:upgrader/upgrader.dart';

class VersionCheckerViewmodel extends BaseViewModel {
  VersionCheckerViewmodel({required this.context});
  final BuildContext context;

  Upgrader _appUpgrader = Upgrader();
  Upgrader get appUpgrader => _appUpgrader;

  Future<void> checkUpdateAvailable() async {
    final PackageInfo packageInfo = await PackageInfo.fromPlatform();
    final upgrader = Upgrader(minAppVersion: packageInfo.version);
    await upgrader.initialize();

    _appUpgrader = upgrader;

    notifyListeners();
  }
}
