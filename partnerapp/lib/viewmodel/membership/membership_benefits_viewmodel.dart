import 'package:Haandvaerker.dk/model/membership_benefits_model.dart';
import 'package:Haandvaerker.dk/services/membership/membership_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MembershipBenefitsViewModel extends BaseViewModel {
  MembershipBenefitsViewModel({required this.context});

  final BuildContext context;

  List<MembershipItem> _memberships = [];
  List<MembershipItem> get memberships => _memberships;

  void reset() {
    _memberships = [];

    notifyListeners();
  }

  Future<void> getMembersV2() async {
    setBusy(true);
    var service = context.read<MembershipService>();

    var user = context.read<UserViewModel>();

    final response =
        await service.getMembersV2(zip: user.userModel!.user!.zip!);

    if (response != null) {
      _memberships = MembershipItem.fromCollection(response.data)
          .where((element) =>
              element.id != '124' &&
              element.id != '123' &&
              element.id != '1812')
          .toList();
    }

    notifyListeners();

    setBusy(false);
  }
}
