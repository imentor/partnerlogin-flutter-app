import 'dart:async';
import 'dart:developer';

import 'package:Haandvaerker.dk/services/location/location_service.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_background_geolocation/flutter_background_geolocation.dart'
    as bg;
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart' as google;
import 'package:provider/provider.dart';

class LocationViewModel extends BaseViewModel {
  LocationViewModel({required this.context});
  final BuildContext context;

  final GeolocatorPlatform _geolocatorPlatform = GeolocatorPlatform.instance;

  late LocationSettings locationSettings;

  google.LatLng _initialPosition = const google.LatLng(55.676098, 12.568337);
  google.LatLng get initialPosition => _initialPosition;

  StreamSubscription<Position>? _positionStreamSubscription;

  LocationPermission _locationPermission = LocationPermission.unableToDetermine;
  LocationPermission get locationPermission => _locationPermission;

  String _initialAddressName = '';
  String get initialAddressName => _initialAddressName;

  final int _locationPermissionStatus = 0;
  int get locationPermissionStatus => _locationPermissionStatus;

  bool _locationDialogShowed = false;
  bool get locationDialogShowed => _locationDialogShowed;

  bool _isLocationServiceEnabled = false;
  bool get isLocationServiceEnabled => _isLocationServiceEnabled;

  bool _isSearchingAddress = false;
  bool get isSearchingAddress => _isSearchingAddress;

  set isSearchingAddress(bool value) {
    _isSearchingAddress = value;
    notifyListeners();
  }

  Future<void> cancelLocationListener() async {
    _positionStreamSubscription?.cancel();
    _positionStreamSubscription = null;
    _initialPosition = const google.LatLng(55.676098, 12.568337);
    _isLocationServiceEnabled = false;
    _locationPermission = LocationPermission.unableToDetermine;
    _locationDialogShowed = false;
    _initialAddressName = '';

    await bg.BackgroundGeolocation.stop();
    notifyListeners();
  }

  Future<void> updateLocationInfo({required int gpsLocation}) async {
    final service = context.read<LocationService>();

    await service.updateLocationInfo(gpsLocation: gpsLocation);
  }

  Future<void> configureLocation() async {
    // final service = context.read<LocationService>();

    bg.BackgroundGeolocation.onLocation(
      (bg.Location location) async {
        log('[location] - ${location.coords.latitude},${location.coords.longitude}');

        // await service.sendLocation(
        //     lat: '${location.coords.latitude}',
        //     lng: '${location.coords.longitude}',
        //     floor: '${location.coords.floor}',
        //     speed: '${location.coords.speed}');
      },
      (p0) {
        log('[location] ERROR - ${p0.code} ${p0.message}');
      },
    );

    bg.BackgroundGeolocation.onMotionChange((bg.Location location) {
      log('[motionchange] - $location');
    });

    bg.BackgroundGeolocation.onProviderChange(
        (bg.ProviderChangeEvent event) async {
      log('[providerchange] - $event -> $locationPermission');

      final serviceEnabled =
          await _geolocatorPlatform.isLocationServiceEnabled();

      final permission = await _geolocatorPlatform.checkPermission();

      _isLocationServiceEnabled = serviceEnabled;
      _locationPermission = permission;

      notifyListeners();
    });

    bg.BackgroundGeolocation.onHttp((bg.HttpEvent event) {
      log('[HttpEvent] - ${event.status}');
    });

    bg.BackgroundGeolocation.ready(bg.Config(
      desiredAccuracy: bg.Config.DESIRED_ACCURACY_HIGH,
      distanceFilter: 200,
      stopOnTerminate: false,
      startOnBoot: true,
      batchSync: false,
      autoSync: true,
      autoSyncThreshold: 0,
      debug: false,
      locationsOrderDirection: 'DESC',
      logLevel: bg.Config.LOG_LEVEL_VERBOSE,
      enableHeadless: true,
      triggerActivities: 'on_foot,walking,running,on_bicycle,in_vehicle',
      allowIdenticalLocations: false,
      preventSuspend: false,
      showsBackgroundLocationIndicator: false,
      backgroundPermissionRationale: bg.PermissionRationale(
        title:
            'Tillad {applicationName} at få adgang til denne enheds placering i baggrunden?',
        message:
            'For at spore din aktivitet i baggrunden, skal du aktivere {backgroundPermissionOptionLabel} placeringstilladelse.',
        positiveAction: 'Ændr til {backgroundPermissionOptionLabel}',
        negativeAction: 'Cancel',
      ),
      locationAuthorizationAlert: {
        'titleWhenNotEnabled': 'Baggrundsplacering er ikke aktiveret',
        'titleWhenOff': 'Lokationstjenester er slået fra',
        'instructions':
            "For at bruge baggrundsplacering skal du aktivere {locationAuthorizationRequest} i Lokationstjenester indstillingerne",
        'cancelButton': 'Cancel',
        'settingsButton': "Indstillinger"
      },
      locationAuthorizationRequest: 'Always',
    )).then((bg.State state) {});
  }

  Future<void> requestPermissions() async {
    await bg.BackgroundGeolocation.requestPermission();

    bool serviceEnabled = false;
    LocationPermission permission = LocationPermission.denied;
    serviceEnabled = await _geolocatorPlatform.isLocationServiceEnabled();

    permission = await _geolocatorPlatform.checkPermission();

    _isLocationServiceEnabled = serviceEnabled;
    _locationPermission = permission;

    try {
      if (serviceEnabled && permission == LocationPermission.always) {
        final position = await _geolocatorPlatform.getCurrentPosition(
            locationSettings:
                const LocationSettings(accuracy: LocationAccuracy.high));

        _initialPosition = google.LatLng(position.latitude, position.longitude);
      }
    } catch (e) {
      log("message checkPermission() $e");
    }

    notifyListeners();
  }

  Future<void> startLocation() async {
    final service = context.read<LocationService>();

    final headers = await service.getHeaders();

    final isEnabled = (await bg.BackgroundGeolocation.state).enabled;

    await bg.BackgroundGeolocation.setConfig(bg.Config(
      headers: headers,
      httpRootProperty: 'data',
      locationTemplate:
          '{"lat":"<%= latitude %>","lang":"<%= longitude %>","speed":"<%= speed %>"}',
      url: '${applic.bitrixApiUrl}/partner/addGeoLocationV2',
    )).whenComplete(() async {
      await bg.BackgroundGeolocation.sync();
    });

    if (!isEnabled) {
      await bg.BackgroundGeolocation.start().whenComplete(() {
        bg.BackgroundGeolocation.changePace(true);
      });
    }

    await updateLocationInfo(gpsLocation: 1);
  }

  Future<void> checkLocationDialog() async {
    final service = context.read<LocationService>();

    _locationDialogShowed = service.getLocationDialog();
    notifyListeners();
  }

  Future<void> checkPermission() async {
    bool serviceEnabled = false;
    LocationPermission permission = LocationPermission.denied;
    serviceEnabled = await _geolocatorPlatform.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return;
    }
    permission = await _geolocatorPlatform.checkPermission();
    if (permission == LocationPermission.denied ||
        permission == LocationPermission.deniedForever) {
      permission = await _geolocatorPlatform.requestPermission();
      if (permission == LocationPermission.denied ||
          permission == LocationPermission.deniedForever) {
        return;
      }
    }

    _isLocationServiceEnabled = serviceEnabled;
    _locationPermission = permission;

    notifyListeners();
  }

  void setLocationDialog() {
    final service = context.read<LocationService>();

    _locationDialogShowed = true;

    service.setLocationDialog();

    notifyListeners();
  }
}
