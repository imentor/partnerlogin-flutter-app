import 'dart:developer';

import 'package:Haandvaerker.dk/model/autocomplete/autocomplete_model.dart';
import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/model/task_type/task_type_model.dart';
import 'package:Haandvaerker.dk/model/wizard/openai_wizard_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_new_version_model.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/booking_wizard/booking_wizard_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart' as google;
import 'package:provider/provider.dart';

import '../../model/get_types_response_model.dart';

class PriceCalculationViewmodel extends BaseViewModel {
  PriceCalculationViewmodel({required this.context});
  final BuildContext context;

  OpenaiAssistantWizardModel? _openaiAssistantWizardData;
  OpenaiAssistantWizardModel? get openaiAssistantWizardData =>
      _openaiAssistantWizardData;

  Address? _selectedAddress;
  Address? get selectedAddress => _selectedAddress;

  List<WizardAssistantMessagesModel> _wizardAssistantMessageList = [];
  List<WizardAssistantMessagesModel> get wizardAssistantMessageList =>
      _wizardAssistantMessageList;

  List<AllWizardText> _industryList = [];
  List<AllWizardText> get industryList => _industryList;

  List<AllWizardText> _filteredIndustries = [];
  List<AllWizardText> get filteredIndustries => _filteredIndustries;

  List<AllWizardText> _selectedIndustries = [];
  List<AllWizardText> get selectedIndustries => _selectedIndustries;

  List<Data> _industryFullList = [];
  List<Data> get industryFullList => _industryFullList;

  int _priceCalculatorWizardStep = 0;
  int get priceCalculatorWizardStep => _priceCalculatorWizardStep;

  int _priceCalculatorSubStepTwo = 0;
  int get priceCalculatorSubStepTwo => _priceCalculatorSubStepTwo;

  int _priceCalculatorProductsStep = 0;
  int get priceCalculatorProductsStep => _priceCalculatorProductsStep;

  int _selectedIndustryLength = 0;
  int get selectedIndustryLength => _selectedIndustryLength;

  List<AutoCompleteAddress> _searchAddressList = [];
  List<AutoCompleteAddress> get searchAddressList => _searchAddressList;

  Map<String, dynamic> _priceCalculatorForms = {
    "priceCalculatorSignatureValid": "30"
  };
  Map<String, dynamic> get priceCalculatorForms => _priceCalculatorForms;

  List<ProductWizardModelV2> _wizardQuestions = [];
  List<ProductWizardModelV2> get wizardQuestions => _wizardQuestions;

  Map<String, String> _wizardProductAnswers = {};
  Map<String, String> get wizardProductAnswers => _wizardProductAnswers;

  List<Map<String, dynamic>> _industryDescriptions = [];
  List<Map<String, dynamic>> get industryDescriptions => _industryDescriptions;

  List<ProjectTaskTypes> _taskTypes = [];
  List<ProjectTaskTypes> get taskTypes => _taskTypes;

  void reset() {
    _industryList = [];
    _filteredIndustries = [];
    _selectedIndustries = [];
    _industryFullList = [];
    _priceCalculatorWizardStep = 0;
    _priceCalculatorSubStepTwo = 0;
    _priceCalculatorProductsStep = 0;
    _selectedIndustryLength = 0;
    _searchAddressList = [];
    _priceCalculatorForms = {"priceCalculatorSignatureValid": "30"};
    _wizardQuestions = [];
    _wizardProductAnswers = {};
    _industryDescriptions = [];
    _taskTypes = [];
    _openaiAssistantWizardData = null;
    _selectedAddress = null;
    _wizardAssistantMessageList = [];

    notifyListeners();
  }

  set searchAddressList(List<AutoCompleteAddress> searchAddressList) {
    _searchAddressList = searchAddressList;
    notifyListeners();
  }

  set selectedAddress(Address? selectedAddress) {
    _selectedAddress = selectedAddress;
    notifyListeners();
  }

  Future<bool?> submitPriceCalculator({required String signature}) async {
    final service = context.read<BookingWizardService>();

    final currentAddress =
        _priceCalculatorForms['priceCalculatorAddress'] as Address;

    double totalPriceWithoutVat = _industryDescriptions
        .expand((industry) => industry['subIndustry'] as List)
        .fold(0, (sum, subIndustry) => sum + subIndustry['price_max']);

    final List<Map<String, dynamic>> saveListPricePayload = [];
    final Map<String, dynamic> descriptionForm = {};
    final List<String> addEnterpriseSubProjectNew = [];
    final List<String> jobList = [];
    final List<int> taskTypeIds = [];

    // final subIndustryAiResponse = await service.getListSubindustryAi(
    //   contactId: 0,
    // );
    // if ((subIndustryAiResponse.data is List) &&
    //     (subIndustryAiResponse.data as List).isEmpty) return false;

    for (final element in industryDescriptions
        .expand((element) => element['subIndustry'] as List)) {
      final task = taskTypes.firstWhere(
        (task) =>
            task.industryId == int.parse(element['subIndustryId'] as String),
        orElse: () => ProjectTaskTypes.defaults(),
      );

      if ((task.subcategories ?? []).isNotEmpty) {
        for (final subTask in (task.subcategories?.first.taskTypes ?? [])) {
          taskTypeIds.add(subTask.taskTypeId);
        }
      }
    }

    for (final industry in industryDescriptions) {
      addEnterpriseSubProjectNew.add(
          '${industry['productTypeId']}: ${(industry['subIndustry'] as List).map((e) => e['subIndustryId']).join('-')}');

      for (final subIndustry in industry['subIndustry'] as List) {
        final Map<String, dynamic> subIndustryMap =
            subIndustry as Map<String, dynamic>;
        descriptionForm.putIfAbsent(
            'text_${industry['productTypeId']}_${subIndustryMap['subIndustryId']}',
            () => subIndustryMap['descriptions']);

        final job =
            '${subIndustry['subIndustryId']}::${industryDescriptions.map((item) => item['subIndustry'] as List).expand((subIndustries) => subIndustries).toList().where((a) => a['subIndustryId'] as String == subIndustry['subIndustryId'] as String).toList().map((b) => b['productTypeId']).join('-')}';

        // '${subIndustry['subIndustryId']}:${((subIndustryAiResponse.data as Map<String, dynamic>)[subIndustry['subIndustryId']] as List).join('-')}:${industryDescriptions.map((item) => item['subIndustry'] as List).expand((subIndustries) => subIndustries).toList().where((a) => a['subIndustryId'] as String == subIndustry['subIndustryId'] as String).toList().map((b) => b['productTypeId']).join('-')}';

        if (!jobList.contains(job)) jobList.add(job);
      }
    }

    final Map<String, dynamic> createProjectForm = {
      "contactId": 0,
      "addressId": currentAddress.addressId,
      "name": _priceCalculatorForms['name'],
      "description": _priceCalculatorForms['description'],
      "source": "app-partner",
      "status": "new_project",
      "contractType": "Hovedentreprise",
      "industry": industryDescriptions
          .expand((element) => (element['subIndustry'] as List))
          .first['subIndustryName'],
      "industryNew": [
        ...industryDescriptions
            .expand((element) => element['subIndustry'] as List)
            .map((e) => int.parse(e['subIndustryId'] as String))
      ],
      "subcategoryNew": [
        ...industryDescriptions
            .expand((element) => element['subIndustry'] as List)
            .map((e) => int.parse(e['subIndustryId'] as String))
      ],
      "taskTypeNew": taskTypeIds,
      "enterpriseIndustry": [...selectedIndustries.map((e) => e.producttypeid)],
      "enterpriseIndustryNew": addEnterpriseSubProjectNew,
      "jobsList": jobList.map((e) => [e]).toList(),
      "budget": totalPriceWithoutVat
    };

    for (var industry in _industryDescriptions) {
      for (final subIndustry in industry['subIndustry']) {
        saveListPricePayload.add({
          "project_id": 0,
          "contact_id": 0,
          "calculation_id": 0,
          "offer_id": 0,
          "industry_id": subIndustry['productTypeId'],
          "job_industry_id": subIndustry['subIndustryId'],
          "total": subIndustry['price_max'],
          "note": _priceCalculatorForms['offerCondition'] == 'null'
              ? null
              : _priceCalculatorForms['offerCondition'],
          "weeks": _priceCalculatorForms['priceCalculatorProjectEstimate'],
          "start_date":
              _priceCalculatorForms['priceCalculatorProjectStartDate'],
          "signature": signature,
          "days": _priceCalculatorForms['priceCalculatorProjectEstimate'],
          "parent_offer_id": 0,
          "sub_project_id": 0,
          "reviews_id": _priceCalculatorForms['reviewIds'],
          "job_description": {"submitted": subIndustry['descriptions']}
        });
      }
    }

    final payload = {
      "data_for_contact": {
        "name": _priceCalculatorForms['homeOwnerName'],
        "email": _priceCalculatorForms['homeOwnerEmail'],
        "mobile": _priceCalculatorForms['homeOwnerPhone']
      },
      "data_for_createAddress": {
        "addressTitle": currentAddress.address,
        "addressId": currentAddress.addressId,
        "adgangsaddressId": currentAddress.adgangsAddressId
      },
      "data_for_createOffer": {
        "parentProjectId": 0,
        "jobIndustry": 0,
        "offers": [
          {
            "title":
                "Tilbud på projektnr. ${_priceCalculatorForms['name']} til ${_priceCalculatorForms['description']}",
            "description": _priceCalculatorForms['description'],
            "subtotalPrice": totalPriceWithoutVat,
            "vat": (totalPriceWithoutVat * 0.25),
            "totalPrice": (totalPriceWithoutVat * 1.25),
            "tasks": [
              {
                "title": '',
                "description": '',
                "unit": '',
                "value": '',
                "quantity": ''
              }
            ]
          }
        ]
      },
      "data_for_saveListPriceAi": createProjectForm,
      "data_for_saveListPriceV2": saveListPricePayload
    };

    final response = await service.submitPriceCalculator(payload: payload);

    if (!response.success!) throw ApiException(message: response.message!);

    return true;
  }

  Future<MinboligApiResponse> getAiCalculationPriceV2({
    required String addressId,
    required String adgangsAdresseId,
    required String subTasks,
    required int bookerWizardId,
    required int industry,
  }) async {
    final service = context.read<BookingWizardService>();

    final response = await service.getAiCalculationPriceV2(
        addressId: addressId,
        adgangsAdresseId: adgangsAdresseId,
        subTasks: subTasks,
        bookerWizardId: bookerWizardId,
        industry: industry);

    return response;
  }

  Future<void> searchAddress({required String query}) async {
    try {
      final request = await Dio().get(
          'https://dawa.aws.dk/autocomplete/?q=$query&startfra=adresse&fuzzy=true');

      if (request.statusCode != 200) _searchAddressList = [];

      _searchAddressList = AutoCompleteAddress.fromCollection(request.data);
    } catch (e) {
      _searchAddressList = [];
    }
    notifyListeners();
  }

  Future<void> saveAiPriceSurvey(
      {required Map<String, dynamic> payload}) async {
    final service = context.read<BookingWizardService>();

    final response = await service.saveAiPriceSurvey(payload: payload);

    if (!response.success!) throw ApiException(message: response.message!);
  }

  Future<void> getTitleAndSummary() async {
    final service = context.read<BookingWizardService>();

    final Map<String, dynamic> tempCreateOfferWizardForms = {
      ..._priceCalculatorForms
    };

    // final titleResponse = await service.getListTitleAi(
    //     industryId: _selectedIndustries.map((e) => e.producttypeid).join(','),
    //     contactId: 0);
    // final summaryResponse = await service.getListSummaryAi(
    //     industryId: _selectedIndustries.map((e) => e.producttypeid).join(','),
    //     contactId: 0);
    final responses = await Future.wait([
      service.getListTitleAi(
          industryId: _selectedIndustries.map((e) => e.producttypeid).join(','),
          contactId: 0),
      service.getListSummaryAi(
          industryId: _selectedIndustries.map((e) => e.producttypeid).join(','),
          contactId: 0),
    ]);

    if (!responses[0].success!) {
      throw ApiException(message: responses[0].message!);
    }

    if (!responses[1].success!) {
      throw ApiException(message: responses[1].message!);
    }

    tempCreateOfferWizardForms.putIfAbsent('name',
        () => (responses[0].data as Map<String, dynamic>)['title'] as String);
    tempCreateOfferWizardForms.putIfAbsent('description',
        () => (responses[1].data as Map<String, dynamic>)['summary'] as String);

    _priceCalculatorForms = {...tempCreateOfferWizardForms};
    _priceCalculatorWizardStep = _priceCalculatorWizardStep + 1;

    notifyListeners();
  }

  Future<void> submitWizardAnswers() async {
    try {
      final service = context.read<BookingWizardService>();
      final wizardId = _openaiAssistantWizardData?.wizard?.wizardId ?? '';
      final Map<String, dynamic> form = {..._wizardProductAnswers};
      final List<Map<String, dynamic>> tempIndustryDescriptions = [];
      final Map<String, dynamic> tempCreateOfferWizardForms = {
        ..._priceCalculatorForms
      };

      form.putIfAbsent('industries',
          () => _selectedIndustries.map((e) => e.producttypeid).join(','));
      form.putIfAbsent('calculation_id', () => 0);
      form.putIfAbsent('project_id', () => 0);
      form.putIfAbsent('update_text', () => 0);
      form.putIfAbsent('userId', () => 0);
      form.putIfAbsent('contactId', () => 0);

      final currentAddress =
          _priceCalculatorForms['priceCalculatorAddress'] as Address;

      final response = await service.addWizardProduct(form: form);

      if (!response.success!) throw ApiException(message: response.message!);

      for (var element in _selectedIndustries) {
        final listPriceResponse = await service.getListPriceAi(
            contactId: 0, industryId: element.producttypeid!);
        final listPriceMap = listPriceResponse.data as Map<String, dynamic>;
        final List<Map<String, dynamic>> subIndustry = [];
        int priceMax = 0;
        int priceMin = 0;

        for (MapEntry<String, dynamic> listPriceMap in listPriceMap.entries) {
          final subIndustryTemp = listPriceMap.value as Map<String, dynamic>;

          List<String> industryIdList =
              subIndustryTemp.entries.map((id) => id.key).toList();
          log('industryIdList: $industryIdList');
          List<int?> branchIdList =
              _industryFullList.map((e) => e.branchId).toList();
          log('branchIdList: $branchIdList');

          Iterable<Future<MinboligApiResponse>> futureList =
              subIndustryTemp.entries.map((subIndustryEntry) async {
            final response = getAiCalculationPriceV2(
                addressId: currentAddress.addressId!,
                adgangsAdresseId: currentAddress.adgangsAddressId!,
                subTasks: element.subindustry!,
                bookerWizardId: int.parse(element.producttypeid!),
                industry: int.parse(subIndustryEntry.key));

            final responseData = await Future.wait([
              response,
              service.getAssistantWizardMessages(wizardId: wizardId)
            ]);

            if (responseData[1] != null && responseData[1]?.data != null) {
              final wizardList = responseData[1]?.data;
              final estimatedPriceList = wizardList.map((wizardMessage) {
                final message = wizardMessage['MESSAGE'];
                if (message != null) {
                  if (message.containsKey('estimated_price')) {
                    if (message['estimated_price'] != null) {
                      return message['estimated_price'];
                    } else {
                      return 0;
                    }
                  } else {
                    return 0;
                  }
                } else {
                  return 0;
                }
              }).toList();

              log('estimatedPriceList: $estimatedPriceList');

              priceMax = estimatedPriceList.fold(estimatedPriceList.first,
                  (current, next) => current > next ? current : next);
              priceMin = estimatedPriceList.fold(estimatedPriceList.first,
                  (current, next) => current < next ? current : next);
            }

            subIndustry.add({
              "price": priceMax,
              "price_min": priceMin < 0 ? 0 : priceMin,
              "price_max": priceMax,
              "productTypeId": element.producttypeid,
              "subIndustryName": _industryFullList
                  .firstWhere(
                    (element) =>
                        element.branchId.toString() == subIndustryEntry.key,
                    orElse: () => Data(branche: 'N/A'),
                  )
                  .branche,
              "subIndustryId": subIndustryEntry.key,
              "descriptions": subIndustryEntry.value as List,
              "hide_description": false
            });

            return response;
          });

          await Future.wait(futureList);

          // for (MapEntry<String, dynamic> subIndustryMap
          //     in subIndustryTemp.entries) {
          //   final calculatorResponse = await service.getAiCalculationPriceV2(
          //       addressId: currentAddress.addressId!,
          //       adgangsAdresseId: currentAddress.adgangsAddressId!,
          //       subTasks: element.subindustry!,
          //       bookerWizardId: int.parse(element.producttypeid!),
          //       industry: int.parse(subIndustryMap.key));

          //   subIndustry.add({
          //     "productTypeId": element.producttypeid,
          //     "subIndustryName": _industryFullList
          //         .firstWhere(
          //           (element) =>
          //               element.branchId.toString() == subIndustryMap.key,
          //           orElse: () => Data(branche: 'N/A'),
          //         )
          //         .branche,
          //     "subIndustryId": subIndustryMap.key,
          //     "descriptions": subIndustryMap.value as List,
          //     "price": calculatorResponse.data['price_max'],
          //     "price_min": calculatorResponse.data['price_min'] < 0
          //         ? 0
          //         : calculatorResponse.data['price_min'],
          //     "price_max": calculatorResponse.data['price_max'],
          //     "hide_description": false
          //   });
          // }
        }

        tempIndustryDescriptions.add({
          "productName": element.producttypeDa,
          "productTypeId": element.producttypeid,
          "subIndustry": subIndustry
        });
      }

      _priceCalculatorSubStepTwo = 2;
      _priceCalculatorForms = {...tempCreateOfferWizardForms};
      _industryDescriptions = [...tempIndustryDescriptions];

      log('industryDescriptions: $_industryDescriptions');

      notifyListeners();
    } catch (e) {
      log("message said $e");
    }
  }

  Future<void> submitWizardAnswersV2() async {
    final service = context.read<BookingWizardService>();
    // final wizardId = _openaiAssistantWizardData?.wizard?.wizardId ?? '';
    final Map<String, dynamic> form = {..._wizardProductAnswers};
    final List<Map<String, dynamic>> tempIndustryDescriptions = [];
    final Map<String, dynamic> tempCreateOfferWizardForms = {
      ..._priceCalculatorForms
    };

    form.putIfAbsent('industries',
        () => _selectedIndustries.map((e) => e.producttypeid).join(','));
    form.putIfAbsent('calculation_id', () => 0);
    form.putIfAbsent('project_id', () => 0);
    form.putIfAbsent('update_text', () => 0);
    form.putIfAbsent('userId', () => 0);
    form.putIfAbsent('contactId', () => 0);

    final response = await service.addWizardProduct(form: form);
    if (!response.success!) throw ApiException(message: response.message!);

    for (var element in _selectedIndustries) {
      final listPriceResponse = await service.getListPriceAi(
          contactId: 0, industryId: element.producttypeid!);
      final listPriceMap = listPriceResponse.data as Map<String, dynamic>;
      final List<Map<String, dynamic>> subIndustry = [];
      int priceMax = 0;
      int priceMin = 0;

      for (MapEntry<String, dynamic> listPriceMap in listPriceMap.entries) {
        final subIndustryTemp = listPriceMap.value as Map<String, dynamic>;

        List<String> industryIdList =
            subIndustryTemp.entries.map((id) => id.key).toList();
        log('industryIdList: $industryIdList');
        List<int?> branchIdList =
            _industryFullList.map((e) => e.branchId).toList();
        log('branchIdList: $branchIdList');

        Iterable<Future<MinboligApiResponse>> futureList =
            subIndustryTemp.entries.map((subIndustryEntry) async {
          Map<String, dynamic> priceEstimatePayload = {
            "createdFrom": 'partner-app',
            "industries": element.producttypeid,
            "dawaAddressId": selectedAddress!.addressId,
            "dawaAdgangsAddressId": selectedAddress!.addressId,
          };

          Map<String, dynamic> selectedIndustryAnswers = {};

          for (var answers in form.entries) {
            if (answers.key.split('_').contains(element.producttypeid)) {
              selectedIndustryAnswers.addAll({answers.key: answers.value});
            }
          }

          priceEstimatePayload.addAll({...selectedIndustryAnswers});

          final priceEstimateResponse =
              await service.priceEstimateAkkio(payload: priceEstimatePayload);

          if (priceEstimateResponse?.data != null &&
              priceEstimateResponse?.data is List &&
              (priceEstimateResponse?.data as List).isNotEmpty) {
            priceMax = ((priceEstimateResponse?.data as List).first
                as Map<String, dynamic>)['Price'];
            priceMin = ((priceEstimateResponse?.data as List).first
                as Map<String, dynamic>)['Price'];
          }

          // final responseData = await Future.value(
          //     service.getAssistantWizardMessages(wizardId: wizardId));

          // if (responseData != null && responseData.data != null) {
          //   final wizardList = responseData.data;
          //   final estimatedPriceList = wizardList.map((wizardMessage) {
          //     final message = wizardMessage['MESSAGE'];
          //     if (message != null) {
          //       if (message.containsKey('estimated_price')) {
          //         if (message['estimated_price'] != null) {
          //           return message['estimated_price'];
          //         } else {
          //           return 0;
          //         }
          //       } else {
          //         return 0;
          //       }
          //     } else {
          //       return 0;
          //     }
          //   }).toList();

          //   log('estimatedPriceList: $estimatedPriceList');

          //   priceMax = estimatedPriceList.fold(estimatedPriceList.first,
          //       (current, next) => current > next ? current : next);
          //   priceMin = estimatedPriceList.fold(estimatedPriceList.first,
          //       (current, next) => current < next ? current : next);
          // }

          subIndustry.add({
            "price": priceMax,
            "price_min": priceMin < 0 ? 0 : priceMin,
            "price_max": priceMax,
            "productTypeId": element.producttypeid,
            "subIndustryName": _industryFullList
                .firstWhere(
                  (element) =>
                      element.branchId.toString() == subIndustryEntry.key,
                  orElse: () => Data(branche: 'N/A'),
                )
                .branche,
            "subIndustryId": subIndustryEntry.key,
            "descriptions": subIndustryEntry.value as List,
            "hide_description": false
          });

          return priceEstimateResponse ?? MinboligApiResponse();
        });

        await Future.wait(futureList);
      }

      tempIndustryDescriptions.add({
        "productName": element.producttypeDa,
        "productTypeId": element.producttypeid,
        "subIndustry": subIndustry
      });
    }

    _priceCalculatorSubStepTwo = 2;
    _priceCalculatorForms = {...tempCreateOfferWizardForms};
    _industryDescriptions = [...tempIndustryDescriptions];

    notifyListeners();
  }

  Future<void> submitWizardAnswersV3() async {
    final service = Provider.of<BookingWizardService>(context, listen: false);
    final Map<String, dynamic> form = {..._wizardProductAnswers};
    final List<Map<String, dynamic>> tempIndustryDescriptions = [];
    final Map<String, dynamic> tempCreateOfferWizardForms = {
      ..._priceCalculatorForms
    };

    form.putIfAbsent('industries',
        () => _selectedIndustries.map((e) => e.producttypeid).join(','));
    form.putIfAbsent('calculation_id', () => 0);
    form.putIfAbsent('project_id', () => 0);
    form.putIfAbsent('update_text', () => 0);
    form.putIfAbsent('userId', () => 0);
    form.putIfAbsent('contactId', () => 0);

    final currentAddress =
        _priceCalculatorForms['priceCalculatorAddress'] as Address;

    final response = await service.addWizardProduct(form: form);

    if (!response.success!) throw ApiException(message: response.message!);

    for (var element in _selectedIndustries) {
      final listPriceResponse = await service.getListPriceAi(
          contactId: 0, industryId: element.producttypeid!);
      final listPriceMap = listPriceResponse.data as Map<String, dynamic>;
      final List<Map<String, dynamic>> subIndustry = [];
      int priceMax = 800;
      int priceMin = 800;

      for (MapEntry<String, dynamic> listPriceMap in listPriceMap.entries) {
        final subIndustryTemp = listPriceMap.value as Map<String, dynamic>;

        List<String> industryIdList =
            subIndustryTemp.entries.map((id) => id.key).toList();
        log('industryIdList: $industryIdList');
        List<int?> branchIdList =
            _industryFullList.map((e) => e.branchId).toList();
        log('branchIdList: $branchIdList');

        Iterable<Future<MinboligApiResponse?>> futureList =
            subIndustryTemp.entries.map((subIndustryEntry) async {
          final selectedIndustry = _industryFullList.firstWhere(
              (value) =>
                  (value.branchId ?? 0).toString() == subIndustryEntry.key,
              orElse: () => Data.defaults());

          String subTask = '';

          subTask = (selectedIndustry.subIndustries ?? [])
              .map((e) => e.subcategoryId ?? 0)
              .toList()
              .join(',');

          final response = service.getAiCalculationPriceV3(
              addressId: currentAddress.addressId!,
              adgangsAddressId: currentAddress.adgangsAddressId!,
              subTasks: subTask,
              contactId: 0,
              industry: element.producttypeid!,
              productType: subIndustryEntry.key);

          final responseData = await Future.value(response);

          if (responseData != null) {
            final aiPriceData = responseData.data;
            final aiPrice = aiPriceData['PRICE'];

            if (aiPrice >= 800) {
              priceMax = aiPrice;
            } else {
              priceMax = 800;
            }
          }

          subIndustry.add({
            "price": priceMax,
            "price_min": priceMin < 0 ? 0 : priceMin,
            "price_max": priceMax,
            "productTypeId": element.producttypeid,
            "subIndustryName": _industryFullList
                .firstWhere(
                  (element) =>
                      element.branchId.toString() == subIndustryEntry.key,
                  orElse: () => Data(branche: 'N/A'),
                )
                .branche,
            "subIndustryId": subIndustryEntry.key,
            "descriptions": subIndustryEntry.value as List,
            "hide_description": false
          });

          return response;
        });

        await Future.wait(futureList);
      }

      tempIndustryDescriptions.add({
        "productName": element.producttypeDa,
        "productTypeId": element.producttypeid,
        "subIndustry": subIndustry
      });
    }

    _priceCalculatorSubStepTwo = 2;
    _priceCalculatorForms = {...tempCreateOfferWizardForms};
    _industryDescriptions = [...tempIndustryDescriptions];

    log('industryDescriptions: $_industryDescriptions');

    notifyListeners();
  }

  Future<void> getWizardProductQuestions() async {
    final List<ProductWizardModelV2> tempProductWizardList =
        []; // FIELDS THAT WILL SHOW THE QUESTIONAIRE

    final service = context.read<BookingWizardService>();

    final response = await service.getWizardProductsNewVersion(
        labels: selectedIndustries.map((e) => e.producttypeid).join(','));

    if (!response.success!) throw response.message!;

    final Map<String, dynamic> productsResponse =
        response.data as Map<String, dynamic>;

    productsResponse.forEach((key, value) {
      List<ProductWizardFieldV2> productValues = <ProductWizardFieldV2>[];
      final productItems = (value as Map<String, dynamic>)['product'];
      productValues = ProductWizardFieldV2.fromCollection(productItems);

      if (value['wizard_product'] is List) {
        final List tempWizardProduct = value['wizard_product'] as List;
        List newWizardProductList = [];

        if (value['wizard_lead'] is List) {
          newWizardProductList = [
            ...(value['wizard_lead'] as List).map((index) {
              return tempWizardProduct[index];
            })
          ];
        } else if (value['wizard_lead'] is Map) {
          newWizardProductList = [
            ...(value['wizard_lead'] as Map).entries.map((index) {
              return tempWizardProduct[int.parse(index.value as String)];
            })
          ];
        }

        for (final p0 in newWizardProductList) {
          final List<ProductWizardFieldV2> productFields = [];

          if (p0 is List) {
            final wizardsList = ProductWizardFieldV2.fromCollection(p0);

            final List<ProductWizardFieldV2> loopingList = [];

            loopingList.addAll([
              ...wizardsList.where((element) => element.parentproductid == 0)
            ]);

            while (loopingList.isNotEmpty) {
              final currentField = loopingList.first;

              if (currentField.productField == 2) {
                productFields.add(currentField);
                loopingList.remove(currentField);
              } else {
                final values = productValues.where((a) =>
                    a.parentproductid.toString() == currentField.id &&
                    a.productField == 1);

                final List<ProductWizardFieldV2> children = [];

                for (final fieldValue in values) {
                  final subChildren = wizardsList.where(
                      (a) => a.parentproductid.toString() == fieldValue.id);

                  for (final sub in subChildren) {
                    if (sub.productField == 2) {
                      children.add(sub);
                    } else {
                      ProductWizardFieldV2 tempSub = sub;

                      final subValues = productValues.where((a) =>
                          a.parentproductid.toString() == sub.id &&
                          a.productField == 1);

                      tempSub.values = [...subValues];

                      children.add(tempSub);
                    }
                  }
                }

                ProductWizardFieldV2 tempCurrentField = currentField;

                tempCurrentField.values = [...values];
                tempCurrentField.productField =
                    values.isEmpty ? 1 : currentField.productField;
                tempCurrentField.children = [...children];

                productFields.add(tempCurrentField);
                loopingList.remove(currentField);
              }
              loopingList.addAll([
                ...productValues.where((element) =>
                    element.parentproductid.toString() == currentField.id &&
                    element.productField != 1)
              ]);
            }
          }
          productFields.sort((a, b) => (a.id ?? '').compareTo((b.id ?? '')));

          tempProductWizardList.add(ProductWizardModelV2(
              productId: key,
              producttype: industryList
                  .firstWhere(
                    (element) => element.producttypeid == key,
                    orElse: () => AllWizardText.defaults(),
                  )
                  .producttypeDa,
              productFields: [
                ...productFields
                    .where((element) => element.parentproductid == 0),
                ...productFields
                    .where((element) => element.parentproductid != 0)
              ]));
        }
      } else if (value['wizard_product'] is Map<String, dynamic>) {
        final Map<String, dynamic> tempWizardProduct =
            value['wizard_product'] as Map<String, dynamic>;

        Map<String, dynamic> finalWizardProduct = {};

        List<String> wizardLeadsIndex = [];

        if (value['wizard_lead'] is List) {
          wizardLeadsIndex = [
            ...(value['wizard_lead'] as List).map((e) => '$e')
          ];
        } else if (value['wizard_lead'] is Map) {
          wizardLeadsIndex = [
            ...(value['wizard_lead'] as Map).entries.map((e) => '${e.value}')
          ];
        }

        finalWizardProduct = {
          for (String index in wizardLeadsIndex) index: tempWizardProduct[index]
        };

        final List<String> sortedKeys = finalWizardProduct.keys.toList()
          ..sort();

        final Map<String, dynamic> sortedWizardProduct = Map.fromEntries(
            sortedKeys.map((key) => MapEntry(key, finalWizardProduct[key])));

        sortedWizardProduct.forEach((key0, value0) {
          final List<ProductWizardFieldV2> productFields = [];

          final wizardsList = ProductWizardFieldV2.fromCollection(value0);

          final List<ProductWizardFieldV2> loopingList = [];

          loopingList.addAll([
            ...wizardsList.where((element) => element.parentproductid == 0)
          ]);

          while (loopingList.isNotEmpty) {
            final currentField = loopingList.first;

            if (currentField.productField == 2) {
              productFields.add(currentField);
              loopingList.remove(currentField);
            } else {
              final values = productValues.where((a) =>
                  a.parentproductid.toString() == currentField.id &&
                  a.productField == 1);

              final List<ProductWizardFieldV2> children = [];

              for (final fieldValue in values) {
                final subChildren = wizardsList.where(
                    (a) => a.parentproductid.toString() == fieldValue.id);

                for (final sub in subChildren) {
                  if (sub.productField == 2) {
                    children.add(sub);
                  } else {
                    final subValues = productValues.where((a) =>
                        a.parentproductid.toString() == sub.id &&
                        a.productField == 1);

                    ProductWizardFieldV2 tempSub = sub;

                    tempSub.values = [...subValues];

                    children.add(tempSub);
                  }
                }
              }

              ProductWizardFieldV2 tempCurrentField = currentField;
              tempCurrentField.values = [...values];
              tempCurrentField.productField =
                  values.isEmpty ? 1 : currentField.productField;
              tempCurrentField.children = [...children];

              productFields.add(tempCurrentField);
              loopingList.remove(currentField);
            }
            loopingList.addAll([
              ...productValues.where((element) =>
                  element.parentproductid.toString() == currentField.id &&
                  element.productField != 1)
            ]);
          }

          productFields.sort((a, b) => (a.id ?? '').compareTo((b.id ?? '')));

          tempProductWizardList.add(
            ProductWizardModelV2(
              productId: key,
              producttype: industryList
                  .firstWhere(
                    (element) => element.producttypeid == key,
                    orElse: () => AllWizardText.defaults(),
                  )
                  .producttypeDa,
              productFields: [
                ...productFields
                    .where((element) => element.parentproductid == 0),
                ...productFields
                    .where((element) => element.parentproductid != 0)
              ],
            ),
          );
        });
      }
    });

    _priceCalculatorSubStepTwo = 1;
    _wizardQuestions = tempProductWizardList
        .where((element) => element.productFields!.isNotEmpty)
        .toList();
    _wizardProductAnswers = {};
    _priceCalculatorProductsStep = 0;

    notifyListeners();
  }

  Future<void> initPriceCalculator(
      {required google.LatLng initialPosition}) async {
    resetPriceCalculationStates();

    try {
      final tempPlaces = await placemarkFromCoordinates(
          initialPosition.latitude, initialPosition.longitude);
      if (tempPlaces.isNotEmpty) {
        final temp = tempPlaces.first.street ?? '';

        try {
          final request = await Dio().get(
              'https://dawa.aws.dk/autocomplete/?q=$temp&startfra=adresse&fuzzy=true');

          if (request.statusCode != 200) _searchAddressList = [];

          _searchAddressList = AutoCompleteAddress.fromCollection(request.data);
        } catch (e) {
          _searchAddressList = [];
        }
      }
    } catch (e) {
      _searchAddressList = [];
    }

    notifyListeners();
  }

  Future<void> createAssistantWizard({required String addressId}) async {
    final service = context.read<BookingWizardService>();

    final payload = {
      // "withInstructions": true,
      "createdFrom": 'partner-app',
      "dawaAddressId": addressId,
      "dawaAdgangsAddressId": addressId,
    };

    final response = await service.createAssistantWizard(payload: payload);

    if (response != null && response.data != null) {
      _openaiAssistantWizardData =
          OpenaiAssistantWizardModel.fromJson(response.data);
    }
  }

  Future<void> assistantWizardMessage() async {
    // final service = context.read<BookingWizardService>();
    // final Map<String, dynamic> payload = {..._wizardProductAnswers};

    // payload['wizard_id'] = _openaiAssistantWizardData?.wizard?.wizardId ?? '';
    // payload['thread_id'] = _openaiAssistantWizardData?.wizard?.threadId ?? '';
    // payload['step'] = 1;

    // for (var element in selectedIndustries) {
    //   final industryAnswers = {};
    //   for (var ans in payload.entries) {
    //     if (ans.key.split('_').contains(element.producttypeid)) {
    //       industryAnswers.addAll({ans.key: ans.value});
    //     }
    //   }

    //   Map<String, dynamic> priceEstimatePayload = {
    //     "createdFrom": 'partner-app',
    //     "industries": element.producttypeid,
    //     "dawaAddressId": selectedAddress!.addressId,
    //     "dawaAdgangsAddressId": selectedAddress!.addressId,
    //   };
    //   priceEstimatePayload.addAll({...industryAnswers});

    //   await service.priceEstimateAkkio(payload: priceEstimatePayload);
    // }

    // await service.assistantWizardMessage(payload: payload);
    _priceCalculatorSubStepTwo = 2;
    notifyListeners();
  }

  List<ProductWizardField> extractProducts(
      {required List<ProductWizardField> product,
      required List<ProductWizardField> productValues}) {
    final List<ProductWizardField> fields = [];

    for (final element in product) {
      if (element.productField == '2') {
        fields.add(element);
      } else {
        List<ProductWizardField> values = [];
        final List<ProductWizardField> children = [];

        final ProductWizardField tempElement = element;

        values = [
          ...productValues
              .where((element) => element.parentproductid == tempElement.id)
        ];

        if (values.isNotEmpty) {
          for (final c in values) {
            if (productValues
                .where((element) => element.parentproductid == c.id)
                .isNotEmpty) {
              final childProduct = productValues
                  .where((element) => element.parentproductid == c.id)
                  .first;

              if (childProduct.productField == '2') {
                children.add(childProduct);
              } else {
                childProduct.values = [
                  ...productValues.where(
                      (element) => element.parentproductid == childProduct.id)
                ];

                children.add(childProduct);
              }
            }
            addChildren(productValues, c, children);
          }
        }

        tempElement.values = values;
        tempElement.children = children;

        fields.add(tempElement);
      }
    }

    return [...fields];
  }

  String formatDate({required String date}) {
    if (date.isNotEmpty) {
      final parsedDate = DateTime.parse(date);
      final year = parsedDate.year;
      final day = parsedDate.day;
      String month = parsedDate.month.toString();

      if (month.isNotEmpty && month.length == 1) {
        month = '0$month';
      }

      return '$day.$month.$year';
    }

    return '';
  }

  void updateIndustryAllDescriptions(
      {required int industryIndex,
      required int subIndustryIndex,
      required List<String> descriptions}) {
    final tempIndustryDescriptions = [..._industryDescriptions];

    tempIndustryDescriptions[industryIndex]['subIndustry'][subIndustryIndex]
        ['descriptions'] = descriptions;

    _industryDescriptions = [...tempIndustryDescriptions];

    notifyListeners();
  }

  void hideOrUnhideDescriptions(
      {required int industryIndex,
      required int subIndustryIndex,
      required bool hideDescription}) {
    final tempIndustryDescriptions = [..._industryDescriptions];

    tempIndustryDescriptions[industryIndex]['subIndustry'][subIndustryIndex]
        ['hide_description'] = hideDescription;

    _industryDescriptions = [...tempIndustryDescriptions];

    notifyListeners();
  }

  void updateIndustryPrice({
    required int industryIndex,
    required int subIndustryIndex,
    required double price,
  }) {
    final tempIndustryDescriptions = [..._industryDescriptions];

    tempIndustryDescriptions[industryIndex]['subIndustry'][subIndustryIndex]
        ['price_max'] = price;

    _industryDescriptions = [...tempIndustryDescriptions];

    notifyListeners();
  }

  void updateWizardProductAnswers(
      {required String key,
      required String value,
      List<String>? options,
      List<String>? selectedChecks}) {
    final tempProductControllers = {..._wizardProductAnswers};

    if (options != null && selectedChecks != null) {
      final selectedSet = Set<String>.from(selectedChecks);

      tempProductControllers.removeWhere((key, value) =>
          options.contains(value) && !selectedSet.contains(value));

      for (var element in selectedChecks) {
        if (options.contains(element)) {
          tempProductControllers['$key$element'] = element;
        }
      }
    } else {
      tempProductControllers[key] = value;
    }

    _wizardProductAnswers = {...tempProductControllers};

    notifyListeners();
  }

  void updateProductStep({required int productStep}) {
    _priceCalculatorProductsStep = productStep;

    notifyListeners();
  }

  void addChildren(List<ProductWizardField> productValues, ProductWizardField c,
      List<ProductWizardField> children) {
    if (c.productField == '3' || c.productField == '4') {
      final val = productValues
          .where((element) => element.parentproductid == c.id)
          .toList();

      final ProductWizardField tempC = c;

      tempC.values = val;

      children.add(tempC);

      for (final d in val) {
        addChildren(productValues, d, children);
      }
    } else {
      children.add(c);
    }
  }

  void updateSubStep({required int subStep}) {
    _priceCalculatorSubStepTwo = subStep;

    notifyListeners();
  }

  void updateStep({required int step}) {
    _priceCalculatorWizardStep = step;

    notifyListeners();
  }

  void updatePriceCalculatorForms(
      {required String key, required dynamic value}) {
    final tempAnswers = {..._priceCalculatorForms};

    if (tempAnswers.containsKey(key)) {
      tempAnswers.update(key, (old) => value);
    } else {
      tempAnswers.putIfAbsent(key, () => value);
    }

    _priceCalculatorForms = {...tempAnswers};

    notifyListeners();
  }

  void searchIndustry({required String query}) {
    final tempList = [..._filteredIndustries];

    if (query.isNotEmpty) {
      _filteredIndustries = tempList.where((item) {
        final itemName = item.producttypeDa!.toLowerCase();
        return itemName.contains(query.toLowerCase());
      }).toList();

      notifyListeners();
    } else {
      _filteredIndustries = [..._industryList];

      notifyListeners();
    }
  }

  void updateSelectedIndustryList({required AllWizardText industry}) {
    final tempList = [..._selectedIndustries];

    if (tempList.contains(industry)) {
      tempList.remove(industry);
    } else {
      tempList.add(industry);
    }

    _selectedIndustries = [...tempList];

    notifyListeners();
  }

  void updateSelectedIndustryLength({required int length}) {
    _selectedIndustryLength = length;
  }

  void resetPriceCalculationStates() {
    _selectedIndustries = [];
    _priceCalculatorWizardStep = 0;
    _priceCalculatorSubStepTwo = 0;
    _priceCalculatorProductsStep = 0;
    _priceCalculatorForms = {"priceCalculatorSignatureValid": "30"};
    _wizardQuestions = [];
    _wizardProductAnswers = {};
    _industryDescriptions = [];
    _searchAddressList = [];
    notifyListeners();
  }

  void setIndustry(
      {required List<AllWizardText> list1,
      required List<Data> list2,
      required List<ProjectTaskTypes> list3}) {
    _filteredIndustries = [...list1];
    _industryList = [...list1];
    _industryFullList = [...list2];
    _taskTypes = [...list3];

    notifyListeners();
  }
}
