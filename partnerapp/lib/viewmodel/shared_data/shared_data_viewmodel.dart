import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/get_types_response_model.dart';
import 'package:Haandvaerker.dk/model/task_type/task_type_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/services/booking_wizard/booking_wizard_service.dart';
import 'package:Haandvaerker.dk/services/contract/contract_service.dart';
import 'package:Haandvaerker.dk/services/settings/settings_service.dart';
import 'package:Haandvaerker.dk/services/wizard/wizard_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SharedDataViewmodel extends BaseViewModel {
  SharedDataViewmodel({required this.context})
      : _wizardService = context.read<WizardService>(),
        _settingService = context.read<SettingsService>(),
        _contractService = context.read<ContractService>(),
        _bookingWizardService = context.read<BookingWizardService>(),
        super();

  final BuildContext context;

  final WizardService _wizardService;
  final SettingsService _settingService;
  final ContractService _contractService;
  final BookingWizardService _bookingWizardService;

  String _termsLabel = '';
  String get termsLabel => _termsLabel;

  String _termsHtml = '';
  String get termsHtml => _termsHtml;

  List<AllWizardText> _industryList = [];
  List<AllWizardText> get industryList => _industryList;

  List<Data> _industryFullList = [];
  List<Data> get industryFullList => _industryFullList;

  List<FullIndustry> _fullIndustries = [];
  List<FullIndustry> get fullIndustries => _fullIndustries;

  List<ProjectTaskTypes> _taskTypes = [];
  List<ProjectTaskTypes> get taskTypes => _taskTypes;

  Map<String, String> _headers = {};
  Map<String, String> get headers => _headers;

  Future<void> init() async {
    final responses = await Future.wait([
      _contractService.getTermsAndConditions(),
      _wizardService.getAllWizardTexts(),
      _settingService.getTaskTypesFromCRMnoId(),
      _wizardService.getFullIndustry(),
      _bookingWizardService.getProjectTaskTypes(),
    ]);

    // service.getPartnerWizardTexts()

    final partnerTerms =
        (responses[0] as MinboligApiResponse).data as Map<String, dynamic>;

    _termsLabel = partnerTerms['LABEL'] as String;
    _termsHtml = partnerTerms['HTML'] as String;

    _industryList = (responses[1] as List<AllWizardText>?) ?? [];

    if (responses[2] != null) {
      final industryResponse = responses[2] as MinboligApiResponse;
      _industryFullList = Data.fromCollection(industryResponse.data);
    }

    if (responses[3] != null) {
      _fullIndustries = responses[3] as List<FullIndustry>;
    }

    if (responses[4] != null) {
      final taskTypeResponse = responses[4] as MinboligApiResponse;
      _taskTypes = ProjectTaskTypes.fromCollection(taskTypeResponse.data);
    }

    _headers = {
      'Authorization':
          (await _contractService.returnHeaders())['Authorization'] ?? '',
      'X-localization':
          (await _contractService.returnHeaders())['X-localization'] ?? '',
    };

    notifyListeners();
  }

  void reset() {
    _termsLabel = '';
    _termsHtml = '';

    notifyListeners();
  }
}
