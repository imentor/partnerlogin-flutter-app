// ignore_for_file: constant_identifier_names

import 'package:Haandvaerker.dk/model/admin_model.dart';
import 'package:Haandvaerker.dk/model/partner/bitrix_company_model.dart';
import 'package:Haandvaerker.dk/model/partner/supplier_info_model.dart';
import 'package:Haandvaerker.dk/model/projects/contacts_with_contracts_model.dart';
import 'package:Haandvaerker.dk/services/main/main_service.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

enum MainModules {
  recommendation,
  your_projects,
  mester_to_mester,
  settings,
  your_customer,
  webshop,
  market_place
}

class AppDrawerViewModel extends BaseViewModel {
  AppDrawerViewModel({this.context});

  final BuildContext? context;

  Map<String, MainModules> mainModule = {
    Routes.readAndAnswerRecommendations: MainModules.recommendation,
    Routes.autoPilotRecommendations: MainModules.recommendation,
    Routes.getRecommendations: MainModules.recommendation,
    Routes.widgetRecommendations: MainModules.recommendation,
    Routes.m2mTaskScreen: MainModules.mester_to_mester,
    Routes.m2mLaborScreen: MainModules.mester_to_mester,
    Routes.myProjects: MainModules.your_projects,
    Routes.createProject: MainModules.your_projects,
    Routes.settings: MainModules.settings,
    Routes.subscription: MainModules.settings,
    Routes.yourMasterInformation: MainModules.settings,
    Routes.yourProfilePage: MainModules.settings,
    Routes.jobTypes: MainModules.settings,
    Routes.holidays: MainModules.settings,
    Routes.creditCards: MainModules.settings,
    Routes.changePassword: MainModules.settings,
    Routes.invoice: MainModules.settings,
    Routes.admin: MainModules.settings,
    Routes.phoneNumber: MainModules.settings,
    Routes.integration: MainModules.settings,
    Routes.webshopAdsHome: MainModules.webshop,
    Routes.webShopHome: MainModules.webshop,
    Routes.webShopProducts: MainModules.webshop,
    Routes.marketPlaceScreen: MainModules.market_place
    //Routes.yourClient: MainModules.your_customer,
    //Routes.partners: MainModules.your_customer,
  };

  List<Permission> permissions = <Permission>[];

  Map<MainModules?, bool> expansionSettings = {
    MainModules.recommendation: false,
    MainModules.mester_to_mester: false,
    MainModules.settings: false,
    MainModules.your_projects: false,
    MainModules.your_customer: false,
    MainModules.webshop: false,
    MainModules.market_place: false,
  };

  String? _currentPage = APPLIC.currentDrawerRoute;
  String? get currentPage => _currentPage;

  final List<ContactWithContractModel> _approvedContracts = [];
  List<ContactWithContractModel> get approvedContracts => _approvedContracts;

  SupplierInfoModel? _supplierProfileModel;
  SupplierInfoModel? get supplierProfileModel => _supplierProfileModel;

  BitrixCompanyModel? _bitrixCompanyModel;
  BitrixCompanyModel? get bitrixCompanyModel => _bitrixCompanyModel;

  bool _isDrawerOpen = false;
  bool get isDrawerOpen => _isDrawerOpen;

  set isDrawerOpen(bool value) {
    _isDrawerOpen = value;

    notifyListeners();
  }

  set currentPage(String? value) {
    _currentPage = value;
    expansionSettings[mainModule[value]] = true;
    expansionSettings.forEach((key, desc) {
      if (key != mainModule[value]) {
        expansionSettings[key] = false;
      }
    });
    notifyListeners();
  }

  // Future<void> getApprovedContracts() async {
  //   var service = Provider.of<MainService>(context, listen: false);
  //   var response = await service.getApprovedContracts();
  //   _approvedContracts = response;

  //   notifyListeners();
  // }

  Future<void> getPrisenResult() async {
    var service = context!.read<MainService>();
    var response = await service.getSupplierInfo();

    if (response != null) {
      if (response.success!) {
        _supplierProfileModel = SupplierInfoModel.fromJson(
            Map<String, dynamic>.from(response.data));

        _bitrixCompanyModel = supplierProfileModel!.bCrmCompany!;
      }
    }

    notifyListeners();
  }

  Future<void> getPrisenResultV2() async {
    final service = context!.read<MainService>();
    final response =
        await service.getSupplierInfoV2(payload: {'data': 'company'});

    if (response != null) {
      if (response.success!) {
        _bitrixCompanyModel = BitrixCompanyModel.fromJson(
            Map<String, dynamic>.from(response.data['company']));
      }
    }

    notifyListeners();
  }

  // bool hasApprovedContract() {
  //   return _approvedContracts.isNotEmpty;
  // }

  bool isCurrentPage(String value) {
    return currentPage == value;
  }

  void update() {
    notifyListeners();
  }
}
