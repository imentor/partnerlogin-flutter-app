import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';

class ConnectivityViewModel extends BaseViewModel {
  ConnectivityViewModel({required this.context});

  final BuildContext context;

  String _connectionStatus = 'Unknown';
  String get connectionStatus => _connectionStatus;

  set connectionStatus(String value) {
    _connectionStatus = value;
    notifyListeners();
  }
}
