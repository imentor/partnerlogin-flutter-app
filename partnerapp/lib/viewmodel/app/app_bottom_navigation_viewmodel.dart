// ignore_for_file: constant_identifier_names

import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';

enum BottomNav { app, klima, klima_docs }

class AppBottomNavigationViewModel extends BaseViewModel {
  AppBottomNavigationViewModel({this.context});

  final BuildContext? context;

  BottomNav _currentBottomNav = BottomNav.app;
  BottomNav get currentBottomNav => _currentBottomNav;

  switchBottomNav(BottomNav value) {
    _currentBottomNav = value;
    update();
  }

  void update() {
    notifyListeners();
  }
}
