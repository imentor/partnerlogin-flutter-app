import 'dart:io';

import 'package:Haandvaerker.dk/model/settings/education_proof_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/services/education/education_service.dart';
import 'package:Haandvaerker.dk/services/wizard/wizard_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EducationViewModel extends BaseViewModel {
  EducationViewModel({required this.context});

  final BuildContext context;

  List<EducationProofModel> _currentEducationProofs = [];
  List<EducationProofModel> get currentEducationProofs =>
      _currentEducationProofs;

  List<FullIndustry> _industries = [];
  List<FullIndustry> get industries => _industries;

  final List<String> _userExtensions = [
    "jpg",
    "png",
    "jpeg",
    "pdf",
  ];
  List<String> get userExtensions => _userExtensions;

  void reset() {
    _currentEducationProofs = [];
    _industries = [];

    notifyListeners();
  }

  set currentEducationProofs(List<EducationProofModel> certs) {
    _currentEducationProofs = certs;
    notifyListeners();
  }

  set industries(List<FullIndustry> industries) {
    _industries = industries;
    notifyListeners();
  }

  //METHODS

  Future<void> getCertificates() async {
    final service = context.read<EducationService>();

    setBusy(true);
    final response = await service.getCertificates();
    if (response != null) {
      currentEducationProofs = EducationProofModel.fromCollection(response.data)
          .where((element) => element.stage == 'active')
          .toList();
    }

    setBusy(false);
  }

  Future<bool> addCertificate({
    required int industryId,
    required File file,
    required String name,
    required String branch,
  }) async {
    final service = context.read<EducationService>();

    return await service.addCertificate(
      name: name,
      industryId: industryId,
      file: file,
      branch: branch,
    );
  }

  Future<bool> deleteCertificate({required int certificateId}) async {
    final service = context.read<EducationService>();

    return await service.deleteCertificate(certificateId: certificateId);
  }

  Future<void> getIndustries() async {
    final service = context.read<WizardService>();
    final response = await service.getFullIndustry();

    if (response != null) {
      industries = response;
    }
  }
}
