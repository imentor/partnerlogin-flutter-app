// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:Haandvaerker.dk/model/packages/package_response_model.dart';
import 'package:Haandvaerker.dk/services/packages/packages_service.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FeatureStatus {
  bool status;
  PackageFeature feature;

  FeatureStatus({required this.status, required this.feature});
}

class PackagesViewModel extends BaseViewModel {
  final BuildContext context;

  PackagesViewModel({required this.context});

  int get nummode => _nummode;
  String get reserveSize => _reserveSize;
  String get packageHandle => _packageHandle;
  String get bookingUrl => _bookingUrl;
  bool get playMarketPlaceVideo => _playMarketPlaceVideo;

  /// FeatureStatus
  PackageResponseModel? get packagesResponse => _packagesResponse;
  List<PackageFeature> get availableUserFeatures => _availableUserFeatures;
  FeatureStatus get climatePartner => _climatePartner;
  FeatureStatus get fbPostRecommendation => _fbPostRecommendation;
  FeatureStatus get partnerAccountIntegration => _partnerAccountIntegration;
  FeatureStatus get extraReservation => _extraReservation;
  FeatureStatus get externalProWebsite => _externalProWebsite;
  FeatureStatus get offerCreator => _offerCreator;
  FeatureStatus get mesterMesterReserve => _mesterMesterReserve;
  FeatureStatus get udbudJobs => _udbudJobs;
  FeatureStatus get marketPlaceReserve => _marketPlaceReserve;
  FeatureStatus get mesterMesterPost => _mesterMesterPost;
  FeatureStatus get marketPlaceViewer => _marketPlaceViewer;
  FeatureStatus get personalPartnerLogin => _personalPartnerLogin;
  FeatureStatus get accessVideoProfile => _accessVideoProfile;
  FeatureStatus get accessDescriptionText => _accessDescriptionText;

  int _nummode = 0;
  String _reserveSize = "0";
  String _packageHandle = '';
  String _bookingUrl = Links.hvdkBusiness;
  PackageResponseModel? _packagesResponse;
  List<PackageFeature> _availableUserFeatures = [];
  bool _playMarketPlaceVideo = false;

  final FeatureStatus _climatePartner = FeatureStatus(
    status: false,
    feature: PackageFeature(
      name: "Klimah\u00e5ndv\u00e6rker kursus",
      id: "9515",
      shortCode: "ClimatePartner",
    ),
  );

  final FeatureStatus _fbPostRecommendation = FeatureStatus(
    status: false,
    feature: PackageFeature(
      name:
          "Auto-post anbefalinger p\u00e5 din facebook-profil (integration til facebook)",
      id: "9514",
      shortCode: "FBPostRecommendation",
    ),
  );

  final FeatureStatus _partnerAccountIntegration = FeatureStatus(
    status: false,
    feature: PackageFeature(
      name:
          "Auto-pilot p\u00e5 anbefalinger (ops\u00e6t integration til Dinero, Billy eller E-conomic)",
      id: "9513",
      shortCode: "PartnerAccountingInteg",
    ),
  );

  final FeatureStatus _extraReservation = FeatureStatus(
    status: false,
    feature: PackageFeature(
      name: "",
      id: "",
      shortCode: "ExtraReservation",
    ),
  );

  final FeatureStatus _externalProWebsite = FeatureStatus(
    status: false,
    feature: PackageFeature(
      name: "Egen hjemmeside med deledata fra profilen",
      id: "9509",
      shortCode: "ExternalProWebsite",
    ),
  );

  final FeatureStatus _offerCreator = FeatureStatus(
    status: false,
    feature: PackageFeature(
      name: "Adgang til tilbudsskabelon",
      id: "9506",
      shortCode: "OfferCreator",
    ),
  );

  FeatureStatus _mesterMesterReserve = FeatureStatus(
    status: false,
    feature: PackageFeature(
      name: "Mester til Mester - reserver opgave (max. 2 ad gangen)",
      id: "9501",
      shortCode: "PartnerloginMesterMesterReserve",
    ),
  );

  FeatureStatus _udbudJobs = FeatureStatus(
    status: false,
    feature: PackageFeature(
      name: "",
      id: "",
      shortCode: "PartnerLoginUdbudJobs",
    ),
  );

  FeatureStatus _marketPlaceReserve = FeatureStatus(
    status: false,
    feature: PackageFeature(
      name: "",
      id: "",
      shortCode: "PartnerLoginMarketplaceReserve",
    ),
  );

  FeatureStatus _mesterMesterPost = FeatureStatus(
    status: false,
    feature: PackageFeature(
      name: "Mester til Mester - opret opgave",
      id: "8161",
      shortCode: "PartnerloginMesterMesterPost",
    ),
  );

  FeatureStatus _marketPlaceViewer = FeatureStatus(
    status:
        true, //updated to always be true since its removed in the backend to be available to all
    feature: PackageFeature(
      name: "",
      id: "",
      shortCode: "PartnerLoginMarketplaceViewer",
    ),
  );

  // this.defaultpackage[13] = 'AccessDescriptionText'
  final FeatureStatus _personalPartnerLogin = FeatureStatus(
    status: false,
    feature: PackageFeature(
      name: "Adgang til personligt Partner-login",
      id: "8128",
      shortCode: "PersonalPartnerLogin",
    ),
  );

  final FeatureStatus _accessVideoProfile = FeatureStatus(
    status: false,
    feature: PackageFeature(
      name: "",
      id: "",
      shortCode: "AccessVideoProProfile",
    ),
  );

  final FeatureStatus _accessDescriptionText = FeatureStatus(
    status: false,
    feature: PackageFeature(
      name:
          "Tilf\u00f8j unik virksomhedsbeskrivelse - Ubegr\u00e6nset l\u00e6ngde",
      id: "8151",
      shortCode: "AccessDescriptionText",
    ),
  );

  set playMarketPlaceVideo(bool play) {
    _playMarketPlaceVideo = play;
    notifyListeners();
  }

  Future<void> getPackages() async {
    setBusy(true);
    final packagesService = context.read<PackagesService>();
    _loadDefaults();
    _packagesResponse = await packagesService.getPackages();

    if (_packagesResponse != null &&
        _packagesResponse!.success! &&
        _packagesResponse!.data != null) {
      _playMarketPlaceVideo = _packagesResponse!.data!.playMarketPlaceVideo!;
      _availableUserFeatures = _packagesResponse!.data!.packageFeatures ?? [];
      _nummode = _packagesResponse!.data!.nummode ?? 0;
      _reserveSize = _packagesResponse!.data!.reserveSize!.isEmpty
          ? "0"
          : _packagesResponse!.data!.reserveSize.toString();
      _bookingUrl = _packagesResponse!.data!.booking ?? Links.hvdkBusiness;
      _packageHandle = _packagesResponse!.data!.packageHandle ?? '';
    }

    for (var feature in _availableUserFeatures) {
      if (feature.shortCode == _climatePartner.feature.shortCode) {
        _climatePartner.status = false;
        _climatePartner.feature.id = feature.id;
        _climatePartner.feature.name = feature.name;
        _climatePartner.feature.addOns = feature.addOns;
        _climatePartner.feature.limitation = feature.limitation;
        _climatePartner.feature.type = feature.type;
        continue;
      }

      if (feature.shortCode == _fbPostRecommendation.feature.shortCode) {
        _fbPostRecommendation.status = _packagesResponse?.data?.active ?? false;
        _fbPostRecommendation.feature.id = feature.id;
        _fbPostRecommendation.feature.name = feature.name;
        _fbPostRecommendation.feature.addOns = feature.addOns;
        _fbPostRecommendation.feature.limitation = feature.limitation;
        _fbPostRecommendation.feature.type = feature.type;
        continue;
      }

      if (feature.shortCode == _partnerAccountIntegration.feature.shortCode) {
        _partnerAccountIntegration.status =
            _packagesResponse?.data?.active ?? false;
        _partnerAccountIntegration.feature.id = feature.id;
        _partnerAccountIntegration.feature.name = feature.name;
        _partnerAccountIntegration.feature.addOns = feature.addOns;
        _partnerAccountIntegration.feature.limitation = feature.limitation;
        _partnerAccountIntegration.feature.type = feature.type;
        continue;
      }

      if (feature.shortCode == _extraReservation.feature.shortCode) {
        _extraReservation.status = _packagesResponse?.data?.active ?? false;
        _extraReservation.feature.id = feature.id;
        _extraReservation.feature.name = feature.name;
        _extraReservation.feature.addOns = feature.addOns;
        _extraReservation.feature.limitation = feature.limitation;
        _extraReservation.feature.type = feature.type;
        continue;
      }

      if (feature.shortCode == _externalProWebsite.feature.shortCode) {
        _externalProWebsite.status = _packagesResponse?.data?.active ?? false;
        _externalProWebsite.feature.id = feature.id;
        _externalProWebsite.feature.name = feature.name;
        _externalProWebsite.feature.addOns = feature.addOns;
        _externalProWebsite.feature.limitation = feature.limitation;
        _externalProWebsite.feature.type = feature.type;
        continue;
      }

      if (feature.shortCode == _offerCreator.feature.shortCode) {
        _offerCreator.status = _packagesResponse?.data?.active ?? false;
        _offerCreator.feature.id = feature.id;
        _offerCreator.feature.name = feature.name;
        _offerCreator.feature.addOns = feature.addOns;
        _offerCreator.feature.limitation = feature.limitation;
        _offerCreator.feature.type = feature.type;
        continue;
      }

      if (feature.shortCode == _mesterMesterReserve.feature.shortCode) {
        _mesterMesterReserve.status = _packagesResponse?.data?.active ?? false;
        _mesterMesterReserve.feature.id = feature.id;
        _mesterMesterReserve.feature.name = feature.name;
        _mesterMesterReserve.feature.addOns = feature.addOns;
        _mesterMesterReserve.feature.limitation = feature.limitation;
        _mesterMesterReserve.feature.type = feature.type;
        continue;
      }

      if (feature.shortCode == _udbudJobs.feature.shortCode) {
        _udbudJobs.status = _packagesResponse?.data?.active ?? false;
        _udbudJobs.feature.id = feature.id;
        _udbudJobs.feature.name = feature.name;
        _udbudJobs.feature.addOns = feature.addOns;
        _udbudJobs.feature.limitation = feature.limitation;
        _udbudJobs.feature.type = feature.type;
        continue;
      }

      if (feature.shortCode == _marketPlaceReserve.feature.shortCode) {
        _marketPlaceReserve.status = _packagesResponse?.data?.active ?? false;
        _marketPlaceReserve.feature.id = feature.id;
        _marketPlaceReserve.feature.name = feature.name;
        _marketPlaceReserve.feature.addOns = feature.addOns;
        _marketPlaceReserve.feature.limitation = feature.limitation;
        _marketPlaceReserve.feature.type = feature.type;
        continue;
      }

      if (feature.shortCode == _mesterMesterPost.feature.shortCode) {
        _mesterMesterPost.status = _packagesResponse?.data?.active ?? false;
        _mesterMesterPost.feature.id = feature.id;
        _mesterMesterPost.feature.name = feature.name;
        _mesterMesterPost.feature.addOns = feature.addOns;
        _mesterMesterPost.feature.limitation = feature.limitation;
        _mesterMesterPost.feature.type = feature.type;
        continue;
      }

      if (feature.shortCode == _marketPlaceViewer.feature.shortCode) {
        _marketPlaceViewer.status = _packagesResponse?.data?.active ?? false;
        _marketPlaceViewer.feature.id = feature.id;
        _marketPlaceViewer.feature.name = feature.name;
        _marketPlaceViewer.feature.addOns = feature.addOns;
        _marketPlaceViewer.feature.limitation = feature.limitation;
        _marketPlaceViewer.feature.type = feature.type;
        continue;
      }

      if (feature.shortCode == _personalPartnerLogin.feature.shortCode) {
        _personalPartnerLogin.status = _packagesResponse?.data?.active ?? false;
        _personalPartnerLogin.feature.id = feature.id;
        _personalPartnerLogin.feature.name = feature.name;
        _personalPartnerLogin.feature.addOns = feature.addOns;
        _personalPartnerLogin.feature.limitation = feature.limitation;
        _personalPartnerLogin.feature.type = feature.type;
        continue;
      }

      if (feature.shortCode == _accessVideoProfile.feature.shortCode) {
        _accessVideoProfile.status = _packagesResponse?.data?.active ?? false;
        _accessVideoProfile.feature.id = feature.id;
        _accessVideoProfile.feature.name = feature.name;
        _accessVideoProfile.feature.addOns = feature.addOns;
        _accessVideoProfile.feature.limitation = feature.limitation;
        _accessVideoProfile.feature.type = feature.type;
        continue;
      }

      if (feature.shortCode == _accessDescriptionText.feature.shortCode) {
        _accessDescriptionText.status =
            _packagesResponse?.data?.active ?? false;
        _accessDescriptionText.feature.id = feature.id;
        _accessDescriptionText.feature.name = feature.name;
        _accessDescriptionText.feature.addOns = feature.addOns;
        _accessDescriptionText.feature.limitation = feature.limitation;
        _accessDescriptionText.feature.type = feature.type;
        continue;
      }

      /// If "marketPlaceReserve" and "mesterMesterReserve" are enabled, enable the "marketplaceViewer" feature
      if (feature.shortCode == _marketPlaceReserve.feature.shortCode ||
          feature.shortCode == _mesterMesterReserve.feature.shortCode ||
          feature.shortCode == _marketPlaceViewer.feature.shortCode) {
        _marketPlaceViewer.status = _packagesResponse?.data?.active ?? false;
        continue;
      }
    }

    setBusy(false);
  }

  _loadDefaults() {
    _mesterMesterReserve = FeatureStatus(
      status: false,
      feature: PackageFeature(
        name: "Mester til Mester - reserver opgave (max. 2 ad gangen)",
        id: "9501",
        shortCode: "PartnerloginMesterMesterReserve",
      ),
    );

    _udbudJobs = FeatureStatus(
      status: false,
      feature: PackageFeature(
        name: "",
        id: "",
        shortCode: "PartnerLoginUdbudJobs",
      ),
    );

    _marketPlaceReserve = FeatureStatus(
      status: false,
      feature: PackageFeature(
        name: "",
        id: "",
        shortCode: "PartnerLoginMarketplaceReserve",
      ),
    );

    _mesterMesterPost = FeatureStatus(
      status: false,
      feature: PackageFeature(
        name: "Mester til Mester - opret opgave",
        id: "8161",
        shortCode: "PartnerloginMesterMesterPost",
      ),
    );

    _marketPlaceViewer = FeatureStatus(
      status:
          true, //updated to always be true since its removed in the backend to be available to all
      feature: PackageFeature(
        name: "",
        id: "",
        shortCode: "PartnerLoginMarketplaceViewer",
      ),
    );
  }
}
