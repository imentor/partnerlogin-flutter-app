import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:Haandvaerker.dk/model/contract/contract_model.dart';
import 'package:Haandvaerker.dk/model/contract/contract_template_model.dart';
import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/model/projects/project_message_model.dart';
import 'package:Haandvaerker.dk/model/wizard/list_price_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/contract/contract_service.dart';
import 'package:Haandvaerker.dk/services/messages/message_service.dart';
import 'package:Haandvaerker.dk/services/mitid/mitid_service.dart';
import 'package:Haandvaerker.dk/services/offer/offer_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:html_to_pdf/html_to_pdf.dart';
import 'package:intl/intl.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

class ContractViewModel extends BaseViewModel {
  ContractViewModel({required this.context});
  final BuildContext context;

  ContractTemplateModel? _contractTemplateModel;
  ContractTemplateModel? get contractTemplateModel => _contractTemplateModel;

  List<ListPrice> _listPrices = [];
  List<ListPrice> get listPrices => _listPrices;

  Map<String, TextEditingController> _textEditingControllers = {};
  Map<String, TextEditingController> get textEditingControllers =>
      _textEditingControllers;

  Map<String, String?> _radioControllers = {};
  Map<String, String?> get radioControllers => _radioControllers;

  Map<String, dynamic> _contractForm = {};
  Map<String, dynamic> get contractForm => _contractForm;

  Map<String, dynamic> _stepForms = {};
  Map<String, dynamic> get stepForms => _stepForms;

  int _currentStep = 0;
  int get currentStep => _currentStep;

  int _totalStep = 0;
  int get totalStep => _totalStep;

  int _totalWithVat = 0;
  int get totalWithVat => _totalWithVat;

  int _totalVatWithoutFee = 0;
  int get totalVatWithoutFee => _totalVatWithoutFee;

  int _editContractStep = 0;
  int get editContractStep => _editContractStep;

  int _editContractItemCount = 5;
  int get editContractItemCount => _editContractItemCount;

  String? _contractPdf;
  String? get contractPdf => _contractPdf;

  String _addendumText = '';
  String get addendumText => _addendumText;

  ContractModel? _contract;
  ContractModel? get contract => _contract;

  Map<String, dynamic> _contractHeaders = {};
  Map<String, dynamic> get contractHeaders => _contractHeaders;

  //V2
  Map<String, dynamic> _contractWizardJsonSteps = {};
  Map<String, dynamic> get contractWizardJsonSteps => _contractWizardJsonSteps;
  //

  String _cancelContractTemplate = '';
  String get cancelContractTemplate => _cancelContractTemplate;

  String _prisUnformattedValue = '';
  String get prisUnformattedValue => _prisUnformattedValue;

  bool _isContractDataLoading = false;
  bool get isContractDataLoading => _isContractDataLoading;

  bool _isCreateNewContractVersion = false;
  bool get isCreateNewContractVersion => _isCreateNewContractVersion;

  void reset() {
    _contractTemplateModel = null;
    _listPrices = [];
    _textEditingControllers = {};
    _radioControllers = {};
    _contractForm = {};
    _stepForms = {};
    _currentStep = 0;
    _totalStep = 0;
    _totalWithVat = 0;
    _totalVatWithoutFee = 0;
    _editContractStep = 0;
    _editContractItemCount = 5;
    _contractPdf = null;
    _contract = null;
    _contractHeaders = {};
    _contractWizardJsonSteps = {};
    _cancelContractTemplate = '';
    _prisUnformattedValue = '';
    _addendumText = '';
    _isContractDataLoading = false;
    _isCreateNewContractVersion = false;

    notifyListeners();
  }

  set isContractDataLoading(bool value) {
    _isContractDataLoading = value;
    notifyListeners();
  }

  set isCreateNewContractVersion(bool value) {
    _isCreateNewContractVersion = value;
    notifyListeners();
  }

  set editContractItemCount(int value) {
    _editContractItemCount = value;
    notifyListeners();
  }

  set addendumText(String value) {
    _addendumText = value;
    notifyListeners();
  }

  set prisUnformattedValue(String value) {
    _prisUnformattedValue = value;
    notifyListeners();
  }

  Future<bool> cancelContract(
      {required int contractId, required Map<String, dynamic> forms}) async {
    final contractService = context.read<ContractService>();
    final mitidService = context.read<MitidService>();

    final Map<String, dynamic> newForms = {
      ...forms['token'] as Map<String, dynamic>
    };
    newForms.putIfAbsent(
        'claims', () => JwtDecoder.decode(newForms['id_token'] as String));

    await mitidService.mitidApproved(forms: newForms);

    final cancelResponse =
        await contractService.cancelContract(contractId: contractId);

    return cancelResponse.success ?? false;
  }

  Future<void> getCancelContractDetails({required int contractId}) async {
    final contractService = context.read<ContractService>();
    List<String> htmlString = [];

    final response = await contractService.getPartnerCancellationContract(
        contractId: contractId);

    if (!(response.success ?? false)) {
      throw response.message ?? '';
    }

    final contractResponse =
        await contractService.getContractInfoOnly(contractId: contractId);

    if (!(contractResponse.success ?? false)) {
      throw contractResponse.message ?? '';
    }

    final htmlResponse =
        await contractService.getContractHtmlPreview(contractId: contractId);

    final responseData = contractResponse.data as Map<String, dynamic>;
    final dataDocument = responseData['DOCUMENTS'] as List<dynamic>;
    for (int i = 0; i < dataDocument.length; i++) {
      final documenJsonData = dataDocument[i]['JSON_DATA'];
      final contactId = documenJsonData['contactId'];
      final totalPriceWithVat = documenJsonData['totalpricewithvat'];
      final totalPriceWithVatAndFee =
          documenJsonData['totalpricewithvatandfee'];
      if (contactId != null && contactId.runtimeType == String) {
        dataDocument[i]['JSON_DATA']['contactId'] = int.tryParse(contactId);
      }
      if (totalPriceWithVat != null &&
          totalPriceWithVat.runtimeType == String) {
        dataDocument[i]['JSON_DATA']['totalpricewithvat'] =
            int.tryParse(totalPriceWithVat);
      }
      if (totalPriceWithVatAndFee != null &&
          totalPriceWithVatAndFee.runtimeType == String) {
        dataDocument[i]['JSON_DATA']['totalpricewithvatandfee'] =
            int.tryParse(totalPriceWithVatAndFee);
      }
    }
    // if (responseData['INSURANCE'].runtimeType != String) {
    //   responseData['INSURANCE'] = responseData['INSURANCE'].toString();
    // }
    final contract = ContractModel.fromJson(responseData);

    Map<String, dynamic> steps = response.data['MAIL']['STEPS'];

    for (var step in steps.entries) {
      for (var element in step.value['fields'] as List) {
        htmlString.add(element['label']);
      }
    }

    if (htmlResponse.data != null) {
      final output = await getTemporaryDirectory();

      final generatedPdfFile = await HtmlToPdf.convertFromHtmlContent(
        htmlContent: htmlResponse.data,
        printPdfConfiguration: PrintPdfConfiguration(
            targetDirectory: output.path, targetName: "contract"),
      );

      _contractPdf = generatedPdfFile.path;
    }

    _cancelContractTemplate = htmlString
        .join(' ')
        .replaceAll('[Craftsman Name]',
            '<strong>${contract.project?.partner?.name ?? ''}</strong>')
        .replaceAll('[ContractSignedDate]',
            '<strong>${DateFormat.yMMMMd('da').format(DateTime.parse(contract.dateSignedByContact!))}</strong>')
        .replaceAll(
            '[ContractPrice]',
            '<strong>${NumberFormat.currency(
              locale: 'da',
              symbol: 'kr.',
            ).format(contract.vatPrice ?? 0)}</strong>')
        .replaceAll('[ContractName]',
            '<strong>${contract.documents?.first.jsonData?.title ?? ''}</strong>')
        .replaceAll('[Customer Name]',
            '<strong>${contract.project?.homeOwner?.name ?? ''}</strong>')
        .replaceAll(
            '[DepositedAmount]',
            '<strong>${NumberFormat.currency(
              locale: 'da',
              symbol: 'kr.',
            ).format(contract.transaction?.depositedAmount ?? 0)}</strong>')
        .replaceAll('[TransactionCut%]',
            '<strong>${contract.transactionCut}%</strong>');

    notifyListeners();
  }

  Future<bool?> submitContractV2(
      {required Map<String, dynamic> newForm,
      required bool isContractUpdate,
      int? contractId,
      String? cvr,
      String? companyName,
      required int companyId,
      required int projectId}) async {
    final service = Provider.of<ContractService>(context, listen: false);
    final contractService =
        Provider.of<ContractService>(context, listen: false);
    final List<Map<String, dynamic>> paymentStages = [];
    final List<ListPrice> tempListPrice = [..._listPrices];

    Map<String, dynamic> tempJsonSteps = {..._contractWizardJsonSteps};

    Map<String, dynamic> submitContractPayload = {};

    String contactId = '';
    int offerId = 0;

    for (var element in newForm.entries) {
      if (RegExp(r'\d').hasMatch(element.key)) {
        final step = element.key.split(':').first;
        final key = element.key.split(':').last;

        List<StepField?> fields =
            tempJsonSteps['step_$step'] as List<StepField?>;

        final index = fields.indexWhere((element) => element?.fieldName == key);

        fields[index]?.defaultValue = element.value;

        tempJsonSteps['step_$step'] = fields;
      }
    }

    for (final listPrice in tempListPrice) {
      for (var price in listPrice.listPricesMinbolig!) {
        // projectId = int.parse(price.projectId!);
        contactId = price.contactId ?? '';
        offerId = int.parse(price.offerId!);
        paymentStages.add({
          "description_payment": price.series,
          "project_id": int.parse(price.projectId!),
          "partner_id": int.parse(price.partnerId!),
          "contact_id": price.contactId,
          "calculation_id": 0,
          "industry_id": price.industry,
          "job_industry_id": price.jobIndustry,
          "price_total": price.total,
          "vat_total": price.totalVat,
          "note": price.note,
          "signature": int.parse(price.signature!),
          "weeks": int.parse(price.weeks!),
          "start_date": price.startDate,
          "offer_id": int.parse(price.offerId!),
          "sub_project_id": price.subProjectId
        });
      }
    }

    submitContractPayload.putIfAbsent(
        'paymentStages', () => [...paymentStages]);
    submitContractPayload.putIfAbsent('steps', () => jsonEncode(tempJsonSteps));

    submitContractPayload.putIfAbsent(
        'totalpricewithvatandfee', () => _totalWithVat);
    submitContractPayload.putIfAbsent(
        'totalpricewithvat', () => _totalVatWithoutFee);
    submitContractPayload.putIfAbsent('addendumText', () => _addendumText);

    for (var element in newForm.entries) {
      List<StepField> fields = <StepField>[];
      if (RegExp(r'\d').hasMatch(element.key)) {
        final step = element.key.split(':').first;
        fields = tempJsonSteps['step_$step'];

        for (var step in fields) {
          if (step.field != 0) {
            submitContractPayload.putIfAbsent(
                '${step.fieldName}', () => step.defaultValue ?? '');
          }
        }
      }

      if (element.key.contains('ADDENDUM')) {
        submitContractPayload.remove('ADDENDUM');
      }
      if (element.key.contains('PRIS')) {
        if (_prisUnformattedValue.contains('.')) {
          int totalpricewithvat = 0;
          int totalpricewithvatandfee = 0;
          if (_editContractItemCount == 5) {
            totalpricewithvat =
                int.parse(_prisUnformattedValue.split('.').first);
            await contractService
                .getTotalFees(totalWithVat: totalpricewithvat)
                .then((value) {
              totalpricewithvatandfee = totalpricewithvat +
                  (value.data as Map<String, dynamic>)['FEEINAMOUNT'] as int;
            });
          } else {
            totalpricewithvat =
                (int.parse(_prisUnformattedValue.split('.').first) * 1.25)
                    .round();
            await contractService
                .getTotalFees(totalWithVat: totalpricewithvat)
                .then((value) {
              totalpricewithvatandfee = totalpricewithvat +
                  (value.data as Map<String, dynamic>)['FEEINAMOUNT'] as int;
            });
          }

          submitContractPayload.update(
              'totalpricewithvat', (value) => totalpricewithvat);
          submitContractPayload.update(
              'totalpricewithvatandfee', (value) => totalpricewithvatandfee);
        } else {
          int totalpricewithvat = 0;
          int totalpricewithvatandfee = 0;
          if (_editContractItemCount == 5) {
            totalpricewithvat =
                int.parse(_prisUnformattedValue.split('.').first);
            await contractService
                .getTotalFees(totalWithVat: totalpricewithvat)
                .then((value) {
              totalpricewithvatandfee = totalpricewithvat +
                  (value.data as Map<String, dynamic>)['FEEINAMOUNT'] as int;
            });
          } else {
            totalpricewithvat =
                (int.parse(_prisUnformattedValue) * 1.25).round();
            await contractService
                .getTotalFees(totalWithVat: totalpricewithvat)
                .then((value) {
              totalpricewithvatandfee = totalpricewithvat +
                  (value.data as Map<String, dynamic>)['FEEINAMOUNT'] as int;
            });
          }

          submitContractPayload.update(
              'totalpricewithvat', (value) => totalpricewithvat);
          submitContractPayload.update(
              'totalpricewithvatandfee', (value) => totalpricewithvatandfee);
        }
        submitContractPayload.remove('PRIS');
      }
    }

    if (isContractUpdate) {
      submitContractPayload.addAll({
        "projectId": _contract?.project?.id ?? '',
        "templateId": _contract?.templateId ?? '',
        "contractTemplateId":
            _contract?.documents?.first.jsonData?.contractTemplateId ?? '',
        "title": _contract?.documents?.first.jsonData?.title ?? '',
        "companyId": _contract?.company ?? '',
        "contactId": _contract?.contact ?? '',
        "name": newForm['partnerFirstName'],
        "lastname": newForm['partnerLastName'],
        "address": newForm['partnerAddress'],
        "zipcode": newForm['partnerZipCode'],
        "email": newForm['partnerEmail'],
        "mobile": newForm['partnerPhoneNumber'],
        "city": newForm['partnerTown'],
        "cvr": cvr,
        "companyName": companyName,
        "OFFERID": offerId,
        "updatedFrom": 'partner-app'
      });
    } else {
      submitContractPayload.addAll({
        "projectId": projectId,
        "templateId": _contractTemplateModel!.templateId,
        "contractTemplateId": _contractTemplateModel!.id,
        "title": 'Kontrakt',
        "companyId": companyId,
        "contactId": contactId,
        "name": newForm['partnerFirstName'],
        "lastname": newForm['partnerLastName'],
        "address": newForm['partnerAddress'],
        "zipcode": newForm['partnerZipCode'],
        "email": newForm['partnerEmail'],
        "mobile": newForm['partnerPhoneNumber'],
        "city": newForm['partnerTown'],
        "cvr": cvr,
        "companyName": companyName,
        "OFFERID": offerId,
        "createdFrom": 'partner-app',
        "isVersion": _isCreateNewContractVersion
      });
    }

    var response = isContractUpdate
        ? await service.updateContract(
            form: submitContractPayload, contractId: contractId!)
        : await service.submitContract(form: submitContractPayload);

    if (!(response.success ?? true)) {
      throw ApiException(message: response.message ?? '');
    }
    return (response.success ?? false);
  }

  void contractDetailsToDefault() {
    _contractTemplateModel = null;
    _listPrices = [];
    _textEditingControllers = {};
    _radioControllers = {};
    _contractForm = {};
    _stepForms = {};
    _currentStep = 0;
    _totalStep = 0;
    _totalWithVat = 0;
    _totalVatWithoutFee = 0;
    _editContractStep = 0;
    _editContractItemCount = 5;
    _prisUnformattedValue = '';
    _addendumText = '';
    _contractPdf = null;
    _contract = null;
  }

  // Future<void> getContractTemplate() async {
  //   setBusy(true);
  //   var service = context.read<ContractService>(context, listen: false);

  //   var response = await service.getContractTemplate();

  //   final templates = ContractTemplateModel.fromCollection(
  //       (response.data as Map<String, dynamic>)['result']);

  //   _contractTemplateModel =
  //       templates.where((element) => element.templateId == "27").first;

  //   for (final item
  //       in (_contractTemplateModel.steps as Map<String, dynamic>).values) {
  //     final step = TemplateSteps.fromJson(item as Map<String, dynamic>);

  //     for (final element in step.fields) {
  //       if (element.field == 2 || element.field == 8 || element.field == 10) {
  //         textEditingControllers.putIfAbsent(
  //             '${step.key}_${element.key}_${element.fieldName}',
  //             () => TextEditingController());
  //       } else if (element.field == 3 ||
  //           element.field == 4 ||
  //           element.field == 7) {
  //         radioControllers.putIfAbsent(
  //             '${step.key}_${element.key}_${element.defaultType}_${element.fieldName}',
  //             () => element.defaultValue);
  //       }
  //     }
  //   }

  //   notifyListeners();
  //   setBusy(false);
  // }

  Future<void> nextStep() async {
    _currentStep++;
    notifyListeners();
  }

  Future<void> previousStep() async {
    _currentStep--;
    notifyListeners();
  }

  Future<void> editContractNextStep() async {
    _editContractStep++;
    notifyListeners();
  }

  Future<void> editContractPreviousStep() async {
    _editContractStep--;
    notifyListeners();
  }

  Future<void> updateContractForm(Map<String, dynamic> form) async {
    _contractForm = form;
    notifyListeners();
  }

  Future<bool> submitContract({
    bool isContractUpdate = false,
    int? contractId,
    String? cvr,
    String? companyName,
  }) async {
    try {
      var service = context.read<ContractService>();
      final List<Map<String, dynamic>> paymentStages = [];
      final List<ListPrice> tempListPrice = [..._listPrices];

      Map<String, dynamic> tempContractForm = {..._contractForm};
      Map<String, dynamic> tempStepForm = {..._stepForms};
      final Map<String, String?> tempRadioControllers = {..._radioControllers};
      final Map<String, TextEditingController> tempTextEditingController = {
        ..._textEditingControllers
      };

      String contactId = '';

      for (final listPrice in tempListPrice) {
        for (var price in listPrice.listPricesMinbolig!) {
          contactId = price.contactId ?? '';
          paymentStages.add({
            "description_payment": price.series,
            "project_id": int.parse(price.projectId!),
            "partner_id": int.parse(price.partnerId!),
            "contact_id": price.contactId,
            "calculation_id": 0,
            "industry_id": price.industry,
            "job_industry_id": price.jobIndustry,
            "price_total": price.total,
            "vat_total": price.totalVat,
            "note": price.note,
            "signature": int.parse(price.signature!),
            "weeks": int.parse(price.weeks!),
            "start_date": price.startDate,
            "offer_id": int.parse(price.offerId!),
            "sub_project_id": price.subProjectId
          });
        }
      }

      tempContractForm.putIfAbsent('paymentStages', () => [...paymentStages]);
      tempContractForm.putIfAbsent('steps', () => jsonEncode(tempStepForm));

      tempTextEditingController.forEach((key, value) {
        if (key.split('_').length == 3) {
          final newKey = key.split('_')[2];
          if (newKey.isNotEmpty) {
            tempContractForm.putIfAbsent(newKey, () => value.text);
          }
        }
      });

      tempRadioControllers.forEach((key, value) {
        if (key.split('_').length == 4) {
          final newKey = key.split('_')[3];
          if (newKey.isNotEmpty) {
            tempContractForm.putIfAbsent(newKey, () => value);
          }
        }
      });
      tempContractForm.putIfAbsent(
          'totalpricewithvatandfee', () => _totalWithVat);
      tempContractForm.putIfAbsent(
          'totalpricewithvat', () => _totalVatWithoutFee);

      if (isContractUpdate) {
        tempContractForm.addAll({
          "projectId": _contract?.documents?.first.jsonData?.projectId ?? '',
          "templateId": _contract?.documents?.first.jsonData?.templateId ?? '',
          "contractTemplateId":
              _contract?.documents?.first.jsonData?.contractTemplateId ?? '',
          "title": _contract?.documents?.first.jsonData?.title ?? '',
          "companyId": _contract?.documents?.first.jsonData?.companyId ?? '',
          "contactId": contactId,
          "name": _contract?.documents?.first.jsonData?.name ?? '',
          "lastname": _contract?.documents?.first.jsonData?.lastName ?? '',
          "address": _contract?.documents?.first.jsonData?.address ?? '',
          "zipcode": _contract?.documents?.first.jsonData?.zipcode ?? '',
          "email": _contract?.documents?.first.jsonData?.email ?? '',
          "mobile": _contract?.documents?.first.jsonData?.mobile ?? '',
          "city": _contract?.documents?.first.jsonData?.city ?? '',
          "cvr": cvr,
          "companyName": companyName
        });
      }

      var response = isContractUpdate
          ? await service.updateContract(
              form: tempContractForm, contractId: contractId!)
          : await service.submitContract(form: tempContractForm);
      return response.success!;
    } catch (e) {
      _currentStep = 0;
      notifyListeners();
      setBusy(false);
      log('message submitContract $e');
      if (e is DioException) log('message submitContract ${e.response!.data}');
      return false;
    }
  }

  Future<void> updateStepForm(TemplateSteps step) async {
    final Map<String, String?> tempRadioControllers = {..._radioControllers};
    final Map<String, TextEditingController> tempTextEditingController = {
      ..._textEditingControllers
    };
    final Map<String, dynamic> forms = {..._stepForms};
    final String key = 'step_${step.key}';
    final List<StepField?> stepField = [...step.fields!];
    for (var i = 0; i < stepField.length; i++) {
      if (tempTextEditingController.containsKey(
                  '${step.key}_${stepField[i]!.key}_${stepField[i]!.fieldName}') &&
              stepField[i]!.field == 2 ||
          stepField[i]!.field == 8 ||
          stepField[i]!.field == 10) {
        final tempStep = stepField[i];
        tempStep!.defaultValue = tempTextEditingController[
                '${step.key}_${stepField[i]!.key}_${stepField[i]!.fieldName}']!
            .text;
        stepField.removeAt(i);
        stepField.insert(i, tempStep);
      } else if (tempRadioControllers.containsKey(
                  '${step.key}_${stepField[i]!.key}_${stepField[i]!.defaultType}_${stepField[i]!.fieldName}') &&
              stepField[i]!.field == 3 ||
          stepField[i]!.field == 4 ||
          stepField[i]!.field == 7) {
        final tempStep = stepField[i];
        tempStep!.defaultValue = tempRadioControllers[
            '${step.key}_${stepField[i]!.key}_${stepField[i]!.defaultType}_${stepField[i]!.fieldName}'];
        stepField.removeAt(i);
        stepField.insert(i, tempStep);
      }
    }
    final templateStep = step;
    templateStep.fields = [...stepField];

    if (forms.containsKey(key)) {
      forms.update(key, (value) => templateStep);
    } else {
      forms.putIfAbsent(key, () => templateStep);
    }

    _stepForms = forms;
    notifyListeners();
  }

  Future<void> updateControllers(String key, String? value) async {
    final Map<String, String?> tempRadioControllers = {..._radioControllers};
    final Map<String, TextEditingController> tempTextEditingController = {
      ..._textEditingControllers
    };
    if (tempRadioControllers.containsKey(key)) {
      tempRadioControllers.update(key, (e) => value);
    } else if (tempTextEditingController.containsKey(key)) {
      final temp = tempTextEditingController[key]!;
      temp.text = value!;

      tempTextEditingController.update(key, (e) => temp);
    }

    _radioControllers = tempRadioControllers;
    _textEditingControllers = tempTextEditingController;
    notifyListeners();
  }

  Future<void> initContractWizard(
      {required int projectId,
      required int parentProjectId,
      required int offerId,
      required String wholeEnterprise,
      required int companyId,
      required int contactId,
      required int homeownerId,
      required List<AllWizardText> wizardsTexts,
      required List<Industry>? industryList,
      required dynamic jobList,
      bool isContractUpdate = false,
      int? contractId,
      int? acceptedOfferVersionId}) async {
    try {
      List<ListPrice> tempListPrices = [];
      List<String> industryIdList = [];
      Map<String, dynamic> tempSteps = {};
      Map<String, dynamic> contractAnswers = {};

      _listPrices.clear();
      _contractForm.clear();
      _radioControllers.clear();
      _textEditingControllers.clear();
      _stepForms.clear();
      _currentStep = 0;
      _totalStep = 0;
      _totalWithVat = 0;
      _totalVatWithoutFee = 0;
      _editContractStep = 0;
      _editContractItemCount = 5;
      _addendumText = '';
      _prisUnformattedValue = '';
      _contractTemplateModel = ContractTemplateModel();
      _contractWizardJsonSteps = {};

      int tempTotalWithVat = 0;
      int tempTotalWithoutVat = 0;

      final List<String?> industries = [];

      var offerService = context.read<OfferService>();
      final contractService = context.read<ContractService>();

      var listPriceResponse =
          (acceptedOfferVersionId != null && acceptedOfferVersionId != 0)
              ? await offerService.getListPriceMinboligOfferVersion(
                  contactId: contactId,
                  offerVersionId: acceptedOfferVersionId,
                  projectId: parentProjectId)
              : await offerService.getListPriceMinbolig(
                  projectId: parentProjectId, contactId: contactId);

      if (listPriceResponse != null) {
        var listPricesMinbolig = ListPriceMinbolig.fromCollection(
            (listPriceResponse.data as Map<String, dynamic>)['result']);

        for (final job in jobList) {
          if (job is List) {
            for (final item in job) {
              industryIdList.add(item.toString().split(':').first);
            }
          } else {
            industryIdList.add(job.toString().split(':').first);
          }
        }

        if (jobList.length != 1 &&
            jobList.contains("1062") &&
            _contract!.project!.contractType == "Hovedentreprise") {
          industryIdList.removeWhere((element) => element == "1062");
        }

        var filteredListPrices = listPricesMinbolig
            .where((element) => element.partnerId == companyId.toString())
            .toList()
            .where((element) => industryIdList.contains(element.jobIndustry))
            .toList();

        for (final filterListPrice in filteredListPrices) {
          if (industries
              .where((element) => element == filterListPrice.industry)
              .isEmpty) {
            industries.add(filterListPrice.industry);
          }
        }

        for (final industry in industries) {
          List<ListPriceMinbolig> tempListPriceMinbolig = [];
          for (final temp in filteredListPrices
              .where((element) => element.industry == industry)
              .toList()) {
            if (temp.totalVat!.contains('.')) {
              tempTotalWithVat += double.parse(temp.totalVat!).toInt();
              tempTotalWithoutVat += double.parse(temp.totalVat!).toInt();
            } else {
              tempTotalWithVat += int.parse(temp.totalVat!);
              tempTotalWithoutVat += int.parse(temp.totalVat!);
            }

            final price = temp;
            price.industryName = industryList!
                .where((element) => element.id.toString() == temp.jobIndustry)
                .first
                .name;
            tempListPriceMinbolig.add(price);
          }

          tempListPrices.add(ListPrice(
              productTypeId: industry,
              productName: wizardsTexts
                  .where((element) => element.producttypeid == industry)
                  .first
                  .producttypeDa,
              productNameEn: wizardsTexts
                  .where((element) => element.producttypeid == industry)
                  .first
                  .producttypeDa,
              listPricesMinbolig: [...tempListPriceMinbolig]));
        }
      }

      if (isContractUpdate) {
        final contractResponse =
            await contractService.getContractInfoOnly(contractId: contractId!);

        final responseData = contractResponse.data as Map<String, dynamic>;
        final dataDocument = responseData['DOCUMENTS'] as List<dynamic>;
        for (int i = 0; i < dataDocument.length; i++) {
          final documenJsonData = dataDocument[i]['JSON_DATA'];
          final contactId = documenJsonData['contactId'];
          final totalPriceWithVat = documenJsonData['totalpricewithvat'];
          final totalPriceWithVatAndFee =
              documenJsonData['totalpricewithvatandfee'];
          if (contactId != null && contactId.runtimeType == String) {
            dataDocument[i]['JSON_DATA']['contactId'] = int.tryParse(contactId);
          }
          if (totalPriceWithVat != null &&
              totalPriceWithVat.runtimeType == String) {
            dataDocument[i]['JSON_DATA']['totalpricewithvat'] =
                int.tryParse(totalPriceWithVat);
          }
          if (totalPriceWithVatAndFee != null &&
              totalPriceWithVatAndFee.runtimeType == String) {
            dataDocument[i]['JSON_DATA']['totalpricewithvatandfee'] =
                int.tryParse(totalPriceWithVatAndFee);
          }
        }
        // if (responseData['INSURANCE'].runtimeType != String) {
        //   responseData['INSURANCE'] = responseData['INSURANCE'].toString();
        // }
        _contract = ContractModel.fromJson(responseData);

        List documents = contractResponse.data['DOCUMENTS'];

        if (documents.isNotEmpty) {
          contractAnswers = documents.first['JSON_DATA'];
        }
      }

      var totalFessResponse =
          await contractService.getTotalFees(totalWithVat: tempTotalWithVat);

      var contractResponse = await contractService.getContractInfoV2(
          contractValue: tempTotalWithVat,
          offerId: offerId,
          projectId: projectId,
          wholeEnterprise: wholeEnterprise,
          homeownerId: homeownerId);

      if (contractResponse != null) {
        ContractTemplateModel tempContractTemplate =
            ContractTemplateModel.fromJson(
                (contractResponse.data as Map<String, dynamic>)['contract']);

        for (final item
            in (tempContractTemplate.steps as Map<String, dynamic>).values) {
          TemplateSteps template =
              TemplateSteps.fromJson(item as Map<String, dynamic>);
          List<StepField> stepFields =
              (template.fields ?? []).map((field) => field!).toList();

          List<StepField> tempFields = [];

          for (var element in stepFields) {
            StepField tempStepField = element;

            if (tempStepField.fieldName != null) {
              if (contractAnswers.containsKey(tempStepField.fieldName)) {
                tempStepField.defaultValue =
                    contractAnswers[tempStepField.fieldName];
              }
            }

            tempFields.add(tempStepField);
          }

          _contractWizardJsonSteps.addAll({
            "step_${template.key}": [...tempFields]
          });

          tempSteps.addAll({
            "step_${template.key}": {
              "label": template.label,
              "key": template.key,
              "Info": template.info,
              "fields": [...tempFields.map((e) => e.toJson())]
            }
          });
        }
        tempContractTemplate.steps = tempSteps;

        _contractTemplateModel = tempContractTemplate;

        _listPrices = tempListPrices;
        _totalWithVat = tempTotalWithVat +
                (totalFessResponse.data as Map<String, dynamic>)['FEEINAMOUNT']
            as int;
        _totalVatWithoutFee = tempTotalWithoutVat;
      }

      notifyListeners();

      setBusy(false);
    } catch (e) {
      setBusy(false);
      log("getContractInfoV2 $e");
      if (e is DioException) log(": ${e.response!.data}");
    }
  }

  Future<void> getContractInfoV2(
      {required int projectId,
      required int parentProjectId,
      required int offerId,
      required String wholeEnterprise,
      required int companyId,
      required int contactId,
      required int homeownerId,
      required List<AllWizardText> wizardsTexts,
      required List<Industry>? industryList,
      required dynamic jobList,
      bool isContractUpdate = false,
      int? contractId}) async {
    try {
      setBusy(true);
      List<ListPrice> tempListPrices = [];
      List<String> industryIdList = [];

      _listPrices.clear();
      _contractForm.clear();
      _radioControllers.clear();
      _textEditingControllers.clear();
      _stepForms.clear();
      _currentStep = 0;
      _totalStep = 0;
      _totalWithVat = 0;
      _totalVatWithoutFee = 0;
      _editContractStep = 0;
      _editContractItemCount = 5;
      _addendumText = '';
      _prisUnformattedValue = '';
      _contractTemplateModel = ContractTemplateModel();

      int tempTotalWithVat = 0;
      int tempTotalWithoutVat = 0;

      final List<String?> industries = [];

      var offerService = context.read<OfferService>();
      final contractService = context.read<ContractService>();

      var listPriceResponse = await offerService.getListPriceMinbolig(
          projectId: parentProjectId, contactId: contactId);

      if (listPriceResponse != null) {
        var listPricesMinbolig = ListPriceMinbolig.fromCollection(
            (listPriceResponse.data as Map<String, dynamic>)['result']);

        for (final job in jobList) {
          if (job is List) {
            for (final item in job) {
              industryIdList.add(item.toString().split(':').first);
            }
          } else {
            industryIdList.add(job.toString().split(':').first);
          }
        }

        if (jobList.length != 1 &&
            jobList.contains("1062") &&
            _contract!.project!.contractType == "Hovedentreprise") {
          industryIdList.removeWhere((element) => element == "1062");
        }

        var filteredListPrices = listPricesMinbolig
            .where((element) => element.partnerId == companyId.toString())
            .toList()
            .where((element) => industryIdList.contains(element.jobIndustry))
            .toList();

        for (final filterListPrice in filteredListPrices) {
          if (industries
              .where((element) => element == filterListPrice.industry)
              .isEmpty) {
            industries.add(filterListPrice.industry);
          }
        }

        for (final industry in industries) {
          List<ListPriceMinbolig> tempListPriceMinbolig = [];
          for (final temp in filteredListPrices
              .where((element) => element.industry == industry)
              .toList()) {
            if (temp.totalVat!.contains('.')) {
              tempTotalWithVat += double.parse(temp.totalVat!).toInt();
              tempTotalWithoutVat += double.parse(temp.totalVat!).toInt();
            } else {
              tempTotalWithVat += int.parse(temp.totalVat!);
              tempTotalWithoutVat += int.parse(temp.totalVat!);
            }

            final price = temp;
            price.industryName = industryList!
                .where((element) => element.id.toString() == temp.jobIndustry)
                .first
                .name;
            tempListPriceMinbolig.add(price);
          }

          tempListPrices.add(ListPrice(
              productTypeId: industry,
              productName: wizardsTexts
                  .where((element) => element.producttypeid == industry)
                  .first
                  .producttypeDa,
              productNameEn: wizardsTexts
                  .where((element) => element.producttypeid == industry)
                  .first
                  .producttypeDa,
              listPricesMinbolig: [...tempListPriceMinbolig]));
        }
      }

      var totalFessResponse =
          await contractService.getTotalFees(totalWithVat: tempTotalWithVat);

      var contractResponse = await contractService.getContractInfoV2(
          contractValue: tempTotalWithVat,
          offerId: offerId,
          projectId: projectId,
          wholeEnterprise: wholeEnterprise,
          homeownerId: homeownerId);

      if (contractResponse != null) {
        final tempContractTemplate = ContractTemplateModel.fromJson(
            (contractResponse.data as Map<String, dynamic>)['contract']);

        for (final item
            in (tempContractTemplate.steps as Map<String, dynamic>).values) {
          final step = TemplateSteps.fromJson(item as Map<String, dynamic>);
          for (final element in step.fields!) {
            if (element!.field == 2 ||
                element.field == 8 ||
                element.field == 10) {
              textEditingControllers.putIfAbsent(
                  '${step.key}_${element.key}_${element.fieldName}',
                  () => TextEditingController(text: element.defaultValue));
            } else if (element.field == 3 ||
                element.field == 4 ||
                element.field == 7) {
              radioControllers.putIfAbsent(
                  '${step.key}_${element.key}_${element.defaultType}_${element.fieldName}',
                  () => element.defaultValue);
            }
          }
        }

        _totalStep =
            (tempContractTemplate.steps as Map<String, dynamic>).values.length +
                1;
        _contractTemplateModel = tempContractTemplate;

        if (isContractUpdate) {
          final contractResponse = await contractService.getContractInfoOnly(
              contractId: contractId!);

          final responseData = contractResponse.data as Map<String, dynamic>;
          final dataDocument = responseData['DOCUMENTS'] as List<dynamic>;
          for (int i = 0; i < dataDocument.length; i++) {
            final documenJsonData = dataDocument[i]['JSON_DATA'];
            final contactId = documenJsonData['contactId'];
            final totalPriceWithVat = documenJsonData['totalpricewithvat'];
            final totalPriceWithVatAndFee =
                documenJsonData['totalpricewithvatandfee'];
            if (contactId != null && contactId.runtimeType == String) {
              dataDocument[i]['JSON_DATA']['contactId'] =
                  int.tryParse(contactId);
            }
            if (totalPriceWithVat != null &&
                totalPriceWithVat.runtimeType == String) {
              dataDocument[i]['JSON_DATA']['totalpricewithvat'] =
                  int.tryParse(totalPriceWithVat);
            }
            if (totalPriceWithVatAndFee != null &&
                totalPriceWithVatAndFee.runtimeType == String) {
              dataDocument[i]['JSON_DATA']['totalpricewithvatandfee'] =
                  int.tryParse(totalPriceWithVatAndFee);
            }
          }
          // if (responseData['INSURANCE'].runtimeType != String) {
          //   responseData['INSURANCE'] = responseData['INSURANCE'].toString();
          // }
          _contract = ContractModel.fromJson(responseData);
        }

        _listPrices = tempListPrices;
        _totalWithVat = tempTotalWithVat +
                (totalFessResponse.data as Map<String, dynamic>)['FEEINAMOUNT']
            as int;
        _totalVatWithoutFee = tempTotalWithoutVat;
      }

      notifyListeners();

      setBusy(false);
    } catch (e) {
      setBusy(false);
      log("getContractInfoV2 $e");
      if (e is DioException) log(": ${e.response!.data}");
    }
  }

  Future<void> getContractById({required int contractId}) async {
    setBusy(true);
    var service = context.read<ContractService>();
    final response = await service.getContractInfoOnly(contractId: contractId);

    bool? isPdfGenerated;

    final responseData = response.data as Map<String, dynamic>;
    final dataDocument = responseData['DOCUMENTS'] as List<dynamic>;
    for (int i = 0; i < dataDocument.length; i++) {
      final documenJsonData = dataDocument[i]['JSON_DATA'];
      final contactId = documenJsonData['contactId'];
      final totalPriceWithVat = documenJsonData['totalpricewithvat'];
      final totalPriceWithVatAndFee =
          documenJsonData['totalpricewithvatandfee'];
      if (contactId != null && contactId.runtimeType == String) {
        dataDocument[i]['JSON_DATA']['contactId'] = int.tryParse(contactId);
      }
      if (totalPriceWithVat != null &&
          totalPriceWithVat.runtimeType == String) {
        dataDocument[i]['JSON_DATA']['totalpricewithvat'] =
            int.tryParse(totalPriceWithVat);
      }
      if (totalPriceWithVatAndFee != null &&
          totalPriceWithVatAndFee.runtimeType == String) {
        dataDocument[i]['JSON_DATA']['totalpricewithvatandfee'] =
            int.tryParse(totalPriceWithVatAndFee);
      }
    }
    // if (responseData['INSURANCE'].runtimeType != String) {
    //   responseData['INSURANCE'] = responseData['INSURANCE'].toString();
    // }
    _contract = ContractModel.fromJson(responseData);

    while (!(isPdfGenerated ?? false)) {
      try {
        final response = await service.getCraftmanContractPDFDownloadT(
            contractId: contractId);

        isPdfGenerated = response.success ?? false;
      } catch (e) {
        if (e is ApiException) {
          if (e.message == 'PDF file not found on server') {
            await service.generateCraftmanContractHTMLT(contractId: contractId);
          }
        }
      }

      await Future.delayed(const Duration(seconds: 2));
    }

    _contractHeaders = await service.returnHeaders();

    notifyListeners();

    setBusy(false);
  }

  Future<bool?> contractSendMessage({
    required int contactId,
    required String title,
    required String message,
    required List<File> files,
  }) async {
    try {
      setBusy(true);
      final messageService = context.read<MessageService>();
      final service = context.read<ContractService>();

      final messagesResponse = await messageService.getProjectMessages(
          projectId: _contract!.project!.id!);
      final messages =
          ProjectMessageModel.fromCollection(messagesResponse.data);

      final List<ProjectMessageModel> tempMessages = [];

      for (final message in messages) {
        if (message.action != null) {
          if (message.action!.items!.isNotEmpty) {
            for (final action in message.action!.items!) {
              if (action.action == 'edit_contract') {
                tempMessages.add(message);
                break;
              }
            }
          }
        }
        if (tempMessages.isNotEmpty) break;
      }

      if (tempMessages.isEmpty) {
        setBusy(false);
        return false;
      }

      final response = await service.contractSendMessage(
          contactId: contactId,
          title: title,
          message: message,
          files: files,
          threadId: tempMessages.first.threadId!);

      setBusy(false);
      return response.success;
    } catch (e) {
      setBusy(false);
      log("contractSendMessage $e");
      if (e is DioException) log("contractSendMessage : ${e.response!.data}");
      return false;
    }
  }

  Future<bool?> sendContractSignature(
      {required int contractId, required String signaturePng}) async {
    try {
      setBusy(true);

      var service = context.read<ContractService>();

      var response = await service.sendContractSignature(
          contractId: contractId, signaturePng: signaturePng);

      final contractResponse =
          await service.getContractInfoOnly(contractId: contractId);

      final htmlResponse =
          await service.getContractHtmlPreview(contractId: contractId);

      final responseData = contractResponse.data as Map<String, dynamic>;
      final dataDocument = responseData['DOCUMENTS'] as List<dynamic>;
      for (int i = 0; i < dataDocument.length; i++) {
        final documenJsonData = dataDocument[i]['JSON_DATA'];
        final contactId = documenJsonData['contactId'];
        final totalPriceWithVat = documenJsonData['totalpricewithvat'];
        final totalPriceWithVatAndFee =
            documenJsonData['totalpricewithvatandfee'];
        if (contactId != null && contactId.runtimeType == String) {
          dataDocument[i]['JSON_DATA']['contactId'] = int.tryParse(contactId);
        }
        if (totalPriceWithVat != null &&
            totalPriceWithVat.runtimeType == String) {
          dataDocument[i]['JSON_DATA']['totalpricewithvat'] =
              int.tryParse(totalPriceWithVat);
        }
        if (totalPriceWithVatAndFee != null &&
            totalPriceWithVatAndFee.runtimeType == String) {
          dataDocument[i]['JSON_DATA']['totalpricewithvatandfee'] =
              int.tryParse(totalPriceWithVatAndFee);
        }
      }
      // if (responseData['INSURANCE'].runtimeType != String) {
      //   responseData['INSURANCE'] = responseData['INSURANCE'].toString();
      // }
      _contract = ContractModel.fromJson(responseData);

      if (htmlResponse.data != null) {
        final output = await getTemporaryDirectory();

        final generatedPdfFile = await HtmlToPdf.convertFromHtmlContent(
          htmlContent: htmlResponse.data,
          printPdfConfiguration: PrintPdfConfiguration(
              targetDirectory: output.path, targetName: "contract"),
        );

        _contractPdf = generatedPdfFile.path;
      }

      return response.success;
    } catch (e) {
      log("sendContractSignature catch $e");
      if (e is DioException) {
        log("sendContractSignature DioException: ${e.response!.data}");
      }

      return false;
    }
  }
}
