import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/model/offer_model.dart';
import 'package:Haandvaerker.dk/model/wizard/list_price_model.dart';
import 'package:Haandvaerker.dk/services/client/client_service.dart';
import 'package:Haandvaerker.dk/services/offer/offer_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EditOfferViewmodel extends BaseViewModel {
  EditOfferViewmodel({required this.context});

  final BuildContext context;

  OfferModel? _viewedOffer;
  OfferModel? get viewedOffer => _viewedOffer;

  PartnerJobModel _activeProject = PartnerJobModel();
  PartnerJobModel get activeProject => _activeProject;

  List<ListPriceMinbolig> _viewedOfferListPrice = [];
  List<ListPriceMinbolig> get viewedOfferListPrice => _viewedOfferListPrice;

  Future<PartnerJobModel?> getJobId() async {
    final clientService = context.read<ClientService>();

    final paginatedJobs = await clientService.getPartnerJobsV2(
        queryParameters: {"projectId": viewedOffer!.projectId!});

    if ((paginatedJobs?.data ?? []).isNotEmpty) {
      return paginatedJobs?.data!.first;
    }
    return null;
  }

  Future<void> getOfferId({required int offerId}) async {
    final service = context.read<OfferService>();

    setBusy(true);

    final response = await service.getOfferById(offerId: offerId);

    if (response != null) {
      _viewedOffer = OfferModel.fromJson(response.data);

      final listPriceResponse = await service.getListPriceMinboligOfferVersion(
          contactId: viewedOffer?.homeowner?.id ?? 0,
          offerVersionId: 0,
          projectId: viewedOffer?.contractType == "Fagentreprise"
              ? (viewedOffer?.parentProjectId ?? 0)
              : (viewedOffer?.projectId ?? 0));
      final activeProject = await getJobId();

      if (listPriceResponse != null) {
        _viewedOfferListPrice = [
          ...ListPriceMinbolig.fromCollection(
                  (listPriceResponse.data as Map<String, dynamic>)['result'])
              .where((element) => int.parse(element.offerId ?? '0') == offerId)
        ];
      }
      if (activeProject != null) {
        _activeProject = activeProject;
      }
    }

    setBusy(false);

    notifyListeners();
  }

  Future<bool> acceptOfferVersion() async {
    final service = context.read<OfferService>();

    final response = await service.acceptOfferVersion(
        offerId: viewedOffer!.id!, projectId: viewedOffer!.projectId!);

    return (response?.success ?? false);
  }

  void reset() {
    _viewedOffer = null;
    _viewedOfferListPrice = [];
    _activeProject = PartnerJobModel();

    notifyListeners();
  }
}
