import 'package:Haandvaerker.dk/model/facebook_page_model/facebook_page_model.dart';
import 'package:Haandvaerker.dk/services/facebook/facebook_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FacebookViewModel extends BaseViewModel {
  FacebookViewModel({required this.context});

  final BuildContext context;

  bool enableChecker = false;

  bool _isIntegrated = false;
  bool get isIntegrated => enableChecker ? _isIntegrated : true;

  set isIntegrated(bool value) {
    _isIntegrated = value;
    notifyListeners();
  }

  List<FacebookPageModel> _facebookPages = [];
  List<FacebookPageModel> get facebookPages => _facebookPages;

  void reset() {
    _isIntegrated = false;
    _facebookPages = [];

    notifyListeners();
  }

  Future<void> getFacebookPage() async {
    final service = context.read<FacebookService>();

    final response = await service.getPartnerFacebookPages();

    if (response != null) {
      _facebookPages = FacebookPageModel.fromCollection(response.data);
    }

    notifyListeners();
  }

  Future<void> activateDeactivatePost(
      {required bool isActivate, required int id}) async {
    final service = context.read<FacebookService>();

    await service.activateDeactivatePost(isActivate: isActivate, id: id);

    await getFacebookPage();
  }

  Future<void> deletePage({required int id}) async {
    final service = context.read<FacebookService>();

    await service.deleteFacebookPages(id: id);

    await getFacebookPage();
  }

  Future<void> facebookRelogin() async {
    final service = context.read<FacebookService>();

    await service.facebookRelogin();
  }
}
