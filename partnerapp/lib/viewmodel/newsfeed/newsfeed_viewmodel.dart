import 'package:Haandvaerker.dk/model/dynamic_story_model.dart';
import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/latest_job_feed_model.dart';
import 'package:Haandvaerker.dk/model/newsfeed_model.dart';
import 'package:Haandvaerker.dk/model/onboarding_data_model.dart';
import 'package:Haandvaerker.dk/services/main/main_service.dart';
import 'package:Haandvaerker.dk/ui/screens/dynamic_story_popup/dynamic_story_popup_dialog.dart';
import 'package:Haandvaerker.dk/utils/extensions.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:story_view/story_view.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class NewsFeedViewModel extends BaseViewModel {
  NewsFeedViewModel({required this.context});
  final BuildContext context;

  NewsFeedResponseModel? _newsFeed;
  NewsFeedResponseModel? get newsFeed => _newsFeed;

  DynamicStoryModel? _dynamicStory;
  DynamicStoryModel? get dynamicStory => _dynamicStory;

  OnboardingDataModel? _onboardingData;
  OnboardingDataModel? get onboardingData => _onboardingData;

  List<LatestJobFeedModel>? _latestJobFeed;
  List<LatestJobFeedModel>? get latestJobFeed => _latestJobFeed;

  List<LatestJobFeedModel> _latestJobFeedForPopup = [];
  List<LatestJobFeedModel> get latestJobFeedForPopup => _latestJobFeedForPopup;

  List<StoryItemsModel> _storyItemsList = <StoryItemsModel>[];
  List<StoryItemsModel> get storyItemsList => _storyItemsList;

  List<BreakDownRating> _breakdown = <BreakDownRating>[];
  List<BreakDownRating> get breakdown => _breakdown;

  List<ProjectNewsFeedView> _displayProjects = <ProjectNewsFeedView>[];
  List<ProjectNewsFeedView> get displayProjects => _displayProjects;

  List<ChartData> _overAllChartData = <ChartData>[];
  List<ChartData> get overAllChartData => _overAllChartData;

  List<StoryItem> _storyItems = <StoryItem>[];
  List<StoryItem> get storyItems => _storyItems;

  List<StoryItem> _onboardingVideoItems = <StoryItem>[];
  List<StoryItem> get onboardingVideoItems => _onboardingVideoItems;

  List<bool> _isUnlockedList = <bool>[];
  List<bool> get isUnlockedList => _isUnlockedList;

  String _redirectUrl = '';
  String get redirectUrl => _redirectUrl;

  String _storyPage = '';
  String get storyPage => _storyPage;

  int _totalClicks = 0;
  int get totalClicks => _totalClicks;

  int _totalViews = 0;
  int get totalViews => _totalViews;

  bool _afterInitNewsFeed = false;
  bool get afterInitNewsFeed => _afterInitNewsFeed;

  bool _isDone = false;
  bool get isDone => _isDone;

  bool _isUnlocked = false;
  bool get isUnlocked => _isUnlocked;

  int _membershipBannerWatchCount = 0;
  int get membershipBannerWatchCount => _membershipBannerWatchCount;

  set membershipBannerWatchCount(int value) {
    _membershipBannerWatchCount = value;

    notifyListeners();
  }

  void reset() {
    _membershipBannerWatchCount = 0;
    _newsFeed = null;
    _dynamicStory = null;
    _onboardingData = null;
    _latestJobFeed = [];
    _storyItemsList = [];
    _breakdown = [];
    _displayProjects = [];
    _overAllChartData = [];
    _storyItems = [];
    _onboardingVideoItems = [];
    _isUnlockedList = [];
    _redirectUrl = '';
    _storyPage = '';
    _totalClicks = 0;
    _totalViews = 0;
    _afterInitNewsFeed = false;
    _isDone = false;
    _isUnlocked = false;

    notifyListeners();
  }

  set storyItemsList(List<StoryItemsModel> storyItemsList) {
    _storyItemsList = storyItemsList;
    notifyListeners();
  }

  set overAllChartData(List<ChartData> data) {
    _overAllChartData = data;
    notifyListeners();
  }

  set isUnlockedList(List<bool> val) {
    _isUnlockedList = val;
    notifyListeners();
  }

  set redirectUrl(String url) {
    _redirectUrl = url;
    notifyListeners();
  }

  set storyPage(String page) {
    _storyPage = page;
    notifyListeners();
  }

  set totalClicks(int clicks) {
    _totalClicks = clicks;
    notifyListeners();
  }

  set totalViews(int views) {
    _totalViews = views;
    notifyListeners();
  }

  set afterInitNewsFeed(bool val) {
    _afterInitNewsFeed = val;
    notifyListeners();
  }

  set isDone(bool val) {
    _isDone = val;
    notifyListeners();
  }

  set isUnlocked(bool val) {
    _isUnlocked = val;
    notifyListeners();
  }

  Future<void> videoBannerTracking({required String type}) async {
    final mainService = context.read<MainService>();

    await mainService.videoBannerTracking(type: type);
  }

  Future<void> getLatestJobFeed() async {
    final mainService = context.read<MainService>();

    final responses = await Future.wait([
      mainService.getLatestJobfeed(),
      mainService.getLatestJobfeedForPopup()
    ]);

    _latestJobFeed = responses[0];
    _latestJobFeedForPopup = responses[1] ?? [];
  }

  Future<void> getNewsFeed() async {
    final mainService = context.read<MainService>();

    //WE GET THE NEWSFEED DATA HERE
    _newsFeed = await mainService.getNewsfeed();

    //GET BREAKDOWN DATA
    getBreakdown();
    getChartData();

    //GET PROJECT DATA TO BE DISPLAYED
    if (_newsFeed!.projects != null) {
      if (_newsFeed!.projects!.isNotEmpty) {
        getVisibleProjects();
      }
    }
  }

  Future<void> getDynamicStory({required String page}) async {
    _dynamicStory = null;
    _storyItemsList = <StoryItemsModel>[];

    final mainService = context.read<MainService>();
    if (page.isNotEmpty) {
      _storyPage = page;
      final response = await mainService.getDynamicStory(page: page);

      if (response != null &&
          response.success == true &&
          response.data != null) {
        if (response.data.runtimeType == List<dynamic>) {
          if (response.data.isNotEmpty) {
            _dynamicStory = DynamicStoryModel.fromJson(response.data.first);
          } else {
            _dynamicStory = null;
          }
        } else {
          _dynamicStory = DynamicStoryModel.fromJson(response.data);
        }
        _storyItemsList = _dynamicStory?.storyItems ?? <StoryItemsModel>[];
        if (_storyItemsList.isNotEmpty) {
          _storyItemsList.removeWhere((element) =>
              ((element.title ?? '').isEmpty &&
                  (element.descriptionText ?? '').isEmpty &&
                  (element.imageUrl ?? '').isEmpty &&
                  (element.videoUrl ?? '').isEmpty));
        }
      } else if (response != null &&
          response.success == false &&
          response.data != null) {
        _dynamicStory = null;
        _storyItemsList = <StoryItemsModel>[];
      }
    }

    notifyListeners();
  }

  Future<void> markDynamicPopupSeen(
      {required int? popupLogId,
      required int maxShow,
      required seenCount}) async {
    final mainService = context.read<MainService>();
    if (popupLogId != null) {
      if (maxShow > seenCount) {
        await mainService.markDynamicPopupSeen(popupLogId: popupLogId);
      }
    }
  }

  Future<void> getOnboardingData() async {
    final mainService = context.read<MainService>();
    final response = await mainService.getOnboardingData();

    if (response != null) {
      if (response.data != null) {
        _onboardingData = OnboardingDataModel.fromJson(response.data);

        _isUnlockedList = <bool>[];
        for (int i = 0; i < (onboardingData?.steps ?? []).length; i++) {
          if (i == 0) {
            _isUnlockedList.add(true);
          } else {
            if ((onboardingData?.steps ?? [])[i - 1].done == true) {
              _isUnlockedList.add(true);
            } else if ((onboardingData?.steps ?? [])[i].done == true) {
              _isUnlockedList.add(true);
            } else {
              _isUnlockedList.add(false);
            }
          }
        }
      }
    }

    notifyListeners();
  }

  Future<void> initNewsfeed() async {
    final mainService = context.read<MainService>();

    final responses = await Future.wait([
      mainService.getNewsfeed(),
      mainService.getLatestJobfeed(),
      mainService.getOnboardingData(),
      mainService.getLatestJobfeedForPopup(),
    ]);
    if (responses[0] != null) {
      _newsFeed = responses[0] as NewsFeedResponseModel?;

      if (_newsFeed!.projects != null) {
        if (_newsFeed!.projects!.isNotEmpty) {
          getVisibleProjects();
        }
      }
    }
    if (responses[1] != null) {
      _latestJobFeed = (responses[1] as List<LatestJobFeedModel>?);
    }

    getBreakdown();
    getChartData();

    if ((responses[2] as MinboligApiResponse?) != null) {
      if ((responses[2] as MinboligApiResponse?)?.data != null) {
        _onboardingData = OnboardingDataModel.fromJson(
            (responses[2] as MinboligApiResponse?)?.data);

        _isUnlockedList = <bool>[];
        for (int i = 0; i < (onboardingData?.steps ?? []).length; i++) {
          if (i == 0) {
            _isUnlockedList.add(true);
          } else {
            if ((onboardingData?.steps ?? [])[i - 1].done == true) {
              _isUnlockedList.add(true);
            } else if ((onboardingData?.steps ?? [])[i].done == true) {
              _isUnlockedList.add(true);
            } else {
              _isUnlockedList.add(false);
            }
          }
        }
      }
    }

    if (responses[3] != null) {
      _latestJobFeedForPopup =
          (responses[3] as List<LatestJobFeedModel>?) ?? [];
    }

    notifyListeners();
  }

  Future<void> getNewsAndLatestJobFeedInitial() async {
    final mainService = context.read<MainService>();
    final responses = await Future.wait([
      mainService.getNewsfeed(),
      mainService.getLatestJobfeed(),
      mainService.getLatestJobfeedForPopup(),
    ]);

    if (responses.isNotEmpty) {
      if (responses[0] != null) {
        _newsFeed = responses[0] as NewsFeedResponseModel?;

        if (_newsFeed!.projects != null) {
          if (_newsFeed!.projects!.isNotEmpty) {
            getVisibleProjects();
          }
        }
      }
      if (responses[1] != null) {
        _latestJobFeed = responses[1] as List<LatestJobFeedModel>?;
      }

      getBreakdown();
      getChartData();
    }

    _latestJobFeedForPopup = (responses[2] as List<LatestJobFeedModel>?) ?? [];

    notifyListeners();
  }

  Future<void> dynamicStoryFunction(
      {required String dynamicStoryPage,
      required bool dynamicStoryEnable,
      required BuildContext pageContext}) async {
    final dynamicStoryMaxShow = dynamicStory?.maxShow ?? 0;
    final dynamicStorySeenCount = dynamicStory?.seenCount ?? 0;

    if (showStoryPopup(dynamicStoryEnable: dynamicStoryEnable)) {
      Future.delayed(
          Duration(milliseconds: ((dynamicStory?.delay ?? 2) * 1000)), () {
        if (pageContext.mounted && dynamicStoryPage == storyPage) {
          dynamicStoryPopupDialog(context: pageContext).whenComplete(() async {
            if (!_afterInitNewsFeed) {
              _afterInitNewsFeed = true;
            }
            markDynamicPopupSeen(
                popupLogId: dynamicStory?.popupLogId ?? 0,
                maxShow: dynamicStoryMaxShow,
                seenCount: dynamicStorySeenCount);
          });
        }
      });
    }
  }

  bool showStoryPopup({required bool dynamicStoryEnable}) {
    final storyDeviceBool =
        (_dynamicStory?.device ?? '').toLowerCase().contains('mobile');
    final storyItemsLength = _storyItemsList.length;
    final storyMaxShow = _dynamicStory?.maxShow ?? 0;
    final storySeenCount = _dynamicStory?.seenCount ?? 0;
    // final storyEnableClose = _dynamicStory?.enableClose ?? 0;

    // Check Dynamic story is NOT null, is ENABLED, is for MOBILE, and is NOT EMPTY
    if (_dynamicStory != null &&
        dynamicStoryEnable &&
        storyDeviceBool &&
        storyItemsLength != 0) {
      // if maxShow is 0, it can be seen without limit
      if (storyMaxShow == 0) {
        // // if enableClose is 1, it can be closed but shown again
        // if (storyEnableClose == 1) {
        //   return true;
        //   // if enableClose is 0 and seenCount is 0, it can be shown once only
        // } else if (storyEnableClose == 0 && storySeenCount == 0) {
        //   return true;
        // } else {
        //   return false;
        // }
        return true;
      } else {
        // if maxShow is greater than seenCount, it can be shown
        if (storyMaxShow > storySeenCount) {
          // // if enableClose is 1, it can be closed but shown again
          // if (storyEnableClose == 1) {
          //   return true;
          //   // if enableClose is 0 and seenCount is 0, it can be shown once only
          // } else if (storyEnableClose == 0 && storySeenCount == 0) {
          //   return true;
          // } else {
          //   return false;
          // }
          return true;
        } else {
          return false;
        }
      }
    } else {
      return false;
    }
  }

  StoryItem setOnboardingVideoList({
    required YoutubePlayerController videoController,
    required StoryController storyController,
    required String videoUrl,
    required int duration,
    required int stepIndex,
  }) {
    if (videoUrl.contains('youtube') || videoUrl.contains('youtu.be')) {
      videoController = YoutubePlayerController(
        initialVideoId: YoutubePlayer.convertUrlToId(videoUrl) ?? '',
        flags: const YoutubePlayerFlags(
          loop: true,
        ),
      );

      return StoryItem(
        YoutubeStoryVideoLoader(
          videoController: videoController,
          storyController: storyController,
          controllerId: stepIndex.toString(),
        ),
        duration: Duration(seconds: duration),
      );
    } else {
      return StoryItem.pageVideo(videoUrl,
          controller: storyController,
          imageFit: BoxFit.contain,
          duration: Duration(seconds: duration));
    }
  }

  void setStoryItems(
      {required List<StoryItemsModel> storyList,
      required YoutubePlayerController videoController,
      required StoryController storyController}) {
    _storyItems = storyList.mapIndexed((index, storyItem) {
      if ((storyItem.title ?? '').isNotEmpty &&
          (storyItem.descriptionText ?? '').isNotEmpty) {
        if ((storyItem.videoUrl ?? '').isNotEmpty) {
          if ((storyItem.videoUrl ?? '').contains('youtube') ||
              (storyItem.videoUrl ?? '').contains('youtu.be')) {
            videoController = YoutubePlayerController(
              initialVideoId:
                  YoutubePlayer.convertUrlToId(storyItem.videoUrl ?? '') ?? '',
            );
            StoryItem item = StoryItem(
              YoutubeStoryVideoLoader(
                videoController: videoController,
                storyController: storyController,
                controllerId: (storyItem.id ?? index).toString(),
                storyTitle: storyItem.title,
                storyDescription: storyItem.descriptionText,
              ),
              duration: (storyItem.delay ?? 0) == 0
                  ? const Duration(seconds: 10)
                  : Duration(seconds: (storyItem.delay ?? 0) + 3),
            );

            return item;
          } else {
            StoryItem item = StoryItem.pageVideo(
              storyItem.videoUrl ?? '',
              controller: storyController,
              imageFit: BoxFit.contain,
              duration: (storyItem.delay ?? 0) == 0
                  ? const Duration(seconds: 10)
                  : Duration(seconds: (storyItem.delay ?? 0) + 3),
            );

            return item;
          }
        } else if ((storyItem.imageUrl ?? '').isNotEmpty) {
          StoryItem item = StoryItem(
            ImageStoryItem(
              imageUrl: storyItem.imageUrl ?? '',
              storyController: storyController,
              storyTitle: storyItem.title,
              storyDescription: storyItem.descriptionText,
              redirectUrl: storyItem.redirectUrl ?? '',
              controllerId: (storyItem.id ?? index).toString(),
            ),
            duration: (storyItem.delay ?? 0) == 0
                ? const Duration(seconds: 5)
                : Duration(seconds: (storyItem.delay ?? 0) + 3),
          );

          return item;
        } else {
          StoryItem item = StoryItem(
            TextStoryItem(
              storyTitle: storyItem.title ?? '',
              storyDescription: storyItem.descriptionText ?? '',
              controllerId: (storyItem.id ?? index).toString(),
            ),
            duration: (storyItem.delay ?? 0) == 0
                ? const Duration(seconds: 5)
                : Duration(seconds: (storyItem.delay ?? 0) + 3),
          );

          return item;
        }
      } else {
        if ((storyItem.videoUrl ?? '').isNotEmpty) {
          if ((storyItem.videoUrl ?? '').contains('youtube') ||
              (storyItem.videoUrl ?? '').contains('youtu.be')) {
            videoController = YoutubePlayerController(
              initialVideoId:
                  YoutubePlayer.convertUrlToId(storyItem.videoUrl ?? '') ?? '',
            );
            StoryItem item = StoryItem(
              YoutubeStoryVideoLoader(
                videoController: videoController,
                storyController: storyController,
                controllerId: (storyItem.id ?? index).toString(),
              ),
              duration: (storyItem.delay ?? 0) == 0
                  ? const Duration(seconds: 10)
                  : Duration(seconds: (storyItem.delay ?? 0) + 3),
            );

            return item;
          } else {
            StoryItem item = StoryItem.pageVideo(
              storyItem.videoUrl ?? '',
              controller: storyController,
              imageFit: BoxFit.contain,
              duration: (storyItem.delay ?? 0) == 0
                  ? const Duration(seconds: 5)
                  : Duration(seconds: (storyItem.delay ?? 0) + 3),
            );

            return item;
          }
        } else {
          StoryItem item = StoryItem(
            Stack(children: <Widget>[
              ImageStoryItem(
                imageUrl: storyItem.imageUrl ?? '',
                storyController: storyController,
                storyTitle: storyItem.title,
                storyDescription: storyItem.descriptionText,
                redirectUrl: storyItem.redirectUrl ?? '',
                controllerId: (storyItem.id ?? index).toString(),
              ),
            ]),
            duration: (storyItem.delay ?? 0) == 0
                ? const Duration(seconds: 5)
                : Duration(seconds: (storyItem.delay ?? 0) + 3),
          );
          return item;
        }
      }
    }).toList();
  }

  void getChartData() {
    //

    List<ChartData> allData = <ChartData>[];
    List<ViewData> viewData = <ViewData>[];
    List<ClickData> clickData = <ClickData>[];

    if (_newsFeed != null) {
      viewData = _newsFeed?.logs?.profileViews?.data
              ?.map(
                (e) => ViewData(
                  '${e.date!.split('-')[1]}-${e.date!.split('-').last}',
                  e.total!.toDouble(),
                ),
              )
              .toList() ??
          [];

      List<ClickData> projectViews = <ClickData>[];
      List<DateTime> dates = <DateTime>[];
      int totalData = 0;

      for (final views
          in _newsFeed?.logs?.projectViews ?? <ProjectViewsData>[]) {
        totalData += views.total ?? 0;
        for (final view in views.days!) {
          if (!dates.contains(DateTime.parse(view.date!))) {
            dates.add(DateTime.parse(view.date!));
          }
          final xlabel =
              '${view.date!.split('-')[1]}-${view.date!.split('-').last}';

          projectViews.add(ClickData(xlabel, view.total!.toDouble()));
        }
      }

      totalClicks = totalData;
      totalViews = _newsFeed?.logs?.profileViews?.total ?? 0;

      List<ClickData> cleanedProjectView = <ClickData>[];
      dates.sort((a, b) => b.compareTo(a));
      final List<String> dateStrings = dates
          .map((e) =>
              Formatter.formatDateTime(type: DateFormatType.isoDate, date: e))
          .toList();
      List<String> sortedQuarters = <String>[];

      for (final dateString in dateStrings) {
        sortedQuarters
            .add("${dateString.split('-')[1]}-${dateString.split('-').last}");
      }

      for (final quarter in sortedQuarters) {
        double total = 0;
        for (final data in projectViews
            .where((element) => element.quarter == quarter)
            .toList()) {
          total += data.clicks;
        }

        cleanedProjectView.add(ClickData(quarter, total));
      }

      clickData = cleanedProjectView;

      List<String> allQuarters = <String>[];
      List<String> temp = <String>[];

      temp = <String>[
        ...clickData.map((e) => e.quarter),
        ...viewData.map((e) => e.quarter)
      ];
      temp.toSet();
      temp.sort((a, b) => b.compareTo(a));

      for (final quarter in temp.toSet()) {
        allQuarters.add(quarter);
      }

      for (final quarter in allQuarters) {
        if (clickData.any((element) => element.quarter == quarter) &&
            viewData.any((element) => element.quarter == quarter)) {
          allData.add(
            ChartData(
              quarter: quarter,
              clicks: clickData
                  .firstWhere(
                    (element) => element.quarter == quarter,
                    orElse: () => ClickData.defaults(),
                  )
                  .clicks,
              views: viewData
                  .firstWhere(
                    (element) => element.quarter == quarter,
                    orElse: () => ViewData.defaults(),
                  )
                  .views,
            ),
          );
        } else {
          if (clickData.any((element) => element.quarter == quarter)) {
            allData.add(
              ChartData(
                quarter: quarter,
                clicks: clickData
                    .firstWhere(
                      (element) => element.quarter == quarter,
                      orElse: () => ClickData.defaults(),
                    )
                    .clicks,
                views: 0,
              ),
            );
          } else {
            allData.add(
              ChartData(
                quarter: quarter,
                clicks: 0,
                views: viewData
                    .firstWhere(
                      (element) => element.quarter == quarter,
                      orElse: () => ViewData.defaults(),
                    )
                    .views,
              ),
            );
          }
        }
      }

      overAllChartData = allData;
    }
  }

  void getBreakdown() {
    List<BreakDownRating> tempBreakdown = <BreakDownRating>[];

    if (_newsFeed!.reviewStats != null) {
      final reviewStats = _newsFeed!.reviewStats!;

      tempBreakdown.addAll([
        BreakDownRating(
          description: 'agreement_rating_desc',
          rating: reviewStats.agreementRating!,
        ),
        BreakDownRating(
          description: 'work_rating_desc',
          rating: reviewStats.workRating!,
        ),
        BreakDownRating(
          description: 'price_rating_desc',
          rating: reviewStats.priceRating!,
        ),
        BreakDownRating(
          description: 'cleaning_rating_desc',
          rating: reviewStats.cleaningRating!,
        ),
        BreakDownRating(
          description: 'contact_rating_desc',
          rating: reviewStats.contactRating!,
        ),
        BreakDownRating(
          description: 'recommend_company_rating_desc',
          rating: reviewStats.recommendCompanyRating!,
        ),
      ]);

      _breakdown = tempBreakdown;
    }
    notifyListeners();
  }

  void getVisibleProjects() {
    List<ProjectNewsFeedView> temp = <ProjectNewsFeedView>[];

    final projectViews = _newsFeed?.logs?.projectViews ?? [];
    final projects = _newsFeed!.projects!
        .where((element) => element.stage != 'deleted')
        .toList();

    final projectsWitViews = projects
        .where((element) => projectViews
            .map((e) => e.itemId)
            .toList()
            .contains(element.id.toString()))
        .toList();
    final projectWithOutViews = projects
        .where((element) => !projectViews
            .map((e) => e.itemId)
            .toList()
            .contains(element.id.toString()))
        .toList();

    for (final withViews in projectsWitViews) {
      temp.add(
        ProjectNewsFeedView(
          title: withViews.title.isNullOrEmpty ? 'No Title' : withViews.title!,
          pictureURL: withViews.pictures!.isEmpty
              ? ""
              : withViews.pictures!.first.toString(),
          viewCount: projectViews
              .firstWhere(
                  (element) => element.itemId == withViews.id.toString(),
                  orElse: () => ProjectViewsData.defaults())
              .total!,
          projectURL: withViews.projectURL!,
        ),
      );
    }

    for (final withOutViews in projectWithOutViews) {
      temp.add(
        ProjectNewsFeedView(
          title: withOutViews.title.isNullOrEmpty
              ? 'No Title'
              : withOutViews.title!,
          pictureURL: withOutViews.pictures!.isEmpty
              ? ""
              : withOutViews.pictures!.first.toString(),
          viewCount: 0,
          projectURL: withOutViews.projectURL!,
        ),
      );
    }

    _displayProjects = temp;

    notifyListeners();
  }
}

class BreakDownRating {
  final String description;
  final int rating;

  BreakDownRating({required this.description, required this.rating});
}

class ProjectNewsFeedView {
  final String title;
  final String pictureURL;
  final int viewCount;
  final String projectURL;

  ProjectNewsFeedView({
    required this.title,
    required this.pictureURL,
    required this.viewCount,
    required this.projectURL,
  });
}

class ClickData {
  ClickData(this.quarter, this.clicks);

  final String quarter;
  final double clicks;

  ClickData.defaults()
      : quarter = '',
        clicks = 0;
}

class ViewData {
  ViewData(this.quarter, this.views);

  final String quarter;
  final double views;

  ViewData.defaults()
      : quarter = '',
        views = 0;
}

class ChartData {
  ChartData({
    required this.quarter,
    required this.clicks,
    required this.views,
  });
  final String quarter;
  final double clicks;
  final double views;

  ChartData.defaults()
      : quarter = '',
        clicks = 0,
        views = 0;
}
