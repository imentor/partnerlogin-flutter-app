import 'dart:developer';
import 'dart:io';
import 'dart:math' as math;

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/payment/payment_stages_model.dart';
import 'package:Haandvaerker.dk/model/projects/extra_work_response_model.dart';
import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/services/payproff/payproff_service.dart';
import 'package:Haandvaerker.dk/services/projects/extra_work/extra_work_service.dart';
import 'package:Haandvaerker.dk/services/projects/projects_service.dart';
import 'package:Haandvaerker.dk/services/request_payment/request_payment_service.dart';
import 'package:Haandvaerker.dk/services/wizard/wizard_service.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/iterable_utils.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

class WalletViewmodel extends BaseViewModel {
  WalletViewmodel({required this.context})
      : _requestPaymentService = context.read<RequestPaymentService>(),
        _wizardService = context.read<WizardService>(),
        _extraWorkService = context.read<ExtraWorkService>(),
        _payproffService = context.read<PayproffService>(),
        _projectService = context.read<ProjectsService>(),
        super();

  final BuildContext context;

  final RequestPaymentService _requestPaymentService;
  final WizardService _wizardService;
  final ExtraWorkService _extraWorkService;
  final PayproffService _payproffService;
  final ProjectsService _projectService;

  PartnerProjectsModel? _currentViewedProject;
  PartnerProjectsModel? get currentViewedProject => _currentViewedProject;

  PaymentStages? _currentViewedPaymentStage;
  PaymentStages? get currentViewedPaymentStage => _currentViewedPaymentStage;

  PaymentStages? _paymentStagesMain;
  PaymentStages? get paymentStagesMain => _paymentStagesMain;

  PaymentStages? _paymentStagesSubContractor;
  PaymentStages? get paymentStagesSubContractor => _paymentStagesSubContractor;

  PaymentStages? _paymentStagesProducent;
  PaymentStages? get paymentStagesProducent => _paymentStagesProducent;

  ExtraWorkResponseModel? _extraWork;
  ExtraWorkResponseModel? get extraWork => _extraWork;

  Stage? _currentViewedStage;
  Stage? get currentViewedStage => _currentViewedStage;

  Map<dynamic, dynamic> _stagesList = {};
  Map<dynamic, dynamic> get stagesList => _stagesList;

  List<FullIndustry> _fullIndustries = [];
  List<FullIndustry> get fullIndustries => _fullIndustries;

  String _paymentStageType = 'normal';
  String get paymentStageType => _paymentStageType;

  bool _isStashed = false;
  bool get isStashed => _isStashed;

  // bool _isFullDeposit = false;
  // bool get isFullDeposit => _isFullDeposit;

  set currentViewedProject(PartnerProjectsModel? value) {
    _currentViewedProject = value;

    notifyListeners();
  }

  set currentViewedStage(Stage? value) {
    _currentViewedStage = value;

    notifyListeners();
  }

  Future<void> getPaymentStageFromSubcontractor(
      {required PartnerProjectsModel project}) async {
    setBusy(true);

    double tempWallet = 0;
    double tempTotalBalance = 0;

    _paymentStageType = 'subcontractor';

    _currentViewedProject = project;

    final paymentResponse = await _requestPaymentService.paymentStages(
        projectId: project.id, type: 'subcontractor');

    if (paymentResponse != null) {
      _currentViewedPaymentStage = PaymentStages.fromJson(paymentResponse.data);

      List<Stage> stages = [];

      stages = [...(currentViewedPaymentStage?.stages ?? [])];

      List<Stage> extraWorkStages = [];

      final stageGroup = stages.groupBy((value) => value.contractId);

      Map<String, Map<String, List<Stage>>> paymentStageMap = {};

      for (final i in stageGroup.entries) {
        Map<String, List<Stage>> stagesMap = {};

        for (var industryMap
            in i.value.groupBy((value) => value.jobIndustryId).entries) {
          final jobIndustryName = fullIndustries.firstWhere(
            (value) => value.brancheId == industryMap.key,
            orElse: () => FullIndustry.defaults(),
          );

          stagesMap[jobIndustryName.brancheDa ?? ''] = [...industryMap.value];
        }

        for (var element in stagesMap.entries) {
          tempWallet += (element.value
              .fold(0, (sum, stage) => sum + (stage.priceDeducted ?? 0)));
          tempTotalBalance += (element.value
              .where((value) => value.type == 'normal')
              .fold(
                  0,
                  (sum, stage) =>
                      sum +
                      ((stage.priceDeducted ?? 0) -
                          (stage.totalAmountPaid ?? 0))));
        }

        paymentStageMap['${i.key}'] = {...stagesMap};
      }

      _stagesList = {
        "paymentStage": paymentStageMap,
        "extraWork": extraWorkStages,
        "totalPrice": tempWallet,
        "balance": tempTotalBalance,
      };
    }

    if ((currentViewedPaymentStage?.stages ?? []).isNotEmpty) {
      _isStashed =
          (currentViewedPaymentStage?.stages ?? []).first.stashed ?? false;
    }

    double totalVatPrice = (currentViewedPaymentStage?.totalAmountVat ?? 0) +
        (currentViewedPaymentStage?.totalAmountApprovedMinusExtraWork ?? 0);

    List<Stage> extraWork = [
      ...(currentViewedPaymentStage?.stages ?? []).where((value) =>
          value.contractType == 'extra_work' && value.type == 'normal')
    ];

    totalVatPrice +=
        extraWork.fold(0, (sum, stage) => sum + (stage.priceVat ?? 0));

    double paymentFromCustomerTotal = 0;

    if (!isStashed) {
      paymentFromCustomerTotal =
          currentViewedPaymentStage?.totalAmountStashed ?? 0;
    } else {
      paymentFromCustomerTotal = ((currentViewedPaymentStage?.totalAmountVat ??
                  0) +
              (currentViewedPaymentStage?.totalStashedExtraWorkAmount ?? 0)) -
          ((currentViewedPaymentStage?.totalAmountPaid ?? 0) +
              (currentViewedPaymentStage?.totalAmountPaidExtraWork ?? 0));
    }

    _stagesList.addAll({
      'totalVatPrice': totalVatPrice,
      'paymentFromCustomerTotal': paymentFromCustomerTotal
    });

    notifyListeners();

    setBusy(false);
  }

  Future<void> getPaymentStageFromManufacturerSubcontractor(
      {required String projectId,
      required String type,
      int? producentId}) async {
    setBusy(true);

    double tempWallet = 0;
    double tempTotalBalance = 0;

    _paymentStageType = type;

    final projectResponse =
        await _projectService.getProjectById(projectId: int.parse(projectId));

    if (projectResponse != null) {
      _currentViewedProject =
          PartnerProjectsModel.fromJson(projectResponse.data);

      final paymentResponse = await _requestPaymentService.paymentStages(
          projectId: projectId, type: type);

      if (paymentResponse != null) {
        _currentViewedPaymentStage =
            PaymentStages.fromJson(paymentResponse.data);

        List<Stage> stages = [];

        if (type == 'producent') {
          stages = [
            ...(currentViewedPaymentStage?.stages ?? [])
                .where((element) => element.producentId == producentId)
          ];
        } else {
          stages = [...(currentViewedPaymentStage?.stages ?? [])];
        }

        List<Stage> extraWorkStages = [];

        final stageGroup = stages.groupBy((value) => value.contractId);

        Map<String, Map<String, List<Stage>>> paymentStageMap = {};

        for (final i in stageGroup.entries) {
          Map<String, List<Stage>> stagesMap = {};

          for (var industryMap
              in i.value.groupBy((value) => value.jobIndustryId).entries) {
            final jobIndustryName = fullIndustries.firstWhere(
              (value) => value.brancheId == industryMap.key,
              orElse: () => FullIndustry.defaults(),
            );

            stagesMap[jobIndustryName.brancheDa ?? ''] = [...industryMap.value];
          }

          for (var element in stagesMap.entries) {
            tempWallet += (element.value
                .fold(0, (sum, stage) => sum + (stage.priceDeducted ?? 0)));
            tempTotalBalance += (element.value
                .where((value) => value.type == 'normal')
                .fold(
                    0,
                    (sum, stage) =>
                        sum +
                        ((stage.priceDeducted ?? 0) -
                            (stage.totalAmountPaid ?? 0))));
          }

          paymentStageMap['${i.key}'] = {...stagesMap};
        }

        _stagesList = {
          "paymentStage": paymentStageMap,
          "extraWork": extraWorkStages,
          "totalPrice": tempWallet,
          "balance": tempTotalBalance,
        };
      }
    }

    if ((currentViewedPaymentStage?.stages ?? []).isNotEmpty) {
      _isStashed =
          (currentViewedPaymentStage?.stages ?? []).first.stashed ?? false;
    }

    double totalVatPrice = (currentViewedPaymentStage?.totalAmountVat ?? 0) +
        (currentViewedPaymentStage?.totalAmountApprovedMinusExtraWork ?? 0);

    List<Stage> extraWork = [
      ...(currentViewedPaymentStage?.stages ?? []).where((value) =>
          value.contractType == 'extra_work' && value.type == 'normal')
    ];

    totalVatPrice +=
        extraWork.fold(0, (sum, stage) => sum + (stage.priceVat ?? 0));

    double paymentFromCustomerTotal = 0;

    if (!isStashed) {
      paymentFromCustomerTotal =
          currentViewedPaymentStage?.totalAmountStashed ?? 0;
    } else {
      paymentFromCustomerTotal = ((currentViewedPaymentStage?.totalAmountVat ??
                  0) +
              (currentViewedPaymentStage?.totalStashedExtraWorkAmount ?? 0)) -
          ((currentViewedPaymentStage?.totalAmountPaid ?? 0) +
              (currentViewedPaymentStage?.totalAmountPaidExtraWork ?? 0));
    }

    _stagesList.addAll({
      'totalVatPrice': totalVatPrice,
      'paymentFromCustomerTotal': paymentFromCustomerTotal
    });

    notifyListeners();

    setBusy(false);
  }

  Future<Uint8List?> mergeUrlPdfs(
      List<Map<String, dynamic>>? uploadedFiles) async {
    try {
      List<String> urls = [];

      for (Map<String, dynamic> element in (uploadedFiles ?? [])) {
        if ((element['NAME'] as String).contains('pdf')) {
          urls.add(element['URL']);
        }
      }

      if (urls.length > 1) {
        PdfDocument newDocument = PdfDocument();
        PdfSection? section;

        for (String url in urls) {
          http.Response response = await http.get(Uri.parse(url));

          if (response.statusCode == 200) {
            if (response.headers['content-type'] == 'application/pdf') {
              PdfDocument loadedDocument =
                  PdfDocument(inputBytes: response.bodyBytes);

              for (int index = 0; index < loadedDocument.pages.count; index++) {
                PdfPage loadedPage = loadedDocument.pages[index];
                PdfTemplate template = loadedPage.createTemplate();

                if (section == null ||
                    section.pageSettings.size != template.size) {
                  section = newDocument.sections!.add();
                  section.pageSettings.size = template.size;
                  section.pageSettings.margins.all = 0;
                }

                PdfPage newPage = section.pages.add();

                // Calculate scaling factors to fit template within the page
                double scaleX =
                    newPage.getClientSize().width / template.size.width;
                double scaleY =
                    newPage.getClientSize().height / template.size.height;
                double scale = scaleX < scaleY ? scaleX : scaleY;

                // Scale the template to fit
                newPage.graphics.drawPdfTemplate(
                  template,
                  const Offset(0, 0),
                  Size(template.size.width * scale,
                      template.size.height * scale),
                );
              }
              loadedDocument.dispose();
            } else {}
          } else {}
        }

        // Save the merged document
        List<int> bytes = await newDocument.save();
        newDocument.dispose();

        return Uint8List.fromList(bytes);
      } else {
        final response = await http.get(Uri.parse(urls.first));

        if (response.statusCode == 200) {
          return response.bodyBytes;
        } else {
          log('Failed to load PDF. Status code: ${response.statusCode}');
          return null;
        }
      }
    } catch (e) {
      log("WALLET VM PDF MERGE ERROR $e ");

      return null;
    }
  }

  Future<File?> mergePdfs(List<PlatformFile> pdfs) async {
    try {
      Directory? directory;

      if (Platform.isIOS) {
        directory = await getApplicationDocumentsDirectory();
      } else {
        directory = await getDownloadsDirectory();
      }

      if (pdfs.isNotEmpty) {
        if (pdfs.length > 1) {
          PdfDocument newDocument = PdfDocument();
          PdfSection? section;

          for (var element in pdfs) {
            File pdfFile = File(element.path!);

            List<int> pdfBytes = await pdfFile.readAsBytes();
            PdfDocument loadedDocument = PdfDocument(inputBytes: pdfBytes);

            for (int pageIndex = 0;
                pageIndex < loadedDocument.pages.count;
                pageIndex++) {
              PdfTemplate template =
                  loadedDocument.pages[pageIndex].createTemplate();

              // Create a new section if page settings differ
              if (section == null ||
                  section.pageSettings.size != template.size) {
                section = newDocument.sections!.add();
                section.pageSettings.size = template.size;
                section.pageSettings.margins.all = 0;
              }

              // Add the template to the new document
              section.pages
                  .add()
                  .graphics
                  .drawPdfTemplate(template, const Offset(0, 0));
            }

            loadedDocument.dispose();
          }

          List<int> mergedBytes = await newDocument.save();
          newDocument.dispose();

          // Save the merged PDF file
          String mergedFilePath =
              '${directory!.path}/merged_file_${DateTime.now().millisecondsSinceEpoch}.pdf';
          final mergedPdfs = File(mergedFilePath);
          await mergedPdfs.writeAsBytes(mergedBytes);

          log("PDF merged successfully and saved at $mergedFilePath");

          return mergedPdfs;
        } else {
          return File(pdfs.first.path!);
        }
      } else {
        return null;
      }
    } catch (e) {
      log("WALLET VM PDF MERGE ERROR $e ");

      return null;
    }
  }

  Future<bool?> downloadFile(
      {required String name,
      required String url,
      required String extension}) async {
    try {
      Directory? directory;
      if (Platform.isIOS) {
        directory = await getApplicationDocumentsDirectory();
      } else {
        directory = await getDownloadsDirectory();
      }
      final randid = math.Random().nextInt(10000);
      var filePath =
          "${directory!.path}/PartnerLogin/${name}_$randid.$extension";

      var file = File(filePath);
      while (file.existsSync()) {
        file = File(filePath);
      }

      final response = await Dio().download(url, filePath);

      return response.statusCode == 200;
    } catch (e) {
      log("WALLETVM downloadFile $e");

      return false;
    }
  }

  Future<bool?> rejectExtraWork({required int extraWorkId}) async {
    final response =
        await _extraWorkService.rejectExtraWork(extraWorkId: extraWorkId);

    return (response?.success ?? false);
  }

  Future<bool?> deleteExtraWork({required int extraWorkId}) async {
    final response =
        await _extraWorkService.deleteExtraWork(extraWorkId: extraWorkId);

    return (response?.success ?? false);
  }

  Future<bool?> approveExtraWork(
      {required ExtraWorkItems extraWork, required String signature}) async {
    final params = {
      'extraWorkId': extraWork.id,
      'paymentStages': [
        {
          "project_id": extraWork.projectId,
          "partner_id": extraWork.company,
          "calculation_id": 0,
          "industry_id": "0",
          "job_industry_id": "0",
          "price_total": extraWork.total,
          "sub_project_id": "0",
          "vat_total": extraWork.totalVat,
          "totalpricewithvatandfee": extraWork.amount,
          "transaction_fee": "",
          "note": "",
          "signature": "",
          "weeks": "",
          "start_date": "",
          "contract_type": "extra_work",
          "offer_id": "0",
          "description_payment": ["${extraWork.description}"]
        }
      ],
      'signaturePng': signature
    };

    final response = await _extraWorkService.approveExtraWork(params: params);

    return (response?.success ?? false);
  }

  Future<bool?> addOrUpdateExtraWork({
    required int contractId,
    required double price,
    required String endDate,
    required String description,
    required String signature,
    required String type,
    int? extraWorkId,
  }) async {
    MinboligApiResponse? addExtraWorkResponse;

    if (extraWorkId == null) {
      addExtraWorkResponse = await _extraWorkService.addExtraWork(params: {
        'contractId': contractId,
        'source': 'app',
        'price': price,
        'endDate': endDate,
        'description': description,
        'createdFrom': 'partner-app',
        'transactionId':
            paymentStagesMain?.transaction?.first.payproffTransactionId ?? '',
        'type': type,
      });
    } else {
      addExtraWorkResponse = await _extraWorkService.updateExtraWork(params: {
        'extraWorkId': extraWorkId,
        'price': price,
        'endDate': endDate,
        'description': description,
        "source": "app",
        'transactionId':
            paymentStagesMain?.transaction?.first.payproffTransactionId ?? '',
        'type': type,
      });
    }

    if (signature.isEmpty) {
      return addExtraWorkResponse?.success ?? false;
    } else {
      final params = {
        'extraWorkId': addExtraWorkResponse?.data['ID'],
        'paymentStages': [
          {
            "project_id": addExtraWorkResponse?.data['PROJECT_ID'],
            "partner_id": addExtraWorkResponse?.data['COMPANY'],
            "calculation_id": 0,
            "industry_id": "0",
            "job_industry_id": "0",
            "price_total": addExtraWorkResponse?.data['TOTAL'],
            "sub_project_id": "0",
            "vat_total": addExtraWorkResponse?.data['TOTAL_VAT'],
            "totalpricewithvatandfee": addExtraWorkResponse?.data['AMOUNT'],
            "transaction_fee": "",
            "note": "",
            "signature": signature,
            "weeks": "",
            "start_date": "",
            "contract_type": "extra_work",
            "offer_id": "0",
            "description_payment": [
              "${addExtraWorkResponse?.data['DESCRIPTION']}"
            ]
          }
        ],
        'signaturePng': signature
      };

      final response = await _extraWorkService.approveExtraWork(params: params);

      return (response?.success ?? false);
    }
  }

  Future<Map<String, dynamic>?> calculatePrices({required double price}) async {
    final response = await _payproffService.calculateFee(amount: price);

    if (!(response.success ?? false)) return null;

    return {
      'amount': (response.data['amount'] as num).toDouble(),
      'totalVatWithFee': (response.data['totalVat'] as num).toDouble(),
    };
  }

  Future<bool?> declinePayout({
    required String reason,
  }) async {
    final response = await _requestPaymentService.declinePayout(payload: {
      'paymentTransactionId': currentViewedStage!.payproffTransactionId,
      'paymentStageId': currentViewedStage!.id,
      'declinedReason': reason,
      'type': paymentStageType
    });

    return response?.success ?? false;
  }

  Future<bool?> forcePartialPayout({required String mitIdToken}) async {
    final response = await _requestPaymentService.forcePartialPayout(payload: {
      'projectId': currentViewedStage!.projectId,
      'transactionId': currentViewedStage!.payproffTransactionId,
      'paymentStageIds': [currentViewedStage!.id],
      'mitIdToken': mitIdToken,
    });

    return response?.success ?? false;
  }

  Future<bool?> requestPartial({required Map<String, dynamic> payload}) async {
    if (currentViewedStage?.type == 'deficiency') {
      final response = await _requestPaymentService
          .requestPaymentPartial(formData: FormData(), payload: {
        "paymentStageId": currentViewedStage!.id,
        "projectId": currentViewedStage!.projectId,
        "amount": currentViewedStage!.priceDeducted,
      });

      return response?.success ?? false;
    } else {
      final formData = FormData();

      for (final file in (payload['files'] as List<PlatformFile>)) {
        formData.files.addAll([
          MapEntry("files[]",
              await MultipartFile.fromFile(file.path!, filename: file.name)),
        ]);
      }

      formData.fields.add(
        MapEntry("paymentStageId", '${currentViewedStage!.id}'),
      );
      formData.fields.add(
        MapEntry("projectId", '${currentViewedStage!.projectId}'),
      );
      formData.fields.add(
        MapEntry("notes", '${payload['comment']}'),
      );
      formData.fields.add(
        MapEntry("comment", '${payload['comment']}'),
      );
      formData.fields.add(
        MapEntry("amount",
            '${double.parse(Formatter.sanitizeCurrency(payload['paymentField']))}'),
      );

      final response = await _requestPaymentService.requestPaymentPartial(
          formData: formData);

      return response?.success ?? false;
    }
  }

  Future<void> initWallet(
      {required int projectId, required int contractId}) async {
    setBusy(true);

    List<Future> futures = [
      getPaymentStage(projectId: projectId),
    ];

    if (contractId > 0) {
      futures.add(getExtraWork(params: {
        'size': 100,
        'page': 1,
        'contractId': contractId,
        'clearCache': true
      }));
    }

    await Future.wait(futures);

    setBusy(false);
  }

  Future<void> getExtraWork({required Map<String, dynamic> params}) async {
    ExtraWorkResponseModel? tempExtraWork =
        await _extraWorkService.getExtraWork(params: params);

    (tempExtraWork?.items?['EXTRA_WORKS'] as List<ExtraWorkItems>).removeWhere(
        (value) => value.stage == 'draft' && value.createdBy == 'homeowner');

    _extraWork = tempExtraWork;

    notifyListeners();
  }

  Future<void> getPaymentStage({required int projectId}) async {
    double tempWallet = 0;
    double tempTotalBalance = 0;

    _paymentStageType = 'normal';

    if (_fullIndustries.isEmpty) {
      final industryResponse = await _wizardService.getFullIndustry();

      _fullIndustries = industryResponse ?? [];
    }

    final responses = await Future.wait([
      _requestPaymentService.paymentStages(
          projectId: projectId, type: 'normal'),
      _requestPaymentService.paymentStages(
          projectId: projectId, type: 'subcontractor'),
      _requestPaymentService.paymentStages(
          projectId: projectId, type: 'producent'),
    ]);

    if (responses[0] != null) {
      _paymentStagesMain = PaymentStages.fromJson(responses[0]!.data);
      _currentViewedPaymentStage = PaymentStages.fromJson(responses[0]!.data);

      List<Stage> stages = [
        ...(currentViewedPaymentStage?.stages ?? []).where((element) =>
            element.contractType == 'normal' ||
            element.contractType == 'fast-track' &&
                element.payproffTransactionId != null)
      ];

      List<Stage> extraWorkStages = [
        ...(currentViewedPaymentStage?.stages ?? []).where((element) =>
            element.contractType == 'extra_work' &&
            element.payproffTransactionId != null)
      ];

      final stageGroup = stages.groupBy((value) => value.contractId);

      Map<String, Map<String, List<Stage>>> paymentStageMap = {};

      for (final i in stageGroup.entries) {
        Map<String, List<Stage>> stagesMap = {};

        for (var industryMap
            in i.value.groupBy((value) => value.jobIndustryId).entries) {
          final jobIndustryName = fullIndustries.firstWhere(
            (value) => value.brancheId == industryMap.key,
            orElse: () => FullIndustry.defaults(),
          );

          stagesMap[jobIndustryName.brancheDa ?? ''] = [...industryMap.value];
        }

        for (var element in stagesMap.entries) {
          tempWallet += (element.value
              .fold(0, (sum, stage) => sum + (stage.priceDeducted ?? 0)));
          tempTotalBalance += (element.value
              .where((value) => value.type == 'normal')
              .fold(
                  0,
                  (sum, stage) =>
                      sum +
                      ((stage.priceDeducted ?? 0) -
                          (stage.totalAmountPaid ?? 0))));
        }

        paymentStageMap['${i.key}'] = {...stagesMap};
      }

      List<ExtraWorkItems> extraWorkMinus = [
        ...(paymentStagesMain?.extraWork ?? []).where(
            (value) => value.type == 'minus' && value.stage == 'approved')
      ];

      _stagesList = {
        "paymentStage": paymentStageMap,
        "extraWork": extraWorkStages,
        "totalPrice": tempWallet,
        "balance": tempTotalBalance,
        "extraWorkMinus": extraWorkMinus,
      };
    }

    if (responses[1] != null) {
      _paymentStagesSubContractor = PaymentStages.fromJson(responses[1]!.data);
    }

    if (responses[2] != null) {
      _paymentStagesProducent = PaymentStages.fromJson(responses[2]!.data);
    }

    if ((paymentStagesMain?.stages ?? []).isNotEmpty) {
      _isStashed = (paymentStagesMain?.stages ?? []).first.stashed ?? false;
    }

    double totalVatPrice = (paymentStagesMain?.totalAmountVat ?? 0) +
        (paymentStagesMain?.totalAmountApprovedMinusExtraWork ?? 0);

    List<Stage> extraWork = [
      ...(paymentStagesMain?.stages ?? []).where((value) =>
          value.contractType == 'extra_work' && value.type == 'normal')
    ];

    totalVatPrice +=
        extraWork.fold(0, (sum, stage) => sum + (stage.priceVat ?? 0));

    double paymentFromCustomerTotal = 0;

    if (!isStashed) {
      paymentFromCustomerTotal = paymentStagesMain?.totalAmountStashed ?? 0;
    } else {
      paymentFromCustomerTotal = ((paymentStagesMain?.totalAmountVat ?? 0) +
              (paymentStagesMain?.totalStashedExtraWorkAmount ?? 0)) -
          ((paymentStagesMain?.totalAmountPaid ?? 0) +
              (paymentStagesMain?.totalAmountPaidExtraWork ?? 0));
    }

    _stagesList.addAll({
      'totalVatPrice': totalVatPrice,
      'paymentFromCustomerTotal': paymentFromCustomerTotal
    });

    notifyListeners();
  }

  void reset() {
    _currentViewedProject = null;
    _currentViewedPaymentStage = null;
    _paymentStagesMain = null;
    _extraWork = null;
    _currentViewedStage = null;
    _stagesList = {};
    // _isFullDeposit = false;

    notifyListeners();
  }
}
