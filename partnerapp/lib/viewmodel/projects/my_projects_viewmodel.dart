import 'package:Haandvaerker.dk/model/partner/case_project_model.dart';
import 'package:Haandvaerker.dk/model/partner/supplier_info_model.dart';
// import 'package:Haandvaerker.dk/model/project_new_model.dart';
import 'package:Haandvaerker.dk/model/projects/approved_projects_model.dart';
import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/model/projects/project_timeline_model.dart';
import 'package:Haandvaerker.dk/services/main/main_service.dart';
import 'package:Haandvaerker.dk/services/projects/projects_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyProjectsViewModel extends BaseViewModel {
  MyProjectsViewModel({required this.context});
  final BuildContext context;

  // ProjectNewModel _project = ProjectNewModel();
  // ProjectNewModel get project => _project;

  List<CaseProjectModel> _projects = <CaseProjectModel>[];
  List<CaseProjectModel> get projects => _projects;

  List<ApprovedProjectsModel> _approvedProjectList = [];
  List<ApprovedProjectsModel> get approvedProjectList => _approvedProjectList;

  List<PartnerProjectsModel> _partnerProjectList = [];
  List<PartnerProjectsModel> get partnerProjectList => _partnerProjectList;

  ProjectTimelineModel _projectTimeline = ProjectTimelineModel();

  PartnerProjectsModel _activeProject = PartnerProjectsModel();
  PartnerProjectsModel get activeProject => _activeProject;

  bool _showDeficiencyList = false;
  bool get showDeficiencyList => _showDeficiencyList;

  int? _currentProjectId;
  int? get currentProjectId => _currentProjectId;

  void reset() {
    _projects = [];
    _approvedProjectList = [];
    _partnerProjectList = [];
    _projectTimeline = ProjectTimelineModel();
    _activeProject = PartnerProjectsModel();
    _showDeficiencyList = false;
    _currentProjectId = 0;

    notifyListeners();
  }

  set activeProject(PartnerProjectsModel project) {
    _activeProject = project;
    notifyListeners();
  }

  set projects(List<CaseProjectModel> projects) {
    _projects = projects;
    notifyListeners();
  }

  set currentProjectId(int? projectId) {
    _currentProjectId = projectId;
    notifyListeners();
  }

  //NEW APIS

  Future<void> getProjects() async {
    final service = context.read<MainService>();

    final response = await service.getSupplierInfo();

    if (response != null) {
      final tempProjects = SupplierInfoModel.fromJson(response.data).projects!;

      projects =
          tempProjects.where((element) => element.stage != 'deleted').toList();
    }
  }

  Future<void> deleteShowCaseProject({required int projectId}) async {
    final service = context.read<ProjectsService>();

    final response = await service.deleteShowCaseProject(projectId: projectId);

    if (response) {
      final tempProjects = <CaseProjectModel>[..._projects];
      tempProjects.removeWhere((element) => element.id == projectId);

      projects = tempProjects;
    }
  }

  Future<List<ApprovedProjectsModel>> approvedProjects() async {
    var service = context.read<ProjectsService>();
    setBusy(true);

    var response = await service.approvedProjects();

    _approvedProjectList = response;

    setBusy(false);

    return _approvedProjectList;
  }

  Future<List<PartnerProjectsModel>> partnerProjects() async {
    var service = context.read<ProjectsService>();
    setBusy(true);

    var response = await service.partnerProjects();

    _partnerProjectList = response;

    setBusy(false);

    return _partnerProjectList;
  }

  Future<ProjectTimelineModel> projectTimeline({required int projectId}) async {
    var service = context.read<ProjectsService>();
    setBusy(true);

    var response = await service.getProjectTimeline(projectId: projectId);

    _projectTimeline = response;

    setBusy(false);

    return _projectTimeline;
  }

  Future<void> getProjectId(
      {required int projectid, String? pageRedirect}) async {
    var service = context.read<ProjectsService>();

    var response = await service.getProjectById(
        projectId: projectid, pageRedirect: pageRedirect);

    if (response != null) {
      _activeProject = PartnerProjectsModel.fromJson(response.data);

      bool tempBool = false;

      if (_activeProject.paymentStages != null &&
          (_activeProject.paymentStages ?? <ProjectPaymentStages>[])
              .isNotEmpty) {
        for (var item in _activeProject.paymentStages!.where((element) =>
            element.contractId == _activeProject.contractId as int?)) {
          if (item.paymentReleased == null ||
              double.parse((item.paymentReleased ?? '0')) !=
                  (double.parse((item.priceVat ?? '0')) +
                      double.parse((item.transactionFee ?? '0')))) {
            _showDeficiencyList = false;
            break;
          } else {
            tempBool = true;
          }
        }
      }

      _showDeficiencyList = tempBool;
    }

    notifyListeners();
  }

  Future<bool> updateShowCaseStatus(
      {required int projectId, required String stage}) async {
    //

    final service = context.read<ProjectsService>();

    return await service.updateShowCaseStatus(
        projectId: projectId, stage: stage);

    //
  }
}
