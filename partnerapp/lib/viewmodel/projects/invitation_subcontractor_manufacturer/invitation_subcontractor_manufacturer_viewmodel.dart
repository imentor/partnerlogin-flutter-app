import 'dart:convert';
import 'dart:developer';

import 'package:Haandvaerker.dk/model/payment/payment_stages_model.dart';
import 'package:Haandvaerker.dk/model/producent/producent_company_model.dart';
import 'package:Haandvaerker.dk/model/wizard/list_price_model.dart';
import 'package:Haandvaerker.dk/services/manufacturer_sub_contractor/manufacturer_sub_contractor_service.dart';
import 'package:Haandvaerker.dk/services/wizard/wizard_service.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/string_utils.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InvitationSubcontractorManufacturerViewmodel extends BaseViewModel {
  InvitationSubcontractorManufacturerViewmodel({required this.context})
      : _wizardService = context.read<WizardService>(),
        _manufacturerSubContractorService =
            context.read<ManufacturerSubContractorService>(),
        super();

  final BuildContext context;

  final WizardService _wizardService;
  final ManufacturerSubContractorService _manufacturerSubContractorService;

  List<Map<String, dynamic>> _listPrices = [];
  List<Map<String, dynamic>> get listPrices => _listPrices;

  Map<String, dynamic> _formValues = {};
  Map<String, dynamic> get formValues => _formValues;

  int _currentFormStep = 0;
  int get currentFormStep => _currentFormStep;

  double _totalAllocatedBudget = 0;
  double get totalAllocatedBudget => _totalAllocatedBudget;

  void updatedFormValues({required String key, dynamic value}) {
    _formValues[key] = value;

    notifyListeners();
  }

  set currentFormStep(int value) {
    _currentFormStep = value;

    notifyListeners();
  }

  void updateSelectedListPirce({
    required int index,
    required dynamic value,
    required String whatToUpdate,
  }) {
    switch (whatToUpdate) {
      case 'select':
        _listPrices[index]['isSelected'] = value;
        break;
      case 'price':
        _listPrices[index]['price'] = value;
        break;
      default:
    }

    notifyListeners();
  }

  void updateSelectedDescriptionListPrice({
    required int index,
    required int indexDescription,
    required dynamic value,
    required String whatToUpdate,
  }) {
    switch (whatToUpdate) {
      case 'select':
        _listPrices[index]['descriptions'][indexDescription]['isSelected'] =
            value;
        break;
      case 'price':
        _listPrices[index]['descriptions'][indexDescription]['price'] = value;
        _listPrices[index]['price'] =
            (_listPrices[index]['descriptions'] as List<Map<String, dynamic>>)
                .fold(0.0, (sum, maps) => sum += double.parse(maps['price']));
        break;
      case 'descriptionEditable':
        _listPrices[index]['descriptions'][indexDescription]['isEditable'] =
            value;
        break;
      case 'editDescription':
        _listPrices[index]['descriptions'][indexDescription]['title'] = value;
        _listPrices[index]['descriptions'][indexDescription]['isEditable'] =
            false;
        break;
      default:
    }

    notifyListeners();
  }

  Future<void> getHeaders() async {
    _formValues['headers'] =
        await _manufacturerSubContractorService.getHeaders();
  }

  Future<bool> acceptInviteSubContractor(Map<String, dynamic> payload) async {
    setBusy(true);

    final response = await _manufacturerSubContractorService
        .acceptInviteSubContractor(payload: payload);

    _formValues['headers'] =
        await _manufacturerSubContractorService.getHeaders();

    setBusy(false);

    return response.success ?? false;
  }

  Future<bool> manufacturerAddRequistion(
      {required int projectId,
      required int contractId,
      required String signature}) async {
    setBusy(true);

    final producentCompany = formValues['producentCompany'] as ProducentCompany;

    if (formValues['priceType'] == 'fixedPrice') {
      double price = double.parse(
          Formatter.sanitizeCurrency(formValues['manufacturerFixedPrice']));

      final formData = FormData.fromMap({
        "Name": producentCompany.fullName ?? '',
        "Subject": formValues['manufacturerFixedPriceDescription'],
        "projectId": '$projectId',
        'email': formValues['manufacturerEmail'],
        "Description": formValues['manufacturerFixedPriceDescription'],
        "CONTRACT": '$contractId',
        "CVR": producentCompany.cvr ?? '',
        'SUBSIDIARYNUMBER':
            int.parse(producentCompany.cvprNumber?.split(' ').last ?? '0'),
        "FIXEDPRICE": '0',
        "DELIVER_ADDRESS": producentCompany.address ?? '',
        "DELIVER_DATE": DateFormatType.isoDate.formatter
            .format((formValues['manufacturerStartDate'] as DateTime))
            .toString()
            .split(' ')
            .first,
        'signature': signature,
        "paymentStages": [
          {
            "industryId": 0,
            "jobIndustryId": 0,
            "startDate": "",
            "endDate": "",
            "description": "",
            "price": price
          }
        ],
        'Price': '$price',
      });

      if (formValues.containsKey('manufacturerFiles')) {
        for (final file
            in (formValues['manufacturerFiles'] as List<PlatformFile>)) {
          formData.files.addAll([
            MapEntry("files[]",
                await MultipartFile.fromFile(file.path!, filename: file.name)),
          ]);
        }
      }

      final response = await _manufacturerSubContractorService
          .manufacturerAddRequisition(form: formData);

      setBusy(false);

      return response.success ?? false;
    } else if (formValues['priceType'] == 'maximumPrice') {
      double price = 0;

      List<Map<String, dynamic>> paymentStagesMap = [];
      for (var element in listPrices) {
        if (element['isSelected']) {
          log("message asd ${jsonEncode(element)}");
          price += element['price'];
          paymentStagesMap.add({
            "industryId": element['industryId'],
            "jobIndustryId": element['jobIndustryId'],
            "startDate": "",
            "endDate": "",
            "description": [
              ...(element['descriptions'] as List)
                  .map((value) => value['title'] as String)
            ]
          });
        }
      }
      final formData = FormData.fromMap({
        "Name": producentCompany.fullName ?? '',
        "Subject": formValues['manufacturerMaximumPriceDescription'],
        "projectId": '$projectId',
        'email': formValues['manufacturerEmail'],
        "Description": formValues['manufacturerMaximumPriceDescription'],
        "CONTRACT": '$contractId',
        "CVR": producentCompany.cvr ?? '',
        'SUBSIDIARYNUMBER':
            int.parse(producentCompany.cvprNumber?.split(' ').last ?? '0'),
        "FIXEDPRICE": '0',
        "DELIVER_ADDRESS": producentCompany.address ?? '',
        "DELIVER_DATE": DateFormatType.isoDate.formatter
            .format((formValues['manufacturerStartDate'] as DateTime))
            .toString()
            .split(' ')
            .first,
        'signature': signature,
        "paymentStages": paymentStagesMap,
        'Price': '$price',
      });
      final response = await _manufacturerSubContractorService
          .manufacturerAddRequisition(form: formData);

      setBusy(false);

      return response.success ?? false;
    }

    setBusy(false);

    return false;
  }

  Future<List<ProducentCompany>> getProducentCompanies(
      {required String search}) async {
    try {
      if (search.isEmpty) return [];

      List<ProducentCompany> companies = [];

      final response = await _manufacturerSubContractorService
          .getProducentCompanies(search: search);

      if (response.data is List) {
        for (Map<String, dynamic> element in response.data as List) {
          String fullname = '';
          String cvprNumber = '';
          String address = '';

          Map<String, dynamic> subsidaryMap = {};

          if (element.containsKey('subsidiaries') &&
              element['subsidiaries'] is List &&
              (element['subsidiaries'] as List).isNotEmpty) {
            subsidaryMap = element['subsidiaries'][0];
          }

          if (element.containsKey('life')) {
            fullname = element['life']['name'] ?? '';
          }

          if (element.containsKey('address')) {
            cvprNumber =
                '${element['address']['cityname'] ?? ''} ${element['address']['steet'] ?? ''} ${subsidaryMap['subsidiarynumber'] ?? ''}';
            address =
                '${element['address']['steet'] ?? ''} ${element['address']['steetcode'] ?? ''} ${element['address']['zipcode'] ?? ''} ${element['address']['cityname'] ?? ''}';
          }

          companies.add(
            ProducentCompany(
                fullName: fullname,
                cvprNumber: cvprNumber,
                cvr: '${subsidaryMap['vat'] ?? ''}',
                address: address),
          );
        }
      }

      return companies;
    } catch (e) {
      log("message getProducentCompanies $e");
      return [];
    }
  }

  String? getValidation({required String type}) {
    if (type == 'subContractor') {
      if (listPrices.where((value) => value['price'] != 0).isEmpty) {
        return 'NO_SELECT';
      } else {
        double totalAmount = 0;

        for (var element in listPrices.where((value) => value['price'] > 0)) {
          for (var descriptionMap in (element['descriptions'] as List<Map>)) {
            if ((descriptionMap['isSelected'] as bool)) {
              totalAmount += double.parse(descriptionMap['price']);
            }
          }
        }

        if (totalAmount > totalAllocatedBudget) {
          return 'MORE_THAN';
        } else {
          currentFormStep += 1;
        }
      }
    } else if (type == 'manufacturer') {
      if ((listPrices.where((value) => value['isSelected']).isEmpty)) {
        return 'NO_SELECT';
      } else {
        double totalAmount = listPrices
            .where((value) => value['isSelected'])
            .fold(0, (sum, listPriceMap) => sum + listPriceMap['price']);

        if (totalAmount > totalAllocatedBudget) {
          return 'MORE_THAN';
        }
      }
    }
    return null;
  }

  Future<bool> subContractorAddSignature(
      {required String signature, int? subcontractorId}) async {
    setBusy(true);
    final response =
        await _manufacturerSubContractorService.subContractorAddSignature(
            signature: signature,
            id: subcontractorId ?? formValues['inviteResponse']['ID']);

    setBusy(false);

    return response.success ?? false;
  }

  Future<void> subContractorInvite(
      {required int contractId, required int projectId}) async {
    setBusy(true);

    double totalAmount = 0;
    List<Map<String, dynamic>> paymentStages = [];

    for (var element in listPrices.where((value) => value['price'] > 0)) {
      for (var descriptionMap in (element['descriptions'] as List<Map>)) {
        if ((descriptionMap['isSelected'] as bool)) {
          totalAmount += double.parse(descriptionMap['price']);
          paymentStages.add({
            "industryId": descriptionMap['industryId'],
            "jobIndustryId": descriptionMap['jobIndustryId'],
            "startDate": (formValues['startDate'] as DateTime).toString(),
            "endDate": (formValues['endDate'] as DateTime).toString(),
            "price": descriptionMap['price'],
            "description": descriptionMap['title']
          });
        }
      }
    }

    Map<String, dynamic> invitePayload = {
      "name":
          '${formValues['inviteFirstName'] ?? ''} ${formValues['inviteLastName'] ?? ''}',
      "email": formValues['inviteEmail'] ?? '',
      "contractId": contractId,
      "projectId": projectId,
      "amount": totalAmount,
      "startDate": (formValues['startDate'] as DateTime).toString(),
      "endDate": (formValues['endDate'] as DateTime).toString(),
      "createdFrom": "partner-app",
      "partnerSignature": "",
      "paymentStages": paymentStages
    };

    final response = await _manufacturerSubContractorService
        .subContractorInvite(payload: invitePayload);

    _formValues['inviteResponse'] = response.data;

    _currentFormStep += 1;

    setBusy(false);

    notifyListeners();
  }

  Future<void> getListPriceMinbolig(
      {required int projectId,
      required int contactId,
      required List<Stage> stages}) async {
    setBusy(true);

    _totalAllocatedBudget =
        stages.fold(0, (sum, stage) => sum + (stage.balance ?? 0));

    final response = await _wizardService.getListPriceMinbolig(
        contactId: contactId, projectId: projectId);

    if (response != null) {
      final listPrices = [
        ...ListPriceMinbolig.fromCollection(response.data['result'])
      ];

      for (var listPrice in listPrices) {
        List<Map<String, dynamic>> descriptions = [];
        for (var element in (listPrice.seriesOriginal ?? [])) {
          descriptions.add({
            'title': element,
            'price': '00',
            'isSelected': false,
            'isEditable': false,
            "industryId": listPrice.industry,
            "jobIndustryId": listPrice.jobIndustry,
          });
        }
        _listPrices.add({
          'id': listPrice.id,
          'title':
              '${(listPrice.jobIndustryName ?? '').toTitleCase()} ${(listPrice.industryName ?? '').toTitleCase()}',
          'descriptions': descriptions,
          'isSelected': false,
          'isEditable': false,
          'price': 0,
          "industryId": listPrice.industry,
          "jobIndustryId": listPrice.jobIndustry,
        });
      }
    }

    _formValues['headers'] =
        await _manufacturerSubContractorService.getHeaders();

    setBusy(false);

    notifyListeners();
  }

  void reset() {
    _listPrices = [];
    _currentFormStep = 0;
    _formValues = {};

    notifyListeners();
  }
}
