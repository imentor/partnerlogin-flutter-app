import 'dart:developer';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/projects/partner_contact_model.dart';
import 'package:Haandvaerker.dk/model/projects/project_message_model.dart';
import 'package:Haandvaerker.dk/services/messages/message_service.dart';
import 'package:Haandvaerker.dk/services/projects/projects_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProjectMessagesViewModel extends BaseViewModel {
  ProjectMessagesViewModel({required this.context});

  final BuildContext context;

  List<ProjectMessageModel> _messages = [];
  List<ProjectMessageModel> get messages => _messages;

  List<ProjectMessageModel> _tempMessages = [];
  List<ProjectMessageModel> get tempMessages => _tempMessages;

  List<ProjectMessageModel> _selectedMessageThread = [];
  List<ProjectMessageModel> get selectedMessageThread => _selectedMessageThread;

  List<PartnerContactModel> _contacts = [];
  List<PartnerContactModel> get contacts => _contacts;

  void reset() {
    _messages = [];
    _tempMessages = [];
    _selectedMessageThread = [];
    _contacts = [];

    notifyListeners();
  }

  Future<void> getProjectMessages({int? projectId}) async {
    try {
      setBusy(true);
      final messageService = context.read<MessageService>();
      final service = context.read<ProjectsService>();

      final contactResponse = await service.getPartnerContacts();

      final response =
          await messageService.getProjectMessages(projectId: projectId!);
      if (response.success!) {
        _messages = ProjectMessageModel.fromCollection(response.data)
            .where((element) => !element.isFromPartner!)
            .toList();
        _tempMessages = [..._messages];
      }
      _contacts = PartnerContactModel.fromCollection(contactResponse.data);
      setBusy(false);
      notifyListeners();
    } catch (e) {
      log("message getProjectMessages catch $e");
    }
  }

  Future<MinboligApiResponse> getProjectMessageThread(
      {required threadId}) async {
    final messageService = context.read<MessageService>();

    final response =
        await messageService.getProjectMessageThread(threadId: threadId);

    if (response.success!) {
      _selectedMessageThread =
          ProjectMessageModel.fromCollection(response.data);
      notifyListeners();
    }

    return response;
  }

  Future<MinboligApiResponse> getPartnerContacts() async {
    var service = context.read<ProjectsService>();

    var response = await service.getPartnerContacts();

    if (response.success!) {
      _contacts = PartnerContactModel.fromCollection(response.data);
      notifyListeners();
    }

    return response;
  }

  Future<MinboligApiResponse> sendProjectMessageThread({
    required threadId,
    required body,
    required contactId,
    required title,
  }) async {
    var service = context.read<MessageService>();

    var response = await service.sendProjectMessageThread(
      threadId: threadId,
      body: body,
      contactId: contactId,
      title: title,
    );

    if (response.success!) {}

    return response;
  }

  Future<MinboligApiResponse> sendNewProjectMessage({
    required body,
    required contactId,
    required title,
  }) async {
    var service = context.read<MessageService>();

    var response = await service.sendNewProjectMessage(
      body: body,
      contactId: contactId,
      title: title,
    );

    if (response.success!) {}

    return response;
  }

  Future<MinboligApiResponse> postMessageAction(
      {required messageActionUrl}) async {
    var service = context.read<MessageService>();

    var response = await service.postMessageAction(
      messageActionUrl: messageActionUrl,
    );

    if (response.success!) {}

    return response;
  }

  Future<void> markAsSeen({required int messageId, required int index}) async {
    var service = context.read<MessageService>();

    var response = await service.markAsSeen(messageId: messageId);

    if (response != null && response.success!) {
      final tempMessage = ProjectMessageModel.fromJson(
          (response.data as Map<String, dynamic>)['message']);

      final tempMessages = [..._messages];
      tempMessages.removeAt(index);
      tempMessages.insert(index, tempMessage);

      _messages = [...tempMessages];
      notifyListeners();
    }
  }

  Future<void> searchKeyword({required String query}) async {
    if (query.isEmpty) {
      _messages = [..._tempMessages];
      notifyListeners();
    } else {
      final search = [
        ..._tempMessages.where((element) =>
            element.message!.contains(query) ||
            element.contact!.name!.contains(query))
      ];
      _messages = [...search];
      notifyListeners();
    }
  }
}
