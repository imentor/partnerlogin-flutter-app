import 'dart:developer';
import 'dart:io';

import 'package:Haandvaerker.dk/model/file/file_data_model.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/file/file_service.dart';
import 'package:Haandvaerker.dk/services/photo_doc/photo_doc_service.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/ui/screens/projects/photo_documentation/photo_documentation_screen.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_editor_plus/image_editor_plus.dart';
import 'package:image_editor_plus/options.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class PhotoDocumentationViewModel extends BaseViewModel {
  ///

  final BuildContext context;

  PhotoDocumentationViewModel({required this.context});

  int _projectId = 0;
  int _tenderFolderId = 0;

  List<FileData> _documentation = [];
  List<FileData> get documentation => _documentation;

  FileData? _currentPhoto;
  FileData? get currentPhoto => _currentPhoto;

  bool _isEditing = false;
  bool get isEditing => _isEditing;

  TextEditingController? _descriptionController;
  TextEditingController? get descriptionController => _descriptionController;

  TextEditingController? _titleController;
  TextEditingController? get titleController => _titleController;

  FocusNode? _focusDescription;
  FocusNode? get focusDescription => _focusDescription;

  FocusNode? _focusTitle;
  FocusNode? get focusTitle => _focusTitle;

  List<String> options = ['edit', 'delete'];

  int _editIndex = 1;
  int get editIndex => _editIndex;

  GlobalKey<FormState>? _addFormKey;
  GlobalKey<FormState>? get addFormKey => _addFormKey;

  GlobalKey<FormState>? _editFormKey;
  GlobalKey<FormState>? get editFormKey => _editFormKey;

  void reset() {
    _projectId = 0;
    _tenderFolderId = 0;
    _documentation = [];
    _currentPhoto = null;
    _isEditing = false;
    _descriptionController = null;
    _titleController = null;
    _focusDescription = null;
    _focusTitle = null;
    _editIndex = 1;
    _addFormKey = null;
    _editFormKey = null;

    notifyListeners();
  }

  set documentation(List<FileData> photos) {
    _documentation = photos;
    notifyListeners();
  }

  set currentPhoto(FileData? data) {
    _currentPhoto = data;
    notifyListeners();
  }

  set isEditing(bool edit) {
    _isEditing = edit;
    notifyListeners();
  }

  set descriptionController(TextEditingController? controller) {
    _descriptionController = controller;
    notifyListeners();
  }

  set titleController(TextEditingController? controller) {
    _titleController = controller;
    notifyListeners();
  }

  set editIndex(int index) {
    _editIndex = index;
    notifyListeners();
  }

  set addFormKey(GlobalKey<FormState>? key) {
    _addFormKey = key;
    notifyListeners();
  }

  set editFormKey(GlobalKey<FormState>? key) {
    _editFormKey = key;
    notifyListeners();
  }

  set focusDescription(FocusNode? focus) {
    _focusDescription = focus;
    notifyListeners();
  }

  set focusTitle(FocusNode? focus) {
    _focusTitle = focus;
    notifyListeners();
  }

  void unFocusFields() {
    _focusDescription!.unfocus();
    _focusTitle!.unfocus();
  }

  void setDsocumentIds({required int projectId, required int folderId}) {
    _projectId = projectId;
    _tenderFolderId = folderId;
  }

  void initValues() {
    setBusy(true);
    isEditing = false;
    currentPhoto = null;
    editIndex = 1;
    addFormKey = GlobalKey<FormState>();
    editFormKey = GlobalKey<FormState>();
    focusDescription = FocusNode();
    focusTitle = FocusNode();
    documentation = <FileData>[];
    descriptionController = TextEditingController();
    titleController = TextEditingController();
  }

  Future<void> getDocumentationPhotos({required int projectId}) async {
    ///
    setBusy(true);

    final photoDocService = context.read<PhotoDocService>();

    final response =
        await photoDocService.getPhotoDocuments(projectId: projectId);

    if (!response.success!) throw ApiException(message: response.message!);

    _documentation = FileData.fromCollection(response.data);

    setBusy(false);

    ///
  }

  Future<void> _addDocumentation(
      {required Map<String, dynamic> payload}) async {
    ///

    setBusy(true);

    final fileService = context.read<FilesService>();

    final response = await fileService.addFile(payload: payload);

    final tempDocumentation = <FileData>[response, ..._documentation];

    _documentation = tempDocumentation;

    setBusy(false);

    ///
  }

  Future<void> saveImage(
      {required BuildContext context,
      required File image,
      required int jobIndustry}) async {
    ///

    if (_addFormKey!.currentState!.validate()) {
      final key = GlobalKey<NavigatorState>();
      showLoadingDialog(context, key);

      final payload = {
        'file': await MultipartFile.fromFile(image.path,
            filename: image.path.split('/').last),
        'parentId': _tenderFolderId,
        'category': 'Documentation',
        'title': _titleController!.text.trim(),
        'tagUserIds': jobIndustry.toString(),
        'description': _descriptionController!.text.trim(),
        'projectId': _projectId,
        'CREATED_BY': 'partner',
      };

      log("projectId:  $_projectId");
      log('jobIndustry: $jobIndustry');

      try {
        await _addDocumentation(payload: payload);
        Navigator.pop(key.currentContext!);
        if (context.mounted) {
          _titleController!.clear();
          _descriptionController!.clear();
          await showSuccessAnimationDialog(
              context, true, tr('upload_documentation_success'));
          if (context.mounted) {
            Navigator.pop(context);
          }
        }
      } catch (e) {
        log('upload failed: $e');
        Navigator.pop(key.currentContext!);
        if (context.mounted) {
          await showSuccessAnimationDialog(
              context, false, tr('upload_documentation_failed'));
        }
      }

      await getDocumentationPhotos(projectId: _projectId);
    }

    ///
  }

  Future<File?> selectPhoto({
    required BuildContext context,
    bool replace = false,
  }) async {
    ///

    final source = await showSelectImageSource(context: context);

    XFile? image;

    switch (source) {
      case 'camera':
        try {
          image = await ImagePicker().pickImage(source: ImageSource.camera);
        } on PlatformException catch (e) {
          log(e.code);
          break;
        }
        break;
      case 'gallery':
        try {
          image = await ImagePicker().pickImage(source: ImageSource.gallery);
        } on PlatformException catch (e) {
          log(e.code);
          break;
        }
        break;
      default:
        break;
    }

    if (image != null) {
      final toBeEdited = await image.readAsBytes();

      final edited = await _editImage(imageData: toBeEdited, path: image.path);
      if (!replace && edited != null) {
        changeDrawerRoute(Routes.addDocumentation, arguments: edited);
      } else {
        return edited;
      }
    }
    return null;

    ///
  }

  Future<File?> _editImage(
      {required Uint8List imageData, required String path}) async {
    ///

    final editedImage = await Navigator.push(
      myGlobals.homeScaffoldKey!.currentContext!,
      MaterialPageRoute(
        builder: (context) => ImageEditor(
          image: imageData,
          imagePickerOption: const ImagePickerOption(
            captureFromCamera: false,
            pickFromGallery: false,
          ),
        ),
      ),
    );

    if (editedImage != null) {
      return await _convertToFile(data: editedImage, path: path);
    } else {
      return null;
    }

    ///
  }

  Future<File> _convertToFile(
      {required Uint8List data, required String path}) async {
    File file = await File(path).create();
    file.writeAsBytesSync(data);

    return file;
  }

  Future<File?> editImageView(
      {required String path, required File image}) async {
    ///

    final toBeEdited = await image.readAsBytes();

    final fileName = '${image.path.split('/').last}-edited($_editIndex)';
    final path = image.path.replaceFirst(image.path.split('/').last, fileName);

    return await _editImage(imageData: toBeEdited, path: path);

    ///
  }
}
