import 'dart:io';

import 'package:Haandvaerker.dk/model/get_types_response_model.dart';
import 'package:Haandvaerker.dk/model/partner/case_project_model.dart';
import 'package:Haandvaerker.dk/model/partner/supplier_info_file_model.dart';
import 'package:Haandvaerker.dk/model/projects/manufacture_model.dart';
import 'package:Haandvaerker.dk/services/projects/projects_service.dart';
import 'package:Haandvaerker.dk/services/settings/settings_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CreateProjectsViewModel extends BaseViewModel {
  CreateProjectsViewModel({required this.context});
  final BuildContext context;

  CaseProjectModel? _currentProject;
  CaseProjectModel? get currentProject => _currentProject;

  List<SubIndustries> _jobTypes = <SubIndustries>[];
  List<SubIndustries> get jobTypes => _jobTypes;

  List<ManufactureModel> _manufactures = <ManufactureModel>[];
  List<ManufactureModel> get manufactures => _manufactures;

  List<SubIndustries> _selectedJobTypes = <SubIndustries>[];
  List<SubIndustries> get selectedJobTypes => _selectedJobTypes;

  List<File> _projectImages = [];
  List<File> get projectImages => _projectImages;

  List<SupplierInfoFileModel> _currentProjectImages = [];
  List<SupplierInfoFileModel> get currentProjectImages => _currentProjectImages;

  List<ManufactureModel> _selectedManufactures = [];
  List<ManufactureModel> get selectedManufactures => _selectedManufactures;

  List<int> _deletedProjectPictures = <int>[];
  List<int> get deletedProjectPictures => _deletedProjectPictures;

  bool _validateJobTypes = false;
  bool get validateJobTypes => _validateJobTypes;

  Map<String, dynamic> _step2FormData = <String, dynamic>{};
  Map<String, dynamic> get step2FormData => _step2FormData;

  String _price = '';
  String get price => _price;

  String _time = '';
  String get time => _time;

  String _description = '';
  String get description => _description;

  String _post = '';
  String get post => _post;

  String _projectCreated = '';
  String get projectCreated => _projectCreated;

  String _title = '';
  String get title => _title;

  String _projectURL = '';
  String get projectURL => _projectURL;

  void setValuesToDefault() {
    _currentProject = null;
    _jobTypes = [];
    _manufactures = [];
    _selectedJobTypes = [];
    _projectImages = [];
    _currentProjectImages = [];
    _selectedManufactures = [];
    _deletedProjectPictures = [];
    _validateJobTypes = false;
    _step2FormData = {};
    _price = '';
    _time = '';
    _description = '';
    _post = '';
    _projectCreated = '';
    _title = '';
    _projectURL = '';

    notifyListeners();
  }

  void reset() {
    _currentProject = null;
    _jobTypes = [];
    _manufactures = [];
    _selectedJobTypes = [];
    _projectImages = [];
    _currentProjectImages = [];
    _selectedManufactures = [];
    _deletedProjectPictures = [];
    _validateJobTypes = false;
    _step2FormData = {};
    _price = '';
    _time = '';
    _description = '';
    _post = '';
    _projectCreated = '';
    _title = '';
    _projectURL = '';

    notifyListeners();
  }

  set currentProject(CaseProjectModel? project) {
    _currentProject = project;
    notifyListeners();
  }

  set price(String value) {
    _price = value;
    notifyListeners();
  }

  set time(String value) {
    _time = value;
    notifyListeners();
  }

  set description(String value) {
    _description = value;
    notifyListeners();
  }

  set post(String value) {
    _post = value;
    notifyListeners();
  }

  set projectCreated(String value) {
    _projectCreated = value;
    notifyListeners();
  }

  set title(String value) {
    _title = value;
    notifyListeners();
  }

  set projectImages(List<File> files) {
    _projectImages = files;
    notifyListeners();
  }

  set selectedManufactures(List<ManufactureModel> manufacture) {
    _selectedManufactures = manufacture;
    notifyListeners();
  }

  set jobTypes(List<SubIndustries> jobTypes) {
    _jobTypes = jobTypes;
    notifyListeners();
  }

  set manufactures(List<ManufactureModel> manufactures) {
    _manufactures = manufactures;
    notifyListeners();
  }

  set selectedJobTypes(List<SubIndustries> jobTypes) {
    _selectedJobTypes = jobTypes;
    notifyListeners();
  }

  set projectURL(String url) {
    _projectURL = url;
    notifyListeners();
  }

  set currentProjectImages(List<SupplierInfoFileModel> images) {
    _currentProjectImages = images;
    notifyListeners();
  }

  set step2FormData(Map<String, dynamic> formData) {
    _step2FormData = formData;
    notifyListeners();
  }

  set validateJobTypes(bool validate) {
    _validateJobTypes = validate;
    notifyListeners();
  }

  void addRemoveJobTypes(SubIndustries value) {
    if (!_selectedJobTypes.contains(value)) {
      _selectedJobTypes.add(value);
    } else {
      _selectedJobTypes.remove(value);
    }
    notifyListeners();
  }

  void clearJobTypes() {
    _selectedJobTypes.clear();
    notifyListeners();
  }

  void addRemoveManufactures(ManufactureModel value) {
    if (!_selectedManufactures.contains(value)) {
      _selectedManufactures.add(value);
    } else {
      _selectedManufactures.remove(value);
    }
    notifyListeners();
  }

  void clearManufactures() {
    _selectedManufactures.clear();
    notifyListeners();
  }

  void addNewImage(File value) {
    _projectImages.add(value);
    notifyListeners();
  }

  void removeNewImage(File value) {
    _projectImages.remove(value);
    notifyListeners();
  }

  set deletedProjectPictures(List<int> deleted) {
    _deletedProjectPictures = deleted;
    notifyListeners();
  }

  //NEW METHODS
  Future<void> getJobTypes() async {
    final service = context.read<SettingsService>();

    final response = await service.getTaskTypesFromCRM();

    if (response != null) {
      List<SubIndustries> types = <SubIndustries>[];
      final industries = Data.fromCollection(response.data);

      for (final data in industries) {
        if (data.subIndustries != null) {
          types.addAll(data.subIndustries!);
        }
      }

      jobTypes = types;
    }
  }

  Future<void> getAllManufactures() async {
    final service = context.read<ProjectsService>();

    manufactures = await service.getAllManufactures();
  }

  Future<void> createProject() async {
    final service = context.read<ProjectsService>();

    final projectId = await service.createShowCaseProject(title: title);

    final response = await Future.wait([
      service.updateShowCaseProject(payload: {
        'COST': _price,
        'DESCRIPTION': _description,
        'MANUFACTUREIDS': _selectedManufactures
            .map((e) => e.manufactureId.toString())
            .toList()
            .join(','),
        'PROJECTID': projectId,
        'PROJECTSTART': _projectCreated,
        'STAGE': "active",
        'SUBTASKTYPE': _selectedJobTypes
            .map((e) => e.subcategoryId.toString())
            .toList()
            .join(','),
        'TAGS': '0',
        'TASKTYPEIDS': '0',
        'TIME': _time,
        'TITLE': _title,
        'YEAR': int.parse(_projectCreated.split('-').first),
        'ZIP': _post,
      }),
      service.updateShowCaseProjectPicture(
          files: _projectImages, projectId: projectId, description: description)
    ]);

    projectURL = response.first as String;
  }

  Future<void> updateShowCaseProject() async {
    final service = context.read<ProjectsService>();

    await service.updateShowCaseProject(payload: {
      'COST': _price,
      'DESCRIPTION': _description,
      'MANUFACTUREIDS': _selectedManufactures
          .map((e) => e.manufactureId.toString())
          .toList()
          .join(','),
      'PROJECTID': currentProject!.projectId,
      'PROJECTSTART': _projectCreated,
      'STAGE': "active",
      'SUBTASKTYPE': _selectedJobTypes
          .map((e) => e.subcategoryId.toString())
          .toList()
          .join(','),
      'TAGS': '0',
      'TASKTYPEIDS': '0',
      'TIME': _time,
      'TITLE': _title,
      'YEAR': int.parse(_projectCreated.split('-').first),
      'ZIP': _post,
    });
  }

  void removeExistingImage({required int fileId}) async {
    //

    final currentImages = <SupplierInfoFileModel>[..._currentProjectImages];
    final deleted = <int>[..._deletedProjectPictures];

    currentImages.removeWhere((element) => element.id == fileId);
    deleted.add(fileId);

    currentProjectImages = currentImages;
    deletedProjectPictures = deleted;

    //
  }

  void removeAddedImages({required int index}) {
    //

    final uploadedImages = <File>[..._projectImages];
    uploadedImages.removeAt(index);

    projectImages = uploadedImages;

    //
  }

  Future<void> deleteShowCasePicture() async {
    //

    final projectService = context.read<ProjectsService>();

    for (final id in _deletedProjectPictures) {
      await projectService.deleteShowCasePicture(fileId: id);
    }

    //
  }

  Future<void> editProject() async {
    //
    final projectService = context.read<ProjectsService>();

    await updateShowCaseProject();

    if (_deletedProjectPictures.isNotEmpty) {
      await deleteShowCasePicture();
    }

    if (_projectImages.isNotEmpty) {
      await projectService.updateShowCaseProjectPicture(
        files: _projectImages,
        projectId: _currentProject!.id!,
        description: description,
      );
    }
  }
}
