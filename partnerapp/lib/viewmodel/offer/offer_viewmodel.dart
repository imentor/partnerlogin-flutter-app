import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:math' as math;

import 'package:Haandvaerker.dk/model/create_offer/list_price_partner_ai_v2_model.dart';
import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/offer/offer_pagination_model.dart';
import 'package:Haandvaerker.dk/model/offer_model.dart';
import 'package:Haandvaerker.dk/model/offer_versions_list_model.dart';
import 'package:Haandvaerker.dk/model/partner_haa_model.dart';
import 'package:Haandvaerker.dk/model/partner_profil_model.dart';
import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/model/task_type/task_type_model.dart';
import 'package:Haandvaerker.dk/model/terms_response_model.dart';
import 'package:Haandvaerker.dk/model/tilbud_model.dart';
import 'package:Haandvaerker.dk/model/user_partner_model.dart';
import 'package:Haandvaerker.dk/model/wizard/list_price_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_new_version_model.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/booking_wizard/booking_wizard_service.dart';
import 'package:Haandvaerker.dk/services/client/client_service.dart';
import 'package:Haandvaerker.dk/services/contract/contract_service.dart';
import 'package:Haandvaerker.dk/services/file/file_service.dart';
import 'package:Haandvaerker.dk/services/offer/offer_service.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/utils/iterable_utils.dart';
import 'package:Haandvaerker.dk/utils/try_catch_wrapper.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/partner/user_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:file_selector/file_selector.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

import '../../model/get_types_response_model.dart';

enum OfferProcessState { add, edit }

enum ClientType { customer, partner, contact }

enum TermType { bilag, betingelser, note, terms }

class OfferViewModel extends BaseViewModel {
  OfferViewModel({required this.context});

  Map<TermType, String> termType = {
    TermType.bilag: 'bilag',
    TermType.betingelser: 'betingelser',
    TermType.note: 'note',
    TermType.terms: 'terms',
  };

  BuildContext context;

  bool enableConditionValidator = false;

  TilbudModel _editOffer = TilbudModel();
  TilbudModel get editOffer => _editOffer;

  OfferProcessState _currentState = OfferProcessState.add;
  OfferProcessState get currentState => _currentState;

  List<TermsResponseModel> _noteList = [];
  List<TermsResponseModel> get noteList => _noteList;

  List<TermsResponseModel> _conditions = [];
  List<TermsResponseModel> get conditions => _conditions;

  List<String> _bilagfiles = [];
  List<String> get bilagfiles => _bilagfiles;

  TermsResponseModel? _notes;
  TermsResponseModel? get notes => _notes;

  TermsResponseModel? _appendix;
  TermsResponseModel? get appendix => _appendix;

  TermsResponseModel? _condition;
  TermsResponseModel? get condition => _condition;

  PartnerProfileModel? _partnerProfile;
  PartnerProfileModel? get partnerProfile => _partnerProfile;

  OfferVersionListModel? _offerVersionList;
  OfferVersionListModel? get offerVersionList => _offerVersionList;

  PartnerHaaModel? _partnerHaa;
  PartnerHaaModel? get partnerHaa => _partnerHaa;

  String? _tilbudTemplateHtml;
  String? get tilbudTemplateHtml => _tilbudTemplateHtml;

  String _appid = '0';
  String get appid => _appid;

  ClientType _type = ClientType.contact;
  ClientType get type => _type;

  Map<String?, List<String>> _taskFiles = <String?, List<String>>{};
  Map<String?, List<String>> get taskFiles => _taskFiles;

  List<TermsResponseModel> _termList = [];
  List<TermsResponseModel> get termList => _termList;

  Info? _quotationForm = Info();
  Info? get quotationForm => _quotationForm;

  String _defaultTaC = 'Standard terms and conditions';
  String? get defaultTaC => _defaultTaC;

  String _defaultCondition = 'Standard terms and conditions';
  String get defaultCondition => _defaultCondition;

  String _defaultNote = 'Standard terms and conditions';
  String get defaultNote => _defaultNote;

  String _defaultAppendix = 'Standard terms and conditions';
  String get defaultAppendix => _defaultAppendix;

  String? _tilbudId = '0';
  String? get tilbudId => _tilbudId;

  String _headerName = '';
  String get headerName => _headerName;

  File? _headerImage;
  File? get headerImage => _headerImage;

  String? _headerImageUrl;
  String? get headerImageUrl => _headerImageUrl;

  String? _customerComments;
  String? get customerComments => _customerComments;

  String _offerType = '';
  String get offerType => _offerType;

  TermsResponseModel _chosenTerm = TermsResponseModel();
  TermsResponseModel get chosenTerm => _chosenTerm;

  UserPartnerModel? _userPartner;
  UserPartnerModel? get userPartner => _userPartner;

  String? _customerAddress = '';
  String? get customerAddress => _customerAddress;

  String _typeApi = '';
  String get typeApi => _typeApi;

  int? _projectId = 0;
  int? get projectId => _projectId;

  List<int?> _reviewIds = [];
  List<int?> get reviewIds => _reviewIds;

  int _customerToggleIndex = 0;
  int get customerToggleIndex => _customerToggleIndex;

  int _selectedIndustryLength = 0;
  int get selectedIndustryLength => _selectedIndustryLength;

  dynamic _selectedModel;
  dynamic get selectedModel => _selectedModel;

  File? _offerFile;
  File? get offerFile => _offerFile;

  String? _offerFileServerName = "";
  String? get offerFileServerName => _offerFileServerName;

  List<String> userExtensions = [
    "jpg",
    "png",
    "jpeg",
    "pdf",
    "doc",
    "docx",
    "xls",
    "xlsx",
    "txt",
    "csv"
  ];

  OfferPaginationModel _offerListPagination = OfferPaginationModel();
  OfferPaginationModel get offerListPagination => _offerListPagination;

  int _createOfferStep = 0;
  int get createOfferStep => _createOfferStep;

  int _createOfferProductsStep = 0;
  int get createOfferProductsStep => _createOfferProductsStep;

  int _stepTwoSubStep = 0;
  int get stepTwoSubStep => _stepTwoSubStep;

  Map<String, dynamic> _createOfferWizardForms = {
    "uploadOfferSignatureValid": "30"
  };
  Map<String, dynamic> get createOfferWizardForms => _createOfferWizardForms;

  Map<String, String> _wizardProductAnswers = {};
  Map<String, String> get wizardProductAnswers => _wizardProductAnswers;

  List<Data> _industryFullList = [];
  List<Data> get industryFullList => _industryFullList;

  List<AllWizardText> _industryList = [];
  List<AllWizardText> get industryList => _industryList;

  List<AllWizardText> _filteredIndustries = [];
  List<AllWizardText> get filteredIndustries => _filteredIndustries;

  List<AllWizardText> _selectedIndustries = [];
  List<AllWizardText> get selectedIndustries => _selectedIndustries;

  List<ProductWizardModelV2> _wizardQuestions = [];
  List<ProductWizardModelV2> get wizardQuestions => _wizardQuestions;

  List<Map<String, dynamic>> _industryDescriptions = [];
  List<Map<String, dynamic>> get industryDescriptions => _industryDescriptions;

  List<CreateOfferWizardModel> _existingCreateOfferWizardList = [];
  List<CreateOfferWizardModel> get existingCreateOfferWizardList =>
      _existingCreateOfferWizardList;

  CreateOfferWizardModel? _existingCreateOfferWizard;
  CreateOfferWizardModel? get existingCreateOfferWizard =>
      _existingCreateOfferWizard;

  PartnerProjectsModel _projectWizard = PartnerProjectsModel();
  PartnerProjectsModel get projectWizard => _projectWizard;

  List<ProjectTaskTypes> _taskTypes = [];
  List<ProjectTaskTypes> get taskTypes => _taskTypes;

  //UPLOAD OFFER STATES
  int _uploadOfferStep = 0;
  int get uploadOfferStep => _uploadOfferStep;

  List<PlatformFile> _uploadOfferFiles = [];
  List<PlatformFile> get uploadOfferFiles => _uploadOfferFiles;

  List<XFile> _selectedOfferFiles = [];
  List<XFile> get selectedOfferFiles => _selectedOfferFiles;

  Map<String, dynamic> _uploadOfferForms = {"uploadOfferSignatureValid": "30"};
  Map<String, dynamic> get uploadOfferForms => _uploadOfferForms;

  bool _isOfferUploaded = false;
  bool get isOfferUploaded => _isOfferUploaded;
  //

  //VIEW OFFER STATES
  OfferModel? _viewedOffer;
  OfferModel? get viewedOffer => _viewedOffer;

  bool _seeMore = true;
  bool get seeMore => _seeMore;

  bool _isNewOfferVersion = false;
  bool get isNewOfferVersion => _isNewOfferVersion;

  bool _isUploadOfferUpdate = false;
  bool get isUploadOfferUpdate => _isUploadOfferUpdate;

  bool _isCreateOfferLoading = false;
  bool get isCreateOfferLoading => _isCreateOfferLoading;

  bool _isUploadOfferLoading = false;
  bool get isUploadOfferLoading => _isUploadOfferLoading;

  String _csUserId = '';
  String get csUserId => _csUserId;

  int _offerVersionId = 0;
  int get offerVersionId => _offerVersionId;

  OfferModel? _viewedSignatureOffer;
  OfferModel? get viewedSignatureOffer => _viewedSignatureOffer;

  List<ListPriceMinbolig> _viewedSignatureOfferListPrice = [];
  List<ListPriceMinbolig> get viewedSignatureOfferListPrice =>
      _viewedSignatureOfferListPrice;

  Map<int, bool> _showDescription = {};
  Map<int, bool> get showDescription => _showDescription;

  Map<String, bool> _isValidateError = {};
  Map<String, bool> get isValidateError => _isValidateError;

  int _viewSignatureOfferVersionId = 0;
  int get viewSignatureOfferVersionId => _viewSignatureOfferVersionId;

  double _tempTotalPrice = 0;
  double get tempTotalPrice => _tempTotalPrice;

  //UPDATE OFFER
  set isValidateError(Map<String, bool> value) {
    _isValidateError = value;

    notifyListeners();
  }

  set viewSignatureOfferVersionId(int value) {
    _viewSignatureOfferVersionId = value;

    notifyListeners();
  }

  set offerVersionId(int value) {
    _offerVersionId = value;

    notifyListeners();
  }

  set csUserId(String value) {
    _csUserId = value;

    notifyListeners();
  }

  set uploadOfferStep(int uploadOfferStep) {
    _uploadOfferStep = uploadOfferStep;

    notifyListeners();
  }

  set uploadOfferForms(Map<String, dynamic> uploadOfferForms) {
    _uploadOfferForms = uploadOfferForms;

    notifyListeners();
  }

  set seeMore(bool seeMore) {
    _seeMore = seeMore;

    notifyListeners();
  }

  set isNewOfferVersion(bool isNewOfferVersion) {
    _isNewOfferVersion = isNewOfferVersion;
    notifyListeners();
  }

  set isUploadOfferUpdate(bool isUploadOfferUpdate) {
    _isUploadOfferUpdate = isUploadOfferUpdate;
    notifyListeners();
  }

  set isCreateOfferLoading(bool isCreateOfferLoading) {
    _isCreateOfferLoading = isCreateOfferLoading;
    notifyListeners();
  }

  set isUploadOfferLoading(bool loading) {
    _isUploadOfferLoading = loading;
    notifyListeners();
  }

  set tempTotalPrice(double value) {
    _tempTotalPrice = value;
    notifyListeners();
  }

  set filteredIndustries(List<AllWizardText> newList) {
    _filteredIndustries = newList;

    notifyListeners();
  }

  set industryList(List<AllWizardText> newList) {
    _industryList = newList;

    notifyListeners();
  }

  set offerFile(File? file) {
    _offerFile = file;
    notifyListeners();
  }

  /// stores the offer file url after it was uploaded to the server
  set offerFileServerName(String? offerFileServerName) {
    _offerFileServerName = offerFileServerName;
    notifyListeners();
  }

  set editOffer(TilbudModel value) {
    _editOffer = value;
    notifyListeners();
  }

  set headerImageUrl(String? value) {
    _headerImageUrl = value;
    notifyListeners();
  }

  set customerComments(String? value) {
    _customerComments = value;
    notifyListeners();
  }

  set currentState(OfferProcessState value) {
    _currentState = value;
    notifyListeners();
  }

  set notes(TermsResponseModel? value) {
    _notes = value;
    notifyListeners();
  }

  set appendix(TermsResponseModel? value) {
    _appendix = value;
    notifyListeners();
  }

  set condition(TermsResponseModel? value) {
    _condition = value;
    notifyListeners();
  }

  set tilbudTemplateHtml(String? value) {
    _tilbudTemplateHtml = value;
    notifyListeners();
  }

  set defaultCondition(String value) {
    _defaultCondition = value;
    notifyListeners();
  }

  set defaultAppendix(String value) {
    _defaultAppendix = value;
    notifyListeners();
  }

  set defaultNote(String value) {
    _defaultNote = value;
    notifyListeners();
  }

  set appid(String value) {
    _appid = value;
    notifyListeners();
  }

  set selectedModel(dynamic value) {
    _selectedModel = value;
    notifyListeners();
  }

  set customerToggleIndex(int value) {
    _customerToggleIndex = value;
    notifyListeners();
  }

  set reviewIds(List<int?> value) {
    _reviewIds = value;
    notifyListeners();
  }

  set type(ClientType value) {
    _type = value;
    notifyListeners();
  }

  set customerAddress(String? value) {
    _customerAddress = value;
    notifyListeners();
  }

  set typeApi(String value) {
    _typeApi = value;
    notifyListeners();
  }

  set projectId(int? value) {
    _projectId = value;
    notifyListeners();
  }

  set userPartner(UserPartnerModel? value) {
    _userPartner = value;
    notifyListeners();
  }

  set chosenTerm(TermsResponseModel value) {
    _chosenTerm = value;
    notifyListeners();
  }

  set headerImage(File? value) {
    _headerImage = value;
    notifyListeners();
  }

  set headerName(String value) {
    _headerName = value;
    notifyListeners();
  }

  set offerType(String value) {
    _offerType = value;
    notifyListeners();
  }

  set tilbudId(String? value) {
    _tilbudId = value;
    notifyListeners();
  }

  set quotationForm(Info? value) {
    _quotationForm = value;
    notifyListeners();
  }

  set showDescription(Map<int, bool> value) {
    _showDescription = value;
    notifyListeners();
  }

  Future<void> getViewedSignatureOffer({required int offerId}) async {
    final service = context.read<OfferService>();

    setBusy(true);

    final response = await service.getOfferById(offerId: offerId);

    if (response != null) {
      _viewedSignatureOffer = OfferModel.fromJson(response.data);

      final listPriceResponse = await service.getListPriceMinboligOfferVersion(
          contactId: _viewedSignatureOffer?.homeowner?.id ?? 0,
          offerVersionId: viewSignatureOfferVersionId,
          projectId: _viewedSignatureOffer?.contractType == "Fagentreprise"
              ? (_viewedSignatureOffer?.parentProjectId ?? 0)
              : (_viewedSignatureOffer?.projectId ?? 0));

      if (listPriceResponse != null) {
        _viewedSignatureOfferListPrice = [
          ...ListPriceMinbolig.fromCollection(
                  (listPriceResponse.data as Map<String, dynamic>)['result'])
              .where((element) => int.parse(element.offerId ?? '0') == offerId)
        ];
      }
    }

    setBusy(false);

    notifyListeners();
  }

  Future<void> mergeFiles() async {
    var offerService = context.read<OfferService>();

    List<Map<String, dynamic>> files = [];

    final tempForms = {..._uploadOfferForms};

    if (_uploadOfferFiles.isNotEmpty) {
      if (_uploadOfferFiles.length == 1) {
        final file = _uploadOfferFiles.first;
        if (path.extension(file.name).contains('pdf')) {
          final file = _uploadOfferFiles.first;
          files.add({
            "fileName": file.name,
            "fileContent": base64Encode(await File(file.path!).readAsBytes()),
          });

          tempForms['fileName'] = files.first['fileName'];
          tempForms['file'] = files.first['fileContent'];

          final fileToViewFile = await Formatter.createImageFileFromString(
              prename: files.first['fileName'],
              uniqueId: math.Random().nextInt(10000),
              encodedStr: files.first['fileContent']);

          tempForms['fileToViewPath'] = fileToViewFile;
        } else {
          for (var element in _uploadOfferFiles) {
            files.add({
              "fileName": element.name,
              "fileContent":
                  base64Encode(await File(element.path!).readAsBytes()),
            });
          }

          final response =
              await offerService.fileMerger(payload: {"files": files});

          if (!response.success!) {
            throw ApiException(message: response.message ?? '');
          }

          tempForms['fileName'] = response.data['fileName'];
          tempForms['file'] = response.data['file'];

          final fileToViewFile = await Formatter.createImageFileFromString(
              prename: response.data['fileName'],
              uniqueId: math.Random().nextInt(10000),
              encodedStr: response.data['file']);

          tempForms['fileToViewPath'] = fileToViewFile;
        }
      } else {
        for (var element in _uploadOfferFiles) {
          files.add({
            "fileName": element.name,
            "fileContent":
                base64Encode(await File(element.path!).readAsBytes()),
          });
        }

        final response =
            await offerService.fileMerger(payload: {"files": files});

        if (!response.success!) {
          throw ApiException(message: response.message ?? '');
        }

        tempForms['fileName'] = response.data['fileName'];
        tempForms['file'] = response.data['file'];

        final fileToViewFile = await Formatter.createImageFileFromString(
            prename: response.data['fileName'],
            uniqueId: math.Random().nextInt(10000),
            encodedStr: response.data['file']);

        tempForms['fileToViewPath'] = fileToViewFile;
      }
    } else if (_selectedOfferFiles.isNotEmpty) {
      if (_selectedOfferFiles.length == 1) {
        final file = _selectedOfferFiles.first;
        if (file.mimeType!.split('/').last == 'pdf') {
          files.add({
            "fileName":
                '${file.name.replaceAll(RegExp('[^A-Za-z0-9]'), '')}.${file.mimeType!.split('/').last}',
            "fileContent": base64Encode(await file.readAsBytes()),
          });

          tempForms['fileName'] = files.first['fileName'];
          tempForms['file'] = files.first['fileContent'];

          final fileToViewFile = await Formatter.createImageFileFromString(
              prename: files.first['fileName'],
              uniqueId: math.Random().nextInt(10000),
              encodedStr: files.first['fileContent']);

          tempForms['fileToViewPath'] = fileToViewFile;
        } else {
          for (var element in _selectedOfferFiles) {
            files.add({
              "fileName":
                  '${element.name.replaceAll(RegExp('[^A-Za-z0-9]'), '')}.${element.mimeType!.split('/').last}',
              "fileContent": base64Encode(await element.readAsBytes()),
            });
          }

          final response =
              await offerService.fileMerger(payload: {"files": files});

          if (!response.success!) {
            throw ApiException(message: response.message ?? '');
          }

          tempForms['fileName'] = response.data['fileName'];
          tempForms['file'] = response.data['file'];

          final fileToViewFile = await Formatter.createImageFileFromString(
              prename: response.data['fileName'],
              uniqueId: math.Random().nextInt(10000),
              encodedStr: response.data['file']);

          tempForms['fileToViewPath'] = fileToViewFile;
        }
      } else {
        for (var element in _selectedOfferFiles) {
          files.add({
            "fileName":
                '${element.name.replaceAll(RegExp('[^A-Za-z0-9]'), '')}.${element.mimeType!.split('/').last}',
            "fileContent": base64Encode(await element.readAsBytes()),
          });
        }

        final response =
            await offerService.fileMerger(payload: {"files": files});

        if (!response.success!) {
          throw ApiException(message: response.message ?? '');
        }

        tempForms['fileName'] = response.data['fileName'];
        tempForms['file'] = response.data['file'];

        final fileToViewFile = await Formatter.createImageFileFromString(
            prename: response.data['fileName'],
            uniqueId: math.Random().nextInt(10000),
            encodedStr: response.data['file']);

        tempForms['fileToViewPath'] = fileToViewFile;
      }
    }

    _uploadOfferForms = tempForms;
    _uploadOfferStep += 1;

    notifyListeners();
  }

  Future<void> mergeFilesFastOffer() async {
    final offerService = context.read<OfferService>();
    List<Map<String, dynamic>> files = [];
    final tempForms = {..._createOfferWizardForms};

    if (_uploadOfferFiles.isNotEmpty) {
      if (_uploadOfferFiles.length == 1) {
        final file = _uploadOfferFiles.first;
        if (path.extension(file.name).contains('pdf')) {
          final file = _uploadOfferFiles.first;
          files.add({
            "fileName": file.name,
            "fileContent": base64Encode(await File(file.path!).readAsBytes()),
          });

          tempForms['fileName'] = files.first['fileName'];
          tempForms['file'] = files.first['fileContent'];

          final fileToViewFile = await Formatter.createImageFileFromString(
              prename: files.first['fileName'],
              uniqueId: math.Random().nextInt(10000),
              encodedStr: files.first['fileContent']);

          tempForms['fileToViewPath'] = fileToViewFile;
        } else {
          for (var element in _uploadOfferFiles) {
            files.add({
              "fileName": element.name,
              "fileContent":
                  base64Encode(await File(element.path!).readAsBytes()),
            });
          }

          final response =
              await offerService.fileMerger(payload: {"files": files});

          if (!response.success!) {
            throw ApiException(message: response.message ?? '');
          }

          tempForms['fileName'] = response.data['fileName'];
          tempForms['file'] = response.data['file'];

          final fileToViewFile = await Formatter.createImageFileFromString(
              prename: response.data['fileName'],
              uniqueId: math.Random().nextInt(10000),
              encodedStr: response.data['file']);

          tempForms['fileToViewPath'] = fileToViewFile;
        }
      } else {
        for (var element in _uploadOfferFiles) {
          files.add({
            "fileName": element.name,
            "fileContent":
                base64Encode(await File(element.path!).readAsBytes()),
          });
        }

        final response =
            await offerService.fileMerger(payload: {"files": files});

        if (!response.success!) {
          throw ApiException(message: response.message ?? '');
        }

        tempForms['fileName'] = response.data['fileName'];
        tempForms['file'] = response.data['file'];

        final fileToViewFile = await Formatter.createImageFileFromString(
            prename: response.data['fileName'],
            uniqueId: math.Random().nextInt(10000),
            encodedStr: response.data['file']);

        tempForms['fileToViewPath'] = fileToViewFile;
      }
    } else if (_selectedOfferFiles.isNotEmpty) {
      if (_selectedOfferFiles.length == 1) {
        final file = _selectedOfferFiles.first;
        if (file.mimeType!.split('/').last == 'pdf') {
          files.add({
            "fileName":
                '${file.name.replaceAll(RegExp('[^A-Za-z0-9]'), '')}.${file.mimeType!.split('/').last}',
            "fileContent": base64Encode(await file.readAsBytes()),
          });

          tempForms['fileName'] = files.first['fileName'];
          tempForms['file'] = files.first['fileContent'];

          final fileToViewFile = await Formatter.createImageFileFromString(
              prename: files.first['fileName'],
              uniqueId: math.Random().nextInt(10000),
              encodedStr: files.first['fileContent']);

          tempForms['fileToViewPath'] = fileToViewFile;
        } else {
          for (var element in _selectedOfferFiles) {
            files.add({
              "fileName":
                  '${element.name.replaceAll(RegExp('[^A-Za-z0-9]'), '')}.${element.mimeType!.split('/').last}',
              "fileContent": base64Encode(await element.readAsBytes()),
            });
          }

          final response =
              await offerService.fileMerger(payload: {"files": files});

          if (!response.success!) {
            throw ApiException(message: response.message ?? '');
          }

          tempForms['fileName'] = response.data['fileName'];
          tempForms['file'] = response.data['file'];

          final fileToViewFile = await Formatter.createImageFileFromString(
              prename: response.data['fileName'],
              uniqueId: math.Random().nextInt(10000),
              encodedStr: response.data['file']);

          tempForms['fileToViewPath'] = fileToViewFile;
        }
      } else {
        for (var element in _selectedOfferFiles) {
          files.add({
            "fileName":
                '${element.name.replaceAll(RegExp('[^A-Za-z0-9]'), '')}.${element.mimeType!.split('/').last}',
            "fileContent": base64Encode(await element.readAsBytes()),
          });
        }

        final response =
            await offerService.fileMerger(payload: {"files": files});

        if (!response.success!) {
          throw ApiException(message: response.message ?? '');
        }

        tempForms['fileName'] = response.data['fileName'];
        tempForms['file'] = response.data['file'];

        final fileToViewFile = await Formatter.createImageFileFromString(
            prename: response.data['fileName'],
            uniqueId: math.Random().nextInt(10000),
            encodedStr: response.data['file']);

        tempForms['fileToViewPath'] = fileToViewFile;
      }
    }

    _createOfferWizardForms = tempForms;
    notifyListeners();
  }

  Future<void> uploadNewOfferFiles({required int tenderId}) async {
    final fileService = context.read<FilesService>();
    var offerService = context.read<OfferService>();

    Map<String, dynamic> jsonData = {
      'parentId': tenderId,
      'category': 'Offer',
      'offerId': _viewedOffer!.id!,
    };

    for (int index = 0; index < uploadOfferFiles.length; index++) {
      final fileData = MultipartFile.fromFileSync(uploadOfferFiles[index].path!,
          filename: uploadOfferFiles[index].name);

      jsonData.putIfAbsent('files[$index]', () => fileData);
    }

    final formDataOne = FormData.fromMap(jsonData);

    await fileService.uploadMultipleFiles(payload: formDataOne);

    final offerResponse =
        await offerService.getOfferById(offerId: _viewedOffer!.id!);

    if (offerResponse != null) {
      _viewedOffer = OfferModel.fromJson(offerResponse.data);
    } else {
      _viewedOffer = null;
    }

    _uploadOfferFiles = [];

    notifyListeners();
  }

  Future<void> deleteOfferFiles({required int fileId}) async {
    final fileService = context.read<FilesService>();
    var offerService = context.read<OfferService>();

    await fileService.deleteFile(fileId: fileId);

    final offerResponse =
        await offerService.getOfferById(offerId: _viewedOffer!.id!);

    if (offerResponse != null) {
      _viewedOffer = OfferModel.fromJson(offerResponse.data);
    } else {
      _viewedOffer = null;
    }

    notifyListeners();
  }

  Future<void> updateOfferPrice({required double price}) async {
    var offerService = context.read<OfferService>();

    final response = await offerService.updateOffer(
      payload: {
        'offer_id': _viewedOffer!.id,
        'subTotal': price,
        'total': price * 1.25,
        'vat': price * .25,
      },
    );

    _viewedOffer = OfferModel.fromJson(response.data);

    notifyListeners();
  }

  Future<void> getOfferById({required int offerId}) async {
    setBusy(true);
    var offerService = context.read<OfferService>();

    final offerResponse = await offerService.getOfferById(offerId: offerId);

    if (offerResponse != null) {
      _viewedOffer = OfferModel.fromJson(offerResponse.data);
      _uploadOfferFiles = [];
    } else {
      _viewedOffer = null;
    }

    setBusy(false);

    notifyListeners();
  }

  Future<void> getOfferByIdV2({required int offerId}) async {
    final createOfferService = context.read<OfferService>();
    final response = await createOfferService.getOfferById(offerId: offerId);

    if (response != null) {
      // await createOfferService.getListPriceMinboligOfferVersion(
      //     offerVersionId: response.data['OFFER_VERSIONS'][0]['ID'],
      //     projectId: projectId);
      _offerVersionList = OfferVersionListModel.fromJson(response.data);

      if (Platform.isAndroid) {
        final appDirectory = await getDownloadsDirectory();

        if (_offerVersionList?.offerFile != null) {
          final randid = math.Random().nextInt(10000);
          String savePath = '${appDirectory!.path}/offer/$randid.pdf';
          pdfDownload(url: _offerVersionList!.offerFile!, savePath: savePath);
          _selectedOfferFiles.add(XFile(savePath, mimeType: 'pdf'));
        } else if (_offerVersionList?.offerFiles != null &&
            _offerVersionList!.offerFiles!.isNotEmpty) {
          final randid = math.Random().nextInt(10000);
          for (var i = 0; i < _offerVersionList!.offerFiles!.length; i++) {
            String savePath = '${appDirectory!.path}/offer$i/$randid.pdf';
            pdfDownload(
                url: _offerVersionList!.offerFiles![i], savePath: savePath);

            _selectedOfferFiles.add(XFile(savePath, mimeType: 'pdf'));
          }
        }
      } else {
        Directory appDirectory = await getApplicationDocumentsDirectory();

        if (_offerVersionList?.offerFile != null) {
          final randid = math.Random().nextInt(10000);
          String savePath = '${appDirectory.path}/offer/$randid.pdf';
          pdfDownload(url: _offerVersionList!.offerFile!, savePath: savePath);
          _uploadOfferFiles.add(
              PlatformFile(name: 'offer/$randid.pdf', size: 0, path: savePath));
        } else if (_offerVersionList?.offerFiles != null &&
            _offerVersionList!.offerFiles!.isNotEmpty) {
          final randid = math.Random().nextInt(10000);
          for (var i = 0; i < _offerVersionList!.offerFiles!.length; i++) {
            String savePath = '${appDirectory.path}/offer$i/$randid.pdf';
            pdfDownload(
                url: _offerVersionList!.offerFiles![i], savePath: savePath);

            _uploadOfferFiles.add(PlatformFile(
                name: 'offer/$randid.pdf', size: 0, path: savePath));
          }
        }
      }

      notifyListeners();
    }
  }

  Future<void> pdfDownload({required String url, required savePath}) async {
    final Dio dio = Dio();

    await tryCatchWrapper(
      context: context,
      function: dio.download(url, savePath),
    );
  }

  Future<void> updateUploadOfferForms(
      {required String key, required dynamic value}) async {
    final tempAnswers = {..._uploadOfferForms};

    if (tempAnswers.containsKey(key)) {
      tempAnswers.update(key, (old) => value);
    } else {
      tempAnswers.putIfAbsent(key, () => value);
    }

    _uploadOfferForms = {...tempAnswers};

    notifyListeners();
  }

  Future<void> updateUploadOfferSteps({required int uploadOfferStep}) async {
    _uploadOfferStep = uploadOfferStep;

    notifyListeners();
  }

  Future<void> addOrDeleteOfferFiles(
      {List<PlatformFile>? files, int? index}) async {
    if (files != null) {
      _uploadOfferFiles.addAll(files);
    } else if (index != null) {
      _uploadOfferFiles.removeAt(index);
    }

    notifyListeners();
  }

  Future<void> addOrDeleteSelectedOfferFiles(
      {List<XFile>? files, int? index}) async {
    if (files != null) {
      _selectedOfferFiles.addAll(files);
    } else if (index != null) {
      _selectedOfferFiles.removeAt(index);
    }

    notifyListeners();
  }

  Future<void> createUploadOffer(
      {required PartnerJobModel job,
      required String signature,
      required dynamic jobsList}) async {
    var offerService = context.read<OfferService>();
    // final service = context.read<BookingWizardService>();

    final isFagen = job.contractType == 'Fagentreprise';

    double uploadOfferPriceWithoutVat = 0;
    double vatValue = 0;
    double uploadOfferPriceWithVat = 0;

    List<Map<String, dynamic>> saveListFormPayloads = [];
    Map<String, dynamic> offerNewVersionPayload = {};
    Map<String, dynamic> editOfferPayload = {};

    if (uploadOfferForms['priceHasVat']) {
      uploadOfferPriceWithoutVat = uploadOfferForms['uploadOfferPrice'] -
          (uploadOfferForms['uploadOfferPrice'] * .25);

      vatValue = uploadOfferForms['uploadOfferPrice'] * .25;

      uploadOfferPriceWithVat = uploadOfferForms['uploadOfferPrice'];
    } else {
      uploadOfferPriceWithoutVat = uploadOfferForms['uploadOfferPrice'];

      vatValue = uploadOfferForms['uploadOfferPrice'] * .25;

      uploadOfferPriceWithVat = uploadOfferPriceWithoutVat + vatValue;
    }

    List<ListPricePartnerAiV2Model> listPrice = [];

    listPrice = await offerService.getListPricePartnerAiV2(
        parentProjectId:
            isFagen ? int.parse(job.parentProject?.id ?? '0') : job.projectId,
        contactId: job.customerId ?? 0);

    Map<String, dynamic> createUploadOfferForm = {
      "projectId": job.projectId,
      "type": 'fast-offer',
      "createdFrom": "partner-app",
      "offers": [
        {
          "title":
              "Tilbud på projektnr. ${job.projectName} til ${job.projectDescription}",
          "description": job.projectDescription,
          "subtotalPrice": uploadOfferPriceWithoutVat,
          "start_date": uploadOfferForms['uploadOfferProjectStartDate'],
          "vat": vatValue,
          "totalPrice": uploadOfferPriceWithVat,
          "expiry": _uploadOfferForms['uploadOfferSignatureValid'],
          "fileName": _uploadOfferForms['fileName'],
          "file": _uploadOfferForms['file'],
          "tasks": [
            {
              "title": '',
              "description": '',
              "unit": '',
              "value": '',
              "quantity": ''
            }
          ]
        }
      ]
    };

    if (((job.submittedJobIndustryIds?.isNotEmpty ?? false) &&
        (job.submittedJobIndustryIds?.first == '1062'))) {
      createUploadOfferForm.putIfAbsent(
          'parentProjectId', () => job.parentProject!.id);
      createUploadOfferForm.putIfAbsent('jobIndustry', () => '1062');
    }

    if (isFagen) {
      if (jobsList != null && jobsList is List) {
        for (final i in jobsList) {
          final jobIndustry = i.split(':').first;
          String industryId = '';

          for (final a in i.split(':').last.split('-')) {
            industryId = a;

            Map<String, dynamic> saveListForm = {
              "project_id": isFagen ? job.parentProject?.id : job.projectId,
              "sub_project_id": '${isFagen ? job.projectId : 0}',
              "contact_id": job.customerId,
              "partner_id": job.partnerId,
              "calculation_id": 0,
              "industry_id": industryId,
              "total": uploadOfferPriceWithoutVat,
              "job_industry_id": jobIndustry,
              "weeks": uploadOfferForms['uploadOfferProjectEstimate'],
              "days": uploadOfferForms['uploadOfferProjectEstimate'],
              "start_date": uploadOfferForms['uploadOfferProjectStartDate'],
              "reviews_id": [],
              "job_description": {
                "submitted": listPrice
                    .firstWhere(
                        (element) =>
                            element.jobIndustry == jobIndustry &&
                            element.industry == industryId,
                        orElse: () => ListPricePartnerAiV2Model.defaults())
                    .series
              },
              "offer_id": job.offerId,
              "parent_offer_id": job.offerId,
              "signature": signature,
            };

            saveListFormPayloads.add(saveListForm);
          }
        }
      }
    } else {
      if (jobsList != null && jobsList is List) {
        for (final jobItem in jobsList) {
          for (final j in jobItem) {
            final jobIndustry = j.split(':').first;
            String industryId = '';
            inspect(jobsList);

            for (final a in j.split(':').last.split('-')) {
              industryId = a;

              Map<String, dynamic> saveListForm = {
                "project_id": isFagen ? job.parentProject?.id : job.projectId,
                "contact_id": job.customerId,
                "calculation_id": 0,
                "industry_id": industryId,
                "job_industry_id": jobIndustry,
                "total": uploadOfferPriceWithoutVat,
                "weeks": uploadOfferForms['uploadOfferProjectEstimate'],
                "start_date": uploadOfferForms['uploadOfferProjectStartDate'],
                "signature": signature,
                "days": uploadOfferForms['uploadOfferProjectEstimate'],
                "reviews_id": [],
                "sub_project_id": '${isFagen ? job.projectId : 0}',
                "job_description": listPrice
                    .firstWhere(
                        (element) =>
                            element.jobIndustry == jobIndustry &&
                            element.industry == industryId,
                        orElse: () => ListPricePartnerAiV2Model.defaults())
                    .series
              };

              saveListFormPayloads.add(saveListForm);
            }
          }
        }
      }
    }

    createUploadOfferForm['saveListPriceV2'] = saveListFormPayloads;

    // final createOfferResponse =
    if (_isNewOfferVersion) {
      offerNewVersionPayload = {
        'offerId': job.offerId,
        "projectId": job.projectId,
        'createdFrom': 'partner-app',
        'saveListPriceV2': saveListFormPayloads,
        'subTotalPrice': uploadOfferPriceWithoutVat,
        'vat': vatValue,
        "tasks": [
          {
            "title": '',
            "description": '',
            "unit": '',
            "value": '',
            "quantity": ''
          }
        ],
        'totalPrice': uploadOfferPriceWithVat,
        "weeks": uploadOfferForms['uploadOfferProjectEstimate'],
        "days": uploadOfferForms['uploadOfferProjectEstimate'],
        "expiry": _uploadOfferForms['uploadOfferSignatureValid'],
        "fileName": _uploadOfferForms['fileName'],
        "file": _uploadOfferForms['file'],
        "type": 'fast-offer',
        "signature": signature,
      };

      if (csUserId.isNotEmpty) {
        offerNewVersionPayload['csUserId'] = csUserId;
        offerNewVersionPayload['contact_id'] = job.customerId;

        if (offerVersionId > 0) {
          offerNewVersionPayload['offerVersionId'] = offerVersionId;
          await offerService.createOfferVersionByCs(
              payload: offerNewVersionPayload);
          _isOfferUploaded = true;
          _csUserId = '';
        } else {
          await offerService.createOfferByCs(payload: offerNewVersionPayload);
          _isOfferUploaded = true;
          _csUserId = '';
        }
      } else {
        await partnerOfferVersion(offerVersionPayload: offerNewVersionPayload);
        _isOfferUploaded = true;
      }
    } else if (_isUploadOfferUpdate) {
      if ((_offerVersionList?.offerSeen ?? true) == true) {
        _isOfferUploaded = false;
      } else {
        editOfferPayload = {
          'offerId': job.offerId,
          'subTotal': uploadOfferPriceWithoutVat,
          'vat': vatValue,
          'total': uploadOfferPriceWithVat,
          "file": _uploadOfferForms['file'],
          "fileName": _uploadOfferForms['fileName'],
        };
        await offerService
            .partnerOfferUpdate(updateOfferPayload: editOfferPayload)
            .then((value) {
          if (value?.success ?? false) {
            _isOfferUploaded = true;
          } else {
            _isOfferUploaded = false;
          }
        });
      }
    } else {
      await offerService.partnerOffer(payload: createUploadOfferForm);
      _isOfferUploaded = true;
    }

    // int offerId = createOfferResponse.first.id!;

    // for (var element in saveListFormPayloads) {
    //   element['offer_id'] = offerId;
    //   element['parent_offer_id'] = offerId;

    //   final saveListPriceResponse = await service.saveListPrice(form: element);

    //   if (!saveListPriceResponse.success!) {
    //     throw saveListPriceResponse.message!;
    //   }
    // }

    notifyListeners();
  }

  Future<void> getWizardProductQuestions() async {
    try {
      final List<ProductWizardModelV2> tempProductWizardList =
          []; // FIELDS THAT WILL SHOW THE QUESTIONAIRE

      final service = context.read<BookingWizardService>();

      final response = await service.getWizardProductsNewVersion(
          labels: selectedIndustries.map((e) => e.producttypeid).join(','));

      if (!response.success!) throw response.message!;

      final Map<String, dynamic> productsResponse =
          response.data as Map<String, dynamic>;

      productsResponse.forEach((key, value) {
        final productValues =
            ProductWizardFieldV2.fromCollection((value)['product']);

        if (value['wizard_product'] is List) {
          final List tempWizardProduct = value['wizard_product'] as List;
          List newWizardProductList = [];

          if (value['wizard_lead'] is List) {
            newWizardProductList = [
              ...(value['wizard_lead'] as List).map((index) {
                return tempWizardProduct[index];
              })
            ];
          } else if (value['wizard_lead'] is Map) {
            newWizardProductList = [
              ...(value['wizard_lead'] as Map).entries.map((index) {
                return tempWizardProduct[int.parse(index.value as String)];
              })
            ];
          }

          for (final p0 in newWizardProductList) {
            final List<ProductWizardFieldV2> productFields = [];

            if (p0 is List) {
              final wizardsList = ProductWizardFieldV2.fromCollection(p0);

              final List<ProductWizardFieldV2> loopingList = [];

              loopingList.addAll([
                ...wizardsList.where((element) => element.parentproductid == 0)
              ]);

              while (loopingList.isNotEmpty) {
                ProductWizardFieldV2 currentField = loopingList.first;

                if (currentField.productField == 2) {
                  productFields.add(currentField);
                  loopingList.remove(currentField);
                } else {
                  final values = productValues.where((a) =>
                      a.parentproductid.toString() == currentField.id &&
                      a.productField == 1);

                  final List<ProductWizardFieldV2> children = [];

                  for (final fieldValue in values) {
                    final subChildren = wizardsList.where(
                        (a) => a.parentproductid.toString() == fieldValue.id);

                    for (final sub in subChildren) {
                      if (sub.productField == 2) {
                        children.add(sub);
                      } else {
                        ProductWizardFieldV2 tempValue = sub;

                        final subValues = productValues.where((a) =>
                            a.parentproductid.toString() == sub.id &&
                            a.productField == 1);

                        tempValue.values = [...subValues];
                        children.add(tempValue);
                      }
                    }
                  }

                  currentField.values = [...values];
                  currentField.productField =
                      values.isEmpty ? 1 : currentField.productField;
                  currentField.children = [...children];

                  productFields.add(currentField);
                  loopingList.remove(currentField);
                }

                loopingList.addAll([
                  ...productValues.where((element) =>
                      element.parentproductid.toString() == currentField.id &&
                      element.productField != 1)
                ]);
              }
            }

            productFields.sort((a, b) => (a.id ?? '').compareTo(b.id ?? ''));

            tempProductWizardList.add(
              ProductWizardModelV2(
                productId: key,
                producttype: industryList
                    .firstWhere(
                      (element) => element.producttypeid == key,
                      orElse: () => AllWizardText.defaults(),
                    )
                    .producttypeDa,
                productFields: [
                  ...productFields
                      .where((element) => element.parentproductid == 0),
                  ...productFields
                      .where((element) => element.parentproductid != 0)
                ],
              ),
            );
          }
        } else if (value['wizard_product'] is Map<String, dynamic>) {
          final Map<String, dynamic> tempWizardProduct =
              value['wizard_product'] as Map<String, dynamic>;

          Map<String, dynamic> finalWizardProduct = {};

          List<String> wizardLeadsIndex = [];

          if (value['wizard_lead'] is List) {
            wizardLeadsIndex = [
              ...(value['wizard_lead'] as List).map((e) => '$e')
            ];
          } else if (value['wizard_lead'] is Map) {
            wizardLeadsIndex = [
              ...(value['wizard_lead'] as Map).entries.map((e) => '${e.value}')
            ];
          }

          finalWizardProduct = {
            for (String index in wizardLeadsIndex)
              index: tempWizardProduct[index]
          };

          final List<String> sortedKeys = finalWizardProduct.keys.toList()
            ..sort();

          final Map<String, dynamic> sortedWizardProduct = Map.fromEntries(
              sortedKeys.map((key) => MapEntry(key, finalWizardProduct[key])));

          sortedWizardProduct.forEach((key0, value0) {
            final List<ProductWizardFieldV2> productFields = [];

            final wizardsList = ProductWizardFieldV2.fromCollection(value0);

            final List<ProductWizardFieldV2> loopingList = [];

            loopingList.addAll([
              ...wizardsList.where((element) => element.parentproductid == 0)
            ]);

            while (loopingList.isNotEmpty) {
              final currentField = loopingList.first;

              if (currentField.productField == 2) {
                productFields.add(currentField);
                loopingList.remove(currentField);
              } else {
                final values = productValues.where((a) =>
                    a.parentproductid.toString() == currentField.id &&
                    a.productField == 1);

                final List<ProductWizardFieldV2> children = [];

                for (final fieldValue in values) {
                  final subChildren = wizardsList.where(
                      (a) => a.parentproductid.toString() == fieldValue.id);

                  for (final sub in subChildren) {
                    if (sub.productField == 2) {
                      children.add(sub);
                    } else {
                      ProductWizardFieldV2 tempSub = sub;

                      final subValues = productValues.where((a) =>
                          a.parentproductid.toString() == sub.id &&
                          a.productField == 1);
                      tempSub.values = [...subValues];
                      children.add(tempSub);
                    }
                  }
                }

                currentField.values = [...values];
                currentField.productField =
                    values.isEmpty ? 1 : currentField.productField;
                currentField.children = [...children];

                productFields.add(currentField);
                loopingList.remove(currentField);
              }

              loopingList.addAll([
                ...productValues.where((element) =>
                    element.parentproductid.toString() == currentField.id &&
                    element.productField != 1)
              ]);
            }

            productFields.sort((a, b) => (a.id ?? '').compareTo(b.id ?? ''));

            tempProductWizardList.add(
              ProductWizardModelV2(
                productId: key,
                producttype: industryList
                    .firstWhere(
                      (element) => element.producttypeid == key,
                      orElse: () => AllWizardText.defaults(),
                    )
                    .producttypeDa,
                productFields: [
                  ...productFields
                      .where((element) => element.parentproductid == 0),
                  ...productFields
                      .where((element) => element.parentproductid != 0)
                ],
              ),
            );
          });
        }
      });

      _stepTwoSubStep = 1;
      _wizardQuestions = tempProductWizardList
          .where((element) => element.productFields!.isNotEmpty)
          .toList();
      _wizardProductAnswers = {};
      _createOfferProductsStep = 0;

      notifyListeners();
    } catch (e) {
      log("message catch $e");
    }
  }

  Future<void> initCreateOffer() async {
    final wizardService = context.read<BookingWizardService>();

    resetCreateOfferWizards();
    final responses = await wizardService.getAllProjects();

    final projectsResponse = responses;
    if (!projectsResponse.success!) throw projectsResponse.message!;

    _existingCreateOfferWizardList =
        CreateOfferWizardModel.fromCollection(projectsResponse.data)
            .where((element) => element.step != 6)
            .toList();

    _isOfferUploaded = false;

    notifyListeners();
  }

  Future<void> initUploadOffer(
      {required int projectId, required int contactId}) async {
    _selectedOfferFiles = [];
    _uploadOfferStep = 0;
    _uploadOfferFiles = [];
    _uploadOfferForms = {"uploadOfferSignatureValid": "30"};

    notifyListeners();
  }

  Future<void> stepTwoSubStepUpdate() async {
    _stepTwoSubStep = 2;
    notifyListeners();
  }

  Future<void> submitWizardAnswers() async {
    try {
      final service = context.read<BookingWizardService>();

      final Map<String, dynamic> form = {..._wizardProductAnswers};

      final List<Map<String, dynamic>> industryDescriptions = [];

      final Map<String, dynamic> tempCreateOfferWizardForms = {
        ..._createOfferWizardForms
      };

      form.putIfAbsent('industries',
          () => _selectedIndustries.map((e) => e.producttypeid).join(','));
      form.putIfAbsent('calculation_id', () => 0);
      form.putIfAbsent('project_id', () => 0);
      form.putIfAbsent('update_text', () => 0);
      form.putIfAbsent(
          'userId', () => tempCreateOfferWizardForms['contact']['ID'] as int);
      form.putIfAbsent('contactId',
          () => tempCreateOfferWizardForms['contact']['ID'] as int);

      final response = await service.addWizardProduct(form: form);

      if (!response.success!) throw response.message!;

      for (var element in _selectedIndustries) {
        final listPriceResponse = await service.getListPriceAi(
            contactId: tempCreateOfferWizardForms['contact']['ID'] as int,
            industryId: element.producttypeid!);

        final listPriceMap = listPriceResponse.data as Map<String, dynamic>;

        final List<Map<String, dynamic>> subIndustry = [];

        listPriceMap.forEach((key, value) {
          final subIndustryTemp = value as Map<String, dynamic>;

          subIndustryTemp.forEach((key, value) {
            (value as List).removeWhere((val) => (val as String).isEmpty);
            subIndustry.add({
              "productTypeId": element.producttypeid,
              "subIndustryName": _industryFullList
                  .firstWhere(
                    (element) => element.branchId.toString() == key,
                    orElse: () => Data.defaults(),
                  )
                  .branche,
              "subIndustryId": key,
              "descriptions": value,
              "price": 0.0
            });
          });
        });

        industryDescriptions.add({
          "productName": element.producttypeDa,
          "productTypeId": element.producttypeid,
          "subIndustry": subIndustry
        });
      }

      // final titleResponse = await service.getListTitleAi(
      //     industryId: _selectedIndustries.map((e) => e.producttypeid).join(','),
      //     contactId: tempCreateOfferWizardForms['contact']['ID'] as int);
      // final summaryResponse = await service.getListSummaryAi(
      //     industryId: _selectedIndustries.map((e) => e.producttypeid).join(','),
      //     contactId: tempCreateOfferWizardForms['contact']['ID'] as int);
      final responses = await Future.wait([
        service.getListTitleAi(
            industryId:
                _selectedIndustries.map((e) => e.producttypeid).join(','),
            contactId: tempCreateOfferWizardForms['contact']['ID'] as int),
        service.getListSummaryAi(
            industryId:
                _selectedIndustries.map((e) => e.producttypeid).join(','),
            contactId: tempCreateOfferWizardForms['contact']['ID'] as int),
      ]);
      final titleResponse = responses[0];
      final summaryResponse = responses[1];

      if (!titleResponse.success!) throw titleResponse.message!;
      if (!summaryResponse.success!) throw summaryResponse.message!;

      tempCreateOfferWizardForms.putIfAbsent(
          'name',
          () =>
              (titleResponse.data as Map<String, dynamic>)['title'] as String);
      tempCreateOfferWizardForms.putIfAbsent(
          'description',
          () => (summaryResponse.data as Map<String, dynamic>)['summary']
              as String);

      _stepTwoSubStep = 2;
      _createOfferWizardForms = {...tempCreateOfferWizardForms};
      _industryDescriptions = [...industryDescriptions];
      notifyListeners();
    } catch (e) {
      if (e is ApiException) log("message catch ${e.message}");
    }
  }

  Future<void> createContactAndAddress({required bool isAffiliate}) async {
    try {
      final service = context.read<BookingWizardService>();

      Map<String, dynamic> tempCreateOfferWizardForms = {
        ..._createOfferWizardForms
      };

      final address = tempCreateOfferWizardForms['address'] as Address;

      final contactResponse = await service.createContact(
          name: 'John Doe',
          email: tempCreateOfferWizardForms['contact']['EMAIL'],
          mobile: '12345678',
          isAffiliate: isAffiliate);

      if (!contactResponse.success!) throw contactResponse.message!;

      tempCreateOfferWizardForms.update(
          'contact', (value) => contactResponse.data['contact']);

      final addressResponse = await service.createAddress(
          contactId: tempCreateOfferWizardForms['contact']['ID'] as int,
          addressTitle: address.address ?? '',
          addressId: address.addressId ?? '',
          adgangsaddressId: address.adgangsAddressId ?? '',
          isAffiliate: isAffiliate);

      if (!addressResponse.success!) throw addressResponse.message!;

      final newAddress = Address.fromJson(
          addressResponse.data['address'] as Map<String, dynamic>);

      tempCreateOfferWizardForms.update('address', (value) => newAddress);

      _createOfferStep = 1;
      _createOfferWizardForms = {...tempCreateOfferWizardForms};

      notifyListeners();
    } catch (e) {
      if (e is ApiException) {
        log("message ${e.message}");
      }
    }
  }

  Future<void> updateIndustryDescriptions(
      {required bool isDelete,
      required bool isAdd,
      required bool isDeleteIndustry,
      required int industryIndex,
      required int subIndustryIndex,
      required int descriptionIndex,
      required String description}) async {
    final tempIndustryDescriptions = [..._industryDescriptions];

    if (isDeleteIndustry) {
      (tempIndustryDescriptions[industryIndex]['subIndustry'] as List)
          .removeAt(subIndustryIndex);
    } else {
      final subIndustryDescriptions = tempIndustryDescriptions[industryIndex]
          ['subIndustry'][subIndustryIndex]['descriptions'] as List;
      if (isDelete) {
        subIndustryDescriptions.removeAt(descriptionIndex);
      } else if (isAdd) {
        subIndustryDescriptions.add(description);
      } else {
        tempIndustryDescriptions[industryIndex]['subIndustry'][subIndustryIndex]
            ['descriptions'][descriptionIndex] = description;
      }
    }

    _industryDescriptions = [...tempIndustryDescriptions];

    notifyListeners();
  }

  Future<void> updateIndustryPrice({
    required int industryIndex,
    required int subIndustryIndex,
    required String price,
  }) async {
    final tempIndustryDescriptions = [..._industryDescriptions];

    tempIndustryDescriptions[industryIndex]['subIndustry'][subIndustryIndex]
        ['price'] = double.parse(price);

    _industryDescriptions = [...tempIndustryDescriptions];

    notifyListeners();
  }

  Future<void> createProject(
      {bool isUpload = false, required bool isAffiliate}) async {
    try {
      final Map<String, dynamic> descriptionForm = {};
      final List<String> addEnterpriseSubProjectNew = [];
      final List<String> jobList = [];

      final List<int> taskTypeIds = [];

      final selectedAddress = _createOfferWizardForms['address'] as Address;

      final service = context.read<BookingWizardService>();

      double totalPriceWithoutVat = _industryDescriptions
          .expand((industry) => industry['subIndustry'] as List)
          .fold(0, (sum, subIndustry) => sum + subIndustry['price']);

      await service.cleanListPriceAi(
          contactId: _createOfferWizardForms['contact']['ID']);

      final subIndustryAiResponse = await service.getListSubindustryAi(
        contactId: _createOfferWizardForms['contact']['ID'],
      );

      for (final element in _industryDescriptions
          .expand((element) => element['subIndustry'] as List)) {
        final task = taskTypes.firstWhere(
          (task) =>
              task.industryId == int.parse(element['subIndustryId'] as String),
          orElse: () => ProjectTaskTypes.defaults(),
        );

        if ((task.subcategories ?? []).isNotEmpty) {
          for (final subTask in (task.subcategories?.first.taskTypes ?? [])) {
            taskTypeIds.add(subTask.taskTypeId);
          }
        }
      }

      for (final industry in industryDescriptions) {
        addEnterpriseSubProjectNew.add(
            '${industry['productTypeId']}: ${(industry['subIndustry'] as List).map((e) => e['subIndustryId']).join('-')}');

        for (final subIndustry in industry['subIndustry'] as List) {
          final Map<String, dynamic> subIndustryMap =
              subIndustry as Map<String, dynamic>;
          descriptionForm.putIfAbsent(
              'text_${industry['productTypeId']}_${subIndustryMap['subIndustryId']}',
              () => subIndustryMap['descriptions']);

          String job = '';
          final subIndustryId = subIndustry['subIndustryId'];
          if (subIndustryAiResponse.data.runtimeType == List<dynamic>) {
            final subIndustryResponseData =
                subIndustryAiResponse.data as List<dynamic>;
            for (final element in subIndustryResponseData) {
              job =
                  '$subIndustryId:${(element as List).join('-')}:${industryDescriptions.map((item) => item['subIndustry'] as List).expand((subIndustries) => subIndustries).toList().where((a) => a['subIndustryId'] as String == subIndustry['subIndustryId'] as String).toList().map((b) => b['productTypeId']).join('-')}';
            }
          } else {
            final subIndustryAiResponseData =
                subIndustryAiResponse.data as Map<String, dynamic>;
            final subIndustryIdList =
                subIndustryAiResponseData[subIndustryId] as List;
            job =
                '$subIndustryId:${(subIndustryIdList).join('-')}:${industryDescriptions.map((item) => item['subIndustry'] as List).expand((subIndustries) => subIndustries).toList().where((a) => a['subIndustryId'] as String == subIndustry['subIndustryId'] as String).toList().map((b) => b['productTypeId']).join('-')}';
          }

          if (!jobList.contains(job)) jobList.add(job);
        }
      }

      final Map<String, dynamic> createProjectForm = {
        "contactId": _createOfferWizardForms['contact']['ID'],
        "addressId": selectedAddress.id,
        "name": isUpload
            ? _createOfferWizardForms['projectName']
            : _createOfferWizardForms['name'],
        "description": isUpload
            ? _createOfferWizardForms['projectName']
            : _createOfferWizardForms['description'],
        "source": "app-partner",
        "status": "new_project",
        "contractType": "Hovedentreprise",
        "industryNew": [
          ...industryDescriptions
              .expand((element) => element['subIndustry'] as List)
              .map((e) => int.parse(e['subIndustryId'] as String))
        ],
        "subcategoryNew": [
          ...industryDescriptions
              .expand((element) => element['subIndustry'] as List)
              .map((e) => int.parse(e['subIndustryId'] as String))
        ],
        "taskTypeNew": taskTypeIds,
        "enterpriseIndustry": [
          ...selectedIndustries.map((e) => e.producttypeid)
        ],
        "enterpriseIndustryNew": addEnterpriseSubProjectNew,
        "jobsList": jobList.map((e) => [e]).toList(),
        "budget": totalPriceWithoutVat,
        "isAffiliate": isAffiliate ? 1 : 0
      };

      createProjectForm.addAll(_wizardProductAnswers);
      createProjectForm.addAll(descriptionForm);

      if (applic.env != 'production') {
        createProjectForm.putIfAbsent('testProject', () => true);
      }

      final projectResponse =
          await service.createProject(form: createProjectForm);

      if (!projectResponse.success!) throw projectResponse.message!;

      _projectWizard = PartnerProjectsModel.fromJson(
          projectResponse.data['project'] as Map<String, dynamic>);

      // _createOfferStep = _createOfferStep + 1;

      notifyListeners();
    } catch (e) {
      log("message createContactAndAddress $e");
      if (e is ApiException) {
        log("message createContactAndAddress ${e.message}");
      }
    }
  }

  Future<void> createOfferWizardContinuation(
      {required int step,
      required int index,
      int? contactId,
      int? addressId,
      int? projectId,
      int? offerId,
      Map<String, dynamic>? contactInfo}) async {
    final service = context.read<BookingWizardService>();
    final tempCreateOfferWizardForms = {..._createOfferWizardForms};

    switch (step) {
      case 1:
      case 2:
        //CONTACT INFO
        tempCreateOfferWizardForms.putIfAbsent('contact', () => contactInfo);

        //ADDRESS INFO
        final addressResponse =
            await service.getAddressId(addressId: addressId!);

        if (!addressResponse.success!) throw addressResponse.message!;

        final newAddress =
            Address.fromJson(addressResponse.data as Map<String, dynamic>);

        tempCreateOfferWizardForms.putIfAbsent('address', () => newAddress);

        _createOfferWizardForms = {...tempCreateOfferWizardForms};
        _createOfferStep = 1;
        _stepTwoSubStep = 0;
        break;
      case 3:
      case 4:
      case 5:
      case 6:
        //CONTACT INFO
        tempCreateOfferWizardForms.putIfAbsent('contact', () => contactInfo);

        //ADDRESS INFO
        final addressResponse =
            await service.getAddressId(addressId: addressId!);

        if (!addressResponse.success!) throw addressResponse.message!;

        final newAddress =
            Address.fromJson(addressResponse.data as Map<String, dynamic>);

        tempCreateOfferWizardForms.putIfAbsent('address', () => newAddress);

        //PROJECT
        final projectResponse =
            await service.getProjectById(projectId: projectId!);

        if (!projectResponse.success!) throw projectResponse.message!;

        _projectWizard = PartnerProjectsModel.fromJson(projectResponse.data);
        _createOfferWizardForms = {...tempCreateOfferWizardForms};
        _createOfferStep = 3;
    }

    _existingCreateOfferWizard = _existingCreateOfferWizardList[index];

    notifyListeners();
  }

  Future<void> getListPrices() async {
    try {
      final contractService = context.read<ContractService>();
      final List<Map<String, dynamic>> industryDescriptions = [];
      final tempCreateOfferWizardForms = {..._createOfferWizardForms};

      for (final element in _selectedIndustries) {
        industryDescriptions.add({
          "productName": element.producttypeDa,
          "productTypeId": element.producttypeid,
          "subIndustry": [
            {
              "productTypeId": element.producttypeid,
              "subIndustryName": _industryFullList
                  .firstWhere((l) => l.branchId.toString() == element.industry,
                      orElse: () => Data(branche: 'N/A'))
                  .branche,
              "subIndustryId": element.industry,
              "descriptions": [],
              "price": 0.0,
              "priceVat": 0.0
            }
          ]
        });
      }

      final totalSubIndustry = industryDescriptions
          .expand((element) => element['subIndustry'] as List)
          .toList()
          .length;
      bool priceHasVat = _createOfferWizardForms['priceHasVat'];
      double projectPriceWithVat = 0;
      double projectPriceWithoutVat = 0;
      double projectPrice =
          double.parse(_createOfferWizardForms['projectPrice']);

      if (priceHasVat) {
        projectPriceWithVat = projectPrice;
        projectPriceWithoutVat = projectPrice / 1.25;
      } else {
        projectPriceWithVat = projectPrice * 1.25;
        projectPriceWithoutVat = projectPrice;
      }

      final totalFessResponse = await contractService.getTotalFees(
          totalWithVat: projectPriceWithVat.ceil());
      for (final industry in industryDescriptions) {
        final subInustryList = industry['subIndustry'] as List;
        for (final subIndustry in subInustryList) {
          subIndustry['price'] =
              (projectPriceWithoutVat / totalSubIndustry).toDouble();
          subIndustry['priceVat'] =
              (projectPriceWithVat / totalSubIndustry).toDouble();
        }
      }

      tempCreateOfferWizardForms['projectPriceFees'] = ((totalFessResponse.data
              as Map<String, dynamic>)['FEEINAMOUNT'] as int)
          .toDouble();

      _createOfferStep = _createOfferStep + 1;
      _industryDescriptions = [...industryDescriptions];
      _createOfferWizardForms = {...tempCreateOfferWizardForms};

      notifyListeners();
    } catch (e) {
      log('getListPrice Error Message: $e');
    }
  }

  Future<void> deleteIndustry(
      {required int industryIndex, required int subIndustryIndex}) async {
    final tempIndustryDescriptions = [..._industryDescriptions];
    (tempIndustryDescriptions[industryIndex]['subIndustry'] as List)
        .removeAt(subIndustryIndex);
    _industryDescriptions = [...tempIndustryDescriptions];

    notifyListeners();
  }

  Future<void> updatePrice(
      {required int industryIndex,
      required int subIndustryIndex,
      required double price}) async {
    final tempIndustryDescriptions = [..._industryDescriptions];
    tempIndustryDescriptions[industryIndex]['subIndustry'][subIndustryIndex]
        ['price'] = price / 1.25;
    tempIndustryDescriptions[industryIndex]['subIndustry'][subIndustryIndex]
        ['priceVat'] = price;
    _industryDescriptions = [...tempIndustryDescriptions];

    notifyListeners();
  }

  Future<void> getOfferHistory({required int page, required int year}) async {
    setBusy(true);
    var clientService = context.read<ClientService>();

    final offerResponse =
        await clientService.getOfferHistory(page: page, year: year);

    if (offerResponse != null) {
      _offerListPagination = OfferPaginationModel.fromJson(
          offerResponse.data as Map<String, dynamic>);
    }

    setBusy(false);

    notifyListeners();
  }

  Future<bool> addSignatureToOffer({required String signature}) async {
    var offerService = context.read<OfferService>();

    MinboligApiResponse? response;

    if (viewSignatureOfferVersionId > 0) {
      response = await offerService.addSignatureToOfferVersion(
          offerVersionId: viewSignatureOfferVersionId,
          offerId: viewedSignatureOffer!.id!,
          signature: signature);
    } else {
      response = await offerService.addSignatureToOffer(
          offerId: viewedSignatureOffer!.id!, signature: signature);
    }

    return response?.success ?? false;
  }

  Future<bool> downloadFile({required String name, required String url}) async {
    try {
      Directory? directory;
      if (Platform.isIOS) {
        directory = await getApplicationDocumentsDirectory();
      } else {
        directory = await getDownloadsDirectory();
      }
      final randid = math.Random().nextInt(10000);
      var filePath = "${directory!.path}/PartnerLogin/${name}_$randid.pdf";

      var file = File(filePath);
      while (file.existsSync()) {
        file = File(filePath);
      }

      final response = await Dio().download(url, filePath);

      return response.statusCode == 200;
    } catch (e) {
      log("message download $e");

      return false;
    }
  }

  Future<bool> partnerOfferVersion(
      {required Map<String, dynamic> offerVersionPayload}) async {
    final createOfferService = context.read<OfferService>();

    final response = await createOfferService.partnerOfferVersion(
        offerVersionPayload: offerVersionPayload);

    if (response != null) {
      return response.success ?? false;
    } else {
      return false;
    }
  }

  Future<bool> checkIndustryPrices() async {
    final tempIndustry = List.from(_industryDescriptions);

    final zeroPrices = <Map<String, dynamic>>[];

    for (var industryIndex = 0;
        industryIndex < tempIndustry.length;
        industryIndex++) {
      final subIndustry = tempIndustry[industryIndex]['subIndustry'] as List;

      for (var subIndex = 0; subIndex < subIndustry.length; subIndex++) {
        final subElement = subIndustry[subIndex];
        if (subElement['price'] == 0) {
          subElement['errorMessage'] = 'price_cannot_be_lower_than_0';
          subIndustry[subIndex] = subElement;
          zeroPrices.add(subElement);
        } else if (subElement['errorMessage'] != null) {
          subElement.remove('errorMessage');
          subIndustry[subIndex] = subElement;
        }
      }

      tempIndustry[industryIndex]['subIndustry'] = subIndustry;
    }

    _industryDescriptions = List.from(tempIndustry);
    notifyListeners();

    return zeroPrices.isEmpty;
  }

  Future<bool> createOffer(
      {required String signature,
      required UserViewModel userVm,
      required bool isAffiliate}) async {
    try {
      final service = context.read<BookingWizardService>();

      int offerId = _existingCreateOfferWizard?.offerId ?? 0;

      double totalPriceWithoutVat = _industryDescriptions.isNotEmpty
          ? _industryDescriptions
              .expand((industry) => industry['subIndustry'] as List)
              .fold(0, (sum, subIndustry) => sum + subIndustry['price'])
          : _projectWizard.budget?.first.amount ?? 0;

      ////UPDATE CONTACT
      if (_createOfferWizardForms['contact']['NAME'] == 'John Doe') {
        await service.updateContact(
            contactId: _createOfferWizardForms['contact']['ID'],
            name: _createOfferWizardForms['homeOwnerName'],
            mobile: int.parse(_createOfferWizardForms['homeOwnerPhone']));
      }

      //// CREATE PROJECT
      await createProject(isAffiliate: isAffiliate);

      ////RESERVE PROJECT
      // if (_existingCreateOfferWizard != null &&
      //     _existingCreateOfferWizard?.reservationId == null) {
      final reserveResponse = await service.reserveProject(
          contactId: _createOfferWizardForms['contact']['ID'],
          projectId: projectWizard.id!,
          isAffiliate: isAffiliate);

      if (!reserveResponse.success!) throw reserveResponse.message!;
      // }

      //// CREATE OFFER
      // if (_existingCreateOfferWizard != null &&
      //     _existingCreateOfferWizard?.offerId == null) {
      Map<String, dynamic> offerWizardPayload = {};
      List<Map<String, dynamic>> saveListPricePayload = [];

      for (final industry in industryDescriptions) {
        for (final subIndustry in industry['subIndustry']) {
          final submittedJobDescription = {
            'submitted': subIndustry['descriptions']
          };
          saveListPricePayload.add({
            "project_id": projectWizard.id,
            "sub_project_id": 0,
            "contact_id": createOfferWizardForms['contact']['ID'],
            "calculation_id": 0,
            "industry_id": subIndustry['productTypeId'],
            "job_description": submittedJobDescription,
            "total": subIndustry['price'],
            "note": createOfferWizardForms['offerCondition'] == 'null'
                ? null
                : createOfferWizardForms['offerCondition'],
            "job_industry_id": subIndustry['subIndustryId'],
            "signature": signature,
            "weeks": createOfferWizardForms['projectEstimate'],
            "days": createOfferWizardForms['projectEstimate'],
            'expiry': createOfferWizardForms['uploadOfferSignatureValid'],
            "start_date": createOfferWizardForms['projectStartDate'],
            "startDate": createOfferWizardForms['projectStartDate'],
            "offer_id": offerId,
            "parent_offer_id": offerId,
            "reviews_id": [],
            "partner_id": userVm.userModel?.user?.id ?? 0,
          });

          // saveListPricePayload.add({
          //   "project_id": projectWizard.id,
          //   "contact_id": createOfferWizardForms['contact']['ID'],
          //   "calculation_id": 0,
          //   "offer_id": offerId,
          //   "industry_id": subIndustry['productTypeId'],
          //   "job_industry_id": subIndustry['subIndustryId'],
          //   "total": subIndustry['price'],
          //   "note": createOfferWizardForms['offerCondition'] == 'null'
          //       ? null
          //       : createOfferWizardForms['offerCondition'],
          //   "weeks": createOfferWizardForms['projectEstimate'],
          //   "start_date": createOfferWizardForms['projectStartDate'],
          //   "signature": signature,
          //   "days": createOfferWizardForms['projectEstimate'],
          //   "parent_offer_id": offerId,
          //   "sub_project_id": 0,
          //   "job_description": subIndustry['descriptions'],
          // });
        }
      }

      offerWizardPayload['contactId'] = createOfferWizardForms['contact']['ID'];
      offerWizardPayload['projectId'] = projectWizard.id!;
      offerWizardPayload['type'] = 'normal';
      offerWizardPayload['saveListPriceV2'] = saveListPricePayload;
      offerWizardPayload['createdFrom'] = 'partner-app';
      offerWizardPayload['expiry'] =
          createOfferWizardForms['uploadOfferSignatureValid'];
      offerWizardPayload['isAffiliate'] = isAffiliate ? 1 : 0;
      offerWizardPayload['offers'] = [
        {
          'title':
              'Tilbud på projektnr. ${projectWizard.id} til ${projectWizard.name}',
          'description': projectWizard.description,
          'subtotalPrice': totalPriceWithoutVat,
          'vat': (totalPriceWithoutVat * 0.25),
          'totalPrice': (totalPriceWithoutVat * 1.25),
          'tasks': [
            {
              'title': '',
              'description': '',
              'unit': '',
              'value': '',
              'quantity': ''
            }
          ],
          'type': 'standard',
          'expiry': createOfferWizardForms['uploadOfferSignatureValid'],
          'start_date': createOfferWizardForms['projectStartDate'],
          'startDate': createOfferWizardForms['projectStartDate'],
          'signature': signature,
          'createdFrom': 'partner-app',
        }
      ];

      // final offerPayload = {
      //   "contactId": _createOfferWizardForms['contact']['ID'],
      //   "projectId": projectWizard.id!,
      //   "type": 'bookerwizard',
      //   "createdFrom": "partner-app",
      //   "saveListPriceV2": saveListPricePayload,
      //   "offers": [
      //     {
      //       "title":
      //           "Tilbud på projektnr. ${projectWizard.id} til ${projectWizard.name}",
      //       "description": projectWizard.description,
      //       "subtotalPrice": totalPriceWithoutVat,
      //       "vat": (totalPriceWithoutVat * 0.25),
      //       "totalPrice": (totalPriceWithoutVat * 1.25),
      //       "tasks": [
      //         {
      //           "title": '',
      //           "description": '',
      //           "unit": '',
      //           "value": '',
      //           "quantity": ''
      //         }
      //       ],
      //       "type": "standard",
      //       "expiry": createOfferWizardForms['projectEstimate'],
      //     }
      //   ],
      // };

      if (industryDescriptions
          .expand((a) => a['subIndustry'] as List)
          .where((e) => e['subIndustryId'] == '1062')
          .isNotEmpty) {
        offerWizardPayload.putIfAbsent('jobIndustry', () => '1062');
      }

      final createOfferResponse =
          await service.createOffer(params: offerWizardPayload);

      if (!createOfferResponse.success!) throw createOfferResponse.message!;

      offerId = (createOfferResponse.data as Map<String, dynamic>)['offer']
          .first['ID'];
      // }

      ////SAVE LIST PRICE

      ////SEND INVITE
      final sendInviteResponse = await service.sendInvite(
          contactId: _createOfferWizardForms['contact']['ID'],
          projectId: projectWizard.id!,
          offerId: offerId,
          isAffiliate: isAffiliate);

      if (!sendInviteResponse.success!) throw sendInviteResponse.message!;

      return true;
    } catch (e) {
      log("message createOffer $e");

      return false;
    }
  }

  Future<bool> createUploadFastOffer(
      {required String signature,
      required UserViewModel userVm,
      required bool isAffiliate}) async {
    try {
      final bookingWizardService = context.read<BookingWizardService>();
      int offerId = _existingCreateOfferWizard?.offerId ?? 0;
      double totalPriceWithoutVat = _industryDescriptions.isNotEmpty
          ? _industryDescriptions
              .expand((industry) => industry['subIndustry'] as List)
              .fold(0, (sum, subIndustry) => sum + subIndustry['price'])
          : _projectWizard.budget?.first.amount ?? 0;

      //// UPDATE CONTACT
      if (_createOfferWizardForms['contact']['NAME'] == 'John Doe') {
        await bookingWizardService.updateContact(
            contactId: _createOfferWizardForms['contact']['ID'],
            name: _createOfferWizardForms['homeOwnerName'],
            mobile: int.parse(_createOfferWizardForms['homeOwnerPhone']));
      }

      //// CREATE PROJECT
      await createProject(isUpload: true, isAffiliate: isAffiliate);

      //// RESERVE PROJECT
      final reserveResponse = await bookingWizardService.reserveProject(
          contactId: _createOfferWizardForms['contact']['ID'],
          projectId: projectWizard.id!,
          isAffiliate: isAffiliate);

      if (!reserveResponse.success!) throw reserveResponse.message!;

      //// CREATE OFFER
      Map<String, dynamic> offerWizardPayload = {};
      List<Map<String, dynamic>> saveListPricePayload = [];

      for (final industry in industryDescriptions) {
        for (final subIndustry in industry['subIndustry']) {
          final submittedJobDescription = {
            'submitted': subIndustry['descriptions']
          };
          saveListPricePayload.add({
            "project_id": projectWizard.id,
            "sub_project_id": 0,
            "contact_id": createOfferWizardForms['contact']['ID'],
            "calculation_id": 0,
            "industry_id": subIndustry['productTypeId'],
            "job_description": submittedJobDescription,
            "total": subIndustry['price'],
            "note": createOfferWizardForms['offerCondition'] == 'null'
                ? null
                : createOfferWizardForms['offerCondition'],
            "job_industry_id": subIndustry['subIndustryId'],
            "signature": signature,
            "weeks": createOfferWizardForms['uploadOfferProjectEstimate'],
            "days": createOfferWizardForms['uploadOfferProjectEstimate'],
            "start_date": createOfferWizardForms['uploadOfferProjectStartDate'],
            "startDate": createOfferWizardForms['uploadOfferProjectStartDate'],
            "offer_id": offerId,
            'OFFERID': offerId,
            "parent_offer_id": offerId,
            "reviews_id": [],
            "partner_id": userVm.userModel?.user?.id ?? 0,
          });
        }
      }

      offerWizardPayload['contactId'] = createOfferWizardForms['contact']['ID'];
      offerWizardPayload['projectId'] = projectWizard.id!;
      offerWizardPayload['type'] = 'fast-offer';
      offerWizardPayload['saveListPriceV2'] = saveListPricePayload;
      offerWizardPayload['createdFrom'] = 'partner-app';
      offerWizardPayload['expiry'] =
          createOfferWizardForms['uploadOfferSignatureValid'];
      offerWizardPayload['isAffiliate'] = isAffiliate ? 1 : 0;
      offerWizardPayload['offers'] = [
        {
          'title':
              'Tilbud på projektnr. ${projectWizard.id} til ${projectWizard.name}',
          'description': projectWizard.description,
          'subtotalPrice': totalPriceWithoutVat,
          'start_date': createOfferWizardForms['uploadOfferProjectStartDate'],
          'startDate': createOfferWizardForms['uploadOfferProjectStartDate'],
          'vat': (totalPriceWithoutVat * 0.25),
          'totalPrice': (totalPriceWithoutVat * 1.25),
          'fileName': _createOfferWizardForms['fileName'],
          'file': _createOfferWizardForms['file'],
          'tasks': [
            {
              'title': '',
              'description': '',
              'unit': '',
              'value': '',
              'quantity': ''
            }
          ],
          'type': 'standard',
          'expiry': createOfferWizardForms['uploadOfferSignatureValid'],
          'signature': signature,
          'createdFrom': 'partner-app',
        }
      ];

      if (industryDescriptions
          .expand((a) => a['subIndustry'] as List)
          .where((e) => e['subIndustryId'] == '1062')
          .isNotEmpty) {
        offerWizardPayload.putIfAbsent('jobIndustry', () => '1062');
      }

      final createOfferResponse =
          await bookingWizardService.createOffer(params: offerWizardPayload);
      if (!createOfferResponse.success!) throw createOfferResponse.message!;
      offerId = (createOfferResponse.data as Map<String, dynamic>)['offer']
          .first['ID'];

      //// SEND INVITE
      final sendInviteResponse = await bookingWizardService.sendInvite(
          contactId: _createOfferWizardForms['contact']['ID'],
          projectId: projectWizard.id!,
          offerId: offerId,
          isAffiliate: isAffiliate);

      if (!sendInviteResponse.success!) throw sendInviteResponse.message!;

      return true;
    } catch (e) {
      log("message createUploadFastOffer: $e");
      return false;
    }
  }

  Future<MinboligApiResponse> addOffer() async {
    var clientService = context.read<ClientService>();
    setBusy(true);
    var response = await clientService.addOffer(
        projectId: projectId,
        title: headerName,
        description: quotationForm!.rows!.last!.customName,
        subtotalPrice: quotationForm!.subtotal,
        vat: 0.25 * quotationForm!.subtotal,
        totalPrice: ((0.25 * quotationForm!.subtotal) + quotationForm!.subtotal)
            .toStringAsFixed(2),
        tasks: quotationForm!.rows!);
    setBusy(false);
    return response;
  }

  Future<Map<String, List<ListPriceMinbolig>>?> getListPriceMinbolig({
    required int contactId,
    required int projectId,
  }) async {
    try {
      var offerService = context.read<OfferService>();

      final listPriceResponse = await offerService.getListPriceMinbolig(
          contactId: contactId, projectId: projectId);

      if (listPriceResponse != null) {
        final listPricesMinbolig = ListPriceMinbolig.fromCollection(
            (listPriceResponse.data as Map<String, dynamic>)['result']);

        return listPricesMinbolig.groupBy((p0) => p0.industry!);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  List<ProductWizardField> extractProducts(
      {required List<ProductWizardField> product,
      required List<ProductWizardField> productValues}) {
    final List<ProductWizardField> fields = [];

    for (final element in product) {
      if (element.productField == '2') {
        fields.add(element);
      } else {
        List<ProductWizardField> values = [];
        final List<ProductWizardField> children = [];

        final ProductWizardField tempElement = element;

        values = [
          ...productValues
              .where((element) => element.parentproductid == tempElement.id)
        ];

        if (values.isNotEmpty) {
          for (final c in values) {
            if (productValues
                .where((element) => element.parentproductid == c.id)
                .isNotEmpty) {
              final childProduct = productValues
                  .where((element) => element.parentproductid == c.id)
                  .first;

              if (childProduct.productField == '2') {
                children.add(childProduct);
              } else {
                childProduct.values = [
                  ...productValues.where(
                      (element) => element.parentproductid == childProduct.id)
                ];

                children.add(childProduct);
              }
            }
            addChildren(productValues, c, children);
          }
        }

        tempElement.values = values;
        tempElement.children = children;

        fields.add(tempElement);
      }
    }

    return [...fields];
  }

  Rows newRow() {
    return Rows(
      uuid: const Uuid().v4(),
      name: '',
      selectedItem: null,
      showCustomItem: false,
      customName: '',
      quantity: 1,
      unitPrice: 0,
      discount: 0,
      tax: 0,
      type: 1,
      amount: 0,
      description: '',
    );
  }

  Info initQuotationForm(String? id) {
    return Info(
      customerId: null,
      currencyId: null,
      date: Formatter.formatDateTime(
          type: DateFormatType.isoDate, date: DateTime.now()),
      expiryDate: Formatter.formatDateTime(
          type: DateFormatType.isoDate,
          date: DateTime.now().add(const Duration(days: 30))),
      expiryDateDetail: '',
      uploadToken: '',
      itemType: '',
      rows: <Rows>[],
      subject: '',
      description: '',
      subtotal: 0,
      subtotalDiscount: 0,
      lineItemDiscountType: true,
      subtotalDiscountType: false,
      subtotalTax: 25,
      subtotalShippingAndHandling: 0,
      total: 0,
      referenceNumber: '',
      quotationNumber: '',
      quotationPrefix: '',
      isDraft: 0,
      showSubTotalTax: true,
      showSubTotalDiscount: true,
      showSubTotalShippingAndHandling: true,
      showLineItemTax: true,
      showLineItemDiscount: true,
      showLineItemDescription: true,
      customerNote: '',
      memo: '',
      tnc: '',
    );
  }

  addTaskFile(String? key, String? fileUrl) {
    _taskFiles.putIfAbsent(key, () => <String>[]);
    _taskFiles[key]!.add('${Links.viewImageUrl}$fileUrl');
    notifyListeners();
  }

  silentInitQuotationForm({String? id}) {
    _quotationForm = initQuotationForm(id);
  }

  void updateCreateOfferWizardForms(
      {required String key, required dynamic value}) {
    final tempAnswers = {..._createOfferWizardForms};

    if (tempAnswers.containsKey(key)) {
      tempAnswers.update(key, (old) => value);
    } else {
      tempAnswers.putIfAbsent(key, () => value);
    }

    _createOfferWizardForms = {...tempAnswers};

    notifyListeners();
  }

  void updateCreateOfferSteps(
      {int? createOfferStep,
      int? createOfferProductsStep,
      int? stepTwoSubStep}) {
    if (createOfferStep != null) {
      _createOfferStep = createOfferStep;
    } else if (createOfferProductsStep != null) {
      _createOfferProductsStep = createOfferProductsStep;
    } else if (stepTwoSubStep != null) {
      _stepTwoSubStep = stepTwoSubStep;
    }

    notifyListeners();
  }

  void updateSelectedIndustryList({required AllWizardText industry}) {
    final tempList = [..._selectedIndustries];

    if (tempList.contains(industry)) {
      log('Removing industry');
      tempList.remove(industry);
    } else {
      log('Adding industry');
      tempList.add(industry);
    }

    _selectedIndustries = [...tempList];

    notifyListeners();
  }

  void searchIndustry({required String query}) {
    final tempList = [..._filteredIndustries];

    if (query.isNotEmpty) {
      _filteredIndustries = tempList.where((item) {
        final itemName = item.producttypeDa!.toLowerCase();
        return itemName.contains(query.toLowerCase());
      }).toList();

      notifyListeners();
    } else {
      _filteredIndustries = [..._industryList];

      notifyListeners();
    }
  }

  void updateSelectedIndustryLength({required int length}) {
    _selectedIndustryLength = length;
  }

  void addChildren(List<ProductWizardField> productValues, ProductWizardField c,
      List<ProductWizardField> children) {
    if (c.productField == '3' || c.productField == '4') {
      final val = productValues
          .where((element) => element.parentproductid == c.id)
          .toList();

      final ProductWizardField tempC = c;

      tempC.values = val;

      children.add(tempC);

      for (final d in val) {
        addChildren(productValues, d, children);
      }
    } else {
      children.add(c);
    }
  }

  void updateWizardProductAnswers(
      {required String key,
      required String value,
      List<String>? options,
      List<String>? selectedChecks}) {
    final tempProductControllers = {..._wizardProductAnswers};

    if (options != null && selectedChecks != null) {
      final selectedSet = Set<String>.from(selectedChecks);

      tempProductControllers.removeWhere((key, value) =>
          options.contains(value) && !selectedSet.contains(value));

      for (var element in selectedChecks) {
        if (options.contains(element)) {
          tempProductControllers['$key$element'] = element;
        }
      }
    } else {
      tempProductControllers[key] = value;
    }

    _wizardProductAnswers = {...tempProductControllers};

    notifyListeners();
  }

  void resetCreateOfferWizards() {
    _createOfferStep = 0;
    _createOfferProductsStep = 0;
    _stepTwoSubStep = 0;
    _selectedIndustryLength = 0;
    _createOfferWizardForms = {"uploadOfferSignatureValid": "30"};
    _wizardProductAnswers = {};
    _showDescription = {};
    _isValidateError = {};
    _selectedIndustries = [];
    _wizardQuestions = [];
    _industryDescriptions = [];
    _uploadOfferFiles = [];
    _selectedOfferFiles = [];
    _projectWizard = PartnerProjectsModel();
    _offerType = '';
    _tempTotalPrice = 0;

    notifyListeners();
  }

  void addBilagFile(String name) {
    _bilagfiles.add(name);
    //this.notifyListeners();
  }

  void resetFields() {
    _headerImage = null;
    _headerName = '';
    _headerImageUrl = '';
    _quotationForm = initQuotationForm('');
    _taskFiles.clear();
    _reviewIds.clear();
    _customerAddress = '';
    _conditions.clear();
    //_appendices?.clear();
    _userPartner = null;
    _offerFile = null;
    _offerFileServerName = "";
    _bilagfiles.clear();

    notifyListeners();
  }

  void loadFields({
    required File? nheaderImage,
    required String? nheaderName,
    required String? nheaderImageUrl,
    required Info? nquotationForm,
    required List<int?> nreviewIds,
    required TermsResponseModel nconditions,
    required TermsResponseModel nappendices,
    required String ncustomerAddress,
    required TermsResponseModel nnotes,
    List<String>? nbilagfiles,
  }) {
    _headerImage = nheaderImage;
    _headerName = nheaderName ?? '';
    _headerImageUrl = nheaderImageUrl ?? '';
    _quotationForm = nquotationForm ?? initQuotationForm('');
    _taskFiles.clear();
    _reviewIds.clear();
    _reviewIds = nreviewIds;
    _customerAddress = ncustomerAddress;

    _condition = nconditions;
    _appendix = nappendices;
    _notes = nnotes;

    if (nbilagfiles != null) {
      _bilagfiles = nbilagfiles;
    }

    notifyListeners();
  }

  void update() {
    notifyListeners();
  }

  void reset() {
    _editOffer = TilbudModel();
    _currentState = OfferProcessState.add;
    _noteList = [];
    _conditions = [];
    _bilagfiles = [];
    _notes = null;
    _appendix = null;
    _condition = null;
    _partnerProfile = null;
    _offerVersionList = null;
    _partnerHaa = null;
    _tilbudTemplateHtml = null;
    _appid = '0';
    _type = ClientType.contact;
    _taskFiles = {};
    _showDescription = {};
    _isValidateError = {};
    _termList = [];
    _quotationForm = Info();
    _defaultTaC = 'Standard terms and conditions';
    _defaultCondition = 'Standard terms and conditions';
    _defaultNote = 'Standard terms and conditions';
    _defaultAppendix = 'Standard terms and conditions';
    _tilbudId = '0';
    _headerName = '';
    _offerType = '';
    _headerImage = null;
    _headerImageUrl = null;
    _customerComments = null;
    _chosenTerm = TermsResponseModel();
    _userPartner = null;
    _customerAddress = '';
    _typeApi = '';
    _projectId = 0;
    _reviewIds = [];
    _customerToggleIndex = 0;
    _tempTotalPrice = 0;
    _selectedModel = null;
    _offerFile = null;
    _offerFileServerName = "";
    _offerListPagination = OfferPaginationModel();
    _createOfferStep = 0;
    _createOfferProductsStep = 0;
    _stepTwoSubStep = 0;
    _createOfferWizardForms = {"uploadOfferSignatureValid": "30"};
    _wizardProductAnswers = {};
    _industryFullList = [];
    _industryList = [];
    _filteredIndustries = [];
    _selectedIndustries = [];
    _wizardQuestions = [];
    _industryDescriptions = [];
    _existingCreateOfferWizardList = [];
    _existingCreateOfferWizard = null;
    _projectWizard = PartnerProjectsModel();
    _taskTypes = [];
    _uploadOfferStep = 0;
    _uploadOfferFiles = [];
    _selectedOfferFiles = [];
    _uploadOfferForms = {"uploadOfferSignatureValid": "30"};
    _isOfferUploaded = false;
    _viewedOffer = null;
    _seeMore = true;
    _isNewOfferVersion = false;
    _isUploadOfferUpdate = false;
    _isCreateOfferLoading = false;
    _isUploadOfferLoading = false;
    _csUserId = '';
    _offerVersionId = 0;
    _selectedIndustryLength = 0;
    _viewedSignatureOffer = null;
    _viewedSignatureOfferListPrice = [];
    _viewSignatureOfferVersionId = 0;

    notifyListeners();
  }

  void setIndustry(
      {required List<AllWizardText> list1,
      required List<Data> list2,
      required List<ProjectTaskTypes> list3}) {
    _filteredIndustries = [...list1];
    _industryList = [...list1];
    _industryFullList = [...list2];
    _taskTypes = [...list3];

    notifyListeners();
  }
}
