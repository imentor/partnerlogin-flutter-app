import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:Haandvaerker.dk/model/contract/contract_model.dart';
import 'package:Haandvaerker.dk/model/contract/contract_template_model.dart';
import 'package:Haandvaerker.dk/model/deficiency/deficiency_model.dart';
import 'package:Haandvaerker.dk/model/get_types_response_model.dart';
import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/model/task_type/task_type_model.dart';
import 'package:Haandvaerker.dk/model/wizard/list_price_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/booking_wizard/booking_wizard_service.dart';
import 'package:Haandvaerker.dk/services/contract/contract_service.dart';
import 'package:Haandvaerker.dk/services/offer/offer_service.dart';
import 'package:Haandvaerker.dk/services/projects/projects_service.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/projects/my_projects_viewmodel.dart';
import 'package:file_picker/file_picker.dart';
import 'package:file_selector/file_selector.dart';
import 'package:flutter/material.dart';
import 'package:html_to_pdf/html_to_pdf.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

class FastTrackOfferViewModel extends BaseViewModel {
  FastTrackOfferViewModel({required this.context});

  BuildContext context;

  List<AllWizardText> _filteredIndustries = [];
  List<AllWizardText> get filteredIndustries => _filteredIndustries;

  List<AllWizardText> _industryList = [];
  List<AllWizardText> get industryList => _industryList;

  List<AllWizardText> _selectedIndustriesOfferWithContract = [];
  List<AllWizardText> get selectedIndustriesOfferWithContract =>
      _selectedIndustriesOfferWithContract;

  List<Data> _industryFullList = [];
  List<Data> get industryFullList => _industryFullList;

  List<Map<String, dynamic>> _industryDescriptions = [];
  List<Map<String, dynamic>> get industryDescriptions => _industryDescriptions;

  List<PlatformFile> _createOfferContractFiles = [];
  List<PlatformFile> get createOfferContractFiles => _createOfferContractFiles;

  // File Selector - only used for android devices
  List<XFile> _createOfferContractFileSelector = [];
  List<XFile> get createOfferContractFileSelector =>
      _createOfferContractFileSelector;

  List<ProjectTaskTypes> _taskTypes = [];
  List<ProjectTaskTypes> get taskTypes => _taskTypes;

  Map<String, dynamic> _createOfferContractForm = {"offerSignatureValid": "30"};
  Map<String, dynamic> get createOfferContractForm => _createOfferContractForm;

  Map<String, dynamic> _contractWizardJsonSteps = {};
  Map<String, dynamic> get contractWizardJsonSteps => _contractWizardJsonSteps;

  Map<String, dynamic> _contractHeaders = {};
  Map<String, dynamic> get contractHeaders => _contractHeaders;

  ContractTemplateModel? _contractTemplate;
  ContractTemplateModel? get contractTemplate => _contractTemplate;

  PartnerProjectsModel? _contractProjectId;
  PartnerProjectsModel? get contractProjectId => _contractProjectId;

  PartnerProjectsModel? _currentProject;
  PartnerProjectsModel? get currentProject => _currentProject;

  int _createOfferContractStep = 0;
  int get createOfferContractStep => _createOfferContractStep;

  double _tempTotalPrice = 0;
  double get tempTotalPrice => _tempTotalPrice;

  bool _isLoading = false;
  bool get isLoading => _isLoading;

  set tempTotalPrice(double newPrice) {
    _tempTotalPrice = newPrice;
    notifyListeners();
  }

  Future<void> updateContract({required Map<String, dynamic> payload}) async {
    final contractService = context.read<ContractService>();

    final Map<String, dynamic> contractPayload = {};

    final currentProject =
        createOfferContractForm['currentProject'] as PartnerProjectsModel;

    ContractModel contract =
        createOfferContractForm['contract'] as ContractModel;

    ContractTemplateModel template =
        createOfferContractForm['contractTemplate'] as ContractTemplateModel;

    Documents? document;

    Map<String, dynamic> templateSteps = {...template.steps};

    ContractModel? updatedContract;
    String contractPdfFilePath = '';

    if ((contract.documents ?? []).isNotEmpty) {
      document = (contract.documents ?? []).last;
    } else {
      do {
        final contractResponse = await contractService.getContractInfoOnly(
            contractId: currentProject.contractId);

        if (contractResponse.data is Map) {
          contract = ContractModel.fromJson(contractResponse.data);
        }

        if (!(contractResponse.success ?? true)) {
          throw ApiException(message: contractResponse.message ?? '');
        }

        await Future.delayed(const Duration(seconds: 2));
      } while ((contract.documents ?? []).isEmpty);

      document = (contract.documents ?? []).last;
    }

    contractPayload.addAll({
      "paymentStages": document.jsonData?.paymentStages,
      "totalpricewithvatandfee":
          document.jsonData?.totalpricewithvatandfee ?? 0,
      "totalpricewithvat": document.jsonData?.totalpricewithvat ?? 0,
      "projectId": document.jsonData?.projectId ?? 0,
      "templateId": document.jsonData?.templateId ?? 0,
      "contractTemplateId": document.jsonData?.contractTemplateId ?? 0,
      "title": document.jsonData?.title ?? '',
      "companyId": document.jsonData?.companyId ?? 0,
      "contactId": document.jsonData?.contactId ?? 0,
      "name": document.jsonData?.name ?? '',
      "lastname": document.jsonData?.lastName ?? '',
      "address": document.jsonData?.address ?? '',
      "zipcode": document.jsonData?.zipcode ?? '',
      "email": document.jsonData?.email ?? '',
      "mobile": document.jsonData?.mobile ?? '',
      "city": document.jsonData?.city ?? '',
      "cvr": document.jsonData?.cvr ?? '',
      "companyName": document.jsonData?.companyName ?? '',
      "offerId": document.jsonData?.offerId ?? 0,
      "createdFrom": 'partner-app'
    });

    for (var element in payload.entries) {
      contractPayload[element.key.split(":").last] = '${element.value}';

      int index = (templateSteps['step_${element.key.split(':').first}']
              ['fields'] as List)
          .indexWhere((l) => l['field_name'] == element.key.split(':').last);

      templateSteps['step_${element.key.split(':').first}']['fields'][index]
          ['default_value'] = '${element.value}';
    }

    contractPayload['steps'] = jsonEncode(templateSteps);

    await contractService.updateContract(
        form: contractPayload, contractId: currentProject.contractId);

    do {
      final response = await contractService.getContractInfoOnly(
          contractId: currentProject.contractId);

      if (response.data is Map) {
        updatedContract = ContractModel.fromJson(response.data);
      }

      if (!(response.success ?? true)) {
        throw ApiException(message: response.message ?? '');
      }

      await Future.delayed(const Duration(seconds: 2));
    } while (
        updatedContract == null && (updatedContract?.documents ?? []).isEmpty);

    final htmlResponse = await contractService.getContractHtmlPreview(
        contractId: currentProject.contractId);

    if (htmlResponse.data != null) {
      final output = await getTemporaryDirectory();
      final generatedPdfFile = await HtmlToPdf.convertFromHtmlContent(
        htmlContent: "${htmlResponse.data}",
        printPdfConfiguration: PrintPdfConfiguration(
            targetDirectory: output.path, targetName: "contract"),
      );
      contractPdfFilePath = generatedPdfFile.path;
    }

    template.steps = templateSteps;

    _createOfferContractForm['contractPdfFilePath'] = contractPdfFilePath;
    _createOfferContractForm['contract'] = updatedContract;
    _createOfferContractForm['contractTemplate'] = template;

    notifyListeners();
  }

  Future<void> fagenSelectContract({required int projectId}) async {
    final offerService = context.read<OfferService>();
    final contractService = context.read<ContractService>();

    final Map<String, dynamic> newForm = {...createOfferContractForm};

    List<PartnerProjectsModel> subProjects =
        createOfferContractForm['subProjects'];
    List<ListPriceMinbolig> listPricesMinbolig = [];

    final selectedProject = subProjects.firstWhere(
      (element) => element.id == projectId,
      orElse: () => PartnerProjectsModel(),
    );

    do {
      final listPriceResponse = await offerService.getListPriceMinbolig(
        contactId: createOfferContractForm['contact']['ID'],
        projectId: selectedProject.parentId ?? 0,
      );

      if (listPriceResponse != null) {
        listPricesMinbolig = ListPriceMinbolig.fromCollection(
            (listPriceResponse.data as Map<String, dynamic>)['result']);
      }

      await Future.delayed(const Duration(seconds: 2));
    } while (listPricesMinbolig.isEmpty);

    double totalPriceWithVat = listPricesMinbolig.fold(
        0, (sum, element) => sum + num.parse(element.totalVat ?? '0'));

    // var totalFessResponse = await contractService.getTotalFees(
    //     totalWithVat: totalPriceWithVat.toInt());
    // final contractResponse = await contractService.getContractInfoV2(
    //     contractValue: totalPriceWithVat.toInt(),
    //     offerId: selectedProject.offers!.first.id!,
    //     projectId: selectedProject.id!,
    //     wholeEnterprise:
    //         selectedProject.contractType == 'Hovedentreprise' ? '1' : '0',
    //     homeownerId: createOfferContractForm['contact']['ID']);
    final responses = await Future.wait([
      contractService.getTotalFees(totalWithVat: totalPriceWithVat.toInt()),
      contractService.getContractInfoV2(
          contractValue: totalPriceWithVat.toInt(),
          offerId: selectedProject.offers!.first.id!,
          projectId: selectedProject.id!,
          wholeEnterprise:
              selectedProject.contractType == 'Hovedentreprise' ? '1' : '0',
          homeownerId: createOfferContractForm['contact']['ID']),
    ]);

    if (responses[1] != null) {
      final tempContractTemplate = ContractTemplateModel.fromJson(
          (responses[1]!.data as Map<String, dynamic>)['contract']);

      for (final item
          in (tempContractTemplate.steps as Map<String, dynamic>).values) {
        final template = TemplateSteps.fromJson(item as Map<String, dynamic>);
        _contractWizardJsonSteps
            .addAll({"step_${template.key}": template.fields});
      }

      _contractTemplate = tempContractTemplate;
    }

    if (responses[0] != null) {
      newForm.addAll({
        "listPriceMinbolig": listPricesMinbolig,
        "totalPriceWithVat": totalPriceWithVat,
        "totalPriceWithVatAndFee": totalPriceWithVat +
            (responses[0]!.data as Map<String, dynamic>)['FEEINAMOUNT'],
        "project": selectedProject,
        "contractInfoPayload": {
          "contractValue": totalPriceWithVat.toInt(),
          "offerId": selectedProject.offers!.first.id!,
          "projectId": selectedProject.id!,
          "wholeEnterprise":
              selectedProject.contractType == 'Hovedentreprise' ? '1' : '0',
          "homeownerId": createOfferContractForm['contact']['ID']
        }
      });
    }

    _createOfferContractStep = createOfferContractStep + 1;
    _createOfferContractForm = {...newForm};
    notifyListeners();
  }

  Future<bool?> signContract({required String signature}) async {
    final contractService = context.read<ContractService>();

    String contractPdfFilePath = '';

    final currentProject =
        (createOfferContractForm['currentProject'] as PartnerProjectsModel);

    var response = await contractService.sendContractSignature(
        contractId: currentProject.contractId, signaturePng: signature);

    if (!(response.success ?? true)) {
      throw ApiException(message: response.message ?? '');
    }

    final contractResponse = await contractService.getContractInfoOnly(
        contractId: currentProject.contractId);

    if (!(contractResponse.success ?? true)) {
      throw ApiException(message: contractResponse.message ?? '');
    }

    final contractInfo = ContractModel.fromJson(contractResponse.data);

    final htmlResponse = await contractService.getContractHtmlPreview(
        contractId: currentProject.contractId);

    if (htmlResponse.data != null) {
      final output = await getTemporaryDirectory();
      final generatedPdfFile = await HtmlToPdf.convertFromHtmlContent(
        htmlContent: "${htmlResponse.data}",
        printPdfConfiguration: PrintPdfConfiguration(
            targetDirectory: output.path, targetName: "contract"),
      );
      contractPdfFilePath = generatedPdfFile.path;
    }

    _createOfferContractForm.addAll(
        {'contract': contractInfo, 'contractPdfFilePath': contractPdfFilePath});

    notifyListeners();

    return true;
  }

  Future<void> createContract(
      {required Map<String, dynamic> newForm,
      required String cvr,
      required String companyName,
      required bool isAffiliate}) async {
    final contractService = context.read<ContractService>();
    final projectService = context.read<ProjectsService>();
    final listPriceMinbolig =
        createOfferContractForm['listPriceMinbolig'] as List<ListPriceMinbolig>;

    Map<String, dynamic> tempJsonSteps = {..._contractWizardJsonSteps};
    Map<String, dynamic> contractPayload = {};
    List<Map<String, dynamic>> paymentStages = [];

    ContractModel? tempContractModel;

    for (var element in newForm.entries) {
      if (RegExp(r'\d').hasMatch(element.key)) {
        final step = element.key.split(':').first;
        final key = element.key.split(':').last;

        List<StepField?> fields =
            tempJsonSteps['step_$step'] as List<StepField?>;

        final index = fields.indexWhere((element) => element?.fieldName == key);
        fields[index]?.defaultValue = element.value;

        tempJsonSteps['step_$step'] = fields;
      }
    }
    PartnerProjectsModel currentProject =
        createOfferContractForm['project'] as PartnerProjectsModel;

    for (var element in listPriceMinbolig) {
      paymentStages.add({
        "description_payment": element.seriesOriginal,
        "project_id": currentProject.id,
        "partner_id": currentProject.partner!.id,
        "contact_id": currentProject.homeOwner!.id,
        "calculation_id": 0,
        "industry_id": element.industry,
        "job_industry_id": element.jobIndustry,
        "price_total": element.total,
        "vat_total": element.totalVat,
        "note": element.note,
        "signature": int.parse(element.signature!),
        "weeks": int.parse(element.weeks!),
        "start_date": element.startDate,
        "offer_id": int.parse(element.offerId!),
        "sub_project_id": element.subProjectId
      });
    }

    contractPayload.addAll({
      "paymentStages": [...paymentStages],
      "steps": jsonEncode(tempJsonSteps),
      "totalpricewithvatandfee":
          createOfferContractForm['totalPriceWithVatAndFee'],
      "totalpricewithvat": createOfferContractForm['totalPriceWithVat'],
      "projectId": currentProject.id,
      "templateId": contractTemplate!.templateId!,
      "contractTemplateId": contractTemplate!.id,
      "title": 'Kontrakt',
      "companyId": currentProject.partner!.id,
      "contactId": createOfferContractForm['contact']['ID'],
      "name": newForm['partnerFirstName'],
      "lastname": newForm['partnerLastName'],
      "address": newForm['partnerAddress'],
      "zipcode": newForm['partnerZipCode'],
      "email": newForm['partnerEmail'],
      "mobile": newForm['partnerPhoneNumber'],
      "city": newForm['partnerTown'],
      "cvr": cvr,
      "companyName": companyName,
      "offerId": createOfferContractForm['offerId'],
      "createdFrom": 'partner-app',
      "isAffiliate": isAffiliate ? 1 : 0
    });

    final response =
        await contractService.createContractWizard(payload: contractPayload);
    if (!(response.success ?? true)) {
      throw ApiException(message: response.message ?? '');
    }

    final projectResponse =
        await projectService.getProjectById(projectId: currentProject.id!);
    if (projectResponse != null) {
      if (!(projectResponse.success ?? true)) {
        throw ApiException(message: projectResponse.message ?? '');
      }

      currentProject = PartnerProjectsModel.fromJson(projectResponse.data);
      _currentProject = currentProject;

      do {
        final contractResponse = await contractService.getContractInfoOnly(
            contractId: currentProject.contractId);

        if (contractResponse.data is Map) {
          tempContractModel = ContractModel.fromJson(contractResponse.data);
          // contractUrl = contractResponse.data['url'];
        }

        if (!(contractResponse.success ?? true)) {
          throw ApiException(message: contractResponse.message ?? '');
        }

        await Future.delayed(const Duration(seconds: 2));
      } while (tempContractModel == null);

      // _contractHtmlString = contractUrl;

      final contractPdfResponse = await contractService.getContractPdfUrl(
          contractId: currentProject.contractId);

      _createOfferContractForm['project'] = currentProject;
      _createOfferContractForm.addAll({
        "contractInfo": tempContractModel,
        "contractPdf": contractPdfResponse.data
      });
    }
  }

  Future<void> createContactAddressProjectOffer({
    required Map<String, dynamic> contractPayload,
    required bool isAffiliate,
  }) async {
    final bookingService = context.read<BookingWizardService>();
    final offerService = context.read<OfferService>();
    final contractService = context.read<ContractService>();
    final projectService = context.read<ProjectsService>();
    final myProjectsVm = context.read<MyProjectsViewModel>();

    Map<String, dynamic> tempCreateOfferContractForm = {
      ...createOfferContractForm
    };

    final List<int> taskTypeIds = [];
    final List<String> addEnterpriseSubProjectNew = [];
    final Map<String, dynamic> descriptionForm = {};
    final List<String> jobList = [];
    List<dynamic> finalJobList = [];
    List<Map<String, dynamic>> offerPayloadSaveListPriceList = [];
    // List<ListPriceMinbolig> listPricesMinbolig = [];
    // List<Map<String, dynamic>> paymentStages = [];

    Map<String, dynamic> tempContractPayload = {...contractPayload};

    String mergeFileName = '';
    String mergeFile = '';

    DeficiencyUser? contact;
    Address address = tempCreateOfferContractForm['customerAddress'] as Address;
    PartnerProjectsModel? currentProject;
    int offerId = 0;
    ContractTemplateModel? contractTemplate2;
    ContractModel? contract;
    String contractPdfFilePath = '';

    bool isFagen =
        tempCreateOfferContractForm['contractType'] == 'Fagentreprise';

    final String startDate = tempContractPayload['STARTDATE'];
    final String endDate = tempContractPayload['ENDDATE'];

    try {
      // MERGE FILES
      if (createOfferContractFiles.length == 1 ||
          createOfferContractFileSelector.length == 1) {
        if (createOfferContractFileSelector.isNotEmpty) {
          mergeFile = base64Encode(
              await File(createOfferContractFileSelector.first.path)
                  .readAsBytes());
        } else if (createOfferContractFiles.isNotEmpty) {
          mergeFile = base64Encode(
              await File(createOfferContractFiles.first.path!).readAsBytes());
        }
      } else {
        List<Map<String, dynamic>> files = [];
        if (createOfferContractFiles.isNotEmpty) {
          // File Picker - only used for iOS devices
          for (var element in createOfferContractFiles) {
            files.add({
              "fileName": element.name,
              "fileContent":
                  base64Encode(await File(element.path!).readAsBytes()),
            });
          }
        } else if (createOfferContractFileSelector.isNotEmpty) {
          // File Selector - only used for android devices
          for (var element in createOfferContractFileSelector) {
            files.add({
              "fileName":
                  '${element.name.replaceAll(RegExp('[^A-Za-z0-9]'), '')}.${element.mimeType!.split('/').last}',
              "fileContent": base64Encode(await element.readAsBytes()),
            });
          }
        }

        final mergeResponse =
            await offerService.fileMerger(payload: {"files": files});

        if ((mergeResponse.success ?? false)) {
          mergeFileName = mergeResponse.data['fileName'];
          mergeFile = mergeResponse.data['file'];
        }
      }

      //

      // CREATE CONTACT
      final contactResponse = await bookingService.createContact(
          name:
              '${tempCreateOfferContractForm['customerFirstName']} ${tempCreateOfferContractForm['customerLastName']}',
          email: tempCreateOfferContractForm['customerEmail'],
          mobile: tempCreateOfferContractForm['customerPhone'],
          isAffiliate: isAffiliate);

      if ((contactResponse.success ?? false)) {
        contact = DeficiencyUser.fromJson(contactResponse.data['contact']);
      }

      //

      // CREATE ADDRESS
      final addressResponse = await bookingService.createAddress(
          contactId: contact?.id ?? 0,
          addressTitle: address.address ?? '',
          addressId: address.addressId ?? '',
          adgangsaddressId: address.adgangsAddressId ?? '',
          isAffiliate: isAffiliate);

      if ((addressResponse.success ?? false)) {
        address = Address.fromJson(
            addressResponse.data['address'] as Map<String, dynamic>);
      }

      //

      // CREATE PROJECT
      double totalPriceWithoutVat = industryDescriptions
          .expand((industry) => industry['subIndustry'] as List)
          .fold(0, (sum, subIndustry) => sum + subIndustry['price']);
      double totalPriceWithVat = industryDescriptions
          .expand((industry) => industry['subIndustry'] as List)
          .fold(0, (sum, subIndustry) => sum + subIndustry['priceVat']);

      for (final element in _industryDescriptions
          .expand((element) => element['subIndustry'] as List)) {
        final task = taskTypes.firstWhere(
          (task) =>
              task.industryId == int.parse(element['subIndustryId'] as String),
          orElse: () => ProjectTaskTypes(subcategories: []),
        );

        if ((task.subcategories ?? []).isNotEmpty) {
          for (final subTask in (task.subcategories?.first.taskTypes ?? [])) {
            taskTypeIds.add(subTask.taskTypeId);
          }
        }
      }

      for (final industry in industryDescriptions) {
        addEnterpriseSubProjectNew.add(
            '${industry['productTypeId']}: ${(industry['subIndustry'] as List).map((e) => e['subIndustryId']).join('-')}');

        for (final subIndustry in industry['subIndustry'] as List) {
          final Map<String, dynamic> subIndustryMap =
              subIndustry as Map<String, dynamic>;
          descriptionForm.putIfAbsent(
              'text_${industry['productTypeId']}_${subIndustryMap['subIndustryId']}',
              () => subIndustryMap['descriptions']);

          final job =
              '${subIndustry['subIndustryId']}::${industryDescriptions.map((item) => item['subIndustry'] as List).expand((subIndustries) => subIndustries).toList().where((a) => a['subIndustryId'] as String == subIndustry['subIndustryId'] as String).toList().map((b) => b['productTypeId']).join('-')}';

          if (!jobList.contains(job)) jobList.add(job);
        }
      }

      if (!isFagen) {
        finalJobList = jobList.map((e) => [e]).toList();
      } else {
        finalJobList = [jobList];
      }

      final Map<String, dynamic> createProjectForm = {
        "contactId": contact?.id ?? 0,
        "addressId": address.id,
        "name": tempCreateOfferContractForm['projectName'],
        "description": '',
        "source": "app-partner",
        "status": "new_project",
        "contractType": tempCreateOfferContractForm['contractType'],
        "industry": industryDescriptions
            .expand((element) => element['subIndustry'] as List)
            .first['subIndustryName'],
        "industryNew": [
          ...industryDescriptions
              .expand((element) => element['subIndustry'] as List)
              .map((e) => int.parse(e['subIndustryId'] as String))
        ],
        "subcategoryNew": [
          ...industryDescriptions
              .expand((element) => element['subIndustry'] as List)
              .map((e) => int.parse(e['subIndustryId'] as String))
        ],
        "taskTypeNew": taskTypeIds,
        "enterpriseIndustry": [
          ...selectedIndustriesOfferWithContract.map((e) => e.producttypeid)
        ],
        "enterpriseIndustryNew": addEnterpriseSubProjectNew,
        "jobsList": finalJobList,
        "budget": totalPriceWithoutVat,
        "isAffiliate": isAffiliate ? 1 : 0
      };

      createProjectForm.addAll(descriptionForm);

      if (applic.env != 'production') {
        createProjectForm.putIfAbsent('testProject', () => true);
      }

      final projectResponse =
          await bookingService.createProject(form: createProjectForm);

      if ((projectResponse.success ?? false)) {
        currentProject = PartnerProjectsModel.fromJson(
            projectResponse.data['project'] as Map<String, dynamic>);
      }

      //

      if (!isFagen) {
        // RESERVE PROJECT
        await bookingService.reserveProject(
            contactId: contact?.id ?? 0,
            projectId: currentProject?.id ?? 0,
            isAffiliate: isAffiliate);

        //

        // CREATE OFFER
        for (final industry in industryDescriptions) {
          for (final subIndustry in industry['subIndustry']) {
            offerPayloadSaveListPriceList.add({
              "project_id": currentProject?.id ?? 0,
              "sub_project_id": 0,
              "contact_id": contact?.id ?? 0,
              "calculation_id": 0,
              "industry_id": subIndustry['productTypeId'],
              "total": subIndustry['price'],
              "note": '',
              "reviews_id": [],
              "job_industry_id": subIndustry['subIndustryId'],
              "signature": '',
              "STARTDATE": startDate,
              "ENDDATE": endDate,
              "expiry": createOfferContractForm['offerSignatureValid'],
              "weeks": createOfferContractForm['projectEstimate'],
              "days": createOfferContractForm['projectEstimate'],
              "offer_id": 0,
              "parent_offer_id": 0,
              "job_description": subIndustry['descriptions']
            });
          }
        }

        final offerPayload = {
          "contactId": contact?.id ?? 0,
          "createdFrom": "partner-app",
          "projectId": currentProject?.id ?? 0,
          "jobIndustry": industryDescriptions[0]['productTypeId'],
          "type": 'fastcontract',
          "files": [],
          "offers": [
            {
              "title":
                  "Tilbud på projektnr. ${currentProject?.id ?? 0} til ${tempCreateOfferContractForm['projectName']}",
              "description": tempCreateOfferContractForm['projectName'],
              "subtotalPrice": totalPriceWithoutVat,
              "vat": (totalPriceWithoutVat * 0.25),
              "totalPrice": (totalPriceWithoutVat * 1.25),
              "expiry": createOfferContractForm['offerSignatureValid'],
              "weeks": createOfferContractForm['projectEstimate'],
              "days": createOfferContractForm['projectEstimate'],
              "STARTDATE": startDate,
              "ENDDATE": endDate,
              "fileName": mergeFileName,
              "file": mergeFile,
              "type": 'fastcontract',
              "tasks": [
                {
                  "title": '',
                  "description": '',
                  "unit": '',
                  "value": '',
                  "quantity": ''
                }
              ]
            }
          ],
          "saveListPriceV2": offerPayloadSaveListPriceList,
          "isAffiliate": isAffiliate ? 1 : 0
        };

        if (industryDescriptions
            .expand((a) => a['subIndustry'] as List)
            .where((e) => e['subIndustryId'] == '1062')
            .isNotEmpty) {
          offerPayload.putIfAbsent('jobIndustry', () => '1062');
        }

        final createOfferResponse =
            await bookingService.createOffer(params: offerPayload);

        if ((createOfferResponse.success ?? false)) {
          offerId = ((createOfferResponse.data as Map<String, dynamic>)['offer']
                  as List)
              .first['ID'];
        }

        //

        // SEND INVITE
        await bookingService.sendInvite(
            contactId: contact?.id ?? 0,
            projectId: currentProject?.id ?? 0,
            offerId: offerId,
            isAffiliate: isAffiliate);

        //

        // CREATE CONTRACT
        // do {
        //   final listPriceResponse = await offerService.getListPriceMinbolig(
        //     contactId: contact?.id ?? 0,
        //     projectId: currentProject?.id ?? 0,
        //   );

        //   if (listPriceResponse != null) {
        //     listPricesMinbolig = ListPriceMinbolig.fromCollection(
        //         (listPriceResponse.data as Map<String, dynamic>)['result']);
        //   }

        //   await Future.delayed(const Duration(seconds: 2));
        // } while (listPricesMinbolig.isEmpty);

        // double totalPriceWithVat = listPricesMinbolig.fold(
        //     0, (sum, element) => sum + num.parse(element.totalVat ?? '0'));

        // final feesResponse = await contractService.getTotalFees(
        //     totalWithVat: totalPriceWithVat.ceil());

        final contractTemplateResponse =
            await contractService.getContractInfoV2(
                contractValue: totalPriceWithVat.toInt(),
                offerId: offerId,
                projectId: currentProject?.id ?? 0,
                wholeEnterprise: '1',
                homeownerId: contact?.id ?? 0);

        if (contractTemplateResponse != null) {
          contractTemplate2 = ContractTemplateModel.fromJson(
              (contractTemplateResponse.data
                  as Map<String, dynamic>)['contract']);

          _contractTemplate = contractTemplate2;
        }

        for (var item in (contractTemplate2!.steps as Map).values) {
          final template = TemplateSteps.fromJson(item as Map<String, dynamic>);

          for (var element in (template.fields ?? []).where((element) =>
              (element?.fieldName ?? '').isNotEmpty &&
              (element?.defaultValue ?? '').isNotEmpty)) {
            tempContractPayload[element!.fieldName!] = element.defaultValue;
          }
        }

        // for (var element in listPricesMinbolig) {
        //   paymentStages.add({
        //     "description_payment": element.seriesOriginal,
        //     "project_id": currentProject?.id ?? 0,
        //     "partner_id": currentProject?.partner?.id ?? 0,
        //     "contact_id": currentProject?.homeOwner?.id ?? 0,
        //     "calculation_id": 0,
        //     "industry_id": element.industry,
        //     "job_industry_id": element.jobIndustry,
        //     "price_total": element.total,
        //     "vat_total": element.totalVat,
        //     "note": element.note,
        //     "signature": int.parse(element.signature!),
        //     "weeks": int.parse(element.weeks!),
        //     "start_date": tempContractPayload['STARTDATE'],
        //     "end_date": tempContractPayload['ENDDATE'],
        //     "offer_id": int.parse(element.offerId!),
        //     "sub_project_id": element.subProjectId
        //   });
        // }

        tempContractPayload.addAll({
          // "paymentStages": [...paymentStages],
          "steps": jsonEncode(contractTemplate2.steps),
          "totalpricewithvatandfee":
              totalPriceWithVat + _createOfferContractForm['projectPriceFees'],
          "totalpricewithvat": totalPriceWithVat,
          "projectId": currentProject?.id ?? 0,
          "templateId": contractTemplate2.templateId,
          "contractTemplateId": contractTemplate2.id,
          "title": 'Kontrakt',
          "companyId": currentProject?.partner?.id ?? 0,
          "contactId": contact?.id ?? 0,
          "OFFERID": offerId,
          "createdFrom": 'partner-app',
          "isVersion": false,
          "STARTDATE": startDate,
          "ENDDATE": endDate,
          "isAffiliate": isAffiliate ? 1 : 0
        });

        final response = await contractService.createContractWizard(
            payload: tempContractPayload);
        if (!(response.success ?? true)) {
          throw ApiException(message: response.message ?? '');
        }

        final projectResponse = await projectService.getProjectById(
            projectId: currentProject?.id ?? 0);

        if (projectResponse != null) {
          currentProject = PartnerProjectsModel.fromJson(projectResponse.data);
          _currentProject = currentProject;
          myProjectsVm.activeProject = currentProject;
        }

        do {
          final contractResponse = await contractService.getContractInfoOnly(
              contractId: currentProject!.contractId);

          if (contractResponse.data is Map) {
            contract = ContractModel.fromJson(contractResponse.data);
          }

          if (!(contractResponse.success ?? true)) {
            throw ApiException(message: contractResponse.message ?? '');
          }

          await Future.delayed(const Duration(seconds: 2));
        } while (contract == null);

        final htmlResponse = await contractService.getContractHtmlPreview(
            contractId: currentProject.contractId);

        if (htmlResponse.data != null) {
          final output = await getTemporaryDirectory();
          final generatedPdfFile = await HtmlToPdf.convertFromHtmlContent(
            htmlContent: "${htmlResponse.data}",
            printPdfConfiguration: PrintPdfConfiguration(
                targetDirectory: output.path, targetName: "contract"),
          );
          contractPdfFilePath = generatedPdfFile.path;
        }

        tempCreateOfferContractForm.addAll({
          "contact": contact,
          "address": address,
          "currentProject": currentProject,
          "OFFERID": offerId,
          "contractTemplate": contractTemplate2,
          "contract": contract,
          "contractPdfFilePath": contractPdfFilePath,
        });

        _contractProjectId = currentProject;
        _createOfferContractForm = {...tempCreateOfferContractForm};

        //
      } else {
        PartnerProjectsModel? currentSubProject;
        // GET SUB PROJECTS
        do {
          final subProjectResponse = await projectService.getSubprojects(
              projectId: currentProject?.id ?? 0);

          if ((subProjectResponse.success ?? false)) {
            final subProjects =
                PartnerProjectsModel.fromCollection(subProjectResponse.data);

            if (subProjects.isNotEmpty) {
              currentSubProject = subProjects.first;
            }
          }
        } while (currentSubProject == null);

        //

        // RESERVE PROJECT
        await bookingService.reserveProject(
            contactId: contact?.id ?? 0,
            projectId: currentSubProject.id ?? 0,
            isAffiliate: isAffiliate);

        //

        // CREATE OFFER
        for (final industry in industryDescriptions) {
          for (final subIndustry in industry['subIndustry']) {
            offerPayloadSaveListPriceList.add({
              "project_id": currentProject?.id ?? 0,
              "sub_project_id": currentSubProject.id,
              "contact_id": contact?.id ?? 0,
              "calculation_id": 0,
              "industry_id": subIndustry['productTypeId'],
              "total": subIndustry['price'],
              "note": '',
              "reviews_id": [],
              "job_industry_id": subIndustry['subIndustryId'],
              "signature": '',
              "STARTDATE": startDate,
              "ENDDATE": endDate,
              "expiry": createOfferContractForm['offerSignatureValid'],
              "weeks": createOfferContractForm['projectEstimate'],
              "days": createOfferContractForm['projectEstimate'],
              "offer_id": 0,
              "parent_offer_id": 0,
              "job_description": subIndustry['descriptions']
            });
          }
        }

        final offerPayload = {
          "contactId": contact?.id ?? 0,
          "createdFrom": "partner-app",
          "projectId": currentSubProject.id ?? 0,
          "jobIndustry": industryDescriptions[0]['productTypeId'],
          "type": 'fastcontract',
          "offers": [
            {
              "title":
                  "Tilbud på projektnr. ${currentSubProject.id ?? 0} til ${tempCreateOfferContractForm['projectName']}",
              "description": tempCreateOfferContractForm['projectName'],
              "subtotalPrice": totalPriceWithoutVat,
              "vat": (totalPriceWithoutVat * 0.25),
              "totalPrice": (totalPriceWithoutVat * 1.25),
              "expiry": createOfferContractForm['offerSignatureValid'],
              "weeks": createOfferContractForm['projectEstimate'],
              "days": createOfferContractForm['projectEstimate'],
              "STARTDATE": startDate,
              "ENDDATE": endDate,
              "fileName": mergeFileName,
              "file": mergeFile,
              "type": 'fastcontract',
              "tasks": [
                {
                  "title": '',
                  "description": '',
                  "unit": '',
                  "value": '',
                  "quantity": ''
                }
              ]
            }
          ],
          "saveListPriceV2": offerPayloadSaveListPriceList,
          "isAffiliate": isAffiliate ? 1 : 0
        };

        if (industryDescriptions
            .expand((a) => a['subIndustry'] as List)
            .where((e) => e['subIndustryId'] == '1062')
            .isNotEmpty) {
          offerPayload.putIfAbsent('jobIndustry', () => '1062');
        }

        final createOfferResponse =
            await bookingService.createOffer(params: offerPayload);

        if ((createOfferResponse.success ?? false)) {
          offerId = ((createOfferResponse.data as Map<String, dynamic>)['offer']
                  as List)
              .first['ID'];
        }

        //

        // SEND INVITE
        await bookingService.sendInvite(
            contactId: contact?.id ?? 0,
            projectId: currentSubProject.id ?? 0,
            offerId: offerId,
            isAffiliate: isAffiliate);

        //

        // CREATE CONTRACT
        // do {
        //   final listPriceResponse = await offerService.getListPriceMinbolig(
        //     contactId: contact?.id ?? 0,
        //     projectId: currentProject?.id ?? 0,
        //   );

        //   if (listPriceResponse != null) {
        //     listPricesMinbolig = ListPriceMinbolig.fromCollection(
        //         (listPriceResponse.data as Map<String, dynamic>)['result']);
        //   }

        //   await Future.delayed(const Duration(seconds: 2));
        // } while (listPricesMinbolig.isEmpty);

        // double totalPriceWithVat = listPricesMinbolig.fold(
        //     0, (sum, element) => sum + num.parse(element.totalVat ?? '0'));

        // final feesResponse = await contractService.getTotalFees(
        //     totalWithVat: totalPriceWithVat.ceil());

        final contractTemplateResponse =
            await contractService.getContractInfoV2(
                contractValue: totalPriceWithVat.toInt(),
                offerId: offerId,
                projectId: currentProject?.id ?? 0,
                wholeEnterprise: '1',
                homeownerId: contact?.id ?? 0);

        if (contractTemplateResponse != null) {
          contractTemplate2 = ContractTemplateModel.fromJson(
              (contractTemplateResponse.data
                  as Map<String, dynamic>)['contract']);

          _contractTemplate = contractTemplate2;
        }

        for (var item in (contractTemplate2!.steps as Map).values) {
          final template = TemplateSteps.fromJson(item as Map<String, dynamic>);

          for (var element in (template.fields ?? []).where((element) =>
              (element?.fieldName ?? '').isNotEmpty &&
              (element?.defaultValue ?? '').isNotEmpty)) {
            tempContractPayload[element!.fieldName!] = element.defaultValue;
          }
        }

        // for (var element in listPricesMinbolig) {
        //   paymentStages.add({
        //     "description_payment": element.seriesOriginal,
        //     "project_id": currentSubProject.id ?? 0,
        //     "partner_id": currentSubProject.partner?.id ?? 0,
        //     "contact_id": currentSubProject.homeOwner?.id ?? 0,
        //     "calculation_id": 0,
        //     "industry_id": element.industry,
        //     "job_industry_id": element.jobIndustry,
        //     "price_total": element.total,
        //     "vat_total": element.totalVat,
        //     "note": element.note,
        //     "signature": int.parse(element.signature!),
        //     "weeks": int.parse(element.weeks!),
        //     "start_date": tempContractPayload['STARTDATE'],
        //     "end_date": tempContractPayload['ENDDATE'],
        //     "offer_id": int.parse(element.offerId!),
        //     "sub_project_id": element.subProjectId
        //   });
        // }

        tempContractPayload.addAll({
          // "paymentStages": [...paymentStages],
          "steps": jsonEncode(contractTemplate2.steps),
          "totalpricewithvatandfee":
              totalPriceWithVat + _createOfferContractForm['projectPriceFees'],
          "totalpricewithvat": totalPriceWithVat,
          "projectId": currentSubProject.id ?? 0,
          "templateId": contractTemplate2.templateId,
          "contractTemplateId": contractTemplate2.id,
          "title": 'Kontrakt',
          "companyId": currentSubProject.partner?.id ?? 0,
          "contactId": contact?.id ?? 0,
          "OFFERID": offerId,
          "createdFrom": 'partner-app',
          "isVersion": false,
          "STARTDATE": startDate,
          "ENDDATE": endDate,
          "isAffiliate": isAffiliate ? 1 : 0
        });

        final response = await contractService.createContractWizard(
            payload: tempContractPayload);
        if (!(response.success ?? true)) {
          throw ApiException(message: response.message ?? '');
        }

        final projectResponse = await projectService.getProjectById(
            projectId: currentSubProject.id ?? 0);

        if (projectResponse != null) {
          currentSubProject =
              PartnerProjectsModel.fromJson(projectResponse.data);
          _currentProject = currentSubProject;
          myProjectsVm.activeProject = currentSubProject;
        }

        do {
          final contractResponse = await contractService.getContractInfoOnly(
              contractId: currentSubProject.contractId);

          if (contractResponse.data is Map) {
            contract = ContractModel.fromJson(contractResponse.data);
          }

          if (!(contractResponse.success ?? true)) {
            throw ApiException(message: contractResponse.message ?? '');
          }

          await Future.delayed(const Duration(seconds: 2));
        } while (contract == null);

        final htmlResponse = await contractService.getContractHtmlPreview(
            contractId: currentSubProject.contractId);

        if (htmlResponse.data != null) {
          final output = await getTemporaryDirectory();
          final generatedPdfFile = await HtmlToPdf.convertFromHtmlContent(
            htmlContent: "${htmlResponse.data}",
            printPdfConfiguration: PrintPdfConfiguration(
                targetDirectory: output.path, targetName: "contract"),
          );
          contractPdfFilePath = generatedPdfFile.path;
        }

        tempCreateOfferContractForm.addAll({
          "contact": contact,
          "address": address,
          "currentProject": currentSubProject,
          "offerId": offerId,
          "contractTemplate": contractTemplate2,
          "contract": contract,
          "contractPdfFilePath": contractPdfFilePath,
        });

        _contractProjectId = currentSubProject;
        _createOfferContractForm = {...tempCreateOfferContractForm};

        //
      }

      notifyListeners();
    } catch (e) {
      log("message asd $e");
    }
  }

  Future<void> initCreateOfferWithContract() async {
    final contractService = context.read<ContractService>();

    resetStates();

    _isLoading = true;

    _contractHeaders = await contractService.returnHeaders();

    _isLoading = false;
    notifyListeners();
  }

  Future<void> getListPrices() async {
    try {
      final contractService = context.read<ContractService>();

      final List<Map<String, dynamic>> industryDescriptions = [];

      final tempForms = {...createOfferContractForm};

      for (var element in selectedIndustriesOfferWithContract) {
        industryDescriptions.add({
          "productName": element.producttypeDa,
          "productTypeId": element.producttypeid,
          "subIndustry": [
            {
              "productTypeId": element.producttypeid,
              "subIndustryName": _industryFullList
                  .firstWhere((l) => l.branchId.toString() == element.industry,
                      orElse: () => Data(branche: 'N/A'))
                  .branche,
              "subIndustryId": element.industry,
              "descriptions": [],
              "price": 0.0,
              "priceVat": 0.0
            }
          ]
        });
      }

      final totalSubIndustry = industryDescriptions
          .expand((element) => element['subIndustry'] as List)
          .toList()
          .length;

      double projectPrice =
          double.parse(createOfferContractForm['projectPrice']);

      bool priceHasVat = createOfferContractForm['priceHasVat'];

      double projectPriceWithVat = 0;
      double projectPriceWithoutVat = 0;

      if (priceHasVat) {
        projectPriceWithVat = projectPrice;

        projectPriceWithoutVat = projectPriceWithVat / 1.25;
      } else {
        projectPriceWithVat = projectPrice * 1.25;

        projectPriceWithoutVat = projectPrice;
      }

      var totalFessResponse = await contractService.getTotalFees(
          totalWithVat: projectPriceWithVat.ceil());

      for (var industry in industryDescriptions) {
        final subIndustryList = industry['subIndustry'] as List;

        for (var subIndustry in subIndustryList) {
          subIndustry['price'] =
              (projectPriceWithoutVat / totalSubIndustry).toDouble();
          subIndustry['priceVat'] =
              (projectPriceWithVat / totalSubIndustry).toDouble();
        }
      }

      tempForms['projectPriceFees'] = ((totalFessResponse.data
              as Map<String, dynamic>)['FEEINAMOUNT'] as int)
          .toDouble();

      _createOfferContractStep = _createOfferContractStep + 1;
      _industryDescriptions = [...industryDescriptions];
      _createOfferContractForm = {...tempForms};

      notifyListeners();
    } catch (e) {
      log("message asd $e");
    }
  }

  Future<void> updatePrice(
      {required int industryIndex,
      required int subIndustryIndex,
      required double price}) async {
    // final contractService = context.read<ContractService>();
    // final tempForms = {...createOfferContractForm};
    final tempIndustryDescriptions = [..._industryDescriptions];
    tempIndustryDescriptions[industryIndex]['subIndustry'][subIndustryIndex]
        ['price'] = price / 1.25;
    tempIndustryDescriptions[industryIndex]['subIndustry'][subIndustryIndex]
        ['priceVat'] = price;
    _industryDescriptions = [...tempIndustryDescriptions];

    // double totalPriceWithVat = 0;
    // for (var industry in tempIndustryDescriptions) {
    //   for (var subIndustry in industry['subIndustry'] as List) {
    //     totalPriceWithVat += subIndustry['priceVat'];
    //   }
    // }
    // var totalFessResponse = await contractService.getTotalFees(
    //     totalWithVat: totalPriceWithVat.ceil());
    // tempForms['projectPriceFees'] =
    //     ((totalFessResponse.data as Map<String, dynamic>)['FEEINAMOUNT'] as int)
    //         .toDouble();
    // _createOfferContractForm = {...tempForms};

    notifyListeners();
  }

  Future<void> deleteIndustry(
      {required int industryIndex, required int subIndustryIndex}) async {
    // final contractService = context.read<ContractService>();
    // final tempForms = {...createOfferContractForm};
    final tempIndustryDescriptions = [..._industryDescriptions];
    (tempIndustryDescriptions[industryIndex]['subIndustry'] as List)
        .removeAt(subIndustryIndex);
    _industryDescriptions = [...tempIndustryDescriptions];

    // double totalPriceWithVat = 0;
    // for (var industry in tempIndustryDescriptions) {
    //   for (var subIndutry in industry['subIndustry'] as List) {
    //     totalPriceWithVat += subIndutry['priceVat'];
    //   }
    // }
    // var totalFessResponse = await contractService.getTotalFees(
    //     totalWithVat: totalPriceWithVat.ceil());
    // tempForms['projectPriceFees'] =
    //     ((totalFessResponse.data as Map<String, dynamic>)['FEEINAMOUNT'] as int)
    //         .toDouble();
    // _createOfferContractForm = {...tempForms};

    notifyListeners();
  }

  void updateCreateOfferContractForms(
      {required String key, required dynamic value}) {
    final tempAnswers = {...createOfferContractForm};

    if (tempAnswers.containsKey(key)) {
      tempAnswers.update(key, (old) => value);
    } else {
      tempAnswers.putIfAbsent(key, () => value);
    }

    _createOfferContractForm = {...tempAnswers};

    notifyListeners();
  }

  void updateCreateOfferContractStep({required int step}) {
    _createOfferContractStep = step;
    notifyListeners();
  }

  void addOrDeleteOfferContractFiles(
      {List<PlatformFile>? files, int? index}) async {
    if (files != null) {
      _createOfferContractFiles.addAll(files);
    } else if (index != null) {
      _createOfferContractFiles.removeAt(index);
    }

    notifyListeners();
  }

  // File Selector - only for android devices since iOS has issues with file selector
  void addOrDeleteOfferContractFileSelector({XFile? file, int? index}) async {
    if (file != null) {
      _createOfferContractFileSelector.addAll([file]);
    } else if (index != null) {
      _createOfferContractFileSelector.removeAt(index);
    }

    notifyListeners();
  }

  void selectIndustryOfferWithContract({required AllWizardText industry}) {
    final tempList = [..._selectedIndustriesOfferWithContract];

    if (tempList.contains(industry)) {
      tempList.remove(industry);
    } else {
      tempList.add(industry);
    }

    _selectedIndustriesOfferWithContract = [...tempList];

    notifyListeners();
  }

  void searchIndustry({required String query}) {
    final tempList = [..._filteredIndustries];

    if (query.isNotEmpty) {
      _filteredIndustries = tempList.where((item) {
        final itemName = item.producttypeDa!.toLowerCase();
        return itemName.contains(query.toLowerCase());
      }).toList();
    } else {
      _filteredIndustries = [..._industryList];
    }

    notifyListeners();
  }

  void resetStates() {
    _filteredIndustries = [];
    _industryList = [];
    _selectedIndustriesOfferWithContract = [];
    _industryFullList = [];
    _industryDescriptions = [];
    _createOfferContractFiles = [];
    _createOfferContractFileSelector = [];
    _taskTypes = [];
    _createOfferContractForm = {"offerSignatureValid": "30"};
    _contractWizardJsonSteps = {};
    _contractHeaders = {};
    _contractTemplate = null;
    _contractProjectId = null;
    _currentProject = null;
    _createOfferContractStep = 0;
    _tempTotalPrice = 0;
    _isLoading = false;

    notifyListeners();
  }

  void setIndustry(
      {required List<AllWizardText> list1,
      required List<Data> list2,
      required List<ProjectTaskTypes> list3}) {
    _filteredIndustries = [...list1];
    _industryList = [...list1];
    _industryFullList = [...list2];
    _taskTypes = [...list3];

    notifyListeners();
  }
}
