import 'dart:developer';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/messages/contacts_response_model.dart';
import 'package:Haandvaerker.dk/model/messages/message_v2_response_model.dart';
import 'package:Haandvaerker.dk/services/messages/message_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MessageV2ViewModel extends BaseViewModel {
  MessageV2ViewModel({required this.context});
  final BuildContext context;

  MessageV2ResponseModel? _inbox;
  MessageV2ResponseModel? get inbox => _inbox;

  List<MessageV2Items> _filteredInbox = [];
  List<MessageV2Items> get filteredInbox => _filteredInbox;

  MessageV2ResponseModel? _sent;
  MessageV2ResponseModel? get sent => _sent;

  List<MessageV2Items> _filteredSent = [];
  List<MessageV2Items> get filteredSent => _filteredSent;

  List<ContactsResponseModel> _contacts = [];
  List<ContactsResponseModel> get contacts => _contacts;

  TextEditingController? _searchController;
  TextEditingController? get searchController => _searchController;

  List<MessageV2Items> _currentMessageThread = [];
  List<MessageV2Items> get currentMessageThread => _currentMessageThread;

  MessageV2ResponseModel _notificationPagination =
      MessageV2ResponseModel.defaults();
  MessageV2ResponseModel get notificationPagination => _notificationPagination;

  int _unreadInbox = 0;
  int get unreadInbox => _unreadInbox;

  void reset() {
    _inbox = null;
    _filteredInbox = [];
    _sent = null;
    _filteredSent = [];
    _contacts = [];
    _searchController = null;
    _currentMessageThread = [];
    _notificationPagination = MessageV2ResponseModel.defaults();
    _unreadInbox = 0;

    notifyListeners();
  }

  Future<void> getMessageThreads({required MessageV2Items message}) async {
    final messageService = context.read<MessageService>();

    setBusy(true);

    if (message.seen == 0) {
      final seenResponse =
          await messageService.markAsSeen(messageId: message.id!);

      if (seenResponse != null) {
        final updatedMessage =
            MessageV2Items.fromJson(seenResponse.data['message']);

        if (filteredInbox
            .where((element) => element.id == message.id)
            .isNotEmpty) {
          MessageV2ResponseModel tempInbox = inbox!;
          final index =
              filteredInbox.indexWhere((element) => element.id == message.id);

          final newInbox = [...filteredInbox];

          newInbox.removeAt(index);
          newInbox.insert(index, updatedMessage);

          _filteredInbox = [...newInbox];
          tempInbox.items = [...newInbox];
          if (_unreadInbox > 0) {
            _unreadInbox = unreadInbox - 1;
          }

          _inbox = tempInbox;
        } else if (filteredSent
            .where((element) => element.id == message.id)
            .isNotEmpty) {
          MessageV2ResponseModel tempSent = sent!;
          final index =
              filteredSent.indexWhere((element) => element.id == message.id);

          final newSent = [...filteredSent];

          newSent.removeAt(index);
          newSent.insert(index, updatedMessage);

          _filteredSent = [...newSent];
          tempSent.items = [...newSent];

          _sent = tempSent;
        }
      }
    }

    final threadResponse =
        await messageService.getMessageThread(messageId: message.id!);

    _currentMessageThread = threadResponse ?? <MessageV2Items>[];

    setBusy(false);

    notifyListeners();
  }

  void searchMessage({required String searchKey}) {
    if (searchKey.isNotEmpty) {
      _filteredInbox = [
        ...(inbox?.items ?? []).where((element) =>
            (element.partner?.name ?? '')
                .toLowerCase()
                .contains(searchKey.toLowerCase()) ||
            (element.contact?.name ?? '')
                .toLowerCase()
                .contains(searchKey.toLowerCase()) ||
            (element.title ?? '').contains(searchKey.toLowerCase()))
      ];
      _filteredSent = [
        ...(sent?.items ?? []).where((element) =>
            (element.partner?.name ?? '')
                .toLowerCase()
                .contains(searchKey.toLowerCase()) ||
            (element.contact?.name ?? '')
                .toLowerCase()
                .contains(searchKey.toLowerCase()) ||
            (element.title ?? '').contains(searchKey.toLowerCase()))
      ];
    } else {
      _filteredInbox = [...inbox?.items ?? []];
      _filteredSent = [...inbox?.items ?? []];
    }

    notifyListeners();
  }

  Future<void> getBothMessage(
      {int? projectId,
      required int page,
      required bool isLoadMore,
      String? from,
      int size = 20}) async {
    try {
      final messageService = context.read<MessageService>();

      setBusy(true);

      final inboxPayload = {
        'page': page,
        'inbox': true,
        'size': size,
      };

      final sentPayload = {
        'page': page,
        'sent': true,
        'size': size,
      };

      if (projectId != null) {
        inboxPayload['projectId'] = projectId;
        sentPayload['projectId'] = projectId;
      }

      if (isLoadMore) {
        if (from == 'inbox') {
          final inboxResponse =
              await messageService.getMessagesV2(params: inboxPayload);

          if (inboxResponse != null) {
            if (projectId != null) {
              _unreadInbox = inboxResponse.unread ?? 0;
            }

            _inbox = inboxResponse;
            _filteredInbox = _inbox?.items ?? [];
          }
        } else if (from == 'sent') {
          final sentResponse =
              await messageService.getMessagesV2(params: sentPayload);

          if (sentResponse != null) {
            _sent = sentResponse;
            _filteredSent = _sent?.items ?? [];
          }
        }
      } else {
        final responses = await Future.wait([
          messageService.getMessagesV2(params: inboxPayload),
          messageService.getMessagesV2(params: sentPayload),
        ]);

        if (responses[0] != null) {
          if (projectId != null) {
            _unreadInbox = responses[0]?.unread ?? 0;
          }
          _inbox = responses[0];

          _filteredInbox = _inbox?.items ?? [];
        }

        if (responses[1] != null) {
          _sent = responses[1];
          _filteredSent = _sent?.items ?? [];
        }
      }

      setBusy(false);

      notifyListeners();
    } catch (e) {
      log("message lalala $e");
    }
  }

  Future<List<MessageV2Items>> deleteNotification(
      {required List<MessageV2Items> currentItems,
      required int messageId}) async {
    final messageService = context.read<MessageService>();

    final tempCurrentItems = [...currentItems];

    await messageService.deleteMessage(messageId: messageId);

    tempCurrentItems.removeWhere((element) => element.id == messageId);

    return tempCurrentItems;
  }

  Future<List<MessageV2Items>> markAsSeenNotification(
      {required List<MessageV2Items> currentItems,
      required int messageId}) async {
    final messageService = context.read<MessageService>();

    await messageService.markAsSeen(messageId: messageId);
    final tempCurrentItems = [...currentItems];
    final index =
        tempCurrentItems.indexWhere((element) => element.id == messageId);

    tempCurrentItems[index].seen = 1;

    return tempCurrentItems;
  }

  Future<List<MessageV2Items>> getNotificationThread(
      {required int threadId}) async {
    final messageService = context.read<MessageService>();
    final response = await messageService.getMessageThread(messageId: threadId);

    return response ?? <MessageV2Items>[];
  }

  Future<void> getNotification({required int page}) async {
    final messageService = context.read<MessageService>();

    final response = await messageService.getMessagesV2(params: {
      'notification': true,
      'page': page,
      'size': 10,
    });

    if (response != null) {
      _notificationPagination = response;
    }

    notifyListeners();
  }

  set filteredInbox(List<MessageV2Items> filtered) {
    _filteredInbox = filtered;
    notifyListeners();
  }

  set filteredSent(List<MessageV2Items> filtered) {
    _filteredSent = filtered;
    notifyListeners();
  }

  set contacts(List<ContactsResponseModel> contacts) {
    _contacts = contacts;
    notifyListeners();
  }

  set searchController(TextEditingController? search) {
    _searchController = search;
    notifyListeners();
  }

  void defaultValues() {
    _inbox = null;
    _unreadInbox = 0;
    _sent = null;
    _contacts = [];
    _filteredInbox = [];
    _filteredSent = [];
    _searchController = TextEditingController();
    notifyListeners();
  }

  Future<bool?> deleteMessage({required int messageId}) async {
    final service = context.read<MessageService>();

    final response = await service.deleteMessage(messageId: messageId);

    return response.success;
  }

  Future<void> getContacts() async {
    final messageV2Service = context.read<MessageService>();
    final contactResponse = await messageV2Service.getContacts();

    if (contactResponse != null) {
      _contacts = contactResponse;
    }
    notifyListeners();
  }

  Future<void> getMessageThread({
    required int messageId,
  }) async {
    final messageV2Service = context.read<MessageService>();

    final threadResponse =
        await messageV2Service.getMessageThread(messageId: messageId);

    _currentMessageThread = threadResponse ?? <MessageV2Items>[];
    notifyListeners();
  }

  Future<List<MessageV2Items>> markAllAsSeen(
      {required List<MessageV2Items> currentItems}) async {
    final service = context.read<MessageService>();

    final response = await service.markAllAsSeen();

    if (currentItems.isNotEmpty) {
      if (response != null) {
        final tempCurrentItems = [...currentItems];
        for (var i = 0; i < tempCurrentItems.length; i++) {
          tempCurrentItems[i].seen = 1;
        }

        return tempCurrentItems;
      } else {
        return currentItems;
      }
    } else {
      return currentItems;
    }
  }

  Future<MinboligApiResponse> sendNewProjectMessage({
    required body,
    required contactId,
    required title,
  }) async {
    var service = context.read<MessageService>();

    var response = await service.sendNewProjectMessage(
      body: body,
      contactId: contactId,
      title: title,
    );

    if (response.success!) {}

    return response;
  }

  Future<MinboligApiResponse> sendProjectMessageThread({
    required threadId,
    required body,
    required contactId,
    required title,
  }) async {
    var service = context.read<MessageService>();

    var response = await service.sendProjectMessageThread(
      threadId: threadId,
      body: body,
      contactId: contactId,
      title: title,
    );

    if (response.success!) {}

    return response;
  }

  Future<MinboligApiResponse> postMessageAction(
      {required messageActionUrl}) async {
    var service = context.read<MessageService>();

    var response = await service.postMessageAction(
      messageActionUrl: messageActionUrl,
    );

    if (response.success!) {}

    return response;
  }

  Future<void> getNotifAndMessagesInitial() async {
    _searchController = TextEditingController();
    final messageV2Service = context.read<MessageService>();

    final responses = await Future.wait(
      [
        messageV2Service.getMessagesV2(params: {
          'notification': true,
          'page': 1,
          'size': 10,
        }),
        messageV2Service.getMessagesV2(params: {
          'page': 1,
          'inbox': true,
          'size': 10,
        }),
        messageV2Service.getContacts(),
        messageV2Service.getMessagesV2(params: {
          'page': 1,
          'sent': true,
          'size': 10,
        }),
      ],
    );

    if (responses[0] != null &&
        responses[1] != null &&
        responses[2] != null &&
        responses[3] != null) {
      _notificationPagination = responses[0] as MessageV2ResponseModel;
      _inbox = responses[1] as MessageV2ResponseModel?;
      _unreadInbox = (responses[1] as MessageV2ResponseModel?)?.unread ?? 0;
      _filteredInbox = _inbox?.items ?? [];
      _sent = responses[3] as MessageV2ResponseModel?;
      _filteredSent = _sent?.items ?? [];

      List<ContactsResponseModel> temp = [];
      temp.add(
        ContactsResponseModel(
          email: 'infohaandvaerker.dk',
          id: 1,
          name: 'Infohaandvaerker.dk',
          isActive: 1,
          isVerified: 1,
          mobile: '',
          package: '',
          profilePicture: '',
          type: '',
          zip: '',
        ),
      );
      temp.addAll(responses[2] as List<ContactsResponseModel>);
      _contacts = temp;
    }

    notifyListeners();
  }
}
