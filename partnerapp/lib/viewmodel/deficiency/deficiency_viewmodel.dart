import 'dart:developer';
import 'dart:io';

import 'package:Haandvaerker.dk/model/deficiency/deficiency_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/services/deficiency/deficiency_service.dart';
import 'package:Haandvaerker.dk/services/wizard/wizard_service.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DeficiencyViewmodel extends BaseViewModel {
  DeficiencyViewmodel({required this.context});

  final BuildContext context;

  List<DeficiencyModel> _deficiencyList = [];
  List<DeficiencyModel> get deficiencyList => _deficiencyList;

  List<FullIndustry> _fullIndustries = [];
  List<FullIndustry> get fullIndustries => _fullIndustries;

  String _endDate = '';
  String get endDate => _endDate;

  String? _lastUpdatedBy = '';
  String? get lastUpdatedBy => _lastUpdatedBy;

  bool? _preApproved = false;
  bool? get preApproved => _preApproved;

  bool? _approved = false;
  bool? get approved => _approved;

  void reset() {
    _deficiencyList = [];
    _fullIndustries = [];
    _endDate = '';
    _lastUpdatedBy = '';
    _preApproved = false;
    _approved = false;

    notifyListeners();
  }

  Future<void> getDeficiencyList({required int projectId}) async {
    try {
      setBusy(true);
      var service = context.read<DeficiencyService>();
      final wizardService = context.read<WizardService>();

      final response = await service.getDeficiencyList(projectId: projectId);
      final industryResponse = await wizardService.getFullIndustry();

      if (response.success!) {
        // _deficiency =
        //     DeficiencyObject.fromJson(response.data as Map<String, dynamic>);
        _deficiencyList = DeficiencyModel.fromCollection(
            (response.data as Map<String, dynamic>)['deficiency']);
        _preApproved = (response.data as Map<String, dynamic>)['preApproved'];
        _approved = (response.data as Map<String, dynamic>)['approved'];
        _lastUpdatedBy =
            (response.data as Map<String, dynamic>)['lastUpdatedBy'];
        if (deficiencyList.isNotEmpty) {
          _endDate = Formatter.formatDateStrings(
              type: DateFormatType.fullMonthYearDate,
              dateString: _deficiencyList.first.endDate);
        }
      }

      if (industryResponse != null) {
        _fullIndustries = [...industryResponse];
      }

      setBusy(false);
      notifyListeners();
    } catch (e) {
      log("getDeficiencyList $e");
      if (e is DioException) {
        log("getDeficiencyList DioException ${e.response!.data}");
      }

      setBusy(false);
    }
  }

  Future<void> addDeficiency(
      {required int projectId,
      required Map<String, dynamic> form,
      required List<File> images}) async {
    try {
      setBusy(true);
      var service = context.read<DeficiencyService>();

      await service.addDeficiency(
          projectId: projectId, form: form, images: images);

      getDeficiencyList(projectId: projectId);
    } catch (e) {
      log("addDeficiency $e");
      if (e is DioException) {
        log("addDeficiency DioException ${e.response!.data}");
      }

      setBusy(false);
    }
  }

  Future<void> updateDeficiency(
      {required int projectId,
      required int deficiencyId,
      required Map<String, dynamic> form,
      required List<File> images}) async {
    try {
      setBusy(true);
      var service = context.read<DeficiencyService>();

      await service.updateDeficiency(
          deficiencyId: deficiencyId,
          projectId: projectId,
          form: form,
          images: images);

      getDeficiencyList(projectId: projectId);
    } catch (e) {
      log("updateDeficiency $e");
      if (e is DioException) {
        log("updateDeficiency DioException ${e.response!.data}");
      }

      setBusy(false);
    }
  }

  Future<void> addDeficiencyComment(
      {required int projectId,
      required int deficiencyId,
      required String comment}) async {
    try {
      setBusy(true);
      var service = context.read<DeficiencyService>();

      await service.addDeficiencyComment(
          deficiencyId: deficiencyId, projectId: projectId, comment: comment);

      getDeficiencyList(projectId: projectId);
    } catch (e) {
      log("addDeficiencyComment $e");
      if (e is DioException) {
        log("addDeficiencyComment DioException ${e.response!.data}");
      }

      setBusy(false);
    }
  }

  Future<void> deleteDeficiency(
      {required int projectId, required int deficiencyId}) async {
    try {
      setBusy(true);
      var service = context.read<DeficiencyService>();

      await service.deleteDeficiency(
        deficiencyId: deficiencyId,
        projectId: projectId,
      );

      getDeficiencyList(projectId: projectId);
    } catch (e) {
      log("deleteDeficiency $e");
      if (e is DioException) {
        log("deleteDeficiency DioException ${e.response!.data}");
      }

      setBusy(false);
    }
  }

  Future<void> updateDeficiencyEndDate(
      {required int projectId, required String endDate}) async {
    try {
      setBusy(true);
      var service = context.read<DeficiencyService>();

      await service.updateDeficiencyEndDate(
        endDate: endDate,
        projectId: projectId,
      );

      getDeficiencyList(projectId: projectId);
    } catch (e) {
      log("updateDeficiencyEndDate $e");
      if (e is DioException) {
        log("updateDeficiencyEndDate DioException ${e.response!.data}");
      }

      setBusy(false);
    }
  }

  Future<void> preApproveDeficiency(
      {required int projectId, required bool approve}) async {
    try {
      setBusy(true);
      var service = context.read<DeficiencyService>();

      await service.preApproveDeficiency(
          projectId: projectId, approve: approve);

      getDeficiencyList(projectId: projectId);
    } catch (e) {
      log("preApproveDeficiency $e");
      if (e is DioException) {
        log("preApproveDeficiency DioException ${e.response!.data}");
      }

      setBusy(false);
    }
  }

  Future<void> markAsDoneDeficiency(
      {required int projectId, required int? deficiencyId}) async {
    try {
      setBusy(true);
      var service = context.read<DeficiencyService>();

      await service.markAsDoneDeficiency(
          projectId: projectId, deficiencyId: deficiencyId);

      getDeficiencyList(projectId: projectId);
    } catch (e) {
      log("markAsDoneDeficiency $e");
      if (e is DioException) {
        log("markAsDoneDeficiency DioException ${e.response!.data}");
      }

      setBusy(false);
    }
  }

  Future<void> approveDeficiency({required int projectId}) async {
    try {
      setBusy(true);
      var service = context.read<DeficiencyService>();

      await service.approveDeficiency(projectId: projectId);

      getDeficiencyList(projectId: projectId);
    } catch (e) {
      log("approveDeficiency $e");
      if (e is DioException) {
        log("approveDeficiency DioException ${e.response!.data}");
      }

      setBusy(false);
    }
  }
}
