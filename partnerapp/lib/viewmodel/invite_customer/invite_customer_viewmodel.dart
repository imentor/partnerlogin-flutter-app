import 'dart:convert';
import 'dart:io';

import 'package:Haandvaerker.dk/model/autocomplete/autocomplete_model.dart';
import 'package:Haandvaerker.dk/model/contact/contact_model.dart';
import 'package:Haandvaerker.dk/model/create_offer/list_price_partner_ai_v3_model.dart';
import 'package:Haandvaerker.dk/model/get_types_response_model.dart';
import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_new_version_model.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/booking_wizard/booking_wizard_service.dart';
import 'package:Haandvaerker.dk/services/offer/offer_service.dart';
import 'package:Haandvaerker.dk/services/payproff/payproff_service.dart';
import 'package:Haandvaerker.dk/services/projects/projects_service.dart';
import 'package:Haandvaerker.dk/services/settings/settings_service.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/utils/file_utils.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

class InviteCustomerFormKeys {
  static const invitationType = 'invitationType';
  static const customerAddress = 'customerAddress';
  static const customerFirstName = 'customerFirstName';
  static const customerLastName = 'customerLastName';
  static const customerEmail = 'customerEmail';
  static const customerMobile = 'customerMobile';
  static const selectedIndustry = 'selectedIndustry';
  static const productAnswers = 'productAnswers';
  static const industryDescriptions = 'industryDescriptions';
  static const industryTotalPriceWoVat = 'industryTotalPriceWoVat';
  static const industryTotalVat = 'industryTotalVat';
  static const industryTotalPriceVat = 'industryTotalPriceVat';
  static const industryTotalPriceVatFee = 'industryTotalPriceVatFee';
  static const tempIndustryTotalPriceWoVat = 'tempIndustryTotalPriceWoVat';
  static const projectEstimate = 'projectEstimate';
  static const projectStartDate = 'projectStartDate';
  static const offerConditions = 'offerConditions';
  static const saveOfferConditions = 'saveOfferConditions';
  static const offerRecommendations = 'offerRecommendations';
  static const customerContact = 'customerContact';
  static const customerProject = 'customerProject';
  static const customerProjectEditTitle = 'customerProjectEditTitle';
  static const customerProjectEditDescription =
      'customerProjectEditDescription';
  static const offerFiles = 'offerFiles';
  static const projectName = 'projectName';
  static const totalProjectPrice = 'projectPrice';
  static const offerValidity = 'offerValidity';
  static const companyName = 'companyName';
  static const companyNameEdit = 'companyNameEdit';
  static const offerComments = 'offerComments';
  static const quickOfferPriceDetails = 'quickOfferPriceDetails';
  static const previewOfferPdf = 'previewOfferPdf';
  static const uploadOfferMergeFileBase64 = 'uploadOfferMergeFileBase64';
  static const uploadOfferMergeFile = 'uploadOfferMergeFile';
  static const uploadOfferMergeFileName = 'uploadOfferMergeFileName';
  static const recipientType = 'recipientType';
  static const recipientTypeNew = 'recipientTypeNew';
  static const recipientTypeExisting = 'recipientTypeExisting';
  static const recipientTypeHomeowner = 'recipientTypeHomeowner';
  static const recipientTypeProject = 'recipientTypeProject';
}

class InviteCustomerType {
  static const createOffer = 'createOffer';
  static const uploadOffer = 'uploadOffer';
  static const quickOffer = 'quickOffer';
}

class InviteCustomerViewmodel extends BaseViewModel {
  InviteCustomerViewmodel({required this.context})
      : _bookingWizardService = context.read<BookingWizardService>(),
        _settingService = context.read<SettingsService>(),
        _projectService = context.read<ProjectsService>(),
        _offerService = context.read<OfferService>(),
        _payproffService = context.read<PayproffService>(),
        super();

  final BuildContext context;

  final BookingWizardService _bookingWizardService;
  final SettingsService _settingService;
  final ProjectsService _projectService;
  final OfferService _offerService;
  final PayproffService _payproffService;

  double _currentStep = 0;
  double get currentStep => _currentStep;

  int _currentProductQuestionnaireStep = 0;
  int get currentProductQuestionnaireStep => _currentProductQuestionnaireStep;

  Map<String, dynamic> _formValues = {
    InviteCustomerFormKeys.offerValidity: '30'
  };
  Map<String, dynamic> get formValues => _formValues;

  List<AllWizardText> _industryList = [];
  List<AllWizardText> get industryList => _industryList;

  List<Data> _industryFullList = [];
  List<Data> get industryFullList => _industryFullList;

  List<ProductWizardModelV2> _productQuestionnaires = [];
  List<ProductWizardModelV2> get productQuestionnaires =>
      _productQuestionnaires;

  List<PartnerProjectsModel> _homeownerProjects = [];
  List<PartnerProjectsModel> get homeownerProjects => _homeownerProjects;

  set homeownerProjects(List<PartnerProjectsModel> value) {
    _homeownerProjects = value;

    notifyListeners();
  }

  set currentStep(double value) {
    _currentStep = value;

    notifyListeners();
  }

  set currentProductQuestionnaireStep(int value) {
    _currentProductQuestionnaireStep = value;

    notifyListeners();
  }

  void resetPrices() {
    _formValues[InviteCustomerFormKeys.industryTotalPriceWoVat] = 0;
    _formValues[InviteCustomerFormKeys.industryTotalVat] = 0;
    _formValues[InviteCustomerFormKeys.industryTotalPriceVat] = 0;

    notifyListeners();
  }

  void reset() {
    _currentStep = 0;
    _currentProductQuestionnaireStep = 0;
    _formValues = {InviteCustomerFormKeys.offerValidity: '30'};
    _productQuestionnaires = [];

    notifyListeners();
  }

  void removeQuickOfferPriceDetails() {
    double totalPriceWoVat = 0;

    List<Map<String, dynamic>> tempList = [
      ...formValues[InviteCustomerFormKeys.quickOfferPriceDetails]
    ];

    tempList.removeLast();

    _formValues[InviteCustomerFormKeys.quickOfferPriceDetails] = [...tempList];

    for (var element
        in (_formValues[InviteCustomerFormKeys.quickOfferPriceDetails]
            as List<Map<String, dynamic>>)) {
      totalPriceWoVat += (element['quantity'] * element['price']);
    }

    _formValues[InviteCustomerFormKeys.industryTotalPriceWoVat] =
        totalPriceWoVat;
    _formValues[InviteCustomerFormKeys.industryTotalVat] =
        totalPriceWoVat * .25;
    _formValues[InviteCustomerFormKeys.industryTotalPriceVat] =
        totalPriceWoVat * 1.25;

    notifyListeners();
  }

  void removeIndustryFromQuickOfferPriceDetails(int index) {
    (_formValues[InviteCustomerFormKeys.quickOfferPriceDetails][index]
            as Map<String, dynamic>)
        .remove('industry');

    notifyListeners();
  }

  void updateQuickOfferPriceDetails(
      {required int index, required String name, required dynamic value}) {
    double totalPriceWoVat = 0;

    _formValues[InviteCustomerFormKeys.quickOfferPriceDetails][index][name] =
        value;

    for (var element
        in (_formValues[InviteCustomerFormKeys.quickOfferPriceDetails]
            as List<Map<String, dynamic>>)) {
      totalPriceWoVat += (element['quantity'] * element['price']);
    }

    _formValues[InviteCustomerFormKeys.industryTotalPriceWoVat] =
        totalPriceWoVat;
    _formValues[InviteCustomerFormKeys.industryTotalVat] =
        totalPriceWoVat * .25;
    _formValues[InviteCustomerFormKeys.industryTotalPriceVat] =
        totalPriceWoVat * 1.25;

    notifyListeners();
  }

  void updatePrice({required double price}) {
    _formValues[InviteCustomerFormKeys.industryTotalPriceWoVat] = price;
    _formValues[InviteCustomerFormKeys.industryTotalVat] = price * .25;
    _formValues[InviteCustomerFormKeys.industryTotalPriceVat] = price * 1.25;
    _formValues[InviteCustomerFormKeys.tempIndustryTotalPriceWoVat] = price;

    notifyListeners();
  }

  void updateIndustryDescription(
      {required int industryIndex,
      required int subIndustryIndex,
      required List<String> descriptions}) {
    _formValues[InviteCustomerFormKeys.industryDescriptions][industryIndex]
        ['subIndustry'][subIndustryIndex]['descriptions'] = [...descriptions];

    notifyListeners();
  }

  void updateIndustryDescriptionView(
      {required int industryIndex, required int subIndustryIndex}) {
    _formValues[InviteCustomerFormKeys.industryDescriptions][industryIndex]
            ['subIndustry'][subIndustryIndex]['viewDescriptions'] =
        !_formValues[InviteCustomerFormKeys.industryDescriptions][industryIndex]
            ['subIndustry'][subIndustryIndex]['viewDescriptions'];

    notifyListeners();
  }

  void removeIndustry(
      {required int industryIndex, required int subIndustryIndex}) {
    _formValues[InviteCustomerFormKeys.industryDescriptions][industryIndex]
        ['subIndustry'][subIndustryIndex]['price'] = 0;
    (_formValues[InviteCustomerFormKeys.industryDescriptions][industryIndex]
            ['subIndustry'] as List)
        .removeAt(subIndustryIndex);

    double totalPriceWoVat =
        (formValues[InviteCustomerFormKeys.industryDescriptions]
                as List<Map<String, dynamic>>)
            .expand(
                (value) => (value['subIndustry'] as List<Map<String, dynamic>>))
            .fold(0, (sum, subIndustry) => sum + subIndustry['price']);

    _formValues[InviteCustomerFormKeys.industryTotalPriceWoVat] =
        totalPriceWoVat;
    _formValues[InviteCustomerFormKeys.industryTotalVat] =
        totalPriceWoVat * .25;
    _formValues[InviteCustomerFormKeys.industryTotalPriceVat] =
        totalPriceWoVat * 1.25;

    notifyListeners();
  }

  void updateFormValues({required String key, required dynamic values}) {
    _formValues[key] = values;

    notifyListeners();
  }

  void updateWizardProductAnswers(
      {required String key,
      required String value,
      List<String>? options,
      List<String>? selectedChecks}) {
    Map<String, dynamic> tempProductControllers = {};

    if (formValues.containsKey(InviteCustomerFormKeys.productAnswers)) {
      tempProductControllers = {
        ...(formValues[InviteCustomerFormKeys.productAnswers] as Map)
      };
    }

    if (options != null && selectedChecks != null) {
      final selectedSet = Set<String>.from(selectedChecks);

      tempProductControllers.removeWhere((key, value) =>
          options.contains(value) && !selectedSet.contains(value));

      for (var element in selectedChecks) {
        if (options.contains(element)) {
          tempProductControllers['$key$element'] = element;
        }
      }
    } else {
      tempProductControllers[key] = value;
    }

    _formValues[InviteCustomerFormKeys.productAnswers] = {
      ...tempProductControllers
    };

    notifyListeners();
  }

  void updateUploadIndustryPrices() {
    List<Map<String, dynamic>> industryDescriptions = [
      ...(formValues[InviteCustomerFormKeys.industryDescriptions])
    ];

    int subIndustryLength =
        industryDescriptions.expand((value) => value['subIndustry']).length;

    for (var industryMap in industryDescriptions) {
      for (var subIndustryMap
          in (industryMap['subIndustry'] as List<Map<String, dynamic>>)) {
        subIndustryMap['price'] =
            formValues[InviteCustomerFormKeys.tempIndustryTotalPriceWoVat] /
                subIndustryLength;
      }
    }

    _formValues[InviteCustomerFormKeys.industryDescriptions] =
        industryDescriptions;

    _formValues[InviteCustomerFormKeys.industryTotalPriceWoVat] =
        formValues[InviteCustomerFormKeys.tempIndustryTotalPriceWoVat];
    _formValues[InviteCustomerFormKeys.industryTotalVat] =
        (formValues[InviteCustomerFormKeys.tempIndustryTotalPriceWoVat] * .25);
    _formValues[InviteCustomerFormKeys.industryTotalPriceVat] =
        (formValues[InviteCustomerFormKeys.tempIndustryTotalPriceWoVat] * 1.25);

    _currentStep = 5;
  }

  Future<bool> quickOfferExistingProject({
    required bool isAffiliate,
    required String signature,
    required String companyIndustry,
    required int companyId,
  }) async {
    setBusy(true);
    Contact contact = formValues[InviteCustomerFormKeys.customerContact];
    PartnerProjectsModel project =
        formValues[InviteCustomerFormKeys.customerProject];

    Offer? offer;

    if ((project.offers ?? []).isNotEmpty) {
      final tempOffer = (project.offers ?? []).firstWhere(
          (value) => value.company == '$companyId',
          orElse: () => Offer.defaults());

      if ((tempOffer.id ?? 0) > 0) {
        offer = tempOffer;
      }
    }

    List<Map<String, dynamic>> offerPayloadSaveListPriceList = [];

    DateTime startDate =
        DateTime.parse(formValues[InviteCustomerFormKeys.projectStartDate]);
    DateTime endDate = startDate.copyWith(month: startDate.month + 1);

    AllWizardText industry =
        industryList.firstWhere((value) => value.industry == companyIndustry);

    offerPayloadSaveListPriceList.add({
      "project_id": project.id!,
      "sub_project_id": 0,
      "contact_id": contact.id!,
      "calculation_id": 0,
      "industry_id": industry.producttypeid,
      "total": formValues[InviteCustomerFormKeys.industryTotalPriceWoVat],
      "note": '',
      "reviews_id": [],
      "job_industry_id": industry.industry,
      "signature": '',
      "STARTDATE": startDate.toString(),
      "ENDDATE": endDate.toString(),
      "expiry": 30,
      "weeks": 30,
      "days": 30,
      "offer_id": offer?.id ?? 0,
      "parent_offer_id": offer?.id ?? 0,
      "job_description": {
        'submitted': [
          '${formValues[InviteCustomerFormKeys.offerComments]} | ${formValues[InviteCustomerFormKeys.industryTotalPriceWoVat]}'
        ]
      }
    });

    final offerPayload = {
      "contactId": contact.id!,
      "createdFrom": "partner-app",
      "projectId": project.id!,
      "jobIndustry": industry.producttypeid,
      "type": 'fast-track',
      "fastTrackOfferType": 'quick',
      "offers": [
        {
          "title": "Tilbud på projektnr. ${project.id!} til ${project.name!}",
          "description": project.description,
          "subtotalPrice":
              formValues[InviteCustomerFormKeys.industryTotalPriceWoVat],
          "vat": formValues[InviteCustomerFormKeys.industryTotalVat],
          "totalPrice":
              formValues[InviteCustomerFormKeys.industryTotalPriceVat],
          "expiry": 30,
          "weeks": 30,
          "days": 30,
          "STARTDATE": startDate.toString(),
          "ENDDATE": endDate.toString(),
          "type": 'fast-track',
          "fileName": 'merged.pdf',
          "file":
              base64Encode(formValues[InviteCustomerFormKeys.previewOfferPdf]),
          "tasks": [
            {
              "title": '',
              "description": '',
              "unit": '',
              "value": '',
              "quantity": ''
            }
          ]
        }
      ],
      "saveListPriceV2": offerPayloadSaveListPriceList,
      "isAffiliate": isAffiliate ? 1 : 0,
      "expiry": 30,
      "start_date": startDate.toString()
    };

    if ((offer?.id ?? 0) > 0) {
      offerPayload['offerId'] = (offer?.id ?? 0);
    }

    final offerResponse = await _offerService.partnerOfferVersion(
        offerVersionPayload: offerPayload);

    if (!(offerResponse?.success ?? false)) {
      throw offerResponse?.message ?? '';
    }

    setBusy(false);
    return true;
  }

  Future<bool> uploadOfferExistingProject(
      {required int companyId,
      required String signature,
      required bool isAffiliate}) async {
    setBusy(true);

    final project = formValues[InviteCustomerFormKeys.customerProject]
        as PartnerProjectsModel;

    final homeowner =
        formValues[InviteCustomerFormKeys.customerContact] as Contact;

    Offer? offer;

    if ((project.offers ?? []).isNotEmpty) {
      final tempOffer = (project.offers ?? []).firstWhere(
          (value) => value.company == '$companyId',
          orElse: () => Offer.defaults());

      if ((tempOffer.id ?? 0) > 0) {
        offer = tempOffer;
      }
    }

    List<Map<String, dynamic>> offerPayloadSaveListPriceList = [];

    for (final industry
        in formValues[InviteCustomerFormKeys.industryDescriptions]
            as List<Map<String, dynamic>>) {
      for (final subIndustry
          in industry['subIndustry'] as List<Map<String, dynamic>>) {
        offerPayloadSaveListPriceList.add({
          "project_id": project.id!,
          "sub_project_id": 0,
          "contact_id": homeowner.id!,
          "calculation_id": 0,
          "industry_id": subIndustry['productTypeId'],
          "total": subIndustry['price'],
          "note": '',
          "reviews_id": [],
          "job_industry_id": subIndustry['subIndustryId'],
          "signature": signature,
          "STARTDATE": '${formValues[InviteCustomerFormKeys.projectStartDate]}',
          "ENDDATE":
              '${(formValues[InviteCustomerFormKeys.projectStartDate] as DateTime).copyWith(month: (formValues[InviteCustomerFormKeys.projectStartDate] as DateTime).month + 1)}',
          "expiry": formValues[InviteCustomerFormKeys.offerValidity],
          "weeks": formValues[InviteCustomerFormKeys.projectEstimate],
          "days": formValues[InviteCustomerFormKeys.projectEstimate],
          "offer_id": offer?.id ?? 0,
          "parent_offer_id": offer?.id ?? 0,
          "job_description": {'submitted': subIndustry['descriptions']}
        });
      }
    }

    final offerPayload = {
      "contactId": homeowner.id!,
      "createdFrom": "partner-app",
      "projectId": project.id!,
      "jobIndustry": formValues[InviteCustomerFormKeys.industryDescriptions][0]
          ['productTypeId'],
      "type": 'fast-track',
      "fastTrackOfferType": 'upload',
      "files": [],
      "offers": [
        {
          "title": "Tilbud på projektnr. ${project.id!} til ${project.name!}",
          "description": project.description,
          "subtotalPrice":
              formValues[InviteCustomerFormKeys.industryTotalPriceWoVat],
          "vat": formValues[InviteCustomerFormKeys.industryTotalVat],
          "totalPrice":
              formValues[InviteCustomerFormKeys.industryTotalPriceVat],
          "expiry": formValues[InviteCustomerFormKeys.offerValidity],
          "weeks": formValues[InviteCustomerFormKeys.projectEstimate],
          "days": formValues[InviteCustomerFormKeys.projectEstimate],
          "STARTDATE": '${formValues[InviteCustomerFormKeys.projectStartDate]}',
          "ENDDATE":
              '${(formValues[InviteCustomerFormKeys.projectStartDate] as DateTime).copyWith(month: (formValues[InviteCustomerFormKeys.projectStartDate] as DateTime).month + 1)}',
          "type": 'fast-track',
          "fileName":
              formValues[InviteCustomerFormKeys.uploadOfferMergeFileName],
          "file": formValues[InviteCustomerFormKeys.uploadOfferMergeFileBase64],
          "tasks": [
            {
              "title": '',
              "description": '',
              "unit": '',
              "value": '',
              "quantity": ''
            }
          ]
        }
      ],
      "saveListPriceV2": offerPayloadSaveListPriceList,
      "isAffiliate": isAffiliate ? 1 : 0
    };

    if ((formValues[InviteCustomerFormKeys.industryDescriptions] as List)
        .expand((a) => a['subIndustry'] as List)
        .where((e) => e['subIndustryId'] == '1062')
        .isNotEmpty) {
      offerPayload.putIfAbsent('jobIndustry', () => '1062');
    }

    if ((offer?.id ?? 0) > 0) {
      offerPayload['offerId'] = (offer?.id ?? 0);
    }

    final offerResponse = await _offerService.partnerOfferVersion(
        offerVersionPayload: offerPayload);

    if (!(offerResponse?.success ?? false)) {
      throw offerResponse?.message ?? '';
    }

    setBusy(false);
    return true;
  }

  Future<bool> createOfferExistingProject(
      {required int companyId,
      required String signature,
      required bool isAffiliate}) async {
    setBusy(true);

    final project = formValues[InviteCustomerFormKeys.customerProject]
        as PartnerProjectsModel;

    final homeowner =
        formValues[InviteCustomerFormKeys.customerContact] as Contact;

    Offer? offer;

    if ((project.offers ?? []).isNotEmpty) {
      final tempOffer = (project.offers ?? []).firstWhere(
          (value) => value.company == '$companyId',
          orElse: () => Offer.defaults());

      if ((tempOffer.id ?? 0) > 0) {
        offer = tempOffer;
      }
    }

    List<Map<String, dynamic>> offerPayloadSaveListPriceList = [];

    for (final industry
        in formValues[InviteCustomerFormKeys.industryDescriptions]
            as List<Map<String, dynamic>>) {
      for (final subIndustry
          in industry['subIndustry'] as List<Map<String, dynamic>>) {
        offerPayloadSaveListPriceList.add({
          "project_id": project.id!,
          "sub_project_id": 0,
          "contact_id": homeowner.id!,
          "calculation_id": 0,
          "industry_id": subIndustry['productTypeId'],
          "total": subIndustry['price'],
          "note": '',
          "reviews_id": [],
          "job_industry_id": subIndustry['subIndustryId'],
          "signature": signature,
          "STARTDATE": '${formValues[InviteCustomerFormKeys.projectStartDate]}',
          "ENDDATE":
              '${(formValues[InviteCustomerFormKeys.projectStartDate] as DateTime).copyWith(month: (formValues[InviteCustomerFormKeys.projectStartDate] as DateTime).month + 1)}',
          "expiry": formValues[InviteCustomerFormKeys.offerValidity],
          "weeks": formValues[InviteCustomerFormKeys.projectEstimate],
          "days": formValues[InviteCustomerFormKeys.projectEstimate],
          "offer_id": offer?.id ?? 0,
          "parent_offer_id": offer?.id ?? 0,
          "job_description": {'submitted': subIndustry['descriptions']}
        });
      }
    }

    final offerPayload = {
      "contactId": homeowner.id!,
      "createdFrom": "partner-app",
      "projectId": project.id!,
      "jobIndustry": formValues[InviteCustomerFormKeys.industryDescriptions][0]
          ['productTypeId'],
      "type": 'fast-track',
      "files": [],
      "fastTrackOfferType": 'extended',
      "offers": [
        {
          "title": "Tilbud på projektnr. ${project.id!} til ${project.name!}",
          "description": project.description,
          "subtotalPrice":
              formValues[InviteCustomerFormKeys.industryTotalPriceWoVat],
          "vat": formValues[InviteCustomerFormKeys.industryTotalVat],
          "totalPrice":
              formValues[InviteCustomerFormKeys.industryTotalPriceVat],
          "expiry": formValues[InviteCustomerFormKeys.offerValidity],
          "weeks": formValues[InviteCustomerFormKeys.projectEstimate],
          "days": formValues[InviteCustomerFormKeys.projectEstimate],
          "STARTDATE": '${formValues[InviteCustomerFormKeys.projectStartDate]}',
          "ENDDATE":
              '${(formValues[InviteCustomerFormKeys.projectStartDate] as DateTime).copyWith(month: (formValues[InviteCustomerFormKeys.projectStartDate] as DateTime).month + 1)}',
          "type": 'fast-track',
          "tasks": [
            {
              "title": '',
              "description": '',
              "unit": '',
              "value": '',
              "quantity": ''
            }
          ]
        }
      ],
      "saveListPriceV2": offerPayloadSaveListPriceList,
      "isAffiliate": isAffiliate ? 1 : 0
    };

    if ((formValues[InviteCustomerFormKeys.industryDescriptions] as List)
        .expand((a) => a['subIndustry'] as List)
        .where((e) => e['subIndustryId'] == '1062')
        .isNotEmpty) {
      offerPayload.putIfAbsent('jobIndustry', () => '1062');
    }

    if ((offer?.id ?? 0) > 0) {
      offerPayload['offerId'] = (offer?.id ?? 0);
    }

    final offerResponse = await _offerService.partnerOfferVersion(
        offerVersionPayload: offerPayload);

    if (!(offerResponse?.success ?? false)) {
      throw offerResponse?.message ?? '';
    }

    setBusy(false);

    return true;
  }

  Future<void> initExistingProject(
      {required Contact homeowner,
      required PartnerProjectsModel project,
      required String type,
      required String companyName}) async {
    setBusy(true);

    List<Map<String, dynamic>> industryDescriptions = [];
    List<Map<String, dynamic>> subIndustryList = [];

    final response = await _offerService.getListPricePartnerAiV3(payload: {
      'projectId': (project.parentId ?? 0) > 0 ? project.parentId : project.id,
      'contactId': homeowner.id,
    });

    List<ListPricePartnerAiV3Model> industrys = [
      ...(response ?? []).where((value) =>
          value.jobIndustry == project.jobIndustry &&
          value.industry == project.enterpriseIndustry)
    ];

    String subIndustryName = industryFullList
            .firstWhere(
              (value) => value.branchId == project.jobIndustry,
              orElse: () => Data.defaults(),
            )
            .branche ??
        '';

    String productTypeName = industryList
            .firstWhere(
                (value) =>
                    value.producttypeid ==
                    (project.enterpriseIndustry).toString(),
                orElse: () => AllWizardText.defaults())
            .producttypeDa ??
        '';

    for (final i in industrys) {
      List<String> tempDescription = [];

      for (var element in (i.series as List)) {
        if (element is String &&
            (element).trim() != '-' &&
            (element).isNotEmpty) {
          tempDescription.add((element).replaceAll('- ', ''));
        }
      }

      subIndustryList.add({
        'productTypeId': '${i.industry}',
        'productTypeName': productTypeName,
        'subIndustryId': '${i.jobIndustry}',
        'subIndustryName': subIndustryName,
        'descriptions': tempDescription,
        'originalDescriptions': tempDescription,
        'price': 0.0,
        'viewDescriptions': false
      });
    }

    industryDescriptions.add({
      'productTypeId': '${project.enterpriseIndustry}',
      'productTypeName': productTypeName,
      'subIndustry': subIndustryList
    });

    _formValues[InviteCustomerFormKeys.customerContact] = homeowner;
    _formValues[InviteCustomerFormKeys.customerProject] = project;
    _formValues[InviteCustomerFormKeys.invitationType] = type;
    _formValues[InviteCustomerFormKeys.recipientType] =
        InviteCustomerFormKeys.recipientTypeExisting;
    _formValues[InviteCustomerFormKeys.industryDescriptions] =
        industryDescriptions;

    if (type == InviteCustomerType.createOffer) {
      _currentStep = 3;
    } else if (type == InviteCustomerType.uploadOffer) {
      _currentStep = 2;
    } else if (type == InviteCustomerType.quickOffer) {
      _formValues.addAll({
        InviteCustomerFormKeys.companyName: companyName,
        InviteCustomerFormKeys.quickOfferPriceDetails: [
          {
            "title": '',
            "quantity": 1,
            "price": 0.0,
          }
        ],
        InviteCustomerFormKeys.companyNameEdit: false,
      });

      _currentStep = 2;
    }

    setBusy(false);
  }

  Future<List<Contact>> searchHomeowner({required String query}) async {
    try {
      final response = await _settingService.getCustomers(query: query);

      if (response != null) {
        return Contact.fromCollection(response.data);
      } else {
        return [];
      }
    } catch (e) {
      return [];
    }
  }

  Future<void> getHomeownersProjects({required int contactId}) async {
    setBusy(true);

    final response =
        await _settingService.getCustomersProjects(contactId: contactId);

    if (response != null) {
      _homeownerProjects = PartnerProjectsModel.fromCollection(response.data);
    }

    setBusy(false);
  }

  Future<void> initQuickOffer({required String companyName}) async {
    _formValues.addAll({
      InviteCustomerFormKeys.companyName: companyName,
      InviteCustomerFormKeys.quickOfferPriceDetails: [
        {
          "title": '',
          "quantity": 1,
          "price": 0.0,
        }
      ],
      InviteCustomerFormKeys.companyNameEdit: false,
    });

    _currentStep += 1;

    notifyListeners();
  }

  Future<void> mergeQuickOfferFiles() async {
    setBusy(true);
    List<Map<String, dynamic>> files = [];

    for (var element
        in (formValues[InviteCustomerFormKeys.offerFiles] as List<File>)) {
      files.add({
        "fileName": getNameFromFile(element.path),
        "fileContent": base64Encode(await element.readAsBytes()),
      });
    }

    final mergeResponse =
        await _offerService.fileMerger(payload: {"files": files});

    if ((mergeResponse.success ?? false)) {
      _formValues[InviteCustomerFormKeys.uploadOfferMergeFileName] =
          mergeResponse.data['fileName'];
      _formValues[InviteCustomerFormKeys.uploadOfferMergeFileBase64] =
          mergeResponse.data['file'];

      final bytes = base64Decode(mergeResponse.data['file']);
      final dir = await getTemporaryDirectory();
      final file = File('${dir.path}/${mergeResponse.data['fileName']}.pdf');
      await file.writeAsBytes(bytes);

      _formValues[InviteCustomerFormKeys.uploadOfferMergeFile] = file;
    }

    setBusy(false);

    currentStep += 1;

    notifyListeners();
  }

  Future<void> updateIndustryPrice(
      {required int industryIndex,
      required int subIndustryIndex,
      required double price,
      bool? calculateFee}) async {
    if ((calculateFee ?? false)) {
      setBusy(true);
    }

    _formValues[InviteCustomerFormKeys.industryDescriptions][industryIndex]
        ['subIndustry'][subIndustryIndex]['price'] = price;

    double totalPriceWoVat =
        (formValues[InviteCustomerFormKeys.industryDescriptions]
                as List<Map<String, dynamic>>)
            .expand(
                (value) => (value['subIndustry'] as List<Map<String, dynamic>>))
            .fold(0, (sum, subIndustry) => sum + subIndustry['price']);

    _formValues[InviteCustomerFormKeys.industryTotalPriceWoVat] =
        totalPriceWoVat;
    _formValues[InviteCustomerFormKeys.industryTotalVat] =
        totalPriceWoVat * .25;
    _formValues[InviteCustomerFormKeys.industryTotalPriceVat] =
        totalPriceWoVat * 1.25;

    if ((calculateFee ?? false)) {
      final response =
          await _payproffService.calculateFee(amount: totalPriceWoVat);

      _formValues[InviteCustomerFormKeys.industryTotalPriceVatFee] =
          response.data['totalVatWithFee'];

      setBusy(false);
    }

    notifyListeners();
  }

  Future<bool?> createQuickOffer(
      {required bool isAffiliate,
      required String signature,
      required String companyIndustry}) async {
    setBusy(true);

    Contact contact = formValues[InviteCustomerFormKeys.customerContact];
    PartnerProjectsModel project =
        formValues[InviteCustomerFormKeys.customerProject];

    List<Map<String, dynamic>> offerPayloadSaveListPriceList = [];

    DateTime startDate =
        DateTime.parse(formValues[InviteCustomerFormKeys.projectStartDate]);
    DateTime endDate = startDate.copyWith(month: startDate.month + 1);

    AllWizardText industry =
        industryList.firstWhere((value) => value.industry == companyIndustry);

    offerPayloadSaveListPriceList.add({
      "project_id": project.id!,
      "sub_project_id": 0,
      "contact_id": contact.id!,
      "calculation_id": 0,
      "industry_id": industry.producttypeid,
      "total": formValues[InviteCustomerFormKeys.industryTotalPriceWoVat],
      "note": '',
      "reviews_id": [],
      "job_industry_id": industry.industry,
      "signature": '',
      "STARTDATE": startDate.toString(),
      "ENDDATE": endDate.toString(),
      "expiry": 30,
      "weeks": 30,
      "days": 30,
      "offer_id": 0,
      "parent_offer_id": 0,
      "job_description": {
        'submitted': [
          '${formValues[InviteCustomerFormKeys.offerComments]} | ${formValues[InviteCustomerFormKeys.industryTotalPriceWoVat]}'
        ]
      }
    });

    final offerPayload = {
      "contactId": contact.id!,
      "createdFrom": "partner-app",
      "projectId": project.id!,
      "jobIndustry": industry.producttypeid,
      "type": 'fast-track',
      "fastTrackOfferType": 'quick',
      "offers": [
        {
          "title": "Tilbud på projektnr. ${project.id!} til ${project.name!}",
          "description": project.description,
          "subtotalPrice":
              formValues[InviteCustomerFormKeys.industryTotalPriceWoVat],
          "vat": formValues[InviteCustomerFormKeys.industryTotalVat],
          "totalPrice":
              formValues[InviteCustomerFormKeys.industryTotalPriceVat],
          "expiry": 30,
          "weeks": 30,
          "days": 30,
          "STARTDATE": startDate.toString(),
          "ENDDATE": endDate.toString(),
          "type": 'fast-track',
          "fileName": 'merged.pdf',
          "file":
              base64Encode(formValues[InviteCustomerFormKeys.previewOfferPdf]),
          "tasks": [
            {
              "title": '',
              "description": '',
              "unit": '',
              "value": '',
              "quantity": ''
            }
          ]
        }
      ],
      "saveListPriceV2": offerPayloadSaveListPriceList,
      "isAffiliate": isAffiliate ? 1 : 0,
      "expiry": 30,
      "start_date": startDate.toString()
    };

    final offerResponse =
        await _bookingWizardService.createOffer(params: offerPayload);

    if (!(offerResponse.success ?? false)) {
      throw offerResponse.message ?? '';
    }

    final offerId =
        ((offerResponse.data as Map<String, dynamic>)['offer'] as List)
            .first['ID'];
    //

    //SEND INVITE
    final inviteResponse = await _bookingWizardService.sendInvite(
        contactId: contact.id!,
        projectId: project.id!,
        offerId: offerId,
        isAffiliate: isAffiliate);

    if (!(inviteResponse.success ?? false)) {
      throw inviteResponse.message ?? '';
    }
    //

    setBusy(false);

    return true;
  }

  Future<void> previewOfferPayload(
      {required bool isAffiliate, required String companyIndustry}) async {
    setBusy(true);

    // String projectTitle = '';
    String projectDescription = '';

    List<String> subIndustrys = [];
    List<String> productTypeIds = [];
    final List<String> jobList = [];
    List<dynamic> finalJobList = [];

    for (var element
        in industryList.where((value) => value.industry == companyIndustry)) {
      if (element.subindustry != null && element.subindustry != '0') {
        subIndustrys.add(element.subindustry ?? '');
      }

      productTypeIds.add(element.producttypeid ?? '');
    }

    jobList.add(
        '$companyIndustry:${subIndustrys.join('-')}:${productTypeIds.join('-')}');
    finalJobList = jobList.map((e) => [e]).toList();

    Map<String, dynamic> previewOfferPayload = {
      "offerDate": formValues[InviteCustomerFormKeys.projectStartDate],
      "offerDescription": formValues[InviteCustomerFormKeys.offerComments],
      "offerNote": formValues[InviteCustomerFormKeys.offerConditions] ?? '',
      "offers": formValues[InviteCustomerFormKeys.quickOfferPriceDetails],
      "offerPrice": formValues[InviteCustomerFormKeys.industryTotalPriceWoVat],
      "offerPriceVat": formValues[InviteCustomerFormKeys.industryTotalVat],
      "offerPriceWithVat":
          formValues[InviteCustomerFormKeys.industryTotalPriceVat],
    };

    if (formValues[InviteCustomerFormKeys.customerProject] == null) {
      //CREATE CONTACT
      final contactResponse = await _bookingWizardService.createContact(
          name:
              '${formValues[InviteCustomerFormKeys.customerFirstName]} ${formValues[InviteCustomerFormKeys.customerLastName]}',
          email: formValues[InviteCustomerFormKeys.customerEmail],
          mobile: formValues[InviteCustomerFormKeys.customerMobile],
          isAffiliate: isAffiliate);

      if (!(contactResponse.success ?? false)) {
        throw contactResponse.message ?? '';
      }

      Contact contact = Contact.fromJson(contactResponse.data['contact']);

      _formValues[InviteCustomerFormKeys.customerContact] = contact;

      //

      //CREATE ADDRESS
      Address address =
          formValues[InviteCustomerFormKeys.customerAddress] as Address;

      final addressResponse = await _bookingWizardService.createAddress(
          contactId: contact.id!,
          addressTitle: address.address ?? '',
          addressId: address.addressId ?? '',
          adgangsaddressId: address.adgangsAddressId ?? '',
          isAffiliate: isAffiliate);

      if (!(addressResponse.success ?? false)) {
        throw addressResponse.message ?? '';
      }

      address = Address.fromJson(addressResponse.data['address']);
      //

      //GET TITLE & SUMMARY
      // final titleResponse = await _bookingWizardService.getListTitleAi(
      //     industryId: productTypeIds.join(','), contactId: contact.id!);

      // if (!(titleResponse.success ?? false)) {
      //   throw titleResponse.message ?? '';
      // }

      // projectTitle =
      //     (titleResponse.data as Map<String, dynamic>)['title'] as String;

      final descriptionResponse = await _bookingWizardService.getListSummaryAi(
          industryId: productTypeIds.join(','), contactId: contact.id!);

      if (!(descriptionResponse.success ?? false)) {
        throw descriptionResponse.message ?? '';
      }

      projectDescription =
          (descriptionResponse.data as Map<String, dynamic>)['summary'];
      //

      //CREATE PROJECT
      final Map<String, dynamic> createProjectForm = {
        "contactId": contact.id!,
        "addressId": address.id,
        "name": formValues[InviteCustomerFormKeys.offerComments],
        "description": projectDescription,
        "source": "app-partner",
        "status": "new_project",
        "contractType": 'Hovedentreprise',
        "industry": subIndustrys.first,
        "industryNew": [...subIndustrys.map((value) => int.parse(value))],
        "industries": productTypeIds.join(','),
        "enterpriseIndustry": productTypeIds,
        "enterpriseIndustryNew": productTypeIds,
        "jobsList": finalJobList,
        "budget": formValues[InviteCustomerFormKeys.industryTotalPriceWoVat],
        "isAffiliate": isAffiliate ? 1 : 0
      };

      if (applic.env != 'production') {
        createProjectForm.putIfAbsent('testProject', () => true);
      }

      final projectResponse =
          await _bookingWizardService.createProject(form: createProjectForm);

      if (!(projectResponse.success ?? false)) {
        throw projectResponse.message ?? '';
      }

      final createdProject = PartnerProjectsModel.fromJson(
          projectResponse.data['project'] as Map<String, dynamic>);

      _formValues[InviteCustomerFormKeys.customerProject] = createdProject;

      previewOfferPayload['projectId'] = createdProject.id!;
      //

      //RESERVE PROJECT
      final reserveResponse = await _bookingWizardService.reserveProject(
          contactId: contact.id!,
          projectId: createdProject.id!,
          isAffiliate: isAffiliate);

      if (!(reserveResponse.success ?? false)) {
        throw reserveResponse.message ?? '';
      }
      //
    } else {
      final createdProject = formValues[InviteCustomerFormKeys.customerProject]
          as PartnerProjectsModel;

      previewOfferPayload['projectId'] = createdProject.id!;
    }

    //PREVIEW OFFER
    final previewOfferResponse =
        await _offerService.previewOfferWizard(payload: previewOfferPayload);

    _formValues[InviteCustomerFormKeys.previewOfferPdf] = previewOfferResponse;
    //

    //SAVE OFFER CONDITION
    if ((formValues[InviteCustomerFormKeys.saveOfferConditions] ?? false) &&
        (formValues[InviteCustomerFormKeys.offerConditions] as String)
            .isNotEmpty) {
      await _settingService.updateSupplier(payload: {
        'UF_CRM_COMPANY_OFFER_NOTE':
            formValues[InviteCustomerFormKeys.offerConditions]
      });
    }
    //

    currentStep += 1;

    setBusy(false);

    notifyListeners();
  }

  Future<void> createContact({required bool isAffiliate}) async {
    setBusy(true);
    final contactResponse = await _bookingWizardService.createContact(
        name:
            '${formValues[InviteCustomerFormKeys.customerFirstName]} ${formValues[InviteCustomerFormKeys.customerLastName]}',
        email: formValues[InviteCustomerFormKeys.customerEmail],
        mobile: formValues[InviteCustomerFormKeys.customerMobile],
        isAffiliate: isAffiliate);

    if (!(contactResponse.success ?? false)) {
      throw contactResponse.message ?? '';
    }

    Contact contact = Contact.fromJson(contactResponse.data['contact']);

    _formValues[InviteCustomerFormKeys.customerContact] = contact;

    notifyListeners();
    setBusy(false);
  }

  Future<bool> submitUploadOffer(
      {required bool isAffiliate, required String signature}) async {
    setBusy(true);

    String projectTitle = formValues[InviteCustomerFormKeys.projectName];
    String projectDescription = '';

    //CREATE CONTACT
    final contactResponse = await _bookingWizardService.createContact(
        name:
            '${formValues[InviteCustomerFormKeys.customerFirstName]} ${formValues[InviteCustomerFormKeys.customerLastName]}',
        email: formValues[InviteCustomerFormKeys.customerEmail],
        mobile: formValues[InviteCustomerFormKeys.customerMobile],
        isAffiliate: isAffiliate);

    if (!(contactResponse.success ?? false)) {
      throw contactResponse.message ?? '';
    }

    Contact contact = Contact.fromJson(contactResponse.data['contact']);
    //

    //CREATE ADDRESS
    Address address =
        formValues[InviteCustomerFormKeys.customerAddress] as Address;
    final addressResponse = await _bookingWizardService.createAddress(
        contactId: contact.id!,
        addressTitle: address.address ?? '',
        addressId: address.addressId ?? '',
        adgangsaddressId: address.adgangsAddressId ?? '',
        isAffiliate: isAffiliate);

    if (!(addressResponse.success ?? false)) {
      throw addressResponse.message ?? '';
    }

    address = Address.fromJson(addressResponse.data['address']);
    //

    //GET SUMMARY
    final descriptionResponse = await _bookingWizardService.getListSummaryAi(
        industryId:
            (formValues[InviteCustomerFormKeys.industryDescriptions] as List)
                .map((value) => value['productTypeId'])
                .join(','),
        contactId: contact.id!);

    if (!(descriptionResponse.success ?? false)) {
      throw descriptionResponse.message ?? '';
    }

    projectDescription =
        (descriptionResponse.data as Map<String, dynamic>)['summary'];
    //

    //CREATE PROJECT
    List<int> taskTypeIds = [];
    List<String> addEnterpriseSubProjectNew = [];
    final Map<String, dynamic> descriptionForm = {};
    final List<String> jobList = [];
    List<dynamic> finalJobList = [];

    for (Map<String, dynamic> productIndustry
        in (formValues[InviteCustomerFormKeys.industryDescriptions]
            as List<Map<String, dynamic>>)) {
      addEnterpriseSubProjectNew.add(
          '${productIndustry['productTypeId']}: ${(productIndustry['subIndustry'] as List).map((e) => e['subIndustryId']).join('-')}');

      for (Map<String, dynamic> element
          in productIndustry['subIndustry'] as List) {
        descriptionForm.putIfAbsent(
            'text_${productIndustry['productTypeId']}_${element['subIndustryId']}',
            () => element['descriptions']);

        final job =
            '${element['subIndustryId']}::${(formValues[InviteCustomerFormKeys.industryDescriptions] as List).expand((item) => item['subIndustry'] as List).where((a) => a['subIndustryId'] as String == element['subIndustryId'] as String).toList().map((b) => b['productTypeId']).join('-')}';

        if (!jobList.contains(job)) jobList.add(job);

        final task = industryFullList.firstWhere(
          (task) =>
              task.industryId == int.parse(element['subIndustryId'] as String),
          orElse: () => Data.defaults(),
        );

        taskTypeIds.addAll([
          ...(task.subIndustries ?? []).map((value) => value.subcategoryId ?? 0)
        ]);
      }
    }

    finalJobList = jobList.map((e) => [e]).toList();

    final Map<String, dynamic> createProjectForm = {
      "contactId": contact.id!,
      "addressId": address.id,
      "name": projectTitle,
      "description": projectDescription,
      "source": "app-partner",
      "status": "new_project",
      "contractType": 'Hovedentreprise',
      "industry": ((formValues[InviteCustomerFormKeys.industryDescriptions]
                  as List<Map<String, dynamic>>)
              .first['subIndustry'] as List<Map<String, dynamic>>)
          .first['subIndustryName'],
      "industryNew": [
        ...(formValues[InviteCustomerFormKeys.industryDescriptions]
                as List<Map<String, dynamic>>)
            .expand((value) => value['subIndustry'])
            .map((value) => int.parse(value['subIndustryId']))
      ],
      "subcategoryNew": [
        ...(formValues[InviteCustomerFormKeys.industryDescriptions]
                as List<Map<String, dynamic>>)
            .expand((value) => value['subIndustry'])
            .map((value) => int.parse(value['subIndustryId']))
      ],
      "taskTypeNew": taskTypeIds,
      "enterpriseIndustry": [
        ...(formValues[InviteCustomerFormKeys.industryDescriptions] as List)
            .map((value) => value['productTypeId'])
      ],
      "enterpriseIndustryNew": addEnterpriseSubProjectNew,
      "jobsList": finalJobList,
      "budget": formValues[InviteCustomerFormKeys.industryTotalPriceWoVat],
      "isAffiliate": isAffiliate ? 1 : 0
    };

    createProjectForm.addAll(descriptionForm);

    if (applic.env != 'production') {
      createProjectForm.putIfAbsent('testProject', () => true);
    }

    final projectResponse =
        await _bookingWizardService.createProject(form: createProjectForm);

    if (!(projectResponse.success ?? false)) {
      throw projectResponse.message ?? '';
    }

    final createdProject = PartnerProjectsModel.fromJson(
        projectResponse.data['project'] as Map<String, dynamic>);
    //

    // //UPDATE CONTACT INFO
    // if (contact.name != 'John Doe') {
    //   final contactResponse = await _bookingWizardService.updateContact(
    //       contactId: contact.id!,
    //       name: formValues[InviteCustomerFormKeys.customerName],
    //       mobile: int.parse(formValues[InviteCustomerFormKeys.customerMobile]));

    //   if (!(contactResponse.success ?? false)) {
    //     throw contactResponse.message ?? '';
    //   }
    // }
    // //

    //RESERVE PROJECT
    final reserveResponse = await _bookingWizardService.reserveProject(
        contactId: contact.id!,
        projectId: createdProject.id!,
        isAffiliate: isAffiliate);

    if (!(reserveResponse.success ?? false)) {
      throw reserveResponse.message ?? '';
    }
    //

    // //MERGE FILES
    // List<Map<String, dynamic>> files = [];

    // for (var element
    //     in (formValues[InviteCustomerFormKeys.offerFiles] as List<File>)) {
    //   files.add({
    //     "fileName": getNameFromFile(element.path),
    //     "fileContent": base64Encode(await element.readAsBytes()),
    //   });
    // }

    // final mergeResponse =
    //     await _offerService.fileMerger(payload: {"files": files});

    // if ((mergeResponse.success ?? false)) {
    //   mergeFileName = mergeResponse.data['fileName'];
    //   mergeFile = mergeResponse.data['file'];
    // }
    // //

    //CREATE OFFER
    List<Map<String, dynamic>> offerPayloadSaveListPriceList = [];

    for (final industry
        in formValues[InviteCustomerFormKeys.industryDescriptions]
            as List<Map<String, dynamic>>) {
      for (final subIndustry
          in industry['subIndustry'] as List<Map<String, dynamic>>) {
        offerPayloadSaveListPriceList.add({
          "project_id": createdProject.id!,
          "sub_project_id": 0,
          "contact_id": contact.id!,
          "calculation_id": 0,
          "industry_id": subIndustry['productTypeId'],
          "total": subIndustry['price'],
          "note": '',
          "reviews_id": [],
          "job_industry_id": subIndustry['subIndustryId'],
          "signature": signature,
          "STARTDATE": '${formValues[InviteCustomerFormKeys.projectStartDate]}',
          "ENDDATE":
              '${(formValues[InviteCustomerFormKeys.projectStartDate] as DateTime).copyWith(month: (formValues[InviteCustomerFormKeys.projectStartDate] as DateTime).month + 1)}',
          "expiry": formValues[InviteCustomerFormKeys.offerValidity],
          "weeks": formValues[InviteCustomerFormKeys.projectEstimate],
          "days": formValues[InviteCustomerFormKeys.projectEstimate],
          "offer_id": 0,
          "parent_offer_id": 0,
          "job_description": {'submitted': subIndustry['descriptions']}
        });
      }
    }

    final offerPayload = {
      "contactId": contact.id!,
      "createdFrom": "partner-app",
      "projectId": createdProject.id!,
      "jobIndustry": formValues[InviteCustomerFormKeys.industryDescriptions][0]
          ['productTypeId'],
      "type": 'fast-track',
      "fastTrackOfferType": 'upload',
      "files": [],
      "offers": [
        {
          "title":
              "Tilbud på projektnr. ${createdProject.id!} til ${createdProject.name!}",
          "description": createdProject.description,
          "subtotalPrice":
              formValues[InviteCustomerFormKeys.industryTotalPriceWoVat],
          "vat": formValues[InviteCustomerFormKeys.industryTotalVat],
          "totalPrice":
              formValues[InviteCustomerFormKeys.industryTotalPriceVat],
          "expiry": formValues[InviteCustomerFormKeys.offerValidity],
          "weeks": formValues[InviteCustomerFormKeys.projectEstimate],
          "days": formValues[InviteCustomerFormKeys.projectEstimate],
          "STARTDATE": '${formValues[InviteCustomerFormKeys.projectStartDate]}',
          "ENDDATE":
              '${(formValues[InviteCustomerFormKeys.projectStartDate] as DateTime).copyWith(month: (formValues[InviteCustomerFormKeys.projectStartDate] as DateTime).month + 1)}',
          "type": 'fast-track',
          "fileName":
              formValues[InviteCustomerFormKeys.uploadOfferMergeFileName],
          "file": formValues[InviteCustomerFormKeys.uploadOfferMergeFileBase64],
          "tasks": [
            {
              "title": '',
              "description": '',
              "unit": '',
              "value": '',
              "quantity": ''
            }
          ]
        }
      ],
      "saveListPriceV2": offerPayloadSaveListPriceList,
      "isAffiliate": isAffiliate ? 1 : 0
    };

    if ((formValues[InviteCustomerFormKeys.industryDescriptions] as List)
        .expand((a) => a['subIndustry'] as List)
        .where((e) => e['subIndustryId'] == '1062')
        .isNotEmpty) {
      offerPayload.putIfAbsent('jobIndustry', () => '1062');
    }

    final offerResponse =
        await _bookingWizardService.createOffer(params: offerPayload);

    if (!(offerResponse.success ?? false)) {
      throw offerResponse.message ?? '';
    }

    final offerId =
        ((offerResponse.data as Map<String, dynamic>)['offer'] as List)
            .first['ID'];
    //

    //SEND INVITE
    final inviteResponse = await _bookingWizardService.sendInvite(
        contactId: contact.id!,
        projectId: createdProject.id!,
        offerId: offerId,
        isAffiliate: isAffiliate);

    if (!(inviteResponse.success ?? false)) {
      throw inviteResponse.message ?? '';
    }
    //

    setBusy(false);

    return true;
  }

  Future<void> getListPrice() async {
    setBusy(true);

    List<Map<String, dynamic>> industryDescriptions = [];

    for (var element in (formValues[InviteCustomerFormKeys.selectedIndustry]
        as List<AllWizardText>)) {
      List<Map<String, dynamic>> subIndustryList = [];

      final listPrice = await _bookingWizardService.getListPriceAi(
          industryId: element.producttypeid!, contactId: 0);

      if (listPrice.data is Map && (listPrice.data as Map).isNotEmpty) {
        Map<String, dynamic> subIndustryMap = (listPrice.data
            as Map)[element.producttypeid!] as Map<String, dynamic>;
        for (var newMap in subIndustryMap.entries) {
          String subIndustryName = industryFullList
                  .firstWhere(
                    (value) => (value.branchId ?? 0).toString() == newMap.key,
                    orElse: () => Data.defaults(),
                  )
                  .branche ??
              '';
          List<String> tempDescription = [];

          for (var element in newMap.value) {
            if (element is String &&
                (element).trim() != '-' &&
                (element).isNotEmpty) {
              tempDescription.add(element);
            }
          }

          subIndustryList.add({
            'productTypeId': element.producttypeid,
            'productTypeName': element.producttypeDa,
            'subIndustryId': newMap.key,
            'subIndustryName': subIndustryName,
            'descriptions': tempDescription,
            'originalDescriptions': tempDescription,
            'price': 0.0,
            'viewDescriptions': false
          });
        }

        industryDescriptions.add({
          'productTypeId': element.producttypeid,
          'productTypeName': element.producttypeDa,
          'subIndustry': subIndustryList
        });
      }
    }

    int subIndustryLength =
        industryDescriptions.expand((value) => value['subIndustry']).length;

    for (var industryMap in industryDescriptions) {
      for (var subIndustryMap
          in (industryMap['subIndustry'] as List<Map<String, dynamic>>)) {
        subIndustryMap['price'] =
            formValues[InviteCustomerFormKeys.tempIndustryTotalPriceWoVat] /
                subIndustryLength;
      }
    }

    _formValues[InviteCustomerFormKeys.industryDescriptions] =
        industryDescriptions;

    _formValues[InviteCustomerFormKeys.industryTotalPriceWoVat] =
        formValues[InviteCustomerFormKeys.tempIndustryTotalPriceWoVat];
    _formValues[InviteCustomerFormKeys.industryTotalVat] =
        (formValues[InviteCustomerFormKeys.tempIndustryTotalPriceWoVat] * .25);
    _formValues[InviteCustomerFormKeys.industryTotalPriceVat] =
        (formValues[InviteCustomerFormKeys.tempIndustryTotalPriceWoVat] * 1.25);

    _currentStep = 5;

    setBusy(false);
  }

  Future<void> editProjectTItleOrDescription(
      {String? title, String? description}) async {
    setBusy(true);

    Map<String, dynamic> params = {};

    if (title != null) {
      params['name'] = title;
    } else if (description != null) {
      params['description'] = description;
    }

    PartnerProjectsModel customerProject =
        (formValues[InviteCustomerFormKeys.customerProject]
            as PartnerProjectsModel);

    final projectResponse = await _projectService.updateProject(
        projectId: customerProject.id!, params: params);

    if (!(projectResponse.success ?? false)) {
      throw projectResponse.message ?? '';
    }

    customerProject =
        PartnerProjectsModel.fromJson(projectResponse.data['parentProject']);

    _formValues[InviteCustomerFormKeys.customerProject] = customerProject;

    if (title != null) {
      _formValues[InviteCustomerFormKeys.customerProjectEditTitle] = false;
    } else if (description != null) {
      _formValues[InviteCustomerFormKeys.customerProjectEditDescription] =
          false;
    }

    setBusy(false);
  }

  Future<bool> submitOffer(
      {required bool isAffiliate, required String signature}) async {
    setBusy(true);

    final customerContact =
        (formValues[InviteCustomerFormKeys.customerContact] as Contact);
    PartnerProjectsModel customerProject =
        (formValues[InviteCustomerFormKeys.customerProject]
            as PartnerProjectsModel);

    // //UPDATE CONTACT INFO
    // if (customerContact.name != 'John Doe') {
    //   final contactResponse = await _bookingWizardService.updateContact(
    //       contactId: customerContact.id!,
    //       name: formValues[InviteCustomerFormKeys.customerName],
    //       mobile: int.parse(formValues[InviteCustomerFormKeys.customerMobile]));

    //   if (!(contactResponse.success ?? false)) {
    //     throw contactResponse.message ?? '';
    //   }
    // }
    // //

    //RESERVE PROJECT
    final reserveResponse = await _bookingWizardService.reserveProject(
        contactId: customerContact.id!,
        projectId: customerProject.id!,
        isAffiliate: isAffiliate);

    if (!(reserveResponse.success ?? false)) {
      throw reserveResponse.message ?? '';
    }
    //

    //CREATE OFFER
    List<Map<String, dynamic>> offerPayloadSaveListPriceList = [];

    for (final industry
        in formValues[InviteCustomerFormKeys.industryDescriptions]
            as List<Map<String, dynamic>>) {
      for (final subIndustry
          in industry['subIndustry'] as List<Map<String, dynamic>>) {
        offerPayloadSaveListPriceList.add({
          "project_id": customerProject.id!,
          "sub_project_id": 0,
          "contact_id": customerContact.id!,
          "calculation_id": 0,
          "industry_id": subIndustry['productTypeId'],
          "total": subIndustry['price'],
          "note": '',
          "reviews_id": [],
          "job_industry_id": subIndustry['subIndustryId'],
          "signature": signature,
          "STARTDATE": '${formValues[InviteCustomerFormKeys.projectStartDate]}',
          "ENDDATE":
              '${(formValues[InviteCustomerFormKeys.projectStartDate] as DateTime).copyWith(month: (formValues[InviteCustomerFormKeys.projectStartDate] as DateTime).month + 1)}',
          "expiry": formValues[InviteCustomerFormKeys.offerValidity],
          "weeks": formValues[InviteCustomerFormKeys.projectEstimate],
          "days": formValues[InviteCustomerFormKeys.projectEstimate],
          "offer_id": 0,
          "parent_offer_id": 0,
          "job_description": {'submitted': subIndustry['descriptions']}
        });
      }
    }

    final offerPayload = {
      "contactId": customerContact.id!,
      "createdFrom": "partner-app",
      "projectId": customerProject.id!,
      "jobIndustry": formValues[InviteCustomerFormKeys.industryDescriptions][0]
          ['productTypeId'],
      "type": 'fast-track',
      "files": [],
      "fastTrackOfferType": 'extended',
      "offers": [
        {
          "title":
              "Tilbud på projektnr. ${customerProject.id!} til ${customerProject.name!}",
          "description": customerProject.description,
          "subtotalPrice":
              formValues[InviteCustomerFormKeys.industryTotalPriceWoVat],
          "vat": formValues[InviteCustomerFormKeys.industryTotalVat],
          "totalPrice":
              formValues[InviteCustomerFormKeys.industryTotalPriceVat],
          "expiry": formValues[InviteCustomerFormKeys.offerValidity],
          "weeks": formValues[InviteCustomerFormKeys.projectEstimate],
          "days": formValues[InviteCustomerFormKeys.projectEstimate],
          "STARTDATE": '${formValues[InviteCustomerFormKeys.projectStartDate]}',
          "ENDDATE":
              '${(formValues[InviteCustomerFormKeys.projectStartDate] as DateTime).copyWith(month: (formValues[InviteCustomerFormKeys.projectStartDate] as DateTime).month + 1)}',
          "type": 'fast-track',
          "tasks": [
            {
              "title": '',
              "description": '',
              "unit": '',
              "value": '',
              "quantity": ''
            }
          ]
        }
      ],
      "saveListPriceV2": offerPayloadSaveListPriceList,
      "isAffiliate": isAffiliate ? 1 : 0
    };

    if ((formValues[InviteCustomerFormKeys.industryDescriptions] as List)
        .expand((a) => a['subIndustry'] as List)
        .where((e) => e['subIndustryId'] == '1062')
        .isNotEmpty) {
      offerPayload.putIfAbsent('jobIndustry', () => '1062');
    }

    final offerResponse =
        await _bookingWizardService.createOffer(params: offerPayload);

    if (!(offerResponse.success ?? false)) {
      throw offerResponse.message ?? '';
    }

    final offerId =
        ((offerResponse.data as Map<String, dynamic>)['offer'] as List)
            .first['ID'];
    //

    //SEND INVITE
    final inviteResponse = await _bookingWizardService.sendInvite(
        contactId: customerContact.id!,
        projectId: customerProject.id!,
        offerId: offerId,
        isAffiliate: isAffiliate);

    if (!(inviteResponse.success ?? false)) {
      throw inviteResponse.message ?? '';
    }
    //

    //SAVE OFFER CONDITION
    if ((formValues[InviteCustomerFormKeys.saveOfferConditions] ?? false) &&
        (formValues[InviteCustomerFormKeys.offerConditions] as String)
            .isNotEmpty) {
      await _settingService.updateSupplier(payload: {
        'UF_CRM_COMPANY_OFFER_NOTE':
            formValues[InviteCustomerFormKeys.offerConditions]
      });
    }
    //

    setBusy(false);

    return true;
  }

  Future<void> createProject({required bool isAffiliate}) async {
    setBusy(true);

    String projectTitle = '';
    String projectDescription = '';

    //CREATE CONTACT
    final contactResponse = await _bookingWizardService.createContact(
        name:
            '${formValues[InviteCustomerFormKeys.customerFirstName]} ${formValues[InviteCustomerFormKeys.customerLastName]}',
        email: formValues[InviteCustomerFormKeys.customerEmail],
        mobile: formValues[InviteCustomerFormKeys.customerMobile],
        isAffiliate: isAffiliate);

    if (!(contactResponse.success ?? false)) {
      throw contactResponse.message ?? '';
    }

    Contact contact = Contact.fromJson(contactResponse.data['contact']);
    //

    //CREATE ADDRESS
    Address address =
        formValues[InviteCustomerFormKeys.customerAddress] as Address;

    final addressResponse = await _bookingWizardService.createAddress(
        contactId: contact.id!,
        addressTitle: address.address ?? '',
        addressId: address.addressId ?? '',
        adgangsaddressId: address.adgangsAddressId ?? '',
        isAffiliate: isAffiliate);

    if (!(addressResponse.success ?? false)) {
      throw addressResponse.message ?? '';
    }

    address = Address.fromJson(addressResponse.data['address']);
    //

    //GET TITLE & SUMMARY
    final titleResponse = await _bookingWizardService.getListTitleAi(
        industryId:
            (formValues[InviteCustomerFormKeys.industryDescriptions] as List)
                .map((value) => value['productTypeId'])
                .join(','),
        contactId: contact.id!);

    if (!(titleResponse.success ?? false)) {
      throw titleResponse.message ?? '';
    }

    projectTitle =
        (titleResponse.data as Map<String, dynamic>)['title'] as String;

    final descriptionResponse = await _bookingWizardService.getListSummaryAi(
        industryId:
            (formValues[InviteCustomerFormKeys.industryDescriptions] as List)
                .map((value) => value['productTypeId'])
                .join(','),
        contactId: contact.id!);

    if (!(descriptionResponse.success ?? false)) {
      throw descriptionResponse.message ?? '';
    }

    projectDescription =
        (descriptionResponse.data as Map<String, dynamic>)['summary'];

    //

    //CREATE PROJECT
    List<int> taskTypeIds = [];
    List<String> addEnterpriseSubProjectNew = [];
    final Map<String, dynamic> descriptionForm = {};
    final List<String> jobList = [];
    List<dynamic> finalJobList = [];

    for (Map<String, dynamic> productIndustry
        in (formValues[InviteCustomerFormKeys.industryDescriptions]
            as List<Map<String, dynamic>>)) {
      addEnterpriseSubProjectNew.add(
          '${productIndustry['productTypeId']}: ${(productIndustry['subIndustry'] as List).map((e) => e['subIndustryId']).join('-')}');

      for (Map<String, dynamic> element
          in productIndustry['subIndustry'] as List) {
        descriptionForm.putIfAbsent(
            'text_${productIndustry['productTypeId']}_${element['subIndustryId']}',
            () => element['descriptions']);

        final job =
            '${element['subIndustryId']}::${(formValues[InviteCustomerFormKeys.industryDescriptions] as List).expand((item) => item['subIndustry'] as List).where((a) => a['subIndustryId'] as String == element['subIndustryId'] as String).toList().map((b) => b['productTypeId']).join('-')}';

        if (!jobList.contains(job)) jobList.add(job);

        final task = industryFullList.firstWhere(
          (task) =>
              task.industryId == int.parse(element['subIndustryId'] as String),
          orElse: () => Data.defaults(),
        );

        taskTypeIds.addAll([
          ...(task.subIndustries ?? []).map((value) => value.subcategoryId ?? 0)
        ]);
      }
    }

    finalJobList = jobList.map((e) => [e]).toList();

    final Map<String, dynamic> createProjectForm = {
      "contactId": contact.id!,
      "addressId": address.id,
      "name": projectTitle,
      "description": projectDescription,
      "source": "app-partner",
      "status": "new_project",
      "contractType": 'Hovedentreprise',
      "industry": ((formValues[InviteCustomerFormKeys.industryDescriptions]
                  as List<Map<String, dynamic>>)
              .first['subIndustry'] as List<Map<String, dynamic>>)
          .first['subIndustryName'],
      "industryNew": [
        ...(formValues[InviteCustomerFormKeys.industryDescriptions]
                as List<Map<String, dynamic>>)
            .expand((value) => value['subIndustry'])
            .map((value) => int.parse(value['subIndustryId']))
      ],
      "subcategoryNew": [
        ...(formValues[InviteCustomerFormKeys.industryDescriptions]
                as List<Map<String, dynamic>>)
            .expand((value) => value['subIndustry'])
            .map((value) => int.parse(value['subIndustryId']))
      ],
      "taskTypeNew": taskTypeIds,
      "enterpriseIndustry": [
        ...(formValues[InviteCustomerFormKeys.industryDescriptions] as List)
            .map((value) => value['productTypeId'])
      ],
      "enterpriseIndustryNew": addEnterpriseSubProjectNew,
      "jobsList": finalJobList,
      "budget": formValues[InviteCustomerFormKeys.industryTotalPriceWoVat],
      "isAffiliate": isAffiliate ? 1 : 0
    };

    createProjectForm.addAll(descriptionForm);

    if (applic.env != 'production') {
      createProjectForm.putIfAbsent('testProject', () => true);
    }

    final projectResponse =
        await _bookingWizardService.createProject(form: createProjectForm);

    if (!(projectResponse.success ?? false)) {
      throw projectResponse.message ?? '';
    }

    final createdProject = PartnerProjectsModel.fromJson(
        projectResponse.data['project'] as Map<String, dynamic>);
    //

    _formValues[InviteCustomerFormKeys.customerContact] = contact;
    _formValues[InviteCustomerFormKeys.customerProject] = createdProject;

    notifyListeners();

    setBusy(false);
  }

  Future<Map<String, dynamic>?> getAiCalculation(
      {required String productType, required String industry}) async {
    setBusy(true);

    String subTasks = _industryFullList
        .firstWhere((value) => (value.branchId ?? 0).toString() == industry)
        .subIndustries!
        .map((value) => (value.subcategoryId ?? 0))
        .join(',');

    final response = await _bookingWizardService.getAiCalculationPriceV3(
        addressId:
            (formValues[InviteCustomerFormKeys.customerAddress] as Address)
                .addressId!,
        adgangsAddressId:
            (formValues[InviteCustomerFormKeys.customerAddress] as Address)
                .adgangsAddressId!,
        contactId: 0,
        industry: industry,
        productType: productType,
        subTasks: subTasks);

    setBusy(false);

    return response?.data;
  }

  Future<void> submitBookerWizardAnswers() async {
    setBusy(true);

    List<Map<String, dynamic>> industryDescriptions = [];

    Map<String, dynamic> addProductPayload = {
      'industries': (formValues[InviteCustomerFormKeys.selectedIndustry]
              as List<AllWizardText>)
          .map((value) => value.producttypeid)
          .join(','),
      'calculation_id': 0,
      'project_id': 0,
      'update_text': 0,
      'userId': 0,
      'contactId': 0,
    };

    addProductPayload.addAll(formValues[InviteCustomerFormKeys.productAnswers]);

    final response =
        await _bookingWizardService.addWizardProduct(form: addProductPayload);

    if (!(response.success ?? false)) {
      throw ApiException(message: response.message ?? '');
    }

    for (var element in (formValues[InviteCustomerFormKeys.selectedIndustry]
        as List<AllWizardText>)) {
      List<Map<String, dynamic>> subIndustryList = [];

      final listPrice = await _bookingWizardService.getListPriceAi(
          industryId: element.producttypeid!, contactId: 0);

      if (listPrice.data is Map && (listPrice.data as Map).isNotEmpty) {
        Map<String, dynamic> subIndustryMap = (listPrice.data
            as Map)[element.producttypeid!] as Map<String, dynamic>;
        for (var newMap in subIndustryMap.entries) {
          String subIndustryName = industryFullList
                  .firstWhere(
                    (value) => (value.branchId ?? 0).toString() == newMap.key,
                    orElse: () => Data.defaults(),
                  )
                  .branche ??
              '';
          List<String> tempDescription = [];

          for (var element in newMap.value) {
            if (element is String &&
                (element).trim() != '-' &&
                (element).isNotEmpty) {
              tempDescription.add(element);
            }
          }

          subIndustryList.add({
            'productTypeId': element.producttypeid,
            'productTypeName': element.producttypeDa,
            'subIndustryId': newMap.key,
            'subIndustryName': subIndustryName,
            'descriptions': tempDescription,
            'originalDescriptions': tempDescription,
            'price': 0.0,
            'viewDescriptions': false
          });
        }

        industryDescriptions.add({
          'productTypeId': element.producttypeid,
          'productTypeName': element.producttypeDa,
          'subIndustry': subIndustryList
        });
      }
    }

    _formValues[InviteCustomerFormKeys.industryDescriptions] =
        industryDescriptions;
    _currentStep = 3;

    setBusy(false);
  }

  // Future<void> getBookerWizardQuestions() async {
  //   setBusy(true);

  //   final List<ProductWizardModelV2> tempProductWizardList =
  //       []; // FIELDS THAT WILL SHOW THE QUESTIONAIRE

  //   final response = await _bookingWizardService.getWizardProductsNewVersion(
  //       labels: (formValues[InviteCustomerFormKeys.selectedIndustry]
  //               as List<AllWizardText>)
  //           .map((value) => value.producttypeid)
  //           .join(','));

  //   if (!(response.success ?? false)) throw response.message ?? "";

  //   final Map<String, dynamic> productsResponse =
  //       response.data as Map<String, dynamic>;

  //   productsResponse.forEach((key, value) {
  //     //KEY IS PRODUCTTYPEID

  //     if (value['wizard_product'] is List) {
  //       final List tempWizardProduct = value['wizard_product'] as List;
  //       List newWizardProductList = [];

  //       if (value['wizard_lead'] is List) {
  //         newWizardProductList = [
  //           ...(value['wizard_lead'] as List).map((index) {
  //             return tempWizardProduct[index];
  //           })
  //         ];
  //       } else if (value['wizard_lead'] is Map) {
  //         newWizardProductList = [
  //           ...(value['wizard_lead'] as Map).entries.map((index) {
  //             return tempWizardProduct[int.parse('${index.value}')];
  //           })
  //         ];
  //       }

  //       for (final p0 in newWizardProductList) {
  //         if (p0 is List) {
  //           final wizardsList = ProductWizardFieldV2.fromCollection(p0);

  //           log("message wizardsList ${jsonEncode([
  //                 ...wizardsList.where((value) => value.parentproductid == 0)
  //               ])}");
  //         }
  //       }
  //     } else if (value['wizard_product'] is Map<String, dynamic>) {}
  //   });

  //   setBusy(false);

  //   notifyListeners();
  // }

  Future<void> getBookerWizardQuestions() async {
    setBusy(true);

    final List<ProductWizardModelV2> tempProductWizardList =
        []; // FIELDS THAT WILL SHOW THE QUESTIONAIRE

    final response = await _bookingWizardService.getWizardProductsNewVersion(
        labels: (formValues[InviteCustomerFormKeys.selectedIndustry]
                as List<AllWizardText>)
            .map((value) => value.producttypeid)
            .join(','));

    if (!(response.success ?? false)) throw response.message ?? "";

    final Map<String, dynamic> productsResponse =
        response.data as Map<String, dynamic>;

    productsResponse.forEach((key, value) {
      List<ProductWizardFieldV2> productValues = <ProductWizardFieldV2>[];
      final productItems = (value as Map<String, dynamic>)['product'];
      productValues = ProductWizardFieldV2.fromCollection(productItems);

      if (value['wizard_product'] is List) {
        final List tempWizardProduct = value['wizard_product'] as List;
        List newWizardProductList = [];

        if (value['wizard_lead'] is List) {
          newWizardProductList = [
            ...(value['wizard_lead'] as List).map((index) {
              return tempWizardProduct[index];
            })
          ];
        } else if (value['wizard_lead'] is Map) {
          newWizardProductList = [
            ...(value['wizard_lead'] as Map).entries.map((index) {
              return tempWizardProduct[int.parse('${index.value}')];
            })
          ];
        }

        for (final p0 in newWizardProductList) {
          final List<ProductWizardFieldV2> productFields = [];

          if (p0 is List) {
            final wizardsList = ProductWizardFieldV2.fromCollection(p0);

            final List<ProductWizardFieldV2> loopingList = [];

            loopingList.addAll([
              ...wizardsList.where((element) => element.parentproductid == 0)
            ]);

            while (loopingList.isNotEmpty) {
              final currentField = loopingList.first;

              if (currentField.productField == 2) {
                productFields.add(currentField);
                loopingList.remove(currentField);
              } else {
                final values = productValues.where((a) =>
                    a.parentproductid.toString() == currentField.id &&
                    a.productField == 1);

                final List<ProductWizardFieldV2> children = [];

                for (final fieldValue in values) {
                  final subChildren = wizardsList.where(
                      (a) => a.parentproductid.toString() == fieldValue.id);

                  for (final sub in subChildren) {
                    if (sub.productField == 2) {
                      children.add(sub);
                    } else {
                      ProductWizardFieldV2 tempSub = sub;

                      final subValues = productValues.where((a) =>
                          a.parentproductid.toString() == sub.id &&
                          a.productField == 1);

                      tempSub.values = [...subValues];

                      children.add(tempSub);
                    }
                  }
                }

                ProductWizardFieldV2 tempCurrentField = currentField;

                tempCurrentField.values = [...values];
                tempCurrentField.productField =
                    values.isEmpty ? 1 : currentField.productField;
                tempCurrentField.children = [...children];

                productFields.add(tempCurrentField);
                loopingList.remove(currentField);
              }
              loopingList.addAll([
                ...productValues.where((element) =>
                    element.parentproductid.toString() == currentField.id &&
                    element.productField != 1)
              ]);
            }
          }
          productFields.sort((a, b) => (a.id ?? '').compareTo((b.id ?? '')));

          tempProductWizardList.add(ProductWizardModelV2(
              productId: key,
              producttype: industryList
                  .firstWhere(
                    (element) => element.producttypeid == key,
                    orElse: () => AllWizardText.defaults(),
                  )
                  .producttypeDa,
              productFields: [
                ...productFields
                    .where((element) => element.parentproductid == 0),
                ...productFields
                    .where((element) => element.parentproductid != 0)
              ]));
        }
      } else if (value['wizard_product'] is Map<String, dynamic>) {
        final Map<String, dynamic> tempWizardProduct =
            value['wizard_product'] as Map<String, dynamic>;

        Map<String, dynamic> finalWizardProduct = {};

        List<String> wizardLeadsIndex = [];

        if (value['wizard_lead'] is List) {
          wizardLeadsIndex = [
            ...(value['wizard_lead'] as List).map((e) => '$e')
          ];
        } else if (value['wizard_lead'] is Map) {
          wizardLeadsIndex = [
            ...(value['wizard_lead'] as Map).entries.map((e) => '${e.value}')
          ];
        }

        finalWizardProduct = {
          for (String index in wizardLeadsIndex) index: tempWizardProduct[index]
        };

        final List<String> sortedKeys = finalWizardProduct.keys.toList()
          ..sort();

        final Map<String, dynamic> sortedWizardProduct = Map.fromEntries(
            sortedKeys.map((key) => MapEntry(key, finalWizardProduct[key])));

        sortedWizardProduct.forEach((key0, value0) {
          final List<ProductWizardFieldV2> productFields = [];

          final wizardsList = ProductWizardFieldV2.fromCollection(value0);

          final List<ProductWizardFieldV2> loopingList = [];

          loopingList.addAll([
            ...wizardsList.where((element) => element.parentproductid == 0)
          ]);

          while (loopingList.isNotEmpty) {
            final currentField = loopingList.first;

            if (currentField.productField == 2) {
              productFields.add(currentField);
              loopingList.remove(currentField);
            } else {
              final values = productValues.where((a) =>
                  a.parentproductid.toString() == currentField.id &&
                  a.productField == 1);

              final List<ProductWizardFieldV2> children = [];

              for (final fieldValue in values) {
                final subChildren = wizardsList.where(
                    (a) => a.parentproductid.toString() == fieldValue.id);

                for (final sub in subChildren) {
                  if (sub.productField == 2) {
                    children.add(sub);
                  } else {
                    final subValues = productValues.where((a) =>
                        a.parentproductid.toString() == sub.id &&
                        a.productField == 1);

                    ProductWizardFieldV2 tempSub = sub;

                    tempSub.values = [...subValues];

                    children.add(tempSub);
                  }
                }
              }

              ProductWizardFieldV2 tempCurrentField = currentField;
              tempCurrentField.values = [...values];
              tempCurrentField.productField =
                  values.isEmpty ? 1 : currentField.productField;
              tempCurrentField.children = [...children];

              productFields.add(tempCurrentField);
              loopingList.remove(currentField);
            }
            loopingList.addAll([
              ...productValues.where((element) =>
                  element.parentproductid.toString() == currentField.id &&
                  element.productField != 1)
            ]);
          }

          productFields.sort((a, b) => (a.id ?? '').compareTo((b.id ?? '')));

          tempProductWizardList.add(
            ProductWizardModelV2(
              productId: key,
              producttype: industryList
                  .firstWhere(
                    (element) => element.producttypeid == key,
                    orElse: () => AllWizardText.defaults(),
                  )
                  .producttypeDa,
              productFields: [
                ...productFields
                    .where((element) => element.parentproductid == 0),
                ...productFields
                    .where((element) => element.parentproductid != 0)
              ],
            ),
          );
        });
      }
    });

    _productQuestionnaires = tempProductWizardList
        .where((element) => element.productFields!.isNotEmpty)
        .toList();

    _currentStep = 2.1;
    _currentProductQuestionnaireStep = 0;

    setBusy(false);

    notifyListeners();
  }

  Future<List<Address>> searchAddress({required String query}) async {
    try {
      final request = await Dio().get(
          'https://dawa.aws.dk/autocomplete/?q=$query&startfra=adresse&fuzzy=true');

      if ((request.statusCode ?? 400) == 200) {
        List<Address> address = [];

        for (var element in AutoCompleteAddress.fromCollection(request.data)) {
          address.add(Address(
              addressId: element.data!.id,
              adgangsAddressId: element.data!.adgangsadresseid!,
              address: element.tekst!,
              zip: element.data!.postnr!,
              latitude: '${element.data!.y!}',
              longitude: '${element.data!.x!}'));
        }

        return address;
      } else {
        return [];
      }
    } catch (e) {
      return [];
    }
  }

  Future<void> init() async {
    setBusy(true);
    _currentStep = 0;
    _formValues = {};

    _formValues[InviteCustomerFormKeys.offerValidity] = '30';

    setBusy(false);

    notifyListeners();
  }

  Future<List<AllWizardText>> filterIndustryList(
      {required String query}) async {
    return industryList
        .where((value) => (value.producttypeDa ?? '')
            .toLowerCase()
            .trim()
            .contains(query.toLowerCase().trim()))
        .toList();
  }

  void setIndustry(
      {required List<AllWizardText> list1, required List<Data> list2}) {
    _industryList = [...list1];
    _industryFullList = [...list2];

    notifyListeners();
  }
}
