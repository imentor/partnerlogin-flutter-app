import 'package:Haandvaerker.dk/model/get_types_response_model.dart';
import 'package:Haandvaerker.dk/model/mester_til_mester/mester_to_mester_job_model.dart';
import 'package:Haandvaerker.dk/services/mester-mester/mester_mester_service.dart';
import 'package:Haandvaerker.dk/services/settings/settings_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MesterPartnerProfileViewModel extends BaseViewModel {
  MesterPartnerProfileViewModel({required this.context});
  final BuildContext context;

  //NEW
  int _lastPage = 1;
  int _currentPage = 1;
  int _type = 0;

  List<MesterToMesterJobItem> _jobItems = <MesterToMesterJobItem>[];
  List<Data> _industryData = <Data>[];

  int get lastPage => _lastPage;
  int get currentPage => _currentPage;
  int get type => _type;
  List<MesterToMesterJobItem> get jobItems => _jobItems;
  List<Data> get industryData => _industryData;

  bool _fromNewsFeed = false;
  bool get fromNewsFeed => _fromNewsFeed;

  set lastPage(int page) {
    _lastPage = page;
    notifyListeners();
  }

  set currentPage(int page) {
    _currentPage = page;
    notifyListeners();
  }

  set type(int type) {
    _type = type;
    notifyListeners();
  }

  set jobItems(List<MesterToMesterJobItem> items) {
    _jobItems = items;
    notifyListeners();
  }

  set industryData(List<Data> data) {
    _industryData = data;
    notifyListeners();
  }

  set fromNewsFeed(bool news) {
    _fromNewsFeed = news;
    notifyListeners();
  }

  Future<bool> reserveMesterJob(
      {required String partnerId, required int mesterId}) async {
    //

    final service = context.read<MesterMesterService>();

    final reserveResponse = await service.reserveMesterJob(
        partnerId: partnerId, mesterId: mesterId);

    return reserveResponse;

    //
  }

  Future<void> getMesterJobs({bool getAll = false}) async {
    final service = context.read<MesterMesterService>();

    final response = await service.getMesterJobs(
        page: _currentPage, type: _type, getAll: getAll);

    lastPage = response.lastPage!;
    jobItems = response.items!;
  }

  Future<void> submitLaborOrTask(
      {required Map<String, dynamic> payload}) async {
    final service = context.read<MesterMesterService>();

    final response = await service.submitLaborOrTask(payload: payload);

    final items = <MesterToMesterJobItem>[..._jobItems];
    items.add(response);

    jobItems = items.reversed.toList();
  }

  Future<void> getIndustryData() async {
    final service = context.read<SettingsService>();

    setBusy(true);

    final response = await service.getTaskTypesFromCRM();

    if (response != null) {
      industryData = Data.fromCollection(response.data);
    }

    setBusy(false);
  }

  Future<bool> deleteLaborOrTask({required int mesterId}) async {
    final service = context.read<MesterMesterService>();

    final response = await service.deleteLaborOrTask(mesterId: mesterId);

    if (response) {
      final items = <MesterToMesterJobItem>[..._jobItems];
      items.removeWhere((element) => element.id == mesterId);

      jobItems = items;
    }

    return response;
  }
}
