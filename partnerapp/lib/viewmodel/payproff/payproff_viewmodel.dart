import 'dart:convert';
import 'dart:developer';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/partner/bitrix_company_model.dart';
import 'package:Haandvaerker.dk/model/payproff/payproff_account_exists_model.dart';
import 'package:Haandvaerker.dk/model/payproff/payproff_wallet_model.dart';
import 'package:Haandvaerker.dk/model/payproff/post_payproff_account_model.dart';
import 'package:Haandvaerker.dk/services/main/main_service.dart';
import 'package:Haandvaerker.dk/services/payproff/payproff_service.dart';
import 'package:Haandvaerker.dk/ui/components/loading_animation_dialog.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class PayproffViewModel extends BaseViewModel {
  PayproffViewModel({required this.context});
  final BuildContext context;

  BitrixCompanyModel? _company;
  BitrixCompanyModel? get company => _company;

  Map<String, dynamic>? _country = <String, dynamic>{};
  Map<String, dynamic>? get country => _country;

  Map<String, dynamic>? _language = <String, dynamic>{};
  Map<String, dynamic>? get language => _language;

  String _selectedCountry = 'DK';
  String get selectedCountry => _selectedCountry;

  String? _selectedLanguage = 'da';
  String? get selectedLanguage => _selectedLanguage;

  String? _selectedCurrency = 'DKK';
  String? get selectedCurrency => _selectedCurrency;
  //IBAN FORM VARIABLES
  bool _isShowBanner = false;
  bool get isShowBanner => _isShowBanner;

  TextEditingController? _ibanController;
  TextEditingController? get ibanController => _ibanController;

  bool _isPreIbanForm = true;
  bool get isPreIbanForm => _isPreIbanForm;

  bool _isIbanValid = true;
  bool get isIbanValid => _isIbanValid;

  bool _isIbanFieldEmpty = true;
  bool get isIbanFieldEmpty => _isIbanFieldEmpty;

  bool _isPayproffVerified = false;
  bool get isPayproffVerified => _isPayproffVerified;

  GlobalKey<FormState> ibanFormKey = GlobalKey<FormState>();

  DateTime? _statusUpdated;
  DateTime? get statusUpdated => _statusUpdated;

  PayproffAccountExists? _payproff;
  PayproffAccountExists? get payproff => _payproff;

  PayproffWallet? _payproffWallet;
  PayproffWallet? get payproffWallet => _payproffWallet;

  String _ibanText = '';
  String get ibanText => _ibanText;

  String _payproffUrl = '';
  String get payproffUrl => _payproffUrl;

  void reset() {
    _company = null;
    _country = {};
    _language = {};
    _selectedCountry = 'DK';
    _selectedLanguage = 'da';
    _selectedCurrency = 'DKK';
    _isShowBanner = false;
    _ibanController = null;
    _isPreIbanForm = true;
    _isIbanValid = true;
    _isIbanFieldEmpty = true;
    _statusUpdated = null;
    _payproff = null;
    _payproffWallet = null;
    _ibanText = '';
    _payproffUrl = '';

    notifyListeners();
  }

  set ibanText(String value) {
    _ibanText = value;
    notifyListeners();
  }

  set isIbanValid(bool valid) {
    _isIbanValid = valid;
    notifyListeners();
  }

  set isBanFieldEmpty(bool empty) {
    _isIbanFieldEmpty = empty;
    notifyListeners();
  }

  set isShowBanner(bool show) {
    _isShowBanner = show;
    notifyListeners();
  }

  set ibanController(TextEditingController? controller) {
    _ibanController = controller;
    notifyListeners();
  }

  set isPreIbanForm(bool pre) {
    _isPreIbanForm = pre;
    notifyListeners();
  }

  set selectedCurrency(String? value) {
    _selectedCurrency = value;
    notifyListeners();
  }

  set selectedLanguage(String? value) {
    _selectedLanguage = value;
    notifyListeners();
  }

  set selectedCountry(String value) {
    _selectedCountry = value;
    notifyListeners();
  }

  set statusUpdated(DateTime? date) {
    _statusUpdated = date;
    notifyListeners();
  }

  set payproff(PayproffAccountExists? payproff) {
    _payproff = payproff;
    notifyListeners();
  }

  set payproffWallet(PayproffWallet? wallet) {
    _payproffWallet = wallet;
    notifyListeners();
  }

  Future<void> getSupplierInfo() async {
    var service = context.read<MainService>();
    var response = await service.getSupplierInfo();

    if (response != null) {
      if (response.success!) {
        _company = BitrixCompanyModel.fromJson(response.data);
      }
    }

    notifyListeners();
  }

  Future<MinboligApiResponse> postPayproffAccount(
      {required PostPayproffAccountModel account}) async {
    try {
      var service = context.read<PayproffService>();

      // final responseValidate =
      //     await service.payproffValidateIban(iban: account.iban!);

      // if (!responseValidate.success!) {
      //   throw responseValidate.message!;
      // }

      final Map<String, dynamic> form = {
        "email": account.email,
        "phoneNumber": account.phoneNumber,
        "phoneNumberAreaCode": '+45',
        "firstName": account.firstName,
        "middleName": account.middleName,
        "lastName": account.lastName,
        "address": account.address,
        "zipCode": account.zipCode,
        "city": account.city,
        "country": "DK",
        "isPep": account.isPep,
        "birthDate": account.birthDate,
        "displayLanguage": account.displayLanguage,
        "companyName": account.companyName,
        "cvr": account.cvr,
        "currency": account.currency,
        "iban": account.iban
      };

      var response = await service.postPayproffAccount(form: form);

      await checkPayproffAcount();

      return response;
    } catch (e) {
      return MinboligApiResponse();
    }
  }

  Future<void> initView() async {
    String data = await rootBundle.loadString("assets/documents/language.json");
    final jsonResultLanguage = jsonDecode(data);

    String dataCountry =
        await rootBundle.loadString("assets/documents/country.json");

    try {
      _language!.clear();
      final jsonLanguage = jsonResultLanguage as Map<String, dynamic>?;
      /*for (var item in jsonLanguage.values) {
        _language.add(item.toString());
      }*/
      _language = jsonLanguage;

      notifyListeners();
    } catch (e) {
      log("$e");
    }

    try {
      _country!.clear();
      final result = json.decode(dataCountry);
      /*for (var item in _result.values) {
        _country.add(item.toString());
      }*/
      _country = result;
      //_country.();
      notifyListeners();
    } catch (e) {
      log("$e");
    }
  }

  Future<void> checkPayproffAcount() async {
    setBusy(true);

    final service = context.read<PayproffService>();

    final response = await service.checkPayproffAccount();

    if (response != null &&
        response.message != 'Payproff user does not exist') {
      if (response.data == null) throw (response.message ?? '');

      final updatedPayproff = PayproffAccountExists.fromJson(
          Map<String, dynamic>.from(response.data));

      statusUpdated = DateTime.tryParse(updatedPayproff.statusUpdated);

      if (updatedPayproff.exists) {
        final responseWallet = await service.payproffGetWallet();

        if (responseWallet.data != null) {
          final updatedPayproffWallet = PayproffWallet.fromJson(
              Map<String, dynamic>.from(responseWallet.data));

          _payproffWallet = updatedPayproffWallet;
        } else {
          _payproffWallet = null;
        }
      }

      _payproff = updatedPayproff;
    } else {
      _payproff = null;
    }

    notifyListeners();

    setBusy(false);
  }

  Future<void> updatePayproffStatus() async {
    var service = context.read<PayproffService>();
    await service.updatePayproffStatus();
    checkPayproffAcount();
  }

  Future<void> payproffVerify() async {
    final service = context.read<PayproffService>();
    final response = await service.payproffVerify();

    if (response != null) {
      if (response.data['PAYPROFF_URL'] != null) {
        _payproffUrl = response.data['PAYPROFF_URL'];
      }
      if (response.data['STATUS'] != null || response.data['STATUS'] != '') {
        _isPayproffVerified = response.data['STATUS'] == 'verified';
      }
    }
  }

  Future<bool> updateWallet({required String iban}) async {
    var service = context.read<PayproffService>();
    final response = await service.payproffUpdateWallet(
        iban: iban, walletId: _payproffWallet!.walletId);

    final responseWallet = await service.payproffGetWallet();
    if (responseWallet.data['UPDATED_AT'] != null) {
      payproffWallet = PayproffWallet.fromJson(
          Map<String, dynamic>.from(responseWallet.data));
    }

    return response.success!;
  }

  Future<bool> validateIban({required String iban}) async {
    final service = context.read<PayproffService>();

    final response = await service.payproffValidateIban(iban: iban);

    return response.data['valid'];
  }

  Future<bool> payproffUpdate({required String iban}) async {
    final service = context.read<PayproffService>();

    if (_payproffWallet != null && _payproffWallet!.walletId.isEmpty) {
      final walletResponse = await service.payproffGetWallet();
      if (walletResponse.data != null) {
        _payproffWallet = PayproffWallet.fromJson(
            Map<String, dynamic>.from(walletResponse.data));
      }
    }

    final response = await service.payproffUpdateWallet(
        iban: iban, walletId: _payproffWallet!.walletId);

    return (response.data['success'] ?? false);
  }

  Future<void> submitIban({required BuildContext context}) async {
    ///
    final service = context.read<PayproffService>();

    if (ibanFormKey.currentState!.validate()) {
      ///
      _ibanText = _ibanController?.text.trim() ?? '';

      final key = GlobalKey<NavigatorState>();
      showLoadingDialog(context, key);

      final ibanNumber = _selectedCountry +
          _ibanText.replaceAll('DK', '').replaceAll(' ', '').trim();
      log('ibanNumber: $ibanNumber');
      final isIbanValid = await validateIban(iban: ibanNumber);

      if (isIbanValid) {
        if (_payproffWallet != null && _payproffWallet!.walletId.isEmpty) {
          final walletResponse = await service.payproffGetWallet();
          if (walletResponse.data != null) {
            _payproffWallet = PayproffWallet.fromJson(
                Map<String, dynamic>.from(walletResponse.data));
          }
        }

        final updateResponse = await service.payproffUpdateWallet(
            iban: ibanNumber, walletId: _payproffWallet!.walletId);

        final response = await service.checkPayproffAccount();

        if (response != null &&
            response.message != 'Payproff user does not exist') {
          if (response.data == null) throw response.message!;

          final updatedPayproff = PayproffAccountExists.fromJson(
              Map<String, dynamic>.from(response.data));

          statusUpdated = DateTime.tryParse(updatedPayproff.statusUpdated);

          if (updatedPayproff.exists) {
            final responseWallet = await service.payproffGetWallet();

            if (responseWallet.data != null) {
              final updatedPayproffWallet = PayproffWallet.fromJson(
                  Map<String, dynamic>.from(responseWallet.data));

              _payproffWallet = updatedPayproffWallet;
            } else {
              _payproffWallet = null;
            }
          }

          _payproff = updatedPayproff;
          _payproffUrl = updatedPayproff.payproffUrl;

          if (context.mounted) {
            Navigator.of(context).pop();
            Navigator.of(key.currentContext!, rootNavigator: true).pop();
            if ((updateResponse.success ?? false) &&
                context.mounted &&
                updatedPayproff.payproffUrl.isNotEmpty) {
              await launchUrl(Uri.parse(_payproff?.payproffUrl ?? ''))
                  .then((value) => checkPayproffAcount());
            } else {
              await updatePayproffStatus().whenComplete(() async {
                await launchUrl(Uri.parse(_payproffUrl))
                    .then((value) => checkPayproffAcount());
              });
            }
          }
        } else {
          _payproff = null;
        }

        notifyListeners();
      } else {
        if (context.mounted) {
          Navigator.of(key.currentContext!, rootNavigator: true).pop();
          await showOkAlertDialog(
            context: context,
            title: tr('error'),
            message: tr('iban_number_invalid'),
            okLabel: tr('ok'),
          );
        }
      }

      ///
    }

    ///
  }
}
