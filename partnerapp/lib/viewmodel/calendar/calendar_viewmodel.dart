import 'package:Haandvaerker.dk/model/calendar/calendar.dart';
import 'package:Haandvaerker.dk/model/messages/contacts_response_model.dart';
import 'package:Haandvaerker.dk/services/calendar/calendar_service.dart';
import 'package:Haandvaerker.dk/services/messages/message_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CalendarViewmodel extends BaseViewModel {
  CalendarViewmodel({required this.context});

  final BuildContext context;

  List<Calendar> _calendarsEvents = [];
  List<Calendar> get calendarsEvents => _calendarsEvents;

  List<CalendarTypes> _calendarTypes = [];
  List<CalendarTypes> get calendarTypes => _calendarTypes;

  List<ContactsResponseModel> _contacts = [];
  List<ContactsResponseModel> get contacts => _contacts;

  void reset() {
    _calendarsEvents = [];
    _calendarTypes = [];
    _contacts = [];

    notifyListeners();
  }

  Future<void> deleteEvent(
      {required int calendarId, required int projectId}) async {
    final service = context.read<CalendarService>();

    setBusy(true);

    await service.deleteEvent(calendarId: calendarId);

    final response =
        await service.getProjectCalendarEvents(projectId: projectId);

    if (response != null) {
      _calendarsEvents = [...Calendar.fromCollection(response.data)];
    }

    setBusy(false);

    notifyListeners();
  }

  Future<void> addUpdateEvent(
      {int? calendarId,
      required Map<String, dynamic> payload,
      required int projectId}) async {
    final service = context.read<CalendarService>();

    setBusy(true);

    await service.addUpdateEvent(payload: payload, calendarId: calendarId);

    final response =
        await service.getProjectCalendarEvents(projectId: projectId);

    if (response != null) {
      _calendarsEvents = [...Calendar.fromCollection(response.data)];
    }

    setBusy(false);

    notifyListeners();
  }

  Future<void> getProjectCalendarEvents({required int projectId}) async {
    final service = context.read<CalendarService>();
    final messageService = context.read<MessageService>();

    setBusy(true);

    if (calendarTypes.isEmpty) {
      final typesResponse = await service.getCalendarTypes();

      if (typesResponse != null) {
        _calendarTypes = [
          ...CalendarTypes.fromCollection(typesResponse.data['calendar_types'])
        ];
      }
    }

    if (contacts.isEmpty) {
      final contactResponse = await messageService.getContacts();

      if (contactResponse != null) {
        _contacts = contactResponse;
      }
    }

    final response =
        await service.getProjectCalendarEvents(projectId: projectId);

    if (response != null) {
      _calendarsEvents = [...Calendar.fromCollection(response.data)];
    }

    setBusy(false);

    notifyListeners();
  }
}
