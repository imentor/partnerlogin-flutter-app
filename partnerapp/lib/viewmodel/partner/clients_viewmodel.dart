import 'dart:io';
import 'dart:math' as math;

import 'package:Haandvaerker.dk/model/affiliate_job/affiliate_job_model.dart';
import 'package:Haandvaerker.dk/model/customer_minbolig/old_jobs_model.dart';
import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/invitations/subcontractor_invitation_model.dart';
import 'package:Haandvaerker.dk/model/offer_model.dart';
import 'package:Haandvaerker.dk/model/projects/contacts_with_contracts_model.dart';
import 'package:Haandvaerker.dk/model/tilbud_model.dart';
import 'package:Haandvaerker.dk/services/booking_wizard/booking_wizard_service.dart';
import 'package:Haandvaerker.dk/services/client/client_service.dart';
import 'package:Haandvaerker.dk/services/file/file_service.dart';
import 'package:Haandvaerker.dk/utils/file_utils.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/create_offer_viewmodel.dart';
import 'package:Haandvaerker.dk/viewmodel/tender/tender_folder_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

enum OfferState {
  newOffer,
  editOffer,
}

class ClientsViewModel extends BaseViewModel {
  ClientsViewModel({required this.context});
  final BuildContext context;

  List<GlobalKey<FormState>> formKey = [];

  final Map<String, String> smsEmailOptions = {
    'SMS og e-mail': '1',
    'SMS': '2',
    'Email': '3',
    'Ingen': '0',
    //'Ring': '4',
  };

  final Map<String, double> offerTypeList = {
    'Quantity': 0,
    'Amount': 1,
    'Hour': 2,
  };

  int? _customerYearFilter;
  int? get customerYearFilter => _customerYearFilter;

  int _offerYearFilter = DateTime.now().year;
  int get offerYearFilter => _offerYearFilter;

  List<OfferFormModel> _offerList = [];
  List<OfferFormModel> get offerList => _offerList;

  List<OfferModel> _offers = [];
  List<OfferModel> get offer => _offers;

  List<PartnerJobModel> _partnerJobs = [];
  List<PartnerJobModel> get partnerJobs => _partnerJobs;

  List<OldPartnerJobsModel> _oldPartnerJobs = <OldPartnerJobsModel>[];
  List<OldPartnerJobsModel> get oldPartnerJobs => _oldPartnerJobs;

  List<OldPartnerJobsModel> _finalOldPartnerJobs = <OldPartnerJobsModel>[];
  List<OldPartnerJobsModel> get finalOldPartnerJobs => _finalOldPartnerJobs;

  List<SubcontractorInvitation> _subContractorInvitations = [];
  List<SubcontractorInvitation> get subContractorInvitations =>
      _subContractorInvitations;

  List<ContactWithContractModel> _contacts = [];
  List<ContactWithContractModel> get contacts => _contacts;

  FocusNode? _searchFocusNode;
  FocusNode? get searchFocusNode => _searchFocusNode;

  TextEditingController? _searchController;
  TextEditingController? get searchController => _searchController;
  // List<CustomerCommentModel> _customerCommentModel = [];
  // List<CustomerCommentModel> get customerCommentModel => _customerCommentModel;

  Info? _quotationForm = Info();
  Info? get quotationForm => _quotationForm;

  OfferState _currentState = OfferState.newOffer;
  OfferState get currentState => _currentState;

  String _editOfferId = '';
  String? get editOfferId => _editOfferId;

  int _currentPage = 1;
  int get currentPage => _currentPage;

  int _lastPage = 1;
  int get lastPage => _lastPage;

  int _firstPage = 1;
  int get firstPage => _firstPage;

  bool _getYourCustomerData = false;
  bool get getYourCustomerData => _getYourCustomerData;

  bool _fromNewsFeed = false;
  bool get fromNewsFeed => _fromNewsFeed;

  int? _currentSort;
  int? get currentSort => _currentSort;

  final Map<String, int> sortByItems = {
    'sort_by_meeting_date': 1,
    'sort_by_status': 2,
    'sort_by_sent_quote_date': 3,
    'sort_by_follow_up_date': 4,
  };

  Map<dynamic, String> _filterByItems = {};
  Map<dynamic, String> get filterByItems => _filterByItems;

  List<bool> _filterValues = [];
  List<bool> get filterValues => _filterValues;

  List<bool> _finalFilterValues = [];
  List<bool> get finalFilterValues => _finalFilterValues;

  Map<String, dynamic> _currentQueryParams = {};
  Map<String, dynamic> get currentQueryParams => _currentQueryParams;

  String _statusIds = '';
  String get statusIds => _statusIds;

  String _jobType = '';
  String get jobType => _jobType;

  String _sort = '';
  String get sort => _sort;

  bool? _withOffers;
  bool? get withOffers => _withOffers;

  int _size = 14;
  int get size => _size;

  int _jobIdFromNewsFeed = 0;
  int get jobIdFromNewsFeed => _jobIdFromNewsFeed;

  List<OldTilbudStatus> oldtilbudStatus = [
    OldTilbudStatus(value: 0, text: 'new_task', filterValue: 1),
    OldTilbudStatus(value: 1, text: 'in_dialogue', filterValue: 2),
    OldTilbudStatus(value: 2, text: 'offers_have_been_sent', filterValue: 3),
    OldTilbudStatus(value: 3, text: 'won_the_task', filterValue: 5),
    OldTilbudStatus(value: 5, text: 'did_not_win_the_task', filterValue: 0),
  ];

  List<String> _currentJobFilters = [];
  List<String> get currentJobFilters => _currentJobFilters;

  PartnerJobModel? _selectedJobFromNewsFeed;
  PartnerJobModel? get selectedJobFromNewsFeed => _selectedJobFromNewsFeed;

  int _calledCustomerJobId = 0;
  int get calledCustomerJobId => _calledCustomerJobId;

  bool _initialClientsLoad = true;
  bool get initialClientsLoad => _initialClientsLoad;

  List<AffiliateJob> _affiliateJobs = [];
  List<AffiliateJob> get affiliateJobs => _affiliateJobs;

  Future<bool?> uploadJobFiles(
      {required List<File> files, required PartnerJobModel job}) async {
    _getYourCustomerData = true;

    final fileService = context.read<FilesService>();

    final formData = FormData();

    for (final file in files) {
      formData.files.addAll([
        MapEntry(
            "files[]",
            await MultipartFile.fromFile(file.path,
                filename: getNameFromFile(file.path))),
      ]);
    }

    formData.fields.addAll([
      MapEntry("parentId", '${job.projectPhotoDocumentationFolderId}'),
      const MapEntry("category", 'Documentation'),
      MapEntry("projectId", '${job.projectId}'),
      MapEntry("jobId", '${job.id}'),
      const MapEntry("description", 'Upload dokumentation til Händærker.dk'),
    ]);

    final response = await fileService.uploadMultipleFiles(payload: formData);

    await getPartnerJobsV2();

    _getYourCustomerData = false;

    notifyListeners();

    return response.success ?? false;
  }

  Future<bool?> acceptAffiliateJob({
    required String affiliateProjectId,
  }) async {
    final clientService = context.read<ClientService>();

    final response = await clientService.acceptAffiliateJob(
        affiliateProjectId: affiliateProjectId);

    return response?.success ?? false;
  }

  Future<bool?> addAffiliateJobNotes({required FormData data}) async {
    final clientService = context.read<ClientService>();

    final response = await clientService.addAffiliateJobNotes(payload: data);

    return response?.success ?? false;
  }

  Future<void> getAffiliateJobs() async {
    final clientService = context.read<ClientService>();

    final response = await clientService.getAffiliateJobs();

    if (response != null) {
      _affiliateJobs = AffiliateJob.fromCollection(response.data['items']);
    }

    notifyListeners();
  }

  Future<bool> resendInvite(
      {required PartnerJobModel job, required bool isAffiliate}) async {
    final bookingService = context.read<BookingWizardService>();

    final response = await bookingService.sendInvite(
        contactId: job.customerId!,
        projectId: job.projectId,
        offerId: job.offerId,
        isAffiliate: isAffiliate);

    return response.success ?? false;
  }

  void reset() {
    _customerYearFilter = null;
    _offerYearFilter = DateTime.now().year;
    _offerList = [];
    _offers = [];
    _partnerJobs = [];
    _oldPartnerJobs = [];
    _finalOldPartnerJobs = [];
    _contacts = [];
    _searchFocusNode = null;
    _searchController = null;
    _quotationForm = null;
    _currentState = OfferState.newOffer;
    _editOfferId = '';
    _currentPage = 1;
    _lastPage = 1;
    _firstPage = 1;
    _getYourCustomerData = false;
    _fromNewsFeed = false;
    _currentSort = null;
    _filterByItems = {};
    _filterValues = [];
    _finalFilterValues = [];
    _currentQueryParams = {};
    _statusIds = '';
    _jobType = '';
    _sort = '';
    _withOffers = null;
    _size = 14;
    _jobIdFromNewsFeed = 0;
    _currentJobFilters = [];
    _selectedJobFromNewsFeed = null;
    _calledCustomerJobId = 0;
    _initialClientsLoad = true;

    notifyListeners();
  }

  set initialClientsLoad(bool value) {
    _initialClientsLoad = value;
    notifyListeners();
  }

  set calledCustomerJobId(int jobId) {
    _calledCustomerJobId = jobId;
    notifyListeners();
  }

  set quotationForm(Info? value) {
    _quotationForm = value;
    notifyListeners();
  }

  set offerList(List<OfferFormModel> value) {
    _offerList = value;
    notifyListeners();
  }

  set currentPage(int current) {
    _currentPage = current;
    notifyListeners();
  }

  set lastPage(int last) {
    _lastPage = last;
    notifyListeners();
  }

  set firstPage(int page) {
    _firstPage = page;
    notifyListeners();
  }

  set partnerJobs(List<PartnerJobModel> jobs) {
    _partnerJobs = jobs;
    notifyListeners();
  }

  set fromNewsFeed(bool newsfeed) {
    _fromNewsFeed = newsfeed;
    notifyListeners();
  }

  set statusIds(String ids) {
    _statusIds = ids;
    notifyListeners();
  }

  set jobType(String type) {
    _jobType = type;
    notifyListeners();
  }

  set sort(String sort) {
    _sort = sort;
    notifyListeners();
  }

  set withOffers(bool? offers) {
    _withOffers = offers;
    notifyListeners();
  }

  set getYourCustomerData(bool get) {
    _getYourCustomerData = get;
    notifyListeners();
  }

  set filterValues(List<bool> values) {
    _filterValues = values;
    notifyListeners();
  }

  set finalFilterValues(List<bool> values) {
    _finalFilterValues = values;
    notifyListeners();
  }

  set filterByItems(Map<dynamic, String> items) {
    _filterByItems = items;
    notifyListeners();
  }

  set currentSort(int? sort) {
    _currentSort = sort;
    notifyListeners();
  }

  set currentQueryParams(Map<String, dynamic> data) {
    _currentQueryParams = data;
    notifyListeners();
  }

  set size(int size) {
    _size = size;
    notifyListeners();
  }

  set oldPartnerJobs(List<OldPartnerJobsModel> jobs) {
    _oldPartnerJobs = jobs;
    notifyListeners();
  }

  Future<void> filterJobs({required List<String> filters}) async {
    final clientService = context.read<ClientService>();

    getYourCustomerData = true;

    Map<String, dynamic> queryParameters = {'page': 1, 'size': 14};

    if (_customerYearFilter != null) {
      queryParameters['year'] = _customerYearFilter;
    }

    final statusIds = {
      'new_task': '1',
      'in_dialogue': '2',
      'offers_have_been_sent': '4',
      'won_the_task': '5',
      'job_is_done': '6',
    };

    final jobType = {'master_to_master': 'mester', 'private': 'marketplace'};

    for (final filter in filters) {
      switch (filter) {
        case 'master_to_master':
        case 'private':
          if (jobType.containsKey(filter)) {
            final jobTypes = filters
                .map((filter) =>
                    jobType.containsKey(filter) ? jobType[filter] : null)
                .whereType<String>()
                .toList();
            queryParameters['jobType'] = jobTypes.join(',');
          }
          continue;
        case 'with_offers':
          queryParameters['withOffers'] = true;
          continue;
        case 'without_offers':
          queryParameters['withOffers'] = false;
          continue;
        case 'archive_customer':
          queryParameters['archive'] = 1;
          continue;
        default:
          if (statusIds.containsKey(filter)) {
            queryParameters = {'page': 1, 'size': 14};

            if (_customerYearFilter != null) {
              queryParameters['year'] = _customerYearFilter;
            }
            final statuses = filters
                .map((filter) =>
                    statusIds.containsKey(filter) ? statusIds[filter] : null)
                .whereType<String>()
                .toList();
            queryParameters['statusIds'] = statuses.join(',');
          }
      }
    }

    currentQueryParams = queryParameters;

    final paginatedJobs =
        await clientService.getPartnerJobsV2(queryParameters: queryParameters);

    currentPage = 1;

    if (paginatedJobs != null) {
      partnerJobs = [
        ...(paginatedJobs.data ?? <PartnerJobModel>[])
            .where((value) => value.jobsList is List)
      ];
      lastPage = paginatedJobs.lastPage ?? 1;
      firstPage = paginatedJobs.from ?? 1;
    }

    _currentJobFilters = [...filters];

    getYourCustomerData = false;

    if (filters.contains('archive_customer')) {
      final response = await clientService.getOldPartnerJobs();

      final oldArchiveJobs = await clientService.getArchivedOldJobs();

      if (response != null) {
        _finalOldPartnerJobs =
            response.where((element) => element.tilbudStatus != 100).toList();
        if (oldArchiveJobs != null) {
          List<String> jobIdsToKeep =
              oldArchiveJobs.map((job) => '${job["JOB_ID"]}').toList();
          _finalOldPartnerJobs.retainWhere(
              (element) => jobIdsToKeep.contains(element.idAppointment));
        }

        oldPartnerJobs = _finalOldPartnerJobs;
      }
    } else {
      final response = await clientService.getOldPartnerJobs();

      final oldArchiveJobs = await clientService.getArchivedOldJobs();

      if (response != null) {
        _finalOldPartnerJobs =
            response.where((element) => element.tilbudStatus != 100).toList();
        if (oldArchiveJobs != null) {
          List<String> jobIdsToRemove =
              oldArchiveJobs.map((job) => '${job["JOB_ID"]}').toList();
          _finalOldPartnerJobs.removeWhere(
              (element) => jobIdsToRemove.contains(element.idAppointment));
        }

        oldPartnerJobs = _finalOldPartnerJobs;
      }
    }
  }

  Future<List<PartnerJobModel>?> getPartnerJobByProjectId(
      {required int projectId}) async {
    final clientService = context.read<ClientService>();
    final paginatedJobs =
        await clientService.getPartnerJobsV2(queryParameters: {
      'page': 1,
      'size': 10,
      'projectId': projectId,
    });

    if (paginatedJobs != null) {
      return [
        ...(paginatedJobs.data ?? <PartnerJobModel>[])
            .where((value) => value.jobsList is List)
      ];
    } else {
      return null;
    }
  }

  Future<void> getSelectedJobFromNewsFeed(
      {required int jobIdFromNewsFeed}) async {
    final clientService = context.read<ClientService>();
    final job = await clientService.getSelectedJobFromNewsFeed(
        jobIdFromNewsFeed: jobIdFromNewsFeed);
    _selectedJobFromNewsFeed = job;

    notifyListeners();
  }

  Future<void> getNewAndOldJobs() async {
    final clientService = context.read<ClientService>();

    Map<String, dynamic> queryParameters = {
      'page': _currentPage,
      'size': _size
    };

    if (_searchController != null) {
      if (_searchController!.text.isNotEmpty) {
        queryParameters['search'] = _searchController!.text;
      }
    }

    if (_customerYearFilter != null) {
      queryParameters['year'] = _customerYearFilter;
    }

    if (_statusIds.isNotEmpty) queryParameters['statusIds'] = _statusIds;

    if (_sort.isNotEmpty) queryParameters['sort'] = _sort;

    if (_jobType.isNotEmpty) queryParameters['jobType'] = _jobType;

    if (_withOffers != null) queryParameters['withOffers'] = _withOffers;

    currentQueryParams = queryParameters;

    final paginatedJobs =
        await clientService.getPartnerJobsV2(queryParameters: queryParameters);

    if (paginatedJobs != null) {
      partnerJobs = [
        ...(paginatedJobs.data ?? <PartnerJobModel>[])
            .where((value) => value.jobsList is List)
      ];
      lastPage = paginatedJobs.lastPage ?? 1;
      firstPage = paginatedJobs.from ?? 1;
    }

    if (oldPartnerJobs.isNotEmpty) {
      if ((_searchController != null && _searchController!.text.isNotEmpty) ||
          _customerYearFilter != null ||
          _statusIds.isNotEmpty) {
        if (_searchController!.text.isNotEmpty) {
          final search = _searchController!.text;
          final queryJobs = <OldPartnerJobsModel>[..._oldPartnerJobs];
          oldPartnerJobs = queryJobs
              .where(
                (element) =>
                    element.firstName!
                        .toLowerCase()
                        .contains(search.toLowerCase()) ||
                    element.lastName!
                        .toLowerCase()
                        .contains(search.toLowerCase()),
              )
              .toList();
        }
        if (_customerYearFilter != null) {
          final tempJobs = <OldPartnerJobsModel>[..._oldPartnerJobs];
          oldPartnerJobs = tempJobs.any((element) =>
                  element.dateReserved!.split('-').first ==
                  _customerYearFilter!.toString())
              ? tempJobs
                  .where((element) =>
                      element.dateReserved!.split('-').first ==
                      _customerYearFilter!.toString())
                  .toList()
              : <OldPartnerJobsModel>[];
        }
        if (_statusIds.isNotEmpty) {
          final statuses =
              statusIds.split(',').map((e) => int.parse(e)).toList();
          final statusJobs = <OldPartnerJobsModel>[..._oldPartnerJobs];
          Map<int, int> filterValues = {};
          for (final filter in oldtilbudStatus) {
            filterValues.addAll({filter.value: filter.filterValue});
          }
          oldPartnerJobs = statusJobs
              .where((element) =>
                  statuses.contains(filterValues[element.tilbudStatus]))
              .toList();
        }
      } else {
        oldPartnerJobs = _finalOldPartnerJobs;
      }
    } else {
      final response = await clientService.getOldPartnerJobs();

      final oldArchiveJobs = await clientService.getArchivedOldJobs();

      if (response != null) {
        _finalOldPartnerJobs =
            response.where((element) => element.tilbudStatus != 100).toList();
        if (oldArchiveJobs != null) {
          List<String> jobIdsToRemove =
              oldArchiveJobs.map((job) => '${job["JOB_ID"]}').toList();
          _finalOldPartnerJobs.removeWhere(
              (element) => jobIdsToRemove.contains(element.idAppointment));
        }

        oldPartnerJobs = _finalOldPartnerJobs;
      }
    }
  }

  Future<void> getPartnerJobsV2() async {
    //
    final clientService = context.read<ClientService>();

    Map<String, dynamic> queryParameters = {...currentQueryParams};

    queryParameters['page'] = _currentPage;
    queryParameters['size'] = _size;

    if (_searchController != null) {
      if (_searchController!.text.isNotEmpty) {
        queryParameters['search'] = _searchController!.text;
      } else {
        if (queryParameters.containsKey('search')) {
          queryParameters.remove('search');
        }
      }
    }

    if (_customerYearFilter != null) {
      queryParameters['year'] = _customerYearFilter;
    }

    if (_statusIds.isNotEmpty) queryParameters['statusIds'] = _statusIds;

    if (_sort.isNotEmpty) queryParameters['sort'] = _sort;

    if (_jobType.isNotEmpty) queryParameters['jobType'] = _jobType;

    if (_withOffers != null) queryParameters['withOffers'] = _withOffers;

    currentQueryParams = queryParameters;

    final paginatedJobs =
        await clientService.getPartnerJobsV2(queryParameters: queryParameters);

    if (paginatedJobs != null) {
      partnerJobs = [
        ...(paginatedJobs.data ?? <PartnerJobModel>[])
            .where((value) => value.jobsList is List)
      ];
      lastPage = paginatedJobs.lastPage ?? 1;
      firstPage = paginatedJobs.from ?? 1;
    }
  }

  Future<void> getOldPartnerJobs() async {
    //
    final clientService = context.read<ClientService>();

    if (oldPartnerJobs.isNotEmpty) {
      if ((_searchController != null && _searchController!.text.isNotEmpty) ||
          _customerYearFilter != null ||
          _statusIds.isNotEmpty) {
        if (_searchController!.text.isNotEmpty) {
          final search = _searchController!.text;
          final queryJobs = <OldPartnerJobsModel>[..._oldPartnerJobs];
          oldPartnerJobs = queryJobs
              .where(
                (element) =>
                    element.firstName!
                        .toLowerCase()
                        .contains(search.toLowerCase()) ||
                    element.lastName!
                        .toLowerCase()
                        .contains(search.toLowerCase()),
              )
              .toList();
        }
        if (_customerYearFilter != null) {
          final tempJobs = <OldPartnerJobsModel>[..._oldPartnerJobs];
          oldPartnerJobs = tempJobs.any((element) =>
                  element.dateReserved!.split('-').first ==
                  _customerYearFilter!.toString())
              ? tempJobs
                  .where((element) =>
                      element.dateReserved!.split('-').first ==
                      _customerYearFilter!.toString())
                  .toList()
              : <OldPartnerJobsModel>[];
        }
        if (_statusIds.isNotEmpty) {
          final statuses =
              statusIds.split(',').map((e) => int.parse(e)).toList();
          final statusJobs = <OldPartnerJobsModel>[..._oldPartnerJobs];
          Map<int, int> filterValues = {};
          for (final filter in oldtilbudStatus) {
            filterValues.addAll({filter.value: filter.filterValue});
          }
          oldPartnerJobs = statusJobs
              .where((element) =>
                  statuses.contains(filterValues[element.tilbudStatus]))
              .toList();
        }
      } else {
        oldPartnerJobs = _finalOldPartnerJobs;
      }
    } else {
      final response = await clientService.getOldPartnerJobs();

      final oldArchiveJobs = await clientService.getArchivedOldJobs();

      if (response != null) {
        _finalOldPartnerJobs =
            response.where((element) => element.tilbudStatus != 100).toList();
        if (oldArchiveJobs != null) {
          List<String> jobIdsToRemove =
              oldArchiveJobs.map((job) => '${job["JOB_ID"]}').toList();
          _finalOldPartnerJobs.removeWhere(
              (element) => jobIdsToRemove.contains(element.idAppointment));
        }

        oldPartnerJobs = _finalOldPartnerJobs;
      }
    }

    //
  }

  Future<void> getClients({
    required TenderFolderViewModel tenderVm,
    required CreateOfferViewModel createOfferVm,
  }) async {
    await Future.wait([
      getNewAndOldJobsInitial(),
    ]);
  }

  Future<void> getOffers() async {
    var clientService = context.read<ClientService>();
    setBusy(true);
    var offers = await clientService.getOffers();
    if (offers != null) {
      _offers = offers;
    }
    notifyListeners();

    setBusy(false);
  }

  Future<void> getNewAndOldJobsInitial() async {
    final clientService = context.read<ClientService>();

    Map<String, dynamic> queryParameters = {...currentQueryParams};

    queryParameters['page'] = _currentPage;
    queryParameters['size'] = _size;

    if (_searchController != null) {
      if (_searchController!.text.isNotEmpty) {
        queryParameters['search'] = _searchController!.text;
      }
    }

    if (_customerYearFilter != null) {
      queryParameters['year'] = _customerYearFilter;
    }

    if (_statusIds.isNotEmpty) queryParameters['statusIds'] = _statusIds;

    if (_sort.isNotEmpty) queryParameters['sort'] = _sort;

    if (_jobType.isNotEmpty) queryParameters['jobType'] = _jobType;

    if (_withOffers != null) queryParameters['withOffers'] = _withOffers;

    currentQueryParams = queryParameters;

    final responses = await Future.wait([
      clientService.getPartnerJobsV2(queryParameters: queryParameters),
      clientService.getOldPartnerJobs(),
      clientService.getArchivedOldJobs(),
      clientService.getAffiliateJobs(),
      clientService.getSubcontractorInvitations(),
    ]);

    if (responses[0] != null) {
      final partnerJob = responses[0] as PaginatedPartnerJobsModel;
      partnerJobs = [
        ...(partnerJob.data ?? <PartnerJobModel>[])
            .where((value) => value.jobsList is List)
      ];
      lastPage = partnerJob.lastPage ?? 1;
      firstPage = partnerJob.from ?? 1;
    }

    if (responses[1] != null) {
      _finalOldPartnerJobs = (responses[1] as List<OldPartnerJobsModel>)
          .where((element) => element.tilbudStatus != 100)
          .toList();
      List<String> jobIdsToRemove =
          (responses[2] as List).map((job) => '${job["JOB_ID"]}').toList();
      _finalOldPartnerJobs.removeWhere(
          (element) => jobIdsToRemove.contains(element.idAppointment));

      oldPartnerJobs = _finalOldPartnerJobs;
    }

    if (responses[3] != null) {
      _affiliateJobs = AffiliateJob.fromCollection(
          (responses[3] as MinboligApiResponse).data['items']);
    }

    if (responses[4] != null) {
      _subContractorInvitations = responses[4] as List<SubcontractorInvitation>;
    }

    notifyListeners();
  }

  Info initQuotationForm(String? id) {
    _currentState = OfferState.newOffer;

    return Info(
      customerId: null,
      currencyId: null,
      date: Formatter.formatDateTime(
          type: DateFormatType.isoDate, date: DateTime.now()),
      expiryDate: Formatter.formatDateTime(
          type: DateFormatType.isoDate,
          date: DateTime.now().add(const Duration(days: 7))),
      expiryDateDetail: '',
      uploadToken: '',
      itemType: '',
      rows: <Rows>[],
      subject: '',
      description: '',
      subtotal: 0,
      subtotalDiscount: 0,
      lineItemDiscountType: true,
      subtotalDiscountType: false,
      subtotalTax: 25,
      subtotalShippingAndHandling: 0,
      total: 0,
      referenceNumber: 'REF-$id', //'REF${userTilbudModel.tilbud!.length}-$id',
      quotationNumber: '',
      quotationPrefix: '',
      isDraft: 0,
      showSubTotalTax: true,
      showSubTotalDiscount: true,
      showSubTotalShippingAndHandling: true,
      showLineItemTax: true,
      showLineItemDiscount: true,
      showLineItemDescription: true,
      customerNote: '',
      memo: '',
      tnc: '',
    );
  }

  Rows newRow() {
    return Rows(
      uuid: const Uuid().v4(),
      name: '',
      selectedItem: null,
      showCustomItem: false,
      customName: '',
      quantity: 1,
      unitPrice: 0,
      discount: 0,
      tax: 0,
      type: 1,
      amount: 0,
      description: '',
    );
  }

  void initializeSortValues() {
    //

    filterByItems = {
      0: 'all',
      'mester': 'master_to_master',
      'private': 'private',
      true: 'with_offers',
      false: 'without_offers',
      1: 'new_task',
      2: 'in_dialogue',
      3: 'offers_have_been_sent',
      5: 'won_the_task',
      7: 'job_is_done',
      8: 'archive_customer'
    };
    filterValues = List.generate(_filterByItems.length, (index) => false);
    finalFilterValues = List.generate(_filterByItems.length, (index) => false);
    currentSort = null;
    statusIds = '';
    jobType = '';
    sort = '';
    _size = 14;
    withOffers = null;

    //
  }

  void setSearchControllers() {
    _searchController = TextEditingController();
    _searchFocusNode = FocusNode();
  }

  void initializeJobIdFromNewsFeed() {
    _jobIdFromNewsFeed = 0;
    notifyListeners();
  }

  void setJobIdFromNewsFeed(int jobId) {
    _jobIdFromNewsFeed = jobId;
    notifyListeners();
  }

  void loadQuotationForm({required TilbudModel model}) {
    _editOfferId = model.id!;
    quotationForm = model.info;
    _currentState = OfferState.editOffer;
    notifyListeners();
  }

  void changeCustomerYearFilter(int? newyear) {
    _customerYearFilter = newyear;
    notifyListeners();
  }

  void changeOfferYearFilter(int newyear) {
    _offerYearFilter = newyear;
    notifyListeners();
  }
}

class OfferFormModel {
  OfferFormModel({
    this.item,
    this.type,
    this.quantity,
    this.price,
    this.amount,
  });
  String? item;
  String? type;
  double? quantity;
  double? price;
  double? amount;
}

Future<Info?> calculate(Info? value) async {
  // bool showQuantity = true;
  double total = 0;
  for (var row in value!.rows!) {
    double discount = 0;
    discount = formatNumber(number: discount)!;
    row!.discount = discount;
    double tax = 0;
    tax = formatNumber(number: tax)!;
    row.tax = tax;
    double quantity = (row.quantity.toDouble() ?? 1.0) as double;
    quantity = formatNumber(number: quantity)!;
    row.quantity = quantity;
    var unitPrice = row.unitPrice ?? 0;
    unitPrice = formatNumber(number: unitPrice)!;
    row.unitPrice = unitPrice;
    if (row.type != 1) {
      var price = quantity * unitPrice;
      // price = price - (this.quotationForm.line_item_discount_type ? (price * discount / 100) : discount)
      // price = price + (price * tax / 100)
      var amount = formatNumber(number: price);
      row.amount = amount;
    } else {
      // var price = quantity * unitPrice;
      // price = price - (this.quotationForm.line_item_discount_type ? (price * discount / 100) : discount)
      // price = price + (price * tax / 100)
      var amount = formatNumber(number: unitPrice);
      row.amount = amount;
    }
    total = total + row.amount!;
  }
  value.subtotal = formatNumber(number: total);
  var subtotalDiscount =
      value.showSubTotalDiscount! ? value.subtotalDiscount ?? 0 : 0;
  subtotalDiscount = formatNumber(number: subtotalDiscount);
  value.subtotalDiscount = subtotalDiscount;
  double subtotalTax = 25;
  subtotalTax = formatNumber(number: subtotalTax)!;
  value.subtotalTax = subtotalTax;
  double subtotalShippingAndHandling = value.showSubTotalShippingAndHandling!
      ? value.subtotalShippingAndHandling.toDouble() ?? 0.0
      : 0.0;
  subtotalShippingAndHandling =
      formatNumber(number: subtotalShippingAndHandling)!;
  value.subtotalShippingAndHandling = subtotalShippingAndHandling;
  total = total -
      (value.subtotalDiscountType!
          ? (total * (subtotalDiscount as num) / 100)
          : (subtotalDiscount as double?)!);

  total = total + (total * subtotalTax / 100);
  total = total + subtotalShippingAndHandling;
  value.total = formatNumber(number: total);

  return value;
}

double? formatNumber({required number, num? decimalPlace}) {
  var mDecimalPlace = decimalPlace;
  mDecimalPlace ??= 2;
  return roundNumber(number: number, precision: mDecimalPlace);
}

double? roundNumber({required number, num? precision}) {
  var mPrecision = precision;
  if (mPrecision is String) {
    mPrecision = int.tryParse(mPrecision as String) == null
        ? 0
        : int.parse(mPrecision as String).abs();
  } else {
    mPrecision = mPrecision!.abs();
  }

  var multiplier = math.pow(10, mPrecision);
  return ((number * multiplier).round() / multiplier) as double?;
}
