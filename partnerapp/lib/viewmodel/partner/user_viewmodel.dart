import 'package:Haandvaerker.dk/model/email/email_model.dart';
import 'package:Haandvaerker.dk/model/not_paid_setting/not_paid_setting.dart';
import 'package:Haandvaerker.dk/model/partner/partner_login_model.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/main/main_service.dart';
import 'package:Haandvaerker.dk/services/push_notification/push_notification_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

class UserViewModel extends BaseViewModel {
  UserViewModel({required this.context});
  final BuildContext context;

  PartnerLoginModel? _userModel;
  PartnerLoginModel? get userModel => _userModel;

  bool _isFetching = false;
  bool get isFetching => _isFetching;

  bool? _rememberMe = false;
  bool? get rememberMe => _rememberMe;

  bool? _isSuccess;
  bool? get isSuccess => _isSuccess;

  bool _isForgotPasswordSuccess = false;
  bool get isForgotPasswordSuccess => _isForgotPasswordSuccess;

  bool _isRequesting = false;
  bool get isRequesting => _isRequesting;

  bool? _isInvalidEmail;
  bool? get isInvalidEmail => _isInvalidEmail;

  bool _isGetEmail = false;
  bool get isGetEmail => _isGetEmail;

  List<EmailModel> _partnerEmails = [];
  List<EmailModel> get partnerEmails => _partnerEmails;

  List<OtherAccounts> _otherAccounts = [];
  List<OtherAccounts> get otherAccounts => _otherAccounts;

  NotPaidSetting? _notPaidSetting;
  NotPaidSetting? get notPaidSetting => _notPaidSetting;

  bool _clickFromNotPaid = false;
  bool get clickFromNotPaid => _clickFromNotPaid;

  bool _notPaidDialogShowed = false;
  bool get notPaidDialogShowed => _notPaidDialogShowed;

  void reset() {
    _userModel = null;
    _isFetching = false;
    _rememberMe = false;
    _isSuccess = false;
    _isForgotPasswordSuccess = false;
    _isRequesting = false;
    _isInvalidEmail = false;
    _isGetEmail = false;
    _partnerEmails = [];
    _otherAccounts = [];
    _notPaidSetting = null;
    _clickFromNotPaid = false;
    _notPaidDialogShowed = false;

    notifyListeners();
  }

  set notPaidDialogShowed(bool value) {
    _notPaidDialogShowed = value;

    notifyListeners();
  }

  set clickFromNotPaid(bool value) {
    _clickFromNotPaid = value;

    notifyListeners();
  }

  set rememberMe(bool? value) {
    _rememberMe = value;
    notifyListeners();
  }

  set userModel(PartnerLoginModel? loginModel) {
    _userModel = loginModel;

    notifyListeners();
  }

  set isSuccess(bool? value) {
    _isSuccess = value;
    notifyListeners();
  }

  set isRequesting(bool value) {
    _isRequesting = value;
    notifyListeners();
  }

  set isForgotPasswordSuccess(bool value) {
    _isForgotPasswordSuccess = value;
    notifyListeners();
  }

  set isGetEmail(bool value) {
    _isGetEmail = value;
    notifyListeners();
  }

  set setIsInvalidEmail(bool? value) {
    _isInvalidEmail = value;
    notifyListeners();
  }

  Future<void> deletePartnerEmail({required int emailId}) async {
    final mainService = context.read<MainService>();

    await mainService.deletePartnerEmails(emailId: emailId);

    await getPartnerEmails();
  }

  Future<void> addPartnerEmail({required String email}) async {
    final mainService = context.read<MainService>();

    await mainService.addPartnerEmails(email: email);

    await getPartnerEmails();
  }

  Future<void> getPartnerEmails() async {
    final mainService = context.read<MainService>();

    final response = await mainService.getPartnerEmails();

    if (!(response.success ?? true)) {
      throw ApiException(message: response.message ?? '');
    }

    _partnerEmails = EmailModel.fromCollection(response.data);

    notifyListeners();
  }

  Future<void> loginCvr({required String cvr, required String password}) async {
    final mainService = context.read<MainService>();
    _isFetching = true;

    final response = await mainService.loginCvr(
      cvr: cvr,
      password: password,
      rememberMe: rememberMe ?? false,
    );
    _userModel = response['data'] as PartnerLoginModel;
    _isSuccess = response['success'] as bool;
    _isFetching = false;

    Sentry.configureScope((scope) => scope
      ..setUser(SentryUser(
          id: '${userModel?.user?.id}', email: userModel?.user?.email)));

    notifyListeners();
  }

  Future<void> login(String email, String password) async {
    final mainService = context.read<MainService>();
    _isFetching = true;
    _otherAccounts = [];

    final emailsResponse = await mainService.checkEmail(email: email);

    if (emailsResponse != null) {
      if ((emailsResponse.data['hasMultipleAccounts'] as bool)) {
        _otherAccounts =
            OtherAccounts.fromCollection(emailsResponse.data['accounts']);
      }
    }

    if (otherAccounts.isEmpty) {
      final response = await mainService.login(
        email: email,
        password: password,
        rememberMe: rememberMe ?? false,
      );
      //await initPushNotification();

      _userModel = response['data'] as PartnerLoginModel;
      _isSuccess = response['success'] as bool;
      _isFetching = false;

      Sentry.configureScope((scope) => scope
        ..setUser(SentryUser(
            id: '${userModel?.user?.id}', email: userModel?.user?.email)));
    }
    notifyListeners();
  }

  startFetching() {
    _isFetching = true;
    notifyListeners();
  }

  stopFetching() {
    _isFetching = false;
    notifyListeners();
  }

  Future<void> initPushNotification() async {
    final service = context.read<PushNotificationService>();

    try {
      await service.initialise();
      //service.subscribeToTopic(Topics.newsfeed);
    } catch (_) {}
  }

  Future<bool> validateToken() async {
    final mainService = context.read<MainService>();

    if ((await mainService.checkToken())) {
      return false;
    }

    try {
      final response = await mainService.validateToken();

      _userModel =
          PartnerLoginModel.fromJson(response.data as Map<String, dynamic>);

      Sentry.configureScope((scope) => scope
        ..setUser(SentryUser(
            id: '${userModel?.user?.id}', email: userModel?.user?.email)));
      notifyListeners();

      return true;
    } catch (e) {
      return false;
    }
  }

  Future<void> logout() async {
    final mainService = context.read<MainService>();
    await mainService.logout();
  }

  Future<Map<String, dynamic>> checkRememberMe() async {
    final mainService = context.read<MainService>();

    final userMap = await mainService.checkRememberMe();

    _rememberMe = userMap.isNotEmpty;

    notifyListeners();

    return userMap;
  }

  Future<void> checkNotPaidSettings() async {
    final mainService = context.read<MainService>();

    final response = await mainService.notPaidSettings();

    if (response != null) {
      _notPaidSetting = NotPaidSetting.fromJson(response.data);
    }

    notifyListeners();
  }

  Future<void> createTaskFromNotPaidPopup() async {
    final mainService = context.read<MainService>();

    await mainService.createTaskFromNotPaidPopup();
  }

  Future<bool> sendResetLink({required String email}) async {
    final mainService = context.read<MainService>();

    setBusy(true);

    final response = await mainService.forgotPassword(email: email);

    setBusy(false);

    return response?.success ?? false;
  }

  Future<bool> resetPassword(
      {required String email, required String password}) async {
    final mainService = context.read<MainService>();
    setBusy(true);

    final response =
        await mainService.resetPassword(email: email, password: password);

    setBusy(false);

    return response?.success ?? false;
  }

  Future<void> logoutFromResetPassword() async {
    final mainService = context.read<MainService>();

    await mainService.logoutFromResetPassword();
  }

  Future<void> clearStorage() async {
    final mainService = context.read<MainService>();

    await mainService.clearStorage();
  }
}
