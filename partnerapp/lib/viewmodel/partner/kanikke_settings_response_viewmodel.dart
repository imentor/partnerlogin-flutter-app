import 'package:Haandvaerker.dk/model/kanikke_popups_model.dart';
import 'package:Haandvaerker.dk/model/kanikke_settings_response_model.dart';
import 'package:Haandvaerker.dk/services/client/client_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class KanIkkeSettingsResponseViewModel with ChangeNotifier {
  KanIkkeSettingsResponseViewModel({required this.context});
  final BuildContext context;

  KanIkkeSettingsResponseModel? _kanIkkePartnerSettings;
  KanIkkeSettingsResponseModel? get kanIkkePartnerSettings =>
      _kanIkkePartnerSettings;

  bool _isBlackListed = false;
  bool get isBlackListed => _isBlackListed;

  bool _isWarningForBlackList = false;
  bool get isWarningForBlackList => _isWarningForBlackList;

  String _popupText = '';
  String get popupText => _popupText;

  String _warningPopupText = '';
  String get warningPopupText => _warningPopupText;

  String _callFirstText = '';
  String get callFirstText => _callFirstText;

  String _kanikkeConfirmationAcceptableStage = '';
  String get kanikkeConfirmationAcceptableStage =>
      _kanikkeConfirmationAcceptableStage;

  String _kanikkeConfirmationWarningStage = '';
  String get kanikkeConfirmationWarningStage =>
      _kanikkeConfirmationWarningStage;

  List<String> _featuresLimitation = [];
  List<String> get featuresLimitation => _featuresLimitation;

  void reset() {
    _kanIkkePartnerSettings = null;
    _isBlackListed = false;
    _isWarningForBlackList = false;
    _popupText = '';
    _warningPopupText = '';
    _callFirstText = '';
    _kanikkeConfirmationAcceptableStage = '';
    _kanikkeConfirmationWarningStage = '';
    _featuresLimitation = [];

    notifyListeners();
  }

  Future<void> getKanIkkeSettings() async {
    final clientService = context.read<ClientService>();

    final responses = await Future.wait([
      clientService.getKanIkkeSettings(),
      clientService.getKanIkkePopups(),
    ]);

    if (responses[0] != null) {
      final kanIkkePartnerSettings =
          responses[0] as KanIkkeSettingsResponseModel;
      _kanIkkePartnerSettings = kanIkkePartnerSettings;
      _isBlackListed = kanIkkePartnerSettings.blacklist!.isBlackListed ?? false;
      _isWarningForBlackList =
          kanIkkePartnerSettings.blacklist!.warningForBlackList ?? false;
      _featuresLimitation =
          kanIkkePartnerSettings.kanIkkeSettings!.featuresLimitation ?? [];
    }
    if (responses[1] != null) {
      final kanIkkePopups = responses[1] as KanIkkePopupsModel;
      _popupText = kanIkkePopups.kanikkePopupText ?? '';
      _warningPopupText = kanIkkePopups.kanikkeWarningPopupText ?? '';
      _callFirstText = kanIkkePopups.callFirstBeforeOffer ?? '';
      _kanikkeConfirmationAcceptableStage =
          kanIkkePopups.kanikkeConfirmationAcceptableStage ?? '';
      _kanikkeConfirmationWarningStage =
          kanIkkePopups.kanikkeConfirmationWarningStage ?? '';
    }

    notifyListeners();
  }
}
