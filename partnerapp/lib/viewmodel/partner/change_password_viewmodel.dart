import 'package:Haandvaerker.dk/services/main/main_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ChangePasswordViewModel extends BaseViewModel {
  ChangePasswordViewModel({required this.context});
  final BuildContext context;

  Future<void> changePassword({required String password}) async {
    final service = context.read<MainService>();

    await service.changePassword(password: password);
  }
}
