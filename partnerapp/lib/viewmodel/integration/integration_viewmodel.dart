import 'package:Haandvaerker.dk/model/settings/order_management_model.dart';
import 'package:Haandvaerker.dk/services/integration/integration_service.dart';
import 'package:Haandvaerker.dk/viewmodel/base_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class IntegrationViewModel extends BaseViewModel {
  IntegrationViewModel({required this.context});

  final BuildContext context;

  OrderManagementModel? _currentOrderManagement;
  OrderManagementModel? get currentOrderManagement => _currentOrderManagement;

  void reset() {
    _currentOrderManagement = null;

    notifyListeners();
  }

  set currentOrderManagement(OrderManagementModel? order) {
    _currentOrderManagement = order;
    notifyListeners();
  }

  Future<void> getOrderManagement() async {
    final service = context.read<IntegrationService>();

    setBusy(true);
    final response = await service.getOrderManagement();
    if (response.isNotEmpty) {
      currentOrderManagement = response.first;
    }
    setBusy(false);
  }

  Future<void> addOrderManagement({required String apiKey}) async {
    final service = context.read<IntegrationService>();

    currentOrderManagement = await service.addOrderManagement(apiKey: apiKey);
  }
}
