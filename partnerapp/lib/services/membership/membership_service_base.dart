import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class MembershipServiceBase {
  Future<MinboligApiResponse?> getMembersV2({required String zip});
}
