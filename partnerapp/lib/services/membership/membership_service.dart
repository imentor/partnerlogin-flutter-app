import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/membership/membership_service_base.dart';
import 'package:dio/dio.dart';

class MembershipService extends MembershipServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  MembershipService({required this.apiService, required this.storage});

  @override
  Future<MinboligApiResponse?> getMembersV2({required String zip}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/bitrix/getMembers',
      headers: await storage.getHeaders(),
      queryParameters: {'zip': zip},
      operation: operation[Operation.getMembers]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }

    //
  }
}
