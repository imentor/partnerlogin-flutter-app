import 'dart:convert';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/manufacturer_sub_contractor/manufacturer_sub_contractor_service_base.dart';
import 'package:dio/dio.dart';

class ManufacturerSubContractorService
    extends ManufacturerSubContractorServiceBase {
  final ApiService apiService;
  final SecuredStorageService storage;

  ManufacturerSubContractorService(
      {required this.apiService, required this.storage});

  @override
  Future<MinboligApiResponse> subContractorInvite(
      {required Map<String, dynamic> payload}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/subContractor/invite',
      headers: await storage.getHeaders(),
      data: jsonEncode(payload),
      operation: operation[Operation.subContractorInvite]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<Map<String, dynamic>> getHeaders() async {
    return await storage.getHeaders();
  }

  @override
  Future<MinboligApiResponse> subContractorAddSignature(
      {required String signature, required int id}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/subContractor/addSignature',
      headers: await storage.getHeaders(),
      data: jsonEncode({
        'partnerSignature': signature,
        'subcontractorId': id,
        'type': 'partner'
      }),
      operation: operation[Operation.subContractorAddSignature]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> getProducentCompanies(
      {required String search}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/searchForProducentv2',
      headers: await storage.getHeaders(),
      queryParameters: {
        'companyname': search,
      },
      operation: operation[Operation.getProducentCompanies]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> manufacturerAddRequisition(
      {required FormData form}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/AddRequistionT',
      headers: await storage.getHeaders(),
      data: form,
      operation: operation[Operation.manufacturerAddRequisition]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> acceptInviteSubContractor(
      {required Map<String, dynamic> payload}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/external/acceptInviteSubContractor',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.acceptInviteSubContractor]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }
}
