import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

@immutable
abstract class ManufacturerSubContractorServiceBase {
  Future<MinboligApiResponse> subContractorInvite(
      {required Map<String, dynamic> payload});

  Future<MinboligApiResponse> subContractorAddSignature(
      {required String signature, required int id});

  Future<MinboligApiResponse> getProducentCompanies({required String search});

  Future<MinboligApiResponse> manufacturerAddRequisition(
      {required FormData form});

  Future<Map<String, dynamic>> getHeaders();

  Future<MinboligApiResponse> acceptInviteSubContractor(
      {required Map<String, dynamic> payload});
}
