import 'dart:convert';
import 'dart:io';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/recommendation/company_reviews_model.dart';
import 'package:Haandvaerker.dk/model/recommendation/invited_customer_model.dart';
import 'package:Haandvaerker.dk/model/recommendation/recommendation_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/recommendation/recommendation_service_base.dart';
import 'package:dio/dio.dart';

class RecommendationService extends RecommendationServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  RecommendationService({required this.apiService, required this.storage});

  @override
  Future<RecommendationModel?> getRecommendations({
    required int page,
    required int rating,
    required String search,
  }) async {
    //

    Map<String, dynamic> queryParams = {
      'page': page,
      'size': 10,
    };
    if (rating != 0 && search != '') {
      queryParams.putIfAbsent('rating', () => rating);
      queryParams.putIfAbsent('search', () => search);
    } else {
      if (rating == 0 && search != '') {
        queryParams.putIfAbsent('search', () => search);
      }
      if (rating != 0 && search == '') {
        queryParams.putIfAbsent('rating', () => rating);
      }
    }

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/rating/reviews',
      headers: await storage.getHeaders(),
      queryParameters: queryParams,
      operation: operation[Operation.getRecommendations]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return RecommendationModel.fromJson(
          Map<String, dynamic>.from(response['data']));
    } else {
      return null;
    }

    //
  }

  @override
  Future<RecommendationDownloadImage?> getReviewImage({
    required int reviewId,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/rating/reviewImage/$reviewId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getReviewImage]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return RecommendationDownloadImage.fromJson(response['data']);
    } else {
      return null;
    }

    //
  }

  @override
  Future<RecommendationDownloadImage?> getReviewImageV2(
      {required int reviewId}) async {
    //
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/rating/reviewImagev2?reviewId=$reviewId&ClearCache=1',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getReviewImageV2]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return RecommendationDownloadImage.fromJson(response['data']);
    } else {
      return null;
    }
  }

  @override
  Future<MinboligApiResponse> createTradesmanComplaint({
    required String complaint,
    int? category,
    required String headline,
    required int reviewId,
    File? file,
    String? fileName,
  }) async {
    //

    final map = {
      "DOCUMENTATIONCOMMENT": headline,
      "TRADEMANCOMPLAINT": complaint,
      "TRADEMANCOMPLAINTHEADLINE": headline,
      "REVIEWID": reviewId,
    };

    if (file != null) {
      map['files[]'] = await MultipartFile.fromFile(
        file.path,
        filename: fileName,
      );
    }

    if (category != null) {
      map['TRADEMANCOMPLAINTCATEGORY'] = category;
    }

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/rating/UploadReviewDocumentationv2',
      headers: await storage.getHeaders(),
      data: FormData.fromMap(map),
      operation: operation[Operation.createTradesmanComplaint]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> commentOnReview(
      {required int reviewId, required String supplierComment}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/EditSupplierComment',
      headers: await storage.getHeaders(),
      data: jsonEncode(
          {"Review_ID": reviewId, "SUPPLIERCOMMENT": supplierComment}),
      operation: operation[Operation.commentOnReview]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> savePartnerReviewOnHomeowner({
    required int customerId,
    required String headline,
    required String comment,
    required int enterpriseSum,
    required int ratingCommunication,
    required int ratingTime,
    required int ratingRealisticExpectations,
    required int ratingFinancialSecurity,
    required int ratingCollaboration,
    required int rating,
    required int ratingAccess,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/SavePartnerReviewonHomeowner',
      headers: await storage.getHeaders(),
      data: {
        'UF_CUSTOMER_ID': customerId,
        'UF_HEADLINE': headline,
        'UF_COMMENT': comment,
        'UF_ENTERPRISE_SUM': enterpriseSum,
        'UF_RATING_COMMUNICATION': ratingCommunication,
        'UF_RATING_TIME': ratingTime,
        'UF_RATING_REALISTIC_EXPECTATIONS': ratingRealisticExpectations,
        'UF_RATING_FINANCIAL_SECURITY': ratingFinancialSecurity,
        'UF_RATING_COLLABORATION': ratingCollaboration,
        'UF_RATING': rating,
        'UF_RATING_ACCESS': ratingAccess,
      },
      operation: operation[Operation.savePartnerReviewOnHomeowner]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> getPartnersReviewsDoneOnClient() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/GetPartnersReviewsDoneOnClient',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getPartnersReviewsDoneOnClient]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse?> getPartnersReviewsDoneOnClientV2(
      {required String homeowner, required int page}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/GetPartnersReviewsDoneOnClientV2',
      headers: await storage.getHeaders(),
      queryParameters: {
        "homeowner": homeowner,
        "page": page,
        "size": 10,
      },
      operation: operation[Operation.getPartnersReviewsDoneOnClientV2]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }

    //
  }

  @override
  Future<MinboligApiResponse> deleteReviewById({required reviewId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/deleteReviewById',
      headers: await storage.getHeaders(),
      data: {"reviewId": reviewId},
      operation: operation[Operation.deleteReviewById]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> updateSavePartnerReviewonHomeowner({
    required reviewId,
    required customerId,
    required int enterpriseSum,
    required String comment,
    required double communicationRating,
    required String headline,
    required double timeRating,
    required double accessRating,
    required double financialRating,
    required double realisticExpectationRating,
    required double collaborationRating,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/UpdateSavePartnerReviewonHomeowner',
      headers: await storage.getHeaders(),
      data: {
        "ID": reviewId,
        "UF_HEADLINE": headline,
        "UF_COMMENT": comment,
        "UF_ENTERPRISE_SUM": enterpriseSum,
        "UF_CUSTOMER_ID": customerId,
        "UF_RATING_COMMUNICATION": communicationRating.toInt(),
        "UF_RATING_TIME": timeRating.toInt(),
        "UF_RATING_ACCESS": accessRating.toInt(),
        "UF_RATING_REALISTIC_EXPECTATIONS": realisticExpectationRating.toInt(),
        "UF_RATING_FINANCIAL_SECURITY": financialRating.toInt(),
        "UF_RATING_COLLABORATION": collaborationRating.toInt(),
      },
      operation: operation[Operation.updateSavePartnerReviewonHomeowner]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<List<CompanyReviewsModel>> getReviewsCompanyV2() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/getReviewsCompanyv2',
      headers: await storage.getHeaders(),
      operation: operation[Operation.get_reviews_companyV2]!,
      cancelToken: cancelToken,
    );

    return CompanyReviewsModel.fromCollection(
        List<dynamic>.from(response['data']));

    //
  }

  @override
  Future<MinboligApiResponse?> getSupplierIntegrations() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/integrations',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getSupplierIntegrations]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }

    //
  }

  @override
  Future<MinboligApiResponse> saveSupplierIntegration(
      {required Map<String, dynamic> payload}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/integration/saveSupplierIntegration',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.saveSupplierIntegration]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<List<InvitedCustomerModel>?> getInvitedCustomers() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/rating/getInvitedCustomers',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getInvitedCustomers]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return InvitedCustomerModel.fromCollection(
          List<dynamic>.from(response['data']));
    } else {
      return null;
    }
    //
  }

  @override
  Future<bool> inviteCustomer({
    required String name,
    required String email,
    required String phone,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/rating/inviteCustomer',
      headers: await storage.getHeaders(),
      data: {
        'name': name,
        'email': email,
        'mobile': phone,
        'image': "undefined",
      },
      operation: operation[Operation.inviteCustomer]!,
      cancelToken: cancelToken,
    );

    return response['data'];

    //
  }

  @override
  Future<bool> deleteInvitedCustomer({required String inviteId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/rating/deleteInvitedCustomers',
      headers: await storage.getHeaders(),
      data: {'inviteId': inviteId},
      operation: operation[Operation.deleteInvitedCustomer]!,
      cancelToken: cancelToken,
    );

    return response['success'];

    //
  }

  //inviteCustomer
}
