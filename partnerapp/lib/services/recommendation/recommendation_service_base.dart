import 'dart:io';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/recommendation/company_reviews_model.dart';
import 'package:Haandvaerker.dk/model/recommendation/invited_customer_model.dart';
import 'package:Haandvaerker.dk/model/recommendation/recommendation_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class RecommendationServiceBase {
  //

  Future<RecommendationModel?> getRecommendations(
      {required int page, required int rating, required String search});

  Future<RecommendationDownloadImage?> getReviewImage({required int reviewId});

  Future<RecommendationDownloadImage?> getReviewImageV2(
      {required int reviewId});

  Future<MinboligApiResponse> createTradesmanComplaint({
    required String complaint,
    int? category,
    required String headline,
    required int reviewId,
    File? file,
  });

  Future<MinboligApiResponse> commentOnReview(
      {required int reviewId, required String supplierComment});

  Future<MinboligApiResponse> savePartnerReviewOnHomeowner({
    required int customerId,
    required String headline,
    required String comment,
    required int enterpriseSum,
    required int ratingCommunication,
    required int ratingTime,
    required int ratingRealisticExpectations,
    required int ratingFinancialSecurity,
    required int ratingCollaboration,
    required int rating,
    required int ratingAccess,
  });

  Future<MinboligApiResponse> getPartnersReviewsDoneOnClient();

  Future<MinboligApiResponse?> getPartnersReviewsDoneOnClientV2(
      {required String homeowner, required int page});

  Future<MinboligApiResponse> deleteReviewById({required reviewId});

  Future<MinboligApiResponse> updateSavePartnerReviewonHomeowner({
    required reviewId,
    required customerId,
    required int enterpriseSum,
    required String comment,
    required double communicationRating,
    required String headline,
    required double timeRating,
    required double accessRating,
    required double financialRating,
    required double realisticExpectationRating,
    required double collaborationRating,
  });

  Future<List<CompanyReviewsModel>> getReviewsCompanyV2();

  Future<MinboligApiResponse?> getSupplierIntegrations();

  Future<MinboligApiResponse> saveSupplierIntegration(
      {required Map<String, dynamic> payload});

  Future<List<InvitedCustomerModel>?> getInvitedCustomers();

  Future<bool> inviteCustomer({
    required String name,
    required String email,
    required String phone,
  });

  Future<bool> deleteInvitedCustomer({required String inviteId});

  //
}
