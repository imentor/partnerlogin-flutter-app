import 'dart:io';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class ContractServiceBase {
  //

  Future<MinboligApiResponse> getContractComments({required contractId});

  Future<MinboligApiResponse> getPartnerContracts();

  Future<MinboligApiResponse> deleteContractComment({required commentId});

  Future<MinboligApiResponse> addContractComment(
      {required contractId, required title, required comments, required page});

  Future<MinboligApiResponse> signContract({required contractId});

  Future<MinboligApiResponse> contractDocumentPreview({required contractId});

  Future<MinboligApiResponse> saveAnnotation(
      {required contractId, required documentId, required annotation});

  Future<MinboligApiResponse> getContractTemplate();

  Future<MinboligApiResponse?> getContractInfoV2({
    required int contractValue,
    required int projectId,
    required int offerId,
    required int homeownerId,
    required String wholeEnterprise,
  });

  Future<MinboligApiResponse> getTotalFees({required int totalWithVat});

  Future<MinboligApiResponse> submitContract(
      {required Map<String, dynamic> form});

  Future<MinboligApiResponse> getContractById({required int contractId});

  Future<MinboligApiResponse> getTermsAndConditions();

  Future<MinboligApiResponse?> getTermsAndConditionsV3();

  Future<MinboligApiResponse> contractSendMessage({
    required int contactId,
    required int threadId,
    required String title,
    required String message,
    required List<File> files,
  });

  Future<MinboligApiResponse> sendContractSignature(
      {required int contractId, required String signaturePng});

  Future<MinboligApiResponse> getContractHtmlPreview({required int contractId});

  Future<MinboligApiResponse> getContractInfoOnly({required int contractId});

  Future<Map<String, dynamic>> returnHeaders();

  Future<MinboligApiResponse> updateContract(
      {required Map<String, dynamic> form, required int contractId});

  Future<MinboligApiResponse> createContractWizard(
      {required Map<String, dynamic> payload});

  Future<MinboligApiResponse> getContractPdfUrl({required int contractId});

  Future<MinboligApiResponse> getPartnerCancellationContract(
      {required int contractId});

  Future<MinboligApiResponse> cancelContract({required int contractId});

  Future<MinboligApiResponse> generateCraftmanContractHTMLT(
      {required int contractId});

  Future<MinboligApiResponse> getCraftmanContractPDFDownloadT(
      {required int contractId});

  //
}
