import 'dart:convert';
import 'dart:io';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/contract/contract_service_base.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/utils/file_utils.dart';
import 'package:dio/dio.dart';

class ContractService extends ContractServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  ContractService({required this.apiService, required this.storage});

  @override
  Future<MinboligApiResponse> getContractComments({required contractId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/contract/comments/$contractId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getContractComments]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> getPartnerContracts() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/contracts',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getPartnerContracts]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> deleteContractComment(
      {required commentId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/contract/comments/$commentId/DeleteComment',
      headers: await storage.getHeaders(),
      operation: operation[Operation.deleteContractComment]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> addContractComment(
      {required contractId,
      required title,
      required comments,
      required page}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/contract/comments',
      headers: await storage.getHeaders(),
      data: {
        "contractId": contractId,
        "title": title,
        "comments": comments,
        "page": page
      },
      operation: operation[Operation.addContractComment]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> signContract({
    required contractId,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/contract/$contractId/approve',
      headers: await storage.getHeaders(),
      queryParameters: {
        'messageId': 1,
        'fileId': 1,
      },
      operation: operation[Operation.signContract]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> contractDocumentPreview(
      {required contractId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/contract/$contractId/preview/document.base64',
      headers: await storage.getHeaders(),
      operation: operation[Operation.contractDocumentPreview]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> saveAnnotation({
    required contractId,
    required documentId,
    required annotation,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/contract/$contractId/saveAnnotation',
      headers: await storage.getHeaders(),
      data: {'documentId': documentId, 'annotation': annotation},
      operation: operation[Operation.saveAnnotation]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> getContractTemplate() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/contract_templates',
      headers: await storage.getHeaders(),
      queryParameters: {"contractValue": 1000000},
      operation: operation[Operation.getContractTemplate]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse?> getContractInfoV2({
    required int contractValue,
    required int projectId,
    required int offerId,
    required int homeownerId,
    required String wholeEnterprise,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/ContractInfoV2',
      headers: await storage.getHeaders(),
      queryParameters: {
        "contractValue": contractValue,
        "projectId": projectId,
        "OfferID": offerId,
        "wholeEnterprise": wholeEnterprise,
        "companyId": await storage.getUserId(),
        "HomeownerID": homeownerId
      },
      operation: operation[Operation.getContractInfoV2]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;

    //
  }

  @override
  Future<MinboligApiResponse> getTotalFees({required int totalWithVat}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/integration/InsuranceSettings',
      headers: await storage.getHeaders(),
      data: jsonEncode({"totalpriceinclVAT": totalWithVat.toString()}),
      operation: operation[Operation.getTotalFees]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> submitContract({
    required Map<String, dynamic> form,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/contract',
      headers: await storage.getHeaders(),
      data: jsonEncode(form),
      operation: operation[Operation.submitContract]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> getContractById({required int contractId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/contract/$contractId/preview/document.base64',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getContractById]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> getTermsAndConditions() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/helper/termsAndConditionsPopupV2',
      headers: await storage.getHeaders(),
      queryParameters: {"COMPANY": (await storage.getUserId())},
      operation: operation[Operation.getTermsAndConditions]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> contractSendMessage({
    required int contactId,
    required int threadId,
    required String title,
    required String message,
    required List<File> files,
  }) async {
    //

    final data = await filesToFormData(files, 'files[]');

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path:
          '/partner/${threadId == 0 ? 'message' : 'messages/$threadId/reply'}',
      headers: await storage.getHeaders(),
      queryParameters: {
        'contactId': contactId,
        "title": title,
        "message": message,
      },
      data: data,
      operation: operation[Operation.sendMessageBitrix]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> sendContractSignature({
    required int contractId,
    required String signaturePng,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/contract/$contractId/approve',
      headers: await storage.getHeaders(),
      data: jsonEncode(
          {"signaturePng": signaturePng, "signedFrom": 'partner-app'}),
      operation: operation[Operation.sendContractSignature]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> getContractHtmlPreview(
      {required int contractId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/contract/$contractId/contractHtml',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getContractHtmlPreview]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> getContractInfoOnly(
      {required int contractId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/contract/$contractId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getContractInfoOnly]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<Map<String, dynamic>> returnHeaders() async {
    return await storage.getHeaders();
  }

  @override
  Future<MinboligApiResponse> updateContract(
      {required Map<String, dynamic> form, required int contractId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/contract/$contractId/update',
      data: jsonEncode(form),
      headers: await storage.getHeaders(),
      operation: operation[Operation.updateContract]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> createContractWizard(
      {required Map<String, dynamic> payload}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/wizard/createContract',
      data: jsonEncode(payload),
      headers: await storage.getHeaders(),
      operation: operation[Operation.createContractWizard]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> getContractPdfUrl(
      {required int contractId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/contract/$contractId/previewPdfUrl',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getContractPdfUrl]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse?> getTermsAndConditionsV3() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/helper/termsAndConditionsPopupV3',
      headers: await storage.getHeaders(),
      queryParameters: {
        "companyId": await storage.getUserId(),
        "type": "partnerContract"
      },
      operation: operation[Operation.getTermsAndConditions]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }

    //
  }

  @override
  Future<MinboligApiResponse> getPartnerCancellationContract(
      {required int contractId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/getPartnerCancellationContract',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getPartnerCancellationContract]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> cancelContract({required int contractId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/contract/$contractId/cancel',
      headers: await storage.getHeaders(),
      operation: operation[Operation.cancelContract]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> generateCraftmanContractHTMLT(
      {required int contractId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/contract/GenerateCraftmanContractHTMLT',
      headers: await storage.getHeaders(),
      operation: operation[Operation.generateCraftmanContractHTMLT]!,
      queryParameters: {
        'CONTRACT_ID': contractId,
        'INSTANT_PDF': 1,
        'RETURN_HTML': 0,
        'MINIMIZE_HTML': 0,
        'DIRTYPRINT': 0,
        'VALIDATEHTML': 0
      },
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> getCraftmanContractPDFDownloadT(
      {required int contractId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/contract/GetCraftmanContractPDFDownloadT',
      headers: await storage.getHeaders(),
      operation: operation[Operation.generateCraftmanContractHTMLT]!,
      queryParameters: {
        'CONTRACT_ID': contractId,
        'FORMAT': 'FINALCONTRACT',
      },
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  //
}
