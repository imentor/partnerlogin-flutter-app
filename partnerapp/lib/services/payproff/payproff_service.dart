import 'dart:convert';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/payproff/payproff_service_base.dart';
import 'package:dio/dio.dart';

class PayproffService extends PayproffServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  PayproffService({required this.apiService, required this.storage});

  @override
  Future<MinboligApiResponse> postPayproffAccount(
      {required Map<String, dynamic> form}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/payproff/account',
      headers: await storage.getHeaders(),
      data: jsonEncode(form),
      operation: operation[Operation.postPayproffAccount]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse?> checkPayproffAccount() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/payproff/account/exists',
      headers: await storage.getHeaders(),
      operation: operation[Operation.checkPayproffAccount]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }

    //
  }

  @override
  Future<MinboligApiResponse> updatePayproffStatus() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/payproff/updateStatus',
      headers: await storage.getHeaders(),
      data: jsonEncode({"status": "clicked"}),
      operation: operation[Operation.updatePayproffStatus]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse?> payproffVerify() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/payproff/verify',
      headers: await storage.getHeaders(),
      operation: operation[Operation.payproffVerify]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;  
    }

    //
  }

  @override
  Future<MinboligApiResponse> payproffGetWallet() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/payproff/wallet',
      headers: await storage.getHeaders(),
      operation: operation[Operation.payproffGetWallet]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> payproffUpdateWallet(
      {required String iban, required String walletId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.PUT,
      path: '/partner/payproff/wallet',
      headers: await storage.getHeaders(),
      data: jsonEncode({"iban": iban, "walletId": walletId}),
      operation: operation[Operation.payproffUpdateWallet]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> payproffValidateIban(
      {required String iban}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/helper/validateIban/$iban',
      headers: await storage.getHeaders(),
      operation: operation[Operation.payproffValidateIban]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> payproffUpdate({required String iban}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/payproff/update',
      headers: await storage.getHeaders(),
      data: {'iban': iban},
      operation: operation[Operation.payproffUpdate]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> calculateFee({required double amount}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/payproff/calculateFee',
      headers: await storage.getHeaders(),
      data: {'amount': amount},
      operation: operation[Operation.calculateFee]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  //
}
