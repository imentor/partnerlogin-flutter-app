import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class PayproffServiceBase {
  //

  Future<MinboligApiResponse> postPayproffAccount(
      {required Map<String, dynamic> form});

  Future<MinboligApiResponse?> checkPayproffAccount();

  Future<MinboligApiResponse> updatePayproffStatus();

  Future<MinboligApiResponse?> payproffVerify();

  Future<MinboligApiResponse> payproffGetWallet();

  Future<MinboligApiResponse> payproffUpdateWallet(
      {required String iban, required String walletId});

  Future<MinboligApiResponse> payproffValidateIban({required String iban});

  Future<MinboligApiResponse> payproffUpdate({required String iban});

  Future<MinboligApiResponse> calculateFee({required double amount});

  //
}
