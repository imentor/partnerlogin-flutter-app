import 'dart:convert';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/latest_job_feed_model.dart';
import 'package:Haandvaerker.dk/model/newsfeed_model.dart';
import 'package:Haandvaerker.dk/model/partner/partner_login_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/main/main_service_base.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class MainService extends MainServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  MainService({
    required this.apiService,
    required this.storage,
  });

  @override
  Future<Map<String, dynamic>> login({
    required String email,
    required String password,
    required bool? rememberMe,
  }) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: "/partner/login",
      data: {
        'email': email,
        'password': password,
      },
      operation: operation[Operation.login]!,
      cancelToken: cancelToken,
    );

    final loginData =
        PartnerLoginModel.fromJson(Map<String, dynamic>.from(response['data']));

    await storage.setData({
      StorageData.haaid: (loginData.user?.id.toString()) ?? '',
      StorageData.email: loginData.user?.email ?? '',
      StorageData.password: password,
      StorageData.language: kDebugMode ? Language.english : Language.danish,
      StorageData.bitrixToken: loginData.user?.token ?? '',
      StorageData.companyName: loginData.user?.name ?? '',
      StorageData.rememberMe: rememberMe,
      StorageData.tokenDateValidity: Formatter.addMonths(DateTime.now(), 2)
    });

    return {
      'success': true,
      'data': loginData,
    };
  }

  @override
  Future<void> setLanguage({
    required String? language,
  }) async {
    await storage.setSpecificData(StorageData.language, language);
  }

  @override
  Future<void> logout() async {
    try {
      final CancelToken cancelToken = CancelToken();
      await apiService.makeRequest(
        method: RequestMethod.POST,
        path: '/partner/logout',
        data: {'deviceToken': await storage.getFcmToken()},
        headers: await storage.getHeaders(),
        operation: operation[Operation.logout]!,
        cancelToken: cancelToken,
      );

      await storage.clearStorage();
    } catch (e) {
      await storage.clearStorage();
    }
  }

  @override
  Future<NewsFeedResponseModel?> getNewsfeed() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/NewsFeedV2',
      headers: await storage.getHeaders(),
      operation: operation[Operation.newsFeed]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return NewsFeedResponseModel.fromJson(
          Map<String, dynamic>.from(response['data']));
    } else {
      return null;
    }
  }

  @override
  Future<List<LatestJobFeedModel>?> getLatestJobfeed() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/latestJobs',
      headers: await storage.getHeaders(),
      operation: operation[Operation.latestJobFeed]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return LatestJobFeedModel.fromCollection(response['data']);
    } else {
      return null;
    }
  }

  @override
  Future<List<LatestJobFeedModel>?> getLatestJobfeedForPopup() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/latestJobs',
      headers: await storage.getHeaders(),
      queryParameters: {'showPopup': true},
      operation: operation[Operation.latestJobFeed]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return LatestJobFeedModel.fromCollection(response['data']);
    } else {
      return null;
    }
  }

  @override
  Future<MinboligApiResponse?> getDynamicStory({required String page}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/dynamicPopup',
      headers: await storage.getHeaders(),
      queryParameters: {
        'page': page,
      },
      operation: operation[Operation.dynamicStory]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }
  }

  @override
  Future<MinboligApiResponse?> getOnboardingData() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/onboarding',
      headers: await storage.getHeaders(),
      operation: operation[Operation.onboardingData]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> dynamicStorySeen({required int storyId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.PUT,
      path: '/partner/markDynamicPopupSeen/$storyId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.dynamicStorySeen]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }
  }

  @override
  Future<MinboligApiResponse?> markDynamicPopupSeen(
      {required int popupLogId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.PUT,
      path: '/partner/markDynamicPopupSeen/$popupLogId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.dynamicStorySeen]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> getSupplierInfo() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/supplier/GetSupplierProfileById/${(await storage.getUserId())}',
      operation: operation[Operation.get_supplier_info]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }
  }

  @override
  Future<MinboligApiResponse?> getSupplierInfoV2(
      {required Map<String, dynamic> payload}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/supplier/GetSupplierProfileByIdV2/${(await storage.getUserId())}',
      operation: operation[Operation.get_supplier_info]!,
      cancelToken: cancelToken,
      data: payload,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }
  }

  @override
  Future<PartnerLoginModel> changePassword({required String password}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/resetPassword',
      headers: await storage.getHeaders(),
      data: {
        'email': await storage.getEmail(),
        'password': password,
      },
      operation: operation[Operation.changePassword]!,
      cancelToken: cancelToken,
    );

    return PartnerLoginModel.fromJson(
        Map<String, dynamic>.from(response['data']));
  }

  @override
  Future<MinboligApiResponse> validateToken() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/validateToken',
      headers: await storage.getHeaders(),
      operation: operation[Operation.validateToken]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse?> getFAQs() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/GetFAQ',
      operation: operation[Operation.getFAQs]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;

    // return FAQModel.fromCollection(response['data']);
  }

  @override
  Future<MinboligApiResponse> addPartnerEmails({required String email}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/addEmail',
      headers: await storage.getHeaders(),
      data: jsonEncode({"email": email}),
      operation: operation[Operation.addPartnerEmails]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> deletePartnerEmails(
      {required int emailId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/deleteEmail',
      headers: await storage.getHeaders(),
      data: jsonEncode({"emailId": emailId}),
      operation: operation[Operation.deletePartnerEmails]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> getPartnerEmails() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/emails',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getPartnerEmails]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse?> forgotPassword({required String email}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/forgotPassword',
      data: jsonEncode({'email': email}),
      operation: operation[Operation.forgotPassword]!,
      cancelToken: cancelToken,
    );
    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<bool> checkToken() async {
    return (await storage.getHeaders()).isEmpty;
  }

  @override
  Future<String> getLanguage() async {
    return (await storage.getLanguage());
  }

  @override
  Future<Map<String, dynamic>> checkRememberMe() async {
    return (await storage.getRememberMe());
  }

  @override
  Future<MinboligApiResponse?> checkEmail({required String email}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/checkEmail',
      headers: await storage.getHeaders(),
      data: jsonEncode({'email': email}),
      operation: operation[Operation.checkEmail]!,
      cancelToken: cancelToken,
    );
    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<Map<String, dynamic>> loginCvr(
      {required String cvr,
      required String password,
      required bool? rememberMe}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: "/partner/loginV2",
      data: {
        'cvr': cvr,
        'password': password,
      },
      operation: operation[Operation.login]!,
      cancelToken: cancelToken,
    );

    final loginData =
        PartnerLoginModel.fromJson(Map<String, dynamic>.from(response['data']));

    await storage.setData({
      StorageData.haaid: (loginData.user?.id.toString()) ?? '',
      StorageData.email: loginData.user?.email ?? '',
      StorageData.password: password,
      StorageData.language: kDebugMode ? Language.english : Language.danish,
      StorageData.bitrixToken: loginData.user?.token ?? '',
      StorageData.companyName: loginData.user?.name ?? '',
      StorageData.rememberMe: rememberMe,
      StorageData.tokenDateValidity: Formatter.addMonths(DateTime.now(), 2)
    });

    return {
      'success': true,
      'data': loginData,
    };
  }

  @override
  Future<MinboligApiResponse?> notPaidSettings() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/notPaidSettings',
      headers: await storage.getHeaders(),
      operation: operation[Operation.notPaidSettings]!,
      cancelToken: cancelToken,
    );
    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<void> createTaskFromNotPaidPopup() async {
    final CancelToken cancelToken = CancelToken();
    await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/createTicket',
      headers: await storage.getHeaders(),
      data: jsonEncode({'createdFrom': 'app'}),
      operation: operation[Operation.createTaskFromNotPaidPopup]!,
      cancelToken: cancelToken,
    );
  }

  @override
  Future<MinboligApiResponse?> resetPassword(
      {required String email, required String password}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/resetPassword',
      data: {
        'email': email,
        'password': password,
      },
      operation: operation[Operation.resetPassword]!,
      cancelToken: cancelToken,
    );
    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<void> logoutFromResetPassword() async {
    if ((await storage.getHeaders()).isNotEmpty) {
      final CancelToken cancelToken = CancelToken();
      await apiService.makeRequest(
        method: RequestMethod.POST,
        path: '/partner/logout',
        data: {'deviceToken': await storage.getFcmToken()},
        headers: await storage.getHeaders(),
        operation: operation[Operation.logout]!,
        cancelToken: cancelToken,
      );
    }

    await storage.clearStorage();
  }

  @override
  Future<void> clearStorage() async {
    await storage.clearStorage();
  }

  @override
  Future<void> videoBannerTracking({required String type}) async {
    await Dio().post(
        'https://bitrixapi.haandvaerker.dk/v1/partner/tracking/newsFeedBanner',
        data: {'action': type},
        options: Options(headers: await storage.getHeaders()));
  }
}
