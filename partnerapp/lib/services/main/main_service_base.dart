import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/latest_job_feed_model.dart';
import 'package:Haandvaerker.dk/model/newsfeed_model.dart';
import 'package:Haandvaerker.dk/model/partner/partner_login_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class MainServiceBase {
  //

  Future<MinboligApiResponse?> checkEmail({
    required String email,
  });

  Future<Map<String, dynamic>> loginCvr({
    required String cvr,
    required String password,
    required bool? rememberMe,
  });

  Future<Map<String, dynamic>> login({
    required String email,
    required String password,
    required bool? rememberMe,
  });

  Future<void> setLanguage({required String? language});

  Future<String> getLanguage();

  Future<void> logout();

  Future<NewsFeedResponseModel?> getNewsfeed();

  Future<List<LatestJobFeedModel>?> getLatestJobfeed();

  Future<List<LatestJobFeedModel>?> getLatestJobfeedForPopup();

  Future<MinboligApiResponse?> getDynamicStory({required String page});

  Future<MinboligApiResponse?> getOnboardingData();

  Future<MinboligApiResponse?> dynamicStorySeen({required int storyId});

  Future<MinboligApiResponse?> markDynamicPopupSeen({required int popupLogId});

  Future<MinboligApiResponse?> getSupplierInfo();

  Future<MinboligApiResponse?> getSupplierInfoV2(
      {required Map<String, dynamic> payload});

  Future<PartnerLoginModel> changePassword({required String password});

  Future<MinboligApiResponse> validateToken();

  Future<MinboligApiResponse?> getFAQs();

  Future<MinboligApiResponse> getPartnerEmails();

  Future<MinboligApiResponse> addPartnerEmails({required String email});

  Future<MinboligApiResponse> deletePartnerEmails({required int emailId});

  Future<MinboligApiResponse?> forgotPassword({required String email});

  Future<bool> checkToken();

  Future<Map<String, dynamic>> checkRememberMe();

  Future<MinboligApiResponse?> notPaidSettings();

  Future<void> createTaskFromNotPaidPopup();

  Future<MinboligApiResponse?> resetPassword(
      {required String email, required String password});

  Future<void> logoutFromResetPassword();

  Future<void> clearStorage();

  Future<void> videoBannerTracking({required String type});
}
