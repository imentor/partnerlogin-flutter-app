import 'package:Haandvaerker.dk/model/customer_minbolig/old_jobs_model.dart';
import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/invitations/subcontractor_invitation_model.dart';
import 'package:Haandvaerker.dk/model/kanikke_popups_model.dart';
import 'package:Haandvaerker.dk/model/kanikke_settings_response_model.dart';
import 'package:Haandvaerker.dk/model/offer_model.dart';
import 'package:Haandvaerker.dk/model/tilbud_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

@immutable
abstract class ClientServiceBase {
  //

  Future<PaginatedPartnerJobsModel?> getPartnerJobsV2(
      {required Map<String, dynamic> queryParameters});

  Future<List<OfferModel>?> getOffers();

  Future<List<OldPartnerJobsModel>?> getOldPartnerJobs();

  Future<PartnerJobModel?> getSelectedJobFromNewsFeed(
      {required int jobIdFromNewsFeed});

  Future<MinboligApiResponse> addOffer({
    required int projectId,
    required String title,
    required String? description,
    required subtotalPrice,
    required vat,
    required totalPrice,
    required List<Rows> tasks,
  });

  Future<MinboligApiResponse?> getOfferHistory(
      {required int page, required int year});

  Future<List?> getArchivedOldJobs();

  Future<KanIkkeSettingsResponseModel?> getKanIkkeSettings();

  Future<KanIkkePopupsModel?> getKanIkkePopups();

  Future<MinboligApiResponse?> getAffiliateJobs();

  Future<MinboligApiResponse?> addAffiliateJobNotes(
      {required FormData payload});

  Future<MinboligApiResponse?> acceptAffiliateJob(
      {required String affiliateProjectId});

  Future<List<SubcontractorInvitation>?> getSubcontractorInvitations();

  //
}
