import 'dart:convert';

import 'package:Haandvaerker.dk/model/customer_minbolig/old_jobs_model.dart';
import 'package:Haandvaerker.dk/model/customer_minbolig/partner_jobs_model.dart';
import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/invitations/subcontractor_invitation_model.dart';
import 'package:Haandvaerker.dk/model/kanikke_popups_model.dart';
import 'package:Haandvaerker.dk/model/kanikke_settings_response_model.dart';
import 'package:Haandvaerker.dk/model/offer_model.dart';
import 'package:Haandvaerker.dk/model/tilbud_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/client/client_service_base.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:dio/dio.dart';

class ClientService extends ClientServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  ClientService({required this.apiService, required this.storage});

  @override
  Future<List<OldPartnerJobsModel>?> getOldPartnerJobs() async {
    //
    late CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/oldJobs',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getOldPartnerJobs]!,
      cancelToken: cancelToken,
    );
    if (response != null) {
      return OldPartnerJobsModel.fromCollection(
          List<dynamic>.from(response['data']));
    } else {
      return null;
    }
    //
  }

  @override
  Future<PaginatedPartnerJobsModel?> getPartnerJobsV2(
      {required Map<String, dynamic> queryParameters}) async {
    //

    late CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/jobsV4',
      headers: await storage.getHeaders(),
      queryParameters: queryParameters,
      operation: operation[Operation.get_partner_jobsV2]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return PaginatedPartnerJobsModel.fromJson(
          Map<String, dynamic>.from(response['data']));
    } else {
      return null;
    }

    //
  }

  @override
  Future<PartnerJobModel?> getSelectedJobFromNewsFeed(
      {required int jobIdFromNewsFeed}) async {
    //

    late CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/job/$jobIdFromNewsFeed',
      headers: await storage.getHeaders(),
      operation: operation[Operation.get_selected_job_from_newsfeed]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return PartnerJobModel.fromJson(response['data']);
    } else {
      return null;
    }

    //
  }

  @override
  Future<List<OfferModel>?> getOffers() async {
    //

    late CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/offers',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getOffers]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return OfferModel.fromCollection(response['data'] as List<dynamic>);
    } else {
      return null;
    }

    //
  }

  @override
  Future<MinboligApiResponse> addOffer({
    required int? projectId,
    required String title,
    required String? description,
    required subtotalPrice,
    required vat,
    required totalPrice,
    required List<Rows?> tasks,
  }) async {
    //

    final data = {
      "projectId": projectId,
      "offers": [
        {
          "title": title,
          "description": description,
          "subtotalPrice": subtotalPrice,
          "vat": vat,
          "totalPrice": totalPrice,
          "tasks": tasks.map((e) {
            String type = '';
            switch (e!.type!.toInt()) {
              case 0:
                type = "Quantity";
                break;
              case 1:
                type = "Amount";
                break;
              case 2:
                type = "Hours";
                break;
              default:
                type = "Quantity";
                break;
            }
            return {
              "title": e.customName,
              "description": e.customName,
              "unit": type,
              "value": (e.unitPrice! * e.quantity).toInt(),
              "quantity": e.quantity.toInt()
            };
          }).toList()
        }
      ]
    };

    late CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/offer',
      headers: await storage.getHeaders(),
      data: data,
      operation: operation[Operation.addOffers]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse?> getOfferHistory(
      {required int page, required int year}) async {
    //

    late CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/offersV2',
      headers: await storage.getHeaders(),
      queryParameters: {'page': page, 'size': 20, 'year': year},
      operation: operation[Operation.getOfferHistory]!,
      cancelToken: cancelToken,
    );
    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }

    //
  }

  @override
  Future<List?> getArchivedOldJobs() async {
    late CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/archivedOldJobs',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getArchivedOldJobs]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return response['data'] ?? [];
    } else {
      return null;
    }
  }

  @override
  Future<KanIkkeSettingsResponseModel?> getKanIkkeSettings() async {
    late CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/kanIkkeSettings',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getKanIkkeSettings]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return KanIkkeSettingsResponseModel.fromJson(response['data']);
    } else {
      return null;
    }
  }

  @override
  Future<KanIkkePopupsModel?> getKanIkkePopups() async {
    late CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/kanIkkePopups',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getKanIkkePopups]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return KanIkkePopupsModel.fromJson(response['data']);
    } else {
      return null;
    }
  }

  @override
  Future<MinboligApiResponse?> getAffiliateJobs() async {
    late CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/affiliateJobs',
      headers: await storage.getHeaders(),
      queryParameters: {
        'size': 100,
        'page': 1,
      },
      operation: operation[Operation.getAffiliateJobs]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> addAffiliateJobNotes(
      {required FormData payload}) async {
    late CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/affiliate/addNotes',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.getAffiliateJobs]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> acceptAffiliateJob(
      {required String affiliateProjectId}) async {
    late CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/affiliate/updateJobs',
      headers: await storage.getHeaders(),
      data: jsonEncode(
          {'affiliateProjectId': int.parse(affiliateProjectId), 'accept': 1}),
      operation: operation[Operation.getAffiliateJobs]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<List<SubcontractorInvitation>?> getSubcontractorInvitations() async {
    late CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/subContractor/invitations',
      headers: await storage.getHeaders(),
      queryParameters: {'size': 100},
      operation: operation[Operation.getSubcontractorInvitations]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      final minboligResponse = MinboligApiResponse.fromJson(response);

      final companyId = await storage.getUserId();

      List<SubcontractorInvitation> invites =
          SubcontractorInvitation.fromCollection(
              minboligResponse.data['items']);

      return [
        ...invites
            .where((value) => (value.company ?? 0).toString() != companyId)
      ];
    }

    return [];
  }
}
