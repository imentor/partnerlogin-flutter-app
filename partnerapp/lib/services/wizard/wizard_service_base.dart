import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

@immutable
abstract class WizarServiceBase {
  //

  Future<List<FullIndustry>?> getFullIndustry();

  Future<List<EnterpriseIndustry>?> getListPriceAi(
      {required int parentProjectId, required int contactId});

  Future<List<AllWizardText>?> getAllWizardTexts();

  Future<List<AllWizardText>?> getPartnerWizardTexts();

  Future<bool> updateJobStatus({required int jobId, required int statusId});

  Future<MinboligApiResponse> addNote({required FormData payload});

  Future<MinboligApiResponse> deleteNote(
      {required int jobId, required int noteId});

  Future<MinboligApiResponse> cancelJob(
      {required Map<String, dynamic> payload});

  Future<MinboligApiResponse> jobUpdate(
      {required int jobId, required Map<String, dynamic> payload});

  Future<MinboligApiResponse> archiveOldJob(
      {required int jobId, required int contactId});

  Future<MinboligApiResponse?> getListPriceMinbolig(
      {required int contactId, required int projectId});

  //
}
