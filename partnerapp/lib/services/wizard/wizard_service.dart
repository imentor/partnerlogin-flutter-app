import 'dart:convert';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/wizard/wizard_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/wizard/wizard_service_base.dart';
import 'package:dio/dio.dart';

class WizardService extends WizarServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  WizardService({required this.apiService, required this.storage});

  @override
  Future<List<FullIndustry>?> getFullIndustry() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/industries',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getFullIndustry]!,
      cancelToken: cancelToken,
    );

    return response != null
        ? FullIndustry.fromCollection(response['data'] as List<dynamic>)
        : null;
  }

  @override
  Future<List<EnterpriseIndustry>?> getListPriceAi(
      {required int parentProjectId, required int contactId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/getListPricePartnerAi',
      headers: await storage.getHeaders(),
      queryParameters: {
        'project_id': parentProjectId,
        'contact_id': contactId,
      },
      operation: operation[Operation.getListPriceAi]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      if (response['data'] is List) {
        return List.empty();
      } else {
        final data = response['data'] as Map<String, dynamic>;
        final enterpriseKeys = data.keys;
        final List<EnterpriseIndustry> list = [];
        for (final key in enterpriseKeys) {
          final sub = data[key] as Map<String, dynamic>;
          final subKeys = sub.keys;
          List<JobIndustry> jobIndustries = [];
          for (final subKey in subKeys) {
            jobIndustries.add(JobIndustry(
              jobIndustryId: subKey,
              descriptions: sub[subKey],
            ));
          }
          list.add(EnterpriseIndustry(
            enterpriseId: key,
            jobIndustries: jobIndustries,
          ));
        }
        return list;
      }
    } else {
      return null;
    }

    //
  }

  @override
  Future<List<AllWizardText>?> getAllWizardTexts() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/all',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getAllWizardTexts]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return AllWizardText.fromCollection(response['data'] as List<dynamic>);
    } else {
      return null;
    }

    //
  }

  @override
  Future<List<AllWizardText>?> getPartnerWizardTexts() async {
    //
    // Wizard Text - replaces partner/wizard/all API call
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/productsV2',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getPartnerWizardTexts]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return AllWizardText.fromCollection(response['data'] as List<dynamic>);
    } else {
      return null;
    }

    //
  }

  @override
  Future<bool> updateJobStatus(
      {required int jobId, required int statusId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/updateJobStatus',
      headers: await storage.getHeaders(),
      data: {
        'jobId': jobId,
        'statusId': statusId,
      },
      operation: operation[Operation.updateJobStatus]!,
      cancelToken: cancelToken,
    );

    return response['success'];

    //
  }

  @override
  Future<MinboligApiResponse> addNote({
    required FormData payload,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/job/addNotes',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.addNote]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> deleteNote({
    required int jobId,
    required int noteId,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/job/$jobId/deleteNotes/$noteId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.deleteNote]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> cancelJob({
    required Map<String, dynamic> payload,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/job/cancel',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.cancelJob]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> jobUpdate(
      {required int jobId, required Map<String, dynamic> payload}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/job/$jobId/update',
      headers: await storage.getHeaders(),
      queryParameters: payload,
      operation: operation[Operation.jobUpdate]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> archiveOldJob(
      {required int jobId, required int contactId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/archiveOldJobs',
      headers: await storage.getHeaders(),
      data: jsonEncode({
        "jobId": jobId,
        "contactId": contactId,
        "partnerId": await storage.getUserId()
      }),
      operation: operation[Operation.archiveOldJob]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  //

  @override
  Future<MinboligApiResponse?> getListPriceMinbolig(
      {required int contactId, required int projectId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/getListPriceMinboligV2',
      headers: await storage.getHeaders(),
      queryParameters: {
        "contact_id": contactId,
        "project_id": projectId,
        "calculation_id": 0,
      },
      operation: operation[Operation.getListPriceMinbolig]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }
}
