import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/marketplace/check_reservation_model.dart';
import 'package:Haandvaerker.dk/model/marketplace/marketplace_v2_response_model.dart';
import 'package:Haandvaerker.dk/model/marketplace/udbud_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class MarketPlaceServiceBase {
  //

  Future<MarketPlaceV2ResponseModel?> marketplaceV2(
      {required Map<String, dynamic>? payload});

  Future<int?> getSupplierInfoIndustry();

  Future<Map<String, dynamic>> reserveJob(
      {required int jobId, required bool toBuy, required int mesterId});

  Future<void> jobsView({required int projectId});

  Future<UdbudResponseModel> getUdbudJobs();

  Future<MinboligApiResponse> projectsMarketplace();

  Future<CheckBitrixReservationResponseModel?> checkReserveMore(
      {required int projectId});

  Future<bool> marketplaceViewVideo();

  Future<MinboligApiResponse?> getMarketplaceFilters();

  Future<MinboligApiResponse?> saveMarketplaceFilters({required String filter});

  Future<MinboligApiResponse?> removeMarketplaceFilter({required int filterId});

  Future<void> emptyMarketplaceNotification();

  Future<MinboligApiResponse?> checkPackage();

  Future<void> jobsSeen({required List<int> projectIds});
  //
}
