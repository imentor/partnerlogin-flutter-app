import 'dart:convert';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/marketplace/check_reservation_model.dart';
import 'package:Haandvaerker.dk/model/marketplace/marketplace_v2_response_model.dart';
import 'package:Haandvaerker.dk/model/marketplace/reserve_response_model.dart';
import 'package:Haandvaerker.dk/model/marketplace/udbud_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/marketplace/marketplace/marketplace_service_base.dart';
import 'package:dio/dio.dart';

class MarketPlaceService extends MarketPlaceServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  MarketPlaceService({required this.apiService, required this.storage});

  @override
  Future<MarketPlaceV2ResponseModel?> marketplaceV2(
      {required Map<String, dynamic>? payload}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/projects/marketplaceV2',
      headers: await storage.getHeaders(),
      queryParameters: payload,
      operation: operation[Operation.marketplaceV2]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MarketPlaceV2ResponseModel.fromJson(
          Map<String, dynamic>.from(response['data']));
    } else {
      return null;
    }

    //
  }

  @override
  Future<int?> getSupplierInfoIndustry() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/supplier/getSupplierInfo/${(await storage.getUserId())}',
      operation: operation[Operation.getSupplierInfoIndustry]!,
      cancelToken: cancelToken,
    );

    return Map<String, dynamic>.from(
        response['data'])['UF_CRM_COMPANY_ENTERPRISE'] as int?;

    //
  }

  @override
  Future<Map<String, dynamic>> reserveJob(
      {required int jobId, required bool toBuy, required int mesterId}) async {
    //

    Map<String, dynamic> data = {};

    if (mesterId != 0) {
      data['jobType'] = 'mester';
      data['mesterId'] = mesterId;
    } else {
      data['jobType'] = 'marketplace';
    }

    if (toBuy) {
      data['partnerId'] = (await storage.getUserId());
    }

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: !toBuy
          ? '/partner/project/$jobId/reserve'
          : '/partner/project/$jobId/marketplace/reserveMore',
      headers: await storage.getHeaders(),
      data: data,
      operation: operation[Operation.reserve_job]!,
      cancelToken: cancelToken,
    );

    final reserved = response['data'] is bool
        ? response['data']
        : ReserveResponseModel.fromJson(
            Map<String, dynamic>.from(response['data']));

    return {
      'message': Map<String, dynamic>.from(response)['message'],
      'data': reserved,
      'success': Map<String, dynamic>.from(response)['success'],
    };

    //
  }

  @override
  Future<void> jobsView({required int projectId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/jobsView',
      headers: await storage.getHeaders(),
      data: {'projectId': projectId},
      operation: operation[Operation.jobsView]!,
      cancelToken: cancelToken,
    );

    //
  }

  @override
  Future<UdbudResponseModel> getUdbudJobs() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/bitrix/getUdbudJobs',
      operation: operation[Operation.getUdbudJobs]!,
      cancelToken: cancelToken,
    );

    return UdbudResponseModel.fromJson(
        Map<String, dynamic>.from(response['data']));

    //
  }

  @override
  Future<MinboligApiResponse> projectsMarketplace() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/projects/marketplace',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getProjectMarketplace]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<CheckBitrixReservationResponseModel?> checkReserveMore({
    required int projectId,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/project/$projectId/marketplace/checkReserveMore',
      headers: await storage.getHeaders(),
      data: {'partnerId': (await storage.getUserId())},
      operation: operation[Operation.checkReserveMore]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      if (response['data'] != null) {
        final responseData = response['data'];
        if (responseData['price'].runtimeType == String) {
          responseData['price'] = double.parse(responseData['price']);
        }

        return CheckBitrixReservationResponseModel.fromJson(
            Map<String, dynamic>.from(responseData));
      } else {
        return null;
      }
    } else {
      return null;
    }

    //
  }

  @override
  Future<bool> marketplaceViewVideo() async {
    ///

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/projects/marketplace/video',
      headers: await storage.getHeaders(),
      operation: operation[Operation.viewMarketPlaceVideo]!,
      cancelToken: cancelToken,
    );

    return response['success'];

    ///
  }

  @override
  Future<MinboligApiResponse?> getMarketplaceFilters() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/projects/marketplaceV2/filters',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getMarketplaceFilters]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }
  }

  @override
  Future<MinboligApiResponse?> saveMarketplaceFilters(
      {required String filter}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/projects/marketplaceV2/saveFilter',
      headers: await storage.getHeaders(),
      data: jsonEncode({"filter": filter}),
      operation: operation[Operation.saveMarketplaceFilters]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }
  }

  @override
  Future<MinboligApiResponse?> removeMarketplaceFilter(
      {required int filterId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/projects/marketplaceV2/removeFilter',
      headers: await storage.getHeaders(),
      data: jsonEncode({"filterId": filterId}),
      operation: operation[Operation.removeMarketplaceFilter]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }
  }

  @override
  Future<void> emptyMarketplaceNotification() async {
    final CancelToken cancelToken = CancelToken();
    await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/projects/marketplaceV2/emptyNotification',
      headers: await storage.getHeaders(),
      operation: operation[Operation.emptyMarketplaceNotification]!,
      cancelToken: cancelToken,
    );
  }

  @override
  Future<MinboligApiResponse?> checkPackage() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: "/bitrix/getPackage/${(await storage.getUserId())}",
      operation: operation[Operation.getPackages]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<void> jobsSeen({required List<int> projectIds}) async {
    final CancelToken cancelToken = CancelToken();

    await apiService.makeRequest(
      method: RequestMethod.POST,
      path: "/partner/jobsSeen",
      headers: await storage.getHeaders(),
      operation: operation[Operation.jobsSeen]!,
      data: jsonEncode({'projectIds': projectIds, 'seenFrom': 'app'}),
      cancelToken: cancelToken,
    );
  }

  //
}
