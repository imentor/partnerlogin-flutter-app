import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class JobInvitationServiceBase {
  //

  Future<MinboligApiResponse?> getInvitationPagination(
      {required int page, required String status});

  Future<Map<String, dynamic>> acceptDeclineInvitation(
      {required int invitationId, required int isAccept});

  //
}
