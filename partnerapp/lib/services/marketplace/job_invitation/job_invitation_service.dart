import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/marketplace/job_invitation/job_invitation_service_base.dart';
import 'package:dio/dio.dart';

class JobInvitationService extends JobInvitationServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  JobInvitationService({required this.apiService, required this.storage});

  @override
  Future<Map<String, dynamic>> acceptDeclineInvitation({
    required int invitationId,
    required int isAccept,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/projectInvitation/$invitationId',
      headers: await storage.getHeaders(),
      data: {'accepted': isAccept},
      operation: operation[Operation.acceptDeclineProjectInvitation]!,
      cancelToken: cancelToken,
    );

    return {
      'message': Map<String, dynamic>.from(response)['message'],
      'success': Map<String, dynamic>.from(response)['success'],
    };

    //
  }

  @override
  Future<MinboligApiResponse?> getInvitationPagination(
      {required int page, required String status}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/projectInvitationsV2',
      headers: await storage.getHeaders(),
      queryParameters: {'page': page, 'size': 20, 'status': status},
      operation: operation[Operation.getProjectInvitations]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }
  }

  //
}
