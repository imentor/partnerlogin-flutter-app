import 'package:Haandvaerker.dk/model/settings/order_management_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class IntegrationServiceBase {
  //

  Future<List<OrderManagementModel>> getOrderManagement();

  Future<OrderManagementModel> addOrderManagement({required String apiKey});

  //
}
