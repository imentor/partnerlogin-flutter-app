import 'package:Haandvaerker.dk/model/settings/order_management_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/integration/integration_service_base.dart';
import 'package:dio/dio.dart';

class IntegrationService extends IntegrationServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  IntegrationService({required this.storage, required this.apiService});

  @override
  Future<List<OrderManagementModel>> getOrderManagement() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/ordrestyring',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getOrderManagement]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      if (response['data'] != null || (response['data'] as List).isNotEmpty) {
        return OrderManagementModel.fromCollection(
            List<dynamic>.from(response['data']));
      } else {
        return <OrderManagementModel>[];
      }
    } else {
      return <OrderManagementModel>[];
    }

    //
  }

  @override
  Future<OrderManagementModel> addOrderManagement(
      {required String apiKey}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/addOrdrestyring',
      headers: await storage.getHeaders(),
      data: {'apiKey': apiKey},
      operation: operation[Operation.addOrderManagement]!,
      cancelToken: cancelToken,
    );

    return OrderManagementModel.fromJson(
        Map<String, dynamic>.from(response['data']));

    //
  }

  //
}
