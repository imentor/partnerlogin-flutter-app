import 'package:Haandvaerker.dk/model/partner/partner_login_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class HelperServiceBase {
  Future<Map<String, dynamic>> decodeAndValidateCode({required String code});
  Future<PartnerLoginModel> validateParamsToken({required String token});

  String? previousInitialAppLink();
  void setinitialAppLink({required String link});
}
