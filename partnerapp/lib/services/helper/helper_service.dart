import 'package:Haandvaerker.dk/model/partner/partner_login_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/helper/helper_service_base.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/formatter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class HelperService extends HelperServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  HelperService({required this.apiService, required this.storage});

  @override
  Future<Map<String, dynamic>> decodeAndValidateCode(
      {required String code}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/helper/decodeCompanyId/$code',
      operation: operation[Operation.decodeAndValidateCode]!,
      cancelToken: cancelToken,
    );

    return {
      "response": response != null ? response['data'] : null,
      "userId": (await storage.getUserId())
    };
  }

  @override
  Future<PartnerLoginModel> validateParamsToken({required String token}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/validateToken',
      headers: {"Authorization": "Bearer $token"},
      operation: operation[Operation.validateParamsToken]!,
      cancelToken: cancelToken,
    );

    final loginData =
        PartnerLoginModel.fromJson(Map<String, dynamic>.from(response['data']));

    await storage.setData({
      StorageData.haaid: (loginData.user?.id.toString()) ?? '',
      StorageData.email: loginData.user?.email ?? '',
      StorageData.language: kDebugMode ? Language.english : Language.danish,
      StorageData.bitrixToken: token,
      StorageData.companyName: loginData.user?.name ?? '',
      StorageData.tokenDateValidity: Formatter.addMonths(DateTime.now(), 2)
    });

    return loginData;
  }

  @override
  String? previousInitialAppLink() {
    return storage.getInitialAppLink();
  }

  @override
  void setinitialAppLink({required String link}) {
    storage.setSharedData({SharedData.initialAppLink: link});
  }
}
