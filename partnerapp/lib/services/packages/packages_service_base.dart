import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/packages/package_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class PackagesServiceBase {
  //

  Future<PackageResponseModel?> getPackages();

  Future<GenericResponseModel> requestFeature(
      {required String shortCode, required String packageHandle});

  //
}
