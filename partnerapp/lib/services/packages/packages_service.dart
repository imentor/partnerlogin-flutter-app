import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/packages/package_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/packages/packages_service_base.dart';
import 'package:dio/dio.dart';

class PackagesService extends PackagesServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  PackagesService({required this.apiService, required this.storage});

  @override
  Future<PackageResponseModel?> getPackages() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: "/bitrix/getPackage/${(await storage.getUserId())}",
      operation: operation[Operation.getPackages]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      final packageData =
          PackageData.fromJson(Map<String, dynamic>.from(response['data']));
      return PackageResponseModel(
        success: response['success'],
        message: response['message'],
        data: packageData,
      );
    } else {
      return null;
    }
  }

  @override
  Future<GenericResponseModel> requestFeature({
    required String shortCode,
    required String packageHandle,
  }) async {
    //

    final CancelToken cancelTokenPartner = CancelToken();
    final partner = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/supplier/GetSupplierProfileById/${(await storage.getUserId())}',
      operation: operation[Operation.get_supplier_info]!,
      cancelToken: cancelTokenPartner,
    );

    final email = partner['data']['b_crm_company']['EMAIL'];
    final phone = partner['data']['b_crm_company']['MOBILE'];

    final data = {
      "company_id": (await storage.getUserId()),
      "company_name": (await storage.getCompanyName()),
      "name": (await storage.getCompanyName()),
      "email": email,
      "phone": phone,
      "features": [shortCode],
      "package_name": packageHandle,
      "requestFrom": 'Mobile',
    };

    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: "/bitrix/requestBitrixFeatures",
      data: data,
      operation: operation[Operation.requestFeatures]!,
      cancelToken: cancelToken,
    );

    return GenericResponseModel(
      success: response['success'],
      message: response['message'],
    );

    //
  }

  //
}
