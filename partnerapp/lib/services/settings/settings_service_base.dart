import 'dart:io';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/partner/supplier_info_file_model.dart';
import 'package:Haandvaerker.dk/model/settings/master_info_model.dart';
import 'package:Haandvaerker.dk/model/settings/phone_number_model.dart';
import 'package:Haandvaerker.dk/model/website_setting_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

@immutable
abstract class SettingsServiceBase {
  //

  Future<MinboligApiResponse?> getTaskTypesFromCRM();

  Future<MinboligApiResponse?> getTaskTypesFromCRMnoId();

  Future<MinboligApiResponse?> savePartnerTaskProfile({
    required List<dynamic> categories,
    required List<Sort> sort,
    required List<dynamic>? taskTypes,
  });

  Future<SupplierInfoFileModel> uploadProfileVideo({required File video});

  Future<bool> deleteProfileVideo({required int videoId});

  Future<List<PhoneNumberModel>> getPhoneNumbers();

  Future<PhoneNumberModel> addPhoneNumber({required String phoneNumber});

  Future<bool> deletePhoneNumber({required int phoneId});

  Future<MinboligApiResponse?> getAllRegions();

  Future<List<int>> getRegions();

  Future<MinboligApiResponse> editRegions({required List<int> regionIds});

  Future<MinboligApiResponse> updateSupplierInfoCustomFields(
      {required FormData fields});

  Future<MinboligApiResponse> getHolidays();

  Future<MinboligApiResponse> addHoliday(
      {required String from, required String to});

  Future<MinboligApiResponse> deleteHoliday({required int holidayId});

  Future<MinboligApiResponse> updateSupplierInfo(
      {required Map<String, dynamic> payload});

  Future<bool> deleteProfilePicture({required int pictureId});

  Future<List<SupplierInfoFileModel>> uploadProfilePicture(
      {required List<File> files});

  Future<bool> updateProfilePictureSorting({required Map<String, int> sorting});

  Future<SupplierInfoFileModel> editProfilePicture(
      {required int pictureId, required File picture});

  Future<bool> downloadPdf(
      {required String fileName, required String fileLink});

  Future<MinboligApiResponse> messageCs({required String message});

  Future<String> cancelSubscriptionPartner({required String description});

  Future<List<MasterInfoGeneralModel>> getMasterInfo();

  Future<bool> saveMasterInfo({required Map<String, dynamic> payload});

  Future<MinboligApiResponse?> getDenmarkCities();

  Future<MinboligApiResponse?> updateSupplier(
      {required Map<String, dynamic> payload});

  Future<MinboligApiResponse?> getCustomers({required String query});

  Future<MinboligApiResponse?> getCustomersProjects({required int contactId});

  //
}
