import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:math' as math;

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/partner/supplier_info_file_model.dart';
import 'package:Haandvaerker.dk/model/settings/master_info_model.dart';
import 'package:Haandvaerker.dk/model/settings/phone_number_model.dart';
import 'package:Haandvaerker.dk/model/settings/regions_model.dart';
import 'package:Haandvaerker.dk/model/website_setting_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/settings/settings_service_base.dart';
import 'package:dio/dio.dart';
import 'package:path_provider/path_provider.dart';

class SettingsService extends SettingsServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  SettingsService({required this.apiService, required this.storage});

  @override
  Future<MinboligApiResponse?> getTaskTypesFromCRM() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/industries/full/${(await storage.getUserId())}',
      operation: operation[Operation.getTaskTypesFromCRM]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> getTaskTypesFromCRMnoId() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/industries/full/',
      operation: operation[Operation.getTaskTypesFromCRM]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> savePartnerTaskProfile({
    required List<dynamic> categories,
    required List<Sort> sort,
    required List<dynamic>? taskTypes,
  }) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/task/savePartnerTaskProfile',
      data: FormData.fromMap({
        'category[]': categories,
        'partnerId': await storage.getUserId(),
        'sort': jsonEncode(sort.map((e) => {'id': e.id}).toList()),
        'taskTypes': jsonEncode(taskTypes),
      }),
      operation: operation[Operation.savePartnerTaskProfile]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<SupplierInfoFileModel> uploadProfileVideo(
      {required File video}) async {
    //

    final payload = FormData.fromMap({
      'video': await MultipartFile.fromFile(
        video.path,
        filename: video.path.split('/').last,
      ),
    });
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/uploadVideo',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.uploadProfileVideo]!,
      cancelToken: cancelToken,
    );

    return SupplierInfoFileModel.fromJson(
        Map<String, dynamic>.from(response['data']));

    //
  }

  @override
  Future<bool> deleteProfileVideo({required int videoId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/deleteVideo',
      headers: await storage.getHeaders(),
      data: {'videoId': videoId},
      operation: operation[Operation.deleteProfileVideo]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response).success!;

    //
  }

  @override
  Future<List<PhoneNumberModel>> getPhoneNumbers() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/phones',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getPhoneNumbers]!,
      cancelToken: cancelToken,
    );

    return PhoneNumberModel.fromCollection(
        List<dynamic>.from(response['data']));

    //
  }

  @override
  Future<PhoneNumberModel> addPhoneNumber({required String phoneNumber}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/addPhone',
      headers: await storage.getHeaders(),
      data: {'phone': phoneNumber},
      operation: operation[Operation.addPhoneNumber]!,
      cancelToken: cancelToken,
    );

    return PhoneNumberModel.fromJson(
        Map<String, dynamic>.from(response['data']));

    //
  }

  @override
  Future<bool> deletePhoneNumber({required int phoneId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/deletePhone',
      headers: await storage.getHeaders(),
      data: {'phoneId': phoneId},
      operation: operation[Operation.deletePhoneNumber]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response).success!;

    //
  }

  @override
  Future<MinboligApiResponse?> getAllRegions() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/regions/all',
      headers: await storage.getHeaders(),
      operation: operation[Operation.get_all_regions]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;

    //
  }

  @override
  Future<List<int>> getRegions() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/regions',
      headers: await storage.getHeaders(),
      operation: operation[Operation.get_regions]!,
      cancelToken: cancelToken,
    );

    return RegionModel.fromCollection(List<dynamic>.from(response['data']))
        .map((e) => e.regionId!)
        .toList();

    //
  }

  @override
  Future<MinboligApiResponse> editRegions(
      {required List<int> regionIds}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/region/edit',
      headers: await storage.getHeaders(),
      data: {'regionIds': regionIds},
      operation: operation[Operation.edit_regions]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> updateSupplierInfoCustomFields(
      {required FormData fields}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path:
          '/supplier/updateSupplierCustomFields/${(await storage.getUserId())}',
      headers: await storage.getHeaders(),
      data: fields,
      operation: operation[Operation.updateSupplierCustomFields]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> getHolidays() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/holidays',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getHolidays]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> addHoliday({
    required String from,
    required String to,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/addHoliday',
      headers: await storage.getHeaders(),
      data: {'from': from, 'to': to},
      operation: operation[Operation.addHoliday]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> deleteHoliday({
    required int holidayId,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/deleteHoliday',
      headers: await storage.getHeaders(),
      data: {'holidayId': holidayId},
      operation: operation[Operation.deleteHoliday]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> updateSupplierInfo(
      {required Map<String, dynamic> payload}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/update',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.updateSupplierInfo]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<bool> deleteProfilePicture({required int pictureId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/deletePicture',
      headers: await storage.getHeaders(),
      data: {'pictureId': pictureId},
      operation: operation[Operation.deleteProfilePicture]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response).success!;

    //
  }

  @override
  Future<List<SupplierInfoFileModel>> uploadProfilePicture(
      {required List<File> files}) async {
    //

    final payload = <String, dynamic>{};

    for (int i = 0; i < files.length; i++) {
      payload["pictures[$i]"] = await MultipartFile.fromFile(
        files[i].path,
        filename: files[i].path.split('/').last,
      );
    }
    payload["type"] = "Gallery";

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/uploadPicture',
      headers: await storage.getHeaders(),
      data: FormData.fromMap(payload),
      operation: operation[Operation.uploadProfilePicture]!,
      cancelToken: cancelToken,
    );

    return SupplierInfoFileModel.fromCollection(
        List<dynamic>.from(response['data']));

    //
  }

  @override
  Future<bool> updateProfilePictureSorting(
      {required Map<String, int> sorting}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/updatePictureSorting',
      headers: await storage.getHeaders(),
      data: {'sort': sorting},
      operation: operation[Operation.updateProfilePictureSorting]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response).success!;

    //
  }

  @override
  Future<SupplierInfoFileModel> editProfilePicture({
    required int pictureId,
    required File picture,
  }) async {
    //

    final payload = FormData.fromMap({
      'pictureId': pictureId,
      'picture': await MultipartFile.fromFile(
        picture.path,
        filename: picture.path.split('/').last,
      ),
    });
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/editPicture',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.editProfilePicture]!,
      cancelToken: cancelToken,
    );

    return SupplierInfoFileModel.fromJson(
        Map<String, dynamic>.from(response['data']));

    //
  }

  @override
  Future<bool> downloadPdf(
      {required String fileName, required String fileLink}) async {
    //

    try {
      Directory? directory;
      if (Platform.isIOS) {
        directory = await getApplicationDocumentsDirectory();
      } else {
        directory = Directory('/storage/emulated/0/Download');
        if (!await directory.exists()) {
          directory = await getExternalStorageDirectory();
        }
      }
      final randid = math.Random().nextInt(10000);

      final filePath = '${directory!.path}/Minbolig/${randid}_$fileName';
      final response = await Dio().download(fileLink, filePath);
      if (response.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      log("message downloadPdf $e");
      return false;
    }

    //
  }

  @override
  Future<MinboligApiResponse> messageCs({required String message}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/sendEmailRequestToCs',
      headers: await storage.getHeaders(),
      data: jsonEncode({'message': message}),
      operation: operation[Operation.messageCs]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<String> cancelSubscriptionPartner(
      {required String description}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/settings/cancelSubscriptionPartner',
      headers: await storage.getHeaders(),
      data: {'DESCRIPTION': description},
      operation: operation[Operation.cancelSubscriptionPartner]!,
      cancelToken: cancelToken,
    );

    return response['data']['ENDDATE']!;

    //
  }

  @override
  Future<List<MasterInfoGeneralModel>> getMasterInfo() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/SeeSettings',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getMasterInfo]!,
      cancelToken: cancelToken,
    );

    return MasterInfoGeneralModel.fromCollection(response['data']);

    //
  }

  @override
  Future<bool> saveMasterInfo({required Map<String, dynamic> payload}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/SaveSettings',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.saveMasterInfo]!,
      cancelToken: cancelToken,
    );

    return response['success'];

    //
  }

  @override
  Future<MinboligApiResponse?> getDenmarkCities() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/find/cities',
      headers: await storage.getHeaders(),
      operation: operation[Operation.denmarkCities]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> updateSupplier(
      {required Map<String, dynamic> payload}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/supplier/updateSupplier/${await storage.getUserId()}',
      headers: await storage.getHeaders(),
      operation: operation[Operation.updateSupplier]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> getCustomers({required String query}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/customers',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getCustomers]!,
      queryParameters: {'limit': 1000, 'name': query},
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> getCustomersProjects(
      {required int contactId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/customersProject/$contactId',
      queryParameters: {'projectResponse': true, 'limit': 1000},
      headers: await storage.getHeaders(),
      operation: operation[Operation.getCustomersProjects]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }
}
