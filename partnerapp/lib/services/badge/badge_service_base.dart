import 'package:Haandvaerker.dk/model/badges/press_article_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class BadgeServiceBase {
  //

  Future<PressArticleModel> getPressArticle();

  //
}
