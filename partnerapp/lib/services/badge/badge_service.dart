import 'package:Haandvaerker.dk/model/badges/press_article_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/badge/badge_service_base.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:dio/dio.dart';

class BadgeService extends BadgeServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  BadgeService({required this.apiService, required this.storage});

  @override
  Future<PressArticleModel> getPressArticle() async {
    //

    final CancelToken cancelToken = CancelToken();

    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/GetQuotes',
      headers: await storage.getHeaders(),
      queryParameters: {"levid": (await storage.getUserId())},
      operation: operation[Operation.getPressArticle]!,
      cancelToken: cancelToken,
    );

    return PressArticleModel.fromJson(
        Map<String, dynamic>.from(response['data']));

    //
  }

  //
}
