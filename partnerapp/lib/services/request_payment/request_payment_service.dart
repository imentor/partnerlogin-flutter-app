import 'dart:io';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/request_payment/request_payment_service_base.dart';
import 'package:dio/dio.dart';

class RequestPaymentService extends RequestPaymentServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  RequestPaymentService({required this.apiService, required this.storage});

  @override
  Future<MinboligApiResponse?> paymentStages(
      {required projectId, required String type}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/paymentStagesV2',
      headers: await storage.getHeaders(),
      queryParameters: {'projectId': projectId, 'type': type},
      operation: operation[Operation.getPaymentStages]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }

    //
  }

  @override
  Future<MinboligApiResponse> requestPayment({
    required int projectId,
    required int paymentStageId,
    required String notes,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/paymentStages/requestPartial',
      headers: await storage.getHeaders(),
      data: notes.isEmpty
          ? {
              'projectId': projectId,
              'paymentStageId': paymentStageId,
            }
          : {
              'projectId': projectId,
              'paymentStageId': paymentStageId,
              'notes': notes,
            },
      operation: operation[Operation.requestPayment]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> requestPartial({
    required bitrixToken,
    required projectId,
    required paymentStageId,
    required List<File> files,
  }) async {
    //

    final formData = FormData();

    for (final file in files) {
      formData.files.addAll([
        MapEntry(
            "files[]",
            await MultipartFile.fromFile(file.path,
                filename: file.path.split("/").last)),
      ]);
    }

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/paymentStages/requestPartial?projectId',
      headers: {'Authorization': 'Bearer $bitrixToken'},
      data: formData,
      queryParameters: {
        'projectId': projectId,
        'paymentStageId': paymentStageId,
      },
      operation: operation[Operation.requestPartial]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> uploadPaymentStagePhotos(
      {required paymentStageId, required List<File> files}) async {
    //

    final formData = FormData();

    for (final file in files) {
      formData.files.addAll([
        MapEntry(
            "files[]",
            await MultipartFile.fromFile(file.path,
                filename: file.path.split("/").last)),
      ]);
    }

    formData.fields.add(
      MapEntry("paymentStageId", '$paymentStageId'),
    );

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/project/uploadPaymentStagePhotos',
      headers: await storage.getHeaders(),
      data: formData,
      operation: operation[Operation.uploadPaymentStagePhotos]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> uploadPaymentStageFiles(
      {required FormData payload}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/files',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.uploadPaymentStagePhotos]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse?> requestPaymentPartial(
      {required FormData formData, Map<String, dynamic>? payload}) async {
    final CancelToken cancelToken = CancelToken();

    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/paymentStages/requestPartial',
      headers: await storage.getHeaders(),
      data: payload ?? formData,
      operation: operation[Operation.requestPartial]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> forcePartialPayout(
      {required Map<String, dynamic> payload}) async {
    final CancelToken cancelToken = CancelToken();

    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/paymentStages/forcePartialPayout',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.forcePartialPayout]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> declinePayout(
      {required Map<String, dynamic> payload}) async {
    final CancelToken cancelToken = CancelToken();

    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/paymentStages/declinePayout',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.declinePayout]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }
}
