import 'dart:io';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

@immutable
abstract class RequestPaymentServiceBase {
  Future<MinboligApiResponse?> paymentStages(
      {required projectId, required String type});

  Future<MinboligApiResponse> requestPayment({
    required int projectId,
    required int paymentStageId,
    required String notes,
  });

  Future<MinboligApiResponse> requestPartial({
    required bitrixToken,
    required projectId,
    required paymentStageId,
    required List<File> files,
  });

  Future<MinboligApiResponse> uploadPaymentStagePhotos(
      {required paymentStageId, required List<File> files});

  Future<MinboligApiResponse> uploadPaymentStageFiles(
      {required FormData payload});

  Future<MinboligApiResponse?> requestPaymentPartial(
      {required FormData formData, Map<String, dynamic>? payload});

  Future<MinboligApiResponse?> forcePartialPayout(
      {required Map<String, dynamic> payload});

  Future<MinboligApiResponse?> declinePayout(
      {required Map<String, dynamic> payload});
}
