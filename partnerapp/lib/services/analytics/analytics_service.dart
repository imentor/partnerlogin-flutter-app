import 'dart:io';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/ui/routing/routing.dart';
import 'package:Haandvaerker.dk/utils/sentry_functions.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:package_info_plus/package_info_plus.dart';

class AnalyticsService {
  AnalyticsService({
    required this.apiService,
    required this.analytics,
    required this.observer,
    required this.storage,
  });

  final ApiService apiService;
  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;
  final SecuredStorageService storage;

  Future logSession() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    Map<String, dynamic> deviceDetails = {};

    if (Platform.isIOS) {
      final IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;

      deviceDetails = {
        'app_version': '${packageInfo.version}+${packageInfo.buildNumber}',
        'isPhysicalDevice': iosDeviceInfo.isPhysicalDevice ? 1 : 0,
        'device_model': iosDeviceInfo.model,
        'company_id': await storage.getUserId(),
        'email': await storage.getEmail(),
        'platform': 'iOS'
      };
    }

    if (Platform.isAndroid) {
      final AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;

      deviceDetails = {
        'app_version': '${packageInfo.version}+${packageInfo.buildNumber}',
        'isPhysicalDevice': androidDeviceInfo.isPhysicalDevice ? 1 : 0,
        'device_model': androidDeviceInfo.model,
        'company_id': await storage.getUserId(),
        'email': await storage.getEmail(),
        'platform': 'Android'
      };
    }

    await analytics.logEvent(
      name: 'partnerlogin_session',
      parameters: {...deviceDetails},
    );
  }

  Future logScreenActivity({required String? route}) async {
    final screenClass = RouteGenerator.routingMap[route] ?? 'ErrorScreen';
    await analytics.logScreenView(screenName: route, screenClass: screenClass);
  }
}
