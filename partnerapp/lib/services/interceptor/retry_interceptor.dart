import 'dart:developer';
import 'dart:io';

import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/interceptor/dio_request_retrier.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:Haandvaerker.dk/utils/reset_viewmodel_states.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class RetryOnConnectionChangeInterceptor extends Interceptor {
  final DioRequestRetrier requestRetrier;
  final SecuredStorageService storage;

  RetryOnConnectionChangeInterceptor({
    required this.requestRetrier,
    required this.storage,
  });

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    options.responseType = ResponseType.json;
    options.connectTimeout = const Duration(milliseconds: 120000);
    options.receiveTimeout = const Duration(milliseconds: 30000);
    options.cancelToken = APPLIC.cancelToken;
    return handler.next(options);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    return handler.next(response);
  }

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) async {
    if (err.response?.data is Map<dynamic, dynamic>) {
      if ((err.response?.data as Map).containsKey('message')) {
        dynamic message = err.response?.data?["message"];
        if (message is String &&
            message == "Unauthorized. User has been logged out.") {
          _redirectToLoginPage();
        } else {
          return handler.next(err);
        }
      } else {
        return handler.next(err);
      }
    }

    if (_shouldRetry(err)) {
      try {
        await requestRetrier.scheduleRequestRetry(err.requestOptions);
      } catch (e) {
        log(e.toString());
        return handler.next(err);
      }
    } else {
      return handler.next(err);
    }

    super.onError(err, handler); // Pass the handler here
  }

  void _redirectToLoginPage() async {
    final status = await storage.clearStorage();
    if (status == true) {
      resetViewmodelStates(
          context: myGlobals.mainNavigatorKey!.currentContext!);
      Navigator.pushNamedAndRemoveUntil(
        myGlobals.mainNavigatorKey!.currentContext!,
        Routes.login,
        (r) => false,
      );
    }
  }

  bool _shouldRetry(DioException err) {
    return err.type == DioExceptionType.connectionTimeout &&
        err.error != null &&
        err.error is SocketException;
  }
}
