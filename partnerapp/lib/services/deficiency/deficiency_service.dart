import 'dart:io';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/deficiency/deficiency_service_base.dart';
import 'package:Haandvaerker.dk/utils/file_utils.dart';
import 'package:dio/dio.dart';

class DeficiencyService extends DeficiencyServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  DeficiencyService({required this.apiService, required this.storage});

  @override
  Future<MinboligApiResponse> getDeficiencyList(
      {required int projectId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/project/$projectId/deficiency',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getDeficiencyList]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> addDeficiency({
    required projectId,
    required Map<String, dynamic> form,
    required List<File> images,
  }) async {
    //

    final data = await filesToFormData(images, 'images[]');
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/project/$projectId/deficiency',
      headers: await storage.getHeaders(),
      queryParameters: form,
      data: data,
      operation: operation[Operation.addDeficiency]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> updateDeficiency({
    required int projectId,
    required int deficiencyId,
    required Map<String, dynamic> form,
    required List<File> images,
  }) async {
    //

    final data = await filesToFormData(images, 'images[]');
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/project/$projectId/deficiency/$deficiencyId',
      headers: await storage.getHeaders(),
      queryParameters: form,
      data: data,
      operation: operation[Operation.updateDeficiency]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> addDeficiencyComment({
    required int projectId,
    required int deficiencyId,
    required String comment,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/project/$projectId/deficiency/$deficiencyId/addComment',
      headers: await storage.getHeaders(),
      data: {'comment': comment},
      operation: operation[Operation.addDeficiencyComment]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> deleteDeficiency(
      {required int projectId, required int deficiencyId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.DELETE,
      path: '/partner/project/$projectId/deficiency/$deficiencyId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.deleteDeficiency]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> updateDeficiencyEndDate(
      {required int projectId, required String endDate}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/project/$projectId/updateDeficiencyEndDate',
      headers: await storage.getHeaders(),
      data: {'endDate': endDate},
      operation: operation[Operation.updateDeficiencyEndDate]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> preApproveDeficiency(
      {required int projectId, required bool approve}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/project/$projectId/preApproveDeficiency',
      headers: await storage.getHeaders(),
      data: {'approve': approve},
      operation: operation[Operation.preApproveDeficiency]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> markAsDoneDeficiency(
      {required int projectId, required int? deficiencyId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/project/$projectId/deficiency/$deficiencyId/markAsDone',
      headers: await storage.getHeaders(),
      operation: operation[Operation.markAsDoneDeficiency]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> approveDeficiency(
      {required int projectId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/project/$projectId/approveDeficiency',
      headers: await storage.getHeaders(),
      operation: operation[Operation.approveDeficiency]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  //
}
