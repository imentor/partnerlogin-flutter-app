import 'dart:io';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class DeficiencyServiceBase {
  //

  Future<MinboligApiResponse> getDeficiencyList({required int projectId});

  Future<MinboligApiResponse> addDeficiency({
    required int projectId,
    required Map<String, dynamic> form,
    required List<File> images,
  });

  Future<MinboligApiResponse> updateDeficiency({
    required int projectId,
    required int deficiencyId,
    required Map<String, dynamic> form,
    required List<File> images,
  });

  Future<MinboligApiResponse> addDeficiencyComment({
    required int projectId,
    required int deficiencyId,
    required String comment,
  });

  Future<MinboligApiResponse> deleteDeficiency(
      {required int projectId, required int deficiencyId});

  Future<MinboligApiResponse> updateDeficiencyEndDate(
      {required int projectId, required String endDate});

  Future<MinboligApiResponse> preApproveDeficiency(
      {required int projectId, required bool approve});

  Future<MinboligApiResponse> markAsDoneDeficiency(
      {required int projectId, required int deficiencyId});

  Future<MinboligApiResponse> approveDeficiency({required int projectId});

  //
}
