import 'dart:convert';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/booking_wizard/booking_wizard_service_base.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:dio/dio.dart';

class BookingWizardService extends BookingWizardServiceBase {
  final ApiService apiService;
  final SecuredStorageService storage;

  BookingWizardService({required this.apiService, required this.storage});

  @override
  Future<MinboligApiResponse> getWizardProducts(
      {required String labels}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/products',
      headers: await storage.getHeaders(),
      queryParameters: {"labels": labels},
      operation: operation[Operation.getWizardProducts]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> getWizardProductsNewVersion(
      {required String labels}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/products',
      headers: await storage.getHeaders(),
      queryParameters: {"labels": labels, "newVersion": 1},
      operation: operation[Operation.getWizardProducts]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> addWizardProduct(
      {required Map<String, dynamic> form}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/wizard/addProduct',
      headers: await storage.getHeaders(),
      data: jsonEncode(form),
      operation: operation[Operation.addWizardProduct]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> getListPriceAi(
      {required String industryId, required int contactId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/getListPriceAi',
      headers: await storage.getHeaders(),
      queryParameters: {
        "industry_id": industryId,
        "project_id": 0,
        "contactId": contactId
      },
      operation: operation[Operation.getWizardProducts]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> createAddress({
    required int contactId,
    required String addressTitle,
    required String addressId,
    required String adgangsaddressId,
    required bool isAffiliate,
  }) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/wizard/createAddress',
      headers: await storage.getHeaders(),
      data: jsonEncode({
        "contactId": contactId,
        "addressTitle": addressTitle,
        "addressId": addressId,
        "adgangsaddressId": adgangsaddressId,
        "isAffiliate": isAffiliate ? 1 : 0
      }),
      operation: operation[Operation.createAddress]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> createContact({
    required String name,
    required String email,
    required String mobile,
    required bool isAffiliate,
  }) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/wizard/createContact',
      headers: await storage.getHeaders(),
      data: jsonEncode({
        "name": name,
        "email": email,
        "mobile": mobile,
        "isAffiliate": isAffiliate ? 1 : 0
      }),
      operation: operation[Operation.createContact]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> getListSummaryAi(
      {required String industryId, required int contactId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/getListSummaryAi',
      headers: await storage.getHeaders(),
      queryParameters: {
        "industry_id": industryId,
        "project_id": 0,
        "contactId": contactId
      },
      operation: operation[Operation.getListSummaryAi]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> getListTitleAi(
      {required String industryId, required int contactId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/getListTitleAi',
      headers: await storage.getHeaders(),
      queryParameters: {
        "industry_id": industryId,
        "project_id": 0,
        "contactId": contactId
      },
      operation: operation[Operation.getListTitleAi]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> cleanListPriceAi({required int contactId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/cleanListPriceAi',
      headers: await storage.getHeaders(),
      queryParameters: {"contactId": contactId},
      operation: operation[Operation.cleanListPriceAi]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> getListSubindustryAi(
      {required int contactId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/getListSubindustryAi',
      headers: await storage.getHeaders(),
      queryParameters: {"contactId": contactId, "projectId": 0},
      operation: operation[Operation.cleanListPriceAi]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> createProject(
      {required Map<String, dynamic> form}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/wizard/createProject',
      headers: await storage.getHeaders(),
      data: jsonEncode(form),
      operation: operation[Operation.cleanListPriceAi]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> reserveProject(
      {required int contactId,
      required int projectId,
      required bool isAffiliate}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/wizard/reserveProject',
      headers: await storage.getHeaders(),
      data: jsonEncode({
        "contactId": contactId,
        "projectId": projectId,
        "isAffiliate": isAffiliate ? 1 : 0
      }),
      operation: operation[Operation.cleanListPriceAi]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> createOffer(
      {required Map<String, dynamic> params}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/wizard/createOffer',
      headers: await storage.getHeaders(),
      data: jsonEncode(params),
      operation: operation[Operation.createOffer]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> getAllProjects() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/allProjects',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getAllProjects]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> saveListPrice(
      {required Map<String, dynamic> form}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/wizard/saveListPriceV2',
      headers: await storage.getHeaders(),
      data: jsonEncode(form),
      operation: operation[Operation.saveListPrice]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> sendInvite({
    required int contactId,
    required int projectId,
    required int offerId,
    required bool isAffiliate,
  }) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/wizard/sendInvite',
      headers: await storage.getHeaders(),
      data: jsonEncode({
        "contactId": contactId,
        "projectId": projectId,
        "offerId": offerId,
        "isAffiliate": isAffiliate ? 1 : 0
      }),
      operation: operation[Operation.sendInvite]!,
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> getProjectById({
    required int projectId,
  }) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/project/$projectId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getProjectById]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> getAddressId({required int addressId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/address/$addressId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getAddressId]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> updateContact(
      {required int contactId,
      required String name,
      required int mobile}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/wizard/updateContact',
      headers: await storage.getHeaders(),
      data:
          jsonEncode({"contactId": contactId, "name": name, "mobile": mobile}),
      operation: operation[Operation.updateContact]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> getAiCalculationPriceV2({
    required String addressId,
    required String adgangsAdresseId,
    required String subTasks,
    required int bookerWizardId,
    required int industry,
  }) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/getAiCalculationPriceV2',
      headers: await storage.getHeaders(),
      queryParameters: {
        'addressId': addressId,
        'adgangsaddressId': adgangsAdresseId,
        'subTasks': subTasks,
        'bookerWizardId': bookerWizardId,
        'industry': industry
      },
      operation: operation[Operation.getAiCalculationPriceV2]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse?> getAiCalculationPriceV3({
    required String addressId,
    required String adgangsAddressId,
    required String subTasks,
    required int contactId,
    required String productType,
    required String industry,
  }) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/getAiCalculationPriceV3',
      headers: await storage.getHeaders(),
      queryParameters: {
        'addressId': addressId,
        'adgangsaddressId': adgangsAddressId,
        'subTasks': subTasks,
        'userId': contactId,
        'productType': productType,
        'industry': industry
      },
      operation: operation[Operation.getAiCalculationPriceV3]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse> submitPriceCalculator(
      {required Map<String, dynamic> payload}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/fast-track/priceCalculator',
      headers: await storage.getHeaders(),
      data: jsonEncode(payload),
      operation: operation[Operation.submitPriceCalculator]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> saveAiPriceSurvey(
      {required Map<String, dynamic> payload}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/wizard/saveAiPriceSurvey',
      headers: await storage.getHeaders(),
      data: jsonEncode(payload),
      operation: operation[Operation.saveAiPriceSurvey]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> getProjectTaskTypes() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/GetTasks',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getProjectTaskTypes]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse?> getOpenAiEnrichData(
      {required int contactId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/openai/enrichData/$contactId/',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getOpenAiEnrichData]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> createAssistantWizard(
      {required Map<String, dynamic> payload}) async {
    final data = payload;
    data['partnerId'] = await storage.getUserId();
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/openai/wizard',
      headers: await storage.getHeaders(),
      data: jsonEncode(payload),
      operation: operation[Operation.createAssistantWizard]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> assistantWizardMessage(
      {required Map<String, dynamic> payload}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/openai/wizard/message',
      headers: await storage.getHeaders(),
      data: jsonEncode(payload),
      operation: operation[Operation.assistantWizardMessage]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> assistantWizardMessageFinalStep(
      {required Map<String, dynamic> payload}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/openai/wizard/updateThreadMessages',
      headers: await storage.getHeaders(),
      data: jsonEncode(payload),
      operation: operation[Operation.assistantWizardMessageFinalStep]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> getAssistantWizardMessages(
      {required String wizardId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/openai/wizard/$wizardId/messages',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getAssistantWizardMessages]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> enrichAkkio(
      {required Map<String, dynamic> payload}) async {
    final data = payload;
    data['partnerId'] = await storage.getUserId();
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/akkio/enrichData',
      headers: await storage.getHeaders(),
      data: jsonEncode(payload),
      operation: operation[Operation.enrichAkkio]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> priceEstimateAkkio(
      {required Map<String, dynamic> payload}) async {
    payload['partnerId'] = await storage.getUserId();
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/akkio/priceEstimate',
      headers: await storage.getHeaders(),
      data: jsonEncode(payload),
      operation: operation[Operation.priceEstimateAkkio]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }
}
