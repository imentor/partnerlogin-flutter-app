import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class BookingWizardServiceBase {
  Future<MinboligApiResponse> getWizardProducts({required String labels});

  Future<MinboligApiResponse> getWizardProductsNewVersion(
      {required String labels});

  Future<MinboligApiResponse> addWizardProduct(
      {required Map<String, dynamic> form});

  Future<MinboligApiResponse> getListPriceAi(
      {required String industryId, required int contactId});

  Future<MinboligApiResponse> createContact({
    required String name,
    required String email,
    required String mobile,
    required bool isAffiliate,
  });

  Future<MinboligApiResponse> createAddress({
    required int contactId,
    required String addressTitle,
    required String addressId,
    required String adgangsaddressId,
    required bool isAffiliate,
  });

  Future<MinboligApiResponse> getListSummaryAi(
      {required String industryId, required int contactId});

  Future<MinboligApiResponse> getListTitleAi(
      {required String industryId, required int contactId});

  Future<MinboligApiResponse> cleanListPriceAi({required int contactId});

  Future<MinboligApiResponse> getListSubindustryAi({required int contactId});

  Future<MinboligApiResponse> createProject(
      {required Map<String, dynamic> form});

  Future<MinboligApiResponse> reserveProject({
    required int contactId,
    required int projectId,
    required bool isAffiliate,
  });

  Future<MinboligApiResponse> createOffer(
      {required Map<String, dynamic> params});

  Future<MinboligApiResponse> getAllProjects();

  Future<MinboligApiResponse> saveListPrice(
      {required Map<String, dynamic> form});

  Future<MinboligApiResponse> sendInvite({
    required int contactId,
    required int projectId,
    required int offerId,
    required bool isAffiliate,
  });

  Future<MinboligApiResponse> getProjectById({
    required int projectId,
  });

  Future<MinboligApiResponse> getAddressId({
    required int addressId,
  });

  Future<MinboligApiResponse> updateContact({
    required int contactId,
    required String name,
    required int mobile,
  });

  Future<MinboligApiResponse> getAiCalculationPriceV2({
    required String addressId,
    required String adgangsAdresseId,
    required String subTasks,
    required int bookerWizardId,
    required int industry,
  });

  Future<MinboligApiResponse?> getAiCalculationPriceV3({
    required String addressId,
    required String adgangsAddressId,
    required String subTasks,
    required int contactId,
    required String productType,
    required String industry,
  });

  Future<MinboligApiResponse> submitPriceCalculator(
      {required Map<String, dynamic> payload});

  Future<MinboligApiResponse> saveAiPriceSurvey(
      {required Map<String, dynamic> payload});

  Future<MinboligApiResponse> getProjectTaskTypes();

  Future<MinboligApiResponse?> getOpenAiEnrichData({required int contactId});

  Future<MinboligApiResponse?> createAssistantWizard(
      {required Map<String, dynamic> payload});

  Future<MinboligApiResponse?> assistantWizardMessage(
      {required Map<String, dynamic> payload});

  Future<MinboligApiResponse?> assistantWizardMessageFinalStep(
      {required Map<String, dynamic> payload});

  Future<MinboligApiResponse?> getAssistantWizardMessages(
      {required String wizardId});

  Future<MinboligApiResponse?> enrichAkkio(
      {required Map<String, dynamic> payload});

  Future<MinboligApiResponse?> priceEstimateAkkio(
      {required Map<String, dynamic> payload});
}
