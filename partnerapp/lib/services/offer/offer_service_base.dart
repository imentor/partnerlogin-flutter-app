import 'package:Haandvaerker.dk/model/create_offer/list_price_partner_ai_v2_model.dart';
import 'package:Haandvaerker.dk/model/create_offer/list_price_partner_ai_v3_model.dart';
import 'package:Haandvaerker.dk/model/create_offer/offer_response_model.dart';
import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:flutter/foundation.dart';

@immutable
abstract class OfferServiceBase {
  //

  Future<List<ListPricePartnerAiV2Model>> getListPricePartnerAiV2(
      {required int parentProjectId, required int contactId});

  Future<List<ListPricePartnerAiV3Model>?> getListPricePartnerAiV3(
      {required Map<String, dynamic> payload});

  Future<bool> sendRequestOffer({required Map<String, dynamic> payload});

  Future<List<OfferPostResponseModel>> partnerOffer(
      {required Map<String, dynamic> payload});

  Future<MinboligApiResponse?> partnerOfferVersion(
      {required Map<String, dynamic> offerVersionPayload});

  Future<MinboligApiResponse?> partnerOfferUpdate(
      {required Map<String, dynamic> updateOfferPayload});

  Future<bool> saveListPrice({required Map<String, dynamic> payload});

  Future<MinboligApiResponse?> getTermsAndConditions();

  Future<MinboligApiResponse> updateOffer(
      {required Map<String, dynamic> payload});

  Future<MinboligApiResponse> updateListPrice(
      {required Map<String, dynamic> payload});

  Future<MinboligApiResponse?> getListPriceMinbolig(
      {required int contactId, required int projectId});

  Future<MinboligApiResponse?> getListPriceMinboligOfferVersion(
      {required int contactId,
      required int offerVersionId,
      required int projectId});

  Future<void> updateCompanyOfferNote({required String offerNote});

  Future<MinboligApiResponse?> getOfferById({required int offerId});

  Future<MinboligApiResponse> fileMerger(
      {required Map<String, dynamic> payload});

  Future<MinboligApiResponse?> acceptOfferVersion(
      {required int offerId, required int projectId});

  Future<MinboligApiResponse?> createOfferByCs(
      {required Map<String, dynamic> payload});

  Future<MinboligApiResponse?> addSignatureToOffer(
      {required int offerId, required String signature});

  Future<MinboligApiResponse?> addSignatureToOfferVersion(
      {required int offerId,
      required String signature,
      required int offerVersionId});

  Future<MinboligApiResponse?> createOfferVersionByCs(
      {required Map<String, dynamic> payload});

  Future<Map<String, dynamic>> getHeaders();

  Future<Uint8List?> previewOfferWizard(
      {required Map<String, dynamic> payload});

  //
}
