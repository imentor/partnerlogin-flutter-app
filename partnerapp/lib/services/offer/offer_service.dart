import 'dart:convert';

import 'package:Haandvaerker.dk/model/create_offer/list_price_partner_ai_v2_model.dart';
import 'package:Haandvaerker.dk/model/create_offer/list_price_partner_ai_v3_model.dart';
import 'package:Haandvaerker.dk/model/create_offer/offer_response_model.dart';
import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/offer/offer_service_base.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class OfferService extends OfferServiceBase {
  OfferService({required this.apiService, required this.storage});
  final ApiService apiService;
  final SecuredStorageService storage;

  @override
  Future<List<ListPricePartnerAiV2Model>> getListPricePartnerAiV2({
    required int parentProjectId,
    required int contactId,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/getListPricePartnerAiV2',
      headers: await storage.getHeaders(),
      queryParameters: {
        'project_id': parentProjectId,
        'contact_id': contactId,
      },
      operation: operation[Operation.getListPricePartnerAiV2]!,
      cancelToken: cancelToken,
    );

    return ListPricePartnerAiV2Model.fromCollection(
        response['data'] as List<dynamic>);

    //
  }

  @override
  Future<List<ListPricePartnerAiV3Model>?> getListPricePartnerAiV3(
      {required Map<String, dynamic> payload}) async {
    //
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/getListPricePartnerAiV3',
      headers: await storage.getHeaders(),
      queryParameters: {
        'projectId': payload['projectId'],
        'contactId': payload['contactId'],
      },
      operation: operation[Operation.getListPricePartnerAiV3]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return ListPricePartnerAiV3Model.fromCollection(
          response['data'] as List<dynamic>);
    } else {
      return null;
    }
  }

  @override
  Future<bool> sendRequestOffer({required Map<String, dynamic> payload}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/message/sendRequestOffer',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.sendRequestOffer]!,
      cancelToken: cancelToken,
    );

    return response['success'];
    //
  }

  @override
  Future<List<OfferPostResponseModel>> partnerOffer(
      {required Map<String, dynamic> payload}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/offer',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.partnerOffer]!,
      cancelToken: cancelToken,
    );

    return OfferPostResponseModel.fromCollection(
        Map<String, dynamic>.from(response['data'])['offer'] as List<dynamic>);

    //
  }

  @override
  Future<MinboligApiResponse?> partnerOfferVersion(
      {required Map<String, dynamic> offerVersionPayload}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/offer/version',
      headers: await storage.getHeaders(),
      data: offerVersionPayload,
      operation: operation[Operation.partnerOffer]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }

    //
  }

  @override
  Future<MinboligApiResponse?> partnerOfferUpdate(
      {required Map<String, dynamic> updateOfferPayload}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/offer/update',
      headers: await storage.getHeaders(),
      data: updateOfferPayload,
      operation: operation[Operation.partnerOffer]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }
  }

  @override
  Future<bool> saveListPrice({required Map<String, dynamic> payload}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/wizard/saveListPriceV2',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.saveListPrice]!,
      cancelToken: cancelToken,
    );

    return Map<String, dynamic>.from(response)['message'] == "Success.";

    //
  }

  @override
  Future<MinboligApiResponse?> getTermsAndConditions() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/helper/termsAndConditionsPopupV2',
      headers: await storage.getHeaders(),
      queryParameters: {"COMPANY": await storage.getUserId()},
      operation: operation[Operation.getTermsAndConditions]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }
    //
  }

  @override
  Future<MinboligApiResponse> updateOffer(
      {required Map<String, dynamic> payload}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.PUT,
      path: '/partner/offer/${payload['offer_id']}',
      headers: await storage.getHeaders(),
      queryParameters: {
        'subTotal': payload['subTotal'],
        'total': payload['total'],
        'vat': payload['vat'],
      },
      operation: operation[Operation.updateOffer]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> updateListPrice(
      {required Map<String, dynamic> payload}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/wizard/updateListPricePartner',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.update_list_price]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse?> getListPriceMinbolig(
      {required int contactId, required int projectId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/getListPriceMinboligV2',
      headers: await storage.getHeaders(),
      queryParameters: {
        "contact_id": contactId,
        "project_id": projectId,
        "calculation_id": 0,
      },
      operation: operation[Operation.getListPriceMinbolig]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }
  }

  @override
  Future<MinboligApiResponse?> getListPriceMinboligOfferVersion(
      {required int contactId,
      required int offerVersionId,
      required int projectId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/wizard/getListPriceMinboligV2',
      headers: await storage.getHeaders(),
      queryParameters: {
        "contact_id": contactId,
        "project_id": projectId,
        "offer_version_id": offerVersionId,
        "calculation_id": 0,
      },
      operation: operation[Operation.getListPriceMinbolig]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }
  }

  @override
  Future<void> updateCompanyOfferNote({required String offerNote}) async {
    ///

    final CancelToken cancelToken = CancelToken();
    await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/supplier/updateSupplier/${(await storage.getUserId())}',
      data: {'UF_CRM_COMPANY_OFFER_NOTE': offerNote},
      operation: operation[Operation.updateCompanyOfferNote]!,
      cancelToken: cancelToken,
    );

    ///
  }

  @override
  Future<MinboligApiResponse?> getOfferById({required int offerId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/offer/$offerId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getOfferById]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse> fileMerger(
      {required Map<String, dynamic> payload}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/merge',
      data: jsonEncode(payload),
      headers: await storage.getHeaders(),
      operation: operation[Operation.fileMerger]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse?> acceptOfferVersion(
      {required int offerId, required int projectId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/offer/acceptOfferVersion',
      data: jsonEncode({
        "offerId": offerId,
        "projectId": projectId,
        "acceptedFrom": "partner-app"
      }),
      headers: await storage.getHeaders(),
      operation: operation[Operation.acceptOfferVersion]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> createOfferByCs(
      {required Map<String, dynamic> payload}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/createOfferByCs',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.createOfferByCs]!,
      cancelToken: cancelToken,
    );
    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> addSignatureToOffer(
      {required int offerId, required String signature}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/offer/addSignatureToOffer',
      headers: await storage.getHeaders(),
      data: jsonEncode({
        "offerId": offerId,
        "file": signature,
        "fileName": "signature name"
      }),
      operation: operation[Operation.addSignatureToOffer]!,
      cancelToken: cancelToken,
    );
    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> addSignatureToOfferVersion(
      {required int offerId,
      required String signature,
      required int offerVersionId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/offer/addSignatureToOfferVersion',
      headers: await storage.getHeaders(),
      data: jsonEncode({
        "offerId": offerId,
        "offerVersionId": offerVersionId,
        "file": signature,
        "fileName": "signature name"
      }),
      operation: operation[Operation.addSignatureToOfferVersion]!,
      cancelToken: cancelToken,
    );
    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> createOfferVersionByCs(
      {required Map<String, dynamic> payload}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/createOfferVersionByCs',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.createOfferVersionByCs]!,
      cancelToken: cancelToken,
    );
    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<Map<String, dynamic>> getHeaders() async {
    return await storage.getHeaders();
  }

  @override
  Future<Uint8List?> previewOfferWizard(
      {required Map<String, dynamic> payload}) async {
    final response = await Dio().post(
      '${applic.bitrixApiUrl}/partner/wizard/previewOffer',
      data: jsonEncode(payload),
      options: Options(
        responseType: ResponseType.bytes,
        headers: await storage.getHeaders(),
      ),
    );

    return response.data;
  }
}
