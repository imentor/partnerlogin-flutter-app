import 'dart:convert';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/calendar/calendar_service_base.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:dio/dio.dart';

class CalendarService extends CalendarServiceBase {
  final ApiService apiService;
  final SecuredStorageService storage;

  CalendarService({required this.apiService, required this.storage});

  @override
  Future<MinboligApiResponse?> getProjectCalendarEvents(
      {required int projectId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/calendars',
      headers: await storage.getHeaders(),
      queryParameters: {"projectId": projectId},
      operation: operation[Operation.getProjectCalendarEvents]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> getCalendarTypes() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/calendarTypes',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getCalendarTypes]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> addUpdateEvent(
      {required Map<String, dynamic> payload, int? calendarId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: calendarId != null ? RequestMethod.PUT : RequestMethod.POST,
      path: calendarId != null
          ? '/partner/calendar/$calendarId'
          : '/partner/calendar',
      headers: await storage.getHeaders(),
      data: jsonEncode(payload),
      operation: operation[Operation.addUpdateEvent]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> deleteEvent({required int calendarId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.DELETE,
      path: '/partner/calendar/$calendarId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.deleteEvent]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }
}
