import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class CalendarServiceBase {
  Future<MinboligApiResponse?> getProjectCalendarEvents(
      {required int projectId});

  Future<MinboligApiResponse?> getCalendarTypes();

  Future<MinboligApiResponse?> addUpdateEvent(
      {required Map<String, dynamic> payload});

  Future<MinboligApiResponse?> deleteEvent({required int calendarId});
}
