import 'dart:convert';

import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/mitid/mitid_service_base.dart';
import 'package:dio/dio.dart';

class MitidService extends MitidServiceBase {
  MitidService({required this.apiService, required this.storage});
  final ApiService apiService;
  final SecuredStorageService storage;

  @override
  Future<void> mitidApproved({required Map<String, dynamic> forms}) async {
    Map<String, dynamic> tempForm = {...forms};

    final userId = (await storage.getUserId());
    final companyName = (await storage.getCompanyName());
    final bitrixToken = (await storage.getBitrixToken());

    tempForm.putIfAbsent('userId', () => userId);
    tempForm.putIfAbsent('name', () => companyName);
    tempForm.putIfAbsent('bearer_token', () => bitrixToken);

    final CancelToken cancelToken = CancelToken();
    await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/MitId/MitidApproved',
      headers: await storage.getHeaders(),
      data: jsonEncode(tempForm),
      operation: operation[Operation.mitidApproved]!,
      cancelToken: cancelToken,
    );
  }
}
