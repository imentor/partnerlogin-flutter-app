import 'dart:developer';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_add_product_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_cart_response_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_category_response_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_checkout_response_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_coupon_response_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_delivery_price_response_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_orders_response_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_product_response_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_terms_and_conditions_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_wishlist_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/webshop/webshop_service_base.dart';
import 'package:dio/dio.dart';

class WebshopService implements WebShopServiceBase {
  WebshopService({required this.storage, required this.apiService});

  final ApiService apiService;
  final SecuredStorageService storage;

  @override
  Future<WebshopProductResponseModel> getWebShopProduct({
    int productId = 0,
    int productName = 0,
    int categoryId = 0,
    int page = 0,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path:
          '/webshop/GetProducts/$productId/$productName/$categoryId?page=$page',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getWebShopProduct]!,
      cancelToken: cancelToken,
    );

    return WebshopProductResponseModel.fromJson(response);

    //
  }

  @override
  Future<WebshopCategoryResponseModel> getWebShopCategory(
      {int categoryId = 0}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/webshop/GetCategory/$categoryId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getWebShopCategory]!,
      cancelToken: cancelToken,
    );

    return WebshopCategoryResponseModel.fromJson(response);

    //
  }

  @override
  Future<WebshopCartResponseModel> getWebShopCartData() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/webshop/GetCart/${(await storage.getUserId())}',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getWebShopCartData]!,
      cancelToken: cancelToken,
    );

    return WebshopCartResponseModel.fromJson(response);

    //
  }

  /// isUpdate if true, will update else will delete
  @override
  Future<GenericResponseModel> webShopModifyCart({
    required bool isUpdate,
    required int productId,
    required int quantity,
  }) async {
    //

    final deleteUpdateFlag = isUpdate ? 0 : -1;
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path:
          '/webshop/ModifyCart/$deleteUpdateFlag/${(await storage.getUserId())}/$productId/$quantity',
      headers: await storage.getHeaders(),
      operation: operation[Operation.webShopModifyCart]!,
      cancelToken: cancelToken,
    );

    return GenericResponseModel.fromJson(response);

    //
  }

  @override
  Future<GenericResponseModel> webShopAddCartItem({
    required int addUpdateFlag,
    required String productGroup,
    required int productId,
    List<String>? logo,
    required int quantity,
  }) async {
    //

    final map = {
      "CompanyID": (await storage.getUserId()),
      "ProductGroup": productGroup,
      "ProductID": productId,
      "Logo": logo,
      "Quantity": quantity,
    };

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/webshop/AddToCart/$addUpdateFlag',
      headers: await storage.getHeaders(),
      operation: operation[Operation.webShopAddCartItem]!,
      data: map,
      cancelToken: cancelToken,
    );

    return GenericResponseModel.fromJson(response);

    //
  }

  @override
  Future<WebshopCheckoutResponseModel> webShopCheckOut(
      {required int couponId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/webshop/PostOrder/${(await storage.getUserId())}',
      headers: await storage.getHeaders(),
      operation: operation[Operation.webShopCheckOut]!,
      data: {"CouponID": couponId},
      queryParameters: {"createinvoice": true},
      cancelToken: cancelToken,
    );

    return WebshopCheckoutResponseModel.fromJson(response);

    //
  }

  @override
  Future<WebshopOrdersResponseModel> getWebShopOrderHistory(
      {required int pageKey, required int invoiceId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path:
          '/webshop/GetOrders/${(await storage.getUserId())}/$invoiceId?page=$pageKey',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getWebShopOrderHistory]!,
      cancelToken: cancelToken,
    );

    return WebshopOrdersResponseModel.fromJson(response);

    //
  }

  @override
  Future<WebshopCouponResponseModel> webShopCheckCoupon(
      {required String couponCode}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/webshop/CheckCoupon/$couponCode/${(await storage.getUserId())}',
      headers: await storage.getHeaders(),
      operation: operation[Operation.webShopCheckCoupon]!,
      cancelToken: cancelToken,
    );

    return WebshopCouponResponseModel.fromJson(response);

    //
  }

  @override
  Future<WebshopWishlistResponseModel> getWebShotWishList() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/webshop/GetWishList/${(await storage.getUserId())}',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getWebShotWishList]!,
      cancelToken: cancelToken,
    );

    return WebshopWishlistResponseModel.fromJson(response);

    //
  }

  @override
  Future<WebshopDeliveryPriceResponseModel> getWebShopDeliveryCharge(
      {required String productGroup}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/webshop/GetWebshopDeliveryChargeDynamic/$productGroup',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getWebShopDeliveryCharge]!,
      cancelToken: cancelToken,
    );

    return WebshopDeliveryPriceResponseModel.fromJson(response);

    //
  }

  @override
  Future<GenericResponseModel> addRemoveWebShopWishList(
      {required int productId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: 'webshop/AddRemoveWishlist',
      headers: await storage.getHeaders(),
      operation: operation[Operation.addRemoveWebShopWishList]!,
      data: {"CompanyID": (await storage.getUserId()), "ProductID": productId},
      cancelToken: cancelToken,
    );

    return GenericResponseModel.fromJson(response);

    //
  }

  // Future<bool?> checkPayment({taggedId}) async {
  //   return await apiService.checkWebshopPayment(taggedId: taggedId);
  // }

  @override
  Future<TermsAndConditionsResponseModel> getWebShopTermsAndConditions() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/bitrix/getWebshopTermsAndConditions',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getWebShopTermsAndConditions]!,
      cancelToken: cancelToken,
    );

    return TermsAndConditionsResponseModel.fromJson(response);

    //
  }

  @override
  Future<String> getDigitalPackageImageUrl() async {
    final isImageCreatedLink =
        "https://widget5.haandvaerker.dk/prisen/winnersBadge/mail/rendered/${(await storage.getUserId())}/1.jpg";
    final imageNotCreated =
        "https://widget5.haandvaerker.dk/prisen/winnersBadge/mail/mailSignaturePng.php?id=${(await storage.getUserId())}";

    final response = await Dio().get(isImageCreatedLink);
    if (response.statusCode == 200) {
      return isImageCreatedLink;
    } else {
      return imageNotCreated;
    }
  }

  ///klima badges
  @override
  Future<String> getKlimaImageUrl() async {
    final isImageCreatedLink =
        "https://widget5.haandvaerker.dk/klima/rendered/klima_${(await storage.getUserId())}.jpg";

    try {
      //checks if the image url exist
      await Dio().get(isImageCreatedLink);
      return isImageCreatedLink;
    } catch (e) {
      log("getKlimaBadgeImageUrl error: $e");

      return '';
    }
  }

  /// badges
  @override
  Future<String> getBadgeImageUrl({required int index}) async {
    final isImageCreatedLink =
        "https://widget5.haandvaerker.dk/prisen/winnersBadge/mail/rendered/${(await storage.getUserId())}/${index + 1}.jpg";

    try {
      final response = await Dio().get(isImageCreatedLink);
      log('response: ${response.data.runtimeType}');

      return isImageCreatedLink;
    } catch (e) {
      log("$e");
      return '';
    }
  }

  @override
  Future<String> getSmallEmailImageUrl({required int index}) async {
    final isImageCreatedLink =
        "https://widget5.haandvaerker.dk/prisen/winnersBadge/mailsmall/rendered/${(await storage.getUserId())}/${index + 1}.jpg";

    try {
      //checks if the image url exist
      await Dio().get(isImageCreatedLink);

      return isImageCreatedLink;
    } catch (e) {
      log("getBadgeImageUrl error: $e");

      return '';
    }
  }

  /// streamer on car
  @override
  Future<String> getStreamerCarImageUrl({int index = 0}) async {
    final isImageCreatedLink =
        "https://widget5.haandvaerker.dk/prisen/winnersBadge/bladder/rendered/${(await storage.getUserId())}/${index + 1}.jpg";
    final urlToCreateImages =
        "https://widget5.haandvaerker.dk/prisen/winnersBadge/bladder/bladderPng.php?id=${(await storage.getUserId())}";

    try {
      //checks if the image url exist
      final response = await Dio().get(isImageCreatedLink);
      if (response.statusCode == 200) {
        return isImageCreatedLink;
      } else {
        //trigger request to create images
        await Dio().get(urlToCreateImages);
        return isImageCreatedLink;
      }
    } catch (e) {
      log('getStreamerCarImageUrl error: $e');
      //trigger request to create images
      await Dio().get(urlToCreateImages);
      return isImageCreatedLink;
    }
  }

  Future<String> createStreamersUrl() async {
    final urlToCreateImages =
        "https://widget5.haandvaerker.dk/prisen/winnersBadge/streamer/streamerPng.php?id=${(await storage.getUserId())}";
    return urlToCreateImages;
  }

  /// streamer only, [index] = `0` means the latest
  @override
  Future<String> getStreamerImageUrl({int index = 0}) async {
    final isImageCreatedLink =
        'https://widget5.haandvaerker.dk/prisen/winnersBadge/streamer/rendered/${(await storage.getUserId())}/${index + 1}.jpg';
    try {
      //checks if the image url exist
      await Dio().get(isImageCreatedLink);

      return isImageCreatedLink;
    } catch (e) {
      log("getStreamerImageUrl error: $e");

      return '';
    }
  }

  // for streamers only
  @override
  Future<WsAddProductModel> createWebShopStreamerProductVariationsManually({
    required String productName,
    required double productPrice,
    required badgeId,
    required String imageURL,
    required categoryId,
  }) async {
    //

    const staticDesc =
        "Skal du have mere opmærksomhed på vejene? Pynt bilen med flere streamers her";

    final map = {
      "product_name": productName,
      "product_price": productPrice,
      "supplier_id": (await storage.getUserId()),
      "badge_id": badgeId,
      "image_url": imageURL,
      "category_id": categoryId,
      "description": staticDesc
    };

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: 'webshop/AddProduct',
      headers: await storage.getHeaders(),
      operation:
          operation[Operation.createWebShopStreamerProductVariationsManually]!,
      data: map,
      cancelToken: cancelToken,
    );

    return WsAddProductModel.fromJson(response);

    //
  }

  // for addOn for Graphics, create separate product
  @override
  Future<WsAddProductModel> createWebShopGraphicHelpProduct({
    required String productName,
    double productPrice = 0.0,
    badgeId,
    required String imageUrl,
    required String productGroup,
    required categoryId,
    String description =
        "Dette viser, at denne virksomhed har brug for hjælp til hans/hendes grafik til dette specifikke produkt",
    required double pdfUnitPrice,
    String pdfUrl = '',
  }) async {
    //

    final map = {
      "product_name": productName,
      "product_price": productPrice,
      "supplier_id": (await storage.getUserId()),
      "badge_id": badgeId,
      "image_url": imageUrl,
      "product_group": productGroup,
      "category_id": categoryId,
      "description": description,
      "pdf_graphic_need": "Yes",
      "pdf_unit_price": pdfUnitPrice,
      "pdf_url": pdfUrl
    };

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: 'webshop/AddProduct',
      headers: await storage.getHeaders(),
      operation: operation[Operation.createWebShopGraphicHelpProduct]!,
      data: map,
      cancelToken: cancelToken,
    );

    return WsAddProductModel.fromJson(response);

    //
  }
}
