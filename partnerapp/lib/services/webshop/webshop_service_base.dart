import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_add_product_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_cart_response_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_category_response_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_checkout_response_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_coupon_response_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_delivery_price_response_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_orders_response_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_product_response_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_terms_and_conditions_model.dart';
import 'package:Haandvaerker.dk/model/webshop/ws_wishlist_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class WebShopServiceBase {
  //

  Future<WebshopProductResponseModel> getWebShopProduct(
      {int productId, int productName, int categoryId, int page});

  Future<WebshopCategoryResponseModel> getWebShopCategory({int categoryId});

  Future<WebshopCartResponseModel> getWebShopCartData();

  Future<GenericResponseModel> webShopModifyCart(
      {required bool isUpdate, required int productId, required int quantity});

  Future<GenericResponseModel> webShopAddCartItem({
    required int addUpdateFlag,
    required String productGroup,
    required int productId,
    List<String>? logo,
    required int quantity,
  });

  Future<WebshopCheckoutResponseModel> webShopCheckOut({required int couponId});

  Future<WebshopOrdersResponseModel> getWebShopOrderHistory(
      {required int pageKey, required int invoiceId});

  Future<WebshopCouponResponseModel> webShopCheckCoupon(
      {required String couponCode});

  Future<WebshopWishlistResponseModel> getWebShotWishList();

  Future<WebshopDeliveryPriceResponseModel> getWebShopDeliveryCharge(
      {required String productGroup});

  Future<GenericResponseModel> addRemoveWebShopWishList(
      {required int productId});

  //STILL IN KUNDEMATCH
  // Future<bool?> checkWebShopPayment({required int taggedId});

  Future<TermsAndConditionsResponseModel> getWebShopTermsAndConditions();

  //STILL IN KUNDEMATCH
  // Future<ChangeCreditResponseModel> confirmWebShopChangeCredit();

  //STILL IN KUNDEMATCH
  // Future<GenericResponseModel> webShopAdsStat(
  //     {required bool isFacebook, required adsId});

  Future<String> getDigitalPackageImageUrl();

  Future<String?> getKlimaImageUrl();

  Future<String?> getBadgeImageUrl({required int index});

  Future<String?> getSmallEmailImageUrl({required int index});

  Future<String> getStreamerCarImageUrl({int index = 0});

  Future<String?> getStreamerImageUrl({int index = 0});

  Future<WsAddProductModel> createWebShopStreamerProductVariationsManually({
    required String productName,
    required double productPrice,
    required badgeId,
    required String imageURL,
    required categoryId,
  });

  Future<WsAddProductModel> createWebShopGraphicHelpProduct({
    required String productName,
    required double productPrice,
    badgeId,
    required String imageUrl,
    required String productGroup,
    required categoryId,
    required String description,
    required double pdfUnitPrice,
    required String pdfUrl,
  });

  //
}
