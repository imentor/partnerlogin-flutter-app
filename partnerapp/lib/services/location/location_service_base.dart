import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class LocationServiceBase {
  //

  Future<MinboligApiResponse?> sendLocation(
      {required String lat,
      required String lng,
      required String speed,
      required String floor});

  Future<MinboligApiResponse> updateLocationInfo({required int gpsLocation});

  Future<Map<String, dynamic>> getHeaders();

  bool getLocationDialog();

  void setLocationDialog();

  //
}
