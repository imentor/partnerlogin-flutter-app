import 'dart:convert';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/location/location_service_base.dart';
import 'package:dio/dio.dart';

class LocationService extends LocationServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  LocationService({required this.apiService, required this.storage});

  @override
  Future<MinboligApiResponse?> sendLocation(
      {required String lat,
      required String lng,
      required String speed,
      required String floor}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/addGeoLocation',
      headers: await storage.getHeaders(),
      data: jsonEncode({
        "lat": lat,
        "lang": lng,
        "speed": speed,
        "floor": floor,
      }),
      operation: 'addGeoLocation',
      cancelToken: cancelToken,
    );
    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }

    //
  }

  @override
  Future<MinboligApiResponse> updateLocationInfo(
      {required int gpsLocation}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/update',
      headers: await storage.getHeaders(),
      queryParameters: {"gpsLocation": gpsLocation},
      operation: 'updateLocationInfo',
      cancelToken: cancelToken,
    );
    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<Map<String, dynamic>> getHeaders() async {
    return await storage.getHeaders();
  }

  @override
  bool getLocationDialog() {
    return storage.getLocationDialogData();
  }

  @override
  void setLocationDialog() {
    storage.setLocationDialogData(value: true);
  }

  //
}
