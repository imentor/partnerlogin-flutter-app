import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/invitation/invitation_service_base.dart';
import 'package:dio/dio.dart';

class InvitationService extends InvitationServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  InvitationService({required this.apiService, required this.storage});

  @override
  Future<MinboligApiResponse> projectInvitations() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/projectInvitations',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getProjectInvitations]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> reserveProject({required projectId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/project/$projectId/reserve',
      headers: await storage.getHeaders(),
      operation: operation[Operation.deleteReviewById]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> acceptOrRejectInvite(
      {required invitationId, required accepted}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/projectInvitation/$invitationId',
      headers: await storage.getHeaders(),
      data: {"accepted": accepted},
      operation: operation[Operation.acceptOrRejectInvite]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  //
}
