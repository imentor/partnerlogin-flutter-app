import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class InvitationServiceBase {
  //

  Future<MinboligApiResponse> projectInvitations();

  Future<MinboligApiResponse> reserveProject({required projectId});

  Future<MinboligApiResponse> acceptOrRejectInvite(
      {required invitationId, required accepted});

  //
}
