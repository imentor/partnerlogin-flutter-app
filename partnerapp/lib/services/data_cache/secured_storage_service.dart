import 'dart:developer';

import 'package:Haandvaerker.dk/utils/encryption_helper.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum StorageData {
  haaid,
  email,
  password,
  rememberMe,
  bitrixToken,
  language,
  // initialAppLink,
  fcmToken,
  companyName,
  encryptionKey,
  encryptionIv,
  tokenDateValidity
}

enum SharedData { locationDialogShowedOnce, initialAppLink }

class SecuredStorageService {
  SecuredStorageService(
      {required this.storage, required this.sharedPreferences});
  final FlutterSecureStorage storage;
  final SharedPreferences sharedPreferences;

  static Map<StorageData, String> mapKey = {
    StorageData.haaid: 'haaid',
    StorageData.email: 'email',
    StorageData.password: 'password',
    StorageData.rememberMe: 'remember_me',
    StorageData.bitrixToken: 'bitrix_token',
    StorageData.language: 'language',
    StorageData.fcmToken: 'fcm_token',
    StorageData.companyName: 'company_name',
    StorageData.encryptionKey: 'encryption_key',
    StorageData.encryptionIv: 'encryption_iv',
    StorageData.tokenDateValidity: 'tokenDateValidity'
  };

  static Map<SharedData, String> sharedMapKey = {
    SharedData.locationDialogShowedOnce: 'location_dialog_showed_once',
    SharedData.initialAppLink: 'initialAppLink',
  };

  void setLocationDialogData({required bool value}) {
    sharedPreferences.setBool(
        sharedMapKey[SharedData.locationDialogShowedOnce]!, value);
  }

  bool getLocationDialogData() {
    return sharedPreferences
            .getBool(sharedMapKey[SharedData.locationDialogShowedOnce]!) ??
        false;
  }

  bool? getLocationDialog() {
    return sharedPreferences
        .getBool(sharedMapKey[SharedData.locationDialogShowedOnce]!);
  }

  void setSharedData(Map<SharedData, dynamic> data) {
    for (var element in data.entries) {
      if (element.value is String) {
        sharedPreferences.setString(sharedMapKey[element.key]!, element.value);
      } else if (element.value is num) {
        sharedPreferences.setDouble(sharedMapKey[element.key]!, element.value);
      } else if (element.value is bool) {
        sharedPreferences.setBool(sharedMapKey[element.key]!, element.value);
      }
    }
  }

  Future<AesKeys> getEncryptionKeys() async {
    final encryptionKey =
        await storage.read(key: mapKey[StorageData.encryptionKey]!);
    final encryptionIv =
        await storage.read(key: mapKey[StorageData.encryptionIv]!);

    final AesKeys keys =
        AesKeys(key: encryptionKey ?? '', iv: encryptionIv ?? '');

    return keys;
  }

  Future<void> setData(Map<StorageData, dynamic> data,
      {int retryCount = 0}) async {
    final encryptionKeys = await getEncryptionKeys();

    try {
      await Future.wait(
        data.entries.map(
          (entry) => storage.write(
              key: mapKey[entry.key]!,
              value:
                  EncryptionHelper.encrypt('${entry.value}', encryptionKeys)),
        ),
      );
    } catch (e) {
      if (e is ArgumentError &&
          e.message == 'Invalid key or IV length' &&
          retryCount < 3) {
        retryCount++;

        final generatedEncryption = EncryptionHelper.generateRandomKeyAndIV();

        await storage.write(
            key: 'encryption_key', value: generatedEncryption.key);
        await storage.write(
            key: 'encryption_iv', value: generatedEncryption.iv);

        await setData(data);
      } else {
        log('Error in setData: $e');
      }
    }
  }

  Future<void> setSpecificData(StorageData key, dynamic value,
      {int retryCount = 0}) async {
    final encryptionKeys = await getEncryptionKeys();
    try {
      await storage.write(
          key: mapKey[key]!,
          value: EncryptionHelper.encrypt('$value', encryptionKeys));
    } catch (e) {
      if (e is ArgumentError &&
          e.message == 'Invalid key or IV length' &&
          retryCount < 3) {
        retryCount++;

        final generatedEncryption = EncryptionHelper.generateRandomKeyAndIV();

        await storage.write(
            key: 'encryption_key', value: generatedEncryption.key);
        await storage.write(
            key: 'encryption_iv', value: generatedEncryption.iv);

        await setSpecificData(key, value);
      } else {
        log('Error in setData: $e');
      }
    }
  }

  Future<bool> clearStorage() async {
    try {
      final rememberMe = await getDecryptedData(StorageData.rememberMe);

      String? appLink =
          sharedPreferences.getString(sharedMapKey[SharedData.initialAppLink]!);

      if (rememberMe == 'true') {
        final tempEmail =
            (await storage.read(key: mapKey[StorageData.email]!)) ?? '';
        final tempPassword =
            (await storage.read(key: mapKey[StorageData.password]!)) ?? '';
        final tempRememberMe =
            (await storage.read(key: mapKey[StorageData.rememberMe]!)) ?? '';
        final tempEncryptionKey =
            (await storage.read(key: mapKey[StorageData.encryptionKey]!)) ?? '';
        final tempEncryptionIv =
            (await storage.read(key: mapKey[StorageData.encryptionIv]!)) ?? '';

        await storage.deleteAll();

        await Future.value([
          storage.write(key: mapKey[StorageData.email]!, value: tempEmail),
          storage.write(
              key: mapKey[StorageData.password]!, value: tempPassword),
          storage.write(
              key: mapKey[StorageData.rememberMe]!, value: tempRememberMe),
          storage.write(
              key: mapKey[StorageData.encryptionKey]!,
              value: tempEncryptionKey),
          storage.write(
              key: mapKey[StorageData.encryptionIv]!, value: tempEncryptionIv),
        ]);
      } else {
        await storage.deleteAll();

        final generatedEncryption = EncryptionHelper.generateRandomKeyAndIV();

        await storage.write(
            key: 'encryption_key', value: generatedEncryption.key);
        await storage.write(
            key: 'encryption_iv', value: generatedEncryption.iv);
      }

      await sharedPreferences.clear();

      if (appLink != null) {
        sharedPreferences.setString(
            sharedMapKey[SharedData.initialAppLink]!, appLink);
      }
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<Map<String, dynamic>> getHeaders() async {
    final encryptionKeys = await getEncryptionKeys();
    String languageCode = 'da';

    if ((await getDecryptedData(StorageData.language)) == 'English') {
      languageCode = 'en';
    }

    final token = await storage.read(key: mapKey[StorageData.bitrixToken]!);
    return token != null
        ? {
            "Authorization":
                "Bearer ${EncryptionHelper.decrypt(token, encryptionKeys)}",
            "X-localization": languageCode
          }
        : {};
  }

  Future<String> getDecryptedData(StorageData storageKey) async {
    final encryptionKeys = await getEncryptionKeys();

    if (mapKey[storageKey] != null) {
      final storageValue = (await storage.read(key: mapKey[storageKey]!)) ?? '';

      return storageValue.isEmpty
          ? ''
          : EncryptionHelper.decrypt(storageValue, encryptionKeys);
    } else {
      return '';
    }
  }

  Future<String> getCompanyName() async {
    return getDecryptedData(StorageData.companyName);
    // return (await storage.read(key: mapKey[StorageData.companyName]!)) ?? '';
  }

  Future<String> getUserId() async {
    return getDecryptedData(StorageData.haaid);
    // return (await storage.read(key: mapKey[StorageData.haaid]!)) ?? '';
  }

  Future<String> getLanguage() async {
    return getDecryptedData(StorageData.language);
    // return (await storage.read(key: mapKey[StorageData.language]!)) ?? '';
  }

  Future<String> getEmail() async {
    return getDecryptedData(StorageData.email);
    // return (await storage.read(key: mapKey[StorageData.email]!)) ?? '';
  }

  String? getInitialAppLink() {
    return sharedPreferences
        .getString(sharedMapKey[SharedData.initialAppLink]!);
  }

  Future<String> getFcmToken() async {
    return getDecryptedData(StorageData.fcmToken);
    // return (await storage.read(key: mapKey[StorageData.fcmToken]!)) ?? '';
  }

  Future<String> getBitrixToken() async {
    return getDecryptedData(StorageData.bitrixToken);
    // return (await storage.read(key: mapKey[StorageData.bitrixToken]!)) ?? '';
  }

  Future<Map<String, dynamic>> getRememberMe() async {
    try {
      final rememberMe = await getDecryptedData(StorageData.rememberMe);
      if (rememberMe == 'true') {
        return {
          'userName': await getDecryptedData(StorageData.email),
          'password': await getDecryptedData(StorageData.password)
        };
      } else {
        return {};
      }
    } catch (e) {
      return {};
    }
  }
}
