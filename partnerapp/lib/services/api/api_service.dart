// ignore_for_file: constant_identifier_names
import 'dart:developer';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/utils/application.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class ApiException implements Exception {
  final String message;

  ApiException({required this.message});
}

class CancelException implements Exception {}

enum RequestMethod { GET, POST, PUT, DELETE }

class ApiService {
  http.Client client;
  late Dio dio;

  ApiService({required this.client, required this.dio});

  Future<dynamic> makeRequest({
    required RequestMethod method,
    String path = '',
    String? baseUrl,
    String? alternateUrl,
    dynamic data,
    required String operation,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? queryParameters,
    required CancelToken cancelToken,
  }) async {
    cancelToken = CancelToken();
    // log('STARTED $path request: ${cancelToken.isCancelled}');
    try {
      Response response;
      String url = alternateUrl ?? "${baseUrl ?? applic.bitrixApiUrl}$path";

      switch (method) {
        case RequestMethod.GET:
          response = await dio.get(
            url,
            queryParameters: queryParameters,
            options: headers == null ? null : Options(headers: headers),
            cancelToken: cancelToken,
          );
          break;
        case RequestMethod.POST:
          response = await dio.post(
            url,
            queryParameters: queryParameters,
            data: data,
            options: headers == null ? null : Options(headers: headers),
            cancelToken: cancelToken,
          );
          break;
        case RequestMethod.PUT:
          response = await dio.put(
            url,
            queryParameters: queryParameters,
            data: data,
            options: headers == null ? null : Options(headers: headers),
            cancelToken: cancelToken,
          );
          break;
        case RequestMethod.DELETE:
          response = await dio.delete(
            url,
            queryParameters: queryParameters,
            data: data,
            options: headers == null ? null : Options(headers: headers),
            cancelToken: cancelToken,
          );
          break;
      }

      final responseData = response.data;

      if (response.data is Map<String, dynamic>) {
        if (!responseData['success']) {
          if (kDebugMode) {
            log('$operation failed with status code ${response.statusCode}: ${responseData['message']}');
          }
        }
      }

      return responseData;
    } on DioException catch (e) {
      if (e.type == DioExceptionType.connectionTimeout) {
        // Caused by a connection timeout.
        throw ApiException(message: 'Connection timeout');
      } else if (e.type == DioExceptionType.sendTimeout) {
        // It occurs when url is sent timeout.
        throw ApiException(message: 'Send timeout');
      } else if (e.type == DioExceptionType.receiveTimeout) {
        // It occurs when receiving timeout.
        throw ApiException(message: 'Receive timeout');
      } else if (e.type == DioExceptionType.badCertificate) {
        // Caused by an incorrect certificate as configured by [ValidateCertificate].
        throw ApiException(message: 'Bad certificate');
      } else if (e.type == DioExceptionType.badResponse) {
        // The [DioException] was caused by an incorrect status code as configured by [ValidateStatus].
        if (e.response != null && e.response!.statusCode != null) {
          if (e.response!.data.runtimeType != String) {
            final responseData = MinboligApiResponse.fromJson(
                e.response!.data as Map<String, dynamic>);
            final errorMessage = responseData.message ?? 'Bad response';
            throw ApiException(message: errorMessage);
          } else {
            throw ApiException(message: e.response!.data);
          }
        } else {
          throw ApiException(message: 'Bad response');
        }
      } else if (e.type == DioExceptionType.connectionError) {
        // Caused for example by a `xhr.onError` or SocketExceptions.
        throw ApiException(message: 'Connection error');
      } else if (e.type == DioExceptionType.cancel) {
        // When the request is cancelled, dio will throw a error with this type.
        cancelToken.cancel();
      } else if (e.response != null && e.response!.statusCode != null) {
        final responseData = MinboligApiResponse.fromJson(
            e.response!.data as Map<String, dynamic>);
        final errorMessage = responseData.message ?? 'Unknown error';
        throw ApiException(message: errorMessage);
      } else {
        throw ApiException(message: 'Unexpected error occurred');
      }
    } catch (e) {
      // Catching generic exceptions
      log(e.toString());
      throw ApiException(message: 'Unexpected error occurred');
    } finally {
      cancelToken.cancel();
    }
  }
}
