import 'dart:convert';

import 'package:Haandvaerker.dk/main.dart';
import 'package:Haandvaerker.dk/services/push_notification/push_notification_service.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

Future onSelectNotification(NotificationResponse? response) async {
  if (response != null) {
    final payloadObject = jsonDecode(response.payload!);
    final customData = jsonDecode(payloadObject['custom_data']);
    navigateToScreen(payloadObject['navigate_to'], argument: customData);
  }
}

Future<void> notification(
  String title,
  String body,
  String payload,
) async {
  const androidNotificationDetails = AndroidNotificationDetails(
    'dk.haandvaerker.partnerapp.channel',
    'Haandvaerker.dk channel',
    channelDescription: 'A description of the notification.',
    priority: Priority.high,
    importance: Importance.max,
    ticker: 'test',
  );

  const iosNotificationDetails = DarwinNotificationDetails();

  const notificationDetails = NotificationDetails(
    android: androidNotificationDetails,
    iOS: iosNotificationDetails,
  );

  await flutterLocalNotificationsPlugin
      .show(0, title, body, notificationDetails, payload: payload);
}
