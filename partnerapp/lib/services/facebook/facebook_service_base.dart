import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class FilesServiceBase {
  Future<MinboligApiResponse?> getPartnerFacebookPages();
  Future<MinboligApiResponse> activateDeactivatePost(
      {required bool isActivate, required int id});
  Future<MinboligApiResponse> deleteFacebookPages({required int id});
  Future<MinboligApiResponse> facebookRelogin();
}
