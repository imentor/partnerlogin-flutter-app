// import 'package:Haandvaerker.dk/model/generic_response_model.dart';
// import 'package:Haandvaerker.dk/model/webshop/ws_facebook_ads_response_model.dart';
import 'dart:convert';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/facebook/facebook_service_base.dart';
import 'package:dio/dio.dart';

class FacebookService extends FilesServiceBase {
  FacebookService({required this.storage, required this.apiService});

  final ApiService apiService;
  final SecuredStorageService storage;

  @override
  Future<MinboligApiResponse?> getPartnerFacebookPages() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/facebook/getPartnerFacebookPages/${(await storage.getUserId())}',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getPartnerFacebookPages]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }
  }

  @override
  Future<MinboligApiResponse> activateDeactivatePost(
      {required bool isActivate, required int id}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: isActivate
          ? '/facebook/activatePostPartnerFacebook'
          : '/facebook/deactivatePostPartnerFacebook',
      headers: await storage.getHeaders(),
      data: jsonEncode({"levId": (await storage.getUserId()), "id": id}),
      operation: operation[Operation.activateDeactivatePost]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> deleteFacebookPages({required int id}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/facebook/DeleteFacebookPage/$id/${(await storage.getUserId())}',
      headers: await storage.getHeaders(),
      operation: operation[Operation.deleteFacebookPages]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> facebookRelogin() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/facebook/relogin',
      headers: await storage.getHeaders(),
      operation: operation[Operation.facebookRelogin]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }
}
