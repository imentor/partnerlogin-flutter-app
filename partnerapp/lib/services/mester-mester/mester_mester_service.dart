import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/mester_til_mester/mester_to_mester_job_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/mester-mester/mester_mester_service_base.dart';
import 'package:dio/dio.dart';

class MesterMesterService extends MesterMesterServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  MesterMesterService({required this.apiService, required this.storage});

  @override
  Future<bool> reserveMesterJob(
      {required String partnerId, required int mesterId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/mester_til_mester/reserve',
      headers: await storage.getHeaders(),
      data: {
        'mesterId': mesterId,
        'partnerId': partnerId,
      },
      operation: operation[Operation.reserveMesterJob]!,
      cancelToken: cancelToken,
    );

    return response['success'];

    //
  }

  @override
  Future<MesterToMesterJobModel> getMesterJobs({
    required int page,
    int size = 10,
    required int type,
    required bool getAll,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: getAll
          ? '/partner/mester_til_mester/all'
          : '/partner/mester_til_mester',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getMesterJobs]!,
      queryParameters: {
        'page': page,
        'size': size,
        'type': type == 0 ? 'opgave' : 'arbejdskraft',
      },
      cancelToken: cancelToken,
    );

    return MesterToMesterJobModel.fromJson(
        Map<String, dynamic>.from(response['data']));

    //
  }

  @override
  Future<MesterToMesterJobItem> submitLaborOrTask(
      {required Map<String, dynamic> payload}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/mester_til_mester',
      headers: await storage.getHeaders(),
      data: FormData.fromMap(payload),
      operation: operation[Operation.submitLaborOrTask]!,
      cancelToken: cancelToken,
    );

    return MesterToMesterJobItem.fromJson(
        Map<String, dynamic>.from(response['data']));

    //
  }

  @override
  Future<bool> deleteLaborOrTask({required int mesterId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/mester_til_mester/delete',
      headers: await storage.getHeaders(),
      data: {'mesterId': mesterId},
      operation: operation[Operation.deleteLaborOrTask]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response).success!;

    //
  }

  //
}
