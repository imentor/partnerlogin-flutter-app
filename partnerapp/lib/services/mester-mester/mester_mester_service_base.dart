import 'package:Haandvaerker.dk/model/mester_til_mester/mester_to_mester_job_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class MesterMesterServiceBase {
  Future<bool> reserveMesterJob(
      {required String partnerId, required int mesterId});

  Future<MesterToMesterJobModel> getMesterJobs({
    required int page,
    int size = 10,
    required int type,
    required bool getAll,
  });

  Future<MesterToMesterJobItem> submitLaborOrTask(
      {required Map<String, dynamic> payload});

  Future<bool> deleteLaborOrTask({required int mesterId});
}
