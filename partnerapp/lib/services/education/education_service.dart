import 'dart:io';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/education/education_service_base.dart';
import 'package:dio/dio.dart';

class EducationService extends EducationServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  EducationService({required this.apiService, required this.storage});

  @override
  Future<MinboligApiResponse?> getCertificates() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/certificates',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getCertificates]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;

    //
  }

  @override
  Future<bool> addCertificate({
    required int industryId,
    required File file,
    required String name,
    required String branch,
  }) async {
    //

    final payload = FormData.fromMap({
      'name': name,
      'BRANCHE': branch,
      'industryId': industryId,
      'file': await MultipartFile.fromFile(file.path,
          filename: file.path.split('/').last),
    });

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/certificate',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.addCertificate]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response).success!;

    //
  }

  @override
  Future<bool> deleteCertificate({required int certificateId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/deleteCertificate',
      headers: await storage.getHeaders(),
      data: {'certificateId': certificateId},
      operation: operation[Operation.deleteReviewById]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response).success!;

    //
  }

  //
}
