import 'dart:io';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class EducationServiceBase {
  //

  Future<MinboligApiResponse?> getCertificates();

  Future<bool> addCertificate({
    required int industryId,
    required File file,
    required String name,
    required String branch,
  });

  Future<bool> deleteCertificate({required int certificateId});

  //
}
