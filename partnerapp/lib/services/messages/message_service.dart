import 'dart:convert';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/messages/contacts_response_model.dart';
import 'package:Haandvaerker.dk/model/messages/message_v2_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/messages/message_service_base.dart';
import 'package:dio/dio.dart';

class MessageService extends MessageServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  MessageService({required this.apiService, required this.storage});

  @override
  Future<MessageV2ResponseModel?> getMessagesV2({
    required Map<String, dynamic> params,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/messagesV2',
      headers: await storage.getHeaders(),
      queryParameters: params,
      operation: operation[Operation.get_messages_v2]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MessageV2ResponseModel.fromJson(
          Map<String, dynamic>.from(response['data']));
    } else {
      return null;
    }

    //
  }

  @override
  Future<List<MessageV2Items>?> getMessageThread(
      {required int messageId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/messages/$messageId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.get_message_thread]!,
      cancelToken: cancelToken,
    );

    return response != null
        ? MessageV2Items.fromCollection(List<dynamic>.from(response['data']))
        : null;

    //
  }

  @override
  Future<List<ContactsResponseModel>?> getContacts() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/contacts',
      headers: await storage.getHeaders(),
      operation: operation[Operation.get_contacts]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return ContactsResponseModel.fromCollection(
          List<dynamic>.from(response['data']));
    } else {
      return null;
    }

    //
  }

  @override
  Future<MinboligApiResponse?> markAsSeen({
    required int messageId,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.PUT,
      path: '/partner/message/$messageId/seen',
      headers: await storage.getHeaders(),
      operation: operation[Operation.mark_as_seen]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }

    //
  }

  @override
  Future<MinboligApiResponse?> markAllAsSeen() async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/message/notifications/seen',
      headers: await storage.getHeaders(),
      operation: operation[Operation.mark_all_as_seen]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }
  }

  @override
  Future<MinboligApiResponse> postMessageAction(
      {required messageActionUrl}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      alternateUrl: messageActionUrl,
      headers: await storage.getHeaders(),
      queryParameters: {"sent": "true"},
      operation: operation[Operation.postMessageAction]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> sendProjectMessageThread({
    required contactId,
    required title,
    required body,
    required threadId,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/messages/$threadId/reply',
      headers: await storage.getHeaders(),
      data: {
        'contactId': contactId,
        'title': title,
        'message': body,
      },
      operation: operation[Operation.sendProjectMessageThread]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> sendNewProjectMessage({
    required contactId,
    required title,
    required body,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/message',
      headers: await storage.getHeaders(),
      data: {
        'contactId': contactId,
        'title': title,
        'message': body,
      },
      operation: operation[Operation.sendNewProjectMessage]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> getProjectMessages(
      {required int projectId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/messages',
      headers: await storage.getHeaders(),
      queryParameters: {'projectId': projectId},
      operation: operation[Operation.getMessageByProjectId]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> getProjectMessageThread(
      {required threadId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/messages/$threadId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getProjectMessageThread]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse> deleteMessage({required int messageId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/message/delete',
      headers: await storage.getHeaders(),
      data: jsonEncode({"messageId": messageId}),
      operation: operation[Operation.getProjectMessageThread]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  //
}
