import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/messages/contacts_response_model.dart';
import 'package:Haandvaerker.dk/model/messages/message_v2_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class MessageServiceBase {
  //

  Future<MessageV2ResponseModel?> getMessagesV2(
      {required Map<String, dynamic> params});

  Future<List<MessageV2Items>?> getMessageThread({required int messageId});

  Future<List<ContactsResponseModel>?> getContacts();

  Future<MinboligApiResponse?> markAsSeen({required int messageId});

  Future<MinboligApiResponse?> markAllAsSeen();

  Future<MinboligApiResponse> postMessageAction({required messageActionUrl});

  Future<MinboligApiResponse> sendProjectMessageThread({
    required contactId,
    required title,
    required body,
    required threadId,
  });

  Future<MinboligApiResponse> sendNewProjectMessage({
    required contactId,
    required title,
    required body,
  });

  Future<MinboligApiResponse> getProjectMessages({required int projectId});

  Future<MinboligApiResponse> getProjectMessageThread({required threadId});

  Future<MinboligApiResponse> deleteMessage({required int messageId});

  //
}
