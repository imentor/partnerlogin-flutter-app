import 'package:Haandvaerker.dk/model/questions/questions_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class QuestionServiceBase {
  //

  Future<List<QuestionResponseModel>> getAllQuestions(
      {required int projectParentId});

  Future<List<ChildQuestionResponseModel>> getChildQuestions(
      {required int projectId, required int parentId});

  Future<QuestionResponseModel> addQuestion({
    required int projectId,
    required String contactId,
    required String descriptionId,
    required String industryId,
    required String jobIndustryId,
    required String question,
  });

  Future<ChildQuestionResponseModel> replyQuestion({
    required int projectId,
    required int parentId,
    required String message,
  });

  //
}
