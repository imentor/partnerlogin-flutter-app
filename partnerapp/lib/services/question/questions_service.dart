import 'package:Haandvaerker.dk/model/questions/questions_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/question/question_service_base.dart';
import 'package:dio/dio.dart';

class QuestionsService extends QuestionServiceBase {
  QuestionsService({required this.apiService, required this.storage});

  final ApiService apiService;
  final SecuredStorageService storage;

  @override
  Future<List<QuestionResponseModel>> getAllQuestions(
      {required int projectParentId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/project/$projectParentId/getAllQuestions',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getAllQuestions]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      if (response['data'] != null || (response['data'] as List).isNotEmpty) {
        return QuestionResponseModel.fromCollection(
            response['data'] as List<dynamic>);
      } else {
        return <QuestionResponseModel>[];
      }
    } else {
      return <QuestionResponseModel>[];
    }
    //
  }

  @override
  Future<List<ChildQuestionResponseModel>> getChildQuestions(
      {required int projectId, required int parentId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/project/$projectId/getQuestion/$parentId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getChildQuestions]!,
      cancelToken: cancelToken,
    );

    return ChildQuestionResponseModel.fromCollection(
        response['data'] as List<dynamic>);
    //
  }

  @override
  Future<QuestionResponseModel> addQuestion({
    required int projectId,
    required String contactId,
    required String descriptionId,
    required String industryId,
    required String jobIndustryId,
    required String question,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/project/$projectId/addQuestion',
      headers: await storage.getHeaders(),
      data: {
        'contactId': contactId,
        'descriptionId': descriptionId,
        'industryId': industryId,
        'jobIndustryId': jobIndustryId,
        'question': question,
      },
      operation: operation[Operation.addQuestion]!,
      cancelToken: cancelToken,
    );

    return QuestionResponseModel.fromJson(
        Map<String, dynamic>.from(response['data']));

    //
  }

  @override
  Future<ChildQuestionResponseModel> replyQuestion({
    required int projectId,
    required int parentId,
    required String message,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/project/$projectId/replyQuestion',
      headers: await storage.getHeaders(),
      data: {
        'parentId': parentId,
        'message': message,
      },
      operation: operation[Operation.replyQuestion]!,
      cancelToken: cancelToken,
    );

    return ChildQuestionResponseModel.fromJson(
        Map<String, dynamic>.from(response['data']));

    //
  }
}
