import 'package:flutter/material.dart';

@immutable
abstract class PushNotificationServiceBase {
  //

  Future initialise();

  Future registerToken(String? token);

  Future registerTokenV2(String? token);

  // Future<String> getId(BuildContext context);

  //
}
