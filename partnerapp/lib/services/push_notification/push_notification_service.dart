// ignore_for_file: use_build_context_synchronously

import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:Haandvaerker.dk/model/messages/message_v2_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/local_notification/local_notification_service.dart';
import 'package:Haandvaerker.dk/services/push_notification/push_notification_service_base.dart';
import 'package:Haandvaerker.dk/ui/routing/route_utils.dart';
import 'package:Haandvaerker.dk/ui/routing/routes_paths.dart';
import 'package:Haandvaerker.dk/utils/constants.dart';
import 'package:Haandvaerker.dk/utils/global_keys.dart';
import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:package_info_plus/package_info_plus.dart';

enum Topics {
  newsfeed,
  general,
}

class PushNotificationService extends PushNotificationServiceBase {
  PushNotificationService({required this.storage, required this.apiService});
  final SecuredStorageService storage;
  final ApiService apiService;

  final Map<Topics, String> _topics = {
    Topics.newsfeed: 'Newsfeed',
    Topics.general: 'General',
  };

  final FirebaseMessaging _fcm = FirebaseMessaging.instance;

  @override
  Future initialise() async {
    String? token;

    await _fcm.requestPermission();
    if (Platform.isIOS) {
      await Future.delayed(const Duration(seconds: 5));

      await _fcm.getAPNSToken();
    }

    token = await _fcm.getToken();

    // await registerToken(token);
    await registerTokenV2(token);

    await _fcm.setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );

    FirebaseMessaging.onBackgroundMessage(myBackgroundMessageHandler);

    FirebaseMessaging.onMessage.listen((message) async {
      if (message.notification != null) {
        final notificationDetails = message.notification!;
        final data = message.data;
        final silent = data['is_silent'];
        try {
          if (silent == 'false') {
            await notification(
              notificationDetails.body ?? '',
              notificationDetails.title ?? '',
              jsonEncode(data),
            );
          }
        } on Exception catch (e) {
          log('Error handling notification: $e');
        }
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      Map<String, dynamic> data = {};

      final initMessage = await _fcm.getInitialMessage();

      if (initMessage != null) {
        data = initMessage.data;
      } else {
        data = message.data;
      }

      final navigate = data['navigate_to'];
      final customData = jsonDecode(data['custom_data']);
      navigateToScreen(navigate, argument: customData);
    });
  }

  @override
  Future registerToken(String? token) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/checkDeviceToken/$token',
      headers: await storage.getHeaders(),
      operation: operation[Operation.setFcmToken]!,
      cancelToken: cancelToken,
    );

    if (response['success']) {
      await storage.setSpecificData(StorageData.fcmToken, token);
    }

    //
  }

  @override
  Future registerTokenV2(String? token) async {
    final pInfo = await PackageInfo.fromPlatform();
    final payload = {
      'deviceToken': token,
      'platform': Platform.operatingSystem,
      'version': pInfo.version
    };

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/updateDeviceToken',
      headers: await storage.getHeaders(),
      operation: operation[Operation.setFcmTokenV2]!,
      cancelToken: cancelToken,
      data: payload,
    );

    if (response['success']) {
      await storage.setSpecificData(StorageData.fcmToken, token);
    }
  }

  void subscribeToTopic(Topics key) {
    _fcm.subscribeToTopic(_topics[key]!).then((onValue) {
      log("Topic Subscribed Successfully");
    }).catchError((onError) {
      log("error subscribing to topic ${onError.toString()}");
    });
  }

  void unSubscribeToTopic(Topics key) {
    _fcm.unsubscribeFromTopic(_topics[key]!).then((onValue) {
      log("Topic Unsubscribed Successfully");
    }).catchError((onError) {
      log("error subscribing to topic ${onError.toString()}");
    });
  }

  Future<bool> deleteToken() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/logout',
      data: {'deviceToken': await storage.getFcmToken()},
      headers: await storage.getHeaders(),
      operation: operation[Operation.logout]!,
      cancelToken: cancelToken,
    );

    await storage.clearStorage();

    return response['success'];

    //
  }
}

Future<dynamic> myBackgroundMessageHandler(RemoteMessage message) async {
  final navigate = message.data['navigate_to'];
  final customData = jsonDecode(message.data['custom_data']);
  navigateToScreen(navigate, argument: customData);
}

Future<void> navigateToScreen(String? screen, {argument}) async {
  if (screen == "change_card") {
    // final cardViewModel = Provider.of<CreditCardViewModel>(
    //     myGlobals.homeScaffoldKey!.currentContext!,
    //     listen: false);
    // var creditCardUrl =
    //     '${Links.creditCardUrl}${cardViewModel.getSalesPerson()}&mode=shop&version=mobile';
    // log('creditCardUrl: $creditCardUrl');
    // if (await canLaunchUrlString(creditCardUrl)) {
    //   log('canLaunch ');
    //   await showCreditChangeUrl(
    //       myGlobals.homeScaffoldKey!.currentContext!, creditCardUrl);
    // } else {
    //   log('cant launch: $creditCardUrl');
    // }
  } else if (screen == "survey") {
    //trigger api here to get id for survey
    //....
    // final surveyModel = await Provider.of<UserPartnerService>(
    //         myGlobals.homeScaffoldKey!.currentContext!,
    //         listen: false)
    //     .partnerMainSurvey();

    // log('surveyUrl: ${surveyModel.surveyUrl}');
    // if (await canLaunchUrlString(surveyModel.surveyUrl!)) {
    //   log('canLaunch ');
    //   await showUrlDialog(myGlobals.homeScaffoldKey!.currentContext!,
    //       surveyModel.surveyUrl ?? '');
    // } else {
    //   log('cant launch: ${surveyModel.surveyUrl}');
    // }
  } else {
    if (screen!.contains(Routes.messageScreen)) {
      changeDrawerRoute(
        Routes.messageView,
        arguments: MessageV2Items(
            id: argument['messageId'], threadId: argument['threadId']),
      );
    } else if (screen.contains(Routes.marketPlaceScreen)) {
      changeDrawerRoute(Routes.marketPlaceScreen);
    } else if (screen.contains(Routes.addPage)) {
    } else if (screen.contains(Routes.yourClient)) {
      if (argument != null) {
        changeDrawerRoute(Routes.yourClient, arguments: {"cid": argument});
      } else {
        changeDrawerRoute(Routes.yourClient);
      }
    } else if (screen.contains(Routes.newsFeed) || screen.contains('home')) {
      myGlobals.drawerRouteKey!.currentState!
          .pushReplacementNamed(Routes.newsFeed);
    } else if (screen.contains(Routes.subscription)) {
      myGlobals.drawerRouteKey!.currentState!
          .pushReplacementNamed(Routes.subscription);
    } else if (screen.contains(Routes.jobTypes)) {
      myGlobals.drawerRouteKey!.currentState!
          .pushReplacementNamed(Routes.jobTypes);
    } else if (screen.contains(Routes.freeWebsiteSettings)) {
      myGlobals.drawerRouteKey!.currentState!
          .pushReplacementNamed(Routes.freeWebsiteSettings);
    } else if (screen.contains(Routes.changePassword)) {
      myGlobals.drawerRouteKey!.currentState!
          .pushReplacementNamed(Routes.changePassword);
    } else if (screen.contains(Routes.getRecommendations)) {
      myGlobals.drawerRouteKey!.currentState!
          .pushReplacementNamed(Routes.getRecommendations);
    } else if (screen.contains(Routes.faq)) {
      myGlobals.drawerRouteKey!.currentState!.pushReplacementNamed(Routes.faq);
    } else if (screen.contains(Routes.creditCards)) {
      myGlobals.drawerRouteKey!.currentState!
          .pushReplacementNamed(Routes.creditCards);
    } else if (screen.contains(Routes.m2mTaskScreen)) {
      myGlobals.drawerRouteKey!.currentState!
          .pushReplacementNamed(Routes.m2mTaskScreen);
    } else if (screen.contains(Routes.m2mLaborScreen)) {
      myGlobals.drawerRouteKey!.currentState!
          .pushReplacementNamed(Routes.m2mLaborScreen);
    } else if (screen.contains(Routes.readAndAnswerRecommendations) ||
        screen.contains('recommendations')) {
      changeDrawerRoute(Routes.readAndAnswerRecommendations);
    } else if (screen.contains(Routes.autoPilotRecommendations)) {
      myGlobals.drawerRouteKey!.currentState!
          .pushReplacementNamed(Routes.autoPilotRecommendations);
    } else if (screen.contains(Routes.createProject)) {
      myGlobals.drawerRouteKey!.currentState!
          .pushReplacementNamed(Routes.createProject);
    } else if (screen.contains(Routes.myProjects)) {
      myGlobals.drawerRouteKey!.currentState!
          .pushReplacementNamed(Routes.myProjects);
    } else if (screen.contains(Routes.emailLogin)) {
      myGlobals.drawerRouteKey!.currentState!
          .pushReplacementNamed(Routes.emailLogin);
    } else if (screen.contains(Routes.allMailbox)) {
      myGlobals.drawerRouteKey!.currentState!
          .pushReplacementNamed(Routes.allMailbox);
    } else if (screen.contains(Routes.webshopBoostSpecificRecommendation)) {
      log('Routes.webshopBoostSpecificRecommendation argument: $argument');
      myGlobals.drawerRouteKey!.currentState!.pushReplacementNamed(
          Routes.webshopBoostSpecificRecommendation,
          arguments: argument);
    } else {
      myGlobals.drawerRouteKey!.currentState!
          .pushReplacementNamed(Routes.newsFeed);
    }
  }
}

Future perFormExtraAction(String route, String action,
    {Map<String, dynamic>? arguments}) async {
  switch (route) {
    case Routes.marketPlaceScreen:
      switch (action) {
        case ExtraActions.refresh:
          if (myGlobals.homeScaffoldKey!.currentContext != null) {
            // await refreshMarketPlaceList(
            //     myGlobals.homeScaffoldKey!.currentContext!);
          }
          break;
      }
      break;
    case Routes.klimaPlan:
      switch (action) {
        case ExtraActions.refresh:
          if (myGlobals.homeScaffoldKey!.currentContext != null) {}
          break;
      }

      break;
    case Routes.messageScreen:
      // if (myGlobals.homeScaffoldKey!.currentContext != null) {
      //   Provider.of<MessageIconViewModel>(
      //           myGlobals.homeScaffoldKey!.currentContext!,
      //           listen: false)
      //       .messageCounter += 1;
      // }
      break;
    case Routes.webshopAdsHome:
      switch (action) {
        case ExtraActions.adBoosting:
          if (myGlobals.homeScaffoldKey!.currentContext != null) {
            // await showAdBoostingDialog(
            //     myGlobals.homeScaffoldKey!.currentContext!,
            //     boostPrice: arguments!['boostPrice'],
            //     reviewId: arguments['reviewId']);
          }
          break;
      }
      break;
  }
}
