import 'package:Haandvaerker.dk/model/file/file_data_model.dart';
import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

@immutable
abstract class FilesServiceBase {
  //

  Future<List<FileData>?> getFiles({required int folderId});

  Future<List<PublicFileData>> getPublicFiles({required int projectParentId});

  Future<List<FileData>> getProjectFiles(
      {required int projectId, required String category});

  Future<FileData> addFile({required Map<String, dynamic> payload});

  Future<MinboligApiResponse> deleteFile({required int fileId});

  Future<MinboligApiResponse> uploadMultipleFiles({required FormData payload});

  //
}
