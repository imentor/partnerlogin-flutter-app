import 'package:Haandvaerker.dk/model/file/file_data_model.dart';
import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/file/file_service_base.dart';
import 'package:dio/dio.dart';

class FilesService extends FilesServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  FilesService({required this.apiService, required this.storage});

  @override
  Future<List<FileData>?> getFiles({required int folderId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/folder/$folderId/children',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getFiles]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return FileData.fromCollection(response['data'] as List<dynamic>);
    } else {
      return null;
    }

    //
  }

  @override
  Future<List<PublicFileData>> getPublicFiles(
      {required int projectParentId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/project/$projectParentId/getAddresPublicFiles',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getPublicFiles]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return PublicFileData.fromCollection(response['data'] as List<dynamic>);
    } else {
      return [];
    }

    //
  }

  @override
  Future<List<FileData>> getProjectFiles(
      {required int projectId, required String category}) async {
    ///

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/project/$projectId/filesV2',
      headers: await storage.getHeaders(),
      queryParameters: {'category': category},
      operation: operation[Operation.getProjectFiles]!,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return FileData.fromCollection(response['data'] as List<dynamic>);
    } else {
      return [];
    }

    ///
  }

  @override
  Future<FileData> addFile({required Map<String, dynamic> payload}) async {
    ///

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/file',
      headers: await storage.getHeaders(),
      data: FormData.fromMap(payload),
      operation: operation[Operation.addFile]!,
      cancelToken: cancelToken,
    );

    return FileData.fromJson(Map<String, dynamic>.from(response['data']));

    ///
  }

  @override
  Future<MinboligApiResponse> deleteFile({required int fileId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.DELETE,
      path: '/partner/file/$fileId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.deleteFile]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> uploadMultipleFiles(
      {required FormData payload}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/files',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.uploadMultipleFiles]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  //
}
