import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/photo_doc/photo_doc_service_base.dart';
import 'package:dio/dio.dart';

class PhotoDocService extends PhotoDocServiceBase {
  PhotoDocService({required this.apiService, required this.storage});
  final ApiService apiService;
  final SecuredStorageService storage;

  @override
  Future<MinboligApiResponse> getPhotoDocuments(
      {required int projectId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/project/$projectId/photoDocumentation',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getPhotoDocuments]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }
}
