import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class PhotoDocServiceBase {
  Future<MinboligApiResponse> getPhotoDocuments({required int projectId});
}
