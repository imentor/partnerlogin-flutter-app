import 'dart:io';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/partner/case_project_model.dart';
import 'package:Haandvaerker.dk/model/projects/approved_projects_model.dart';
import 'package:Haandvaerker.dk/model/projects/manufacture_model.dart';
import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/model/projects/project_timeline_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class ProjectsServiceBase {
  //

  Future<List<ApprovedProjectsModel>> approvedProjects();

  Future<List<PartnerProjectsModel>> partnerProjects();

  Future<ProjectTimelineModel> getProjectTimeline({required int projectId});

  Future<MinboligApiResponse> getPartnerContacts();

  Future<MinboligApiResponse?> getProjectById(
      {required int projectId, String? pageRedirect});

  Future<bool> deleteShowCaseProject({required int projectId});

  Future<List<ManufactureModel>> getAllManufactures();

  Future<int> createShowCaseProject({required String title});

  Future<String> updateShowCaseProject({required Map<String, dynamic> payload});

  Future<bool> updateShowCaseProjectPicture({
    required List<File> files,
    required int projectId,
    required String description,
  });

  Future<bool> updateShowCaseStatus(
      {required int projectId, required String stage});

  Future<bool> deleteShowCasePicture({required int fileId});

  Future<CaseProjectModel> getPartnerProject({required int projectId});

  Future<MinboligApiResponse> getSubprojects({required int projectId});

  Future<MinboligApiResponse> updateProject(
      {required int projectId, required Map<String, dynamic> params});

  //
}
