import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/projects/extra_work_response_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/projects/extra_work/extra_work_service_base.dart';
import 'package:dio/dio.dart';

class ExtraWorkService extends ExtraWorkServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  ExtraWorkService({required this.apiService, required this.storage});

  @override
  Future<ExtraWorkResponseModel?> getExtraWork({
    required Map<String, dynamic> params,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/extra_work',
      headers: await storage.getHeaders(),
      queryParameters: params,
      operation: operation[Operation.getExtraWork]!,
      cancelToken: cancelToken,
    );

    return response != null
        ? ExtraWorkResponseModel.fromJson(
            Map<String, dynamic>.from(response['data']))
        : null;

    //
  }

  @override
  Future<MinboligApiResponse?> addExtraWork({
    required Map<String, dynamic> params,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/extra_work',
      headers: await storage.getHeaders(),
      data: params,
      operation: operation[Operation.addExtraWork]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;

    //
  }

  @override
  Future<MinboligApiResponse?> updateExtraWork({
    required Map<String, dynamic> params,
  }) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/extra_work/update/${params['extraWorkId']}',
      headers: await storage.getHeaders(),
      data: {
        'price': params['price'],
        'endDate': params['endDate'],
        'description': params['description'],
        'source': params['source'],
      },
      operation: operation[Operation.updateExtraWork]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;

    //
  }

  @override
  Future<MinboligApiResponse?> approveExtraWork({
    required Map<String, dynamic> params,
  }) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/extra_work/${params['extraWorkId']}/approve',
      headers: await storage.getHeaders(),
      data: {
        "paymentStages": params['paymentStages'],
        "signaturePng": params['signaturePng'],
      },
      operation: operation[Operation.approveExtraWork]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> rejectExtraWork(
      {required int extraWorkId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/RejectExtraWork',
      headers: await storage.getHeaders(),
      queryParameters: {
        "extraWorkId": extraWorkId,
      },
      operation: operation[Operation.rejectExtraWork]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }

  @override
  Future<MinboligApiResponse?> deleteExtraWork(
      {required int extraWorkId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/extra_work/$extraWorkId/delete',
      headers: await storage.getHeaders(),
      operation: operation[Operation.deleteExtraWork]!,
      cancelToken: cancelToken,
    );

    return response != null ? MinboligApiResponse.fromJson(response) : null;
  }
}
