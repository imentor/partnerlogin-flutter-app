import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/projects/extra_work_response_model.dart';
import 'package:flutter/material.dart';

@immutable
abstract class ExtraWorkServiceBase {
  //

  Future<ExtraWorkResponseModel?> getExtraWork(
      {required Map<String, dynamic> params});

  Future<MinboligApiResponse?> addExtraWork(
      {required Map<String, dynamic> params});

  Future<MinboligApiResponse?> updateExtraWork(
      {required Map<String, dynamic> params});

  Future<MinboligApiResponse?> approveExtraWork(
      {required Map<String, dynamic> params});

  Future<MinboligApiResponse?> rejectExtraWork({required int extraWorkId});

  Future<MinboligApiResponse?> deleteExtraWork({required int extraWorkId});

  //
}
