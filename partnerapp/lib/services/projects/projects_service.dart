import 'dart:io';

import 'package:Haandvaerker.dk/model/generic_response_model.dart';
import 'package:Haandvaerker.dk/model/partner/case_project_model.dart';
import 'package:Haandvaerker.dk/model/projects/approved_projects_model.dart';
import 'package:Haandvaerker.dk/model/projects/manufacture_model.dart';
import 'package:Haandvaerker.dk/model/projects/partner_projects_model.dart';
import 'package:Haandvaerker.dk/model/projects/project_timeline_model.dart';
import 'package:Haandvaerker.dk/services/api/api_operations.dart';
import 'package:Haandvaerker.dk/services/api/api_service.dart';
import 'package:Haandvaerker.dk/services/data_cache/secured_storage_service.dart';
import 'package:Haandvaerker.dk/services/projects/projects_service_base.dart';
import 'package:dio/dio.dart';

class ProjectsService extends ProjectsServiceBase {
  //

  final ApiService apiService;
  final SecuredStorageService storage;

  ProjectsService({required this.apiService, required this.storage});

  @override
  Future<List<ApprovedProjectsModel>> approvedProjects() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/projects/approved',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getApprovedProjects]!,
      cancelToken: cancelToken,
    );

    return ApprovedProjectsModel.fromCollection(
        response['data'] as List<dynamic>);

    //
  }

  @override
  Future<List<PartnerProjectsModel>> partnerProjects() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/projects',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getPartnerProjects]!,
      cancelToken: cancelToken,
    );

    return PartnerProjectsModel.fromCollection(
        response['data'] as List<dynamic>);

    //
  }

  @override
  Future<ProjectTimelineModel> getProjectTimeline(
      {required int projectId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/project/$projectId/timeline/pending',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getProjectTimeline]!,
      cancelToken: cancelToken,
    );

    return ProjectTimelineModel.fromJson(
        Map<String, dynamic>.from(response['data']));

    //
  }

  @override
  Future<MinboligApiResponse> getPartnerContacts() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/contacts',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getPartnerContacts]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);

    //
  }

  @override
  Future<MinboligApiResponse?> getProjectById(
      {required int projectId, String? pageRedirect}) async {
    final query = {'contractType': 'normal'};

    if ((pageRedirect ?? '').contains('subcontractor')) {
      query['contractType'] = 'subcontractor';
    } else if ((pageRedirect ?? '').contains('producent')) {
      query['contractType'] = 'producent';
    }

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/project/$projectId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getProjectById]!,
      queryParameters: query,
      cancelToken: cancelToken,
    );

    if (response != null) {
      return MinboligApiResponse.fromJson(response);
    } else {
      return null;
    }
  }

  @override
  Future<bool> deleteShowCaseProject({required int projectId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/project/DeleteShowCase',
      headers: await storage.getHeaders(),
      data: {'PROJECTID': projectId},
      operation: operation[Operation.deleteShowCaseProject]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response).success!;

    //
  }

  @override
  Future<List<ManufactureModel>> getAllManufactures() async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/manufacture/GetAllManufacture/3000',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getAllManufactures]!,
      cancelToken: cancelToken,
    );

    return response != null && response['data'] != null
        ? ManufactureModel.fromCollection(List<dynamic>.from(response['data']))
        : [];

    //
  }

  @override
  Future<int> createShowCaseProject({required String title}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/project/CreateShowCase',
      headers: await storage.getHeaders(),
      data: {'TITLE': title},
      operation: operation[Operation.createShowCaseProject]!,
      cancelToken: cancelToken,
    );

    return Map<String, dynamic>.from(response['data'])['PROJECTID'];

    //
  }

  @override
  Future<String> updateShowCaseProject(
      {required Map<String, dynamic> payload}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/project/updateShowCase',
      headers: await storage.getHeaders(),
      data: payload,
      operation: operation[Operation.updateShowCaseProject]!,
      cancelToken: cancelToken,
    );

    final updated = List<Map<String, dynamic>>.from(
            Map<String, dynamic>.from(response['data'])['new'])
        .first;

    return updated['PROJECTURL'];

    //
  }

  @override
  Future<bool> updateShowCaseProjectPicture({
    required List<File> files,
    required int projectId,
    required String description,
  }) async {
    //

    List<bool> responses = [];

    for (final file in files) {
      final map = {
        'PROJECTID': projectId,
        'DESCRIPTION': description,
      };

      map['files[]'] = await MultipartFile.fromFile(
        file.path,
        filename: file.path.split('/').last,
      );

      final payload = FormData.fromMap(map);
      final CancelToken cancelToken = CancelToken();
      final response = await apiService.makeRequest(
        method: RequestMethod.POST,
        path: '/partner/project/UploadShowCasePicture',
        headers: await storage.getHeaders(),
        data: payload,
        operation: operation[Operation.updateShowCaseProjectPicture]!,
        cancelToken: cancelToken,
      );

      responses.add(response['success']);
    }

    return responses.any((element) => !element);

    //
  }

  @override
  Future<bool> updateShowCaseStatus(
      {required int projectId, required String stage}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/project/updateShowCaseSTATUS',
      headers: await storage.getHeaders(),
      data: {'PROJECTID': projectId, 'STAGE': stage},
      operation: operation[Operation.updateShowCaseStatus]!,
      cancelToken: cancelToken,
    );

    return response['success'];

    //
  }

  @override
  Future<bool> deleteShowCasePicture({required int fileId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.POST,
      path: '/partner/project/DeleteShowCasePicture',
      headers: await storage.getHeaders(),
      data: {'FILEID': fileId},
      operation: operation[Operation.deleteShowCasePicture]!,
      cancelToken: cancelToken,
    );

    return response['success'];

    //
  }

  @override
  Future<CaseProjectModel> getPartnerProject({required int projectId}) async {
    //

    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/supplier/GetprojectById',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getPartnerProject]!,
      cancelToken: cancelToken,
    );

    return CaseProjectModel.fromJson(
        Map<String, dynamic>.from(response['data']));

    //
  }

  @override
  Future<MinboligApiResponse> getSubprojects({required int projectId}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.GET,
      path: '/partner/project/GetSubProjects/$projectId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getSubprojects]!,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  @override
  Future<MinboligApiResponse> updateProject(
      {required int projectId, required Map<String, dynamic> params}) async {
    final CancelToken cancelToken = CancelToken();
    final response = await apiService.makeRequest(
      method: RequestMethod.PUT,
      path: '/partner/project/$projectId',
      headers: await storage.getHeaders(),
      operation: operation[Operation.getSubprojects]!,
      queryParameters: params,
      cancelToken: cancelToken,
    );

    return MinboligApiResponse.fromJson(response);
  }

  //
}
