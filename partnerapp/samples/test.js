displayProducts () {
      this.insideLoading = true
      var data = {
        operation: 'bitrixApi',
        salesPerson: this.salesPerson,
        type: 'GET',
        url: 'webshop/GetProducts/0/0/' + this.activeTabName + '?page=' + this.curPage
      }
      this.$http.post('', data, { emulateJSON: true }).then(
        (response) => {
          var res = response.body
          if (res.success) {
            var dataRes = response.data.data
            var pagLastPage = response.data.data.last_page + '0'
            var products = []
            var deductBadgesCount = 0
            for (var j = 0; j < dataRes.length; j++) {
              for (var k = 0; k < dataRes[j].get_element_property.length; k++) {
                if (dataRes[j].get_element_property[k].IBLOCK_PROPERTY_ID === 427) {
                  if (dataRes[j].get_element_property[k].VALUE === this.haaId) {
                    products.push(dataRes[j])
                  } else {
                    deductBadgesCount++
                  }
                }
              }
            }
            console.log('get all products', products)
            this.prodData = products
            this.deductBadgesProduct = deductBadgesCount
            this.lastPage = pagLastPage
            if (!dataRes.length) {
              this.prodMsg = 'Ingen Produkter.'
            } else {
              this.prodMsg = ''
            }
            if (this.activeTabName === '205') {
              this.showBreadCrumb()
              this.getBadges()
              for (var i = 0; i < this.prodData.length; i++) {
              // Get product badges id
                for (var x = 0; x < this.prodData[i].get_element_property.length; x++) {
                  if (this.prodData[i].get_element_property[x].IBLOCK_PROPERTY_ID === 428) {
                    this.prodBadgesIdsArr.push(parseInt(this.prodData[i].get_element_property[x].VALUE))
                  }
                }
              }
            }
            for (var e = 0; e < this.prodData.length; e++) {
              if (this.prodData[e].SIZE === '') {
                this.prodData[e].EXT = '0'
              } else {
                this.prodData[e].EXT = this.prodData[e].SIZE.replace(/\s/g, '').replaceAll('B', '').replaceAll('H', '')
              }
              this.prodData[e].LOAD = true
            }
          } else {
            this.showBreadCrumb()
            this.loading = false
            this.insideLoading = false
            console.log('Error in displayProducts()')
          }
        },
        (response) => {
          console.log('Error: ', response)
        }
  )
}